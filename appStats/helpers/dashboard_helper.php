<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

#koushik
function get_all_tool_type($tool_type_id)
{
    $CI = & get_instance();
    $CI->db->select('tool_type_id,name');
    $CI->db->from('tool_type');
    $CI->db->where('status',1);
    if($tool_type_id!='')
    $CI->db->where('tool_type_id',$tool_type_id);
    $res = $CI->db->get();
    return $res->result_array();
}

#koushik
function getCalibrationDueData($searchParams)
{
	$ci = & get_instance();
	$zone = $searchParams['zone'];
	$modality_id = $searchParams['modality_id'];
	$wh_id = $searchParams['wh_id'];
	$tool_type_id = $searchParams['tool_type_id'];

	$m_arr = get_all_modality($modality_id);
	$t_arr = get_all_tool_type($tool_type_id);
	$m_list = array(); 
	$c1Data1 = array();
	$t_list = array(); 
	$c2Data2 = array(); $xAxisCategory = array();

	foreach ($m_arr as $mod) 
	{
		$m_list[] = $mod['modality_id'];
	}
	foreach ($t_arr as $tool_type) 
	{
		$t_list[] = $tool_type['tool_type_id'];
	}
	
	if($zone == 1 && $wh_id == '')
	{ 
		$zone_list = $ci->Report_m->get_zones_based_on_country($searchParams);
		foreach ($zone_list as $zon) 
		{
			if($wh_id == '')
			{
				$wh_list = get_wh_by_zone($zon['location_id'],$searchParams['task_access']);
			}
			else
			{ 
				$wh_list = array($wh_id); 
			}
			$cal_date = get_cal_by_due_date($m_list,$wh_list,$t_list);
			$c1Data1[] = $cal_date[0];
			$c2Data2[] = $cal_date[1];
			$c3Data3[] = $cal_date[2];
			$xAxisCategory[] = $zon['name'];
		}
		$xAxisLable = 'Zone';
	}
	else
	{
		if($wh_id == '')
		{ 
			$wh_list = get_wh_by_zone($zone,$searchParams['task_access']); 
		}
		else 
		{ 
			$wh_list = array($wh_id); 
		}
		foreach ($wh_list as $wh) 
		{
			$wh_arr = array($wh);
			$cal_date = get_cal_by_due_date($m_list,$wh_arr,$t_list);
			$c1Data1[] = $cal_date[0];
			$c2Data2[] = $cal_date[1];
			$c3Data3[] = $cal_date[2];
			$wh_data = $ci->Common_model->get_data_row('warehouse',array('wh_id'=>$wh));
			$xAxisCategory[] = $wh_data['wh_code'].' -('.$wh_data['name'].')';
		}
		$xAxisLable = 'Warehouse';
	}
	$series = array();
	$series []= array('name'=>'90 Days','data'=>$c1Data1);
	$series []= array('name'=>'45 Days','data'=>$c2Data2);
	$series []= array('name'=>'30 Days','data'=>$c3Data3);
	
   $text ="Calibration Queue";
   if($searchParams['category']!='')
   {
   	 $text .=" For ".$searchParams['category'];
   }
	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'flag'=>2,'first_x_cat'=>$searchParams['category'],'text'=>$text);
	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

#Prasad
function getCalibrationDueDataForCountries($searchParams)
{
	$ci = & get_instance();
	$zone = $searchParams['zone'];
	$modality_id = $searchParams['modality_id'];
	$wh_id = $searchParams['wh_id'];
	$tool_type_id = $searchParams['tool_type_id'];

	$m_arr = get_all_modality($modality_id);
	$t_arr = get_all_tool_type($tool_type_id);
	$m_list = array(); 
	$c1Data1 = array();
	$t_list = array(); 
	$c2Data2 = array();
	$c3Data3 = array(); $xAxisCategory = array();

	foreach ($m_arr as $mod) 
	{
		$m_list[] = $mod['modality_id'];
	}
	foreach ($t_arr as $tool_type) 
	{
		$t_list[] = $tool_type['tool_type_id'];
	}

	if($searchParams['country_id']!='')
	{
		$country_arr = array($searchParams['country_id']);
	}
	else
	{
		$country_arr = $_SESSION['countriesIndexedArray'];
	}

	foreach ($country_arr as $key => $value) 
	{
		$country_id = $value;
		$country_name = $ci->Common_model->get_value('location',array('location_id'=>$country_id),'name');
		$cal_date = get_cal_by_due_date_for_countries($m_list,$t_list,$wh_id,$country_id);
		$c1Data1[] = $cal_date[0];
		$c2Data2[] = $cal_date[1];
		$c3Data3[] = $cal_date[2];
		$xAxisCategory[] = $country_name;
	}

	if($searchParams['task_access']==3 && $wh_id == '')
	{
		$xAxisLable = 'Country';
	}
	else if($wh_id == '')
	{
		$xAxisLable = 'Zone';
	}
	else
	{
		$xAxisLable = 'Warehouse';
	}
    $text = "Calibration Queue";
    if($searchParams['country_id']!='')
    {
    	$country_name = $ci->Common_model->get_value('location',array('location_id'=>$searchParams['country_id']),'name');
    	$text .=" For ".$country_name;
    }
    else
    {
    	$text .=" For All Countries";
    }
	$series = array();
	$series []= array('name'=>'90 Days','data'=>$c1Data1);
	$series []= array('name'=>'45 Days','data'=>$c2Data2);
	$series []= array('name'=>'30 Days','data'=>$c3Data3);
	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'flag'=>1,'text'=>$text);
	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

#koushik
function get_cal_by_due_date($m_list,$wh_list,$t_list)
{
	$ci= & get_instance();
	$ci->db->select('DATEDIFF(a.cal_due_date,now())as days,a.asset_id');
	$ci->db->from('asset a');
	$ci->db->join('rc_asset rca','rca.asset_id = a.asset_id');
	$ci->db->join('part p','p.asset_id = a.asset_id');
	$ci->db->join('tool t','t.tool_id = p.tool_id');
	$ci->db->where('p.part_level_id',1);
	$ci->db->where('rca.status',1);
	$ci->db->where('rca.current_stage_id >=',11);
	$ci->db->where('rca.current_stage_id <=',16);
	$ci->db->where_in('a.modality_id',$m_list);
	$ci->db->where_in('a.wh_id',$wh_list);
	$ci->db->where_in('t.tool_type_id',$t_list);
	$res = $ci->db->get();
	$asset_count_1 = 0;
	$asset_count_2 = 0;
	$asset_count_3 = 0;
	foreach($res->result_array() as $row)
	{
		if($row['days']<=90) $asset_count_1++;
		if($row['days']<=45) $asset_count_2++;
		if($row['days']<=30) $asset_count_3++;
	}
	$a1 = $asset_count_1-$asset_count_2;
	$a2 = $asset_count_2-$asset_count_3;
	$a3 = $asset_count_3;
	return array($a1,$a2,$a3);
}

function get_cal_by_due_date_for_countries($m_list,$t_list,$wh_id,$country_id)
{	
	$ci= & get_instance();
	$ci->db->select('DATEDIFF(a.cal_due_date,now())as days,a.asset_id');
	$ci->db->from('asset a');
	$ci->db->join('rc_asset rca','rca.asset_id = a.asset_id');
	$ci->db->join('part p','p.asset_id = a.asset_id');
	$ci->db->join('tool t','t.tool_id = p.tool_id');
	$ci->db->join('location l','a.country_id=l.location_id');
	$ci->db->join('warehouse w','a.wh_id=w.wh_id');
	if($wh_id != '')
	{
		$ci->db->Where('a.wh_id',$wh_id);
	}
	$ci->db->where('a.country_id',$country_id);
	$ci->db->where('p.part_level_id',1);
	$ci->db->where('rca.status',1);
	$ci->db->where('w.status',1);
	$ci->db->where('rca.current_stage_id >=',11);
	$ci->db->where('rca.current_stage_id <=',16);
	$ci->db->where_in('a.modality_id',$m_list);
	$ci->db->where_in('t.tool_type_id',$t_list);
	$res = $ci->db->get();
	$asset_count_1 = 0;
	$asset_count_2 = 0;
	$asset_count_3 = 0;
	foreach($res->result_array() as $row)
	{
		if($row['days']<=90) $asset_count_1++;
		if($row['days']<=45) $asset_count_2++;
		if($row['days']<=30) $asset_count_3++;
	}
	$a1 = $asset_count_1-$asset_count_2;
	$a2 = $asset_count_2-$asset_count_3;
	$a3 = $asset_count_3;
	return array($a1,$a2,$a3);
}

#koushik
function getCalStatusData($zone,$wh_id,$modality_id,$tool_type_id,$x_category,$first_x_cat,$task_access,$country_id)
{
	$ci = & get_instance();
	$zone = $zone;
	$modality_id = $modality_id;
	$wh_id = $wh_id;
	$tool_type_id = $tool_type_id;
	$category = $x_category;
	$stages = $ci->Common_model->get_data('current_stage',array('workflow_type'=>3,'status'=>1,'current_stage_id<'=>17));
	$tool_type = $ci->Common_model->get_data('tool_type',array('status'=>1));
	$m_arr = get_all_modality($modality_id);
	$t_arr = get_all_tool_type($tool_type_id);
	$c1Data1 = array(); 
	$c2Data2 = array();
	$c3Data3 = array();
	$tc1Data1 = array(); 
	$tc2Data2 = array();
	$tc3Data3 = array();
	$m_list = array();
	$t_list = array(); 
	$xAxisCategory = array();

	foreach ($m_arr as $mod) 
	{
		$m_list[] = $mod['modality_id'];
	}
	foreach ($t_arr as $tool) 
	{
		$t_list[] = $tool['tool_type_id'];
	}

	if($zone == 1 && $wh_id == '')
	{
		$location_id = $ci->Report_m->get_second_level_zone($category,$first_x_cat,$task_access);
		$wh_list = get_wh_by_zone($location_id);
		foreach ($stages as $key => $value) 
		{
			$s_data = get_cal_status_count($wh_list,$value['current_stage_id'],$m_list,$t_list);
			$c1Data1[] = $s_data[0];
			$c2Data2[] = $s_data[1];
			$c3Data3[] = $s_data[2];
			$xAxisCategory[] = $value['name'];
		}
	}
	else
	{
		$var = explode(" ", $category);
		$location = $ci->Report_m->get_wh_by_wh_code($var,$country_id);
		if($wh_id !='') $wh_list = array($wh_id);
		else $wh_list = array($location);
		foreach ($stages as $key => $value) 
		{
			$s_data = get_cal_status_count($wh_list,$value['current_stage_id'],$m_list,$t_list);
			$c1Data1[] = $s_data[0];
			$c2Data2[] = $s_data[1];
			$c3Data3[] = $s_data[2];
			$xAxisCategory[] = $value['name'];
		}
	}

	if($zone == 1 && $wh_id == '')
	{
		$location_id = $ci->Report_m->get_second_level_zone($category,$first_x_cat,$task_access);
		$wh_list = get_wh_by_zone($location_id);
		foreach ($tool_type as $key => $value) 
		{
			$ts_data = get_cal_status_count_by_tool_type($wh_list,$value['tool_type_id'],$m_list);
			$tc1Data1[] = $ts_data[0];
			$tc2Data2[] = $ts_data[1];
			$tc3Data3[] = $ts_data[2];
			$txAxisCategory[] = $value['name'].' Tool';
		}
	}
	else
	{
		$var = explode(" ", $category);
		$location = $ci->Report_m->get_wh_by_wh_code($var,$country_id);
		if($wh_id !='') $wh_list = array($wh_id);
		else $wh_list = array($location);
		if($tool_type_id !='')
		{
			$tool_type = $ci->Common_model->get_data('tool_type',array('tool_type_id'=>
				$tool_type_id));
		}
		foreach ($tool_type as $key => $value) 
		{
			$ts_data = get_cal_status_count_by_tool_type($wh_list,$value['tool_type_id'],$m_list);
			$tc1Data1[] = $ts_data[0];
			$tc2Data2[] = $ts_data[1];
			$tc3Data3[] = $ts_data[2];
			$txAxisCategory[] = $value['name'];
		}
	}

	$xAxisLable = 'Stages';
	$text = "Calibration Status".' For '.$category.'';
	$series = array();
	$series []= array('name'=>'90 Days','data'=>$c1Data1);
	$series []= array('name'=>'45 Days','data'=>$c2Data2);
	$series []= array('name'=>'30 Days','data'=>$c3Data3);


	$txAxisLable = 'Tool Type';
	$ttext = "Tool Type".' For '.$category.'';
	if($zone!=1)
	{   
		$zone_name = $ci->Common_model->get_value('location',array('location_id'=>$zone),'name');
		$ttext .=" Of ".$zone_name;
		$text .=" Of ".$zone_name;
	}
	if($first_x_cat!='')
	{
		$ttext .=" In ".$first_x_cat;
		$text .=" In ".$first_x_cat;
	}

	$tseries = array();
	$tseries []= array('name'=>'90 Days','data'=>$tc1Data1);
	$tseries []= array('name'=>'45 Days','data'=>$tc2Data2);
	$tseries []= array('name'=>'30 Days','data'=>$tc3Data3);

	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'text'=>$text,'txAxisCategory'=>$txAxisCategory,'tchart2Series'=>$tseries,'txAxisLable'=>$txAxisLable,'ttext'=>$ttext);

	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

#koushik
function get_cal_status_count($wh_list,$current_stage_id,$m_list,$t_list)
{
	$ci= & get_instance();
	$ci->db->select('DATEDIFF(a.cal_due_date,now())as days');
	$ci->db->from('asset a');
	$ci->db->join('rc_asset rca','rca.asset_id = a.asset_id');
	$ci->db->join('part p','p.asset_id = a.asset_id');
	$ci->db->join('tool t','t.tool_id = p.tool_id');
	$ci->db->where('p.part_level_id',1);
	$ci->db->where('rca.status',1);
	$ci->db->where('rca.current_stage_id',$current_stage_id);
	$ci->db->where_in('a.wh_id',$wh_list);
	$ci->db->where_in('a.modality_id',$m_list);
	$ci->db->where_in('t.tool_type_id',$t_list);
	$res = $ci->db->get();
	$asset_count_1 = 0;
	$asset_count_2 = 0;
	$asset_count_3 = 0;
	foreach($res->result_array() as $row)
	{
		if($row['days']<=90) $asset_count_1++;
		if($row['days']<=45) $asset_count_2++;
		if($row['days']<=30) $asset_count_3++;
	}
	$a1 = $asset_count_1-$asset_count_2;
	$a2 = $asset_count_2-$asset_count_3;
	$a3 = $asset_count_3;
	return array($a1,$a2,$a3);
}

function get_cal_status_count_by_tool_type($wh_list,$tool_type_id,$m_list)
{
	$ci= & get_instance();
	$ci->db->select('DATEDIFF(a.cal_due_date,now())as days');
	$ci->db->from('asset a');
	$ci->db->join('rc_asset rca','rca.asset_id = a.asset_id');
	$ci->db->join('part p','p.asset_id = a.asset_id');
	$ci->db->join('tool t','t.tool_id = p.tool_id');
	$ci->db->where('p.part_level_id',1);
	$ci->db->where('rca.status',1);
	$ci->db->where('rca.current_stage_id >=',11);
	$ci->db->where('rca.current_stage_id <=',16);
	$ci->db->where_in('a.wh_id',$wh_list);
	$ci->db->where_in('a.modality_id',$m_list);
	$ci->db->where('t.tool_type_id',$tool_type_id);
	$res = $ci->db->get();
	$asset_count_1 = 0;
	$asset_count_2 = 0;
	$asset_count_3 = 0;
	foreach($res->result_array() as $row)
	{
		if($row['days']<=90) $asset_count_1++;
		if($row['days']<=45) $asset_count_2++;
		if($row['days']<=30) $asset_count_3++;
	}
	$a1 = $asset_count_1-$asset_count_2;
	$a2 = $asset_count_2-$asset_count_3;
	$a3 = $asset_count_3;
	return array($a1,$a2,$a3);
}

function getToolStatusFirstPieData($searchParams)
{
	$ci = & get_instance();
	$toolStatusResult = getActiveOrInactiveCountInToolReport($searchParams);
	// looping for active or inactive
    $finalToolStatusResult = array();
	foreach ($toolStatusResult as $key => $value) {
	    $finalToolStatusResult[$value['type']] = $value['count'] ;
	}

	$piedata= array();
	$sampleResultSet = array( 
							array('status'=>'Active','tool_count'=>@$finalToolStatusResult[1]),
							array('status'=>'Inactive','tool_count'=>@$finalToolStatusResult[2])
						);
	foreach($sampleResultSet as $row){
	   $resData = array('name' => $row['status'], 'y'=>(int)$row['tool_count']);
	   $piedata[] = $resData;
	}
	$firstPieData = array();
	//$firstPieData[]=$searchParams['category'];
	$firstPieData []= array('name'=>'Tool Count','colorByPoint'=>true,'data'=>$piedata,'flag'=>2);
	$firstPieData = json_encode($firstPieData);
	return $firstPieData;
}
function getToolStatusFirstDataCountries($searchParams)
{
	$ci = & get_instance();
	$c1Data1=array();
	$c2Data2=array();
	$xAxisCategory = array();
	$toolStatusResult = getActiveOrInactiveCountInToolReportForCountries($searchParams);
	$tools_array=array();
	foreach($toolStatusResult as $key =>$value)
	{
		$tools_array[$value['country_name']][$value['type']]['count'] =$value['count'];
	}
	foreach ($tools_array as $k1 => $v1) {
		$xAxisCategory[]=$k1;
		/*foreach ($v1 as $k2  => $v2) {
			if($k2 ==1)
			{
				$c1Data1[]=(int)$v2['count'];
			}
			if($k2 ==2)
			{
				$c2Data2[]=(int)$v2['count'];
			}
		}*/
		// Modified By Maruthi on 17thMay19	
		$c1Data1[]=(int)@$v1[1]['count'];
		$c2Data2[]=(int)@$v1[2]['count'];
	}
	$text = "Tool Inventory Report";
	if($searchParams['country_id']!='')
    {
    	$country_name = $ci->Common_model->get_value('location',array('location_id'=>$searchParams['country_id']),'name');
    	$text .=" For ".$country_name;
    }
    else
    {
    	$text .=" For All Countries";
    }
	$xAxisLable = "Countries";
	$series = array();
	$series []= array('name'=>'Active','data'=>$c1Data1);
	$series []= array('name'=>'Inactive','data'=>$c2Data2);
	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'flag'=>1,'first_x_cat'=>$searchParams['category'],'text'=>$text);
	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

function getToolStatusData2($zone,$wh_id,$modality_id,$asset_status,$tool_type,$first_x_cat,$country_id,$task_access,$tool_status_name)
{
	$CI = &get_instance();	
    $searchParams2 = array(
        'r_status'       => $asset_status,
        'r_zone'         => @$zone,
        'r_wh_id'        => @$wh_id,
        'r_modality_id'  => @$modality_id,
        'r_tool_type'    => @$tool_type,
        'country_id'     => $country_id,
        'first_x_cat'    => $first_x_cat,
        'task_access'    => $task_access
        );
    //echo '<pre>';print_r($searchParams2);exit;
    // looping the warehouses or zones
    $WhorZoneArr = getWHorZonedataToolReport($searchParams2);
    //echo '<pre>';print_r($WhorZoneArr);exit;
    if(count($WhorZoneArr)>0)
    {
        //echo '<pre>';print_r($WhorZoneArr);
        $WhorZone = $WhorZoneArr[0];
        $type = $WhorZoneArr[1];
        //echo '<pre>';print_r($WhorZone);exit;

        $status_arr = $CI->Common_model->get_data('asset_status',array('type'=>$searchParams2['r_status']));
        
        //echo '<pre>';print_r($status_arr);exit;
        $dy_arr = array();
        foreach ($status_arr as $key => $value1)
        {
            $dy_arr[$value1['asset_status_id']] = $value1['name'];
        }
        if($asset_status == 1)
        {
            $at_warehouse = array(); $lock_in_period = array(); $field_deployed = array(); $in_calibration = array(); $in_transit = array(); $out_of_calibration = array();
        }
        else
        {
            $defective = array(); $missed = array(); $lost =array(); $out_of_tolerance = array();
            $in_repair = array(); $scaraped = array();
        }
        //$i =1;
        $xAxisCategory = array();

        foreach ($WhorZone as $key => $row)
        {
            $status_data_arr = array();
            $status_data = getWHorZoneStatusData(@$row['location_id'],$type,@$row['wh_id'],$searchParams2);
            //echo '<pre>';print_r($status_data);
            //echo count($status_data);exit;
            //exit;
            if($status_data > 0)
            { //echo 'popo';exit();
                foreach($status_data as  $value3)
                {
                   // echo $i.'--';
                    $status_data_arr[$value3['status']] = $value3['count'];
                }
            } //echo 'lkl';exit;
           // $i++;
            //echo '<pre>';print_r($status_data_arr);exit;
           // echo $asset_data_arr[1];exit;
            if($asset_status  == 1)
            {
                $at_warehouse[]       = ((int)@$status_data_arr[1]);
                $lock_in_period[]     = ((int)@$status_data_arr[2]);
                $in_transit[]         = ((int)@$status_data_arr[8]);
                $field_deployed[]     = ((int)@$status_data_arr[3]);
                $in_calibration[]     = ((int)@$status_data_arr[4]);
                $out_of_calibration[] = ((int)@$status_data_arr[12]);
            }
            else
            {
                $defective[]          = ((int)@$status_data_arr[5]);
                $missed[]             = ((int)@$status_data_arr[6]);
                $lost[]               = ((int)@$status_data_arr[7]);
                $out_of_tolerance[]   = ((int)@$status_data_arr[9]);
                $in_repair[]          = ((int)@$status_data_arr[10]);
                $scaraped[]           = ((int)@$status_data_arr[11]);
            }
            $xAxisCategory[] = $row['name'];			
        }
        $xAxisLable =($type == 1)?'Zones':'Warehouse';
       
    }
	
    $text= $tool_status_name.' Tools';
    if($zone!=1)
    {
    	$zone_name = $CI->Common_model->get_value('location',array('location_id'=>$zone),'name');
    	$text.= ' In '.$zone_name;
    }
	$series = array();
	if($asset_status == 1)
	{
		$series []= array('name'=>'At Warehouse','data'=>$at_warehouse);
		$series []= array('name'=>'Lockin Period','data'=>$lock_in_period);
		$series []= array('name'=>'Field Deployed','data'=>$field_deployed);
		$series []= array('name'=>'In Calibration','data'=>$in_calibration);
		$series []= array('name'=>'In Transit','data'=>$in_transit);		
		$series []= array('name'=>'Out Of Calibration','data'=>$out_of_calibration);
	}
	else
	{
		$series []= array('name'=>'defective','data'=>$defective);
		$series []= array('name'=>'missed','data'=>$missed);
		$series []= array('name'=>'Lost','data'=>$lost);
		$series []= array('name'=>'Out Of Tolerence','data'=>$out_of_tolerance);
		$series []= array('name'=>'In Repair','data'=>$in_repair);
		$series []= array('name'=>'Scraped','data'=>$scaraped);
	}
	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'text'=>$text);
	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

function getToolStatusData($zone,$wh_id,$modality_id,$tool_type)
{
	$searchParams = array(
                'r_zone'          => @$zone,
                'r_wh_id'         => @$wh_id,
                'r_modality_id'   => @$modality_id,
                'r_tool_type'     => @$tool_type,
                );
	$toolStatusResult = getActiveOrInactiveCount(@$searchParams);
            // looping for active or inactive
            $finalToolStatusResult = array();
            foreach ($toolStatusResult as $key => $value) {
                $finalToolStatusResult[$value['type']] = $value['count'] ;
            }
           /* //echo '<pre>';print_r($finalToolStatusResult);exit;
            $pi_chart = array();
            $pi_chart[] = array("Active",(int)@$finalToolStatusResult[1]);
            $pi_chart[] = array("InActive",(int)@$finalToolStatusResult[2]);
            $data['pi_chart'] = json_encode($pi_chart);*/

	$piedata= array();
	$sampleResultSet = array( 
							array('status'=>'Active','tool_count'=>@$finalToolStatusResult[1]),
							array('status'=>'Inactive','tool_count'=>@$finalToolStatusResult[2])
						);
		foreach($sampleResultSet as $row){
		   $resData = array('name' => $row['status'], 'y'=>(int)$row['tool_count']);
		   $piedata[] = $resData;
		}
		$firstPieData = array();
		$firstPieData []= array('name'=>'Tool Count','colorByPoint'=>true,'data'=>$piedata);
		return json_encode($firstPieData);

}

function getToolStatusData3($zone,$warehouse,$post_modality_id,$asset_status,$zone_or_wh,$tool_type,$first_x_cat,$country_id,$task_access)
{
	$CI = &get_instance();	
	$asset_status = ($asset_status =='Active')?1:2;
    $post_value = $zone_or_wh;  
    //echo $post_value;exit;  
    $post_type_data = $CI->Common_model->get_data('warehouse',array('name'=>trim(strtolower($post_value))));
    if(count($post_type_data) > 0)
    {
        // get the warehouse id 
        $third_level_id = $CI->Common_model->get_value('warehouse',array('name'=>trim(strtolower($post_value))),'wh_id');
        $third_level_type  = 2;
    }
    else
    {   
    	if($country_id!='')
       {
           	$parent_id = $country_id;
       }
       elseif($task_access==1 || $task_access==2)
       {
       	  $parent_id = $_SESSION['s_country_id'];
       }
       elseif($task_access==3)
       {
       	    if($_SESSION['header_country_id']!='')
       	    {
       	    	$parent_id = $_SESSION['header_country_id'];
       	    }
       	    else{
       	    	$parent_id = $CI->Common_model->get_value('location',array('name'=>$first_x_cat),'location_id');
			}
        }
        $third_level_id = $CI->Common_model->get_value('location',array('name'=>trim(strtolower($post_value)),'parent_id'=>$parent_id),'location_id');
        $third_level_type = 1;
    }

    $searchParams3 = array(
        'r_status'          => $asset_status,
        'third_level_type'  => $third_level_type,
        'third_level_id'    => $third_level_id,
        'tool_type'			=> @$tool_type,
        'country_id'        => $country_id,
        'first_x_cat'       => $first_x_cat,
        'task_access'       => $task_access
        );
    //echo '<pre>';print_r($searchParams3);exit;
    
    if(@$post_modality_id!='')
        $modality_loop = $CI->Common_model->get_data('modality',array('modality_id'=>$post_modality_id));
    else
        $modality_loop = $CI->Common_model->get_data('modality',array('status'=>1));


    $status_arr = $CI->Common_model->get_data('asset_status',array('type'=>$searchParams3['r_status']));
    $dynamic = '';
    
    if($asset_status == 1)
    {
        $at_warehouse = array(); $lock_in_period = array(); $field_deployed = array(); $in_calibration = array(); $in_transit = array(); $out_of_calibration = array();
    }
    else
    {
        $defective = array(); $missed = array(); $lost =array(); $out_of_tolerance = array();
        $in_repair = array(); $scaraped = array();
    }
    //$i =1;
    foreach ($modality_loop as $key => $row)
    {
        $status_data_arr = array();

        $status_data =getWHorZoneModalityDataWithStatus($searchParams3,$row['modality_id']);
         if($status_data > 0)
        { //echo 'popo';exit();
            foreach($status_data as  $value3)
            {
               // echo $i.'--';
                $status_data_arr[$value3['status']] = $value3['count'];
            }
        }  if($asset_status  == 1)
        {
            $at_warehouse[]       = ((int)@$status_data_arr[1]);
            $lock_in_period[]     = ((int)@$status_data_arr[2]);
            $in_transit[]         = ((int)@$status_data_arr[8]);
            $field_deployed[]     = ((int)@$status_data_arr[3]);
            $in_calibration[]     = ((int)@$status_data_arr[4]);
            $out_of_calibration[] = ((int)@$status_data_arr[12]);
        }
        else
        {
            $defective[]          = ((int)@$status_data_arr[5]);
            $missed[]             = ((int)@$status_data_arr[6]);
            $lost[]               = ((int)@$status_data_arr[7]);
            $out_of_tolerance[]   = ((int)@$status_data_arr[9]);
            $in_repair[]          = ((int)@$status_data_arr[10]);
            $scaraped[]           = ((int)@$status_data_arr[11]);
        }
        $xAxisCategory[] = $row['name'];
    } //exit;
    $xAxisLable = 'Modality';
    $series = array();
	if($asset_status == 1)
	{
		$series []= array('name'=>'At Warehouse','data'=>$at_warehouse);
		$series []= array('name'=>'Lockin Period','data'=>$lock_in_period);
		$series []= array('name'=>'Field Deployed','data'=>$field_deployed);
		$series []= array('name'=>'In Calibration','data'=>$in_calibration);
		$series []= array('name'=>'In Transit','data'=>$in_transit);		
		$series []= array('name'=>'Out Of Calibration','data'=>$out_of_calibration);
	}
	else
	{
		$series []= array('name'=>'defective','data'=>$defective);
		$series []= array('name'=>'missed','data'=>$missed);
		$series []= array('name'=>'Lost','data'=>$lost);
		$series []= array('name'=>'Out Of Tolerence','data'=>$out_of_tolerance);
		$series []= array('name'=>'In Repair','data'=>$in_repair);
		$series []= array('name'=>'Scraped','data'=>$scaraped);
	}

	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable);
	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

function getFeToolsFirstPieData($searchParams)
{
	$CI        = &get_instance();	
	$zone      = $searchParams['zone'];
	$user_id   = $searchParams['user_id'];
	$piedata= array();
	$fe_owned_arr =array(6,3,4,12);
	if($zone!=1)
	{
		$users = getUsersByZone($zone);
	}
	else
	{
		$users = get_fe_users($searchParams['task_access'],'','',$searchParams['country_id']);
	}
	if($users)
	{
		$fe_sso_arr = array();
		foreach ($users as $key => $value)
		{
			$fe_sso_arr[]= $value['sso_id']	;
		}
	}
	else
	{
		$fe_sso_arr = array(0);
	}
	
		$CI->db->select('a.modality_id,count(*) as count');
		$CI->db->from('asset a');
		$CI->db->join('asset_position ap','ap.asset_id = a.asset_id');
		$CI->db->where('ap.to_date',NULL);
		$CI->db->where('ap.transit',0);
		$CI->db->where_in('ap.sso_id',$fe_sso_arr);
		if($user_id!='')
		{
			$CI->db->where('ap.sso_id',$user_id);
		}
		$CI->db->where_in('a.status',$fe_owned_arr);
		$CI->db->group_by('a.modality_id');
		$res = $CI->db->get();
		/*echo $CI->db->last_query();
		echo '<pre>';print_r($res->result_array());exit;*/
		if($res->num_rows()>0)
			$result = $res->result_array();
		else
			$result = FALSE;
		//echo '<pre>';print_r($result);exit;
		if($result)
		{
			$d_arr = array();
			foreach ($result as $key => $value) 
			{
				$d_arr[$value['modality_id']] = $value['count'];
			}
		}
		$sampleResultSet = array();
		$modalities = $CI->Common_model->get_data('modality',array('status'=>1));
		foreach ($modalities as $key => $value2)
		{
			$sampleResultSet[] = array('modality'=>$value2['name'],'tool_count'=>(int)@$d_arr[$value2['modality_id']]);
		}
		foreach($sampleResultSet as $row){
		   $resData = array('name' => $row['modality'], 'y'=>(int)$row['tool_count']);
		   $piedata[] = $resData;
		}
		$firstPieData = array();
		$firstPieData []= array('name'=>'Tool Count','colorByPoint'=>true,'data'=>$piedata);
		return json_encode($firstPieData);

}

function getUsersByRoles($roles = array())
{
	$ci = & get_instance();
	// Get Field engineers, Zonal, National users
	$ci->db->from('user');
	$ci->db->where_in('role_id',$roles);
	$res = $ci->db->get();
	if($res->num_rows()>0)
	{
		return $res->result_array();
	}
	
}

function getFeToolsData2($searchParams)
{
	$CI            = &get_instance();
	$zone          = $searchParams['zone'];
	$user_id       = $searchParams['user_id'];
	$modality_name = $searchParams['modality'];
	$modality_id = $CI->Common_model->get_value('modality',array('name'=>trim(strtolower($modality_name))),'modality_id');	
	$piedata= array();
	$fe_owned_status = array(6,3,4,12);
	if($zone!=1)
	{
		$users = getUsersByZone($zone);
	}
	else
	{
		$users = get_fe_users($searchParams['task_access'],'','',$searchParams['country_id']);
	}
	if($users)
	{
		$fe_sso_arr = array();
		foreach ($users as $key => $value)
		{
			$fe_sso_arr[]= $value['sso_id']	;
		}
	}
	else
	{
		$fe_sso_arr = array(0);
	}
		$CI->db->select('a.modality_id,a.status,count(*) as count');
		$CI->db->from('asset a');
		$CI->db->join('asset_position ap','ap.asset_id = a.asset_id');
		$CI->db->where('ap.to_date',NULL);
		$CI->db->where('ap.transit',0);
		$CI->db->where_in('ap.sso_id',$fe_sso_arr);
		if($user_id!='')
		{
			$CI->db->where('ap.sso_id',$user_id);
		}
		$CI->db->where_in('a.status',$fe_owned_status);
		$CI->db->where('a.modality_id',$modality_id);
		$CI->db->group_by('a.status');
		$res = $CI->db->get();
		/*echo $CI->db->last_query();
		echo '<pre>';print_r($res->result_array());exit;*/
		if($res->num_rows()>0)
		{
			$result = $res->result_array();
			$final_fe_owned_arr = array();
			foreach ($result as $key => $rec) {
				$final_fe_owned_arr[$rec['status']] = $rec['count'];
			}
		}
		else
		{
			$result = FALSE;
		}
	
	$c1Data1 = array(
					 array('y'=>(int)@$final_fe_owned_arr[3],'color'=>'#3F51B5'),
					 array('y'=>(int)@$final_fe_owned_arr[4],'color'=>'#FF9800'),
					 array('y'=>(int)@$final_fe_owned_arr[12],'color'=>'#F44336')
				);
	$xAxisCategory = array("FE Owned","Calibration returns","Out Of Calibration");
	$xAxisLable = 'Stage';

	
	

	$series = array();
	$series []= array('name'=>$modality_name,'data'=>$c1Data1);
	

	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable);
	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}
	
#srilekha 30-05-2018
function get_in_calibration_assets_FirstPieData($searchParams,$task_access)
{
	$CI = &get_instance();	
	$piedata= array();
	$CI->db->select(' count(DISTINCT(a.asset_id)) as count,a.status,l.name as country');
	$CI->db->from('asset a');
	$CI->db->join('part p','p.asset_id=a.asset_id');
	$CI->db->join('tool t','t.tool_id=p.tool_id');
	$CI->db->join('location l','l.location_id=a.country_id');
	$CI->db->join('modality m','m.modality_id=a.modality_id');
	if($searchParams['modality_id']!='')
	{
		$CI->db->where('a.modality_id',$searchParams['modality_id']);
	}
	elseif($searchParams['tool_id']!='')
	{
		$CI->db->where('p.tool_id',$searchParams['tool_id']);
	}

	if($task_access == 1 || $task_access == 2)
	{
		$CI->db->where('a.country_id',$CI->session->userdata('s_country_id'));
	}
	else if($task_access == 3)
	{
		if($CI->session->userdata('header_country_id')!='')
		{
			$CI->db->where('a.country_id',$CI->session->userdata('header_country_id'));	
		}
		else
		{
			if($searchParams['country_id']!='')
			{
				$CI->db->where('a.country_id',$searchParams['country_id']);
			}
			else
			{
				$CI->db->where_in('a.country_id',$CI->session->userdata('countriesIndexedArray'));	
			}
		}
	}
	$CI->db->where('t.cal_type_id',1);
	$CI->db->group_by('a.country_id,a.status');
	$res = $CI->db->get();
	if($res->num_rows()>0)
		$result = $res->result_array();
	else
		$result = FALSE;
	$in_calibration=0;
	$out_of_calibration=0;
	$country_name='';
	$in_cal=array();
	$out_cal=array();
	if($result)
	{
		foreach($result as $row)
		{
			
			if($row['status']!=4 && $row['status']!=12) 
			{
				if(isset($in_cal[$row['country']]))
				{   

					$in_calibration = $in_cal[$row['country']]+$row['count'];
					$in_cal[$row['country']] = $in_calibration;
				}
				else
				{   
					$in_cal[$row['country']] = $row['count'];
					$in_calibration = $row['count'];

				}
			}
			if($row['status']==12)
			{
				if(isset($out_cal[$row['country']]))
				{
					$out_of_calibration = $out_cal[$row['country']]+$row['count'];
					$out_cal[$row['country']] = $out_of_calibration;
				}
				else
				{
					$out_cal[$row['country']] = $row['count'];
					$out_of_calibration = $row['count'];
				}
			}

			$country_name =@$row['country'];
		}
		
	}
	$firstPieData = array();
	$firstPieData []= array('name'=>'Calibrated','y'=>(int)$in_calibration,'country'=>$country_name);
    $firstPieData []=array('name'=>'Out of Calibration','y'=>(int)$out_of_calibration,'country'=>$country_name);
    $res=array();
    $res[]=array('name'=>'Asset Count','data'=>$firstPieData,'flag'=>2);
	return json_encode($res);
}

#srilekha 30-05-2018
function get_in_calibration_assets_FirstPieData_countries($searchParams,$task_access)
{
	$CI = &get_instance();	
	$piedata= array();
	$c1Data1 = array();
	$c2Data2 = array();
	$c3Data3 = array(); 
	$xAxisCategory = array();
	$CI->db->select(' count(DISTINCT(a.asset_id)) as count,a.status,l.name as country');
	$CI->db->from('asset a');
	$CI->db->join('part p','p.asset_id=a.asset_id');
	$CI->db->join('tool t','t.tool_id=p.tool_id');
	$CI->db->join('location l','l.location_id=a.country_id');
	$CI->db->join('modality m','m.modality_id=a.modality_id');
	if($searchParams['modality_id']!='')
	{
		$CI->db->where('a.modality_id',$searchParams['modality_id']);
	}
	elseif($searchParams['tool_id']!='')
	{
		$CI->db->where('p.tool_id',$searchParams['tool_id']);
	}
	if($task_access == 1 || $task_access == 2)
	{
		$CI->db->where('a.country_id',$CI->session->userdata('s_country_id'));
	}
	else if($task_access == 3)
	{
		if($CI->session->userdata('header_country_id')!='')
		{
			$CI->db->where('a.country_id',$CI->session->userdata('header_country_id'));	
		}
		else
		{
			if($searchParams['country_id']!='')
			{
				$CI->db->where('a.country_id',$searchParams['country_id']);
			}
			else
			{
				$CI->db->where_in('a.country_id',$CI->session->userdata('countriesIndexedArray'));	
			}
		}
	}
	$CI->db->where('t.cal_type_id',1);
	$CI->db->group_by('a.country_id,a.status');
	$res = $CI->db->get();
	if($res->num_rows()>0)
		$result = $res->result_array();
	else
		$result = FALSE;
	$in_calibration = 0;
	$out_of_calibration = 0;
	$in_cal = array();
	$out_cal = array();
	if($result)
	{
		foreach(@$result as $row)
		{
			if($row['status']!=4 && $row['status']!=12) 
			{
				if(isset($in_cal[$row['country']]))
				{   

					$in_calibration = $in_cal[$row['country']]+$row['count'];
					$in_cal[$row['country']] = $in_calibration;
				}
				else
				{   
					$in_cal[$row['country']] = $row['count'];
					$in_calibration = $row['count'];

				}
			}
			if($row['status']==12)
			{
				if(isset($out_cal[$row['country']]))
				{
					$out_of_calibration = $out_cal[$row['country']]+$row['count'];
					$out_cal[$row['country']] = $out_of_calibration;
				}
				else
				{
					$out_cal[$row['country']] = $row['count'];
					$out_of_calibration = $row['count'];
				}
			}
			
			$xAxisCategory[] =@$row['country'];
		}
	}
	$c1Data1=$c2Data2=$xAxisCat=array();
	$x_cat = array_unique($xAxisCategory);
	foreach ($x_cat as $row) {
	 	$c1Data1[]=(int)@$in_cal[$row];
	 	$c2Data2[]=(int)@$out_cal[$row];
	 	$xAxisCat[] =$row;
	}
	$xAxisLable = 'Countries';
	$text = "Calibration Assets" ;
	if($searchParams['country_id']!='')
    {
    	$country_name = $CI->Common_model->get_value('location',array('location_id'=>$searchParams['country_id']),'name');
    	$text .=" For ".$country_name;
    }
    else
    {
    	$text .=" For All Countries";
    }

	$series = array();
	$series []= array('name'=>'Calibrated','data'=>$c1Data1);
	$series []= array('name'=>'Out of Calibration','data'=>$c2Data2);
	$chart1Data = array('xAxisCategory'=>$xAxisCat,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'flag'=>1,'text'=>$text);

	
	return json_encode($chart1Data);
}

#srilekha 30-05-2018
function get_in_calibration_Chart2Data($searchParams)
{
		$CI = &get_instance();
		$sampleResultSet = array();
		$current_stage = $CI->Common_model->get_data('current_stage',array('workflow_type'=>3));
		$c1Data1=array();
		$result=array();
		if($searchParams['name'] == 'Calibrated')
		{
			$ResultSet = $CI->Report_m->get_all_asset_status();

			foreach($ResultSet as $key => $value)
			{
				$initial_wh_count[$value['name']]=$CI->Report_m->get_all_assets_in_wh($value['asset_status_id'],$searchParams);
			}
			
			foreach($initial_wh_count as $key1=>$row)
			{
				$sampleResultSet[]=$key1;
				$result[$key1]=$row['count'];
			}

		}
		elseif($searchParams['name']== 'Out of Calibration')
		{
			$sampleResultSet = array('At WH','In Transit','Field Deployed');
			$wh_count=$CI->Report_m->get_at_wh_count($searchParams);

			//echo $CI->db->last_query();exit;
			//print_r($wh_count); exit;
			$in_transit_count=$CI->Report_m->get_in_transit_count($searchParams);
			$field_deployed_count=$CI->Report_m->get_field_deployed_count($searchParams);
			if(count($wh_count)>0)
			{
				$wh_count_sum=array_sum(array_column($wh_count, 'count'));
			}
			else
			{
				$wh_count_sum='';
			}

			if(count($in_transit_count)>0)
			{
				$in_transit_count_sum=array_sum(array_column($in_transit_count, 'count'));
			}
			else
			{
				$in_transit_count_sum='';
			}
			if(count($field_deployed_count)>0)
			{
				$field_deployed_count_sum=array_sum(array_column($field_deployed_count, 'count'));
			}
			else
			{
				$field_deployed_count_sum='';
			}
			$result=array($sampleResultSet[0]=>$wh_count_sum,$sampleResultSet[1]=>$in_transit_count_sum,$sampleResultSet[2]=>$field_deployed_count_sum);
			
		}
		if(count($result)>0)
		{
			foreach($result as $row)
			{
				//echo $row['count']; exit;
				$c1Data1[]=array('y'=>(int)@$row,'color'=>'#3F51B5');
				//echo $row;
			}
		}
		$xAxisCategory=$sampleResultSet;
			
		$series = array();
		$series []= array('name'=>'Current Stages','showInLegend'=>'false','data'=>$c1Data1,'country_id'=>$searchParams['country_id']);
		$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series);
		$chart1Data = json_encode($chart1Data);
		return $chart1Data;
}
/* file end: ./application/helpers/dashboard_helper.php */

#koushik
function egm_getCalibrationDueData($searchParams)
{
	$ci = & get_instance();
	$zone = $searchParams['zone'];
	$modality_id = $searchParams['modality_id'];
	$wh_id = $searchParams['wh_id'];
	$tool_type_id = $searchParams['tool_type_id'];

	$m_arr = get_all_modality($modality_id);
	$t_arr = get_all_tool_type($tool_type_id);
	$m_list = array(); 
	$c1Data1 = array();
	$t_list = array(); 
	$c2Data2 = array(); $xAxisCategory = array();

	foreach ($m_arr as $mod) 
	{
		$m_list[] = $mod['modality_id'];
	}
	foreach ($t_arr as $tool_type) 
	{
		$t_list[] = $tool_type['tool_type_id'];
	}
	
	if($zone == 1 && $wh_id == '')
	{ 
		$zone_list = $ci->Report_m->get_zones_based_on_country($searchParams);
		foreach ($zone_list as $zon) 
		{
			if($wh_id == '')
			{
				$wh_list = get_wh_by_zone($zon['location_id'],$searchParams['task_access']);
			}
			else
			{ 
				$wh_list = array($wh_id); 
			}
			$cal_date = egm_get_cal_by_due_date($m_list,$wh_list,$t_list);
			$c1Data1[] = $cal_date[0];
			$c2Data2[] = $cal_date[1];
			$xAxisCategory[] = $zon['name'];
		}
		$xAxisLable = 'Zone';
	}
	else
	{
		if($wh_id == '')
		{ 
			$wh_list = get_wh_by_zone($zone,$searchParams['task_access']); 
		}
		else 
		{ 
			$wh_list = array($wh_id); 
		}
		foreach ($wh_list as $wh) 
		{
			$wh_arr = array($wh);
			$cal_date = egm_get_cal_by_due_date($m_list,$wh_arr,$t_list);
			$c1Data1[] = $cal_date[0];
			$c2Data2[] = $cal_date[1];
			$wh_data = $ci->Common_model->get_data_row('warehouse',array('wh_id'=>$wh));
			$xAxisCategory[] = $wh_data['wh_code'].' -('.$wh_data['name'].')';
		}
		$xAxisLable = 'Warehouse';
	}
	$series = array();
	$series []= array('name'=>'45 Days','data'=>$c1Data1);
	$series []= array('name'=>'30 Days','data'=>$c2Data2);
	
   $text ="Calibration Queue";
   if($searchParams['category']!='')
   {
   	 $text .=" For ".$searchParams['category'];
   }
	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'flag'=>2,'first_x_cat'=>$searchParams['category'],'text'=>$text);
	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

#Prasad
function egm_getCalibrationDueDataForCountries($searchParams)
{
	$ci = & get_instance();
	$zone = $searchParams['zone'];
	$modality_id = $searchParams['modality_id'];
	$wh_id = $searchParams['wh_id'];
	$tool_type_id = $searchParams['tool_type_id'];

	$m_arr = get_all_modality($modality_id);
	$t_arr = get_all_tool_type($tool_type_id);
	$m_list = array();
	$c1Data1 = array();
	$t_list = array(); 
	$c2Data2 = array();
	$c3Data3 = array(); $xAxisCategory = array();

	foreach ($m_arr as $mod) 
	{
		$m_list[] = $mod['modality_id'];
	}
	foreach ($t_arr as $tool_type)
	{
		$t_list[] = $tool_type['tool_type_id'];
	}

	if($searchParams['country_id']!='')
	{
		$country_arr = array($searchParams['country_id']);
	}
	else
	{
		$country_arr = $_SESSION['countriesIndexedArray'];
	}

	foreach ($country_arr as $key => $value) 
	{
		$country_id = $value;
		$country_name = $ci->Common_model->get_value('location',array('location_id'=>$country_id),'name');
		$cal_date = egm_get_cal_by_due_date_for_countries($m_list,$t_list,$wh_id,$country_id);
		$c1Data1[] = $cal_date[0];
		$c2Data2[] = $cal_date[1];
		$xAxisCategory[] = $country_name;
	}

	if($searchParams['task_access']==3 && $wh_id == '')
	{
		$xAxisLable = 'Country';
	}
	else if($wh_id == '')
	{
		$xAxisLable = 'Zone';
	}
	else
	{
		$xAxisLable = 'Warehouse';
	}
    $text = "Calibration Queue";
    if($searchParams['country_id']!='')
    {
    	$country_name = $ci->Common_model->get_value('location',array('location_id'=>$searchParams['country_id']),'name');
    	$text .=" For ".$country_name;
    }
    else
    {
    	$text .=" For All Countries";
    }
	$series = array();
	$series []= array('name'=>'45 Days','data'=>$c1Data1);
	$series []= array('name'=>'30 Days','data'=>$c2Data2);
	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'flag'=>1,'text'=>$text);
	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

#koushik
function egm_get_cal_by_due_date($m_list,$wh_list,$t_list)
{
	$ci= & get_instance();
	$ci->db->select('DATEDIFF(a.cal_due_date,now())as days,a.asset_id');
	$ci->db->from('asset a');
	$ci->db->join('egm_calibration ec','ec.asset_id = a.asset_id');
	$ci->db->join('part p','p.asset_id = a.asset_id');
	$ci->db->join('tool t','t.tool_id = p.tool_id');
	$ci->db->where('p.part_level_id',1);
	$ci->db->where('ec.status',1);
	$ci->db->where('ec.cal_status_id <',3);
	$ci->db->where_in('a.modality_id',$m_list);
	$ci->db->where_in('a.wh_id',$wh_list);
	$ci->db->where_in('t.tool_type_id',$t_list);
	$res = $ci->db->get();
	$asset_count_2 = 0;
	$asset_count_3 = 0;
	foreach($res->result_array() as $row)
	{
		//365 to include 45 and beyond 45
		if($row['days']<=365) $asset_count_2++;
		if($row['days']<=30) $asset_count_3++;
	}
	$a2 = $asset_count_2-$asset_count_3;
	$a3 = $asset_count_3;
	return array($a2,$a3);
}

function egm_get_cal_by_due_date_for_countries($m_list,$t_list,$wh_id,$country_id)
{	
	$ci= & get_instance();
	$ci->db->select('DATEDIFF(a.cal_due_date,now())as days,a.asset_id');
	$ci->db->from('asset a');
	$ci->db->join('egm_calibration ec','ec.asset_id = a.asset_id');
	$ci->db->join('part p','p.asset_id = a.asset_id');
	$ci->db->join('tool t','t.tool_id = p.tool_id');
	$ci->db->join('location l','a.country_id=l.location_id');
	$ci->db->join('warehouse w','a.wh_id=w.wh_id');
	if($wh_id != '')
	{
		$ci->db->Where('a.wh_id',$wh_id);
	}
	$ci->db->where('a.country_id',$country_id);
	$ci->db->where('p.part_level_id',1);
	$ci->db->where('ec.status',1);
	$ci->db->where('w.status',1);
	$ci->db->where('ec.cal_status_id <',3);
	$ci->db->where_in('a.modality_id',$m_list);
	$ci->db->where_in('t.tool_type_id',$t_list);
	$res = $ci->db->get();
	$asset_count_2 = 0;
	$asset_count_3 = 0;
	foreach($res->result_array() as $row)
	{
		//365 to include 45 and beyond 45
		if($row['days']<=365) $asset_count_2++;
		if($row['days']<=30) $asset_count_3++;
	}
	$a2 = $asset_count_2-$asset_count_3;
	$a3 = $asset_count_3;
	return array($a2,$a3);
}

#koushik
function egm_getCalStatusData($zone,$wh_id,$modality_id,$tool_type_id,$x_category,$first_x_cat,$task_access,$country_id)
{
	$ci = & get_instance();
	$zone = $zone;
	$modality_id = $modality_id;
	$wh_id = $wh_id;
	$tool_type_id = $tool_type_id;
	$category = $x_category;
	$stages = $ci->Common_model->get_data('egm_cal_status',array('status'=>1,'cal_status_id <'=>3));
	$tool_type = $ci->Common_model->get_data('tool_type',array('status'=>1));
	$m_arr = get_all_modality($modality_id);
	$t_arr = get_all_tool_type($tool_type_id);
	$c1Data1 = array(); 
	$c2Data2 = array();
	$c3Data3 = array();
	$tc1Data1 = array(); 
	$tc2Data2 = array();
	$tc3Data3 = array();
	$m_list = array();
	$t_list = array(); 
	$xAxisCategory = array();

	foreach ($m_arr as $mod) 
	{
		$m_list[] = $mod['modality_id'];
	}
	foreach ($t_arr as $tool) 
	{
		$t_list[] = $tool['tool_type_id'];
	}

	if($zone == 1 && $wh_id == '')
	{
		$location_id = $ci->Report_m->get_second_level_zone($category,$first_x_cat,$task_access);
		$wh_list = get_wh_by_zone($location_id);
		foreach ($stages as $key => $value) 
		{
			$s_data = egm_get_cal_status_count($wh_list,$value['cal_status_id'],$m_list,$t_list);
			$c1Data1[] = $s_data[0];
			$c2Data2[] = $s_data[1];
			$xAxisCategory[] = $value['name'];
		}
	}
	else
	{
		$var = explode(" ", $category);
		$location = $ci->Report_m->get_wh_by_wh_code($var,$country_id);
		if($wh_id !='') $wh_list = array($wh_id);
		else $wh_list = array($location);
		foreach ($stages as $key => $value) 
		{
			$s_data = egm_get_cal_status_count($wh_list,$value['cal_status_id'],$m_list,$t_list);
			$c1Data1[] = $s_data[0];
			$c2Data2[] = $s_data[1];
			$xAxisCategory[] = $value['name'];
		}
	}

	if($zone == 1 && $wh_id == '')
	{
		$location_id = $ci->Report_m->get_second_level_zone($category,$first_x_cat,$task_access);
		$wh_list = get_wh_by_zone($location_id);
		foreach ($tool_type as $key => $value) 
		{
			$ts_data = egm_get_cal_status_count_by_tool_type($wh_list,$value['tool_type_id'],$m_list);
			$tc1Data1[] = $ts_data[0];
			$tc2Data2[] = $ts_data[1];
			$txAxisCategory[] = $value['name'].' Tool';
		}
	}
	else
	{
		$var = explode(" ", $category);
		$location = $ci->Report_m->get_wh_by_wh_code($var,$country_id);
		if($wh_id !='') $wh_list = array($wh_id);
		else $wh_list = array($location);
		if($tool_type_id !='')
		{
			$tool_type = $ci->Common_model->get_data('tool_type',array('tool_type_id'=>
				$tool_type_id));
		}
		foreach ($tool_type as $key => $value) 
		{
			$ts_data = egm_get_cal_status_count_by_tool_type($wh_list,$value['tool_type_id'],$m_list);
			$tc1Data1[] = $ts_data[0];
			$tc2Data2[] = $ts_data[1];
			$txAxisCategory[] = $value['name'];
		}
	}

	$xAxisLable = 'Stages';
	$text = "Calibration Status".' For '.$category.'';
	$series = array();
	$series []= array('name'=>'45 Days','data'=>$c1Data1);
	$series []= array('name'=>'30 Days','data'=>$c2Data2);


	$txAxisLable = 'Tool Type';
	$ttext = "Tool Type".' For '.$category.'';
	if($zone!=1)
	{   
		$zone_name = $ci->Common_model->get_value('location',array('location_id'=>$zone),'name');
		$ttext .=" Of ".$zone_name;
		$text .=" Of ".$zone_name;
	}
	if($first_x_cat!='')
	{
		$ttext .=" In ".$first_x_cat;
		$text .=" In ".$first_x_cat;
	}

	$tseries = array();
	$tseries []= array('name'=>'45 Days','data'=>$tc1Data1);
	$tseries []= array('name'=>'30 Days','data'=>$tc2Data2);

	$chart1Data = array('xAxisCategory'=>$xAxisCategory,'chart1Series'=>$series,'xAxisLable'=>$xAxisLable,'text'=>$text,'txAxisCategory'=>$txAxisCategory,'tchart2Series'=>$tseries,'txAxisLable'=>$txAxisLable,'ttext'=>$ttext);

	$chart1Data = json_encode($chart1Data);
	return $chart1Data;
}

#koushik
function egm_get_cal_status_count($wh_list,$cal_status_id,$m_list,$t_list)
{
	$ci= & get_instance();
	$ci->db->select('DATEDIFF(a.cal_due_date,now())as days');
	$ci->db->from('asset a');
	$ci->db->join('egm_calibration ec','ec.asset_id = a.asset_id');
	$ci->db->join('part p','p.asset_id = a.asset_id');
	$ci->db->join('tool t','t.tool_id = p.tool_id');
	$ci->db->where('p.part_level_id',1);
	$ci->db->where('ec.status',1);
	$ci->db->where('ec.cal_status_id',$cal_status_id);
	$ci->db->where_in('a.wh_id',$wh_list);
	$ci->db->where_in('a.modality_id',$m_list);
	$ci->db->where_in('t.tool_type_id',$t_list);
	$res = $ci->db->get();
	$asset_count_2 = 0;
	$asset_count_3 = 0;
	foreach($res->result_array() as $row)
	{
		//365 to include 45 and beyond 45
		if($row['days']<=365) $asset_count_2++;
		if($row['days']<=30) $asset_count_3++;
	}
	$a2 = $asset_count_2-$asset_count_3;
	$a3 = $asset_count_3;
	return array($a2,$a3);
}

function egm_get_cal_status_count_by_tool_type($wh_list,$tool_type_id,$m_list)
{
	$ci= & get_instance();
	$ci->db->select('DATEDIFF(a.cal_due_date,now())as days');
	$ci->db->from('asset a');
	$ci->db->join('egm_calibration ec','ec.asset_id = a.asset_id');
	$ci->db->join('part p','p.asset_id = a.asset_id');
	$ci->db->join('tool t','t.tool_id = p.tool_id');
	$ci->db->where('p.part_level_id',1);
	$ci->db->where('ec.status',1);
	$ci->db->where('ec.cal_status_id <',3);
	$ci->db->where_in('a.wh_id',$wh_list);
	$ci->db->where_in('a.modality_id',$m_list);
	$ci->db->where('t.tool_type_id',$tool_type_id);
	$res = $ci->db->get();
	$asset_count_2 = 0;
	$asset_count_3 = 0;
	foreach($res->result_array() as $row)
	{
		//365 to include 45 and beyond 45
		if($row['days']<=365) $asset_count_2++;
		if($row['days']<=30) $asset_count_3++;
	}
	$a2 = $asset_count_2-$asset_count_3;
	$a3 = $asset_count_3;
	return array($a2,$a3);
}