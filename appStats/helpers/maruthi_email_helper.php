<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Ordered Tool Information Table
 * created by Maruthi on 11th August 2017, 11:25AM
*/

function orderedToolInfoTable($tool_order_id)
{
	$CI = & get_instance();
	$CI->db->select('t.part_number,t.part_description,ot.quantity as ordered_qty,ot.available_quantity as available_qty');
	$CI->db->from('ordered_tool ot');
	$CI->db->join('tool t','ot.tool_id = t.tool_id');
	$CI->db->where('ot.tool_order_id',$tool_order_id);
	$CI->db->where('ot.status<=',2);
	$query = $CI->db->get();
	$message1 = '';
	$message1.= '<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center"> Ordered Quantity</th>
								<th style="border: 1px solid black;" align="center"> Available Quantity in WH</th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($query->result_array() as $row1) {
		$message1.= '<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['ordered_qty'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['available_qty'].'</td>
					</tr>';
	}
	$message1.='</tbody></table>';
	if(count($query->result_array())==0)
	{
		$message1 = '';
	}
	return $message1;	
}

/*
 * send Address Change Notification To Admins
 * created by Maruthi on 11th August 2017, 11:25AM
*/
function sendAddressChangeNotification($tool_order_id) 
{
	$CI = & get_instance();	
	$site_id = $CI->Common_model->get_value('order_address',array('tool_order_id'=>$tool_order_id),'site_id');
	$order_data =$CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
	$original_address = $CI->Common_model->get_data_row('customer_site',array('site_id'=>$site_id));
	$changeble_address = $CI->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
	$country_id = $order_data['country_id'];

	#get assigned users for trigger
	$task_name = 'address_change';
	$task_access = array(2,3);
	$admins = get_assigned_users_by_task($task_name,$task_access,$country_id);
	$admin_array=array();
    foreach ($admins as $key => $value) 
    {
    	$admin_array[]=$value['email'];
    }
    $to_address = array_unique($admin_array);

    $message_info = orderedToolInfoTable($tool_order_id);
    if(count($original_address)>0 && count($changeble_address)>0 && $tool_order_id!='' && $message_info!='' && count($to_address) >0 )
    {
    	$subject = subject_test() .'Alert: Change Of Customer Address'.$order_data['order_number'] ;
		$message = '';
		$message = '<p> Hi All,</p>';
		$message.= '<p>Change of Customer Address For Order Number:'.$order_data['order_number'].' , Requested By '.getNameBySSO($order_data['sso_id']).'.
						<a href="'.SITE_URL.'address_change">click here to take action</a>
					  </p>';
		$message.= '<div class="row"
						<div class="col-md-6">
							<h2> From </h2>
								'.$original_address['address1'].'<br>
								'.$original_address['address2'].'<br>
								'.$original_address['address3'].'<br>
								'.$original_address['address4'].'<br>
								'.$original_address['zip_code'].'<br>
						</div>
						<div class="col-md-6">
							<h2> To </h2>							
								'.$changeble_address['address1'].'<br>
								'.$changeble_address['address2'].'<br>
								'.$changeble_address['address3'].'<br>
								'.$changeble_address['address4'].'<br>
								'.$changeble_address['pin_code'].'<br>
						</div>
					</div>
					';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		// sending email
		if(count($to_address)>0 && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
    }
}

function sendToolsNotAvailableNotification($tool_order_id,$success=0,$logistic=0) 
{
	$CI = & get_instance();	
	$order_data =$CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
	$order_address = $CI->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
	// send email reminder
	if($logistic ==0)
	{
		$task_name = "open_order";
		$task_access = array(2,3);
		$admins = get_assigned_users_by_task($task_name,$task_access,$order_data['country_id']);
	}

	else
	{
		$admins = get_logistic_users($logistic);
		if(getTaskPreference("send_email_tc_as_logistics",$order_data['country_id'])){
			$tc = get_email_tools_coordinators($order_data['country_id']);
			if(count($tc)>0){
				$admins = array_merge($admins,$tc);
			}
		}	
		
	}
	$admin_array=array();
	if(count($admins)>0){
		foreach ($admins as $key => $value) 
		{
			$admin_array[]=$value['email'];
		}
	}
	$to_address = array_unique($admin_array);
	$message_info = orderedToolInfoTable($tool_order_id);
	if(count($order_address)>0 && count($order_data)>0 && $tool_order_id!='' && $message_info!='' && count($to_address) > 0)
	{
		$message = '';
		if($success ==0)
		{
			$subject = subject_test() .'Alert: Tools Not Available in Assigned Warehouse for '.$order_data['order_number'];
		}
		else
		{
			$subject = subject_test() .'Alert: Tool Order Request '.$order_data['order_number'];
		}
		$message = '<p>Hi All,</p>';
		
		if($success == 0)
		{
			$message.= '<p>Tools Not Available For Order Number:'.$order_data['order_number'].' , Requested By '.getNameBySSO($order_data['sso_id']).' - '.getLocationBySSO($order_data['sso_id']).'.
					<a href="'.SITE_URL.'raiseSTforOrder">click here to take action</a></p>';
		}
		else
		{
			if($logistic == 0)
			{
				$message.= '<p>Tool order placed by '.getNameBySSO($order_data['sso_id']).' - '.getLocationBySSO($order_data['sso_id']).' with reference to Order Number:'.$order_data['order_number'].'.
					<a href="'.SITE_URL.'open_order">click here to take action</a></p>';
			}
			else
			{
				$message.= '<p>Tool order placed by '.getNameBySSO($order_data['sso_id']).' - '.getLocationBySSO($order_data['sso_id']).' with reference to Order Number:'.$order_data['order_number'].'.
					<a href="'.SITE_URL.'fe_delivery_list">click here to take action</a></p>';	
			}
		}
		$message.= '<div class="row"
						<div class="col-md-6">
							<h2> Ship To Address </h2>
								'.$order_address['address1'].',
								'.$order_address['address2'].',
								'.$order_address['address3'].',
								'.$order_address['address4'].',
								'.$order_address['pin_code'].',
						</div>
					</div><br>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		//echo $message;exit;
		#sending email
		if($to_address!='' && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}

function orderedAssets($order_status_id)
{
	$CI = &get_instance();
    $CI->load->model('Order_m');
    $results = $CI->Order_m->getAssetDataByOrderStatusID($order_status_id);
	$messageOA = '';
	$messageOA.='<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>
								<th style="border: 1px solid black;" align="center">Asset Number</th>
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center">Tool Level</th>
								<th style="border: 1px solid black;" align="center">Tool Type</th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($results as $row1) 
	{
		$messageOA.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_level'].'</td>						
						<td style="border: 1px solid black;" align="center">'.$row1['tool_type'].'</td>	
					</tr>';
	}
	$messageOA.='</tbody></table>';
	if(count($results)==0)
	{
		$messageOA = '';
	}
	return $messageOA;	
}

function defective_asset_info($defective_asset_id)
{
	$CI = &get_instance();
    $CI->load->model('Alerts_m');
    $results = $CI->Alerts_m->defectiveOrMissedAssetInfo($defective_asset_id);
	$messageAI = '';
	$messageAI.='<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>								
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center">Serial Number</th>
								<th style="border: 1px solid black;" align="center">Tool Level</th>
								<th style="border: 1px solid black;" align="center">Tool Type</th>
								<th style="border: 1px solid black;" align="center">Quantity </th>
								<th style="border: 1px solid black;" align="center">Tool Health</th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($results as $row1) 
	{
		$messageAI.= '<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>						
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['serial_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_level'].'</td>						
						<td style="border: 1px solid black;" align="center">'.$row1['tool_type'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['qty'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['condition'].'</td>	
					</tr>';
	}
	$messageAI.='</tbody></table>';
	if(count($results)==0)
	{
		$messageAI = '';
	}
	return $messageAI;	
}


function sendPickupMailToFE2($from_address,$to_address,$assets_list)
{
	$CI = & get_instance();	
	$message_info = shippedAssets($assets_list);
	if($message_info!='' && count($to_address)>0 && $from_address!='')
	{
		$to = $to_address['email'];
		$message = '';
		$subject = subject_test() .'Alert: Shipment details for your Order Number '.$to_address['order_number'];
		$message = '<p> Hi '.$to_address['sso_name'].',</p>';
		$message.= '<p>Your Order Number '.$to_address['order_number'].' has been successfully shipped FROM '.$from_address['address1'].','.$from_address['address2'].','.$from_address['address3'].','.$from_address['address4'].','.$from_address['pin_code'].' TO '.$to_address['address1'].','.$to_address['address2'].','.$to_address['address3'].','.$to_address['address4'].','.$to_address['pin_code'].' with below tool details and will reach you shortly. <a href="'.SITE_URL.'fe2_fe_receive">click here to take action</a></p>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if($to!='' && $subject!='' && $message!='')
		{	
			send_email($to,$subject,$message);
		}
	}
}

function shippedAssets($assets_info)
{
	$CI = &get_instance();    
	$messageAI = '';
	$messageAI.= '<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>								
								<th style="border: 1px solid black;" align="center">Asset Number</th>
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($assets_info as $row1) 
	{
		$messageAI.= '<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>						
						<td style="border: 1px solid black;" align="center">'.$row1['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>						
					</tr>';
	}
	$messageAI.='</tbody></table>';
	if(count($assets_info)==0)
	{
		$messageAI = '';
	}
	return $messageAI;	
}

function sendFeDeliveryMail($address_info,$assets_info)
{
	$CI = & get_instance();	
	$to = $address_info['email'];
	$drow = $CI->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$address_info['tool_order_id']));
	$courier_info ='';
	if($drow['courier_type']==1)
	{
		$courier_info = $drow['courier_name'].','.$drow['courier_number'];
	}
	elseif ($drow['courier_type']==2)
	{
		$courier_info = $drow['contact_person'].','.$drow['phone_number'];
	}
	else
	{
		$courier_info = $drow['vehicle_number'].','.$drow['phone_number'];
	}

	$message_info = shippedAssets($assets_info);
	if(count($address_info)>0 && $message_info!='' && count($drow)>0)
	{
		$message = '';
		$subject = subject_test() .'Alert: Shipment details for your Order Number '.$address_info['order_number'];
		$message = '<p> Hi '.$address_info['sso_name'].',</p>';
		$message.= '<p>Your Order Number '.$address_info['order_number'].' has been successfully shipped with below details '.$courier_info.' <br><br><strong>FROM</strong> <br> '.$address_info['wh_add1'].','.$address_info['wh_add2'].','.$address_info['wh_add3'].','.$address_info['wh_add4'].','.$address_info['wh_pin_code'].'<br><br> <b>TO</b><br> '.$address_info['address1'].','.$address_info['address2'].','.$address_info['address3'].','.$address_info['address4'].','.$address_info['pin_code'].' with below tool details and will reach you shortly. <a href="'.SITE_URL.'receive_order">click here to view the dispatch details</a></p>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		
		#sending email
		if($to!='' && $subject!='' && $message!='')
		{
			send_email($to,$subject,$message);
		}
	}
}

function sendFe2FeTransferNotificationToAdmin($return_number,$rto_id,$fe1_tool_order_id,$fe2_tool_order_id,$country_id=2)
{
	$CI = & get_instance();	
	$fe1_info = getOrderInfo($fe1_tool_order_id);
	$fe2_info = getOrderInfo($fe2_tool_order_id);
	$order_status_id = $CI->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id),'order_status_id');

	// send email reminder
	$task_name = "fe2_fe_receive";
	$task_access = array(2,3);
	$admins = get_assigned_users_by_task($task_name,$task_access,$country_id);
	$admin_array=array();
	foreach ($admins as $key => $value) 
	{
		$admin_array[]=$value['email'];
	}
	$to_address = array_unique($admin_array);
	$message_info = orderedAssets($order_status_id);
	if(count($fe1_info)>0 && count($fe2_info)>0 && $order_status_id!='' && $message_info!='' && count($to_address)>0)
	{
		$message = '';
		$subject = subject_test() .'Alert: FE To FE Transfer Request ';
		$message = '<p> Hi All,</p>';
		$message.= '<p>SSO '.$fe1_info['sso'].' Having the Order Number '.$fe1_info['order_number'].' Wants To Transfer Below Tools to SSO '.$fe2_info['sso'].' Having The Order Number '.$fe2_info['order_number'].'. <a href="'.SITE_URL.'fe2_fe_approval">click here to take action</a></p>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();

		#sending email
		if(count($to_address)>0 && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}

function sendDefectiveOrMissedAssetMail($defective_asset_id,$type,$country_id=2,$atUser,$transactionId) 
{
	$CI = & get_instance();	
	$CI->load->model('Alerts_m');
	$asset_info = $CI->Alerts_m->getDefectiveAssetInfo($defective_asset_id);
	// at user 1 is at warehouse while checking the asset condition 
	// 2 at FE Order Workflow
	$to_address=array();
	if($atUser == 1)
	{
		$by_str = getNameBySSO($transactionId);
		$asset_id = $asset_info['asset_id'];
		$wh_id = $CI->Common_model->get_value('asset',array('asset_id'=>$asset_id,'status'=>1),'wh_id');
		$logistics = get_logistic_users($wh_id);
		$logistic_array = array();
		if(count($logistic_array)>0){
			foreach ($logistics as $key => $value) {
				$logistic_array[] = $value['email'];
			}	
			$to_address = array_unique($logistic_array);	
		}
	}
	else if($atUser == 2)
	{
		$order_data = $CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$transactionId));
		$order_num = ($order_data['order_type'] == 2)?$order_data['order_number']:$order_data['stn_number'];
		$by_str = getNameBySSO($order_data['sso_id']).' for Order Number:'.@$order_num;
		$transactionId = $order_data['sso_id'];
		$respective_email =$CI->Common_model->get_value('user',array('sso_id'=>$transactionId,'status'=>1),'email');
		$to_address[] = $respective_email;
	}

	$country_id = $asset_info['country_id'];

	if(count($to_address)>0)
	{
		#get assigned users for trigger
		$task_name = 'defective_asset';
		$task_access = array(2,3);
		$admins = get_assigned_users_by_task($task_name,$task_access,$country_id);
		$admin_array =  array();
		foreach ($admins as $key => $value) 
		{
			$admin_array[]=$value['email'];
		}
		$to_address = array_unique($admin_array);
		$to_address = array_unique($to_address);
	}
	$message_info = defective_asset_info($defective_asset_id);
	if(count($asset_info)>0 && $defective_asset_id!='' && count($to_address)>0 && $message_info!='')
	{
		$message = '';
		if($type == 1)
		{	
			$mail_url ="defective_asset";	 
			$subject = subject_test() .'Alert: Asset Number '.$asset_info['asset_number'].' Detected as Defective '; $type_str = 'Defective';
		}
		else 
		{ 		
			$mail_url ="missed_asset";		
			$subject = subject_test() .'Alert: Asset Detected as Missed/Lost '; $type_str='Missed/Lost'; 
		}
		
		$message = '<p> Hi All,</p>';
		$message.= '<p> Below tools are identified as '.$type_str.' With Asset Number '.$asset_info['asset_number'].'  by '.$by_str.'.
					<a href="'.SITE_URL.$mail_url.'">click here to take action</a>
		 </p>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if(count($to_address)>0 && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}

#srilekha 20-06-2018
function send_faq_mail($sso_id,$faq)
{
	$ci=&get_instance();
	$fe_arr = $ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
	$fe_mail=$fe_arr['email'];
	$country_id = $fe_arr['country_id'];

	#get assigned users for trigger
	$task_name = 'faq';
	$task_access = array(2,3);
	$admins = get_assigned_users_by_task($task_name,$task_access,$country_id);
	$admin_array=array();
	foreach($admins as $row)
	{ 
		$admin_array[] = $row['email'];
	}
	$to_address = array_unique($admin_array);

	$faq_mess = validate_string($faq);
	if($faq_mess!='' && count($to_address)>0 && $fe_mail!='')
	{
		$message='<p> Hi All,</p>';
		$message.=$faq_mess;
		$message.='<p>Regards,</p>';
		$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		$subject='Information Needed';

		#send email
		if(count($to_address)>0 && $subject!='' && $message!='' && $fe_mail!='')
		{
			send_email($to_address,$subject,$message,$cc=NULL,$from=$fe_mail);
		}
	}
}

// ST mails
function sendSTMails($tool_order_id,$users,$type)  // 1 User // 2 Logistics // 3 FE  -- 1 for raised //2 for shipped
{
	$CI = & get_instance();	
	$order_data = dataForSTMails($tool_order_id);
	// send email reminder
	if($users ==1)
	{
		$task_name = "open_order";
		$task_access = array(2,3);
		$admins = get_assigned_users_by_task($task_name,$task_access,$order_data['country_id']);
		$url = 'openSTforOrder';
	}
	if($users == 2)
	{
		$admins = get_logistic_users($order_data['from_log_wh']);
		$url = 'wh_stock_transfer_list';
	}
	if($users ==3)
	{
        $admins[]['email'] = $order_data['email'];
        $url ='open_order';
	}

	if($type == 1)
		$type_str = "Raised";
	if($type == 2)
		$type_str = "Shipped";
	
	$admin_array=array();
	foreach ($admins as $key => $value) 
	{
		$admin_array[]=$value['email'];
	}
	$to_address = array_unique($admin_array);
	$message_info = orderedToolInfoTable($tool_order_id);
	if(count($order_data)>0 && $tool_order_id!='' && $message_info!='' && count($to_address)>0)
	{
		$message = '';
		
		$subject = subject_test() .'Alert: Stock Tranfer '.$order_data['stn_number'].' for Tool Order Request '.$order_data['order_number'];
		if($users == 3)
			$message = '<p>  Hi ' .$order_data['fe_name'].',</p>';
		else
			$message = '<p> Hi All,</p>';
		
		$message.= '<p>Stock Transfer Request '.$order_data['stn_number'].' Has been '.$type_str.' for Order Number '.$order_data['order_number'].'
					 Requested By '.$order_data['fe_info'].'
					<a href="'.SITE_URL.''.$url.' ">click here to take action</a></p>';
		
		$message.= '<div class="row"
						<div class="col-md-6">
							<h2> Ship To Address </h2>
								'.trim($order_data['address1']).',
								'.trim($order_data['address2']).',
								'.trim($order_data['address3']).',
								'.trim($order_data['address4']).',
								'.trim($order_data['pin_code']).'.
						</div>
					</div><br>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if(count($to_address)>0 && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}



function sendfetowhtoolreturn($return_number,$rto_id,$tool_order_id,$fe2_tool_order_id,$country_id=2,$to_wh_id,$from_wh_id)
{
	$CI = & get_instance();	
	$fe1_info = getOrderInfo($tool_order_id);
	$order_status_id = $CI->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id),'order_status_id');
	$to_wh_address = $CI->Common_model->get_data_row('warehouse',array('wh_id'=>$to_wh_id));
	$return_order_id = $CI->Common_model->get_value('return_tool_order',array('rto_id'=>$rto_id),'return_order_id');
	$from_wh_address = $CI->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));	
	$message_info = get_fe_to_wh_order_info($return_order_id);
	$admins = get_admins($country_id);
	$admin_array=array();
	foreach ($admins as $key => $value) 
	{
		$admin_array[]=$value['email'];
	}
	$tools_cordinators = get_email_tools_coordinators($country_id);
	if(count($tools_cordinators)>0)
	{
		foreach ($tools_cordinators as $key2 => $row) 
		{
			$admin_array[]=$row['email'];
		}
	}
	$to_address = array_unique($admin_array);

	if(count($fe1_info)>0  && count($to_address)>0 && $message_info!='')
	{
		$message = '';
		$subject = subject_test() .'Alert: Tool Return has been initiated by FE '.$fe1_info['sso'].'for Order '.$fe1_info['order_number'];
		$message = '<p> Hi All,</p>';
		$message = '<p>Please find the return Details Below.</p>';
		$message.= ' <h4> From  </h4>
							<p>'.$from_wh_address['address1'].','.$from_wh_address['address2'].','.$from_wh_address['address3'].','.$from_wh_address['address4'].','.$from_wh_address['zip_code'].'</p>.<br>';
		$message.= ' <h4> To  </h4>
							<p>'.$to_wh_address['address1'].','.$to_wh_address['address2'].','.$to_wh_address['address3'].','.$to_wh_address['address4'].','.$to_wh_address['pin_code'].'with below tool details. <a href="'.SITE_URL.'open_order">click here to take action</a></p>,';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if(count($to_address)>0 && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}

	$logistic_array=array();
	$logistics = get_asset_logistic($to_wh_id,$country_id);
	foreach ($logistics as $key1 => $wh_logistic) 
	{
		$logistic_array[]=$wh_logistic['email'];
	}
	$to_log_address = array_unique($logistic_array);
	$return_type = array(1,2);	
	$CI->db->select('to.order_number,ro.return_number,ro.created_time,
					   concat(ro.address3,", ",ro.address4) as location_name,ro.return_order_id,l1.name as country_name,
					   concat(wh.wh_code," -(",wh.name,")") as wh_name,
					   concat(u.sso_id," -(",u.name,")") as sso_name');
	$CI->db->from('return_order ro');
	$CI->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
	$CI->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
	$CI->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
	$CI->db->join('location l1','l1.location_id = to.country_id','LEFT');
	$CI->db->join('warehouse wh','wh.wh_id = ro.wh_id','LEFT');
	$CI->db->join('user u','u.sso_id = to.sso_id');
	$CI->db->where('osh.current_stage_id',8);//waiting for pickup
	$CI->db->where('osh.end_time IS NULL');
	$CI->db->where('to.order_type',2); //tool order = 2
	$CI->db->where('address_check!=',1);
	$CI->db->where_in('ro.return_type_id',$return_type);	
	$CI->db->where('rto.rto_id',$rto_id);
	$res = $CI->db->get();
	$pickup_check = $res->num_rows();
	if($pickup_check>0)
		$url = 'pickup_list';
	else
		$url ='open_fe_returns';	
	if(count($fe1_info)>0  && count($to_log_address)>0 && $message_info!='')
	{
		$message = '';
		$subject = subject_test().'Alert: Tool Return has been initiated by FE '.$fe1_info['sso'].'for Order '.$fe1_info['order_number'];
		$message = '<p> Hi All,</p>';
		$message.= ' <h4> From  </h4>
							<p>'.$from_wh_address['address1'].','.$from_wh_address['address2'].','.$from_wh_address['address3'].','.$from_wh_address['address4'].','.$from_wh_address['zip_code'].'</p>.<br>';
		$message.= ' <h4> To  </h4>
							<p>'.$to_wh_address['address1'].','.$to_wh_address['address2'].','.$to_wh_address['address3'].','.$to_wh_address['address4'].','.$to_wh_address['pin_code'].'with below tool details. <a href="'.SITE_URL.$url.'">click here to take action</a></p>,';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if(count($to_log_address)>0 && $subject!='' && $message!='')
		{
			send_email($to_log_address,$subject,$message);
		}
	}
   
	$to_fe_address[] = $fe1_info['email'];
	if(count($fe1_info)>0  && count($to_fe_address)>0 && $message_info!='')
	{
		$message = '';
		
		$subject = subject_test() .'Alert: Tool Return has been initiated by FE '.$fe1_info['sso'].'for Order '.$fe1_info['order_number'];
		$message = '<p> Hi All,</p>';
		$message.= ' <h4> From  </h4>
							<p>'.$from_wh_address['address1'].','.$from_wh_address['address2'].','.$from_wh_address['address3'].','.$from_wh_address['address4'].','.$from_wh_address['zip_code'].'</p>.<br>';
		$message.= ' <h4> To  </h4>
							<p>'.$to_wh_address['address1'].','.$to_wh_address['address2'].','.$to_wh_address['address3'].','.$to_wh_address['address4'].','.$to_wh_address['pin_code'].'with below tool details. <a href="'.SITE_URL.'receive_order">click here to take action</a></p>,';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if(count($to_fe_address)>0 && $subject!='' && $message!='')
		{
			send_email($to_fe_address,$subject,$message);
		}
	}
}
function get_fe_to_wh_order_info($return_order_id)
{	
	$CI = & get_instance();
	$CI->db->select('count(oah.oah_id) as return_qty,t.part_number,t.part_description,GROUP_CONCAT(a.asset_number SEPARATOR ",") as asset_number_list,t.tool_id,rto.tool_order_id');
	$CI->db->from('return_order ro');
	$CI->db->join('return_tool_order rto','rto.return_order_id=ro.return_order_id');
	$CI->db->join('order_status_history osh','osh.tool_order_id=rto.tool_order_id AND rto.rto_id = osh.rto_id');
	$CI->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');	
	$CI->db->join('ordered_asset oa','oa.ordered_asset_id=oah.ordered_asset_id');
	$CI->db->join('asset a','a.asset_id = oa.asset_id');
	$CI->db->join('part p','p.asset_id = a.asset_id');
	$CI->db->join('tool t','t.tool_id = p.tool_id');	
	$CI->db->where('p.part_level_id',1);
	$CI->db->where('ro.return_order_id',$return_order_id);
	$CI->db->group_by('t.tool_id');	
	$res = $CI->db->get();
	$results = $res->result_array();
	$messageOA = '';
	$messageOA.='<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>								
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center">Asset Number</th>
								<th style="border: 1px solid black;" align="center">Received Quantity</th>
								<th style="border: 1px solid black;" align="center">Return Quantity</th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($results as $row1) 
	{
		$messageOA.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>						
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['asset_number_list'].'</td>
						<td style="border: 1px solid black;" align="center">'.get_on_hand_received_qty($row1['tool_order_id'],$row1['tool_id'],1).'</td>						
						<td style="border: 1px solid black;" align="center">'.$row1['return_qty'].'</td>	
					</tr>';
	}
	$messageOA.='</tbody></table>';
	if(count($results)==0)
	{
		$messageOA = '';
	}
	return $messageOA;	
}

function sendtoolordernotificationtofe($tool_order_id,$flag) 
{
	$CI = & get_instance();	
	$order_data =$CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
	$order_address = $CI->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
	$sso_id = $order_data['sso_id'];
	if(getUserStatus($sso_id) ==1)
	{
		$fe_info = $CI->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
		$to_address = $fe_info['email'];
	}
	$message_info = orderedToolInfoTable($tool_order_id);
	if(count($order_address)>0 && count($order_data)>0 && $tool_order_id!='' && $message_info!='' && @$to_address!='')
	{
		$message = '';
		if($flag==1)
		{
			$subject = subject_test() .'Alert: New Tool Order Request '.$order_data['order_number'];
		}
		if($flag==2)
		{
			$subject = subject_test() .'Alert: Tool Order '.$order_data['order_number'].' Updated' ;
		}
		
		$message = '<p> Hi '.getNameBySSO($order_data['sso_id']).',</p>';
		if($flag==1)
		{
			$message.= '<p>Thanks for placing the Tool order with reference to Order Number:'.$order_data['order_number'].'. Please 
					<a href="'.SITE_URL.'open_order">click here to take action</a> For adding more tools or to cancel if order placed wrongly.</p>';
		}
		if($flag==2)
		{
			$message.= '<p> You Have Updated the Tool Order '.$order_data['order_number'].' as below: <a href="'.SITE_URL.'open_order">click here to take action</a> </p>';
		}
		
		$message.= '<div class="row"
						<div class="col-md-6">
							<h2> Ship To Address </h2>
								'.$order_address['address1'].',
								'.$order_address['address2'].',
								'.$order_address['address3'].',
								'.$order_address['address4'].',
								'.$order_address['pin_code'].',
						</div>
					</div><br>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();

		#sending email
		if($to_address!='' && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}

function sendtoolordernotificationtofeupdate($tool_order_id,$success=0,$logistic=0) 
{
	$CI = & get_instance();	
	$order_data =$CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
	$order_address = $CI->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
	// send email reminder
	if($logistic ==0)
	{
		$task_name = "open_order";
		$task_access = array(2,3);
		$admins = get_assigned_users_by_task($task_name,$task_access,$order_data['country_id']);
	}
	else
	{
		$admins = get_logistic_users($logistic);
		if(getTaskPreference("send_email_tc_as_logistics",$order_data['country_id'])){
			$tc = get_email_tools_coordinators($order_data['country_id']);
			if(count($tc)>0){
				$admins = array_merge($admins,$tc);
			}
		}
	}
	$admin_array=array();
	foreach ($admins as $key => $value) 
	{
		$admin_array[]=$value['email'];
	}
	$to_address = array_unique($admin_array);
	$message_info = orderedToolInfoTable($tool_order_id);
	if(count($order_address)>0 && count($order_data)>0 && $tool_order_id!='' && $message_info!='' && count($to_address) > 0)
	{
		$message = '';
		$subject = subject_test() .'Alert: Tool Order Updated '.$order_data['order_number'];
		
		$message = '<p> Hi All,</p>';
		if($logistic == 0)
		{
			$message.= '<p>Updated the Tool Order Number:'.$order_data['order_number'].'.
					<a href="'.SITE_URL.'open_order">click here to take action</a></p>';
		}
		else
		{
			$message.= '<p>Updated the Tool Order Number:'.$order_data['order_number'].'.
					<a href="'.SITE_URL.'fe_delivery_list">click here to take action</a></p>';
		}
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();

		#sending email
		if(count($to_address) > 0 && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}

function sendmailtofe1fortoolorder($tool_order_id,$fe1_order_number)
{
	$CI = & get_instance();	
	$order_data =$CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
	$order_address = $CI->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
	$sso_id = $order_data['sso_id'];
	$fe_info = $CI->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));

	$task_name = "open_order";
	$task_access = array(2,3);
	$admins = get_assigned_users_by_task($task_name,$task_access,$order_data['country_id']);
	$admin_array = array();
	foreach ($admins as $key => $value) 
	{
		$admin_array[]=$value['email'];
	}
	if(count($fe_info)>0)
		$admin_array[] = $fe_info['email'];
	$cc_address  = array_unique($admin_array);

	$fe1_order_info = $CI->Common_model->get_data_row('tool_order',array('order_number'=>$fe1_order_number,'country_id'=>$order_data['country_id']));
	$fe1_id = $fe1_order_info['sso_id'];
	$fe1_info = $CI->Common_model->get_data_row('user',array('sso_id'=>$fe1_id,'status'=>1));
	if(count($fe1_info)>0)
		$to_address = $fe1_info['email'];
	$message_info = fetofeordertoolinfo($tool_order_id);
	if(count($order_address)>0 && count($order_data)>0 && $tool_order_id!='' && $message_info!='' && @$to_address!='' && count($cc_address)>0)
	{
		$message = '';
		$subject = subject_test() .'Alert: FE to FE Tool Request Order Number '.$order_data['order_number'];		
		$message = '<p> Hi '.getNameBySSO($fe1_id).',</p>';
		$message.= '<p> SSO '.getNameBySSO($order_data['sso_id']).' Having the Order Number '.$order_data['order_number'].' asking for below tool/tools and would like to get transferred from your '.getNameBySSO($fe1_info['sso_id']).' Having the Order Number '.$fe1_order_info['order_number'].' </p>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if($to_address!='' && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message,$cc_address);
		}
	}
}

function fetofeordertoolinfo($tool_order_id)
{
	$CI = & get_instance();
	$CI->db->select('t.part_number,t.part_description,ot.quantity as ordered_qty,ot.available_quantity as available_qty,pl.name as part_level,tt.name as tool_type');
	$CI->db->from('ordered_tool ot');
	$CI->db->join('tool t','ot.tool_id = t.tool_id');
	$CI->db->join('part_level pl','pl.part_level_id = t.part_level_id');
	$CI->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
	$CI->db->where('ot.tool_order_id',$tool_order_id);
	$CI->db->where('ot.status<=',2);
	$query = $CI->db->get();
	$message1 = '';
	$message1.= '<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center"> Tool Level</th>
								<th style="border: 1px solid black;" align="center"> Tool Type</th>
								<th style="border: 1px solid black;" align="center"> Quantity</th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($query->result_array() as $row1) {
		$message1.= '<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_level'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['tool_type'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['ordered_qty'].'</td>
					</tr>';
	}
	$message1.='</tbody></table>';
	if(count($query->result_array())==0)
	{
		$message1 = '';
	}
	return $message1;	
}
function sendmailtoadminforfe2fetoolorder($tool_order_id,$fe1_order_number)
{
	$CI = & get_instance();	
	$order_data =$CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
	$order_address = $CI->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
	$sso_id = $order_data['sso_id'];
	$fe_info = $CI->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));

	$task_name = "open_order";
	$task_access = array(2,3);
	$admins = get_assigned_users_by_task($task_name,$task_access,$order_data['country_id']);
	
	
	$fe1_order_info = $CI->Common_model->get_data_row('tool_order',array('order_number'=>$fe1_order_number,'country_id'=>$order_data['country_id']));
	$fe1_id = $fe1_order_info['sso_id'];
	$fe1_info = $CI->Common_model->get_data_row('user',array('sso_id'=>$fe1_id,'status'=>1));

	$admin_array = array();
	foreach ($admins as $key => $value) {
		$admin_array[] = $value['email'];
	}
	$to_address = array_unique($admin_array);

	$message_info = fetofeordertoolinfo($tool_order_id);
	if(count($order_address)>0 && count($order_data)>0 && $tool_order_id!='' && $message_info!='' && count($fe1_info)>0)
	{
		$message = '';
		$subject = subject_test().'Need Approval for FE to FE Transfer Request';
		
		$message = '<p> Hi '.getNameBySSO($value['sso_id']).',</p>';
		$message.= '<p> SSO '.getNameBySSO($order_data['sso_id']).' Having the Order Number '.$order_data['order_number'].' asking for below tool/tools and would like to get transferred from '.getNameBySSO($fe1_info['sso_id']).' Having the Order Number '.$fe1_order_info['order_number'].' <a href="'.SITE_URL.'fe2_fe_approval">click here to take action</a> </p>';
		
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		
		#sending email
		if($subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}

function approvefe2fetoolrequestbyadmin($fe1_tool_order_id,$fe2_tool_order_id,$return_number)
{
	$CI = & get_instance();	
	#Fe1 Order Info
	$fe1_order_data =$CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe1_tool_order_id));
	$order_address = $CI->Common_model->get_data_row('order_address',array('tool_order_id'=>$fe1_tool_order_id));
	$fe1_sso_id = $fe1_order_data['sso_id'];
	$fe1_info = $CI->Common_model->get_data_row('user',array('sso_id'=>$fe1_sso_id,'status'=>1));

	$to_address=array();
	if(count($fe1_info)>0)
		$to_address[] = $fe1_info['email'];

	#Fe2 Order Info
	$fe2_order_info = $CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe2_tool_order_id));
	$fe2_sso_id = $fe2_order_info['sso_id'];
	$fe2_info = $CI->Common_model->get_data_row('user',array('sso_id'=>$fe2_sso_id,'status'=>1));
	if(count($fe2_info)>0)
		$to_address[] = $fe2_info['email'];

	
	$message_info = get_approved_tool_order_info($fe2_tool_order_id,$fe2_sso_id,$fe1_sso_id);
	if(count($order_address)>0 && count($fe2_order_info)>0 && $fe2_tool_order_id!='' && $message_info!='' && count($to_address)>0)
	{
		$message = '';
		$subject = subject_test() .'Alert: Approved: FE to FE Tool Request Order Number '.$fe1_order_data['order_number'].' With Return Number '. @$return_number;
		
		$message = '<p> Hi,</p>';
		$message.= '<p>Following Request has been Approved </p>';
		
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if($to_address!='' && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}

function get_approved_tool_order_info($fe2_tool_order_id,$fe2_sso_id,$fe1_sso_id)
{
	$CI = & get_instance();
	$CI->db->select('t.part_number,t.part_description,ot.quantity as ordered_qty,tt.name as tool_type');
	$CI->db->from('ordered_tool ot');
	$CI->db->join('tool t','ot.tool_id = t.tool_id');
	$CI->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
	$CI->db->where('ot.tool_order_id',$fe2_tool_order_id);
	$CI->db->where('ot.status<=',2);
	$query = $CI->db->get();
	$message1 = '';
	$message1.= '<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center"> Tool Type</th>
								<th style="border: 1px solid black;" align="center"> Requested By</th>
								<th style="border: 1px solid black;" align="center"> Requested From</th>
								<th style="border: 1px solid black;" align="center"> Quantity</th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($query->result_array() as $row1) {
		$message1.= '<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['tool_type'].'</td>
						<td style="border: 1px solid black;" align="center">'.getNameBySSO($fe2_sso_id).'</td>
						<td style="border: 1px solid black;" align="center">'.getNameBySSO($fe1_sso_id).'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['ordered_qty'].'</td>
					</tr>';
	}
	$message1.='</tbody></table>';
	if(count($query->result_array())==0)
	{
		$message1 = '';
	}
	return $message1;
}



function pickuppointactiontaken($return_order_id,$from_address,$to_address_data)
{
	$CI = & get_instance();	
	$CI->db->select('u.email,to.order_number');
	$CI->db->from('return_order ro');
	$CI->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
	$CI->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
	$CI->db->join('user u','u.sso_id = to.sso_id');
	if(sendMailsEvenFEDeactivated() == 1)
		$CI->db->where('u.status',1);
	$CI->db->where('ro.return_order_id',$return_order_id);
	$res = $CI->db->get();
	$result = $res->row_array();
	$return_order_info = $CI->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
	$wh_id = $return_order_info['ro_to_wh_id'];
	$country_id = $return_order_info['country_id'];
	$logistics = get_asset_logistic($wh_id,$country_id);
	$logistic_array = array();
	foreach($logistics as $key=>$value)
	{
		$logistic_array[] = $value['email'];
	}
	if(count($result)>0)
		$logistic_array[] = $result['email'];
	$to_address = array_unique($logistic_array);
	$message_info = get_wh_pickup_info($return_order_id);
	if(count($to_address)>0  && $message_info!='' && count($to_address) > 0)
	{
		$message = '';
		$subject = subject_test() .'Alert: Tool Pickup has been successfully picked for Return Number '.$return_order_info['return_number'].' with order Number '.$result['order_number'];
		$message = '<p> Hi All,</p>';
		$message.= '<p> Below are the details for the pickup</p>';
		$message.= '<h2>From</h2><br> '.$from_address['address1'].', '.$from_address['address2'].', '.$from_address['address3'].', '.$from_address['address4'].', '.$from_address['pin_code'].'.<br><h2>To</h2><br>'.$to_address_data['address1'].', '.$to_address_data['address2'].', '.$to_address_data['address3'].','.$to_address_data['address4'].', '.$to_address_data['pin_code'].'.<br>';
		$message.= $message_info;
		$message.= '<p>Regards,</p>';
		$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
		#sending email
		if($to_address!='' && $subject!='' && $message!='')
		{
			send_email($to_address,$subject,$message);
		}
	}
}

function get_wh_pickup_info($return_order_id)
{
	$CI = &get_instance();	
	$CI->db->select('count(oa.asset_id) as on_hand_rec_qty,ot.quantity as ordered_qty,t.part_number,t.part_description,ot.tool_id,rto.tool_order_id');
	$CI->db->from('return_order ro');
	$CI->db->join('return_tool_order rto','rto.return_order_id=ro.return_order_id');
	$CI->db->join('order_status_history osh','osh.tool_order_id=rto.tool_order_id');
	$CI->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');
	$CI->db->join('ordered_tool ot','ot.tool_order_id=rto.tool_order_id');
	$CI->db->join('ordered_asset oa','oa.ordered_tool_id=ot.ordered_tool_id');
	$CI->db->join('tool t','t.tool_id = ot.tool_id');
	$CI->db->group_by('oa.ordered_tool_id');
	$CI->db->where('ro.return_order_id',$return_order_id);
	$qry = $CI->db->get();
	$results = $qry->result_array();
	$messageOA = '';
	$messageOA.='<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center">Ordered Quantity</th>
								<th style="border: 1px solid black;" align="center">On Hand Received Quantity</th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($results as $row1) 
	{
		$messageOA.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['ordered_qty'].'</td>
						<td style="border: 1px solid black;" align="center">'.get_on_hand_received_qty($row1['tool_order_id'],$row1['tool_id'],1).'</td>
					</tr>';
	}
	$messageOA.='</tbody></table>';
	if(count($results)==0)
	{
		$messageOA = '';
	}
	return $messageOA;	
}

function get_on_hand_received_qty($tool_order_id,$tool_id,$qty=0)
{   // 1 for details and 2 for count of assets according to the tool
    $CI = &get_instance();    
    $CI->db->select('t.tool_id,t.part_description,t.part_number,count(*) as qty,ot.quantity as ordered_qty');
    $CI->db->from('order_status_history osh');
    $CI->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
    $CI->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
    $CI->db->join('asset a','a.asset_id = oa.asset_id');
    $CI->db->join('part p','p.asset_id = a.asset_id');
    $CI->db->join('tool t','t.tool_id = p.tool_id');
    $CI->db->join('tool_order to','to.tool_order_id =osh.tool_order_id');
    $CI->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id AND ot.tool_id = t.tool_id');
    $CI->db->join('user u','u.sso_id= to.sso_id');
    $CI->db->where('p.part_level_id',1);
    $CI->db->where('oa.status',1);
    $CI->db->where('osh.tool_order_id',$tool_order_id);
    $CI->db->where('ot.tool_id',$tool_id);
    $CI->db->group_by('t.tool_id');
    $CI->db->where('osh.current_stage_id',7);
    $res = $CI->db->get();    
    if($qty ==0)    
    {
    	return $res->num_rows();
    }
    else
    {
    	if($res->num_rows()>0)
    	{
    		$result = $res->row_array();
    		return $result['qty'];
    	}
    	else
    	{
    		return 0;
    	}
    }
}

function getOrderInfo($tool_order_id)
{
	$CI = & get_instance();
	$CI->db->select('to.*,oda.*,concat(u.sso_id, "-(" ,u.name,")") as sso,u.email');
	$CI->db->from('tool_order to');
	$CI->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
	$CI->db->join('user u','u.sso_id = to.sso_id');
	$CI->db->where('to.tool_order_id',$tool_order_id);
	$res = $CI->db->get();
	return $res->row_array();
}

function get_wh_users($wh_id,$role_id,$country_id)
{
    $CI = & get_instance();
    $CI->db->select('u.*,concat(u.sso_id, " - (" ,u.name,")") as user_name');
    $CI->db->from('user u');
    $CI->db->join('warehouse wh','wh.wh_id=u.wh_id');
    $CI->db->where('u.country_id',$country_id);
    $CI->db->where('u.role_id',$role_id);
    $CI->db->join('user_wh uwh','uwh.sso_id=u.sso_id','left');
    $str = '(u.wh_id = '.$wh_id.'  OR (uwh.wh_id = '.$wh_id.' AND uwh.status = 1) )' ;
    $CI->db->where($str);
    $CI->db->where('u.status',1);
    $CI->db->order_by('u.role_id ASC');
    $CI->db->group_by('u.sso_id');
    $res = $CI->db->get();
    return $res->result_array();
}

# email triggering if acknowledgement is not done even after 48 hrs
# place this method in cron job
function get_unacknowledged_tools($countryId=0)
{
	$ci=& get_instance();
	if($countryId ==0)
		$countryList = $ci->Common_model->get_data('location',array('status'=>1,'level_id'=>2));
	else
		$countryList = $ci->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'location_id'=>$countryId));
	if(count($countryList)>0)
	{
		foreach ($countryList as $key1 => $value1)
		{
			$country_id = $value1['location_id'];
			$details = get_unacknowledged_tool_details($country_id);	
			$task_name = 'crossed_expected_delivery_date';
			$task_access = array(2,3);
			$admins = get_assigned_users_by_task($task_name,$task_access,$country_id);
			$admin_array = array();
			foreach ($admins as $row) 
			{
				$admin_array[]=$row['email'];
			}
			
			if(count($details)>0)
			{
				foreach ($details as $r1)
				{
					$log_array = array();
					$message_info = get_tool_order_details($r1['tool_order_id'],$r1['order_number']);	
					if(@$r1['wh_id']!='')
					{
						$role_id = 3;
						$wh_users = get_wh_users($r1['wh_id'],$role_id,$country_id);
						foreach ($wh_users as $row) 
						{
							$log_array[]=$row['email'];
						}
					}
					$allAddress = array_merge($admin_array,$log_array);
					$cc_address = array_unique($allAddress);
					if($message_info!='' && count($cc_address)>0)
					{	
						if($r1['order_number']==''){
							$user_name = $r1['mto_user_name'];
							$to_address = $r1['mto_email'];
							$orderNumberString = $r1['main_order_number'].' ( '.$r1['stn_number'].' )';
						}else{
							$user_name = $r1['user_name'];
							$to_address = $r1['email'];
							$orderNumberString = $r1['order_number'];
						}
					    $message ='';
					    $subject = subject_test() .'Alert: Tool Acknowledgement is pending for order #'.$orderNumberString;
					    $message ='<p> Hi '.$user_name.',</p>';
					    $message.='<p>Tool acknowledgement is pending for order number:'.$orderNumberString.'<a href="'.SITE_URL.'receive_order'.'">click here to take action</a>
						  </p>';
						$message.= $message_info;
						
						$message.='<p>Regards,</p>';
						$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				        #sending email
				        if($to_address!='' && $subject!='' && $message!='' && count($cc_address)>0)
				        {
				        	send_email($to_address,$subject,$message,$cc_address);
				        }
					}
				}
			}
		}
	}
}

# modified by maruthi on 19thJan19 to get stock transfers and orders also
function get_unacknowledged_tool_details($country_id)
{
	$check_date=date('Y-m-d',strtotime("+2 days"));
	$ci=& get_instance();
	$ci->db->select('to.*,od.*,u.name as user_name,u.email as email,mto.order_number as main_order_number,
		mtou.email as mto_email,mtou.name as mto_user_name,mtou.status as mtou_status,mtou.sso_id as mtou_sso,u.status as to_status,u.sso_id as to_sso');
	$ci->db->from('tool_order to');
	$ci->db->join('order_delivery od','to.tool_order_id=od.tool_order_id');
	$ci->db->join('user u','to.sso_id=u.sso_id');	
	$ci->db->join('st_tool_order sto','sto.stock_transfer_id=to.tool_order_id','left');
	$ci->db->join('tool_order mto','mto.tool_order_id = sto.tool_order_id','left');
	$ci->db->join('user mtou','mtou.sso_id=mto.sso_id','left');	
	$str = '(to.current_stage_id =6 OR (to.current_stage_id = 2 AND to.fe_check= 1))';
	$ci->db->where($str);
	$ci->db->where('od.expected_delivery_date<',$check_date);
	$ci->db->where('to.country_id',$country_id);
	if(sendMailsEvenFEDeactivated() == 1)
	{
		# 1. some times for tool order u.status won't have the child stock transfers at that time mtou.status won't be availbel
		# 2.if any stock transfers are there then u.status it self act as stock transfers and mtou.status
		# -- main tool order user stauts also should required
		# 3.The Admin who created the st may deactivated but FE is not yet deactivated
		$str2 = '( (mtou.status = 1 AND u.status=1) OR (mtou.status IS NULL AND u.status=1) OR (mtou.status = 1)  )';
		$ci->db->where($str2);
	}
	$ci->db->group_by('to.tool_order_id');
	$res=$ci->db->get();
	return $res->result_array();
}


// modified by maruthi on 23rd april'18
function get_tool_order_details($tool_order_id,$order_number)
{
	$CI = & get_instance();
	$CI->db->select('a.asset_number,t.part_number,t.tool_id,t.part_description,ot.quantity,ot.available_quantity as available_qty');
	$CI->db->from('ordered_tool ot');
	$CI->db->join('ordered_asset oa','ot.ordered_tool_id=oa.ordered_tool_id');
	$CI->db->join('part p','oa.asset_id=p.asset_id');
	$CI->db->join('asset a','p.asset_id=a.asset_id');
	$CI->db->join('tool t','p.tool_id = t.tool_id');
	$CI->db->where('ot.tool_order_id',$tool_order_id);
	$CI->db->where('p.part_level_id',1);
	$CI->db->where('ot.status',1);
	$CI->db->where('oa.status',1);
	$query = $CI->db->get();
	$result = $query->result_array();
	$message1 = '';
	$message1.='<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>								
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center"> Ordered Quantity</th>
								<th style="border: 1px solid black;" align="center"> On Hand Received Qty </th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($result as $row1) {
		if($order_number!='')
		{
			$get_on_hand_received_qty = 0;
		}
		else
		{
			$get_on_hand_received_qty = get_on_hand_received_qty($tool_order_id,$row1['tool_id'],2);
		}
		$message1.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>						
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['quantity'].'</td>
						<td style="border: 1px solid black;" align="center">'.$get_on_hand_received_qty.'</td> 
					</tr>';
	}

	$message1.='</tbody></table>';
	if(count($result)==0)
	{
		$message1 = '';
	}
	return $message1;	
}

// modified by maruthi on 23rd april'18
function get_delayed_data($tool_order_id)
{
	$CI = & get_instance();
	$data = get_current_stage_involved_assets_details($tool_order_id,7,array(1,2),2);
	$message1 = '';
	$message1.= '<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>								
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center"> Ordered Quantity</th>
								<th style="border: 1px solid black;" align="center"> In Hand Qty </th>
							</tr>
						</thead>
					<tbody>';
	$sn = 1;
	foreach ($data as $row1) {
		$message1.= '<tr>
						<td style="border: 1px solid black;" align="center">'.$sn++.'</td>						
						<td style="border: 1px solid black;" align="center">'.$row1['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['ordered_qty'].'</td>
						<td style="border: 1px solid black;" align="center">'.$row1['qty'].'</td> 
					</tr>';
	}
	$message1.='</tbody></table>';
	if(count($data)==0)
	{
		$message1 = '';
	}
	return $message1;	
}

// place in cron job
function get_delayed_return_tools($countryId=0)
{
	$ci=& get_instance();
	if($countryId == 0)
		$countryList = $ci->Common_model->get_data('location',array('status'=>1,'level_id'=>2));
	else
		$countryList = $ci->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'location_id'=>$countryId));
	if(count($countryList)>0)
	{
		foreach ($countryList as $key1 => $value1)
		{
			$country_id = $value1['location_id'];
			$details= get_delayed_return_tool_details($country_id);
			$task_name = 'exceededOrderDuration';
			$task_access = array(2,3);
			$admins = get_assigned_users_by_task($task_name,$task_access,$country_id);
			$admin_array = array();
			foreach ($admins as $row) 
			{
				$admin_array[]=$row['email'];
			}
			if(count($details)>0)
			{
				foreach ($details as $r1) 
				{
					$manager_arr = array();
					$to_address = array();
					$message_info = get_delayed_data($r1['tool_order_id']);
					if($r1['rm_status'] == 1)
						$manager_arr[] = $r1['rm_email'];
					$allAddress = array_merge($admin_array,$manager_arr);
					$cc_address = array_unique($allAddress);
					if($message_info!='' && count($cc_address)>0)
					{
						$now = time(); 
				        $return_date = strtotime($r1['return_date']);
				        $datediff = $now - $return_date;
				        $no_of_days = round($datediff / (60 * 60 * 24));
						$to_address[] = $r1['user_email'];
					    $message = '';
					    $subject = subject_test() .'Alert: Tool Due in for return for order # '.$r1['order_number'];
					    $message = '<p> Hi '.$r1['user_name'].',</p>';
					    $message.='<p>Tools is/are due in for return for order number:'.$r1['order_number'].' for more than '.$no_of_days.' days. <a href="'.SITE_URL.'raise_pickup'.'">click here to take action</a>
						  </p>';
						$message.= $message_info;
						$message.='<p>Regards,</p>';
						$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				        #sending email
				        if(count($to_address)> 0 && $subject!='' && $message!='' && count($cc_address)>0)
				        {
				        	send_email($to_address,$subject,$message,$cc_address);
				        }
					}
				}
			}
		}
	}
}

function get_delayed_return_tool_details($country_id)
{
	$check_date=date('Y-m-d');
	$ci=& get_instance();
	$ci->db->select('to.*,od.*,u.name as user_name,u.email as user_email,u.status as user_status,urm.sso_id as rm_sso_id,urm.email as rm_email,urm.status as rm_status');
	$ci->db->from('tool_order to');
	$ci->db->join('order_delivery od','to.tool_order_id=od.tool_order_id');
	$ci->db->join('user u','to.sso_id=u.sso_id');
	$ci->db->join('user urm','urm.sso_id = u.rm_id','left');
	$ci->db->join('order_status_history osh','to.tool_order_id = osh.tool_order_id');
	$ci->db->join('order_asset_history oah','osh.order_status_id=oah.order_status_id');
	$ci->db->where('oah.status<3');
	$ci->db->where('to.current_stage_id',7);
	$ci->db->where('to.return_date<',$check_date);
	$ci->db->where('to.country_id',$country_id);
	if(sendMailsEvenFEDeactivated() == 1)
		$ci->db->where('u.status',1);
	$ci->db->group_by('to.tool_order_id');
	$res=$ci->db->get();
	return $res->result_array();
}

// Mail Triggering For Assets
function calibration_mail($countryId=0)
{
	$ci= & get_instance();
	if($countryId == 0)
		$countryList = $ci->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'region_id !='=>2));
	else
		$countryList = $ci->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'location_id'=>$countryId,'region_id !='=>2));
	if(count($countryList)>0)
	{
		foreach ($countryList as $key1 => $value1) 
		{
			$country_id = $value1['location_id'];
			#get assets with first stage by country
			$ci->db->select('a.asset_id,DATEDIFF(a.cal_due_date,now())as days,a.wh_id');
			$ci->db->from('rc_asset rca');
			$ci->db->join('asset a','a.asset_id = rca.asset_id');
			$ci->db->join('part p','p.asset_id = a.asset_id');
			$ci->db->join('tool t','t.tool_id = p.tool_id');
			$ci->db->where('t.cal_type_id',1);
			$ci->db->where('p.part_level_id',1);
			$ci->db->where('rca.rca_type',2);
			$ci->db->where('rca.current_stage_id',11);
			$ci->db->where('a.country_id',$country_id);
			$res = $ci->db->get();
			$result=$res->result_array();

			#get assigned users for trigger
			$task_name = 'calibration';
			$task_access = array(2,3);
			$admins = get_assigned_users_by_task($task_name,$task_access,$country_id);
			$to_address=array();

		    foreach ($admins as $key => $value) 
		    {
		    	$to_address[]=$value['email'];
		    }
		    $to_address = array_unique($to_address);

			$sn=1;
			$admin_array_90=array();
			$admin_array_45=array();
			$admin_array_30=array();
			$arr_key_90=array();
			$arr_key_45=array();
			$arr_key_30=array();
			$arr_key_15=array();
			$admin_array_15=array();
			$admin_array_0=array();
			$wh_array_15=array();
			$wh_array_30 = array();
			$wh_array_45 = array();
			$wh_array_90 = array();

			if(count($result)>0)
			{
				foreach($result as $row)
				{
					#Checking Asset Position
					$ci->db->select('*');
					$ci->db->from('asset_position');
					$ci->db->where('asset_id',$row['asset_id']);
					$ci->db->where('to_date IS NULL');
					$ci->db->order_by('asset_position_id DESC');
					$ci->db->limit(1);
					$result=$ci->db->get();
					$position=$result->row_array();

					$logistics = get_asset_logistic($row['wh_id'],$country_id);

					#Check 90 days to Calibration 
					if($row['days']==90)
					{
						#At FE
						if(@$position['transit']==0 && @$position['sso_id']!='')
						{
							$arr_key_90[$position['sso_id']]['assets'][] = $row['asset_id'];
						}
						#At Admins
						$admin_array_90[] = $row['asset_id'];

						#To Logistic
						$wh_array_90[$row['wh_id']]['assets'][] = $row['asset_id'];
						foreach($logistics as $key=>$wh_logistic)
						{
							$wh_array_90[$row['wh_id']]['users'][] = $wh_logistic['email'];
						}
						
					}

					#Check 45 days to Calibration 
					if($row['days']==45)
					{
						#At FE
						if(@$position['transit']==0 && @$position['sso_id']!='')
						{
							$arr_key_45[$position['sso_id']]['assets'][] = $row['asset_id'];
						}
						#At Admins
						$admin_array_45[] = $row['asset_id'];

						#To Logistic
						$wh_array_45[$row['wh_id']]['assets'][] = $row['asset_id'];
						foreach($logistics as $key=>$wh_logistic)
						{
							$wh_array_45[$row['wh_id']]['users'][] = $wh_logistic['email'];
						}
					}

					#Check 30 days to Calibration 
					if($row['days']==30)
					{
						#At FE
						if(@$position['transit']==0 && @$position['sso_id']!='')
						{
							$arr_key_30[$position['sso_id']]['assets'][] = $row['asset_id'];
						}
						#At Admins
						$admin_array_30[] = $row['asset_id'];

						#To Logistic
						$wh_array_30[$row['wh_id']]['assets'][] = $row['asset_id'];
						foreach($logistics as $key=>$wh_logistic)
						{
							$wh_array_30[$row['wh_id']]['users'][] = $wh_logistic['email'];
						}

					}

					#check 15 days to calibration
					if($row['days']==15)
					{
						#To Logistic
						$wh_array_15[$row['wh_id']]['assets'][] = $row['asset_id'];
						foreach($logistics as $key=>$wh_logistic)
						{
							$wh_array_15[$row['wh_id']]['users'][] = $wh_logistic['email'];
						}
					}

					#Checking Calibration to <15 days and And At FE
					if($row['days']<15 && $row['days']>0)
					{
						if(@$position['transit']==0 && @$position['sso_id']!='')
						{
							$arr_key_15[$position['sso_id']]['assets'][] = $row['asset_id'];
						}
					}

					#Check Calibration to 15 Days and Still At FE will route to manager
					if($row['days']==15 && @$position['transit']==0 && @$position['sso_id']!='')
					{
						$admin_array_15[$position['sso_id']]['assets'][] = $row['asset_id'];
						
					}

					#Calibration Date is Exceeded 
					if($row['days'] < 0 && $position['transit']==0 && @$position['sso_id']!='')
					{
						$admin_array_0[] = $row['asset_id'];
					}
				}
			}

			$message='';
			$message1='';
			$header='<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>
								<th style="border: 1px solid black;" align="center">Asset Number</th>
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center">Calibration Due Date</th>
								<th style="border: 1px solid black;" align="center">Warehouse</th>
							</tr>
						</thead>
					<tbody>';

			// Send Mail To Admin when Calibration days are 90
			if(count(@$admin_array_90)>0 && count($to_address)>0)
			{
				$message = '<p> Hi All,</p>';
				$message.='Following Assets are due in next 90 days for Calibration <a href="'.SITE_URL.'calibration">Click here to take action</a>';
				$message.=$header;
				$sno=1;
				foreach(@$admin_array_90 as $key => $asset_id)
				{
					$asset_details = asset_details_view($asset_id);
					$message.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
					</tr>';
				}
		       
				$subject = subject_test().'Alert: Following Assets are due in next 90 days for Calibration';
				$message.='</tbody>';
				$message.='</table>';	
				$message.='<p>Regards,</p>';
				$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				if(count($to_address)>0 && $subject!='' && $message!='')
				{
					send_email($to_address,$subject,$message);
				}
			}

			// Send Mail To Tool Owner when Calibration days are 90
			if(count(@$arr_key_90)>0)
			{
				foreach (@$arr_key_90 as $sso_id => $value) 
				{
					$user_arr=$ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
					if(count($user_arr)>0)
					{
						$to = $user_arr['email'];
						$fe_name = $user_arr['name'];
					    $message = '<p> Hi '.$fe_name.',</p>';
						$message.='Following Assets are due in next 90 days for Calibration <a href="'.SITE_URL.'calibration">Click here to take action</a>';
						$message.=$header;
						$sno=1;
						foreach($value['assets'] as $key => $asset_id)
						{
							$asset_details = asset_details_view($asset_id);
							$message.='<tr>
								<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
								<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
							</tr>';
						}
				       
						$subject =subject_test().'Alert: Following Assets are due in next 90 days for Calibration';
						$message.='</tbody>';
						$message.='</table>';	
						$message.='<p>Regards,</p>';
						$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
						if($to!='' && $subject!='' && $message!='')
						{
							send_email($to,$subject,$message);
						}
					}
				}
			}

			// Send Mail to Logistic When Calibration days are at 90
		    if(count(@$wh_array_90)>0)
		    {
		    	foreach ($wh_array_90 as $key => $value) 
		    	{
		    		$message = '<p> Hi All,</p>';
					$message.='Following Assets are due in next 90 days for Calibration';
					$message.=$header;
					$sno=1;

					foreach($value['assets'] as $key => $asset_id)
					{
						$asset_details = asset_details_view($asset_id);
						$message.='<tr>
							<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
							<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
						</tr>';
					}

					$subject = subject_test().'Reminder: Following Assets are due in next 90 days for Calibration';
					$message.='</tbody>';
					$message.='</table>';	
					$message.= '<p>Regards,</p>';
					$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
					$wh_users = array();
					if(count(@$value['users'])>0)
						$wh_users = array_unique($value['users']);
					if(count($wh_users)>0 && $subject!='' && $message!='')
					{
						send_email($wh_users,$subject,$message);
					}	
		    	}
			}

			// Send Mail To Admin when Calibration days are 45
			if(count(@$admin_array_45)>0 && count($to_address)>0)
		    {
			    $message = '<p> Hi All,</p>';
				$message.='Following Assets are due in next 45 days for Calibration <a href="'.SITE_URL.'calibration">Click here to take action</a>';
				$message.=$header;
				$sno=1;
				foreach($admin_array_45 as $key => $asset_id)
				{
					$asset_details = asset_details_view($asset_id);
					$message.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
					</tr>';
				}
		       
				$subject = subject_test().'Alert: Following Assets are due in next 45 days for Calibration';
				$message.='</tbody>';
				$message.='</table>';	
				$message.='<p>Regards,</p>';
				$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				
				if(count($to_address)>0 && $subject!='' && $message!='')
				{
					send_email($to_address,$subject,$message);
				}
			} 

			// Send Mail To Tool Owner when Calibration days are 45
			if(count(@$arr_key_45)>0)
			{
				foreach (@$arr_key_45 as $sso_id => $value) 
				{
					$user_arr=$ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
					if(count($user_arr)>0)
					{
						$to = $user_arr['email'];
						$fe_name = $user_arr['name'];
					    $message = '<p> Hi '.$fe_name.',</p>';
						$message.='Following Assets are due in next 45 days for Calibration <a href="'.SITE_URL.'calibration">Click here to take action</a>';
						$message.=$header;
						$sno=1;
						foreach($value['assets'] as $key => $asset_id)
						{
							$asset_details = asset_details_view($asset_id);
							$message.='<tr>
								<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
								<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
							</tr>';
						}
				       
						$subject =subject_test().'Alert: Following Assets are due in next 45 days for Calibration';
						$message.='</tbody>';
						$message.='</table>';	
						$message.='<p>Regards,</p>';
						$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
						if($to!='' && $subject!='' && $message!='')
						{
							send_email($to,$subject,$message);
						}
					}
				}
			}

			// Send Mail to Logistic When Calibration days are at 45
		    if(count(@$wh_array_45)>0)
		    {
		    	foreach ($wh_array_45 as $key => $value) 
		    	{
		    		$message = '<p> Hi All,</p>';
					$message.='Following Assets are due in next 45 days for Calibration';
					$message.=$header;
					$sno=1;

					foreach($value['assets'] as $key => $asset_id)
					{
						$asset_details = asset_details_view($asset_id);
						$message.='<tr>
							<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
							<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
						</tr>';
					}

					$subject = subject_test().'Reminder: Following Assets are due in next 45 days for Calibration';
					$message.='</tbody>';
					$message.='</table>';	
					$message.= '<p>Regards,</p>';
					$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
					$wh_users = array();
					if(count(@$value['users'])>0)
						$wh_users = array_unique($value['users']);
					if(count($wh_users)>0 && $subject!='' && $message!='')
					{
						send_email($wh_users,$subject,$message);
					}	
		    	}
			}
			
			// Send Mail to Admin When Calibration days are at 30
		    if(count(@$admin_array_30)>0 && count($to_address)>0)
		    {
			    $message = '<p> Hi All,</p>';
				$message.='Following Assets are due in next 30 days for Calibration <a href="'.SITE_URL.'calibration">Click here to take action</a>';
				$message.=$header;
				$sno=1;
				foreach($admin_array_30 as $key => $asset_id)
				{
					$asset_details = asset_details_view($asset_id);
					$message.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
					</tr>';
				}
		       
				$subject = subject_test().'Reminder: Following Assets are due in next 30 days for Calibration';
				$message.='</tbody>';
				$message.='</table>';	
				$message.= '<p>Regards,</p>';
				$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				if(count($to_address)>0 && $subject!='' && $message!='')
				{
					send_email($to_address,$subject,$message);
				}	
			}

			// Send Mail to Tool Owner when Caibration Days are at 30
			if(count(@$arr_key_30)>0)
			{
				foreach (@$arr_key_30 as $sso_id => $value) 
				{
					$user_arr=$ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
					if(count($user_arr)>0)
					{
						$to = $user_arr['email'];
						$fe_name = $user_arr['name'];
					    $message = '<p> Hi '.$fe_name.',</p>';
						$message.='Following Assets are due in next 30 days for Calibration';
						$message.=$header;
						$sno=1;
						foreach($value['assets'] as $key => $asset_id)
						{
							$asset_details = asset_details_view($asset_id);
							$message.='<tr>
								<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
								<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
							</tr>';
						}
				       
						$subject =subject_test().'Reminder: Following Assets are due in next 30 days for Calibration';
						$message.='</tbody>';
						$message.='</table>';	
						$message.='<p>Regards,</p>';
						$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
						if($to!='' && $subject!='' && $message!='')
						{
							send_email($to,$subject,$message);
						}
					}
				}
			}

			// Send Mail to Logistic When Calibration days are at 30
		    if(count(@$wh_array_30)>0)
		    {
		    	foreach ($wh_array_30 as $key => $value) 
		    	{
		    		$message = '<p> Hi All,</p>';
					$message.='Following Assets are due in next 30 days for Calibration';
					$message.=$header;
					$sno=1;

					foreach($value['assets'] as $key => $asset_id)
					{
						$asset_details = asset_details_view($asset_id);
						$message.='<tr>
							<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
							<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
						</tr>';
					}

					$subject = subject_test().'Reminder: Following Assets are due in next 30 days for Calibration';
					$message.='</tbody>';
					$message.='</table>';	
					$message.= '<p>Regards,</p>';
					$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
					$wh_users = array();
					if(count(@$value['users'])>0)
						$wh_users = array_unique($value['users']);
					if(count($wh_users)>0 && $subject!='' && $message!='')
					{
						send_email($wh_users,$subject,$message);
					}	
		    	}
			}
			
			// Send Mail to Logistic When Calibration days are at 15
		    if(count(@$wh_array_15)>0)
		    {
		    	foreach ($wh_array_15 as $key => $value) 
		    	{
		    		$message = '<p> Hi All,</p>';
					$message.='Following Assets are due in next 15 days for Calibration';
					$message.=$header;
					$sno=1;

					foreach($value['assets'] as $key => $asset_id)
					{
						$asset_details = asset_details_view($asset_id);
						$message.='<tr>
							<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
							<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
						</tr>';
					}

					$subject = subject_test().'Reminder: Following Assets are due in next 15 days for Calibration';
					$message.='</tbody>';
					$message.='</table>';	
					$message.= '<p>Regards,</p>';
					$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
					
					$wh_users = array();
					if(count(@$value['users'])>0)
						$wh_users = array_unique($value['users']);
					if(count($wh_users)>0 && $subject!='' && $message!='')
					{
						send_email($wh_users,$subject,$message);
					}	
		    	}
			}

			// Send Mail daily to Tool Owner when calibration days are 15 and still At FE
			if(count(@$arr_key_15)>0)
			{
				foreach (@$arr_key_15 as $sso_id => $value) 
				{
					$user_arr=$ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
					if(count($user_arr)>0)
					{
						$to = $user_arr['email'];
						$fe_name = $user_arr['name'];
					    $message = '<p> Hi '.$fe_name.',</p>';
						$message.='Following Assets are due in for Calibration<br>';
						$message.=$header;
						$sno=1;
						foreach($value['assets'] as $key => $asset_id)
						{
							$asset_details = asset_details_view($asset_id);
							$message.='<tr>
								<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
								<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
							</tr>';
						}
				       
						$subject = subject_test().'Reminder: Following Assets are due in for Calibration';
						$message.='</tbody>';
						$message.='</table>';	
						$message.= '<p>Regards,</p>';
						$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
						if($to!='' && $subject!='' && $message!='')
						{
							send_email($to,$subject,$message);
						}
					}
				}
			}

			// Send Mail to Admin when calibration Date is Exceeded
			if(count($admin_array_0)>0 && count($to_address)>0)
			{
			    $message = '<p> Hi All,</p>';
				$message.='Following Assets are out of calibration and need immediate action for calibrating these Assets <a href="'.SITE_URL.'calibration">Click here to take action</a><br>';
				$message.=$header;
				$sno=1;
				foreach($admin_array_0 as $key => $asset_id)
				{
					$asset_details = asset_details_view($asset_id);
					$message.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
					</tr>';
				}
		       
				$subject = subject_test().'Reminder: Following Assets are out of calibration and need immediate action for calibrating these Assets';
				$message.='</tbody>';
				$message.='</table>';	
				$message.='<p>Regards,</p>';
				$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				if(count($to_address)>0 && $subject!='' && $message!='')
				{
					send_email($to_address,$subject,$message);
				}
			}

			// Send Mail to Manager when Calibration days are at 15 and tool is Still At FE
			if(count(@$admin_array_15)>0)
			{
				foreach (@$admin_array_15 as $sso_id => $value) 
				{
					$fe_arr = $ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
					if(count($fe_arr)>0)
					{
						$fe_name = $fe_arr['name'];
						$manager_arr = $ci->Common_model->get_data_row('user',array('sso_id'=>$fe_arr['rm_id'],'status'=>1));
						if(count($manager_arr)>0)
						{
							$to = $manager_arr['email'];
							$admin_name = $manager_arr['name'];
						    $message = '<p> Hi '.$admin_name.',</p>';
							$message.='Following Assets are due in next 15 days But Still At '.$fe_name.'<br>';
							$message.= $header;
							$sno=1;
							foreach($value['assets'] as $key => $asset_id)
							{
								$asset_details = asset_details_view($asset_id);
								$message.='<tr>
									<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
									<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
									<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
									<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
									<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
									<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
								</tr>';
							}
					       
							$subject = subject_test().'Reminder: Following Assets are due in next 15 days But Still At '.$fe_name;
							$message.='</tbody>';
							$message.='</table>';	
							$message.= '<p>Regards,</p>';
							$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
							if($to!='' && $subject!='' && $message!='')
							{
								send_email($to,$subject,$message);
							}
						}
					}
				}	
			}
		}
	}
}

//To get Logistics Email for Calibration Mail Trigger
function get_asset_logistic($wh_id,$country_id)
{
	if($wh_id!='' && $country_id!='')
	{
		$CI =&get_instance();
		$CI->db->select('u.*');
		$CI->db->from('user u');
		$CI->db->join('user_wh uwh','uwh.sso_id=u.sso_id','left');
		$CI->db->where('u.country_id',$country_id);
		$CI->db->where('u.role_id',3);
        $str = '(u.wh_id = '.$wh_id.'  OR (uwh.wh_id = '.$wh_id.' AND uwh.status = 1) )' ;
        $CI->db->where($str);
        $CI->db->where('u.status',1);
        $CI->db->group_by('u.sso_id');
		$res = $CI->db->get();
	    return $res->result_array();
	}
	else
	{
		return array();
	}
}

# to get Tools coordinators
function get_email_tools_coordinators($country_id)
{
	if($country_id!='')
	{
		$CI = &get_instance();
		$CI->db->select('u.*');
		$CI->db->from('user u');
		$CI->db->where('u.country_id',$country_id);
		$CI->db->where('u.role_id',9);
		$CI->db->where('u.status',1);
		$res = $CI->db->get();
	    return $res->result_array();	
	}
	else
	{
		return array();
	}	
}

// Mail Triggering For Assets
function calibration_mail_egm()
{
	$ci=& get_instance();
	$countryList = $ci->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'region_id'=>2));
	if(count($countryList)>0)
	{
		foreach ($countryList as $key1 => $value1) 
		{
			$country_id = $value1['location_id'];
			#get assets with first stage by country
			$ci->db->select('a.asset_id,DATEDIFF(a.cal_due_date,now())as days,a.wh_id');
			$ci->db->from('egm_calibration ec');
			$ci->db->join('asset a','a.asset_id = ec.asset_id');
			$ci->db->join('part p','p.asset_id = a.asset_id');
			$ci->db->join('tool t','t.tool_id = p.tool_id');
			$ci->db->where('t.cal_type_id',1);
			$ci->db->where('p.part_level_id',1);
			$ci->db->where('ec.cal_status_id',1);
			$ci->db->where('a.country_id',$country_id);
			$res = $ci->db->get();
			$result = $res->result_array();

			#get assigned users for trigger
			$task_name = 'calibration';
			$task_access = array(2,3);
			$admins = get_assigned_users_by_task($task_name,$task_access,$country_id);

			$to_address=array();
		    foreach ($admins as $key => $value) 
		    {
		    	$to_address[]=$value['email'];
		    }
		    $to_address = array_unique($to_address);

			$sn = 1;
			$admin_array_45 = array();
			$admin_array_30 = array();
			$arr_key_45 = array();
			$arr_key_30 = array();
			$arr_key_15 = array();
			$admin_array_0 = array();
			$wh_array_15 = array();
			$wh_array_30 = array();
			$wh_array_45 = array();

			if(count($result)>0)
			{
				foreach($result as $row)
				{
					#Checking Asset Position
					$ci->db->select('*');
					$ci->db->from('asset_position');
					$ci->db->where('asset_id',$row['asset_id']);
					$ci->db->where('to_date IS NULL');
					$ci->db->order_by('asset_position_id DESC');
					$ci->db->limit(1);
					$result = $ci->db->get();
					$position = $result->row_array();

					$logistics = get_asset_logistic($row['wh_id'],$country_id);

					#Check 45 days to Calibration 
					if($row['days']==45)
					{
						#At FE
						if(@$position['transit']==0 && @$position['sso_id']!='')
						{
							$arr_key_45[$position['sso_id']]['assets'][] = $row['asset_id'];
						}

						#At Admins
						$admin_array_45[] = $row['asset_id'];

						#To Logistic
						$wh_array_45[$row['wh_id']]['assets'][] = $row['asset_id'];
						foreach($logistics as $key=>$wh_logistic)
						{
							$wh_array_45[$row['wh_id']]['users'][] = $wh_logistic['email'];
						}
					}

					#Check 30 days to Calibration 
					if($row['days']==30)
					{
						#At FE
						if(@$position['transit']==0 && @$position['sso_id']!='')
						{
							$arr_key_30[$position['sso_id']]['assets'][] = $row['asset_id'];
						}

						#At Admins
						$admin_array_30[] = $row['asset_id'];

						#To Logistic
						$wh_array_30[$row['wh_id']]['assets'][] = $row['asset_id'];
						foreach($logistics as $key=>$wh_logistic)
						{
							$wh_array_30[$row['wh_id']]['users'][] = $wh_logistic['email'];
						}

					}

					#check 15 days to calibration
					if($row['days']==15)
					{
						#To Logistic
						$wh_array_15[$row['wh_id']]['assets'][] = $row['asset_id'];
						foreach($logistics as $key=>$wh_logistic)
						{
							$wh_array_15[$row['wh_id']]['users'][] = $wh_logistic['email'];
						}
					}

					#Checking Calibration to <15 days and And At FE
					if($row['days']<15 && $row['days']>0)
					{
						if(@$position['transit']==0 && @$position['sso_id']!='')
						{
							$arr_key_15[$position['sso_id']]['assets'][] = $row['asset_id'];
						}
					}

					#Calibration Date is Exceeded 
					if($row['days'] < 0)
					{
						$admin_array_0[] = $row['asset_id'];
					}
				}
			}

			$message='';
			$message1='';
			$header='<table style="border-collapse: collapse;">
						<thead style="color:#ffffff; background-color:#6B9BCF;">
							<tr>
								<th style="border: 1px solid black;" align="center">SNO</th>
								<th style="border: 1px solid black;" align="center">Asset Number</th>
								<th style="border: 1px solid black;" align="center">Tool Number</th>
								<th style="border: 1px solid black;" align="center">Tool Description</th>
								<th style="border: 1px solid black;" align="center">Calibration Due Date</th>
								<th style="border: 1px solid black;" align="center">Warehouse</th>
							</tr>
						</thead>
						<tbody>';

			// Send Mail To Admin when Calibration days are 45
			if(count(@$admin_array_45)>0 && count($to_address)>0)
		    {
			    $message = '<p> Hi All,</p>';
				$message.='Following Assets are due in next 45 days for Calibration <a href="'.SITE_URL.'asset">Click here to take action</a>';
				$message.=$header;
				$sno=1;
				foreach($admin_array_45 as $key => $asset_id)
				{
					$asset_details = asset_details_view($asset_id);
					$message.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
					</tr>';
				}
		       
				$subject = subject_test().'Alert: Following Assets are due in next 45 days for Calibration';
				$message.='</tbody>';
				$message.='</table>';	
				$message.='<p>Regards,</p>';
				$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				
				if(count($to_address)>0 && $subject!='' && $message!='')
				{
					send_email($to_address,$subject,$message);
				}
			} 

			// Send Mail To Tool Owner when Calibration days are 45
			if(count(@$arr_key_45)>0)
			{
				foreach (@$arr_key_45 as $sso_id => $value) 
				{
					$user_arr=$ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
					if(count($user_arr)>0)
					{
						$to = $user_arr['email'];
						$fe_name = $user_arr['name'];
					    $message = '<p> Hi '.$fe_name.',</p>';
						$message.='Following Assets are due in next 45 days for Calibration <a href="'.SITE_URL.'asset">Click here to take action</a>';
						$message.=$header;
						$sno=1;
						foreach($value['assets'] as $key => $asset_id)
						{
							$asset_details = asset_details_view($asset_id);
							$message.='<tr>
								<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
								<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
							</tr>';
						}
				       
						$subject =subject_test().'Alert: Following Assets are due in next 45 days for Calibration';
						$message.='</tbody>';
						$message.='</table>';	
						$message.='<p>Regards,</p>';
						$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
						if($to!='' && $subject!='' && $message!='')
						{
							send_email($to,$subject,$message);
						}
					}
				}
			}

			// Send Mail to Logistic When Calibration days are at 45
		    if(count(@$wh_array_45)>0)
		    {
		    	foreach ($wh_array_45 as $key => $value) 
		    	{
		    		$message = '<p> Hi All,</p>';
					$message.='Following Assets are due in next 45 days for Calibration';
					$message.=$header;
					$sno=1;

					foreach($value['assets'] as $key => $asset_id)
					{
						$asset_details = asset_details_view($asset_id);
						$message.='<tr>
							<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
							<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
						</tr>';
					}

					$subject = subject_test().'Reminder: Following Assets are due in next 45 days for Calibration';
					$message.='</tbody>';
					$message.='</table>';	
					$message.= '<p>Regards,</p>';
					$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
					$wh_users = array();
					if(count(@$value['users'])>0)
						$wh_users = array_unique($value['users']);
					if(count($wh_users)>0 && $subject!='' && $message!='')
					{
						send_email($wh_users,$subject,$message);
					}	
		    	}
			}
			
			// Send Mail to Admin When Calibration days are at 30
		    if(count(@$admin_array_30)>0 && count($to_address)>0)
		    {
			    $message = '<p> Hi All,</p>';
				$message.='Following Assets are due in next 30 days for Calibration <a href="'.SITE_URL.'asset">Click here to take action</a>';
				$message.=$header;
				$sno=1;
				foreach($admin_array_30 as $key => $asset_id)
				{
					$asset_details = asset_details_view($asset_id);
					$message.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
					</tr>';
				}
		       
				$subject = subject_test().'Reminder: Following Assets are due in next 30 days for Calibration';
				$message.='</tbody>';
				$message.='</table>';	
				$message.= '<p>Regards,</p>';
				$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				if(count($to_address)>0 && $subject!='' && $message!='')
				{
					send_email($to_address,$subject,$message);
				}	
			}

			// Send Mail to Tool Owner when Caibration Days are at 30
			if(count(@$arr_key_30)>0)
			{
				foreach (@$arr_key_30 as $sso_id => $value) 
				{
					$user_arr=$ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
					if(count($user_arr)>0)
					{
						$to = $user_arr['email'];
						$fe_name = $user_arr['name'];
					    $message = '<p> Hi '.$fe_name.',</p>';
						$message.='Following Assets are due in next 30 days for Calibration';
						$message.=$header;
						$sno=1;
						foreach($value['assets'] as $key => $asset_id)
						{
							$asset_details = asset_details_view($asset_id);
							$message.='<tr>
								<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
								<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
							</tr>';
						}
				       
						$subject =subject_test().'Reminder: Following Assets are due in next 30 days for Calibration';
						$message.='</tbody>';
						$message.='</table>';	
						$message.='<p>Regards,</p>';
						$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
						if($to!='' && $subject!='' && $message!='')
						{
							send_email($to,$subject,$message);
						}
					}
				}
			}

			// Send Mail to Logistic When Calibration days are at 30
		    if(count(@$wh_array_30)>0)
		    {
		    	foreach ($wh_array_30 as $key => $value) 
		    	{
		    		$message = '<p> Hi All,</p>';
					$message.='Following Assets are due in next 30 days for Calibration';
					$message.=$header;
					$sno=1;

					foreach($value['assets'] as $key => $asset_id)
					{
						$asset_details = asset_details_view($asset_id);
						$message.='<tr>
							<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
							<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
						</tr>';
					}

					$subject = subject_test().'Reminder: Following Assets are due in next 30 days for Calibration';
					$message.='</tbody>';
					$message.='</table>';	
					$message.= '<p>Regards,</p>';
					$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
					$wh_users = array();
					if(count(@$value['users'])>0)
						$wh_users = array_unique($value['users']);
					if(count($wh_users)>0 && $subject!='' && $message!='')
					{
						send_email($wh_users,$subject,$message);
					}	
		    	}
			}
			
			// Send Mail to Logistic When Calibration days are at 15
		    if(count(@$wh_array_15)>0)
		    {
		    	foreach ($wh_array_15 as $key => $value) 
		    	{
		    		$message = '<p> Hi All,</p>';
					$message.='Following Assets are due in next 15 days for Calibration';
					$message.=$header;
					$sno=1;

					foreach($value['assets'] as $key => $asset_id)
					{
						$asset_details = asset_details_view($asset_id);
						$message.='<tr>
							<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
							<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
							<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
						</tr>';
					}

					$subject = subject_test().'Reminder: Following Assets are due in next 15 days for Calibration';
					$message.='</tbody>';
					$message.='</table>';	
					$message.= '<p>Regards,</p>';
					$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
					
					$wh_users = array();
					if(count(@$value['users'])>0)
						$wh_users = array_unique($value['users']);
					if(count($wh_users)>0 && $subject!='' && $message!='')
					{
						send_email($wh_users,$subject,$message);
					}	
		    	}
			}

			// Send Mail daily to Tool Owner when calibration days are 15 and still At FE
			if(count(@$arr_key_15)>0)
			{
				foreach (@$arr_key_15 as $sso_id => $value) 
				{
					$user_arr=$ci->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
					if(count($user_arr)>0)
					{
						$to = $user_arr['email'];
						$fe_name = $user_arr['name'];
					    $message = '<p> Hi '.$fe_name.',</p>';
						$message.='Following Assets are due in for Calibration<br>';
						$message.=$header;
						$sno=1;
						foreach($value['assets'] as $key => $asset_id)
						{
							$asset_details = asset_details_view($asset_id);
							$message.='<tr>
								<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
								<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
								<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
							</tr>';
						}
				       
						$subject = subject_test().'Reminder: Following Assets are due in for Calibration';
						$message.='</tbody>';
						$message.='</table>';	
						$message.= '<p>Regards,</p>';
						$message.= '<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
						if($to!='' && $subject!='' && $message!='')
						{
							send_email($to,$subject,$message);
						}
					}
				}
			}

			// Send Mail to Admin when calibration Date is Exceeded
			if(count($admin_array_0)>0 && count($to_address)>0)
			{
			    $message = '<p> Hi All,</p>';
				$message.='Following Assets are out of calibration and need immediate action for calibrating these Assets <a href="'.SITE_URL.'asset">Click here to take action</a><br>';
				$message.=$header;
				$sno=1;
				foreach($admin_array_0 as $key => $asset_id)
				{
					$asset_details = asset_details_view($asset_id);
					$message.='<tr>
						<td style="border: 1px solid black;" align="center">'.$sno++.'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['asset_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_number'].'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['part_description'].'</td>
						<td style="border: 1px solid black;" align="center">'.date('d-m-Y',strtotime($asset_details['cal_due_date'])).'</td>
						<td style="border: 1px solid black;" align="center">'.$asset_details['warehouse'].'</td>
					</tr>';
				}
		       
				$subject = subject_test().'Reminder: Following Assets are out of calibration and need immediate action for calibrating these Assets';
				$message.='</tbody>';
				$message.='</table>';	
				$message.='<p>Regards,</p>';
				$message.='<p>STATS,<br>WIPRO GE Healthcare</p>'.testing_mail();
				if(count($to_address)>0 && $subject!='' && $message!='')
				{
					send_email($to_address,$subject,$message);
				}
			}
			//end
		}
	}
}