<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');//Mahesh Helper


/*
** check is current page is authorized for the logged in user role
** @params: role_id(int), page_name(string)
** @return: true/flase(boolean)
** Created by: Mahesh 7th Dec 2016 03:50 pm, Updated by: --
*/
function is_authorized_page($block_designation_id,$page_name)
{
	$exclude_pages = array('home','unauthorized_request');
	if(in_array($page_name, $exclude_pages))
	{
		return true;
	}
	if($block_designation_id!=''&&$page_name!='')
	{
		$ci = & get_instance();
		$ci->db->from('block_designation_page bdp');
		$ci->db->join('page p','p.page_id = bdp.page_id','inner');
		$ci->db->where('bdp.block_designation_id',$block_designation_id);
		$ci->db->where('p.name',$page_name);
		$res = $ci->db->get();
		return ($res->num_rows()>0)?true:false;
	}
}

function get_logged_user_id()
{
	$ci = & get_instance();
	return $ci->session->userdata('sso_id');
}

function get_logged_user_role()
{
	$ci = & get_instance();
	return $ci->session->userdata('role_id');
}

function is_user_logged_in()
{
	$ci = & get_instance();
	return ($ci->session->userdata('sso_id')>0)?TRUE:FALSE;

}

function get_financial_year()
{
	if(date('m')<4)
	{
		$start_date = (date('Y')-1).'-4-1';
		$end_date = date('Y').'-3-31';
	}
	else
	{
		$start_date = date('Y').'-4-1';
		$end_date = (date('Y')+1).'-3-31';
	}
	$fa = array('start_date'=>$start_date,'end_date'=>$end_date);
	return $fa;
}


function format_date($date,$format='Y-m-d')
{
	$timestamp = strtotime($date);
	return date($format,$timestamp);
}

// mofied by maruthi on 7thAug'18
function indian_format($date,$format='d-m-Y')
{
	if(@$date == "1970-01-01" || @$date =="0000-00-00" || @$date =="")
	{
		return 'NA';
	}
	else
	{
		$timestamp = strtotime($date);
		return date($format,$timestamp);	
	}
	
}
// created by maruthi on 7thAug'18
function full_indian_format($date,$format='d-M-Y')
{
	if(@$date == "1970-01-01" || @$date =="0000-00-00" || @$date =="")
	{
		return 'NA';
	}
	else
	{
		$timestamp = strtotime($date);
		return date($format,$timestamp);	
	}
	
}

// Mahesh 24th June 2017 11:13 PM
function qr_temp_path($excludeSlash=false)
{
	$tempPath = 'uploads/temp';
	if(!$excludeSlash)
	$tempPath .= '/';
	return $tempPath;
}

// Mahesh 24th June 2017 11:34 PM
function qr_temp_url($excludeSlash=false)
{
	$tempPath = SITE_URL1.'uploads/temp';
	if(!$excludeSlash)
	$tempPath .= '/';
	return $tempPath;
}
function get_printer_models()
{
	$printer_modles = array();
	$printer_modles['300dpi'] = array('id'=>'300dpi','name'=>'300 DPI','printer_name'=>'Zebra300');
	$printer_modles['203dpi'] = array('id'=>'203dpi','name'=>'203 DPI','printer_name'=>'Zebra203');
	return $printer_modles;
}