<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* client side Login, Logout URL Check */

function sso_login_url()
{
    $ci = & get_instance();
    $login_url = $ci->config->item('login_url');
    return $login_url;
}

function sso_logout_url()
{
    $ci = & get_instance();
    $logout_url = $ci->config->item('logout_url');
    return $logout_url;
}

function format_url($url)
{
    $url = str_replace(':', '%3A', $url);
    $url = str_replace('/', '%2F', $url);
    return $url;
}

function login_sso_id()
{
    $ci = & get_instance();
    $SM_USER = $ci->config->item('SM_USER');
    return $SM_USER;
}

/*Send email*/
function send_email( $to,$subject = "---", $body,$cc=NULL,$from='gehcindiaservicetoolsteam@ge.com',$from_name='WGE-STATS', $bcc=NULL, $replyto=NULL,  $attachments=[]) {
    $ci = & get_instance();
    $smtp_arr = $ci->config->item('smtp_settings');

    $ci->load->helper('email');
    $ci->load->library('email');
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = $smtp_arr['smtp_host'];
    $config['smtp_port'] = $smtp_arr['smtp_port'];
    $config['smtp_timeout'] = '50';
    $config['smtp_user'] = $smtp_arr['smtp_user'];
    $config['smtp_pass'] = $smtp_arr['smtp_pass'];

    $config['charset'] = 'utf-8';
    $config['newline'] = "\r\n";
    $config['mailtype'] = 'html';
    $config['validation'] = TRUE;    

    $ci->email->initialize($config);
    $email_object = $ci->email;
    $email_object->from($from,$from_name);
    $email_object->to($to);
    $email_object->cc($cc);
    $email_object->subject($subject);
    $email_object->message($body);
    $email_object->bcc($bcc);
    $email_object->reply_to($replyto);
    
    if(count($attachments)>0)
    {
        foreach($attachments as $temp_name=>$path)
        {
            $email_object->attach($path, 'attachment',$temp_name);
        }
    }
    $status = $email_object->send();
    return $status;
}

include('common_helper.php');
include('notification_helper.php');
include('mahesh_helper.php');
include('maruthi_helper.php');
include('maruthi_email_helper.php');
include('koushik_helper.php');
include('roopa_helper.php');
include('srilekha_helper.php');
include('dashboard_helper.php');
include('prasad_helper.php');
include('audit_trail_helper.php');
include('special_cases_helper.php');

/* file end: ./application/helpers/main_helper.php */