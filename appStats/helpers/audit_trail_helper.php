<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function audit_readable_columns($key)
{
    $columns_arr = array(
        'service_type_id'        => 'Service Type',
        'sso_id'                 => 'SSO',
        'order_delivery_type_id' => 'Delivery Type',
        'current_stage_id'       => 'Current Stage',
        'wh_id'                  => 'Warehouse',
        'to_wh_id'               => 'To Warehouse',
        'location_id'            => 'Country',
        'request_date'           => 'Need Date',
        'deploy_date'            => 'Requested Date',
        'siebel_number'          => 'Siebel SR Number',
        'fe_check'               => 'Transfer Type',
        'country_id'             => 'Country',
        'check_address'          => 'Change Address',
        'address_check'          => 'Change Address',
        'asset_status_id'        => 'Asset Status',
        'quantity'               => 'Ordered Qty',
        'asset_condition_id'     => 'Tool Health',
        'return_type_id'         => 'Return Type',
        'address3'               => 'City',
        'address4'               => 'State',
        'ro_to_wh_id'            => 'Return To Warehouse',
        'pickup_point_id'        => 'Pickup Point',
        'shipby_wh_id'           => 'Ship by',
        'fe2_order_number'       => 'FE2 Order Number',
        'asset_status_id'        => 'Asset Status',
        'rm_id'                  => 'Reporting Manager',
        'role_id'                => 'Role',
        'designation_id'         => 'Designation',
        'branch_id'              => 'Branch',
        'modality_id'            => 'Modality',
        'part_level_id'          => 'Part Level',
        'cal_supplier_id'        => 'Calibration Supplier',
        'supplier_id'            => 'Supplier',
        'asset_type_id'          => 'Asset Type',
        'tool_type_id'           => 'Tool Type',
        'asset_level_id'         => 'Asset Level',
        'remarks2'               => 'Allow Order For Oneyear',
        'eq_model_id'            => 'Equipment Model',
        'tool_sub_id'            => 'Child Tool',
        'currency_id'            => 'Currency',
        'gps_tool_id'            => 'GPS Tool',
        'asset_condition_id'     => 'Asset Condition',
        'mwh_id'                 => 'Main Warehouse',
        'asset_status_id'        => 'Asset Status',
        'asset_id'               => 'Asset Num',
        'tool_id'                => 'Tool Desc',
        'print_id'               => 'Print Number',
        'billed_to'              => 'Billed From',
        'tool_order_id'          => 'Tool Order No',
        'return_order_id'        => 'Return Order No',
        'install_base_id'        => 'System ID'
    );

    if(isset($columns_arr[$key]))
    {
        return $columns_arr[$key];
    }
    else
    {
        return ucwords(str_replace("_"," ",$key));
    }
}

function returnDataForSpecialCases($pk,$value)
{
    $specialData = array('fe_check','order_type','check_address','site_readiness','kit','cal_type_id','remarks2','part_status','availability_status','courier_type','return_approval','address_check');
    if(in_array($pk, $specialData))
    { 
        switch ($pk) 
        {
            case 'fe_check':
                $array = array('Request From WH','Transfer From FE');
                break;

            case 'order_type':
                $array = array(1=>'Stock Transfer',2=>'Tool Order');
                break;

            case 'check_address':
            case 'address_check':
                $array = array('No','Yes');
                break;
            case 'site_readiness':
                $array = array(1=>'Yes',2=>'No');
                break;
            case 'return_approval':
                $array = array(1=>"Waiting for FE to FE Transfer Approval",2=>"FE to FE transfer Approved",3=>"FE to FE Transfer Rejected");
            case 'kit':
                $array = array(1=>'Yes',2=>'No');
                break;
            case 'cal_type_id':
                $array = array(1=>'Required',2=>'Not Required');
                break;
            case 'remarks2':
                $array = array(1=>'Yes',2=>'No');
                break;
            case 'part_status':
                $array = array(1=>'Good',2=>'Defective',3=>'Missed');
                break;
            case 'availability_status':
                $array = array(1=>'Active',2=>'Inactive');
                break;
            case 'courier_type':
                $array = array(1=>'By Courier',2=>'By Hand',3=>'By Dedicated');
                break;
            default:
                return FALSE;
                break;
        }
        return $array[$value];
    }
    else
    {
        return FALSE;
    }
}

function checkingPkColumn($pk)
{
    $pk_columns = array(
        'service_type_id',
        'sso_id',
        'order_delivery_type_id',
        'current_stage_id',
        'wh_id',
        'to_wh_id',
        'location_id',
        'country_id',
        'asset_status_id',
        'document_type_id',
        'tool_id',
        'asset_id',
        'role_id',
        'branch_id',
        'designation_id',
        'fe_position',
        'rm_id',
        'modality_id',
        'part_level_id',
        'cal_supplier_id',
        'supplier_id',
        'asset_type_id',
        'tool_type_id',
        'asset_level_id',
        'eq_model_id',
        'tool_sub_id',
        'currency_id',
        'gps_tool_id',
        'asset_condition_id',
        'mwh_id',
        'asset_status_id',
        'print_id',
        'billed_to',
        'tool_order_id',
        'return_order_id',
        'asset_condition_id',
        'return_type_id',
        'ro_to_wh_id',
        'pickup_point_id',
        'shipby_wh_id',
        'install_base_id'
    );
    return (in_array($pk, $pk_columns))?TRUE:FALSE;
}

function auditTableWithPks($pk)
{
    $pkWithTableName = array(
        'service_type_id'        => 'service_type',
        'sso_id'                 => 'user',
        'order_delivery_type_id' => 'order_delivery_type',
        'current_stage_id'       => 'current_stage',
        'wh_id'                  => 'warehouse',
        'to_wh_id'               => 'warehouse',
        'ro_to_wh_id'            => 'warehouse',
        'country_id'             => 'location',
        'asset_status_id'        => 'asset_status',
        'document_type_id'       => 'document_type',
        'tool_id'                => 'tool',
        'asset_id'               => 'asset',
        'asset_condition_id'     => 'asset_condition',
        'return_type_id'         => 'return_type',
        'location_id'            => 'location',
        'pickup_point_id'        => 'order_delivery_type',
        'shipby_wh_id'           => 'warehouse',
        'role_id'                => 'role',
        'designation_id'         => 'designation',
        'branch_id'              => 'branch',
        'fe_position'            => 'location',
        'rm_id'                  => 'user',
        'modality_id'            => 'modality',
        'location_id'            => 'location',
        'part_level_id'          => 'part_level',
        'cal_supplier_id'        => 'supplier',
        'supplier_id'            => 'supplier',
        'asset_type_id'          => 'asset_type',
        'tool_type_id'           => 'tool_type',
        'asset_level_id'         => 'asset_level',
        'eq_model_id'            => 'equipment_model',
        'tool_sub_id'            => 'tool',
        'currency_id'            => 'currency',
        'gps_tool_id'            => 'gps_tool',
        'asset_condition_id'     => 'asset_condition',
        'mwh_id'                 => 'warehouse',
        'asset_status_id'        => 'asset_status',
        'print_id'               => 'print_format',
        'billed_to'              => 'warehouse',
        'tool_order_id'          => 'tool_order',
        'return_order_id'        => 'return_order',
        'install_base_id'        => 'install_base'
    );
    return $pkWithTableName[$pk];
}

function getValueName($pk)
{
    $getValueName = array(
        'service_type_id'        => 'name',
        'sso_id'                 => 'CONCAT(sso_id,"-",name)',
        'order_delivery_type_id' => 'name',
        'pickup_point_id'        => 'name',
        'current_stage_id'       => 'name',
        'wh_id'                  => 'CONCAT(wh_code,"-(",name,")")',
        'shipby_wh_id'           => 'CONCAT(wh_code,"-(",name,")")',
        'to_wh_id'               => 'CONCAT(wh_code,"-(",name,")")',
        'ro_to_wh_id'            => 'CONCAT(wh_code,"-(",name,")")',
        'current_stage_id'       => 'name',
        'wh_id'                  => 'CONCAT(wh_code,"-(",name,")")',
        'to_wh_id'               => 'CONCAT(wh_code,"-(",name,")")',
        'location_id'            => 'name',
        'country_id'             => 'name',
        'asset_status_id'        => 'name',
        'document_type_id'       => 'name',
        'tool_id'                => 'concat(part_number,"-(",part_description,")")',
        'asset_id'               => 'asset_number',
        'asset_condition_id'     => 'name',
        'return_type_id'         => 'name',
        'tool_id'                => 'CONCAT(part_number,"-(",part_description,")")',
        'asset_id'               => 'asset_number',
        'role_id'                => 'name',
        'branch_id'              => 'name',
        'designation_id'         => 'name',
        'rm_id'                  => 'CONCAT(sso_id,"-",name)',
        'fe_position'            => 'name',
        'modality_id'            => 'name',
        'part_level_id'          => 'name',
        'cal_supplier_id'        => 'CONCAT(supplier_code,"-(",name,")")',
        'supplier_id'            => 'CONCAT(supplier_code,"-(",name,")")',
        'asset_type_id'          => 'name',
        'tool_type_id'           => 'name',
        'asset_level_id'         => 'name',
        'eq_model_id'            => 'name',
        'tool_sub_id'            => 'CONCAT(part_number,"-(",part_description,")")',
        'currency_id'            => 'name',
        'gps_tool_id'            => 'CONCAT(uid_number,"-(",name,")")',
        'asset_condition_id'     => 'name',
        'mwh_id'                 => 'CONCAT(wh_code,"-(",name,")")',
        'asset_status_id'        => 'name',
        'print_id'               => 'format_number',
        'billed_to'              => 'CONCAT(wh_code,"-(",name,")")',
        'tool_order_id'          => 'order_number',
        'return_order_id'        => 'return_number',
        'install_base_id'        => 'system_id'
    );
    return $getValueName[$pk];
}

function getBlockName($blockArr,$am_id)
{
    $block_name = NULL;
    if(count($blockArr)>0)
    { 
        foreach ($blockArr as $keys => $values) {
            $key = $keys;
            $value = $values;
            break;
        }
        $check_pk_columns = checkingPkColumn($key);
        if($check_pk_columns)
        {
            $block_name = getPKValue($key,$value);
        }
        else
        {
            $block_name = $value;
        }
        
    }
    return $block_name;
}

function tool_order_audit_data($workflow, $primary_key, $table_name, $trans_type, $parent_ad_id = NULL, $acd_array = array(),$blockArr = array(),$old_data_array = array(),$remarks = NULL,$trans_ad_id = NULL,$transactionExceptionColumns = array(),$main_table=NULL,$main_key=NULL,$country_id=NULL)
{
    $ci = & get_instance();
    $am_id = $ci->Common_model->get_value('audit_master',array('work_flow'=>$workflow),'am_id');
    $block_name = getBlockName($blockArr,$am_id);   
    $main_country = ($country_id =='')?$ci->session->userdata('country_id'):$country_id;
    $insert_ad = array(
        'primary_key'  => $primary_key,
        'am_id'        => $am_id,
        'table_name'   => $table_name,
        'created_by'   => $ci->session->userdata('main_sso_id'),
        'created_time' => date('Y-m-d H:i:s'),
        'status'       => 1,
        'trans_type'   => $trans_type,
        'parent_ad_id' => check_for_empty($parent_ad_id),
        'remarks'      => check_for_empty($remarks),
        'trans_ad_id'  => check_for_empty($trans_ad_id),
        'block_name'   => $block_name,
        "main_table"   => check_for_empty($main_table),
        "main_key"     => check_for_empty($main_key),
        'country_id'   => $main_country
    );
    $ad_id = $ci->Common_model->insert_data('audit_data',$insert_ad);
    $child_data_arr = array();
    switch ($trans_type) {
        case 1:
            if(count($acd_array)>0)
            {
                foreach ($acd_array as $key => $value) 
                {
                    if(!( (in_array($key, exception_columns())) || (in_array($key,$transactionExceptionColumns )) ) )
                    {
                        $column_name = audit_readable_columns($key);
                        if(count($old_data_array)>0)
                        {
                            $old_value = get_old_value($key,$old_data_array);    
                        }
                        else
                        {
                            $old_value = NULL;    
                        }                        
                        $new_value = getOldNewValues($key,$value,2); // 2 is new value
                        $insert_acd = array(
                            'column_name' => $column_name,
                            'old_value'   => $old_value,
                            'new_value'   => $new_value,
                            'ad_id'       => $ad_id
                        );
                        $child_data_arr[] = $insert_acd;
                    }
                }                
            }
            break;
        
        case 2:
            if(count($acd_array)>0)
            {
                foreach ($acd_array as $key => $value) 
                {
                    if(!( (in_array($key, exception_columns())) || (in_array($key,$transactionExceptionColumns )) ) )
                    {
                        $column_name = audit_readable_columns($key);
                        if(count($old_data_array)>0)
                        {
                            $old_value = get_old_value($key,$old_data_array);    
                        }
                        else
                        {
                            $old_value = NULL;    
                        }      
                        $new_value = getOldNewValues($key,$value,2); // 2 is new value
                        $insert_acd = array(
                            'column_name' => $column_name,
                            'old_value'   => $old_value,
                            'new_value'   => $new_value,
                            'ad_id'       => $ad_id
                        );
                        $child_data_arr[] = $insert_acd;
                    }
                }                
            }
            break;

        case 3:
            if(count($acd_array)>0)
            {
                foreach ($acd_array as $key => $value) 
                {
                    if(!in_array($key, exception_columns()))
                    {
                        $column_name = audit_readable_columns($key);
                        $check_pk_columns = checkingPkColumn($key);
                        if($check_pk_columns && $trans_type!=1)
                        {
                            $old_value = getPKValue($key,$value);
                        }
                        else
                        {
                            $return_value = returnDataForSpecialCases($key,$value);
                            if($return_value)
                            {
                                $old_value = $return_value;
                            }
                            else
                            {
                                $old_value = get_old_value($key,$old_data_array);    
                            }
                        }
                        if($check_pk_columns)
                        {
                            $new_value = getPKValue($key,$value);
                        }
                        else
                        {
                            if(returnDataForSpecialCases($key,$value))
                            {
                                
                                $new_value = returnDataForSpecialCases($key,$value);
                            }
                            else
                            {
                                $new_value = $value;    
                            }                    
                        }

                        $insert_acd = array(
                            'column_name' => $column_name,
                            'old_value'   => $old_value,
                            'new_value'   => $new_value,
                            'ad_id'       => $ad_id
                        );
                        $child_data_arr[] =$insert_acd;
                    }
                }
            }
            break;

        default:
            # code...
            break;
    }
    // if($am_id == 17)
    // {
    //     echo "<pre>";print_r($child_data_arr);exit;
    // }
    if(count(@$child_data_arr)>0)
    {
        $ci->Common_model->insert_batch_data('audit_child_data', $child_data_arr);
    }
    return  $ad_id;
}

function updatingAuditData($ad_id,$updateArray,$table_level=2)
{
    $CI = & get_instance();    
    if(count($updateArray)>0)
    {
        foreach ($updateArray as $key => $value) 
        {
            $column_name = audit_readable_columns($key);
            $new_value = getOldNewValues($key,$value,2); // 1 is old value

            if($table_level == 2)
            {
                $updateData = array(
                    'new_value'     => $new_value
                );                
                $updateWhere = array(
                    'ad_id'         => $ad_id,
                    'column_name'   => $column_name,
                );
            }
            else
            {
                $updateData = array(
                    'remarks'   => $new_value
                );
                $updateWhere = array(
                    'ad_id'     =>$ad_id
                );
            }   
            
            if($table_level == 2) $table_name = "audit_child_data";
            else $table_name = "audit_data";
            $CI->Common_model->update_data("$table_name",$updateData,$updateWhere);            
        } 
    }
}

function assetStatusAudit($asset_id,$oldStatus,$newStatus,$workflow,$trans_type,$parent_ad_id=NULL,$remarks=NULL,$trans_ad_id=NULL,$main_table=NULL,$main_key=NULL,$country_id=2)
{
    //echo "<pre>";print_r(func_get_args());exit;
    if(is_array($oldStatus))
    {
        $oldArr = $oldStatus;
        $newArr = $newStatus;
    }
    else
    {
        $oldArr = array('asset_status_id'=>$oldStatus);
        $newArr = array('asset_status_id'=>$newStatus);    
    }
    
    $blockArr = array('asset_id'=>$asset_id);
    # Transaction Audit Entry
    return tool_order_audit_data($workflow,$asset_id,'asset',$trans_type,$parent_ad_id,$newArr,$blockArr,$oldArr,$remarks,$trans_ad_id,array(),$main_table,$main_key,$country_id); 
}

function returnPksForSpecialCases($pk)
{
    $originalPks = array(
        'to_wh_id'        => 'wh_id',
        'country_id'      => 'location_id',
        'fe_position'     => 'location_id',
        'rm_id'           => 'sso_id',
        'tool_sub_id'     => 'tool_id',
        'cal_supplier_id' => 'supplier_id',
        'mwh_id'          => 'wh_id',
        'billed_to'       => 'wh_id',
        'pickup_point_id'   => 'order_delivery_type_id',
        'shipby_wh_id'      => 'wh_id',
        'ro_to_wh_id'       => 'wh_id'
        );
    if(array_key_exists($pk, $originalPks))
        return $originalPks[$pk];
    else
        return $pk;
}

function getPKValue($pk,$value)
{
    $CI = & get_instance();    
    $table = auditTableWithPks($pk);
    $returnValue = getValueName($pk);
    $primary_key = returnPksForSpecialCases($pk);
    return $CI->Common_model->get_value("$table",array("$primary_key"=>$value),$returnValue);
}

function getOldNewValues($key,$valueParameter,$type)
{
    $check_pk_columns = checkingPkColumn($key);
    if($check_pk_columns)
    {
        $value = getPKValue($key,$valueParameter);
    }
    else
    {
        $value = returnDataForSpecialCases($key,$valueParameter);
        if(!$value)
        {
            $value = $valueParameter;
        }                    
    }
    return check_for_empty($value);
}

function audit_data($workflow, $primary_key, $table_name, $trans_type, $parent_ad_id = NULL, $acd_array = array(), $blockArr = array(), $old_acd_array = array(),$remarks = NULL,$trans_ad_id = NULL,$transactionExceptionColumns = array(),$main_table = NULL,$main_key = NULL,$country_id = NULL)
{
    $ci = & get_instance();
    $am_id = $ci->Common_model->get_value('audit_master',array('work_flow'=>$workflow),'am_id');

    $block_name = getBlockName($blockArr,$am_id);
    if($country_id == NULL)
    {
        $country_id = $ci->session->userdata('s_country_id');
    }
    $created_by = $ci->session->userdata('main_sso_id');
    $insert_ad = array(
        'primary_key'  => $primary_key,
        'am_id'        => $am_id,
        'table_name'   => $table_name,
        'created_by'   => check_for_empty($created_by),
        'created_time' => date('Y-m-d H:i:s'),
        'status'       => 1,
        'trans_type'   => $trans_type,
        'parent_ad_id' => check_for_empty($parent_ad_id),
        'remarks'      => check_for_empty($remarks),
        'trans_ad_id'  => check_for_empty($trans_ad_id),
        'block_name'   => check_for_empty($block_name),
        'main_table'   => check_for_empty($main_table),
        'main_key'     => check_for_empty($main_key),
        'country_id'   => check_for_empty($country_id)
    );
    $ad_id = $ci->Common_model->insert_data('audit_data',$insert_ad);

    $child_data_arr = array();
    switch ($trans_type) {
        case 1:
            if(count($acd_array)>0)
            {
                foreach ($acd_array as $key => $value)
                {
                    if(!( (in_array($key, exception_columns())) || (in_array($key,$transactionExceptionColumns )) ))
                    {
                        $column_name = audit_readable_columns($key);
                        if(count($old_acd_array)>0)
                        {
                            $old_value = get_old_value($key,$old_acd_array);    
                        }
                        else
                        {
                            $old_value = NULL;    
                        }                        
                        $new_value = getOldNewValues($key,$value,2);//2 is new value
                        $insert_acd = array(
                            'column_name' => $column_name,
                            'old_value'   => $old_value,
                            'new_value'   => $new_value,
                            'ad_id'       => $ad_id
                        );
                        $child_data_arr[] = $insert_acd;
                    }
                }
            }
            break;
        
        case 2:
            if(count($acd_array)>0)
            {
                foreach ($acd_array as $key => $value) 
                {
                    if(!( (in_array($key, exception_columns())) || (in_array($key,$transactionExceptionColumns )) ) )
                    {
                        $column_name = audit_readable_columns($key);
                        if(count($old_acd_array)>0)
                        {
                            $old_value = get_old_value($key,$old_acd_array); 
                        }
                        else
                        {
                            $old_value = NULL;    
                        }
                        
                        $new_value = getOldNewValues($key,$value,2);//2 is new value
                        $insert_acd = array(
                            'column_name' => $column_name,
                            'old_value'   => $old_value,
                            'new_value'   => $new_value,
                            'ad_id'       => $ad_id
                        );
                        $child_data_arr[] = $insert_acd;
                    }
                }
            }
            break;

        case 3:
            if(count($old_acd_array)>0)
            {
                foreach ($old_acd_array as $key => $value) 
                {
                    if(!( (in_array($key, exception_columns())) || (in_array($key,$transactionExceptionColumns )) ) )
                    {
                        $column_name = audit_readable_columns($key);
                        if(count($old_acd_array)>0)
                        {
                            $old_value = get_old_value($key,$old_acd_array); 
                        }
                        else
                        {
                            $old_value = NULL;    
                        }
                        
                        $new_value = NULL;
                        $insert_acd = array(
                            'column_name' => $column_name,
                            'old_value'   => $old_value,
                            'new_value'   => $new_value,
                            'ad_id'       => $ad_id
                        );
                        $child_data_arr[] = $insert_acd;
                    }
                }
            }
            break;

        default:
            # code...
            break;
    }

    if(count(@$child_data_arr)>0)
    {
        $ci->Common_model->insert_batch_data('audit_child_data', $child_data_arr);
    }
    return  $ad_id;
}

function exception_columns()
{
    $exp_col = array('created_by','created_time','modified_by','modified_time','status');
    return $exp_col;
}

function get_old_value($key,$old_acd_array)
{
    $old_value = NULL;
    if(isset($old_acd_array[$key]))
    {
        $old_value = getOldNewValues($key,$old_acd_array[$key],1); // 1 is old 
        if($old_value=='')
        {
            $old_value = NULL;
        }
    }
    return $old_value;
}

function check_for_empty($value)
{
    if($value!='')
    {
        return $value;
    }
    else
    {
        return NULL;
    }
}
function unsetOrderedToolsExceptionColumns($order_tool_data)
{
    unset($order_tool_data['tool_order_id'],$order_tool_data['tool_id'],$order_tool_data['status'],$order_tool_data['created_by'],$order_tool_data['created_time']);
    return $order_tool_data;
}
function unsetStockTransferData($st_data)
{
    unset($st_data['fe_check'],$st_data['created_by'],$st_data['created_time']);
    return $st_data;
}

# Key value is 1 is key 2 is value
# type 1 is defective , 2 is missed
function defectiveApprovalKeyValues($keyValue=1,$type=2) 
{
    if($keyValue == 1)
    {
        return "Approval";
    }
    else
    {
        switch (@$type) {
        case 1:
            $value = "Good";
            break;
        case 2:
            $value = "Waiting for Defectiva Asset Approval";
            break;
        case 3:
            $value = "Waiting for Missed Asset Approval";
            break;
        case 4:
            $value = "NA";
            break;
        default:
            $value = "NA";
            break;

        } 
        return $value;   
    }
}

function defectiveAssetKeyValues($asset_id)
{
    $CI = &get_instance();
    $approval = $CI->Common_model->get_value('asset',array('asset_id'=>$asset_id),'approval_status');
    if($approval == 0)
    {
        $type = 4;
    }
    else # previously it is under approval 
    {
        $dataWhere = array(
            'asset_id'  => $asset_id,
            'status <'  => 10
        );
        $tempType = $CI->Common_model->get_value('defective_asset',$dataWhere,'type');
        if($tempType!='') # if unable to find the previous record then NA
            $type = ($tempType == 1)?2:3;
        else
            $type = 4;
    }

    return array(defectiveApprovalKeyValues(1) => defectiveApprovalKeyValues(2,$type));
}

# prepare old and new arrays for asset position,approval,stats
function auditAssetData($arr,$oldNew=1)
{
    $CI = &get_instance();
    $asset_id = @$arr['asset_id'];
    $asset_status_id = @$arr['asset_status_id'];
    $type = @$arr['type'];  // 2 for defective // 3 for missing
    if(isset($arr['asset_status_id']))
    {
        $auditAssetData = array(
            'tool_availability'     => get_asset_position($asset_id),
            'asset_status_id'       => $asset_status_id
        );   
    }
    else
    {
        $auditAssetData = array();
    }
    # pushing the approval if it changes
    if($oldNew == 1)
    {
        $auditAssetData = array_merge($auditAssetData,defectiveAssetKeyValues($asset_id));
    }
    else
    {
        if($type!='')
            $auditAssetData[defectiveApprovalKeyValues(1) ] = defectiveApprovalKeyValues(2,$type) ;
    }
    return $auditAssetData;
}

function get_da_key()
{
    return "approval_status";
}

function get_da_status($asset_id,$approval_status)
{
    $CI = & get_instance();
    if($approval_status == 0)
    {
        $type = 4;
    }
    else # previously it is under approval 
    {
        $dataWhere = array(
            'asset_id'  => $asset_id,
            'status <'  => 10
        );
        $tempType = $CI->Common_model->get_value('defective_asset',$dataWhere,'type');
        if($tempType!='') # if unable to find the previous record then NA
            $type = ($tempType == 1)?2:3;
        else
            $type = 4;
    }

    switch ($type) {
        case 1:
            $value = "Good";
            break;
        case 2:
            $value = "Waiting for Defective Asset Approval";
            break;
        case 3:
            $value = "Waiting for Missed Asset Approval";
            break;
        case 4:
            $value = "NA";
            break;
        default:
            $value = "NA";
            break;
    }
    return $value;
}

function get_ad_list($loop_arr,$final_arr)
{
    $ci=& get_instance();
    $loop_list = $loop_arr;
    
    if(count($loop_list)>0)
    {
        $loop_arr = array();
        foreach ($loop_list as $key => $value) 
        {
            $ad_list = $ci->Common_model->get_data('audit_data',array('parent_ad_id'=>$value));
            if(count($ad_list)>0)
            {
                foreach ($ad_list as $key1 => $value1) 
                {
                    $loop_arr[$value1['ad_id']] = $value1['ad_id'];
                    $final_arr[$value1['ad_id']] = $value1['ad_id'];
                }
            }
        }
        return get_ad_list($loop_arr,$final_arr);
    }
    else
    {
        return $final_arr;
    }
}

function get_display_name($send_data)
{
    $ci = &get_instance();
    $received = explode('~', $send_data);
    $primary_key = validate_string(storm_decode($received[0]));
    $table_name  = validate_string($received[1]);
    $work_flow   = validate_string($received[2]);
    $main_table  = validate_string($received[3]);
    $main_key    = validate_number($received[4]);

    $display_name = '';
    if($primary_key==0 && $main_table !='')
    {
        $order_number = $ci->Common_model->get_value('tool_order',array('tool_order_id'=>$main_key),'order_number');
        $display_name = "Tool Order : <strong>".$order_number."</strong>";
    }
    else if($table_name == 'user')
    {
        $arr = $ci->Common_model->get_data_row('user',array('sso_id'=>$primary_key));
        $name = $arr['sso_id'].'-('.$arr['name'].')';
        $display_name = "User : <strong>".@$name."</strong>";
    }
    else if($table_name == 'tool')
    {
        $arr = $ci->Common_model->get_data_row('tool',array('tool_id'=>$primary_key));
        $name = $arr['part_number'].'-('.$arr['part_description'].')';
        $display_name = "Tool : <strong>".@$name."</strong>";
    }
    else if($table_name == 'asset')
    {
        $arr = $ci->Common_model->get_data_row('asset',array('asset_id'=>$primary_key));
        $name = $arr['asset_number'];
        $display_name = "Asset : <strong>".@$name."</strong>";
    }
    return $display_name;
}

function validate_string_is_date($string)
{
    $data = $string;
    if (DateTime::createFromFormat('Y-m-d', $string) !== FALSE) 
    {
        $data = date('d-m-Y',strtotime($string));
    }
    return $data;
}