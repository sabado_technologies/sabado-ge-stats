<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_country_location($country_id)
{
	$ci = & get_instance();
	$country = $ci->Common_model->get_value('location',array('location_id'=>$country_id),'name');
	return $country;
}

function getActiveOrInactiveCountInToolReport($searchParams)
{
	$CI = & get_instance();
	if($searchParams['zone']!=1)
	{   
		$zon =  $CI->Report_m->get_zone_details_based_on_country($searchParams);
		$wh_arr =  get_wh_by_zone($zon['location_id'],$searchParams['task_access']);
	}
	if($searchParams['category']!='')
		{
			$cat=$CI->Common_model->get_value('location',array('name'=>$searchParams['category']),'location_id');
		}
	$CI->db->select('as.type,count(*) as count');
    $CI->db->from('asset a');
    $CI->db->join('asset_status as','a.status = as.asset_status_id');
    $CI->db->join('part p','p.asset_id = a.asset_id');
    $CI->db->join('tool t','t.tool_id = p.tool_id');
    $CI->db->join('warehouse w','a.wh_id=w.wh_id');
    if($searchParams['modality_id']!='')
	$CI->db->where('a.modality_id',$searchParams['modality_id']);
	if(@$searchParams['tool_type_id']!='')
	$CI->db->where('t.tool_type_id',$searchParams['tool_type_id']);
	if($searchParams['zone']!=1)
	{
		$CI->db->where_in('a.wh_id',$wh_arr);
	}
	else
	{
		if($searchParams['task_access']==1 || $searchParams['task_access']==2)
		{
			$CI->db->where('a.country_id',$CI->session->userdata('s_country_id'));
		}
		if($searchParams['task_access'] == 3 && $_SESSION['header_country_id']!='')
        {
        	$CI->db->where('a.country_id',$CI->session->userdata('header_country_id'));
        }
        if($searchParams['country_id']!='')
        {
        	$CI->db->where('a.country_id',$searchParams['country_id']);
        }
        if($searchParams['category']!='')
        {
        	$CI->db->Where('a.country_id',$cat);
        }

	}
	if($searchParams['wh_id']!='')
	{
		$CI->db->Where('a.wh_id',$searchParams['wh_id']);
	}
	$CI->db->where('w.status',1);
	$CI->db->where('p.part_level_id',1);
	
	$CI->db->group_by('as.type');
	$res = $CI->db->get();
	return $res->result_array();
}
function getActiveOrInactiveCountInToolReportForCountries($searchParams)
{
	$CI = & get_instance();
	$CI->db->select('as.type,count(*) as count,a.country_id,l.name as country_name');
    $CI->db->from('asset a');
    $CI->db->join('asset_status as','a.status = as.asset_status_id');
    $CI->db->join('part p','p.asset_id = a.asset_id');
    $CI->db->join('tool t','t.tool_id = p.tool_id');
    $CI->db->join('warehouse w','a.wh_id=w.wh_id');
    $CI->db->join('location l','a.country_id=l.location_id');
    if($searchParams['modality_id']!='')
	$CI->db->where('a.modality_id',$searchParams['modality_id']);
	if(@$searchParams['tool_type_id']!='')
	$CI->db->where('t.tool_type_id',$searchParams['tool_type_id']);
	if($searchParams['country_id']!='')
	{
		$CI->db->where('a.country_id',$searchParams['country_id']);
	}
	else
	{
		$CI->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);	
	}
	if($searchParams['wh_id']!='')
	{
		$CI->db->Where('a.wh_id',$searchParams['wh_id']);
	}
	$CI->db->where('w.status',1);
	$CI->db->where('p.part_level_id',1);
	$CI->db->group_by('a.country_id,as.type');
	$res = $CI->db->get();
	return $res->result_array();
}
 function getWHorZonedataToolReport($searchParams)
    {
        $CI = &get_instance();        
        
        // for All
        if($searchParams['r_zone'] == 1)
        { 
            if($searchParams['r_wh_id']!='')  
            { 
                $single_wh =  $CI->Common_model->get_data('warehouse',array('wh_id'=>$searchParams['r_wh_id']));
                return array($single_wh,2); // 2 is For Warehosue
            }
            else
            {
               if($searchParams['country_id']!='')
               {
               	   $country_id = $searchParams['country_id'];
               }
               elseif($searchParams['task_access']==1 || $searchParams['task_access']==2)
               {
               	  $country_id = $_SESSION['s_country_id'];
               }
               elseif($searchParams['task_access']==3)
               {
               	    if($_SESSION['header_country_id']!='')
               	    {
               	    	$country_id = $_SESSION['header_country_id'];
               	    }
               	    else{
               	    	$country_id = $CI->Common_model->get_value('location',array('name'=>$searchParams['first_x_cat']),'location_id');
					}
               }
               $location_loop = $CI->Common_model->get_data('location',array('level_id'=>3,'parent_id'=>$country_id));
               return array($location_loop,1); // 1 is For Locations
            }
        }
        // particular Zone
        else
        {
            if($searchParams['r_wh_id']!='')  
            {
                $single_wh =  $CI->Common_model->get_data('warehouse',array('wh_id'=>$searchParams['r_wh_id']));
                return array($single_wh,2); // 2 is For Warehosue
            }
            else
            {
                $whs = get_wh_by_zone($searchParams['r_zone'],$searchParams['task_access']);
                $CI->db->from('warehouse');
                $CI->db->where_in('wh_id',$whs);
                $res= $CI->db->get();
                $wh_list = $res->result_array();
                if(count($whs)>0)
                {
                    return array($wh_list,2); // 2 is for warehouse
                } 
                else
                {
                    return array();
                } 
            }

        }        
    }
function get_tool_asset_list_ib_records($searchParams)
{
    $status = array(1,2,3,4,8,10,12);
    $CI = & get_instance();
    $CI->db->select('t.tool_id,t.part_number,t.part_description');
    $CI->db->from('tool t');
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->where('p.part_level_id',1);
    $CI->db->where_in('a.status',$status);
    if($searchParams['modality_id'] !='')
    {
        $CI->db->where('t.modality_id',$searchParams['modality_id']);
    }
    if($searchParams['country_id']!='')
    {
        $CI->db->where('t.country_id',$searchParams['country_id']);
    }
    $CI->db->group_by('t.tool_id');
    $res = $CI->db->get();
    return $res->result_array();
}
function getIBdataIbReport($searchParams)
{
    $CI = & get_instance();
    $zone = $searchParams['zone'];
    $modality_id = $searchParams['modality_id'];
    $wh_id = $searchParams['wh_id'];
    $tool_id = $searchParams['tool_id'];
    if($modality_id == '')
    {
        if($tool_id!='')
        {
            $modality_id = $CI->Common_model->get_value('tool',array('tool_id'=>$tool_id),'modality_id');
        }
    }
    $m_list = get_all_modality($modality_id);
	$chartsData = array();
	$modality_list = array();
	$c_data = array();
	$t_data = array();
    if($zone==1)
    {
        foreach ($m_list as $mod) 
	    {
	        $modality_list[] = $mod['name'];
	        $c_data[] = get_install_base_all_countries($mod['modality_id'],$searchParams);
	        $t_data[] = get_tool_count_all_countries($mod['modality_id'],$searchParams);
	    }
    }
    else
    {

    	if($wh_id == '')
	    { 
	    	$wh_list = get_wh_by_zone($zone,$searchParams['task_access']); 
	    }
	    else
	    { 
	    	$wh_list = array($wh_id); 
	    }
	    $loc_list = get_locations_by_zone($zone);
	    foreach ($m_list as $mod) 
	    {
	        $modality_list[] = $mod['name'];
	        $c_data[] = get_install_base($mod['modality_id'],$loc_list);

	        $t_data[] = get_tool_count($mod['modality_id'],$wh_list,$tool_id);
	    }
	}
    $chartsData['modality_list'] = $modality_list;
    $chartsData['CustomerData'] = $c_data;
    $chartsData['ToolData'] = $t_data;
   // echo "<pre>"; print_r($chartsData);exit;
    $chartsData = json_encode($chartsData, JSON_NUMERIC_CHECK);
    return $chartsData;
}
function gettoolIBdataIBReport($searchParams)
{
    $CI = & get_instance();
    $zone = $searchParams['zone'];
    $modality_name = $searchParams['modality_id'];
    $wh_id = $searchParams['wh_id'];
    $tool_id = $searchParams['tool_id'];
    $tool_list = array();
    if($modality_name!='')
    {
        $modality_id = $CI->Common_model->get_value('modality',array('name'=>$modality_name),'modality_id');
    }
    else
    {
        $modality_id = '';
    }
    
    $searchParams['modality_id']=$modality_id;
    if($tool_id == '')
    {
        $tool_list = get_tool_asset_list_ib_records($searchParams);
    }
    else
    {
        $tool_list[] = $CI->Common_model->get_data_row('tool',array('tool_id'=>$tool_id));
    }
    $chartsData = array();
    $tool_name_list = array();
    $c_data = array();
    $t_data = array();
    if($zone==1)
    {
    	foreach ($tool_list as $tool) 
	    {
	        $tool_name_list[] = $tool['part_number'];
	        $searchParams['tool_id']=$tool['tool_id'];
	        $c_data[] = get_install_base_all_countries($modality_id,$searchParams);
	        $t_data[] = get_tool_count_all_countries($modality_id,$searchParams);
	    }
    }
    else
    {
    	if($wh_id == ''){ $wh_list = get_wh_by_zone($zone); }
	    else { $wh_list = array($wh_id); }
	    $loc_list = get_locations_by_zone($zone);
	     foreach ($tool_list as $tool) 
	    {
	        $tool_name_list[] = $tool['part_number'];
	        $c_data[] = get_install_base($modality_id,$loc_list);
	        $t_data[] = get_tool_count($modality_id,$wh_list,$tool['tool_id']);
	    }
	}
   
    $chartsData['tool_name_list'] = $tool_name_list;
    $chartsData['CustomerData'] = $c_data;
    $chartsData['ToolData'] = $t_data;
    $chartsData = json_encode($chartsData, JSON_NUMERIC_CHECK);
    return $chartsData;
}
function get_tool_count_all_countries($modality_id,$searchParams)
{
    $status = array(1,2,3,4,8,10,12);
    $CI = & get_instance();
    $CI->db->select('count(a.asset_id) as tool_count');
    $CI->db->from('tool t');
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
     $CI->db->join('warehouse w','a.wh_id=w.wh_id');
    $CI->db->where('w.status',1);
    if($searchParams['task_access']==1 || $searchParams['task_access']==2)
    {
    	$CI->db->where('t.country_id',$CI->session->userdata('s_country_id'));
    }
    elseif($searchParams['task_access']==3)
    {
    	if($_SESSION['header_country_id']!='')
			{
				$CI->db->where('t.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$CI->db->where('t.country_id',$searchParams['country_id']);
				}
				else
				{
					$CI->db->where_in('t.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
    }
    if($searchParams['wh_id']!='')
    {
    	$CI->db->where('a.wh_id',$searchParams['wh_id']);
    }
    $CI->db->where('p.part_level_id',1);
    $CI->db->where_in('a.status',$status);
    if($modality_id!='')
    {
    	 $CI->db->where('a.modality_id',$modality_id);
    }
    if($searchParams['tool_id'] != '')
    {
        $CI->db->where('t.tool_id',$searchParams['tool_id']);
    }
    $res = $CI->db->get();
    $result = $res->row_array();

    if($res->num_rows()>0)
    {
        $res = $result['tool_count'];
    }
    else
    {
        $res = 0;
    }
    return $res;

}
function get_install_base_all_countries($modality_id,$searchParams)
{
    $CI = & get_instance();
    $CI->db->select('ib.modality_id,count(ib.install_base_id) as mod_count');
    $CI->db->from('install_base ib');
    $CI->db->join('customer_site cs','cs.customer_site_id = ib.customer_site_id');
    if($modality_id!='')
    {
    	$CI->db->where('ib.modality_id',$modality_id);
    }
    if($searchParams['task_access']==1 || $searchParams['task_access']==2)
    {
    	$CI->db->where('cs.country_id',$CI->session->userdata('s_country_id'));
    }
    elseif($searchParams['task_access']==3)
    {
    	if($_SESSION['header_country_id']!='')
			{
				$CI->db->where('cs.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$CI->db->where('cs.country_id',$searchParams['country_id']);
				}
				else
				{
					$CI->db->where_in('cs.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
    }
    $CI->db->where('ib.status',1);
    $res = $CI->db->get();
    $result = $res->row_array();
    if($res->num_rows()>0)
    {
        $res = $result['mod_count'];
    }
    else
    {
        $res = 0;
    }
    return $res;
}
?>