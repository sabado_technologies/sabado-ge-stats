<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function switch_url_based_on_current_page()
{
    if(isset($_SESSION['current_link_string']))
    {
        $check = $_SESSION['current_link_string'];
    }
    else
    {
         $check = '';
    }
    if($check == '')
    {
        $_SESSION['current_offset_string'] = base_url(uri_string());
        $_SESSION['current_link_string'] = base_url(uri_string());
    }
    else
    {
        $_SESSION['current_offset_string'] = $_SESSION['current_link_string'];
        $_SESSION['current_link_string'] = base_url(uri_string());
    }
}

function get_current_offset($current_offset_string)
{
    $string = strrev($current_offset_string);
    $first = explode('/', $string);
    $check = validate_number($first[0]);
    if($check)
    {
        return '/'.strrev($check);
    }
    else
    {
        return '/0';
    }
}

function addressChangeType()
{
    $address_array = array(
        array("id"=>1,"name"=>"Order Address Change"),
        array("id"=>2,"name"=>"Return Address Change")
    );
    return $address_array;              
}

function convertIntoIndexedArray($array,$value,$string=0)
{
    $indexedArray = array();
    if(is_numeric($array))
    {
        $indexedArray[] = $array;
    }
    else
    {
        //echo '<pre>';print_r($array);exit;
        foreach ($array as $key => $values) {
            $indexedArray[] = $values["$value"];
        }
    }
    if($string==1)
        return implode(',', $indexedArray);
    else
        return $indexedArray;
}

function checkAllSTsCancelForFEOrder($tool_order_id)
{
    $CI = &get_instance();
    $cancel_data = $CI->Common_model->get_data('st_tool_order',array('tool_order_id'=>$tool_order_id,'status'=>2));
    $all_data = $CI->Common_model->get_data('st_tool_order',array('tool_order_id'=>$tool_order_id));
    if(count($cancel_data) == count($all_data))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
// cancel 1 for cancelling 
// order_type = 1 for sts  
# $auditAt 
    // 1 -audit should happen at helper, 
    // 2 - at controller level and tool status also decided at controller
    //3 audit at helper and tool is removed
function unlockingByToolOrderId($tool_order_id,$cancel=0,$order_type=0,$cancel_ordered_tool=0,$ad_id=NULL,$auditAt=1,$zonalWhChanged=0)   
{
    $CI = &get_instance();
    $orderInfo= $CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
    $transCounryId = $orderInfo['country_id'];
    $fe_check = $orderInfo['fe_check'];
    $orderedToolWorkFlow = "ordered_tools";
    $lockedAssetsWorkFlow = "locked_assets";
    $stToolOrderId = $tool_order_id;
    if($order_type == 1){
        $orderedToolWorkFlow = "st_ordered_tools";
        $lockedAssetsWorkFlow = "st_locked_assets";
        $stToolOrderId = $CI->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$orderInfo['tool_order_id']),'tool_order_id');
    }
    if($cancel_ordered_tool == 0)
        $ordered_tools = $CI->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status<'=>3));
    else
        $ordered_tools = $CI->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'ordered_tool_id'=>$cancel_ordered_tool,'status<'=>3));
    foreach ($ordered_tools as $key => $values)
    {
        $ordered_tool_id = $values['ordered_tool_id'];
        $available_quantity=$values['available_quantity'];
        $quantity= @$values['quantity'];
        $orderData = $CI->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $order_number = $orderData['order_number'];
        $customer_site_id = $orderData['current_stage_id'];

        # Audit Start 
        $auditOldData = array(
            'quantity'           => $values['quantity'],
            'available_quantity' => $values['available_quantity'],
            'tool_status'        => "Updated"
        );
        $auditNewArr = array(
            'quantity'           => NULL,
            'available_quantity' => NULL,
            'tool_status'        => ""
        );
        $finalArr = array_diff_assoc($auditNewArr, $auditOldData);
        $auditBlock = array("tool_id"=> $values['tool_id']);
        if($zonalWhChanged ==1)
            $zonalOrderedToolremarks = "Zonal Warehouse Changed";
        $auditOrderedToolId = tool_order_audit_data($orderedToolWorkFlow,$ordered_tool_id,'ordered_tool',2,$ad_id,$finalArr,$auditBlock,$auditOldData,@$zonalOrderedToolremarks,'',array(),"tool_order
            ",$tool_order_id,$transCounryId);    
        # Audit End 
        if($available_quantity > 0)
        {
            // as it was transaction quanity             
            $main_needed_locked_assets = $available_quantity;
            $locked_assets = $CI->Order_m->get_locked_assets($ordered_tool_id,$available_quantity);
            if($locked_assets)
            {
                foreach ($locked_assets as $key => $value)
                {       
                    $asset_main_status = $CI->Common_model->get_value('asset',array('asset_id'=>$value['asset_id']),'status');
                    $asset_actual_status = ($asset_main_status==2)?1:$asset_main_status;
                
                    # Audit Start
                    $order = ($orderData['order_type'] ==1)?$orderData['stn_number']:$orderData['order_number'];
                    $auditAssetRemarks = "Unlocked for ".$order;
                    assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,$lockedAssetsWorkFlow,2,$auditOrderedToolId,'','',"tool_order",$tool_order_id,$transCounryId);
                    assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,"tool_order",$stToolOrderId,$transCounryId);
                    # Audit End
                    
                    # Updating Asset
                    $assetUpdateData = array(
                        'status'        => $asset_actual_status,
                        'modified_by'   => $CI->session->userdata('sso_id'),
                        'modified_time' => date('Y-m-d H:i:s')
                    );
                    $assetUpdateWhere = array('asset_id'=>$value['asset_id']);
                    $CI->Common_model->update_data('asset',$assetUpdateData,$assetUpdateWhere);

                    # Update insert asset status history
                    $assetStatusHistoryArray = array(
                        'asset_id'          => $value['asset_id'],
                        'created_by'        => $CI->session->userdata('sso_id'),
                        'status'            => $asset_actual_status,
                        'ordered_tool_id'   => $ordered_tool_id, // gives new tool_order id
                        'current_stage_id'  => 4,
                        'tool_order_id'     => $tool_order_id
                    );
                    updateInsertAssetStatusHistory($assetStatusHistoryArray);

                    $main_needed_locked_assets--;                       
                    if($main_needed_locked_assets == 0)
                        break;
                }
            }            
        } 

        # while cancelling the tool order/st update all ordered tools
        if($cancel_ordered_tool == 0)        
        {
            $CI->Common_model->update_data('ordered_tool',array('status'=>3),array('ordered_tool_id'=>$ordered_tool_id));
        }

        if($auditAt == 1 || $auditAt == 3) 
        {
            $CI->Common_model->update_data('ordered_tool',array('status'=>3),array('ordered_tool_id'=>$ordered_tool_id));
            $auditStatus = ($auditAt == 1)?"Updated":"Removed";
            $auditUpdateData = array(
                    'tool_status'            => $auditStatus,
                    'quantity'               => $quantity,
                    'available_quantity'     => 0
            );
            updatingAuditData($auditOrderedToolId,$auditUpdateData);    
        }
        else
        {
            return $auditOrderedToolId;
        }                  
    }
    if($cancel==1 && $order_type == 1)
    {
        $CI->Common_model->update_data('tool_order',array('status'=>4,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));
        $actual_tool_order_id = $CI->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        // updating the combination with 2 because next time it should come under raise ST for Order
        $CI->Common_model->update_data('st_tool_order',array('status'=>2),array('stock_transfer_id'=>$tool_order_id,'tool_order_id'=>$actual_tool_order_id));
        $stStatus = array('current_stage_id'=>3,'status'=>4,'fe_check'=>$fe_check);
        $updateAuditData =array('current_stage'=>stockTransferStatus($stStatus));
        updatingAuditData($ad_id,$updateAuditData);
    }
}

function getListOfSTsRaisedForFEOrder($actual_tool_order_id,$ackFromWH=0)
{
    $CI = &get_instance();
    $CI->db->select('stn.*');
    $CI->db->from('tool_order stn');
    $CI->db->join('st_tool_order sto','stn.tool_order_id = sto.stock_transfer_id');
    $CI->db->where('sto.tool_order_id',$actual_tool_order_id);
    if($ackFromWH==1)
    {
        $CI->db->where('stn.fe_check',1);
        $str = '(stn.current_stage_id =2 OR ((stn.current_stage_id =3 AND stn.status = 5) OR (stn.current_stage_id =3 AND stn.status = 6) OR (stn.current_stage_id =3 AND stn.status = 10) ) )';
        $CI->db->where($str);

    }
    if($ackFromWH == 2) // to get the ordered which are raised
    {
        $CI->db->where('stn.current_stage_id',1);
    }
    $res = $CI->db->get();
    return $res->result_array();
}

function checkSTsInShipping($tool_order_id)
{
    $CI = &get_instance();
    // $CI->db->select('stock_transfer_id');
    // $CI->db->from('st_tool_order sto');        
    // $CI->db->join('tool_order to','to.tool_order_id = sto.stock_transfer_id');
    // $CI->db->where('sto.tool_order_id',$tool_order_id);
    // $CI->db->Where('to.current_stage_id',2);
    // $CI->db->or_Where('to.current_stage_id =3 AND to.status =5');
    // $CI->db->or_Where('to.current_stage_id =3 AND to.status =10');
    // //$str ='(to.current_stage_id=3 AND(to.status=5 OR to.status =10))';    
    //$CI->db->where($str);    
    $qry = "SELECT `stock_transfer_id`
                FROM `st_tool_order` `sto`
                JOIN `tool_order` `to` ON `to`.`tool_order_id` = `sto`.`stock_transfer_id`
                WHERE `sto`.`tool_order_id` = '".$tool_order_id."' 
                AND (`to`.`current_stage_id` = 2 OR (`to`.`current_stage_id` = 3 AND `to`.`status` = 5) OR (`to`.`current_stage_id` = 3 AND `to`.`status` = 10 ))  ";
    $res = $CI->db->query($qry);                

    // $res = $CI->db->get(); 
    //echo $CI->db->last_query();
    if($res->num_rows()>0)
        return 1;
    else
        return 0;
}

// updating tool order main status by passing tool order id
function get_paginationConfig2() {
    //config for bootstrap pagination class integration
        $config = array();
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['first_url'] = "#";
        $config['first_url'] = "#0";
        /*modification by prasad 16-04-2018*/
        $config['num_links'] = 10;
        return $config;
}

function decideMainOrderStatus($tool_order_id)
{
    $CI = &get_instance();
    $current_stage_id = $CI->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'current_stage_id');
    $ninthStage = 2;
    $tenthStatge = 2;
    if($current_stage_id == 8)
    {   
        // getting any returns are pending for that order
        $CI = &get_instance();
        $CI->db->select('ro.*');
        $CI->db->from('return_order ro');        
        $CI->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
        $CI->db->where('rto.tool_order_id',$tool_order_id); 
        $CI->db->where('ro.status',1);
        $res = $CI->db->get();
        if($res->num_rows()>0)
            $return_data  = 1;
        else
            $return_data = 0;
        
        if($return_data === 0) // no returns are pending for that order
        { 
            $updateData = array(
                'current_stage_id' =>9,
                'modified_by'      => $CI->session->userdata('sso_id'),
                'modified_time'    => date('Y-m-d H:i:s')
            );
            $CI->Common_model->update_data('tool_order',$updateData,array('tool_order_id'=>$tool_order_id));
            $ninthStage = 1;
        }
    }
    
    $received_assets = count(get_current_stage_involved_assets($tool_order_id,7));
    $wh_received_assets = count(get_current_stage_involved_assets($tool_order_id,10));
    $missed_assets = count(get_current_stage_involved_assets($tool_order_id,7,array(3)));
    $returned_assets = ($wh_received_assets+$missed_assets);
    if($received_assets === $returned_assets )
    {
        $updateData = array(
            'current_stage_id' => 10,
            'status'           => 10,
            'modified_by'      => $CI->session->userdata('sso_id'),
            'modified_time'    => date('Y-m-d H:i:s')
        );
        $CI->Common_model->update_data('tool_order',$updateData,array('tool_order_id'=>$tool_order_id));
        $tenthStatge = 1;
    }
    return array($ninthStage,$tenthStatge);
}
function decideMainOrder8thstage($tool_order_id)
{
    $CI = &get_instance();       
    $received_assets = count(get_current_stage_involved_assets($tool_order_id,7));
    $return_initiated_assets = count(get_current_stage_involved_assets($tool_order_id,7,array(3,4,5,6))); // missed //wh//fe2fe
    if($received_assets === $return_initiated_assets)
    {
        $CI->Common_model->update_data('tool_order',array('current_stage_id'=>8,'modified_by'     => $CI->session->userdata('sso_id'),
                                       'modified_time'    => date('Y-m-d H:i:s')),array('tool_order_id'=>$tool_order_id));
        return 1;

    }
    else
    {
        return 2;
    }
}

function getReturnInitiateAction($tool_order_id)
{
    $CI = &get_instance();
    $CI->db->select('ro.*');
    $CI->db->from('tool_order to');     
    $CI->db->join('return_tool_order rto','rto.tool_order_id = to.tool_order_id');
    $CI->db->join('return_order ro','ro.return_order_id = rto.return_order_id');
    $CI->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
    $CI->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
    $CI->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
    //$CI->db->where_in('oah.status',array(1,2));
    $CI->db->where('to.tool_order_id',$tool_order_id);
    $CI->db->where('osh.current_stage_id',7);
    $CI->db->where('ro.status',1);
    $CI->db->where_in('ro.return_approval',array(1,2));
    $CI->db->where('to.order_type',2);
    $CI->db->order_by('ro.return_order_id','DESC');
    $CI->db->limit(1);
    $res = $CI->db->get();
    //echo '<pre>';pr7int_r($res->result_array());exit;
    if($res->num_rows()>0)
        return $res->row_array();
    else
        return FALSE;
}

function validate_number($post_value)
{
    if(isset($post_value))
       return is_numeric($post_value)?$post_value:NULL;
    return NULL;
}

function validate_string($post_value)
{
    if(isset($post_value))
    {
        $post_value = filter_var($post_value,FILTER_SANITIZE_STRING);
        $post_value = trim($post_value);
        return $post_value;
    }
}

function sanitize_output($output)
{
    return htmlspecialchars(trim($output));
}

function checkSTidExist($tool_order_id)
{
   $CI = &get_instance();
   $CI->db->select('stock_transfer_id');
   $CI->db->from('st_tool_order ');        
   $CI->db->where('stock_transfer_id',$tool_order_id);
   $res = $CI->db->get(); 
   if($res->num_rows()>0)
        return 1;
    else
        return 0;
}

function checkSTorderExist($tool_order_id)
{
   $CI = &get_instance();
   $CI->db->select('tool_order_id');
   $CI->db->from('st_tool_order');        
   $CI->db->where('tool_order_id',$tool_order_id);
   $CI->db->where('status',1);
   $res = $CI->db->get(); 
   if($res->num_rows()>0)
        return 1;
    else
        return 0;
}

function checkPSFromDedicatedWh($tool_order_id)
{
   $CI = &get_instance();
   $CI->db->select('to.*');
   $CI->db->from('st_tool_order sto'); 
   $CI->db->join('tool_order to','to.tool_order_id = sto.stock_transfer_id');
   $CI->db->join('tool_order mto','mto.tool_order_id = sto.tool_order_id AND mto.wh_id = to.wh_id');
   //$CI->db->join('warehouse wh','wh.wh_id=mto.wh_id');
   $CI->db->where('sto.tool_order_id',$tool_order_id);
   // $CI->db->where('sto.status',1);
   // $CI->db->where('sto.status',1);
   //$str = '(to.current_stage_id = 3 AND (to.status >= 4 OR to.status <=6))'; 
   $str = '(to.current_stage_id = 2 OR to.current_stage_id = 1 OR ((to.current_stage_id =3 AND to.status = 5) OR (to.current_stage_id =3 AND to.status = 10) ) )';
   //$str = ' (to.current_stage_id = 3 AND to.status = 6) ';
   $CI->db->where($str);
   //$CI->db->where('mto.order_delivery_type_id',2);
   $str2 = '(`mto`.`order_delivery_type_id` = 2 OR (mto.order_delivery_type_id in (1,3) ))';
   $CI->db->where($str2);
   $res = $CI->db->get(); 
   if($res->num_rows()>0)
        return 1;
    else
        return 0;
}

function deleteUploadData()
{
    $CI = &get_instance();
    // deleting upload tool part data and tool
    $CI->Common_model->delete_data('upload_tool_part',array('upload_tool_part_id>'=>0));
    $CI->Common_model->delete_data('upload_tool',array('upload_tool_id>'=>0));

    // deleting upload tool part data and tool
    $CI->Common_model->delete_data('upload_asset_part',array('upload_asset_part_id>'=>0));
    $CI->Common_model->delete_data('upload_asset',array('upload_asset_id>'=>0));

    // deleting upload installbase
    $CI->Common_model->delete_data('upload_install_base',array('upload_install_base_id>'=>0));
    
    // deleting upload installbase
    $CI->Common_model->delete_data('upload_user',array('upload_user_id>'=>0));
}

function get_a1_warehouse_id()
{
    $CI = &get_instance();     
    $CI->db->select('l1.location_id as area_id,w.wh_id,w.wh_code as wh_code');
    $CI->db->from('location l');
    $CI->db->join('location l1','l1.parent_id = l.location_id','left');       
    $CI->db->join('warehouse w','w.location_id = l1.location_id');
    $CI->db->where('l.name','Bangalore');
    $CI->db->where('w.wh_id>',1);
    $res2 = $CI->db->get();
    //echo $CI->db->last_query();exit;
    //echo '<pre>';print_r($res2->result_array());exit;
    $wh_arr = array();
    if($res2->num_rows()>0)
    {
        foreach ($res2->result_array() as $key => $value) {
            $wh_arr [] =  $value['wh_id'];
        }
        return $wh_arr;
    }
    else
    {
        return false;
    }
}
function st_process_array()
{
    return array(4,1,2);
}
    
function get_warehouse_id_for_bs()
 { 
    $CI = &get_instance();
    $CI->db->select('w.wh_id');
    $CI->db->from('location l');//state
    $CI->db->join('location l1','l1.parent_id = l.location_id','left');//city
    $CI->db->join('location l2','l2.parent_id = l1.location_id','left');//area
    $CI->db->join('warehouse w','w.location_id = l2.location_id');
    $CI->db->where_in('l.name',array('Bangladesh','Srilanka'));
    $res2 = $CI->db->get();
    $wh_arr = array();
    if($res2->num_rows()>0)
    {
        foreach ($res2->result_array() as $key => $value) 
        {
            $wh_arr [] =  $value['wh_id'];
        }
    }
    else
    {
        $wh_arr = array(0);
    }
    return $wh_arr;
}
    
function st_asset_status_include_lock_in(){
    return array(1,2,4,5,9,10,11,12);
}

function getAssetsInCalAndRepairProcess($assets_list)
{
    $CI = &get_instance();
    $assetsInCalandRep = array();       
    $CI->db->select('rca.asset_id');
    $CI->db->from('rc_asset rca');
    $CI->db->where('rca.status',1); 
    if(count($assets_list)>0)
    {
        $CI->db->where_not_in('rca.asset_id',$assets_list);
    }       
    $CI->db->where_in('rca.current_stage_id',array(12,13,14,15,16,17,18,19,21,22,23,24,25,26,27));        
    $res = $CI->db->get();
    //echo '<pre>';print_r($res->result_array());exit;
    if($res->num_rows()>0)
    {
        foreach ($res->result_array() as $key => $value) {
            $assetsInCalandRep[] = $value['asset_id'];
        }
        //echo '<pre>';print_r($assetsInST);exit;
        return $assetsInCalandRep;
    }
    else
        return FALSE;
}

function getAssetsInTransferProcess($assets_list)
{
    $CI = &get_instance();
    $assetsInST = array();        
    $CI->db->select('oa.asset_id');
    $CI->db->from('tool_order to');
    $CI->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
    $CI->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
    $CI->db->where('oa.status',1);
    $CI->db->where('to.order_type',1);
    if(count($assets_list)>0)
    {
        $CI->db->where_not_in('oa.asset_id',$assets_list);
    }
    $CI->db->where_in('to.current_stage_id',st_process_array());  //4,1,2

    $res = $CI->db->get();
    if($res->num_rows()>0)
    {
        foreach ($res->result_array() as $key => $value) 
        {
            $assetsInST[] = $value['asset_id'];
        }
        return $assetsInST;
    }
    else
    {
        return FALSE;
    }
}

/*11-10-2018 koushik */
function check_for_st_entry($asset_id,$tool_order_id = 0)
{
    $CI = &get_instance();
    $CI->db->select('oa.asset_id,to.stn_number');
    $CI->db->from('tool_order to');
    $CI->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
    $CI->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
    if($tool_order_id!=0 && $tool_order_id!='')
    {
        $CI->db->where('to.tool_order_id !=',$tool_order_id);
    }
    $CI->db->where('oa.status',1);
    $CI->db->where('to.order_type',1);
    $CI->db->where('oa.asset_id',$asset_id);
    $CI->db->where_in('to.current_stage_id',st_process_array());  //4,1,2
    $res = $CI->db->get();
    return $res->row_array();
}
function asset_info($asset_id,$type=1) //1 is for row and 2 is for result_array
{
    $CI = &get_instance();
    $CI->db->select('t.part_number,t.tool_id,as.asset_status_id,
        t.part_description,a.asset_id,a.asset_number,as.name as status_name,p.serial_number');
    $CI->db->from('tool t');   
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->join('asset_status as','as.asset_status_id = a.status');
    $CI->db->where('a.asset_id',$asset_id);
    $CI->db->where('p.part_level_id',1);
    $CI->db->group_by('p.asset_id');
    $res = $CI->db->get();
    if($type == 1)
        return $res->row_array();
    else
        return $res->result_array();
    
}
function st_asset_status()
{
    return array(1,4,5,9,10,11,12);
}
function st_status_array()
{
   $CI = &get_instance();
   $CI->db->select('as.*');
   $CI->db->from('asset_status as');        
   $CI->db->where_in('as.asset_status_id',st_asset_status());
   $res = $CI->db->get(); 
   return $res->result_array();        
   
}
function getFEToolType($tool_id)
{
   $CI = &get_instance();
   $CI->db->select('t.tool_type_id ');
   $CI->db->from('tool t');        
   $CI->db->where('t.tool_id',$tool_id);
   $res = $CI->db->get(); 
   $result = $res->row_array();        
   return $result['tool_type_id'];       
}
function order_info($tool_order_id)
{
   $CI = &get_instance();
   $CI->db->select('*');
   $CI->db->from('tool_order');        
   $CI->db->where('tool_order_id',$tool_order_id);
   $res = $CI->db->get(); 
   return  $res->row_array();
}
function getWhIncludingBuffer()
{
   $CI = &get_instance();
   $CI->db->select('concat(wh.wh_code," - (" ,wh.name,")") as name,wh.wh_id ');
    $CI->db->from('warehouse wh');        
    $CI->db->where_in('wh.status',array(1,3));
    $res = $CI->db->get(); 
    return $res->result_array();        
   
}
function getWhCodeAndName($wh_id)
{
    $CI = &get_instance();
    $CI->db->select('concat(wh.wh_code, " - (",wh.name,")") as wh_name');
    $CI->db->from('warehouse wh');
    $CI->db->where('wh.wh_id',$wh_id);
    $CI->db->where('wh.status',1);
    $res = $CI->db->get(); 
    $result = $res->row_array();
    return $result['wh_name'] ;
   
}
function getWhArrNotInSession()
{
    $CI = &get_instance();  
    $arr = array(1,$_SESSION['s_wh_id']);
    $CI->db->select('wh.wh_id,concat(wh.wh_code," - (",wh.name,")") as name ');
    $CI->db->from('warehouse wh');
    $CI->db->where_not_in('wh.wh_id',$arr);
    $CI->db->where('status',1);
    $res = $CI->db->get();
    return $res->result_array();
}

function getUsersByZone($zone_id)
{   
    $CI = &get_instance();              
    $whs = getWarehouseByLocationID($zone_id);        
    if(@$whs)
    {   
        $wh_arr = array();
        foreach ($whs as $key => $value)
        {
            $wh_arr[] = $value['wh_id'];
        }
        $role_arr = getTaskRoles('raise_order');
        $CI->db->select('*');
        $CI->db->from('user u');
        $CI->db->where_in('u.wh_id',$wh_arr);
        $CI->db->where_in('u.role_id',$role_arr);
        $res = $CI->db->get();            
        if($res->num_rows()>0)
            $results = $res->result_array();
        else
            $results = FALSE;            
    }
    else
    {
        $results = FALSE;
    }
    return $results;
    
}
function getWHorZoneModalityDataWithStatus($searchParams3,$modality_id)
{
    $CI = &get_instance(); 
    if($searchParams3['third_level_type'] == 1)
    {
        $whs = getWarehouseByLocationID($searchParams3['third_level_id']);     
    }
    else
    {
        $whs = array($searchParams3['third_level_id']);
    }        
    if(count($whs)>0)
    {   
        // 1 for multiple warehouse 
        if($searchParams3['third_level_type'] == 1)
        {
            $wh_arr = array();
            foreach ($whs as $key => $value) 
            {   
                $wh_arr[] = $value['wh_id'];
            } 
        }            
        else
        {
            $wh_arr = array($searchParams3['third_level_id']);
        }            
        $CI->db->select('a.status,as.name as status_name,count(*) as count');
        $CI->db->from('asset a');
        $CI->db->join('asset_status as','a.status = as.asset_status_id');
        $CI->db->join('part p','p.asset_id = a.asset_id');
        $CI->db->join('tool t','t.tool_id = p.tool_id');
        $CI->db->where_in('a.wh_id',$wh_arr);
        $CI->db->where('as.type',$searchParams3['r_status']);
        $CI->db->where('a.modality_id',$modality_id);
        $CI->db->where('p.part_level_id',1);
        if(@$searchParams3['tool_type']!='')    
            $CI->db->where('t.tool_type_id',$searchParams3['tool_type']);
        $CI->db->group_by('a.status');
        $res = $CI->db->get();
        if($res->num_rows() > 0)
            return $res->result_array();
        else
            return FALSE;
    }
}
function getWHorZoneStatusData($location_id,$type,$wh_id,$searchParams2)
{
    // get warehouse in location       
    $CI = &get_instance(); 
    if($type == 1)
    {
        $whs = getWarehouseByLocationID($location_id);            
    }
    else
        $whs = array($wh_id);        
    if(count($whs)>0)
    {   
        // 1 for multiple warehouse 
        if($type == 1)
        {
            $wh_arr = array();
            foreach ($whs as $key => $value) 
            {   
                $wh_arr[] = $value['wh_id'];
            } 
        }            
        else
        {
            $wh_arr = array($wh_id);
        }            
        $CI->db->select('a.status,as.name as status_name,count(*) as count');
        $CI->db->from('asset a');
        $CI->db->join('asset_status as','a.status = as.asset_status_id');
        $CI->db->join('part p','p.asset_id = a.asset_id');
        $CI->db->join('tool t','t.tool_id = p.tool_id');
        $CI->db->where('p.part_level_id',1);
        $CI->db->where_in('a.wh_id',$wh_arr);
        $CI->db->where('as.type',$searchParams2['r_status']);
        if($searchParams2['r_modality_id']!='')
            $CI->db->where('a.modality_id',$searchParams2['r_modality_id']);
        if($searchParams2['r_tool_type']!='')
            $CI->db->where('t.tool_type_id',$searchParams2['r_tool_type']);
        $CI->db->group_by('a.status');

        $res = $CI->db->get();            
        if($res->num_rows() > 0)
            return $res->result_array();
        else
            return FALSE;
    }
    else
    { 
        return FALSE;
    }
}
function getActiveOrInactiveCount($searchParams)
{
    $CI = &get_instance();
    if(@$searchParams['r_zone']!='')
    {  
        // For Zones
        if(@$searchParams['r_zone'] != 1)
        {   
            // for perticular warehouse
            if(@$searchParams['r_wh_id']!='')
            {
                $warehouse_where = 1;                    
            }
            else
            { 
                $whs = getWarehouseByLocationID($searchParams['r_zone']);
                // getting all warehouses and making as array
                if(count($whs)>0)
                {
                    $wh_arr = array();
                    foreach ($whs as $key => $value) {
                        $wh_arr[] = $value['wh_id'];
                    }
                } 
                else
                {
                    $wh_arr = array(0);
                } 
            }
        } 
    }
    
    $CI->db->select('as.type,count(*) as count');
    $CI->db->from('asset a');
    $CI->db->join('asset_status as','a.status = as.asset_status_id');
    $CI->db->join('part p','p.asset_id = a.asset_id');
    $CI->db->join('tool t','t.tool_id = p.tool_id');
    if(@$searchParams['r_zone']!='')
    {
        // for All
        if($searchParams['r_zone'] == 1)
        {
            // for perticular warehouse
            if(@$searchParams['r_wh_id']!='')
            {
                $CI->db->where('a.wh_id',$searchParams['r_wh_id']);
            }
        }            
        else
        {
            if(@$warehouse_where == 1)
            {
                $CI->db->where('a.wh_id',$searchParams['r_wh_id']);
            }
            else
            {
                $CI->db->where_in('a.wh_id',$wh_arr);
            }
        }
    }
    if($searchParams['r_modality_id']!='')
        $CI->db->where('a.modality_id',$searchParams['r_modality_id']);
    if(@$searchParams['r_tool_type']!='')
        $CI->db->where('t.tool_type_id',$searchParams['r_tool_type']);
    $CI->db->where('a.wh_id>',1);
    $CI->db->where('p.part_level_id',1);
    $CI->db->group_by('as.type');
    $res = $CI->db->get();        
    return $res->result_array();
}
function getWHorZonedata($searchParams)
{
    $CI = &get_instance();                
    // for All
    if($searchParams['r_zone'] == 1)
    { 
        if($searchParams['r_wh_id']!='')  
        { 
            $single_wh =  $CI->Common_model->get_data('warehouse',array('wh_id'=>$searchParams['r_wh_id']));
            return array($single_wh,2); // 2 is For Warehosue
        }
        else
        {

           $location_loop = $CI->Common_model->get_data('location',array('level_id'=>3));
           return array($location_loop,1); // 1 is For Locations
        }
    }        
    else
    {
        if($searchParams['r_wh_id']!='')  
        { 
            $single_wh =  $CI->Common_model->get_data('warehouse',array('wh_id'=>$searchParams['r_wh_id']));
            return array($single_wh,2); // 2 is For Warehosue
        }
        else
        {
            $whs = getWarehouseByLocationID($searchParams['r_zone']);                
            if(count($whs)>0)
            {
                return array($whs,2); // 2 is for warehouse
            } 
            else
            {
                return array();
            } 
        }
    }        
}

function calibrationRequiredTools()
{   
    $CI = &get_instance();
    $CI->load->model('Calibration_m');
    $status=array(1,2);
    $CI->db->select('to.*');
    $CI->db->from('tool_order to');
    $CI->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
    $CI->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');
    $CI->db->where_in('oah.status',$status);
    $CI->db->where('to.current_stage_id',7);
    if($_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6)
        $CI->db->where('to.sso_id',$_SESSION['sso_id']);
    $CI->db->group_by('to.tool_order_id');
    $CI->db->order_by('to.tool_order_id ASC');
    $res = $CI->db->get();
    if($res->num_rows()>0)
    {
        $calibration_results =  $res->result_array();    
        $cal_orders = array();
        $final_cal_required_tools = array();
        foreach($calibration_results as $key =>$value)
        {
            $cal_required_tools=$CI->Calibration_m->cal_required_tools($value['tool_order_id']);                
            foreach($cal_required_tools as  $row1)
            {
                $final_cal_required_tools[$value['tool_order_id']][] = $row1;                                     
            }
        }
        return count($final_cal_required_tools);
    }
    else
    {
        return 0;
    }
}
    // notification count helpers
        function fePositionChange($searchParams)
        {
            $CI = &get_instance();
            $CI->load->model('Fe_position_change_m');
            return $CI->Fe_position_change_m->fe_position_change_total_num_rows(@$searchParams);
        }

        function upcomingCalibration()
        {
            $CI = &get_instance();
            $CI->load->model('Calibration_m');
            return $CI->Calibration_m->calibration_total_num_rows(@$searchParams); 
        }

        function crossedExpectedDeliveryDate()
        {
            $CI = &get_instance();
            $CI->load->model('Alerts_m');
            return $CI->Alerts_m->crossed_expected_delivery_total_num_rows(@$searchParams); 
        }
        
        function pendingCalibrationDeliveries()
        {
            $CI = &get_instance();
            $CI->load->model('Calibration_m');
            return $CI->Calibration_m->wh_calibration_total_num_rows(@$searchParams); 
        }
        function ackFromWH()
        {
            $CI = &get_instance();
            $CI->load->model('Stock_transfer_m');
            return $CI->Stock_transfer_m->wh2_wh_receive_order_total_num_rows(@$searchParams);
        }
        function ackFromFE()
        {
            $CI = &get_instance();
            $CI->load->model('Pickup_point_m');
            return $CI->Pickup_point_m->open_fe_returns_list_total_num_rows(@$searchParams);
        }
        function pendingSTDeliveries()
        {
            $CI = &get_instance();
            $CI->load->model('Wh_stock_transfer_m');
            return $CI->Wh_stock_transfer_m->st_total_num_rows(@$searchParams); 
        }
        function pendingFEDeliveries()
        {
            $CI = &get_instance();
            $CI->load->model('Fe_delivery_m');
            return $CI->Fe_delivery_m->fe_delivery_list_total_num_rows(@$searchParams);
        }
        function toolsTobeReturn()
        {
            $CI = &get_instance();
            $CI->load->model('Order_m');
            return $CI->Order_m->owned_order_total_num_rows(@$searchParams);
        } 
        function toolsTobeReceiveFromFE()
        {
            $CI = &get_instance();
            $CI->load->model('Fe2fe_m');
            return $CI->Fe2fe_m->fe2_fe_receive_order_total_num_rows(@$searchParams);
        }
        function toolsTobeReceiveFromWH()
        {
            $CI = &get_instance();
            $CI->load->model('Order_m');
            $CI->load->model('Stock_transfer_m');
            $asOrder = $CI->Order_m->receive_order_total_num_rows(@$searchParams);            
            // stock transfers
            $data['orderResults2'] = $CI->Order_m->raisedSTforOrderResults(@$searchParams);            
            $asST = 0;            
            if(count($data['orderResults2'])>0)
            {
                foreach ($data['orderResults2'] as $key => $value)
                {
                    $StockTransfers = count($CI->Stock_transfer_m->stnForOrder($value['tool_order_id'],1,1));             
                    $asST = $asST + $StockTransfers;
                }
            }
            $ackAlerts = ($asOrder + $asST);
            return $ackAlerts;
        }
        function fe2_fe_transfer_requests()
        {
            $CI = &get_instance();
            $CI->load->model('Fe2fe_m');
            return $CI->Fe2fe_m->fe2_fe_approval_order_total_num_rows(@$searchParams);
        }
        function missed_asset_total_num_rows()
        {
            $CI = &get_instance();
            $CI->load->model('Defective_asset_m');
            return $CI->Defective_asset_m->missed_asset_total_num_rows(@$searchParams);
        }
        function defective_asset_total_num_rows()
        {
            $CI = &get_instance();
            $CI->load->model('Defective_asset_m');
            return $CI->Defective_asset_m->defective_asset_total_num_rows(@$searchParams);
        }
        function addressChangeTotalNumRows($searchParams)
        {
            $CI = &get_instance();
            $CI->load->model('Alerts_m');
            return $CI->Alerts_m->addressChangeTotalNumRows(@$searchParams);            
        }
    // End Of Admin and FE Notifications
    //Log Notifications
        function pendingCalibrationDeliveries_admin()
        {
            $CI = &get_instance();
            $CI->load->model('Calibration_m');
            return $CI->Calibration_m->wh_calibration_total_num_rows_admin(); 
        }

        function calibration_count($current_stage_id)
        {
            $CI = &get_instance();
            $CI->load->model('Calibration_m');
            return $CI->Calibration_m->calibration_count_notification($current_stage_id); 
        }

        function ack_cal_tools()
        {
            $CI = &get_instance();
            $CI->load->model('Calibration_m');
            return $CI->Calibration_m->vendor_calibration_total_num_rows(@$searchParams);
        }

        function ack_repair_tools()
        {
            $CI = &get_instance();
            $CI->load->model('Repair_m');
            return $CI->Repair_m->wh_vendor_repair_total_num_rows(@$searchParams);
        }
    // End of Logistics         

function exceededOrderDurationTotalNumRows($searchParams,$task_access)
{
    $CI = &get_instance();
    $CI->db->select('to.*,odt.name as order_type');
    $CI->db->from('tool_order to');               
    $CI->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');        
    $CI->db->join('order_extended_days oed','oed.tool_order_id = to.tool_order_id');
    if(@$searchParams['order_number']!='')
        $CI->db->like('to.order_number',$searchParams['order_number']);
    if(@$searchParams['order_delivery_type_id']!='')
        $CI->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
    if(@$searchParams['deploy_date']!='')
        $CI->db->where('to.deploy_date>=',$searchParams['deploy_date']);
    if(@$searchParams['return_date']!='')
        $CI->db->where('to.return_date<=',$searchParams['return_date']);    
    if($task_access==1)
    {
        $CI->db->where('to.sso_id',$CI->session->userdata('sso_id'));
    }
    elseif($task_access == 2)
    {
        $CI->db->where('to.country_id',$CI->session->userdata('s_country_id'));
        if($searchParams['users_id']!='')
        {
            $CI->db->where('to.sso_id',$searchParams['users_id']);
        }
    }
    else if($task_access == 3)
    {
        if($_SESSION['header_country_id']!='')
        {
            $CI->db->where('to.country_id',$_SESSION['header_country_id']);   
        }
        else
        {
            if($searchParams['country_id']!='')
            {
                $CI->db->where('to.country_id',$searchParams['country_id']);
            }
            else
            {
                $CI->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
            }
        }
        if($searchParams['users_id']!='')
        {
            $CI->db->where('to.sso_id',$searchParams['users_id']);
        }
    }               
    $CI->db->where('to.days_approval',1);
    $CI->db->where('oed.status',2);
    $res = $CI->db->get();    
    return $res->num_rows();
}

function get_logistic_users($wh_id=0)
{    
    $CI = & get_instance(); 
    if($wh_id!=0)
        $CI->db->select('u.*');
    else
        $CI->db->select('u.sso_id,concat(u.sso_id,"-",u.name," -(",wh.wh_code," )") as name');
    $CI->db->from('user u'); 
    $CI->db->join('warehouse wh','wh.wh_id = u.wh_id');
    $CI->db->where_in('u.role_id',3);
    if($wh_id!=0)
    {
        $CI->db->join('user_wh uwh','uwh.sso_id=u.sso_id','left');
        $str = '(u.wh_id = '.$wh_id.'  OR (uwh.wh_id = '.$wh_id.' AND uwh.status = 1) )' ;
        $CI->db->where($str);
    }
    $CI->db->where('u.status',1);
    $CI->db->group_by('u.sso_id');
    $res = $CI->db->get();
    return $res->result_array();
}

function get_admins($country_id=2) // by default to avaoid error passing 2
{    
    $CI = & get_instance(); 
    $CI->db->select('u.*');
    $CI->db->from('user u'); 
    $CI->db->where('u.status',1);
    $CI->db->where_in('u.role_id',array(1,4));
    $CI->db->where('u.country_id',$country_id);
    $res = $CI->db->get();
    return $res->result_array();
}

function get_current_stage_involved_assets_details($tool_order_id,$current_stage_id,$oah_status=array(),$type=1)
{   // 1 for details and 2 for count of assets according to the tool
    $CI = &get_instance();
    if($type == 1)
        $CI->db->select('*');
    else
        $CI->db->select('t.tool_id,t.part_description,t.part_number,count(*) as qty,ot.quantity as ordered_qty');
    $CI->db->from('order_status_history osh');
    $CI->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
    $CI->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
    $CI->db->join('asset a','a.asset_id = oa.asset_id');
    $CI->db->join('part p','p.asset_id = a.asset_id');
    $CI->db->join('tool t','t.tool_id = p.tool_id');
    $CI->db->join('tool_order to','to.tool_order_id =osh.tool_order_id');
    $CI->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id AND ot.tool_id = t.tool_id');
    $CI->db->join('user u','u.sso_id= to.sso_id');
    $CI->db->where('p.part_level_id',1);
    $CI->db->where('oa.status<',3);
    $CI->db->where('osh.tool_order_id',$tool_order_id);
    $CI->db->where('osh.current_stage_id',$current_stage_id);
    if(count(@$oah_status)!= 0)
    {
        $CI->db->where_in('oah.status',$oah_status);
    }
    if($type == 2)
        $CI->db->group_by('t.tool_id');
    $res = $CI->db->get();
    return $res->result_array();    
}
function get_current_stage_involved_assets($tool_order_id,$current_stage_id,$oah_status=array())
{
    $CI = &get_instance();
    $CI->db->select('oah.*');
    $CI->db->from('order_status_history osh');
    $CI->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
    $CI->db->where('osh.tool_order_id',$tool_order_id);
    $CI->db->where('osh.current_stage_id',$current_stage_id);
    if(count(@$oah_status)!= 0)
    {
        $CI->db->where_in('oah.status',$oah_status);
    }
    $res = $CI->db->get();
    return $res->result_array();
}

  // $associate array = 1 means return  me the associate array
function get_whs_string($searchParams,$type=0,$task_access=1,$associateArray=0,$country_id=0) // 0 for to get string other than that arr
{
    $CI = & get_instance();
    // get the warehouses for user
    $role_id = getRoleByUser($searchParams['order_sso_id']);
    $sso_id = $searchParams['order_sso_id'];
    if($country_id===0)
    {
        if($task_access == 1 && $role_id !=5 && $role_id !=6 )
        {
            $wh_ids = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'wh_id');
        }
        else
        {
            if($role_id == 5 || $role_id == 6)
                $location_id = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'location_id');
            else
            {
                if($task_access == 2)
                {
                    $location_id = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'country_id');
                }
                else
                {
                    $location_id = getCountryListForTransaction($sso_id,$task_access); // return will be array         
                }
            }            
            $whs = getWarehouseByLocationID($location_id);
            if($associateArray!=0)  // to use in common models because I am already have the function to convert into indexed array
            {
                return $whs;
            }
            $wh_ids = array();
            foreach ($whs as $key => $value)
            {
                $wh_ids[] = $value['wh_id'];
            }
            if($type == 0)
            {
                $wh_ids = implode(',', $wh_ids);
            }
        }
    }
    else
    {
        $whs = getWarehouseByLocationID($country_id);
        if($associateArray!=0)  // to use in common models because I am already have the function to convert into indexed array
        {
            return $whs;
        }
        $wh_ids = array();
        foreach ($whs as $key => $value)
        {
            $wh_ids[] = $value['wh_id'];
        }
        if($type == 0)
        {
            $wh_ids = implode(',', $wh_ids);
        } 
    }
    return $wh_ids;
}

function available_assets_for_order($searchParams)
{
    $CI = & get_instance();
    // get the warehouses for user    
    $role_id = getRoleByUser($searchParams['order_sso_id']);
    $sso_id = $searchParams['order_sso_id'];
    if($role_id == 2)
    {
        $wh_ids = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'wh_id');
    }
    else
    {
        $location_id = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'location_id');
        $whs = getWarehouseByLocationID($location_id);
        $wh_ids = array();
        foreach ($whs as $key => $value)
        {
            $wh_ids[] = $value['wh_id'];
        }
    }


    $CI->db->select('t.*,a.*');
    $CI->db->from('tool t');   
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->join('asset_position ap','ap.asset_id = a.asset_id');
    $CI->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
    $CI->db->where('t.tool_id',$searchParams['tool_id']);
    $CI->db->where_in('a.wh_id',$wh_ids);  
    $CI->db->where('ap.to_date IS NULL');
    $CI->db->where('ap.wh_id IS NOT NULL');  
    $CI->db->group_by('p.asset_id');
    $CI->db->where('a.status',1);
    $CI->db->where('ap.to_date IS NULL');
    $CI->db->where('ap.wh_id IS NOT NULL');  
    $CI->db->where('p.part_level_id',1);
    $CI->db->where('a.approval_status',0);
    $res = $CI->db->get();
    //echo $CI->db->last_query();exit;
    return $res->result_array();
}
function availableAssetsForZonal($searchParams)
{
    $CI = & get_instance();
    // get the warehouses for user    
    $role_id = getRoleByUser($searchParams['order_sso_id']);
    $sso_id = $searchParams['order_sso_id'];
    if($role_id == 2)
    {
        $wh_ids = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'wh_id');
    }
    else
    {
        $location_id = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'location_id');
        $whs = getWarehouseByLocationID($location_id);
        $wh_ids = array();
        foreach ($whs as $key => $value)
        {
            $wh_ids[] = $value['wh_id'];
        }
    }
    $whAndQty = array();
    foreach ($whs as $key => $value) {
        $CI->db->select('t.*,a.*');
        $CI->db->from('tool t');   
        $CI->db->join('part p','p.tool_id = t.tool_id');
        $CI->db->join('asset a','a.asset_id = p.asset_id');
        $CI->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
        $CI->db->where('t.tool_id',$searchParams['tool_id']);
        $CI->db->where('a.wh_id',$value['wh_id']);          
        $CI->db->group_by('p.asset_id');
        $CI->db->where('a.status',1);
        $CI->db->where('p.part_level_id',1);
        $CI->db->where('a.approval_status',0);
        $res = $CI->db->get();
        if($res->num_rows()>0)
        {
            $arr = array(
                    'wh_name' => $value['wh_code'].'-( '.$value['name'].' )',
                    'quantity'  => $res->num_rows()
                );
            $whAndQty[] = $arr; 
        }
    }
    if(count($whAndQty))
        return $whAndQty;
    else
        return FALSE;
}

function available_assets($searchParams,$assets_arry=array())
{
    $CI = & get_instance();
    $CI->db->select('t.*,a.*,a.status as asset_main_status');
    $CI->db->from('tool t');   
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->join('asset_position ap','ap.asset_id = a.asset_id');
    $CI->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
    $CI->db->where('ap.to_date IS NULL');
    $CI->db->where('ap.wh_id IS NOT NULL');
    if(@$searchParams['tool_id']!='')
        $CI->db->where('t.tool_id',$searchParams['tool_id']);
    if(@$searchParams['wh_id']!='')
        $CI->db->where('a.wh_id',$searchParams['wh_id']); 
    $CI->db->where('ap.to_date IS NULL');
    $CI->db->where('ap.wh_id IS NOT NULL');  
    $CI->db->group_by('p.asset_id');
    $CI->db->where('a.status',1);
    $CI->db->where('p.part_level_id',1);
    $CI->db->where('a.approval_status',0);
    if(count($assets_arry)>0)
        $CI->db->or_where_in('a.asset_id',$assets_arry);
    $res = $CI->db->get();    
    return $res->result_array();
}
function available_assets_for_transfer($searchParams)
{   
    $CI = & get_instance();
    $CI->db->select('t.*,a.*');
    $CI->db->from('tool t');   
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->join('asset_position ap','ap.asset_id = a.asset_id');
    $CI->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
    $CI->db->where('ap.to_date IS NULL');
    $CI->db->where('ap.wh_id IS NOT NULL');
    if(@$searchParams['tool_id']!='')
        $CI->db->where('t.tool_id',$searchParams['tool_id']);
    if(@$searchParams['wh_id']!='')
        $CI->db->where('a.wh_id',$searchParams['wh_id']);    
    $CI->db->group_by('p.asset_id');
    $CI->db->where('ap.to_date IS NULL');
    $CI->db->where('ap.wh_id IS NOT NULL');  
    $CI->db->where_in('a.status',st_asset_status());
    $CI->db->where('p.part_level_id',1);
    $res = $CI->db->get();
    return $res->result_array();
}
function get_asset_data($asset_id)
{
    $CI = & get_instance();
    $CI->db->select('t.*,p.serial_number,p.quantity as parts_quantity,pl.name as part_level_name');
    $CI->db->from('tool t');   
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->join('part_level pl','pl.part_level_id =p.part_level_id ');      
    $CI->db->where('a.asset_id',$asset_id);   
    $CI->db->group_by('t.tool_id');
    $res = $CI->db->get();
    return $res->result_array();   
}
function tool_assets($searchParams)
{
    $CI = & get_instance();
    $CI->db->select('t.*,a.*');
    $CI->db->from('tool t');   
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');    
    if($searchParams['tool_id']!='')
        $CI->db->where('t.tool_id',$searchParams['tool_id']);    
    if($searchParams['country_id']!='')
        $CI->db->where('t.country_id',$searchParams['country_id']); 
    $CI->db->group_by('p.asset_id');   
    $CI->db->where('p.part_level_id',1); 
    $res = $CI->db->get();
    return $res->result_array();
}
function get_tool_information($tool_id)
{
    $CI = & get_instance();
    $CI->db->select('t.part_number,t.part_description,pl.name as part_level_name,tp.quantity as parts_quantity');
    $CI->db->from('tool t');
    $CI->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
    $CI->db->join('part_level pl','pl.part_level_id = t.part_level_id');
    $CI->db->join('tool_part tp','tp.tool_sub_id = t.tool_id');
    $CI->db->where('t.tool_id',$tool_id);
    $res1=$CI->db->get();
    return $res1->row_array();
}
function get_main_tool_information($tool_id)
{
    $CI = & get_instance();
    $CI->db->select('t.part_number,t.part_description,pl.name as part_level_name');
    $CI->db->from('tool t');
    $CI->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
    $CI->db->join('part_level pl','pl.part_level_id = t.part_level_id');
    $CI->db->where('t.tool_id',$tool_id);
    $res1=$CI->db->get();
    return $res1->row_array();
}

function getUserDetails($sso_id = 0)
{
    $CI = & get_instance();
    $dis = array(1,3);//for active users and admin,superadmin
    $CI->db->select('u.*, concat(u.sso_id, " -( " ,u.name," )") as name,b.name as branch_name, d.name as designation_name, r.name as role_name');
    $CI->db->from('user u');
    $CI->db->join('role r','u.role_id = r.role_id');
    $CI->db->join('designation d','d.designation_id = u.designation_id');
    $CI->db->join('branch b','b.branch_id = u.branch_id');
    $CI->db->where('u.sso_id',$sso_id);
    $CI->db->where('d.status',1);
    $CI->db->where('b.status',1);
    $CI->db->where('r.status',1);
    $CI->db->where_in('u.status',$dis);
    $res = $CI->db->get();
    if($res->num_rows() > 0)
    {
        $ret = $res->row_array();
    }
    else $ret = array();
    return $ret;
}

function getNameBySSO($sso_id)
{
    $CI = & get_instance(); 
    $CI->db->select('concat(u.sso_id," - (",u.name,")") as name');
    $CI->db->from('user u'); 
    $CI->db->where('u.sso_id',$sso_id);
    $res = $CI->db->get();
    $result = $res->row_array();
    return $result['name'];
}
function getLocationBySSO($sso_id)
{
    $CI = & get_instance(); 
    $CI->db->select('l.name');
    $CI->db->from('user u'); 
    $CI->db->join('location l','l.location_id=u.fe_position');
    $CI->db->where('u.sso_id',$sso_id);
    $res = $CI->db->get();
    $result = $res->row_array();
    return $result['name'];
}

function getRoleByUser($sso_id)
{
    $CI = & get_instance();
    $userDetails = getUserDetails($sso_id);    
    return $userDetails['role_id'];
}

function getWHByUser($sso_id)
{
    $CI = & get_instance();
    $userDetails = getUserDetails($sso_id);
    return $userDetails['wh_id'];
}

function getUserDetailsByUser($sso_id)
{
    $CI = & get_instance();
    $userDetails = getUserDetails($sso_id);
    return $userDetails;
}
function kit_array()
{
    $kit_array = array(
        array("id"=>1,"name"=>"Yes"),
        array("id"=>2,"name"=>"No")
    );
    return $kit_array;              
}
function st_type()
{
    $kit_array = array(
        array("id"=>2,"name"=>"For FE Order"),
        array("id"=>1,"name"=>"For WH to WH")
        );
    return $kit_array;              
}
function ack_from()
{
    $kit_array = array(
        array("id"=>1,"name"=>"Warehouse"),
        array("id"=>2,"name"=>"FE")
    );
    return $kit_array;              
}
function closed_order_status()
{
    $closed_status = array(
        array("status"=>10,"name"=>"Successfully Closed"),
        array("status"=>4,"name"=>"Cancelled")
    );
    return $closed_status;              
}
function get_closed_order_status($status)
{
    if($status == 1) return 'Successfully Closed';
    if($status == 3) return 'Partially Closed';
    if($status == 10) return 'Forcefully Closed';
}
function get_warehouses_under_location($location_id,$html=0,$sso_id,$transactionCountry=0)
{
    $CI = & get_instance();    
    $role_id = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'role_id');
    if($html == 1)
    {
        if($role_id == 2)
        {
            
            $wh_id = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'wh_id');    
            
            $wh_data = $CI->Common_model->get_data('warehouse',array('wh_id'=>$wh_id));
            $single_warehouse ='';
            $single_warehouse="<option value=''>-Select Warehosue- </option>";

            if(count($wh_data)>0)
            {
                foreach($wh_data as $wh)
                {
                    $single_warehouse.='<option value="'.$wh['wh_id'].'">'.$wh['wh_code'].'- ('.$wh['name'].' )'.'</option>';      
                }
            }  
            else
            {
                $single_warehouse.="<option value=''>-No Data Found-</option>";
            }
            return $single_warehouse;
        }
        else
        {
            
            $results = getWarehouseByLocationID($location_id);    
            
            $warehouse_options="<option value=''>-Select Warehosue- </option>";
            if(count($results)>0)
            {
                foreach($results as $wh)
                {
                    $warehouse_options.='<option value="'.$wh['wh_id'].'">'.$wh['name'].'</option>';      
                }
            }  
            else
            {
                $warehouse_options.="<option value=''>-No Data Found-</option>";
            }
            return $warehouse_options;           
        }
    }
    else
    {
        $results = getWarehouseByLocationID($location_id);        
        return $results;        
    }
}

function getWarehouseByLocationID($location_id) // soome times variable and some times array
{
    $CI = & get_instance();    
    if(is_numeric($location_id))
        $territory_level = $CI->Common_model->get_value('location',array('location_id'=>@$location_id),'level_id');       
    if(@$territory_level == 3)
    {    
        $CI->db->select('l.location_id, l.name, l1.name as city_name,l3.location_id as zone');
        $CI->db->from('location l');
        $CI->db->join('location l1','l1.location_id = l.parent_id','left');
        $CI->db->join('location l2','l2.location_id = l1.parent_id','left');
        $CI->db->join('location l3','l3.location_id = l2.parent_id','left');                 
        $CI->db->where('l3.location_id', $location_id);         
        $res = $CI->db->get();
        if($res->num_rows()>0)
            $child_locations = $res->result_array();
        else 
            $child_locations = array();
    }
    else
    { 
        $CI->db->select('l.location_id, l.name, l1.name as city_name,l4.location_id as country_id');
        $CI->db->from('location l');
        $CI->db->join('location l1','l1.location_id = l.parent_id','left');
        $CI->db->join('location l2','l2.location_id = l1.parent_id','left');
        $CI->db->join('location l3','l3.location_id = l2.parent_id','left'); 
        $CI->db->join('location l4','l4.location_id = l3.parent_id','left');                 
        if(is_numeric($location_id))
        {
            $CI->db->where('l4.location_id', $location_id);         
        }
        else
        {            
            $CI->db->where_in('l4.location_id', $location_id);         
        }
        $res = $CI->db->get();
        if($res->num_rows()>0)
            $child_locations = $res->result_array();
        else 
            $child_locations = array();
    }
    $location_array =array();
    if(count($child_locations)>0)
    {
        foreach ($child_locations as $key => $value) 
        {
           $location_array[] = $value['location_id'];
        }        
    }
    else
    {
        $location_array = array(0);
    } 
    $child_locations_string = implode(',', $location_array);
        $qry = 'SELECT w.* FROM warehouse as w
                INNER JOIN location as l ON w.location_id = l.location_id
                WHERE l.location_id IN ('.$child_locations_string.') AND w.status =1
                ';
    $res = $CI->db->query($qry);
    return $res->result_array();
}

function get_supplier_type()
{
    $sup_arr = array
        (
            array("sup_type_id"=>1,"sup_type_name"=>"Supplier"),
            array("sup_type_id"=>2,"sup_type_name"=>"Calibration Supplier")
        );
    return $sup_arr;              
}
function calibration_type()
{
    $cal_array = array
        (
            array("calibration_id"=>1,"calibration_name"=>"Provide"),
            array("calibration_id"=>2,"calibration_name"=>"Not Provide")
        );
    return $cal_array;              
}
function asset_status_array()
{
    $st_arr = array
        (
            array("status"=>"G","name"=>"Good"),
            array("status"=>"D","name"=>"Defective")
        );
    return $st_arr;              
}

function asset_document_path()
{
    return 'uploads/asset_upload/';
}
function order_document_path()
{
    return 'uploads/order_documents/';
}
function get_asset_upload_url()
{
    return SITE_URL1.'uploads/asset_upload/';
}
function get_file_extension($file_name)
{
    if($file_name!='')
    {
        $arr = explode('.',$file_name);
        return $arr[count($arr)-1];
    }
}

function get_current_serial_number($modality_id,$country_id)
{
    $CI = & get_instance();
    $CI->db->select('max(a.running_sno) as csno');
    $CI->db->from('asset a');
    $CI->db->where('a.modality_id',$modality_id);
    $CI->db->where('a.country_id',$country_id);
    $res = $CI->db->get();
    if($res->num_rows()>0)
    {
        $row = $res->row_array();
        return $row['csno']+1;
    }
    else
    {
        return 1;
    }
}
function get_running_sno_five_digit($serial)
{
    $count = strlen($serial);
    $zero_count = 5-$count;
    $sno = '';
    while($zero_count > 0)
    {
        $sno.='0';
        $zero_count--;
    }
    $sno.=$serial;
    return $sno;
}
function template_asset_document_path()
{
    return 'samples/';
}
function get_calibration_supplier()
{
    $CI = & get_instance();
    $qry = 'SELECT * FROM supplier where  calibration = 1';
    $res = $CI->db->query($qry);
    return $res->result_array();
}

// inserting into upload tool table with remarks
function wge_tool_failed_transaction($remarks_string,$eq_model_ids,$first_row,$upload_id,$remarks_string2)
{
    $CI = & get_instance();
    $eq_model_string =''; $e= 0;
    foreach ($eq_model_ids as $key => $value){                          
        if($e==0){
            $eq_model_string = $value;
            $e++;
            continue;
        }
        $eq_model_string = $eq_model_string.'|'.$value;
    }
    $data = array(
        'part_number'        => $first_row['part_number'],
        'tool_code'          => $first_row['tool_code'],
        'part_description'   => $first_row['part_description'],                            
        'remarks'            => @$remarks_string.@$remarks_string2,
        'upload_id'          => $upload_id,
        'eq_model'           => $eq_model_string
        );
        
    $upload_tool_id = $CI->Common_model->insert_data('upload_tool',$data);

    // updating upload master table status to 2
    $CI->Common_model->update_data('upload',array('status'=>2),array('upload_id'=>$upload_id)); 
    return $upload_tool_id;
}

// checking weather to insert master table or temp table 
// inserting into upload tool table with remarks
function tool_failed_transaction($remarks_string,$eq_model_ids,$eq_model_ids_data,$first_row,$upload_id,$remarks_string2)
{
    $CI = & get_instance();
    $eq_model_string =''; $e= 0;
    
    foreach ($eq_model_ids_data as $key => $value){                          
        if($e==0){
            $eq_model_string = $value;
            $e++;
            continue;
        }
        $eq_model_string = $eq_model_string.'|'.$value;
    }
    $data = array(
        'part_number'        => $first_row['part_number'],
        'tool_code'          => $first_row['tool_code'],
        'part_description'   => $first_row['part_description'],                            
        'remarks'            => @$remarks_string.@$remarks_string2,
        'upload_id'          => $upload_id,
        'eq_model'           => $eq_model_string
        );
        
    $upload_tool_id = $CI->Common_model->insert_data('upload_tool',$data);

    // updating upload master table status to 2
    $CI->Common_model->update_data('upload',array('status'=>2),array('upload_id'=>$upload_id)); 
    return $upload_tool_id;
}
// checking weather to insert master table or temp table 
function bulk_upload_tools($remarks_string,$first_row,$eq_model_ids,$eq_model_ids_data,$records_status,$upload_id)
{
    $CI = & get_instance();
    if($records_status == 1)
    {
        $original_ids_count = count($eq_model_ids);
        $after_duplicates_arr = array_unique($eq_model_ids);
        $after_duplicates_count = count($after_duplicates_arr);        
        if($original_ids_count == $after_duplicates_count)
        {
            $first_row['created_by'] = $CI->session->userdata('sso_id');
            $first_row['created_time'] = date('Y-m-d H:i:s');
            $tool_id = $CI->Common_model->insert_data('tool',$first_row); 

            foreach ($eq_model_ids as $key => $value) {
                $eqmt_data = array('tool_id'=>$tool_id,'eq_model_id'=>$value);                            
                $CI->Common_model->insert_data('equipment_model_tool',$eqmt_data);
            }
            $return_val = 0;// 0 for successfully inserted
        }
        else
        {
            $remarks_string2 = 'duplicate Equipment models found for  Same Record';
            $return_val = tool_failed_transaction($remarks_string,$eq_model_ids,$eq_model_ids_data,$first_row,$upload_id,$remarks_string2);
        }
        
    }
    else
    {
        $return_val = tool_failed_transaction($remarks_string,$eq_model_ids,$eq_model_ids_data,$first_row,$upload_id,@$remarks_string2);
    }    
    return @$return_val;
}

// inserting into upload asset table with remarks
function asset_failed_transaction($asset_remarks_string,$asset_success_data,$asset_failed_data,$success_part_array,
    $failed_part_array,$records_status,$upload_id,$asset_history_data,$asset_document_data)
{
    $CI = & get_instance();     
    $upload_asset_id = $CI->Common_model->insert_data('upload_asset',$asset_failed_data);
    $i=0;
    foreach ($failed_part_array as $key => $failed_part_data) {
        if($i==0)
        {
            $failed_part_data['part_upload_remarks'] = $asset_remarks_string; 
            $i++;       
        }
        $failed_part_data['upload_asset_id'] = $upload_asset_id;
        $upload_asset_part_id = $CI->Common_model->insert_data('upload_asset_part',$failed_part_data);
    }
    // updating upload master table status to 2
    $CI->Common_model->update_data('upload',array('status'=>2),array('upload_id'=>$upload_id)); 
    return $upload_asset_id;
}
function asset_success_transaction($asset_remarks_string,$asset_success_data,$asset_failed_data,$success_part_array,
    $failed_part_array,$records_status,$upload_id,$asset_history_data,$asset_document_data)
{
    $CI = & get_instance(); 
    $asset_id = $CI->Common_model->insert_data('asset',$asset_success_data);     
    $asset_history_data['asset_id'] = $asset_id;
    $CI->Common_model->insert_data('asset_history',$asset_history_data);
    // inserting into cal_due_date history
    if($asset_success_data['cal_type_id'] == 1)
    {
        $cal_history = array(
                'calibrated_date'   => $asset_success_data['calibrated_date'],
                'cal_due_date'      => $asset_success_data['cal_due_date'],
                'asset_id'          => $asset_id,
                'created_by'        => $CI->session->userdata('sso_id'),
                'created_time'      => date('Y-m-d H:i:s')
            );
        $CI->Common_model->insert_data('cal_due_date_history',$cal_history);
    }
    // inserting into asset position table
    $asset_position_data = array(
        'asset_id' =>$asset_id,
        'wh_id'    =>$asset_success_data['wh_id'],
        'transit'  => 0,
        'sso_id'   => NULL,
        'from_date' =>date('Y-m-d'),
        'status'    =>1
        );
    $CI->Common_model->insert_data('asset_position',$asset_postion_data);

    if(count(@$asset_document_data)>0)
    {
        $asset_document_data['asset_id'] = $asset_id;
        $CI->Common_model->insert_data('asset_documents_data',$asset_document_data);
    }    
    foreach ($success_part_array as $key => $success_part_data)
    {     
        $success_part_data['asset_id'] = $asset_id;     
        $CI->Common_model->insert_data('part',$success_part_data);
        if($success_part_data['status'] == 2) $main_asset_status = 5;        
    }
    if(@$main_asset_status == 5)
        $CI->Common_model->update_data('asset',array('status'=>$main_asset_status),array('asset_id'=>$asset_id));    
    return  0;// 0 for successfully inserted
}
// checking weather to insert master table or temp table 
function bulk_upload_assets($asset_remarks_string,$asset_success_data,$asset_failed_data,$success_part_array,
    $failed_part_array,$records_status,$upload_id,$asset_history_data,$asset_document_data)
{
    $CI = & get_instance();    
    if($records_status == 1)
    {
        if($asset_success_data['cal_type_id']==1)
        {
            $cal_supplier_id = $asset_success_data['cal_supplier_id'];
            $cal_due_date = $asset_success_data['cal_due_date'];
            if($cal_supplier_id =='' || $cal_due_date =='')
            {
                if($cal_supplier_id=='') $asset_remarks_string .='Calibration Supplier Name Not Given';
                if($cal_due_date == '') $asset_remarks_string .='Calibration Due Date Not Given';
                 $return_val = asset_failed_transaction($asset_remarks_string,$asset_success_data,$asset_failed_data,$success_part_array,
                    $failed_part_array,$records_status,$upload_id,$asset_history_data,$asset_document_data);
            }
            else
            {
               $return_val = asset_success_transaction($asset_remarks_string,$asset_success_data,$asset_failed_data,$success_part_array,
                    $failed_part_array,$records_status,$upload_id,$asset_history_data,$asset_document_data);
            }
        } 
        else
        {
            $return_val = asset_success_transaction($asset_remarks_string,$asset_success_data,$asset_failed_data,$success_part_array,
                    $failed_part_array,$records_status,$upload_id,$asset_history_data,$asset_document_data);
        }  
    }
    else
    {
        $return_val = asset_failed_transaction($asset_remarks_string,$asset_success_data,$asset_failed_data,$success_part_array,
                    $failed_part_array,$records_status,$upload_id,$asset_history_data,$asset_document_data);
    }
    return $return_val;
}

// checking weather to insert master table or temp table 
function wge_bulk_upload_assets($asset_success_data,$asset_failed_data,$success_part_array,
    $failed_part_array,$records_status,$upload_id,$asset_history_data)
{
    $CI = & get_instance();
    if($records_status == 1)
    {
       $return_val = wge_asset_success_transaction($asset_success_data,$asset_failed_data,$success_part_array,$failed_part_array,$records_status,$upload_id,$asset_history_data);
    }
    else
    {
        $return_val = wge_asset_failed_transaction($asset_success_data,$asset_failed_data,$success_part_array,$failed_part_array,$records_status,$upload_id);
    }
    return $return_val;
}

function wge_asset_success_transaction($asset_success_data,$asset_failed_data,
                $success_part_array,$failed_part_array,$records_status,$upload_id,$asset_history_data)
{
    $CI = & get_instance(); 
    $cal_type_id = @$asset_success_data['cal_type_id'];
    unset($asset_success_data['cal_type_id']);
    $asset_id = $CI->Common_model->insert_data('asset',$asset_success_data);

    #audit data
    unset($asset_success_data['mwh_id'],$asset_success_data['b_inventory']);
    $asset_success_data['tool_availability'] = NULL;
    $asset_success_data['asset_status_id'] = $asset_success_data['status'];
    $remarks_a = "Asset:".$asset_success_data['asset_number']." Creation";
    $ad_id = audit_data('asset_master',$asset_id,'asset',1,'',$asset_success_data,array('asset_id'=>$asset_id),array(),$remarks_a,'',array(),'','',$asset_success_data['country_id']);

    $asset_history_data['asset_id'] = $asset_id;
    $CI->Common_model->insert_data('asset_history',$asset_history_data);

    // inserting into cal_due_date history
    if(@$cal_type_id == 1)
    {
        $cal_history = array(
            'calibrated_date' => $asset_success_data['calibrated_date'],
            'cal_due_date'    => $asset_success_data['cal_due_date'],
            'asset_id'        => $asset_id,
            'rc_asset_id'     => NULL,
            'created_by'      => $CI->session->userdata('sso_id'),
            'created_time'    => date('Y-m-d H:i:s')
        );
        $CI->Common_model->insert_data('cal_due_date_history',$cal_history);
    }

    // inserting into asset position table
    $asset_position_data = array(
        'asset_id'  => $asset_id,
        'wh_id'     => $asset_success_data['wh_id'],
        'transit'   => 0,
        'sso_id'    => NULL,
        'from_date' => date('Y-m-d'),
        'status'    => 1
        );
    $CI->Common_model->insert_data('asset_position',$asset_position_data);
    updatingAuditData($ad_id,array('tool_availability'=>get_asset_position($asset_id)));

    // insert into asset status history
    $asset_status_history_data = array(
            'status'       => 1,
            'asset_id'     => $asset_id,            
            'created_by'   => $CI->session->userdata('sso_id'),
            'created_time' => date('Y-m-d H:i:s'));
    $CI->Common_model->insert_data('asset_status_history',$asset_status_history_data);

    foreach ($success_part_array as $key => $success_part_data)
    {
        $success_part_data['asset_id'] = $asset_id;
        $part_id = $CI->Common_model->insert_data('part',$success_part_data);

        #audit data
        unset($success_part_data['asset_id']);
        $remarks = "Child Items for asset:".$asset_success_data['asset_number'];
        $success_part_data['asset_condition_id'] = $success_part_data['status'];
        audit_data('part',$part_id,'part',1,$ad_id,$success_part_data,array('tool_id'=>$success_part_data['tool_id']),array(),$remarks,'',array(),'','',$asset_success_data['country_id']);
    }

    // updating the asset status
    $failed_status = $CI->Common_model->get_data('part',array('status'=>2,'asset_id'=>$asset_id));
    $failed_count = count($failed_status);
    if(@$failed_count > 0)
    {
        $CI->Common_model->update_data('asset',array('availability_status'=>2,'asset_condition_id'=>2,'status'=>5),array('asset_id'=>$asset_id));
        #audit data update
        updatingAuditData($ad_id,array('availability_status'=>2,'asset_condition_id'=>2,'asset_status_id'=>5));
    }
    return  0;// 0 for successfully inserted
}

function wge_asset_failed_transaction($asset_success_data,$asset_failed_data,
                    $success_part_array,$failed_part_array,$records_status,$upload_id)
{
    $CI = & get_instance();     
    $upload_asset_id = $CI->Common_model->insert_data('upload_asset',$asset_failed_data);
    $i=0;
    foreach ($failed_part_array as $key => $failed_part_data) {       
        $failed_part_data['upload_asset_id'] = $upload_asset_id;
        $upload_asset_part_id = $CI->Common_model->insert_data('upload_asset_part',$failed_part_data);
    }
    // updating upload master table status to 2
    $CI->Common_model->update_data('upload',array('status'=>2),array('upload_id'=>$upload_id));     
    return $upload_asset_id;
}

function error_helper($asset_level_id,$asset_remarks_string,$modality_code,$tool_type,$kit,$asset_type,$part_level,$part_number,$tool_code,$calibration,$supplier_code,$cal_supplier_code,$eq_model,$modality_arr,$tool_type_arr,$eq_model_data_arr,$error,$country_id)
{
    $CI = &get_instance();
    #modality validation
    $code_exist = array_key_exists($modality_code, $modality_arr);
    if($code_exist)
    {
        $modality_id = $modality_arr[$modality_code];
    }
    else
    {
        $error = 1;
        $asset_remarks_string.='Modality Code '.@$modality_code.' Not Exist. ';
    }
    
    #supplier Validation
    $s_code_exist = $CI->Common_model->get_value('supplier',array('status'=>1,'sup_type_id'=>1,'country_id'=>$country_id,'supplier_code'=>$supplier_code),'supplier_id');
    if($s_code_exist)
    {
        $supplier_id = $s_code_exist;
    }
    else
    {
        $error = 1;
        $asset_remarks_string.='Supplier Code '.@$supplier_code.' Not Exist. ';
    }

    #Tool Type Validation
    $tool_type_exist = array_key_exists($tool_type, $tool_type_arr);
    if($tool_type_exist)
    {
        $tool_type_id = $tool_type_arr[$tool_type];
    }
    else
    {
        $error = 1;
        $asset_remarks_string.='Tool Type '.@$tool_type.' Not Exist. ';
    }

    #kit Validation
    if($kit == 'Y' || $kit == 'N')
    {
        $kit_exist = 1;
        $kit_id = ($kit=='Y')?1:2;  
    } 
    else 
    {
        $kit_exist = '';
    }
    if($kit_exist =='')
    {
        $error = 1 ;
        $asset_remarks_string.= 'Kit '.$kit .' Not Exist ';
    }

    #Asset type validation
    if($asset_type == 'assembly' || $asset_type == 'component')
    {
        $asset_type_exist = 1;
        $asset_type_id = ($asset_type == 'assembly')?1:2;  
    } 
    else 
    {
        $asset_type_exist = '';
    }
    if($asset_type_exist =='')
    {
        $error = 1 ;
        $asset_remarks_string.= 'Asset Type '.$asset_type .' Not Exist ';
    }

    #part level validation
    if($part_level == 'L0' || $part_level == 'L1')
    {
        $part_level_exist = 1;
        if($asset_level_id == '')
        {
            $part_level_id = 2;
        }
        else 
        {
            if($asset_level_id == 3) 
            {
                $part_level_id = 2;
            }
            else
            {
                $part_level_id = 1;
            }
        }

        #check length of tool code
        $tc_length = strlen($tool_code);
        if($tc_length == 4 || $tc_length == 5)
        {
            #tool code unique validation
            $qry2 = "SELECT * FROM tool WHERE tool_code = '".$tool_code."' AND country_id='".$country_id."' ";
            $available2 = $CI->Common_model-> get_no_of_rows($qry2);
            if($available2>0)
            {
                $error = 1;
                $asset_remarks_string.=' tool code '.$tool_code.' already exist, '; 
            }
        }
        else
        {
           $error = 1;
           $asset_remarks_string.=' Tool code : '.$tool_code.' Length should be 4 or 5, ';  
        }

        if($part_level_id == 1)
        {
            #part number validation
            $qry = "SELECT * FROM tool WHERE part_number = '".$part_number."' AND country_id='".$country_id."' ";
            $available1 = $CI->Common_model-> get_no_of_rows($qry);
            if($available1>0)
            {
                $error = 1;
                $asset_remarks_string.='Part Number '.$part_number.' already exist, ';
            }

            #tool code unique validation
            $qry2 = "SELECT * FROM tool WHERE tool_code = '".$tool_code."' AND country_id='".$country_id."' ";
            $available2 = $CI->Common_model-> get_no_of_rows($qry2);
            if($available2>0)
            {
                $error = 1;
                $asset_remarks_string.=' tool code '.$tool_code.' already exist, '; 
            }
        }
        else
        {
            #checking same 02 level already existed with that part number
            if($asset_level_id == 3)
            {
                $qry = "SELECT * FROM tool WHERE part_number = '".trim($part_number)."' AND country_id='".$country_id."' ";
                $available1 = $CI->Common_model-> get_no_of_rows($qry);
                if($available1>0)
                {                                
                    $existed_tool_id = $CI->Common_model->get_value('tool',array('part_number'=>trim($part_number),'country_id'=>$country_id),'tool_id');
                    $error = 1;
                    $asset_remarks_string.= ' given '.$part_number.' Already Exist, ';  
                }
            }

        }
    } 
    else 
    {
        $part_level_exist = '';
    }
    if($part_level_exist =='')#if part level is empty
    {
        $error = 1 ;
        $asset_remarks_string.= 'Part Level '.$part_level .' Not Exist, ';
    }
    
    #calibration tool or not validation
    if($calibration =='y' || $calibration =='n')
    {
      $calibration_exist = 1;
      $cal_type_id = ($calibration =='y')?1:2;  
    } 
    else 
    {
        $calibration_exist = '';
    }
    if($calibration_exist =='')
    {
        $error = 1 ;
        $asset_remarks_string.= 'Calibration '.$calibration .' Not Exist, ';
    }
    
    #if calibration tool, check cal supplier avail or not
    if(@$cal_type_id == 1)
    {
        $cs_code_exist = $CI->Common_model->get_value('supplier',array('status'=>1,'calibration'=>1,'country_id'=>$country_id,'supplier_code'=>$cal_supplier_code),'supplier_id');

        if($cs_code_exist)
        {
            $cal_supplier_id = $cs_code_exist;
        }
        else
        {
            $error = 1;
           $asset_remarks_string.='Calibration Supplier Code '.@$cal_supplier_code.' Not Exist, ';
        }
    }
    else
    {
        $cal_supplier_id = NULL;
    }

    #equipment Model Validation
    if(@$eq_model)
    {
        $excel_eq_model_arr = explode('/',$eq_model);
        if(count($excel_eq_model_arr)>0)
        {
            $loop_eq_model_id_arr= array();
            foreach ($excel_eq_model_arr as $key => $value) 
            {
                $loop_eq_model = trim(strtolower($value));
                $loop_eq_model_exist = array_key_exists($loop_eq_model, $eq_model_data_arr);
                if($loop_eq_model_exist)
                {
                    $loop_eq_model_id_arr[] = $eq_model_data_arr[$loop_eq_model];
                }
                else
                {
                    $error = 1;
                    $asset_remarks_string .= 'Equipment Model '.@$loop_eq_model.' Not Exist, ';
                }
            }
            $original_ids_count     = count($loop_eq_model_id_arr);
            $after_duplicates_arr   = array_unique($loop_eq_model_id_arr);
            $after_duplicates_count = count($after_duplicates_arr);
            if($original_ids_count != $after_duplicates_count)
            {
                $error = 1;
                $asset_remarks_string .= 'Duplicate Found in ' .$eq_model;
            }
        }
    }
        
    return array(@$error,@$asset_remarks_string,@$tool_type_id,@$kit_id,@$asset_type_id,
    @$part_level_id,@$cal_type_id,@$loop_eq_model_id_arr,@$modality_id,@$supplier_id,
    @$cal_supplier_id,@$existed_tool_id);
}

function get_common_modality_id()
{
    $CI = & get_instance();
    $CI->db->select('m.modality_id');
    $CI->db->from('modality m');
    $CI->db->where('m.modality_code','CO');
    $res = $CI->db->get();
    if($res->num_rows()>0)
    {
        $row = $res->row_array();
        return $row['modality_id'];
    }
    else
    {
        return 1;
    }
}

function wge_bulk_upload_tools($tool_failed_data,$success_part_array,$failed_part_array,$records_status,$upload_id,$country_id)
{
    $CI = & get_instance();
    if($records_status == 1)
    {        
        $i= 0;
        $final_tool_sub_arr = array(); 
        $tool_sub_id_arr = array(); 
        foreach ($success_part_array as $key => $success_part_data)
        {
            $eq_model_ids_arr =  $success_part_data['eq_model_ids_arr'];
            $quantity =  $success_part_data['quantity'];

            unset($success_part_data['quantity']);
            unset($success_part_data['eq_model_ids_arr']);

            $part_number =  $success_part_data['part_number'];            
            $asset_level_id = $success_part_data['asset_level_id'];
            // Checking For 00 and 02 level 
            if($asset_level_id == 3)
            {
                $qry = "SELECT * FROM tool WHERE part_number = '".trim($part_number)."' AND country_id='".$country_id."' ";
                $available1 = $CI->Common_model->get_no_of_rows($qry);
                // when 02 tool given and it is already available
                if($available1>0)
                {
                    if($i == 0)#available and it is 02 level
                    {
                        $success_part_data['modality_id'] = get_common_modality_id();
                        $existed_tool_id = $CI->Common_model->get_value('tool',array('part_number'=>trim($part_number)),'tool_id');
                        $CI->Common_model->update_data('tool',array('modality_id'=>get_common_modality_id()),array('tool_id'=>$existed_tool_id));
                        $i++;
                    }
                    #available and it is comes under some other part then pickup the available and link it
                    else
                    {
                        $tool_sub_id = $CI->Common_model->get_value('tool',array('part_number'=>trim($part_number),'country_id'=>$country_id),'tool_id');
                        $CI->Common_model->update_data('tool',array('modality_id'=>get_common_modality_id()),array('tool_id'=>$tool_sub_id));
                        $tool_sub_id_arr[] = $tool_sub_id;
                        $tool_sub_arr = array(
                            'tool_main_id' => $tool_main_id,
                            'tool_sub_id'  => $tool_sub_id,
                            'quantity'     => $quantity,
                            'status'       => 1
                        );
                        $final_tool_sub_arr[] = $tool_sub_arr;
                        $i++;
                    }
                }
                else //inserting the 02 level
                {
                    // when 02 tool given and it is not available for first time
                    if($i == 0)
                    {
                        $tool_main_id = $CI->Common_model->insert_data('tool',$success_part_data);

                        #audit data
                        $remarks_a = "Tool:".$success_part_data['part_number']." Creation";
                        $ad_id = audit_data('tool_master',$tool_main_id,'tool',1,'',$success_part_data,array('tool_id'=>$tool_main_id),array(),$remarks_a,'',array(),'','',$country_id);

                        if(count($eq_model_ids_arr)>0)
                        {
                            foreach ($eq_model_ids_arr as $key => $eq_model_id) 
                            {
                                $CI->Common_model->insert_data('equipment_model_tool',array('tool_id'=>$tool_main_id,'eq_model_id'=>$eq_model_id));
                                
                                #audit data
                                $insert_eq = array('eq_model_id'=>$eq_model_id);
                                audit_data('equipment_model_tool',$eq_model_id,'equipment_model_tool',1,$ad_id,$insert_eq,array('eq_model_id'=>$eq_model_id),array(),'','',array(),'','',$country_id);
                            }
                        }
                        $i++;
                    }
                    else// when given 01 level and second time if the tool is NA
                    {
                        $tool_sub_id = $CI->Common_model->insert_data('tool',$success_part_data); 

                        #audit data
                        $ad_id = audit_data('tool_master',$tool_sub_id,'tool',1,'',$success_part_data,array('tool_id'=>$tool_sub_id),array(),'','',array(),'','',$country_id);

                        if(count($eq_model_ids_arr)>0)
                        {
                            foreach ($eq_model_ids_arr as $key => $eq_model_id) 
                            {
                                $CI->Common_model->insert_data('equipment_model_tool',array('tool_id'=>$tool_sub_id,'eq_model_id'=>$eq_model_id));

                                #audit data
                                $insert_eq = array('eq_model_id'=>$eq_model_id);
                                audit_data('equipment_model_tool',$eq_model_id,'equipment_model_tool',1,$ad_id,$insert_eq,array('eq_model_id'=>$eq_model_id),array(),'','',array(),'','',$country_id);
                            }
                        }
                        $tool_sub_id_arr[] = $tool_sub_id;
                        $tool_sub_arr = array(
                            'tool_main_id' => $tool_main_id,
                            'tool_sub_id'  => $tool_sub_id,
                            'quantity'     => $quantity,
                            'status'       => 1
                        );
                        $final_tool_sub_arr[] = $tool_sub_arr;
                        $i++;
                    }
                }
            }

            if($i == 0)
            {                
                $tool_main_id = $CI->Common_model->insert_data('tool',$success_part_data);

                #audit data
                $remarks_a = "Tool:".$success_part_data['part_number']." Creation";
                $ad_id = audit_data('tool_master',$tool_main_id,'tool',1,'',$success_part_data,array('tool_id'=>$tool_main_id),array(),$remarks_a,'',array(),'','',$country_id);
                if(count($eq_model_ids_arr)>0)
                {
                    foreach ($eq_model_ids_arr as $key => $eq_model_id) 
                    {
                        $CI->Common_model->insert_data('equipment_model_tool',array('tool_id'=>$tool_main_id,'eq_model_id'=>$eq_model_id));

                        #audit data
                        $insert_eq = array('eq_model_id'=>$eq_model_id);
                        audit_data('equipment_model_tool',$eq_model_id,'equipment_model_tool',1,$ad_id,$insert_eq,array('eq_model_id'=>$eq_model_id),array(),'','',array(),'','',$country_id);
                    }
                }
                $i++;
                continue;
            }
        } // end of for loop
        if(count($tool_sub_id_arr)>0)
        {
            foreach ($final_tool_sub_arr as $key => $final_tool_sub_arr_data)
            {
                $CI->Common_model->insert_data('tool_part',$final_tool_sub_arr_data);

                #audit data
                if(!isset($ad_id)){ $ad_id = NULL;}
                $tool_sub_id = $final_tool_sub_arr_data['tool_sub_id'];
                unset($final_tool_sub_arr_data['tool_main_id']);
                audit_data('tool_part',$tool_sub_id,'tool_part',1,$ad_id,$final_tool_sub_arr_data,array('tool_id'=>$tool_sub_id),array(),'','',array(),'','',$country_id);
            }
        }
        $return_val =0;
    }
    else
    {
        $upload_tool_id = $CI->Common_model->insert_data('upload_tool',$tool_failed_data);
        foreach ($failed_part_array as $key => $failed_part_data)
        {
            $failed_part_data['upload_tool_id'] = $upload_tool_id;
            $return_val = $CI->Common_model->insert_data('upload_tool_part',$failed_part_data);
        }
        $CI->Common_model->update_data('upload',array('status'=>2),array('upload_id'=>$upload_id));
    }
    return @$return_val;
}

function quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$tool_id,$notIncludePSFEcheck0=0)
{
    $CI = & get_instance();
    $CI->db->select('count(oah.oah_id) as ack_qty');
    $CI->db->from('tool_order to');
    $CI->db->join('st_tool_order sto','to.tool_order_id = sto.tool_order_id');
    $CI->db->join('tool_order st','st.tool_order_id = sto.stock_transfer_id ');            
    $CI->db->join('order_status_history osh','st.tool_order_id = osh.tool_order_id');
    $CI->db->join('order_asset_history oah','oah.order_status_id =osh.order_status_id');
    $CI->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
    $CI->db->join('ordered_tool odt','odt.ordered_tool_id = oa.ordered_tool_id');    
    $CI->db->where('osh.current_stage_id',3);  // stock transfer completed
    $CI->db->where_in('oah.status',array(1,2));
    $CI->db->where('to.tool_order_id',$tool_order_id);
    $CI->db->where('odt.tool_id',$tool_id);
    if($notIncludePSFEcheck0 == 1)
    {
        $CI->db->where('st.fe_check!=0');
    }
    if($notIncludePSFEcheck0 == 2)
    {
        $CI->db->where('st.fe_check=0');   
    }

    $res = $CI->db->get();   
    if($res->num_rows()>0) 
    {
        $result = $res->row_array();
        return $result['ack_qty'];
    }
    else
    {
        return 0;
    }    

}

function quantityInProcessByOtherStockTransfer($tool_order_id,$tool_id,$notIncludePSFEcheck0=0)
{
    $CI = & get_instance();
    $CI->db->select('sum(odt.available_quantity) as in_process_qty');
    $CI->db->from('tool_order to');
    $CI->db->join('st_tool_order sto','to.tool_order_id = sto.tool_order_id');
    $CI->db->join('tool_order st','st.tool_order_id = sto.stock_transfer_id ');            
    $CI->db->join('ordered_tool odt','st.tool_order_id = odt.tool_order_id');    
    $CI->db->where_in('st.current_stage_id',array(1,2));  // stock transfer at wh or in transit    
    $CI->db->where('to.tool_order_id',$tool_order_id);
    $CI->db->where('odt.tool_id',$tool_id);
    if($notIncludePSFEcheck0 == 1)
    {
        $CI->db->where('st.fe_check!=0');
    }
    if($notIncludePSFEcheck0 == 2)
    {
        $CI->db->where('st.fe_check=0');   
    }
    $res = $CI->db->get();   
    if($res->num_rows()>0) 
    {
        $result = $res->row_array();
        return ($result['in_process_qty']!='')?$result['in_process_qty']:0;
        
    }
    else
    {
        return 0;
    }    

}

//new helpers 15-05-2018
function getTaskRoles($parent_page)
{
    $CI = & get_instance();

    $CI->db->select('rt.role_id');
    $CI->db->from('role_task rt');
    $CI->db->join('task t','t.task_id = rt.task_id');
    $CI->db->where('t.name',$parent_page);
    $CI->db->where('rt.task_access',1);
    $CI->db->where('rt.status',1);
    $res = $CI->db->get();
    $roleData = $res->result_array();
    return convertIntoIndexedArray($roleData,'role_id');
}

function getPageAccess($parent_page,$role_id)
{
    $CI = &get_instance();    
    $task_id = $CI->Common_model->get_value('task',array('name'=>$parent_page),'task_id');    
    $page_access = $CI->Common_model->get_value('role_task',array('role_id'=>$role_id,'task_id'=>@$task_id),'task_access');
    return $page_access;
}

function getCountryListForTransaction($sso_id)  // be task access should be 3
{
    $CI = &get_instance();    
    $task_id = $CI->Common_model->get_value('task',array('name'=>'country_dropdown'),'task_id');    
    $role_id = getRoleByUser($sso_id);
    $assign_countries_list = $CI->Login_model->get_user_countries($sso_id);
    if(count(@$assign_countries_list)>0)
    {
        $countryList = $assign_countries_list;
        $countriesIndexedlist = convertIntoIndexedArray($assign_countries_list,'location_id');
    }
    else
    {
        $countries = $CI->Common_model->get_data('location',array('level_id'=>2,'status'=>1));
        $countriesIndexedlist = convertIntoIndexedArray($countries,'location_id');
    }

    return $countriesIndexedlist;
}

function get_fe_users($task_access=1,$sso_id=0,$default_parent_page=0,$country_id=0)
{
    if($default_parent_page==0)
    {
        $fe_arr =  getTaskRoles('raise_order');
    }
    else
    {
        $fe_arr = getTaskRoles($default_parent_page);
    }
    $CI = & get_instance();
    $CI->db->select('u.*,concat(u.sso_id, " - (" ,u.name,")") as name');
    $CI->db->from('user u');
    if($country_id!=0)
    {
        $CI->db->where('u.country_id',$country_id); 
    }
    else if($task_access == 1 || $task_access == 2)
    {
        $CI->db->where('u.country_id',$CI->session->userdata('s_country_id'));
    }
    else if($task_access == 3)
    {
        if($CI->session->userdata('header_country_id')!='')
        {
            $CI->db->where('u.country_id',$CI->session->userdata('header_country_id')); 
        }
        else
        {
            $CI->db->where_in('u.country_id',$CI->session->userdata('countriesIndexedArray'));  
        }
    }
    $CI->db->where('u.status',1);
    $CI->db->where_in('u.role_id',$fe_arr);
    $CI->db->order_by('u.role_id ASC');
    $CI->db->order_by('u.country_id','ASC');
    $res = $CI->db->get();
    return $res->result_array();
}

function ajaxUsersDropDown($country_id,$html=1)
{
    $users = get_fe_users(0,0,0,$country_id);
    if($html ==1)
    {
        $results='';
        if($users > 0)
        {
            $results.='<option value="">- Select SSO -</option>'; 
            foreach($users as $row)
            {
                $results.='<option value="'.$row['sso_id'].'">'.$row['name'].'</option>';      
            }
        }
        else
        {
            $results.="<option value=''>-No Data Found-</option>"; 
        } 
    }
    else
    {
        $results = $users;
    }
    return $results;
}

function ajaxCountryDropDown($sso_id,$html=1)
{
    $countries = get_user_countries($sso_id);
    if($html ==1)
    {        
        if(count($countries) > 0)
        {
            $results='';
            $results.='<option value="">- Select Transaction Country -</option>'; 
            foreach($countries as $row)
            {
                $results.='<option value="'.$row['location_id'].'">'.$row['name'].'</option>';      
            }
        }
        else
        {
            $results = 0;
        } 
    }
    else
    {
        $results = $users;
    }
    return $results;
}
// created by maruthi on 13th Nov'18 to get list of countries can user access
function get_user_countries($sso_id)
{
    $CI = &get_instance();
    $CI->db->select('l.name,l.location_id');
    $CI->db->from('location l');
    $CI->db->join('user_country uc','uc.country_id = l.location_id');
    $CI->db->where('uc.sso_id',$sso_id);
    $CI->db->where('uc.status',1);
    $CI->db->where('l.status',1);
    $res = $CI->db->get();
    return $res->result_array();    
}

function NeedStockTransferOrWhStockTransferApprovalIds($task_access=3)
{
    $CI = &get_instance();
    $CI->load->model('Stock_transfer_m');
    $CI->db->select('to.*,oda.*,l.name as fe_position');
    $CI->db->from('tool_order to');   
    $CI->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');    
    $CI->db->join('user u','to.sso_id = u.sso_id');
    $CI->db->join('location l','l.location_id = u.fe_position');                          
    $CI->db->where('to.current_stage_id',4);  
    $CI->db->where_in('to.order_type',array(1,2));
    $CI->db->where('oda.check_address',0);
    $CI->db->where('to.fe_check',0); 
    if($task_access == 1 || $task_access == 2)
    {
        if($task_access == 1)
            $CI->db->where('to.sso_id',$CI->session->userdata('sso_id'));
        else
            $CI->db->where('to.country_id',$CI->session->userdata('s_country_id'));
    }
    else if($task_access == 3)
    {
        if($CI->session->userdata('header_country_id')!='')
        {
            $CI->db->where('to.country_id',$CI->session->userdata('header_country_id'));    
        }
        else
        {
            $CI->db->where_in('to.country_id',$CI->session->userdata('countriesIndexedArray')); 
         
        }
    }        
    $res = $CI->db->get();   
    //echo "<pre>" ;print_r($res->result_array());exit;
    $final_data = array();
    $tool_order_id_arr = array();
    if($res->num_rows()>0)
    {                
        foreach ($res->result_array() as $key => $value)
        {
            $tool_order_id = $value['tool_order_id'];
            
            if(!array_key_exists($tool_order_id, $tool_order_id_arr))
            {
                if($value['order_type'] == 2)
                {
                    $ordered_tools = $CI->Stock_transfer_m->get_ordered_tool_info($tool_order_id,2); 
                    if(count($ordered_tools))
                    {
                        foreach ($ordered_tools as $key1 => $value1)
                        {
                            $checkPSFromDedicatedWh = checkPSFromDedicatedWh($tool_order_id);                            
                            if($checkPSFromDedicatedWh == 0)
                            {
                                $ack_qty = quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$value1['tool_id']);                                 
                            }
                            else
                            {                                
                                $ack_qty = quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$value1['tool_id'],1);
                            }
                            $in_process_qty = quantityInProcessByOtherStockTransfer($tool_order_id,$value1['tool_id']);  
                            // if($tool_order_id ==1135)                           
                            // {
                            //     echo $ack_qty.'--'.$in_process_qty;
                            //     //exit;
                            // }
                            if($value['order_delivery_type_id'] == 2)
                            {
                               // special case for partial shipment happening from dedicated warehouse                                
                                if($checkPSFromDedicatedWh == 1 )                                
                                {
                                    $final_qty = @$ack_qty + $in_process_qty; 
                                }
                                else
                                {
                                    $final_qty = $in_process_qty + @$value1['available_quantity']; 
                                }
                            }
                            else
                            {                                
                                $final_qty = @$ack_qty + $in_process_qty;
                            }                            
                            if($value1['quantity'] > $final_qty)
                            {                                    
                                $tool_order_id_arr[$tool_order_id] = $tool_order_id;
                                break;                                    
                            }              
                        }
                    }
                }
                else
                {                            
                    $tool_order_id_arr[$tool_order_id] = $tool_order_id;                            
                }
            }
        }
    }    
    $final_arr = array();
    if(count($tool_order_id_arr)>0)
    {
        foreach ($tool_order_id_arr as $key => $value) {
            $final_arr[] = $key;
        }
    }
    else
    {
        $final_arr = array(0);
    }    
    return $final_arr ;    
}

// for Dashbord Stock Transfer Count
function STforOrder_tool_total_num_rows($searchParams,$task_access)
{
    $tool_order_id_arr = NeedStockTransferOrWhStockTransferApprovalIds($task_access);
    $CI = &get_instance();
    $CI->db->select('to.*,oda.*');
    $CI->db->from('tool_order to'); 
    $CI->db->join('location l','l.location_id = to.country_id','LEFT');  
    $CI->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
    $CI->db->join('st_tool_order sto','to.tool_order_id = sto.tool_order_id ','left');
    $CI->db->join('tool_order stn','sto.stock_transfer_id = stn.tool_order_id','left');
    if(@$searchParams['st_order_number']!='')
        $CI->db->or_like(array('to.order_number' => $searchParams['st_order_number'], 'to.stn_number' => $searchParams['st_order_number']));
    if(@$searchParams['st_sso_id']!='')
        $CI->db->where('to.sso_id',$searchParams['st_sso_id']);
    if(@$searchParams['st_request_date']!='')
        $CI->db->where('to.request_date>=',format_date($searchParams['st_request_date']));
    if(@$searchParams['st_return_date']!='')
        $CI->db->where('to.return_date<=',format_date($searchParams['st_return_date']));
    $CI->db->where('to.current_stage_id',4);  
    $CI->db->where_in('to.order_type',array(1,2));
    $CI->db->where('oda.check_address',0);
    $CI->db->where('to.fe_check',0);
    $CI->db->where_in('to.tool_order_id',$tool_order_id_arr);
    if($task_access == 1 || $task_access == 2)
    {
        if($task_access == 1)
            $CI->db->where('to.sso_id',$CI->session->userdata('sso_id'));
        else
            $CI->db->where('to.country_id',$CI->session->userdata('s_country_id'));
    }
    else if($task_access == 3)
    {
        if($CI->session->userdata('header_country_id')!='')
        {
            $CI->db->where('to.country_id',$CI->session->userdata('header_country_id'));    
        }
        else
        {
            if($searchParams['st_country_id']!='')
            {
                $CI->db->where('to.country_id',$searchParams['st_country_id']);
            }
            else
            {
                $CI->db->where_in('to.country_id',$CI->session->userdata('countriesIndexedArray')); 
            }
        }
    }
    $CI->db->group_by('to.tool_order_id');
    $res = $CI->db->get();
    return $res->num_rows();
}

function get_countries_string($searchParams,$type=0,$task_access=1,$associateArray=0) // 0 for to get string other than that arr
{
    $CI = & get_instance();
    // get the warehouses for user    
    $role_id = getRoleByUser($searchParams['order_sso_id']);
    $sso_id = $searchParams['order_sso_id'];
    $country_ids = array();
    if($task_access == 1)
    {
        $country_ids = $CI->Common_model->get_data('user',array('sso_id'=>$sso_id),'country_id');        
    }
    else
    {
        if($role_id == 5 || $role_id == 6)
        {
            $country_ids = $CI->Common_model->get_data('user',array('sso_id'=>$sso_id),'country_id');        
        }
        else
        {
            if($task_access == 2)
            {
                $country_ids = $CI->Common_model->get_data('user',array('sso_id'=>$sso_id),'country_id');                
            }
            else
            {                
                if($associateArray!=0)
                {
                    $assign_countries_list = $CI->Login_model->get_user_countries($sso_id);
                    if(count(@$assign_countries_list)>0)
                    {
                        $country_ids = $assign_countries_list;                        
                    }
                    else
                    {
                        $country_ids = $CI->Common_model->get_data('location',array('level_id'=>2,'status'=>1));                        
                    }
                    return $country_ids;
                }
                $countries = getCountryListForTransaction($sso_id); // return will be array                
                if($type == 0)
                    $country_ids = implode(',', $country_ids);
                else
                    $country_ids = $countries;
            }
        }
    }
    return $country_ids;
}


function getWhArrNotInID($wh_id,$sso_id=0,$whsIndexed = array()) // sso id modified by maruthi on20th may'18
{
    $CI = &get_instance();  
    $country_id = $CI->Common_model->get_value('warehouse',array('wh_id'=>$wh_id),'country_id');
    if($sso_id!=0)
    {
        $countries = getCountryListForTransaction($sso_id); // 0 for to get string other than that arr                    
    }

    $CI->db->select('wh.wh_id,concat(wh.wh_code," - (",wh.name,")") as name,wh.wh_code');
    $CI->db->from('warehouse wh');
    $CI->db->where('wh.wh_id!=',$wh_id);
    if($sso_id ==0)        
        $CI->db->where('wh.country_id',$country_id);
    else
        $CI->db->where_in('wh.country_id',$countries);
    if(count($whsIndexed)>0)
    {
        $CI->db->where_in('wh_id',$whsIndexed);
        $string = implode(',', $whsIndexed);
        $CI->db->order_by('FIELD ( wh_id,'.$string.' )');
    }    
    $CI->db->where('status',1);
    $res = $CI->db->get();    
    return $res->result_array();
}

function get_install_basedata($system_id,$only_name=0)
{
    $CI = &get_instance();
    $CI->db->select('cs.*,c.name as c_name,concat(c.name,"(-",c.customer_number,")") as c_full_name');
    $CI->db->from('install_base ib');     
    $CI->db->join('customer_site cs','ib.customer_site_id = cs.customer_site_id');
    $CI->db->join('customer c','c.customer_id = cs.customer_id');
    $CI->db->where('ib.system_id',$system_id);
    $res = $CI->db->get();
    if($only_name == 0)
    {
        return $res->row_array();
    }
    else
    {
        $result = $res->row_array();
        return $result['c_full_name'];
    }

}

function getSTOrderNumber($country_id)
{
    $CI = &get_instance();
    $number = $CI->Order_m->get_st_latest_record($country_id);    
    if(count($number) == 0)
    {
        $f_order_number = "1-00001";
    }
    else
    {
        $num = ltrim($number['stn_number'],"ST");                    
        $result = explode("-", $num);
        $running_no = (int)$result[1];
        if($running_no == 99999)
        {
            $first_num = $result[0]+1;
            $second_num = get_running_sno_five_digit(1);  
        }
        else
        {
            $first_num = $result[0];
            $val = $running_no+1;
            $second_num = get_running_sno_five_digit($val);
        }
        $f_order_number = $first_num.'-'.$second_num;
    }
    return $f_order_number;
}

function crossedReturnDateOrdersTotalNumRows($searchParams,$task_access)
{
    $CI = &get_instance();
    $CI->db->select('to.*,odt.name as order_type');
    $CI->db->from('tool_order to'); 
    $CI->db->join('user u','u.sso_id = to.sso_id','LEFT');
    $CI->db->join('location l','l.location_id = to.country_id','LEFT');      
    $CI->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
    $CI->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
    if(@$searchParams['cr_order_number']!='')
        $CI->db->like('to.order_number',@$searchParams['cr_order_number']);
    if(@$searchParams['cr_order_delivery_type_id']!='')
        $CI->db->where('to.order_delivery_type_id',@$searchParams['cr_order_delivery_type_id']);    
    $CI->db->where('to.return_date<',date('Y-m-d'));
    $CI->db->where('to.order_type',2);
    $CI->db->where('to.current_stage_id',7);
    if(@$searchParams['crd_sso_id']!='')
        $CI->db->where('to.sso_id',@$searchParams['crd_sso_id']);
    if($task_access == 1 || $task_access == 2)
    {
        if($task_access == 1)
        {
            if(count($CI->session->userdata('countriesIndexedArray'))>1)
            {
                if($CI->session->userdata('header_country_id')!='')
                {
                    $CI->db->where('to.country_id',$CI->session->userdata('header_country_id'));    
                }
                else
                {
                    if($searchParams['crd_country_id']!='')
                    {
                        $CI->db->where('to.country_id',$searchParams['crd_country_id']);
                    }
                    else
                    {
                        $CI->db->where_in('to.country_id',$CI->session->userdata('countriesIndexedArray')); 
                    }
                }
            }
            $CI->db->where('to.sso_id',$CI->session->userdata('sso_id'));
        }
        else
        {
            $CI->db->where('to.country_id',$CI->session->userdata('s_country_id'));
        }
    }
    else if($task_access == 3)
    {
        if($CI->session->userdata('header_country_id')!='')
        {
            $CI->db->where('to.country_id',$CI->session->userdata('header_country_id'));    
        }
        else
        {
            if($searchParams['crd_country_id']!='')
            {
                $CI->db->where('to.country_id',$searchParams['crd_country_id']);
            }
            else
            {
                $CI->db->where_in('to.country_id',$CI->session->userdata('countriesIndexedArray')); 
            }
        }
    }
    $res = $CI->db->get();
    return $res->num_rows();
}

function get_sub_parts_data($tool_id)
{
    $CI = & get_instance();
    $CI->db->select('t.*');
    $CI->db->from('tool t');
    $CI->db->join('tool_part tp','tp.tool_sub_id=t.tool_id','left');
    $CI->db->where('tp.tool_main_id',$tool_id);
    $res = $CI->db->get();
    return $res->result_array();
}

function get_fe2_fe_transit_status($tool_order_id)
{    
    $CI = & get_instance();
    $CI->db->select('osh.current_stage_id');
    $CI->db->from('order_status_history osh');    
    $CI->db->join('return_order ro','osh.tool_order_id = osh.tool_order_id');    
    $CI->db->where('osh.tool_order_id',$tool_order_id);
    $CI->db->where('osh.rto_id>',0);
    $CI->db->where('osh.current_stage_id',6);
    $CI->db->where('osh.current_stage_id<7');
    $CI->db->where('osh.end_time IS NULL');    
    $res = $CI->db->get();
    return $res->row_array(); 
}

function indian_date_format($timestamp)
{
    if($timestamp != '')
    {
        $time = strtotime($timestamp);
        return date('d-m-Y',$time);
    }
    else return '';
}
function shipDatesForSTs($tool_order_id)
{
    $CI = & get_instance();
    $CI->db->select('st.stn_number,DATE(oddl.created_time) as sts_shipped_date');
    $CI->db->from('tool_order to');
    $CI->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id');        
    $CI->db->join('tool_order st','st.tool_order_id = sto.stock_transfer_id');   
         $CI->db->join('order_delivery oddl','oddl.tool_order_id = st.tool_order_id');   
    $CI->db->where('sto.tool_order_id',$tool_order_id);
    $CI->db->where('st.current_stage_id',3);
    $CI->db->where('st.status>',4);
    $res = $CI->db->get();
    return $res->result_array();
}
function getFinalShipDate($row)
{
    $final_data = '';
    $CI = &get_instance();
    if($row['final_status']==10)
    {
        if($row['shipped_date']=='')   // stock transfer happen from other warehouse to fe location or fe2 fe happen
        {
            if(checkSTorderExist($row['tool_order_id'])) // sts happed
            {
                $sts_data = shipDatesForSTs($row['tool_order_id']);
                $i = 1;
                $final_data='';
                foreach ($sts_data as $key => $f_value) {                   
                    if(count($sts_data) ==$i)
                        $final_data.=@$f_value['stn_number'].' ('.indian_format($f_value['sts_shipped_date']).')';
                    else
                        $final_data.=@$f_value['stn_number'].' ('.indian_format($f_value['sts_shipped_date']).')'.' ,';
                    $i++;
                }
            }
            else   // returns happen from FE2 fe by Hand and courier
            {
                $return_data = $CI->Common_model->get_data_row('return_order',array('tool_order_id'=>$row['tool_order_id'],'return_approval'=>2));
                if($return_data['return_type_id'] == 3)
                    $final_data = indian_format(@$return_data['modified_time']);                    
                else
                    $final_data ="By Hand";

            }
        }
        else
        {
            $final_data = (@$row['shipped_date']=='')?"NA":indian_format($row['shipped_date']);             
        }
    }
    else  // cancelled
    {
        $final_data = "NA";
    }
    return ($final_data=='')?"NA":$final_data;
}

function getXLFinalShipDate($row)
{
    $final_data = '';
    $CI = &get_instance();
    if($row['final_status']==10)
    {
        if($row['shipped_date']=='')   // stock transfer happen from other warehouse to fe location or fe2 fe happen
        {
            if(checkSTorderExist($row['tool_order_id'])) // sts happed
            {
                $sts_data = shipDatesForSTs($row['tool_order_id']);
                $i = 1;
                $final_data='';
                if(count($sts_data)>0)
                {
                    foreach ($sts_data as $key => $f_value) {                        
                        if(count($sts_data) ==$i)
                            $final_data.=@$f_value['stn_number'].' ('.full_indian_format($f_value['sts_shipped_date']).')';
                        else
                            $final_data.=@$f_value['stn_number'].' ('.full_indian_format($f_value['sts_shipped_date']).')'.' ,';
                        $i++;
                    }
                }
            }
            else   // returns happen from FE2 fe by Hand and courier
            {
                $return_data = $CI->Common_model->get_data_row('return_order',array('tool_order_id'=>$row['tool_order_id'],'return_approval'=>2));
                if($return_data['return_type_id'] == 3)
                    $final_data = full_indian_format(@$return_data['modified_time']);                    
                else
                    $final_data ="By Hand";
            }
        }
        else
        {
            $final_data = (@$row['shipped_date']=='')?"NA":full_indian_format($row['shipped_date']);             
        }
    }
    else  // cancelled
    {
        $final_data = "NA";
    }
    return ($final_data=='')?"NA":$final_data;
}

function getOpenOrderStatus($row)
{
    $CI = &get_instance();
    if($row['current_stage_id'] == 4)
    {
        if(checkSTorderExist($row['tool_order_id'])==1)
        {
            return 'In Stock Transfer';
        }
        else
        {
            return getReasonForOrderAtAdmin($row['tool_order_id']);
        }
    }
    else
    {
        if($row['current_stage_id'] == 6)
        {
            $fe2_fe_transit = get_fe2_fe_transit_status($row['tool_order_id']);
            if($fe2_fe_transit)
                return 'Intransit From FE to FE';
            else
                return $CI->Common_model->get_value('current_stage',array('current_stage_id'=>$row['current_stage_id']),"name");
        }
        else
        {
            if($row['current_stage_id'] == 5)
            {
                return $CI->Common_model->get_value('current_stage',array('current_stage_id'=>$row['current_stage_id']),"name");
            }
            else
            {
                if($row['current_stage_id'] == 7)
                {
                    $status_data= $CI->Common_model->get_value('current_stage',array('current_stage_id'=>$row['current_stage_id']),"name");
                    $reason = getReasonForOrderAtAdmin($row['tool_order_id']);
                    if($reason !='')
                    {
                        $status_data.=" & ".$reason;
                    }
                    $reason2 = feOrderStatusFrom8thStageOnwards($row['tool_order_id']);
                    if($reason2!='')
                        $status_data.=" & ".$reason2;
                    return $status_data;
                }
                else
                {
                    return feOrderStatusFrom8thStageOnwards($row['tool_order_id']);
                }                                       
            }
                
        }
    }
}

function getReasonForOrderAtAdmin($tool_order_id)
{
    $CI = &get_instance();
    $CI->db->select('to.*,oad.*,CONCAT(u.sso_id," -(",u.name,") , ",to.fe1_order_number) AS fe1_name');
    $CI->db->from('tool_order to');
    $CI->db->join('tool_order to1','to1.order_number = to.fe1_order_number AND to1.country_id = to.country_id','LEFT'); 
    $CI->db->join('user u','u.sso_id = to1.sso_id','LEFT');       
    $CI->db->join('order_address oad','oad.tool_order_id = to.tool_order_id');
    $CI->db->where('to.tool_order_id',$tool_order_id); 
    $res = $CI->db->get();
    $result = $res->row_array();
    $reason_arr = array();
    $break1 = ''; $break2 = '';
    if($result['fe_check']==1 && $result['check_address'] == 1)
    {
        $break1 = "<br>& ";
    }

    $reason_string ='';
    if($result['fe_check']==1 && $result['current_stage_id'] == 4)
    {
        $reason_string.=' Requested Transfer From <br>'.@$result['fe1_name'];
        $return_data = checkMultipleReturnsForFE2Order($tool_order_id);
        if(count($return_data)>0)
        {
            $reason_string.=' and In Progress';
        }
    }
    if($result['check_address'] ==1)
    {
        $reason_string.=$break1.'Waiting For Address Approval';
    }
    if($result['fe_check'] == 0)
    {
        $available_status = $CI->Common_model->get_data('ordered_tool',array('status'=>2,'tool_order_id'=>$tool_order_id));

        if($result['fe_check']==1 && count($available_status)>0)
        {
            $break2 = "<br>& ";
        }
        if($result['check_address']==1 && count($available_status)>0)
        {
            $break2 = "<br>& ";
        }
        if(count($available_status)>0 && $result['current_stage_id'] == 4)
        {
            $reason_string.=$break2.'Need Stock Transfer';
        }
    }
    if(@$result['days_approval']==1 && $result['current_stage_id'] == 7)
    {
        $reason_string.="Waiting For Return Date Extension Approval";
    }
    else if(@$result['days_approval']==3 && $result['current_stage_id'] == 7)
    {
        $reason_string.="Return Date Extension has been Rejected";
    }
    return $reason_string;

}
function getReturnOrderStatus($row,$return_stage)
{
    if(count($row)>0)
    {
        $remarks =  @$row['return_order_remarks'];
        $return_type_id = @$row['return_type_id'];
        $return_approval = @$row['return_approval'];   
        if($remarks=='Cancelled By FE')
        {
            return 'Cancelled';                                                    
        }
        else
        {
            $status = ($row['address_check'] == 1)?"Waiting for Return Address Change Approval.":"";
            if($return_type_id == 3 || $return_type_id == 4 )
            {
                if($return_approval == 2)
                {
                    if($return_type_id== 3 )
                    {
                        if($return_stage == 8 )
                            return $status.'Waiting For '.@$row['to_sso'].'-'.@$row['to_order'].' to Receive';
                        else
                            return $status.@$row['to_sso'].' &nbsp;&nbspfor Order No:'.@$row['to_order'].' Received';
                    }
                    else
                    {
                        if($return_stage == 8)
                        {
                            return $status.'Waiting For '.@$row['wh1'].' to Pickup';
                        }
                        else
                        {
                            if($return_stage == 9)
                                return $status.'Waiting For '.@$row['to_sso'].'-'.@$row['to_order'].' to Receive';
                            else
                                return $status.@$row['to_sso'].'&nbsp;&nbsp'.@$row['to_order'].'  Received';

                        }
                    }
                }
                if($return_approval == 1)
                {
                    return $status.'At Admin for Transfer Approval';
                }
                if($return_approval == 3)                
                {
                    return $status.'Rejected';
                }
            }
            else
            {
                if($return_stage == 8)
                {
                    return $status.'Waiting For '.@$row['wh1'].' to Pickup';
                }
                else
                {
                    if($return_stage == 9)
                        return $status.'Waiting For '.@$row['wh2'].' to Ack ';                
                    else
                        return $status.'Received At '.@$row['wh2'];
                }
            }
        }
    }
    else
    {
        return 'NA';
    }
}
 function feOrderStatusFrom8thStageOnwards($tool_order_id)
{  
    $CI = &get_instance();     
    $CI->db->select('to.order_number as fe1_order_number,to.tool_order_id as from_tool_order_id,
                    l.name as countryName,concat(u.sso_id," - ",u.name) as sso,ro.*,
                    rt.name as return_type,osh.order_status_id,rto.rto_id as rto_id,
                    to_order.order_number as to_order,concat(to_user.sso_id," - ",to_user.name) as to_sso,
                    to_order.current_stage_id as to_current_stage_id,ro.remarks as return_order_remarks,
                    concat(wh1.wh_code," - ",wh1.name) as wh1,concat(wh2.wh_code," - ",wh2.name) as wh2

                    ');
    $CI->db->from('tool_order to');   
    $CI->db->join('return_tool_order rto','rto.tool_order_id = to.tool_order_id');
    $CI->db->join('return_order ro','ro.return_order_id = rto.return_order_id');
    $CI->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
    $CI->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
    $CI->db->join('user u','u.sso_id = to.sso_id');
    $CI->db->join('location l','l.location_id = ro.country_id','left');
    $CI->db->join('tool_order to_order','to_order.tool_order_id = ro.tool_order_id','left');
    $CI->db->join('user to_user','to_user.sso_id = to_order.sso_id','left');
    $CI->db->join('warehouse wh1','wh1.wh_id = ro.wh_id','left');
    $CI->db->join('warehouse wh2','wh2.wh_id = ro.ro_to_wh_id','left');    
    $CI->db->where_in('ro.return_approval',array(0,1,2));    
    $CI->db->group_by('ro.return_order_id');
    $CI->db->order_by('ro.return_order_id');
    $CI->db->where('to.tool_order_id',$tool_order_id);
    $res = $CI->db->get();
    $result = $res->result_array();    
    $reason ='';
    if(count($result)>0)
    {
        $reason ='';       
        foreach ($result as $key => $row)
        {
            $arr = array('tool_order_id'=>@$row['from_tool_order_id'],'rto_id'=>@$row['rto_id']);                                                
            $return_stage_data = get_transaction_latest_record($arr);                                                
            $return_stage = @$return_stage_data['current_stage_id'];
            
            if(!($row['return_type_id']==3 && $row['status']== 10 && $row['to_current_stage_id']>4) )
                $reason.= $row['return_number'].'&nbsp;&nbsp;'.getReturnOrderStatus($row,$return_stage)."<br>";   
            elseif (($row['return_type_id']==3 && $row['status']== 10 && $row['to_current_stage_id']>4)) {
                $reason.= $row['return_number'].'&nbsp;&nbsp;'.getReturnOrderStatus($row,$return_stage)."<br>";   
            }
        }
    }
    return $reason;
}
function get_transaction_latest_record($arr)
{
    $CI = & get_instance();
    $CI->db->select('current_stage_id');
    $CI->db->from('order_status_history');    
    $CI->db->where('tool_order_id',$arr['tool_order_id']);
    $CI->db->where('rto_id',$arr['rto_id']);
    $CI->db->order_by('order_status_id DESC');
    $res = $CI->db->get();    
    return $res->row_array();
}

function checkMultipleReturnsForFE2Order($fe1_tool_order_id)
{
    $CI = & get_instance();
    $CI->db->select('ro.return_number,fe1.order_number as fe1_order_number,fe2.order_number as fe2_order_number');
    $CI->db->from('return_order ro');
    $CI->db->join('return_tool_order rto','rto.return_order_id=ro.return_order_id');
    $CI->db->join('tool_order fe1','fe1.tool_order_id = rto.tool_order_id');
    $CI->db->join('tool_order fe2','fe2.tool_order_id = ro.tool_order_id');
    $CI->db->where_in('ro.return_approval',array(1,2));       
    $CI->db->where('ro.tool_order_id',$fe1_tool_order_id);
    $res = $CI->db->get();
    return $res->row_array();
}

function stockTransferStatus($value,$user=1)  // 1 Admin/SA  2  FE
{
    $str ='';
    $str = ($value['fe_check'] == 1)?"At FE":"At WH";
    if($value['current_stage_id'] == 1)
        return 'At WH';
    if($value['current_stage_id'] == 2 && $value['fe_check'] == 0)
        return 'Intransit WH to WH';
    if($value['current_stage_id'] == 2 && $value['fe_check'] == 1)
        return 'Intransit WH to FE';
    if($value['current_stage_id']==3 && $value['status'] ==5)
        return 'Partially Received';
    if($value['current_stage_id']==3 && $value['status'] ==4)
        return 'Cancelled';
    if($value['current_stage_id']==3 && $value['status'] ==6)
        return 'Not Received Any Quantity';
    if($value['current_stage_id']==3 && $value['status'] == 10 && $user == 1)
        return 'Closed '.$str;
    if($value['current_stage_id']==3 && $value['status'] == 10 && $user == 2)
        return 'Received';
}

function checkForPartialClosing($tool_order_id)
{
    $CI = &get_instance();
    $CI->db->select('st.*');
    $CI->db->from('tool_order st');
    $CI->db->join('st_tool_order sto','sto.stock_transfer_id = st.tool_order_id');
    $CI->db->where('sto.tool_order_id',$tool_order_id);
    $CI->db->where_in('st.current_stage_id',array(1,2));
    $res = $CI->db->get();
    if($res->num_rows()>0)
        return $res->result_array();
    else
        return FALSE;
}

function checkCancelOrderForSTsRaisedToDedicatedWh($tool_order_id)
{
    $CI = &get_instance();
    $CI->db->select('st.*');
    $CI->db->from('tool_order st');
    $CI->db->join('st_tool_order sto','sto.stock_transfer_id = st.tool_order_id');
    $CI->db->where('sto.tool_order_id',$tool_order_id);
    $str = '(st.current_stage_id = 2 OR st.current_stage_id = 1 OR ((st.current_stage_id =3 AND st.status = 5) OR (st.current_stage_id =3 AND st.status = 10) ) )';       
    $CI->db->where($str);
    $CI->db->where('st.fe_check',1);
    $res = $CI->db->get();
    if($res->num_rows()>0)
        return $res->result_array();
    else
        return FALSE;
}

function dataForSTMails($tool_order_id)
{
    $CI = &get_instance();
    $CI->db->select('st.stn_number,mto.order_number,CONCAT(u.sso_id," - ",u.name) as fe_info,oda.*,mto.country_id,u.email,
                    st.wh_id as from_log_wh,u.name as fe_name');
    $CI->db->from('tool_order st');
    $CI->db->join('st_tool_order sto','sto.stock_transfer_id = st.tool_order_id');
    $CI->db->join('tool_order mto','mto.tool_order_id = sto.tool_order_id');
    $CI->db->join('user u','u.sso_id = mto.sso_id');
    $CI->db->join('order_address oda','oda.tool_order_id = mto.tool_order_id');
    $CI->db->join('warehouse wh','wh.wh_id = st.wh_id');
    $CI->db->where('st.tool_order_id',$tool_order_id);                
    $res = $CI->db->get();   
    return $res->row_array();
}

function testing_mail()
{
    $ci = & get_instance();
    $testing_mail = $ci->config->item('testing_mail');
    return $testing_mail;
}

function subject_test()
{
    $ci = & get_instance();
    $subject_test = $ci->config->item('subject_test');
    return $subject_test;
}

function getRegionIdById($id,$type)  // type 1 is get from country, 2 from sso id
{
    $CI = &get_instance();
    $CI->db->select('l.location_id');
    $CI->db->from('location l');    
    if($type ==1 )    
    {
        $CI->db->where('l.location_id',$id);                
    }
    else
    {
        $CI->db->join('user u','u.country_id = l.location_id');
        $CI->db->where('u.sso_id',$id);                   
    }
    $res = $CI->db->get();   
    $data = $res->row_array();  
    return $data['location_id'];
}

function getAseanRegionId()
{
    $CI = &get_instance();
    $CI->db->select('l.location_id');
    $CI->db->from('location l'); 
    $CI->db->where('l.name',"ASEAN");
    $res = $CI->db->get();
    $data = $res->row_array();
    return $data['location_id'];
}

function checkAsean($id,$type)
{
    if(getAseanRegionId() == getRegionIdById($id,$type)) 
        return TRUE;
    else
        return FALSE;
}

function getAseanWarehouse($id,$type) // 1 is ID , 2 is array
{
    $CI = &get_instance();
    if($type == 1)
        $data = $CI->Common_model->get_value('warehouse',array('country_id'=>$id,'status'=>1),'wh_id');
    if($type == 2)
        $data = $CI->Common_model->get_data('warehouse',array('country_id'=>$id,'status'=>1));
    return $data;
}
// valodation type 1 client, 2 server
function checkOrderNumberAvailability($validationType,$fe1_order_number,$toolIdWithQty,$transactionCountry) 
{
    $CI = &get_instance();
    $fe1_tool_order_id_dat = $CI->Common_model->get_data('tool_order',array('order_number'=>trim($fe1_order_number),'current_stage_id'=>7,'country_id'=>$transactionCountry));        
    // get the FE2 pending tools from admin 
    if(count($fe1_tool_order_id_dat) ==  0)
    {
        $message = 'Order Number '.$fe1_order_number.' Not Found or FE1 do not have any tools to send';
        if($validationType == 1)
        {
            $data['flg'] = 0;
            $data['message']  = $message;
            return $data;
        }
        else
        {
            $CI->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> '.$message.'</div>'); 
             redirect(SITE_URL.'home');     
        }
        
    }
    $fe1_tool_order_id = $fe1_tool_order_id_dat[0]['tool_order_id'];
    $fe1_tools_with_qty = get_current_stage_involved_assets_details($fe1_tool_order_id,7,array(1,2),2);
    if(count($fe1_tools_with_qty)>0)
    {
        $fe1_tools_with_qty_arr = array();
        $fe1_tools = array();
        foreach ($fe1_tools_with_qty as $key => $value)
        {
            $fe1_tools_with_qty_arr[$value['tool_id']] = $value['qty'];
            $fe1_tools[] = $value['tool_id'];
        }
    }
    else
    {
        $message = 'FE1 Order  '.$fe1_order_number.' do not have any tools to send or He already Initiated the Return.';
        if($validationType == 1)
        {
            $data['flg'] = 0;
            $data['message']  = $message;
            return $data;
        }
        else
        {
           $CI->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Sorry!</strong> '.$message.'</div>'); 
             redirect(SITE_URL.'home');  
        }  
    }

    foreach ($toolIdWithQty as $tool_id => $qty)
    {
        if(in_array($tool_id, $fe1_tools))
        {
            if($qty > $fe1_tools_with_qty_arr[$tool_id])
            {
                $message = 'The Tools Quantity which you added to cart are not matching with FE1 '.$fe1_order_number.' Owned tools Quantity,Please Check.';
                if($validationType == 1)
                {
                    $data['flg'] = 0;
                    $data['message']  = $message;
                    return $data;
                }
                else
                {
                    $CI->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Sorry!</strong> '.$message.'</div>'); 
                    redirect(SITE_URL.'home');   
                }
            }
        }
        else
        {
            $message = 'Tools which you added to cart are not matching with FE1 '.$fe1_order_number.' Owned tools,Please Check.';
            if($validationType == 1)
            {
                $data['flg'] = 0;
                $data['message']  = $message;
                return $data;
            }
            else
            {
                $CI->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> '.$message.'</div>'); 
                 redirect(SITE_URL.'home');   
            }
        }
    }
    $message = '';
    if($validationType == 1)
    {
        $data['flg'] = 1;
        $data['message']  = $message;
        return $data;
    }
    else
    {
        return 1;
    }
}

function unsetToolOrderSessions($type=1) // 1 unset the session, 2 get
{
    $toolOrderSessions = array('transactionUser','transactionCountry','onbehalf_fe','tool_id','quantity','first_url_count','last_url_count');
    if($type ==1)
    {
        unset($_SESSION['transactionCountry'],$_SESSION['transactionUser'],$_SESSION['onbehalf_fe'],$_SESSION['tool_id'],$_SESSION['quantity'],$_SESSION['first_url_count'],$_SESSION['last_url_count']);
    }
    else
    {
        return  $toolOrderSessions;
    }
}

function getCountryIdWithToolOrder($tool_order_id,$type=1)  // type 1 is user country id, 2 is tool order country id
{
    $CI = &get_instance();
    if($type == 1)
        $CI->db->select('u.country_id');
    else
        $CI->db->select('to.country_id');
    $CI->db->from('tool_order to');
    $CI->db->join('user u','u.sso_id = to.sso_id');
    $CI->db->where('to.tool_order_id',$tool_order_id);
    $res = $CI->db->get();
    $result = $res->row_array();
    return $result['country_id'];
}

function updateInsertAssetPosition($arr)
{
    $CI = &get_instance();
    $asset_id = $arr['asset_id'];
    $status = $arr['status'];
    $transit = $arr['transit'];
    
    # Update Asset position
    $update_asset_position = array('to_date' => date('Y-m-d H:i:s'));
    $update_w = array('asset_id'=>$asset_id,'to_date'=>NULL);
    $CI->Common_model->update_data('asset_position',$update_asset_position,$update_w);

    # Insert Asset Position
    $insert_asset_position = array(
        'asset_id'      => $asset_id,
         'transit'      => 0,
         'from_date'    => date('Y-m-d H:i:s'),
         'status'       => 3
    );
    if(isset($arr['sso_id']))
        $insert_asset_position['sso_id'] = $arr['sso_id'];
    if(isset($arr['wh_id']))
        $insert_asset_position['wh_id'] = $arr['wh_id'];

    $CI->Common_model->insert_data('asset_position',$insert_asset_position);
}


function updateInsertAssetStatusHistory($arr)
{
    $CI = &get_instance();
    $asset_id = $arr['asset_id'];
    $created_by = $arr['created_by'];
    $status = $arr['status'];
    $current_stage_id = $arr['current_stage_id'];

    $getValueData = array('end_time'=>NULL,'asset_id'=>$asset_id);
    $ash_id = $CI->Common_model->get_value('asset_status_history',$getValueData,'ash_id');

    $updateData = array('end_time' => date('Y-m-d H:i:s'),'modified_by'=>$created_by);
    $CI->Common_model->update_data('asset_status_history',$updateData,array('ash_id'=>$ash_id));

    # Insert asset status history
    $asset_status_history_data = array(
        'status'            => $status,
        'asset_id'          => $asset_id,
        'current_stage_id'  => $current_stage_id,                        
        'created_by'        => $created_by,
        'created_time'      => date('Y-m-d H:i:s')
    );
    if(isset($arr['tool_order_id']))
        $asset_status_history_data['tool_order_id'] = $arr['tool_order_id'];
    if(isset($arr['ordered_tool_id']))
        $asset_status_history_data['ordered_tool_id'] = $arr['ordered_tool_id'];
    
    $new_ash_id = $CI->Common_model->insert_data('asset_status_history',$asset_status_history_data);
}


function dataForPendingPickupStatus($rto_id,$exclude10=0)
{  
    $CI = &get_instance(); 
    $CI->db->select('to.order_number as fe1_order_number,to.tool_order_id as from_tool_order_id,
                    l.name as countryName,concat(u.sso_id," - ",u.name) as sso,ro.*,
                    rt.name as return_type,osh.order_status_id,rto.rto_id as rto_id,
                    to_order.order_number as to_order,concat(to_user.sso_id," - ",to_user.name) as to_sso,
                    to_order.current_stage_id as to_current_stage_id,ro.remarks as return_order_remarks,
                    concat(wh1.wh_code," - ",wh1.name) as wh1,concat(wh2.wh_code," - ",wh2.name) as wh2
                    ');     
    $CI->db->from('tool_order to');   
    $CI->db->join('return_tool_order rto','rto.tool_order_id = to.tool_order_id');
    $CI->db->join('return_order ro','ro.return_order_id = rto.return_order_id');
    $CI->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
    $CI->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
    $CI->db->join('user u','u.sso_id = to.sso_id');
    $CI->db->join('location l','l.location_id = ro.country_id','left');
    $CI->db->join('tool_order to_order','to_order.tool_order_id = ro.tool_order_id','left');
    $CI->db->join('user to_user','to_user.sso_id = to_order.sso_id','left');
    $CI->db->join('warehouse wh1','wh1.wh_id = ro.wh_id','left');
    $CI->db->join('warehouse wh2','wh2.wh_id = ro.ro_to_wh_id','left');   
    if($exclude10 == 0)            
        $CI->db->where('ro.status !=',10);
    $CI->db->where('rto.rto_id',$rto_id);
    $CI->db->where_in('ro.return_approval',array(0,1,2));
    $CI->db->group_by('ro.return_order_id');    
    $res = $CI->db->get();
    return $res->row_array();
}

function returnOrderStatusWithRtoId($rto_id,$exclude10=0)
{
    $statusData = dataForPendingPickupStatus($rto_id,$exclude10);
    //echo "<pre>";print_r($statusData);exit;
    $arr = array('tool_order_id'=>$statusData['from_tool_order_id'],'rto_id'=>$rto_id);                                                
    $return_stage_data = get_transaction_latest_record($arr);  
    //echo "<pre>" ;print_r($return_stage_data);exit;
    $return_stage = @$return_stage_data['current_stage_id'];
    $finalStatus = getReturnOrderStatus($statusData,$return_stage);    
    if($finalStatus =='')
    {
        return "NA";
    }
    else
    {
        return $finalStatus;
    }
}

function getAtWhLockinList($from)
{
    $ci=& get_instance();
    $from = $from;
    $to = $from+999;
    $query = "select asset_id, cal_due_date 
              from asset 
              where status in (1,2) 
              AND asset_id BETWEEN ".$from." and ".$to."";
    $res = $ci->db->query($query);
    return $res->result_array();
}
function getAshLastRecord()
{
    $CI = &get_instance();
    $CI->db->select('a.asset_number,ash.*,a.status as asset_main_status,a.wh_id');
    $CI->db->from('asset_status_history ash');
    $CI->db->join('asset a','a.asset_id = ash.asset_id');
    $CI->db->where('ash.end_time IS NULL');  // created by maruthi on 8th Oct'18
    $CI->db->where('a.status',2);
    $CI->db->order_by('ash.ash_id DESC');
    $CI->db->group_by('ash.asset_id');
    $CI->db->where('ash.tool_order_id IS NULL');  // created by maruthi on 8th Oct'18
    $res = $CI->db->get();    
    return $res->result_array();   
}

function getAshLastRecordEndTime()
{
    $CI = &get_instance();
    $CI->db->select('a.asset_number,ash.*,a.status as asset_main_status,a.wh_id');
    $CI->db->from('asset_status_history ash');
    $CI->db->join('asset a','a.asset_id = ash.asset_id');
    $CI->db->order_by('ash.ash_id DESC');
    $CI->db->group_by('ash.asset_id');
    $res = $CI->db->get();    
    return $res->result_array();   
}
function sendMailsEvenFEDeactivated()
{
    return 1;  //1 means don't send 2 means send
}
function getUserStatus($sso_id)
{
    $CI = &get_instance();
    if(sendMailsEvenFEDeactivated() == 1)
    {
        if($sso_id!='')
            return $CI->Common_model->get_value('user',array('sso_id'=>$sso_id),'status');
        else
            return 2;
    }
    else
    {
        return 1;
    }
}

#delete CR Request
function delete_cr_request($countryId,$rcNumber)
{
    $CI=&get_instance();
    if($countryId!='' && $rcNumber!=''){        
        $rc_asset_id = $CI->Common_model->get_value('rc_asset',array('rc_number'=>$rcNumber,'country_id'=>$countryId),'rc_asset_id');
        if($rc_asset_id != '') {
            $rc_row = $CI->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));
            $asset_id = $rc_row['asset_id'];

            #update rc asset //cancelled
            $update_rca = array(
                'status'           => 10,
                'current_stage_id' => 28,
                'modified_by'      => 0 ,
                'modified_time'    => date('Y-m-d H:i:s')
            );
            $update_rca_where = array('rc_asset_id'=>$rc_asset_id);
            $CI->Common_model->update_data('rc_asset',$update_rca,$update_rca_where);

            #update status only when out of calibration
            $asset_arr = $CI->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
            $flag = get_position_for_calibration($asset_id);
            if($flag == 1)
            {
                $new_status = 1;
            }
            else if($flag == 2)
            {
                $new_status = 3;
            }
            else if($flag == 4)
            {
                $new_status = 8;
            }
            else
            {
                $new_status = '';
            }
            if($asset_arr['status']==12 && $new_status !='')
            {
                #audit data
                $old_data = array('asset_status_id'=>$asset_arr['status']);
                $update_a = array(
                    'status'              => $new_status,
                    'approval_status'     => 0,
                    'availability_status' => 1,
                    'modified_by'         => 0,
                    'modified_time'       => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id'=>$asset_id);
                $CI->Common_model->update_data('asset',$update_a,$update_a_where);
            }    
        }        
    }
}
// sso id, value-1/data row -2, email-1,name-2
function getReportingManager($sso_id,$type=1,$returnValue=1)
{
    $CI = &get_instance();
    $returnValueData = '';
    $returnValueArray = array();
    if($sso_id!=''){
        if($type == 1) {
            $value = ($returnValue == 1)?"email":"name";     
            $returnValueData = $CI->Common_model->get_value('user',array('sso_id'=>$sso_id,'status'=>1),"$value");
            return $returnValueData;
        }
        else
        {
            $tempReturnValueArray = $CI->Common_model->get_data_row('user',array('sso_id'=>$sso_id,'status'=>1));
            if(count($tempReturnValueArray) == 0) 
                return $returnValueArray;
            else
                return $tempReturnValueArray;
        }
    }
    else
    {
        $return =($type == 1)?$returnValueData:$returnValueArray;
        return $return;
    }
}

function getTaskPreference($prefName,$country_id)
{
    $CI = &get_instance();
    $CI->db->select('lp.status');
    $CI->db->from('location_pref lp');
    $CI->db->join('preference p','lp.pref_id = p.pref_id');
    $CI->db->where('p.name',$prefName);
    $CI->db->where('lp.location_id',$country_id);
    $res = $CI->db->get();
    if($res->num_rows()>0){
        $result = $res->row_array();
        if($result['status'] == 1)
            return TRUE;
        else
            return FALSE;
    }else{
        return FALSE;
    }
}
?>
