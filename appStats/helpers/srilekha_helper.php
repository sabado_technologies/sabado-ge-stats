<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_ib_tool_list($search_list)
{
	//$status = array(1,2,3,4,8,10,12);
	$part_name = $search_list['part_name'];
	$ci=& get_instance();
    $limit = getDefaultSelect2Limit();
    $ci->db->select('t.*');
    $ci->db->from('tool t');
    $ci->db->join('part p','p.tool_id = t.tool_id');
    //$ci->db->join('asset a','a.asset_id = p.asset_id');
    if($search_list['part_name']!='')
    {
        $where = "(t.part_description LIKE '%$part_name%' OR t.part_number LIKE '%$part_name%')";
        $ci->db->where($where);
    }
    if($search_list['country_id']!=0 && $search_list['country_id']!='')
    {
    	$ci->db->where('t.country_id',$search_list['country_id']);
    }
    if($search_list['modality_id']!=0 && $search_list['modality_id']!='')
    {
    	$ci->db->where('t.modality_id',$search_list['modality_id']);
    }
    $ci->db->where('p.part_level_id',1);
    //$ci->db->where_in('a.status',$status);
    $ci->db->group_by('t.tool_id');
    $ci->db->order_by('t.tool_id ASC');
    $ci->db->limit($limit,0);
    $res=$ci->db->get();
    //echo $ci->db->last_query(); exit;
    $data = array();
    foreach($res->result_array() as $row)
    {
        $part_desc = $row['part_number'].' - '.$row['part_description'];
        $data[] = array('id'=>$row['tool_id'], 'text'=>$part_desc);
    }
    return $data;
}


function get_tools_result($part_name,$country_id)
{
    $ci=& get_instance();
    $limit = getDefaultSelect2Limit();
    $ci->db->select('t.*');
    $ci->db->from('tool t');
    if($part_name!='')
    {
        $where = "(t.part_description LIKE '%$part_name%' OR t.part_number LIKE '%$part_name%')";
        $ci->db->where($where);
    }
    if($country_id!=0)
    {
    	$ci->db->where('t.country_id',$country_id);
    }
    $ci->db->where('t.status',1);
    $ci->db->order_by('t.tool_id ASC');
    $ci->db->limit($limit,0);
    $res=$ci->db->get();
    //echo $ci->db->last_query(); exit;
    $data = array();
    foreach($res->result_array() as $row)
    {
        $part_desc = $row['part_number'].' - '.$row['part_description'];
        $data[] = array('id'=>$row['tool_id'], 'text'=>$part_desc);
    }
    return $data;
}

function get_tool_master_tools($part_name,$country_id)
{
    $ci=& get_instance();
    $limit = getDefaultSelect2Limit();
    $ci->db->select('t.*');
    $ci->db->from('tool t');
    if($part_name!='')
    {
        $where = "(t.part_description LIKE '%$part_name%' OR t.part_number LIKE '%$part_name%')";
        $ci->db->where($where);
    }
    if($country_id!=0)
    {
    	$ci->db->where('t.country_id',$country_id);
    }
    $ci->db->where('t.asset_level_id',3);
    $ci->db->where('t.status',1);
    $ci->db->order_by('t.tool_id ASC');
    $ci->db->limit($limit,0);
    $res=$ci->db->get();
    //echo $ci->db->last_query(); exit;
    $data = array();
    foreach($res->result_array() as $row)
    {
        $part_desc = $row['part_number'].' - '.$row['part_description'];
        $data[] = array('id'=>$row['tool_id'], 'text'=>$part_desc);
    }
    return $data;
}

// Get Asset Level Id
function get_asset_level_id($asset_level)
{
	if($asset_level==0)
	{
		return 1;
	}
	else if($asset_level==1)
	{
		return 2;
	}
	else if($asset_level==2)
	{
		return 3;
	}
}

function calibration_status()
{
	$calibration_status=array(1=>'In Calibration',2=>'Calibrated',3=>'Scrapped',4=>'Sent For Re-Calibration',5=>'Out Of Tolerance',6=>'Out Of Calibration',7=>'Cancelled');

	return $calibration_status;
}
//Get  Tool Id 
function get_top_tool_id($asset_id)
{
	$ci= & get_instance();
	$ci->db->select('p.tool_id');
	$ci->db->from('part p');
	$ci->db->join('asset a','a.asset_id = p.asset_id');
	$ci->db->where('a.asset_id',$asset_id);
	$ci->db->order_by('p.part_id ASC');
	$res = $ci->db->get();
	$result = $res->row_array();
	return $result['tool_id'];
}

// Get Tool Data
function asset_details_view($asset_id,$tool_id=0)
{
	$ci= & get_instance();
	$ci->db->select('t.part_description,t.part_number,t.tool_code,wh.name as warehouse,a.asset_number,a.cal_due_date');
	$ci->db->from('asset a');
	$ci->db->join('part p','a.asset_id = p.asset_id');
	$ci->db->join('tool t','p.tool_id = t.tool_id');
	$ci->db->join('warehouse wh','a.wh_id = wh.wh_id');
	$ci->db->where('a.asset_id',$asset_id);
	$ci->db->where('p.part_level_id',1);
	$ci->db->group_by('a.asset_id');
	$res = $ci->db->get();
	return $res->row_array();
}

// Get latest CR Number for Calibration
function get_rc_number($country_id)
{
	$ci=&get_instance();
	$ci->db->select('rc_number');
	$ci->db->from('rc_asset');
	$ci->db->where('rca_type',2);
	$ci->db->where('country_id',$country_id);
	$ci->db->order_by('rc_asset_id DESC');
	$res=$ci->db->get();
	return $res->row_array();
}

// Get latest CR Number for Calibration
function get_cr_number($country_id)
{
	$cr_number = '';
	$ci=&get_instance();
	$ci->db->select('cr_number');
	$ci->db->from('egm_calibration');
	$ci->db->where('country_id',$country_id);
	$ci->db->order_by('ec_id DESC');
	$res = $ci->db->get();
	$result = $res->row_array();
	if(isset($result['cr_number']))
	{
		$cr_number = $result['cr_number'];
	}
	else
	{
		$ci->db->select('rc_number');
		$ci->db->from('rc_asset');
		$ci->db->where('rca_type',2);
		$ci->db->where('country_id',$country_id);
		$ci->db->order_by('rc_asset_id DESC');
		$res = $ci->db->get();
		$result = $res->row_array();
		if(isset($result['rc_number']))
		{
			$cr_number = $result['rc_number'];
		}
	}
	return $cr_number;
}

// Get latest CRB number for Calibration
function get_rcb_number($country_id)
{
	$ci=&get_instance();
	$ci->db->select('rcb_number');
	$ci->db->from('rc_order');
	$ci->db->where('rc_type',2);
	$ci->db->where('country_id',$country_id);
	$ci->db->order_by('rc_order_id DESC');
	$res=$ci->db->get();
	return $res->row_array();
}
// Get latest CR number for Repair
function get_repair_rc_number($country_id)
{
	$ci=&get_instance();
	$ci->db->select('rc_number');
	$ci->db->from('rc_asset');
	$ci->db->where('rca_type',1);
	$ci->db->where('country_id',$country_id);
	$ci->db->order_by('rc_asset_id DESC');
	$res=$ci->db->get();
	return $res->row_array();
}
// Get latest CRB number for Repair
function get_repair_rcb_number($country_id)
{
	$ci=&get_instance();
	$ci->db->select('rcb_number');
	$ci->db->from('rc_order');
	$ci->db->where('rc_type',1);
	$ci->db->where('country_id',$country_id);
	$ci->db->order_by('rc_order_id DESC');
	$res=$ci->db->get();
	return $res->row_array();
}

// To Check Whether RC Request Has Been Raised Or Not
function check_for_rc_entry($asset_id)
{
	$ci=& get_instance();
	$ci->db->select('rc_asset_id');
	$ci->db->from('rc_asset');
	$ci->db->where('current_stage_id>=',11);
	$ci->db->where('current_stage_id<=',16);
	$ci->db->where('asset_id',$asset_id);
	$res = $ci->db->get();
	$result = $res->row_array();
	if(isset($result['rc_asset_id']))
	{
		return $result['rc_asset_id'];
	}
	else
	{
		return 0;
	}
}

// To Check Whether CR Request Has Been Raised Or Not for EGM
function check_for_cr_entry($asset_id)
{
	$ci=& get_instance();
	$ci->db->select('ec_id');
	$ci->db->from('egm_calibration');
	$ci->db->where_in('cal_status_id',array(1,2));
	$ci->db->where('asset_id',$asset_id);
	$res = $ci->db->get();
	$result = $res->row_array();
	if(isset($result['ec_id']))
	{
		return $result['ec_id'];
	}
	else
	{
		return 0;
	}
}

//To Know Asset Current Position
function get_asset_position($asset_id)
{
	$ci= & get_instance();
	$ci->db->select('wh_id,transit,sso_id,supplier_id');
	$ci->db->from('asset_position');
	$ci->db->where('asset_id',$asset_id);
	$ci->db->where('to_date IS NULL');
	$res = $ci->db->get();
	$arr=$res->row_array();
	if(count($arr)!='')
	{
		$wh_id=$arr['wh_id'];
		$transit=$arr['transit'];
		$sso_id=$arr['sso_id'];
		$supplier_id=$arr['supplier_id'];
		if($transit == 0)
		{
			if($wh_id !='')
			{
				$wh=$ci->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
				return 'At '.$wh['wh_code'].' -('.$wh['name'].')';
			}
			if($sso_id !='')
			{
				$sso_name=$ci->Common_model->get_value('user',array('sso_id'=>$sso_id),'name');
				return 'At '.$sso_id.' -('.$sso_name.')';
			}
			if($supplier_id !='')
			{
				$supplier=$ci->Common_model->get_data_row('supplier',array('supplier_id'=>$supplier_id));
				return 'At '.$supplier['supplier_code'].' -('.$supplier['name'].')';
			}
		}
		elseif ($transit == 1)
		{
			if($wh_id!='')
			{
				$wh_name=$ci->Common_model->get_value('warehouse',array('wh_id'=>$wh_id),'name');
				return 'In Transit To '.$wh_name;
			}
			if($sso_id !='')
			{
				$sso_name=$ci->Common_model->get_value('user',array('sso_id'=>$sso_id),'name');
				return 'In Transit To '.$sso_id.' -('.$sso_name.')';
			}
			if($wh_id=='' && $sso_id=='')
			{
				return 'Lost In Transit';
			}
		}
		else
		{
			return 'No Data Found';
		}
	}
}
// Get Asset position To check same warehouse in Calibration
function get_asset_position_with_flag($asset_id)
{
	$ci= & get_instance();
	$ci->db->select('ap.wh_id,ap.transit,ap.sso_id,ap.supplier_id,wh.status as wh_status');
	$ci->db->from('asset_position ap');
	$ci->db->join('warehouse wh','wh.wh_id = ap.wh_id','LEFT');
	$ci->db->where('ap.asset_id',$asset_id);
	$ci->db->where('ap.to_date IS NULL');
	$res = $ci->db->get();
	$arr=$res->row_array();
	if(count($arr)!='')
	{
		$wh_id=$arr['wh_id'];
		$transit=$arr['transit'];
		$sso_id=$arr['sso_id'];
		$supplier_id=$arr['supplier_id'];
		$wh_status = $arr['wh_status'];
		if($transit == 0)
		{
			if($wh_id !='')
			{
				if($wh_status == 1)
				{
					$wh=$ci->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
					return array('At '.$wh['wh_code'].' -('.$wh['name'].')',1);// 1 is Defined to know at WH
				}
				else
				{
					$wh=$ci->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
					return array('At '.$wh['wh_code'].' -('.$wh['name'].')',3);// 3 is Defined to know at Buffer Warehouse
				}
				
			}
			if($sso_id !='')
			{
				$sso_name=$ci->Common_model->get_value('user',array('sso_id'=>$sso_id),'name');
				return array('At '.$sso_id.' -('.$sso_name.')',2); // 2 is Defined to know AT FE
			}
			if($supplier_id !='')
			{
				$supplier=$ci->Common_model->get_data_row('supplier',array('supplier_id'=>$supplier_id));
				return array('At '.$supplier['supplier_code'].' -('.$supplier['name'].')',9);
			}
		}
		elseif ($transit == 1)
		{
			if($wh_id!='')
			{
				$wh_name=$ci->Common_model->get_value('warehouse',array('wh_id'=>$wh_id),'name');
				return array('In Transit To '.$wh_name,3); // 2 is Defined to know In Transit to WH
			}
			if($sso_id !='')
			{
				$sso_name=$ci->Common_model->get_value('user',array('sso_id'=>$sso_id),'name');
				return array('In Transit To '.$sso_id.'('.$sso_name.')',4); // 4 is Defined to know In Transit to FE
			}
			if($wh_id=='' && $sso_id=='')
			{
				return array('Lost In Transit',7); // 5 is Defined to know Lost In Transit 
			}
		}
		else
		{
			return array('No Data Found',8); // 8 is Defined to know No Data Found
		}
	}
}

function get_position_for_calibration($asset_id)
{
	$ci= & get_instance();
	$ci->db->select('ap.wh_id,ap.transit,ap.sso_id');
	$ci->db->from('asset_position ap');
	$ci->db->join('warehouse wh','wh.wh_id = ap.wh_id','LEFT');
	$ci->db->where('ap.asset_id',$asset_id);
	$ci->db->where('ap.to_date IS NULL');
	$res = $ci->db->get();
	$arr=$res->row_array();
	if(count($arr)!='')
	{
		$wh_id=$arr['wh_id'];
		$transit=$arr['transit'];
		$sso_id=$arr['sso_id'];
		if($transit == 0)
		{
			if($wh_id !='')
			{
				return 1; // 1 is defined to know at Wh
			}
			if($sso_id !='')
			{
				return 2; // 2 is Defined to know as field deployed
			}
		}
		else if ($transit == 1)
		{
			return 4; // 4 is Defined to know In Transit
		}
		else
		{
			return 10; //cannot identify the scenerio
		}
	}
}

function repair_document_path()
{
    return 'uploads/repair_upload/';
}

function get_repair_document_path()
{
    return SITE_URL1.'uploads/repair_upload/';
}
function scrap_document_path()
{
    return 'uploads/scrap_upload/';
}

function get_scrap_document_path()
{
    return SITE_URL1.'uploads/scrap_upload/';
}

// To get count of warehouse
// To get count of warehouse modified by maruthi on 15th may'18
function count_of_warehouse($tool_id,$sso_id,$transactionCountry=0)
{

	$ci=&get_instance();
	if($transactionCountry == 0 )
		$country_id = $ci->Common_model->get_value('user',array('sso_id'=>$sso_id),'country_id');
	else
		$country_id = $transactionCountry;
	$role_id = getRoleByUser($sso_id);

	$task_access = getPageAccess('raise_order',getRoleByUser($sso_id));
	$countriesData = get_countries_string(array('order_sso_id'=>$sso_id),1,$task_access); 
	//$countries = conver
	//echo $ci->db->last_query();exit;
	//echo '<pre>';print_r($countriesData);exit;

	$ci->db->select('w.name,w.wh_id,w.wh_code');
	$ci->db->from('tool t');
	$ci->db->join('part p','p.tool_id=t.tool_id');
	$ci->db->join('asset a','a.asset_id=p.asset_id');
	$ci->db->join('warehouse w','w.wh_id=a.wh_id');
	$ci->db->where('t.tool_id',$tool_id);
	$ci->db->where('a.status',1);
	$ci->db->where('w.status',1);	
	if($role_id == 5 || $role_id == 6)
	{		
		$ci->db->where('w.country_id',$country_id);
	}
	else
	{		
		if($task_access == 1 || $task_access == 2)
		{			
			$ci->db->where('w.country_id',$country_id);
		}
		else if($task_access == 3)
		{
			if($ci->session->userdata('header_country_id')!='')
			{
				$ci->db->where('w.country_id',$ci->session->userdata('header_country_id'));	
			}
			else
			{
				if(@$searchParams['country_id']!='')
				{
					$ci->db->where('w.country_id',$searchParams['country_id']);
				}
				else
				{
					$ci->db->where_in('w.country_id',$countriesData);
				}
			}
		}	
	}
	$ci->db->group_by('a.wh_id');
	$res=$ci->db->get();
	//echo $ci->db->last_query();exit;
	//echo '<pre>';print_r($res->result_array());exit;
	return $res->result_array();
}

// To Get Count of Availability
function count_of_availability($tool_id,$wh_id)
{
	$CI = & get_instance();
	$CI->db->select('t.*');
    $CI->db->from('tool t');   
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->join('warehouse w','a.wh_id = w.wh_id');
    $CI->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
    $CI->db->where('t.tool_id',$tool_id);
    $CI->db->where('a.wh_id',$wh_id);    
    $CI->db->group_by('p.asset_id');
    $CI->db->where('a.status',1);
    $CI->db->where('w.status',1);
    $CI->db->where('p.part_level_id',1);
    $CI->db->where('a.approval_status',0);
    $res = $CI->db->get();
    return $res->result_array();
}

// To get Tool Details //maruthi
function get_tool_sub_parts($tool_id)
{
	$CI = & get_instance();
	$CI->db->select('tp.tool_sub_id');
	$CI->db->from('tool t');
	$CI->db->join('tool_part tp','tp.tool_main_id=t.tool_id','left');
	$CI->db->where('t.tool_id',$tool_id);
	$res = $CI->db->get();
	return $res->result_array();
}

//maruthi
function get_tool_parts($tool_sub_id)
{
	$CI = & get_instance();
	$CI->db->select('t.part_number,t.part_description,t.part_level_id');
	$CI->db->from('tool t');
	$CI->db->where('t.tool_id',$tool_sub_id);
	$res1=$CI->db->get();
	return $res1->row_array();
}

// Count Notification Total srilekha 30-05-2018
function get_notification_count($val)
{
	$notification_count = 0;
	if($val>=1 && $val<=999)
	{
		$notification_count = $val;
	}
	elseif($val>=1000 && $val<=99999)
	{
		$total_count = $val/1000;
		$total = explode('.', $total_count);
		$count = $total[0];
		if(@count($total[1])>0)
		{
			$notification_count = $count.'K+';
		}
		else
		{
			$notification_count = $count.'K';
		}
	}
	elseif($val>=100000 && $val<=9999999)
	{
		$total_count = $val/100000;
		$total = explode('.', $total_count);
		$count = $total[0];
		if(@count($total[1])>0)
		{
			$notification_count = $count.'L+';
		}
		else
		{
			$notification_count = $count.'L';
		}
	}
	return $notification_count;
}

// Raise Calibration request Except Bangladesh & Srilanka
function insert_calibration($countryId=0)
{
	$ci= & get_instance();
	$status=array(4,5,6,7,9,10,11,12);
	$wh_arr=get_warehouse_id_for_bs();
	$ci->db->select('a.asset_id,a.wh_id,DATEDIFF(a.cal_due_date,now())as days,a.country_id,a.status');
	$ci->db->from('asset a');
	$ci->db->join('part p','p.asset_id=a.asset_id');
	$ci->db->join('tool t','t.tool_id=p.tool_id');
	$ci->db->join('location l','l.location_id = a.country_id');
	$ci->db->where('l.region_id !=',2);//exclude EGM Assets
	$ci->db->where('t.cal_type_id',1);
	$ci->db->where('p.part_level_id',1);
	$ci->db->where_not_in('a.status',$status);
	if($countryId !=0)
		$ci->db->where('a.country_id',$countryId);
	$res = $ci->db->get();
	$result = $res->result_array();
	foreach($result as $row)
	{
		// Check Whether RC Number Has Raised Or Not
		$rc_check=check_for_rc_entry($row['asset_id']);
		if($rc_check==0)
		{
			if(in_array($row['wh_id'], $wh_arr))
			{
				$check_days = 90;
			}
			else
			{
				$check_days = 45;
			}
			if($row['days']>$check_days)
			{
				continue;
			}
			else
			{
				// Dynamic CR number Generarion
				$number=get_rc_number($row['country_id']);
	            if(count($number) == 0)
	            {
	                $f_order_number = "1-00001";
	            }
	            else
	            {
	                $num = ltrim($number['rc_number'],"C");
	                $result = explode("-", $num);
	                $running_no = (int)$result[1];

	                if($running_no == 99999)
	                {
	                    $first_num = $result[0]+1;
	                    $second_num = get_running_sno_five_digit(1);  
	                }
	                else
	                {
	                    $first_num = $result[0];
	                    $val = $running_no+1;
	                    $second_num = get_running_sno_five_digit($val);
	                }
	                $f_order_number = $first_num.'-'.$second_num;
	            }
	            $order_number = 'C'.$f_order_number;
				$data= array(
				'rc_number'			=>	$order_number,
				'asset_id'			=>	$row['asset_id'],
				'current_stage_id'	=>	11,
				'country_id'		=>  $row['country_id'],
				'rca_type'			=>	2,
				'created_by'		=>  0,
				'created_time'		=>	date('Y-m-d H:i:s'),
				'status'			=>	1
				);
				// Insert Data into Calibration
				$rc_asset_id=$ci->Common_model->insert_data('rc_asset',$data);
				
				// Insertdata into rc_status_history
				$rc_status_history=array(
				'rc_asset_id'		=>	$rc_asset_id,
				'current_stage_id'	=>	11,
				'created_by'		=>	0,
				'created_time'		=>	date('Y-m-d H:i:s'),
				'status'			=>	1
				);
				$rcsh_id=$ci->Common_model->insert_data('rc_status_history',$rc_status_history);
				// Insert Data into cal_asset_history
				$rc_asset_history=array(
				'rc_asset_id'			=>	$rc_asset_id,
				'rcsh_id'				=>	$rcsh_id,
				'created_by'		    =>  0,
				'created_time'		    =>	date('Y-m-d H:i:s'),
				'status'				=>	1,
				);
				$rcah_id = $ci->Common_model->insert_data('rc_asset_history',$rc_asset_history);
				
				$part_list=$ci->Common_model->get_data('part',array('asset_id'=>$row['asset_id'],'part_status'=>1));
				foreach($part_list as $parts)
				{
					$part_arr=array(
					'part_id'				=>	$parts['part_id'],
					'asset_condition_id'	=>	$parts['status'],
					'rcah_id'				=>	$rcah_id,
					'created_by'		    =>  0,
					'created_time'		    =>	date('Y-m-d H:i:s'),
					);
					$ci->Common_model->insert_data('rc_asset_health',$part_arr);
				}

				if($row['days']<0)
				{
					#audit data
					$old_data = array('asset_status_id' => $row['status']);

					$update=$ci->Common_model->update_data('asset',array('status'=>12),array('asset_id'=>$row['asset_id']));

					#audit data
					$remarks = "Changed Status In Daily Scheduler";
					$new_data = array('asset_status_id' => 12);
					audit_data('asset_master',$row['asset_id'],'asset',2,'',$new_data,array('asset_id'=>$row['asset_id']),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$row['country_id']);

					// update old asset status history with end time 

					$ash_id=$ci->Common_model->get_value('asset_status_history',array('asset_id'=>$row['asset_id'],'end_time'=>NULL),'ash_id');
					$ash_update=array(
					'end_time'		=>	date('Y-m-d H:i:s'),
					'modified_by'	=>	0
					);
					$ci->Common_model->update_data('asset_status_history',$ash_update,array('ash_id'=>$ash_id));
					//insert new asset status history
					$data=array(
					'status'		    =>	12,
					'asset_id'		    =>	$row['asset_id'],
					'current_stage_id'	=>  11,
					'created_by'		=>  0,
					'created_time'		=>	date('Y-m-d H:i:s')
					);
					$ci->Common_model->insert_data('asset_status_history',$data);
				}
			}
		}
		else
		{
			if($row['days']<0)
			{
				#audit data
				$old_data = array('asset_status_id' => $row['status']);

				$update=$ci->Common_model->update_data('asset',array('status'=>12),array('asset_id'=>$row['asset_id']));

				#audit data
				$remarks = "Changed Status In Daily Scheduler";
				$new_data = array('asset_status_id' => 12);
				audit_data('asset_master',$row['asset_id'],'asset',2,'',$new_data,array('asset_id'=>$row['asset_id']),$old_data,$remarks,'',array(),'rc_asset',$rc_check,$row['country_id']);

				// update old asset status history with end time 
				$ash_id=$ci->Common_model->get_value('asset_status_history',array('asset_id'=>$row['asset_id'],'end_time'=>NULL),'ash_id');
				$ash_update=array(
				'end_time'		=>	date('Y-m-d H:i:s'),
				'modified_by'	=>	0
				);
				$ci->Common_model->update_data('asset_status_history',$ash_update,array('ash_id'=>$ash_id));

				$data=array(
				'status'		    =>	12,
				'asset_id'		    =>	$row['asset_id'],
				'current_stage_id'	=>  11,
				'created_by'		=>  0,
				'created_time'		=>	date('Y-m-d H:i:s')
				);
				$ci->Common_model->insert_data('asset_status_history',$data);
			}
		}
	}
}

//raise cal for EGM
function insert_calibration_egm()
{
	$ci= & get_instance();
	$status=array(4,5,6,7,9,10,11,12);
	$ci->db->select('a.asset_id,DATEDIFF(a.cal_due_date,now())as days,a.status,a.country_id');
	$ci->db->from('asset a');
	$ci->db->join('part p','p.asset_id=a.asset_id');
	$ci->db->join('tool t','t.tool_id=p.tool_id');
	$ci->db->join('location l','l.location_id = a.country_id');
	$ci->db->where('l.region_id',2);//Only EGM Assets
	$ci->db->where('t.cal_type_id',1);
	$ci->db->where('p.part_level_id',1);
	$ci->db->where_not_in('a.status',$status);
	$res = $ci->db->get();
	$result = $res->result_array();
	foreach($result as $row)
	{
		// Check Whether CR Number Has Raised Or Not
		$ec_id=check_for_cr_entry($row['asset_id']);
		if($ec_id==0)
		{
			$check_days = 45;
			if($row['days']>$check_days)
			{
				continue;
			}
			else
			{
				// Dynamic CR number Generarion
				$cr_number=get_cr_number($row['country_id']);
	            if($cr_number == '')
	            {
	                $f_order_number = "1-00001";
	            }
	            else
	            {
	                $num = ltrim($cr_number,"C");
	                $result = explode("-", $num);
	                $running_no = (int)$result[1];

	                if($running_no == 99999)
	                {
	                    $first_num = $result[0]+1;
	                    $second_num = get_running_sno_five_digit(1);  
	                }
	                else
	                {
	                    $first_num = $result[0];
	                    $val = $running_no+1;
	                    $second_num = get_running_sno_five_digit($val);
	                }
	                $f_order_number = $first_num.'-'.$second_num;
	            }
	            $order_number = 'C'.$f_order_number;

	            // Insert Data into EGM Calibration
				$e_data = array(
					'cr_number'		=> $order_number,
					'asset_id'		=> $row['asset_id'],
					'cal_status_id'	=> 1,
					'country_id'	=> $row['country_id'],
					'created_by'	=> 0,
					'created_time'	=> date('Y-m-d H:i:s'),
					'status'		=> 1,
					'remarks'	    => 'System Generated'
				);
				$ec_id = $ci->Common_model->insert_data('egm_calibration',$e_data);
				
				// Insertdata into egm_cal_history
				$ech_data=array(
					'ec_id'		    => $ec_id,
					'cal_status_id'	=> 1,
					'start_time'	=> date('Y-m-d H:i:s'),
					'remarks'	    => 'System Generated'
				);
				$ech_id = $ci->Common_model->insert_data('egm_cal_history',$ech_data);

				if($row['days']<0)
				{
					#audit data
					$old_data = array('asset_status_id' => $row['status']);

					$update=$ci->Common_model->update_data('asset',array('status'=>12),array('asset_id'=>$row['asset_id']));

					#audit data
					$remarks = "Changed Status In Daily Scheduler";
					$new_data = array('asset_status_id' => 12);
					audit_data('asset_master',$row['asset_id'],'asset',2,'',$new_data,array('asset_id'=>$row['asset_id']),$old_data,$remarks,'',array(),'egm_calibration',$ec_id,$row['country_id']);

					// update old asset status history with end time 
					$ash_id=$ci->Common_model->get_value('asset_status_history',array('asset_id'=>$row['asset_id'],'end_time'=>NULL),'ash_id');

					$ash_update=array(
						'end_time'	  => date('Y-m-d H:i:s'),
						'modified_by' => 0
					);
					$ci->Common_model->update_data('asset_status_history',$ash_update,array('ash_id'=>$ash_id));

					//insert new asset status history
					$data=array(
						'status'       => 12,
						'asset_id'	   => $row['asset_id'],
						'created_by'   => 0,
						'created_time' => date('Y-m-d H:i:s')
					);
					$ci->Common_model->insert_data('asset_status_history',$data);
				}
			}
		}
		else if($row['days']<0)
		{
			#audit data
			$old_data = array('asset_status_id' => $row['status']);

			$update=$ci->Common_model->update_data('asset',array('status'=>12),array('asset_id'=>$row['asset_id']));

			#audit data
			$remarks = "Changed Status In Daily Scheduler";
			$new_data = array('asset_status_id' => 12);
			audit_data('asset_master',$row['asset_id'],'asset',2,'',$new_data,array('asset_id'=>$row['asset_id']),$old_data,$remarks,'',array(),'egm_calibration',$ec_id,$row['country_id']);

			//update old asset status history with end time
			$ash_id = $ci->Common_model->get_value('asset_status_history',array('asset_id'=>$row['asset_id'],'end_time'=>NULL),'ash_id');

			$ash_update=array(
				'end_time'	  => date('Y-m-d H:i:s'),
				'modified_by' => 0
			);
			$ci->Common_model->update_data('asset_status_history',$ash_update,array('ash_id'=>$ash_id));

			//insert new asset status history
			$data=array(
				'status'	   => 12,
				'asset_id'	   => $row['asset_id'],
				'created_by'   => 0,
				'created_time' => date('Y-m-d H:i:s')
			);
			$ci->Common_model->insert_data('asset_status_history',$data);
		}
	}
}

function update_egm_calibration($arr)
{
	$ci =&get_instance();
	$asset_id      = $arr['asset_id'];
	$asset_status  = $arr['asset_status'];
	$old_data      = $arr['old_data'];
	$a_remarks     = $arr['a_remarks'];
	$new_data      = $arr['new_data'];
	$cal_date      = $arr['cal_date'];
	$cal_due_date  = $arr['cal_due_date'];
	$ec_id         = $arr['ec_id'];
	$cal_status_id = $arr['cal_status_id'];
	$ec_remarks    = $arr['ec_remarks'];
	$ec_status     = $arr['ec_status'];
  	$country_id    = $arr['country_id'];

	#update EGM calibration
    $update_ec = array(
        'cal_status_id' => $cal_status_id,
        'remarks'       => $ec_remarks,
        'modified_by'   => $ci->session->userdata('sso_id'),
        'modified_time' => date('Y-m-d H:i:s'),
        'status'        => $ec_status
    );
    $update_ec_where = array('ec_id' => $ec_id);
    $ci->Common_model->update_data('egm_calibration',$update_ec,$update_ec_where);

    #update EGM cal history
    $update_ech = array('end_time' => date('Y-m-d H:i:s'));
    $update_ech_where = array('end_time' => NULL,'ec_id' => $ec_id);
    $ci->Common_model->update_data('egm_cal_history',$update_ech,$update_ech_where);

    #Insertdata into egm_cal_history
    $ech_data = array(
        'ec_id'         => $ec_id,
        'cal_status_id' => $cal_status_id,
        'start_time'    => date('Y-m-d H:i:s'),
        'remarks'       => $ec_remarks
    );
    $ech_id = $ci->Common_model->insert_data('egm_cal_history',$ech_data);

    #update asset
    $update_a = array(
        'status'        => $asset_status,
        'modified_by'   => $ci->session->userdata('sso_id'),
        'modified_time' => date('Y-m-d H:i:s')
    );
    if($cal_date!='')
    {
    	$update_a['calibrated_date'] = $cal_date;
    	$update_a['cal_due_date'] = $cal_due_date;

    	$insert_cddh = array(
            'calibrated_date' => $cal_date,
            'cal_due_date'    => $cal_due_date,
            'asset_id'        => $asset_id,
            'created_by'      => $ci->session->userdata('sso_id'),
            'created_time'    => date('Y-m-d H:i:s'),
            'status'          => 1
        );
        $ci->Common_model->insert_data('cal_due_date_history',$insert_cddh);
    }

    $update_a_where = array('asset_id' => $asset_id);
    $ci->Common_model->update_data('asset',$update_a,$update_a_where);

    #audit trail
    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$a_remarks,'',array(),'egm_calibration',$ec_id,$country_id);

    #update asset status history
    $update_ash = array(
    	'end_time'    => date('Y-m-d H:i:s'),
    	'modified_by' => $ci->session->userdata('sso_id')
    );
    $update_ash_where = array('end_time' => NULL,'asset_id' => $asset_id);
    $ci->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

    #insert asset status history
    $insert_ash = array(
    	'asset_id'     => $asset_id,
    	'status'       => $asset_status,
    	'created_by'   => $ci->session->userdata('sso_id'),
    	'created_time' => date('Y-m-d H:i:s')
    );
    $ci->Common_model->insert_data('asset_status_history',$insert_ash);
}

function cancel_egm_cr_entry($asset_id)
{
	$ci =&get_instance();

	$ec_id = check_for_cr_entry($asset_id);
	if($ec_id != '')
	{
		#update EGM calibration
	    $update_ec = array(
	        'cal_status_id' => 7,//cancelled
	        'remarks'       => 'cancelled',
	        'modified_by'   => $ci->session->userdata('sso_id'),
	        'modified_time' => date('Y-m-d H:i:s'),
	        'status'        => 10
	    );
	    $update_ec_where = array('ec_id' => $ec_id);
	    $ci->Common_model->update_data('egm_calibration',$update_ec,$update_ec_where);

	    #update EGM cal history
	    $update_ech = array('end_time' => date('Y-m-d H:i:s'));
	    $update_ech_where = array('end_time' => NULL,'ec_id' => $ec_id);
	    $ci->Common_model->update_data('egm_cal_history',$update_ech,$update_ech_where);

	    #Insertdata into egm_cal_history
	    $ech_data = array(
	        'ec_id'         => $ec_id,
	        'cal_status_id' => 7,
	        'start_time'    => date('Y-m-d H:i:s'),
	        'remarks'       => 'cancelled'
	    );
	    $ech_id = $ci->Common_model->insert_data('egm_cal_history',$ech_data);
	}
	return 0;
}

function get_cr_remarks($asset_id)
{
	$ci =&get_instance();
	$cr_remarks = '';
	$ci->db->select('ec_id, cr_number, remarks');
	$ci->db->from('egm_calibration');
	$ci->db->where('asset_id',$asset_id);
	$ci->db->where('cal_status_id',5);
	$ci->db->order_by('ec_id DESC');
	$ci->db->limit(1);
	$res = $ci->db->get();
	$result = $res->row_array();

	if(count($result)>0)
	{
		$ci->db->select('ec_id');
		$ci->db->from('egm_calibration');
		$ci->db->where('asset_id',$asset_id);
		$ci->db->order_by('ec_id DESC');
		$ci->db->limit(1);
		$res = $ci->db->get();
		$result2 = $res->row_array();
		
		$ec_id = $result['ec_id'];
		$match_id = $result2['ec_id'];
		if($ec_id == $match_id)
		{
			$cr_remarks = $result['remarks'].' (CR: '.$result['cr_number'].')';
		}
	}
	return $cr_remarks;
}