<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * getting location details....
 * created by Roopa on 17th june 2017..
*/
function getParentLocation($location_id)
{
  $CI = & get_instance();
  if($location_id != '')
  {
    $q = "SELECT l1.location_id, CASE WHEN (l1.level_id = 2) then l1.name else
        concat(l1.name, ' (', l2.name, ')') end as name from location l
        LEFT JOIN location l1 on l1.location_id = l.parent_id
        LEFT JOIN location l2 on l2.location_id = l1.parent_id
        WHERE l.location_id = '".$location_id."'";
    $res = $CI->db->query($q);
    if($res->num_rows() > 0)
    {
      $data = $res->result_array();
      return $data[0];
    }
    else
      return array('location_id' => '', 'name' => '-Select Parent-');   
  }
  else
    return array('location_id' => '', 'name' => '-Select Parent-');
}

function get_country_details()
{
    $CI = & get_instance();
    $CI->db->select('l.*');
    $CI->db->from('location l');            
    $CI->db->where('tl.level_id','2');
    $CI->db->join('territory_level tl ','tl.level_id = l.level_id');
    $res = $CI->db->get();
    if($res->num_rows()>0)    
        return $res->result_array();   
    else
        return FALSE;    
}
function get_zone_details()
  {
    $CI = & get_instance();
    $CI->db->select('l.location_id,concat(l.name," (",l1.name,") ") as name');
    $CI->db->from('location l');    
    $CI->db->join('location l1','l1.location_id = l.parent_id','left');
    $CI->db->where('tl.level_id', '3');
    $CI->db->join('territory_level tl ','tl.level_id = l.level_id');
    $res=$CI->db->get();
    return $res->result_array();
  }
  function get_state_details()
  {
    $CI = & get_instance();
    $CI->db->select('l.location_id,concat(l.name," (",l1.name,") ") as name');
    $CI->db->from('location l');    
    $CI->db->join('location l1','l1.location_id = l.parent_id','left');
    $CI->db->where('tl.level_id', '4');
    $CI->db->join('territory_level tl ','tl.level_id = l.level_id');
    $res=$CI->db->get();
    return $res->result_array();
  }
  function get_city_details()
  {
    $CI = & get_instance();
    $CI->db->select('l.location_id,concat(l.name," (",l1.name,") ") as name');
    $CI->db->from('location l');    
    $CI->db->join('location l1','l1.location_id = l.parent_id','left');
    $CI->db->where('tl.level_id', '5');
    $CI->db->join('territory_level tl ','tl.level_id = l.level_id');
    $res=$CI->db->get();
    return $res->result_array();
  }

  function ajax_get_zone_by_country_id($country_id)
{
    $CI = & get_instance();
    $CI->db->select('l.location_id,concat(l.name," (",l1.name,") ") as name');
    $CI->db->from('location l');      
    $CI->db->join('location l1','l1.location_id = l.parent_id','left');
    $CI->db->where('l1.location_id',$country_id);
    $CI->db->where('l.status', 1);
    $CI->db->join('territory_level tl ','tl.level_id = l.level_id');
    $res = $CI->db->get();
    $zone_results="<option value=''>-Select Zone-</option>";
    if($res->num_rows()>0)
    {
        foreach($res->result_array() as $zone)
        {
            $zone_results.='<option value="'.$zone['location_id'].'">'.$zone['name'].'</option>';      
        }
    }
    else
    {
        $zone_results.="<option value=''>-No Data Found-</option>"; 
    }
    return $zone_results;
}
function ajax_get_state_by_zone_id($zone_id)
{
    $CI = & get_instance();
    $CI->db->select('l.*');
    $CI->db->from('location l');            
    $CI->db->where('tl.level_id', 4);
    $CI->db->where('l.parent_id',$zone_id);
    $CI->db->where('l.status', 1);
    $CI->db->join('territory_level tl ','tl.level_id = l.level_id');
    $res = $CI->db->get();
    $state_results="<option value=''>-Select State-</option>";

    if($res->num_rows()>0)
    {
        foreach($res->result_array() as $state)
        {
            $state_results.='<option value="'.$state['location_id'].'">'.$state['name'].'</option>';      
        }
    }
    else
    {
        $state_results.="<option value=''>-No Data Found-</option>"; 
    } 
    return $state_results;
}

function ajax_get_city_by_state_id($state_id)
{
    $CI = & get_instance();
    $CI->db->select('l.*');
    $CI->db->from('location l');            
    $CI->db->where('tl.level_id', 5);
    $CI->db->where('l.parent_id',$state_id);
    $CI->db->where('l.status', 1);
    $CI->db->join('territory_level tl ','tl.level_id = l.level_id');
    $res = $CI->db->get();
    $city_results="<option value=''>-Select City-</option>";   

    if($res->num_rows()>0)
    {   
        foreach($res->result_array() as $city)
        {
            $city_results.='<option value="'.$city['location_id'].'">'.$city['name'].'</option>';      
        }
    }
    else
    {
        $city_results.="<option value=''>-No Data Found-</option>";
    }
    return $city_results;
}

function ajax_get_area_by_city_id($city_id)
{
    $CI = & get_instance();
    $CI->db->select('l.*');
    $CI->db->from('location l');            
    $CI->db->where('tl.level_id', 6);
    $CI->db->where('l.parent_id',$city_id);
    $CI->db->where('l.status', 1);
    $CI->db->join('territory_level tl ','tl.level_id = l.level_id');
    $res = $CI->db->get();
    $area_results="<option value=''>-Select Area-</option>";   

    if($res->num_rows()>0)
    {   
        foreach($res->result_array() as $area)
        {
            $area_results.='<option value="'.$area['location_id'].'">'.$area['name'].'</option>';      
        }
    }
    else
    {
        $area_results.="<option value=''>-No Data Found-</option>";
    }
    return $area_results;
}
/*
 * getting asset number by ID details....
 * created by Roopa on 23rd june 2017..
*/

function get_asset_number_by_id($asset_id)
{
    $CI = & get_instance();
    $CI->db->select('a.asset_number');
    $CI->db->from('asset a');
    $CI->db->where('a.asset_id',$asset_id);
    $res=$CI->db->get();
    $result = $res->row_array();
    return $result['asset_number'];
}