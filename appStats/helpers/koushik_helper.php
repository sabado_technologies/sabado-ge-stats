<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');//Koushik Helper

function get_assets_with1_2()
{ 
    $ci=& get_instance();
    $ci->db->select('a.asset_id,a.wh_id,ap.wh_id as ap_wh_id');
    $ci->db->from('asset a');
    $ci->db->join('asset_position ap','ap.asset_id = a.asset_id AND a.wh_id != ap.wh_id');
    $ci->db->where_in('a.status',array(1,2));
    $ci->db->where('ap.wh_id IS NOT NULL');
    $ci->db->where('ap.to_date IS NULL');
    $ci->db->where('ap.transit',0);
    $ci->db->group_by('a.asset_id');
    $res=$ci->db->get();
    return $res->result_array();
}       

function check_for_rc_status_entry($asset_id)
{
    $ci = & get_instance();
    $ci->db->select('current_stage_id');
    $ci->db->from('rc_asset');
    $ci->db->where('current_stage_id>=',11);
    $ci->db->where('current_stage_id<=',16);
    $ci->db->where('asset_id',$asset_id);
    $res = $ci->db->get();
    $result = $res->row_array();
    if(isset($result['current_stage_id']))
    {
        return $result['current_stage_id'];
    }
    else
    {
        return 0;
    }
}

function get_asset_listwith_1($from)
{
    $ci=& get_instance();
    $from = $from;
    $to = $from+999;
    $query = "select asset_id, cal_due_date 
              from asset 
              where status = 1 
              AND asset_id BETWEEN ".$from." and ".$to."";
    $res = $ci->db->query($query);
    return $res->result_array();
}

function get_for_scanned_assets($asset_id)
{
    $ci=& get_instance();
    $ci->db->select('oa.asset_id');
    $ci->db->from('tool_order to');
    $ci->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
    $ci->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
    $ci->db->join('order_asset_history oah','oah.ordered_asset_id = oa.ordered_asset_id');
    $ci->db->where('oa.asset_id',$asset_id);
    $ci->db->where('oa.status <',3);
    $ci->db->where_in('to.current_stage_id',array(1,5));
    $res = $ci->db->get();
    $result = $res->row_array();
    if(isset($result['asset_id']))
    {
        return $result['asset_id'];
    }
    else
    {
        return 0;
    }
}

function get_ash($asset_id)
{
    $ci=& get_instance();
    $ci->db->select('asset_id');
    $ci->db->from('asset_status_history');
    $ci->db->where('status',2);
    $ci->db->where('asset_id',$asset_id);
    $ci->db->where('((ordered_tool_id IS NOT NULL AND end_time IS NULL)');
    $ci->db->or_where('(current_stage_id = 3 AND end_time IS NULL )');
    $ci->db->or_where('(current_stage_id = 1))');
    $ci->db->order_by('ash_id DESC');
    $ci->db->group_by('asset_id');
    $ci->db->limit(1);
    $res=$ci->db->get();
    return $res->num_rows();
}

function check_for_rr_status_entry($asset_id)
{
    $ci=& get_instance();
    $ci->db->select('asset_id');
    $ci->db->from('rc_asset');
    $ci->db->where('current_stage_id>=',20);
    $ci->db->where('current_stage_id<=',23);
    $ci->db->where('asset_id',$asset_id);
    $res=$ci->db->get();
    return $res->num_rows();
}

function validate_fe2_order_number($type)//1=client validation,2=server validation
{
    $ci = & get_instance();
    $tool_order_id = validate_number($ci->input->post('tool_order_id',TRUE));
    if($type == 1)
    {
        $order_number = validate_string($ci->input->post('fe2_order_number',TRUE));
    }
    else
    {
        $order_number = validate_string($ci->input->post('order_number',TRUE));
    }
    $owned_asset_count = validate_number($ci->input->post('owned_assets_count',TRUE));
    $order_status_id = validate_number($ci->input->post('order_status_id',TRUE));
    $oah_id_with_ordered_asset_id = $ci->input->post('oah_id',TRUE);
    $oah_condition_id = $ci->input->post('oah_condition_id',TRUE);
    $oa_health_id_arr = $ci->input->post('oa_health_id',TRUE);
    $remarks_arr = $ci->input->post('remarks',TRUE);
    $oah_oa_health_id_part_id = $ci->input->post('oah_oa_health_id_part_id',TRUE); // to get part ID
    $check_address = validate_number($ci->input->post('check_address',TRUE));
    $address_remarks = validate_string($ci->input->post('address_remarks',TRUE));
    $fe1_order_number = validate_string($ci->input->post('fe1_order_number',TRUE));
    $return_type_id = validate_number($ci->input->post('return_type_id',TRUE));
    $order_delivery_type_id = validate_number($ci->input->post('delivery_type_id',TRUE));
    $to_wh_id = validate_string($ci->input->post('to_wh_id',TRUE));
    $from_wh_id = validate_string($ci->input->post('from_wh_id',TRUE));
    $zonal_wh_id = validate_string($ci->input->post('zonal_wh_id',TRUE));
    $site_id = validate_string($ci->input->post('site_id',TRUE));
    $system_id = validate_string($ci->input->post('system_id',TRUE));

    $tool_order_arr = $ci->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
    $returnUser = $tool_order_arr['sso_id'];
    $returnUserCountry = $tool_order_arr['country_id'];
    $sso_id = $returnUser;
    $conditionRole = getRoleByUser($returnUser);
    $conditionAccess = getPageAccess('raise_pickup',$conditionRole);
    $error_message = '';
    #checking the condition whether fe2 needed fe1 assets or not
    if(@$return_type_id == 3 || @$return_type_id == 4)
    {
        $fe2_tool_order_id_dat = $ci->Common_model->get_data('tool_order',array('order_number'=>trim($order_number),'current_stage_id'=>4,'country_id'=>$returnUserCountry));

        #if FE2 order number is invalid
        if(count($fe2_tool_order_id_dat) == 0)
        {
            if($type==1)
            {
                $error_message.='Sorry! Order Number :'.$order_number.' Not Found !';
                $data = array('error_message'=>$error_message);
                echo json_encode($data); exit();
            }
            else
            {
                $ci->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> Order Number :<strong>'.$order_number.'</strong> Not Found !</div>');
                redirect(SITE_URL.'raise_pickup');  exit();
            }
        }

        $fe2_tool_order_id = $fe2_tool_order_id_dat[0]['tool_order_id'];

        #check the FE2 order is All Tools Available Quantity Zero Or Not
        $all_quantity = $ci->Common_model->get_data('ordered_tool',array('available_quantity >'=>0,'tool_order_id'=>$fe2_tool_order_id));

        if(count($all_quantity) > 0)
        {
            if($type==1)
            {
                $error_message.='Sorry! FE can not Send the Tools to Partially Fullfilled Order.';
                $data = array('error_message'=>$error_message);
                echo json_encode($data); exit();
            }
            else
            {
                $ci->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> FE can not Send the Tools to Partially Fullfilled Order.</div>');
                redirect(SITE_URL.'raise_pickup'); exit;
            }
        }

        $total_required_quantity = $ci->Order_m->get_total_ordered_quantity($fe2_tool_order_id);
        $post_tot_qty = count($oah_id_with_ordered_asset_id);
        if($total_required_quantity != $post_tot_qty)
        {
            if($type==1)
            {
                $error_message.='Sorry! The Tools Which You Wish To Send is Partially Fullfilling the Fe2 Order Request.';
                $data = array('error_message'=>$error_message);
                echo json_encode($data); exit();
            }
            else
            {
                $ci->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> The Tools Which You Wish To Send to Other FE, Is Partially Fullfilling the other Fe Order.</div>'); 
                redirect(SITE_URL.'raise_pickup'); exit();
            }
        }
        $fe2_pending_tools = $ci->Common_model->get_data('ordered_tool',array('tool_order_id'=>$fe2_tool_order_id,'status'=>2));
        if(count($fe2_pending_tools)>0)
        {
            #checking this asset belongs to that tool category
            $fe2_tools_arr  = array();
            $fe2_assets_arr = array();
            $fe1_assets_arr = array();
            $failed_string  = '';
            $error = 0;
            foreach ($fe2_pending_tools as $key => $tools)
            {
                $searchParams = array('tool_id'=>$tools['tool_id'],'country_id'=>$returnUserCountry);
                $tool_assets_data = tool_assets($searchParams);
                foreach ($tool_assets_data as $key => $value)
                {
                     $fe2_tools_arr[] = $value['asset_id'];
                }
            }
            foreach ($oah_id_with_ordered_asset_id as $oah_id => $ordered_asset_id)
            {
                $oa_arr = $ci->Order_m->get_order_asset_arr($ordered_asset_id);
                $asset_id = $oa_arr['asset_id'];
                $asset_number = $oa_arr['asset_number'];
                $part_description = $oa_arr['part_description'];

                if(!in_array($asset_id, $fe2_tools_arr))
                {
                    $error = 1;
                    $failed_string .= 'Asset Number:'.$asset_number.' Matched tool: '.$part_description.' is not available in FE2 ordered tools.';
                }
            }
            if(@$error == 1)
            {
                if($type==1)
                {
                    $error_message.='Sorry! FE2 do not have the access to take the tools. '.$failed_string;
                    $data = array('error_message'=>$error_message);
                    echo json_encode($data); exit();
                }
                else
                {
                    $ci->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Sorry!</strong> FE2 do not have the access to take the tools. '.$failed_string.'</div>'); 
                    redirect(SITE_URL.'raise_pickup'); exit();
                }
            }
        }
        else
        {
            if($type==1)
            {
                $error_message.='Sorry! FE2 Order Number: >'.@$order_number.' do not have the access to take the tools. The Reason Might Be the tools which you want to send to other FE has been already full filled or did not ordered the same tools!';

                $data = array('error_message'=>$error_message);
                echo json_encode($data); exit();
            }
            else
            {
                $ci->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> FE2 Order Number: <strong>'.@$order_number.'</strong> do not have the access to take the tools. The Reason Might Be the tools which you want to send to other FE has been already full filled or did not ordered the same tools!</div>');
                redirect(SITE_URL.'raise_pickup'); exit();
            }
        }

        $multipleReturns = checkMultipleReturnsForFE2Order($fe2_tool_order_id);
        if(count($multipleReturns)>0)
        {
            if($type==1)
            {
                $error_message.='Sorry! '.$multipleReturns['fe1_order_number'].' has already raised the return for '.$multipleReturns['fe2_order_number'];

                $data = array('error_message'=>$error_message);
                echo json_encode($data); exit();
            }
            else
            {
                $ci->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> '.$multipleReturns['fe1_order_number'].' has already raised the return for '.$multipleReturns['fe2_order_number'].'. </div>');
                redirect(SITE_URL.'raise_pickup'); exit();
            }
        }
    }
    foreach ($oah_id_with_ordered_asset_id as $oah_id => $ordered_asset_id)
    {
        $status = $ci->Common_model->get_value('order_asset_history',array('oah_id'=>$oah_id),'status');
        if($status > 2)
        {
            if($type==1)
            {
                $error_message.='Sorry! Something went wrong with Order Number '.$fe1_order_number.'.Please do the transaction again!.';

                $data = array('error_message'=>$error_message);
                echo json_encode($data); exit();
            }
            else
            {
                $ci->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> Something went wrong with Order Number '.$fe1_order_number.'.Please do the transaction again!. </div>'); 
                redirect(SITE_URL.'raise_pickup'); exit();
            }
        }
    }

    $m = 0;// $m is count of missed asset
    foreach ($oah_condition_id as $key => $value)
    {
        if($value == 2) $m++;
        if($m!=0 && $return_type_id>2)
        {
            if($type==1)
            {
                $error_message.='Sorry! You can not give the missed tools to other FE!';
                $data = array('error_message'=>$error_message);
                echo json_encode($data); exit();
            }
            else
            {
                $ci->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> You can not give the missed tools to other FE! </div>');
                redirect(SITE_URL.'raise_pickup'); exit();
            }
        }
    }

    if($error_message == '' && $type == 1)
    {
        $data = array('error_message'=>$error_message);
        echo json_encode($data); exit();
    }
    else if($type == 2)
    {
        return $m;
    }
}

function get_asean_country_name()
{
    $ci = & get_instance();
    $asean_name = $ci->config->item('asean_country_name');
    return $asean_name;
}

function get_asean_region_id()
{
    $CI = &get_instance();
    $asean_name = $CI->config->item('asean_country_name');
    $CI->db->select('l.region_id');
    $CI->db->from('location l'); 
    $CI->db->where('l.name',$asean_name);
    $res = $CI->db->get();
    $data = $res->row_array();
    return $data['region_id'];
}

function exclude_Asean_for_roles()
{
    return array(1,3,9);
}

function update_print_format_date($print_date,$print_id)
{
    $ci = & get_instance();
    $update_print = array('created_time'=>$print_date.' '.date('H:i:s'));
    $update_print_where = array('print_id'=>$print_id);
    $ci->Common_model->update_data('print_format',$update_print,$update_print_where);
    return true;
}
function get_user_by_role($role_id)
{
    $ci = & get_instance();
    $header_country_id = $ci->session->userdata('header_country_id');
    $ci->db->select('u.*');
    $ci->db->from('user u');
    $ci->db->where('u.role_id',$role_id);
    $ci->db->where('u.status',1);
    if($header_country_id!='')
    {
        $ci->db->where('u.country_id',$header_country_id);
    }
    else
    {
        $ci->db->where_in('u.country_id',$ci->session->userdata('countriesIndexedArray'));
    }
    $res = $ci->db->get();
    return $res->result_array();
}

function get_assigned_users_by_task($task_name,$task_access,$country_id=0)
{
    $CI = & get_instance();
    $roles_arr = get_assigned_roles_by_task($task_name,$task_access);
    if(count($roles_arr)>0)
    {
        $CI->db->select('u.*,concat(u.sso_id, " - (" ,u.name,")") as user_name');
        $CI->db->from('user u');
        if($country_id!=0)
        {
            $CI->db->join('user_country uc','uc.sso_id  = u.sso_id','left');
            $str = '(u.country_id = '.$country_id.' OR (uc.country_id = '.$country_id.' AND uc.status = 1) )';
            $CI->db->where($str);
        }
        $CI->db->where('u.status',1);
        $CI->db->where_in('u.role_id',$roles_arr);
        $CI->db->group_by('u.sso_id');
        $CI->db->order_by('u.role_id ASC');
        $res = $CI->db->get();
        return $res->result_array();
    }
    else
    {
        return array();
    }
}

function get_assigned_roles_by_task($task_name,$task_access)
{
    $CI = & get_instance();
    $CI->db->select('rt.role_id');
    $CI->db->from('role_task rt');
    $CI->db->join('task t','t.task_id = rt.task_id');
    $CI->db->where('t.name',$task_name);
    if(is_array($task_access))  // modified by maruthi to get admins,sa,tc
        $CI->db->where_in('rt.task_access',$task_access); // get specific taskaccess
    else
         $CI->db->where('rt.task_access',$task_access); // get specific taskaccess
    //$CI->db->where_not_in('rt.task_access',1); // gets both 2,3
    $CI->db->where('rt.status',1);
    $res = $CI->db->get();
    $roleData = $res->result_array();
    return array_unique(convertIntoIndexedArray($roleData,'role_id'));
}

// modified by koushik on 28-may-2018
function page_access_check($parent_page,$current_role=0)
{
    $CI = & get_instance();
    if($current_role===0)
    {
        $role_id = $CI->session->userdata('s_role_id');
    }
    else
    {
        $role_id  = $current_role;
    }

    $CI->db->select('rt.task_access');
    $CI->db->from('role_task rt');
    $CI->db->join('task t','t.task_id = rt.task_id');
    $CI->db->where('t.name',$parent_page);
    $CI->db->where('rt.role_id',$role_id);
    $res = $CI->db->get();
    $result = $res->row_array();
    if(isset($result['task_access']))
    {
        return $result['task_access'];
    }
    else
    {
       if($current_role == 0)
        {
            $CI->session->set_flashdata('response','<div class=" col-md-offset-1 col-md-10 alert alert-danger alert-white rounded" style="margin-top:10px;">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> You Dont Have Access To This Page! </div>'); 
        }
        else
        {
            $role_name = $CI->Common_model->get_value('role',array('role_id'=>$role_id),'name');   
            $CI->session->set_flashdata('response','<div class=" col-md-offset-1 col-md-10 alert alert-danger alert-white rounded" style="margin-top:10px;">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> '.$role_name.' Role Dont Have Access To This Page! </div>');                
        }
        redirect(SITE_URL.'home'); exit();
    }
}

// modified by koushik on 28-may-2018
function check_page_access_notification($parent_page)
{
    $CI = & get_instance();
    $role_id = $CI->session->userdata('s_role_id');

    $CI->db->select('rt.task_access');
    $CI->db->from('role_task rt');
    $CI->db->join('task t','t.task_id = rt.task_id');
    $CI->db->where('t.name',$parent_page);
    $CI->db->where('rt.role_id',$role_id);
    $res = $CI->db->get();
    $result = $res->row_array();
    if(isset($result['task_access']))
    {
        return $result['task_access'];
    }
    else
    {
        return 1;
    }
}

function get_tool_code($tool_code)
{
    $count = strlen($tool_code);
    $zero_count = 4-$count;
    $sno = '';
    while($zero_count > 0)
    {
        $sno.='0';
        $zero_count--;
    }
    $sno.=$tool_code;
    return $sno;
}

function get_tool_asset_list($modality_id)
{
    $status = array(1,2,3,4,8,10,12);
    $CI = & get_instance();
    $CI->db->select('t.tool_id,t.part_number,t.part_description');
    $CI->db->from('tool t');
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->where('p.part_level_id',1);
    $CI->db->where_in('a.status',$status);
    if($modality_id !='')
    {
        $CI->db->where('t.modality_id',$modality_id);
    }
    $CI->db->group_by('t.tool_id');
    $res = $CI->db->get();
    return $res->result_array();
}

//invoice number generation financially
function get_current_invoice_id($print_type)
{
    $financial_year = get_financial_year();
    $ci = & get_instance();
    $print_type = $print_type;
    $wh_id = $ci->session->userdata('swh_id');
    $wh_list = get_wh_list($wh_id);

    $ci->db->select('max(running_sno) as csno');
    $ci->db->from('invoice_number');
    $ci->db->where('DATE(created_time)>=',$financial_year['start_date']);
    $ci->db->where('DATE(created_time)<=',$financial_year['end_date']);
    $ci->db->where_in('wh_id',$wh_list);
    $ci->db->where('print_type',$print_type);
    $res = $ci->db->get();
    if($res->num_rows()>0)
    {
        $row = $res->row_array();
        $number = $row['csno']+1;
    }
    else
    {
        $number = 1;
    }

    if($print_type == 1)
    {
        $format_number = get_invoice_format_number($number,$wh_id);
    }
    else if($print_type == 2)
    {
        $format_number = get_delivery_challan_number($number,$wh_id);
    }
    $insert_invoice_number = array(
        'format_number' => $format_number,
        'running_sno'   => $number,
        'wh_id'         => $wh_id,
        'print_type'    => $print_type,
        'created_by'    => $ci->session->userdata('sso_id'),
        'created_time'  => date('Y-m-d H:i:s'),
        'status'        => 1);
    $invoice_number_id = $ci->Common_model->insert_data('invoice_number',$insert_invoice_number);
    return $invoice_number_id;
}

function get_invoice_format_number($number,$wh_id)
{
    $CI = & get_instance();
    $fy = get_financial_year();
    $from = date('y',strtotime($fy['start_date']));
    $to = date('y',strtotime($fy['end_date']));
    $CI->db->select('l2.short_name');
    $CI->db->from('location l');
    $CI->db->join('warehouse wh','wh.location_id = l.location_id');//area
    $CI->db->join('location l1','l1.location_id = l.parent_id','left');//city
    $CI->db->join('location l2','l2.location_id = l1.parent_id','left');//state               
    $CI->db->where('wh.wh_id', $wh_id);         
    $res = $CI->db->get();
    $sn = $res->row_array();
    $r_no = get_running_sno_five_digit($number);
    $format_number = $from.'-'.$to.'/'.$sn['short_name'].'/TL-'.$r_no;
    return $format_number;
}

function get_delivery_challan_number($number,$wh_id)
{
    $CI = & get_instance();
    $fy = get_financial_year();
    $from = date('y',strtotime($fy['start_date']));
    $to = date('y',strtotime($fy['end_date']));
    $CI->db->select('l2.short_name');
    $CI->db->from('location l');
    $CI->db->join('warehouse wh','wh.location_id = l.location_id');//area
    $CI->db->join('location l1','l1.location_id = l.parent_id','left');//city
    $CI->db->join('location l2','l2.location_id = l1.parent_id','left');//state               
    $CI->db->where('wh.wh_id', $wh_id);         
    $res = $CI->db->get();
    $sn = $res->row_array();
    $r_no = get_running_sno_five_digit($number);
    $format_number = $from.'-'.$to.'/'.$sn['short_name'].'/DC-'.$r_no;
    return $format_number;
}

function pendingpickuprequests()
{
    $CI = &get_instance();
    $CI->load->model('Pickup_point_m');
    return $CI->Pickup_point_m->pickup_list_total_num_rows(@$searchParams); 
}

function pendingrepairDeliveries()
{
    $CI = &get_instance();
    $CI->load->model('Wh_repair_m');
    return $CI->Wh_repair_m->wh_calibration_total_num_rows(@$searchParams); 
    //return 89;
}

#modified by prasad 30-05-2018
function get_wh_by_zone($location_id,$task_access=0)
{
    $CI = & get_instance();
    $location_array = get_area_locations_array($location_id);
    $CI->db->select('w.wh_id,CONCAT(w.wh_code," -(",w.name,")") as wh_name');
    $CI->db->from('warehouse w');
    if($task_access == 0 || $task_access == 1 || $task_access == 2)
    {
        if($CI->session->userdata('header_country_id')!='')
        {
            $CI->db->where('w.country_id',$CI->session->userdata('header_country_id'));
        }
        else
        {
            $CI->db->where_in('w.country_id',$CI->session->userdata('countriesIndexedArray'));
        }
    }
    else if($task_access == 3)
    {
        if($CI->session->userdata('header_country_id')!='')
        {
            $CI->db->where('w.country_id',$CI->session->userdata('header_country_id'));
        }
        else
        {
            $CI->db->where_in('w.country_id',$CI->session->userdata('countriesIndexedArray'));
        }
    }
    $CI->db->where('w.status',1);
    $CI->db->where_in('w.location_id',$location_array);
    $CI->db->order_by('w.country_id ASC');
    $res = $CI->db->get();
    $wh_location = $res->result_array(); 
    $wh_array = get_index_array_result($wh_location,'wh_id');
    return $wh_array;
}

function get_area_locations_array($location_id)
{
    $CI = & get_instance();
    $territory_level = $CI->Common_model->get_value('location',array('location_id'=>$location_id),'level_id');
    $child_locations = array();
    $CI->db->select('l.location_id');
    $CI->db->from('location l');//area
    $CI->db->join('location l1','l1.location_id = l.parent_id','left');//city
    $CI->db->join('location l2','l2.location_id = l1.parent_id','left');//state
    $CI->db->join('location l3','l3.location_id = l2.parent_id','left');//zone 
    $CI->db->join('location l4','l4.location_id = l3.parent_id','left');//country 
    $CI->db->join('location l5','l5.location_id = l4.parent_id','left');//globe 
    if($territory_level == 4)//state
    {
        $CI->db->where('l2.location_id', $location_id); 
    }  
    if($territory_level == 3)//zone
    {
        $CI->db->where('l3.location_id', $location_id); 
    }   
    else if($territory_level == 2)//country 
    {
        $CI->db->where('l4.location_id', $location_id); 
    }        
    else if($territory_level == 1)//globe
    {
        $CI->db->where('l5.location_id', $location_id);
    }
             
    $res = $CI->db->get();
    $child_locations = $res->result_array();
    $return_data = get_index_array_result($child_locations,'location_id');
    return $return_data;
}

function get_index_array_result($result,$key_value)
{
    $ci = & get_instance();
    $result_array = array();
    if(count($result)>0)
    {
        foreach ($result as $key => $value) 
        {
           $result_array[] = $value[$key_value];
        }
    }
    else
    {
        $result_array[] = 0;
    }
    return $result_array;
}
    
//modified by koushik 22-05-2018
function get_wh_by_zone_tools_inventory($location_id,$task_access=0)
{
    $CI = & get_instance();
    $location_array = get_area_locations_array($location_id);

    $CI->db->select('w.wh_id,CONCAT(w.wh_code," -(",w.name,")") as wh_name');
    $CI->db->from('warehouse w');
    if($task_access == 1)
    {
        $CI->db->where('w.country_id',$CI->session->userdata('s_country_id'));
        $CI->db->where('w.status',1);
    }
    else if($task_access == 2)
    {
        $CI->db->where('w.country_id',$CI->session->userdata('s_country_id'));
        $status = array(1,3);
        $CI->db->where_in('w.status',$status);
    }
    else if($task_access == 3)
    {
        if($CI->session->userdata('header_country_id')!='')
        {
            $CI->db->where('w.country_id',$CI->session->userdata('header_country_id'));
        }
        else
        {
            $CI->db->where_in('w.country_id',$CI->session->userdata('countriesIndexedArray'));
        }
        $status = array(1,3);
        $CI->db->where_in('w.status',$status);
    }
    $CI->db->where_in('w.location_id',$location_array);
    $CI->db->order_by('w.country_id ASC');
    $res = $CI->db->get();
    return $res->result_array(); 
}

function get_locations_by_zone($zone)
{
    $CI = & get_instance();
    $child_locations = get_area_locations_array($zone);
    return $child_locations; 
}

function get_all_modality($modality_id)
{
    $CI = & get_instance();
    $CI->db->select('modality_id,name');
    $CI->db->from('modality');
    $CI->db->where('status',1);
    if($modality_id!='')
    $CI->db->where('modality_id',$modality_id);
    $res = $CI->db->get();
    return $res->result_array(); 
}

function get_install_base($modality_id,$loc_list)
{
    $CI = & get_instance();
    $CI->db->select('ib.modality_id,count(ib.install_base_id) as mod_count');
    $CI->db->from('install_base ib');
    $CI->db->join('customer_site cs','cs.customer_site_id = ib.customer_site_id');
    $CI->db->where('ib.modality_id',$modality_id);
    $CI->db->where_in('cs.location_id',$loc_list);
    $CI->db->where('ib.status',1);
    $res = $CI->db->get();
    $result = $res->row_array();
    if($res->num_rows()>0)
    {
        $res = $result['mod_count'];
    }
    else
    {
        $res = 0;
    }
    return $res;
}

function get_tool_count($modality_id,$wh_list,$tool_id)
{
    $status = array(1,2,3,4,8,10,12);
    $CI = & get_instance();
    $CI->db->select('count(a.asset_id) as tool_count');
    $CI->db->from('tool t');
    $CI->db->join('part p','p.tool_id = t.tool_id');
    $CI->db->join('asset a','a.asset_id = p.asset_id');
    $CI->db->where_in('a.wh_id',$wh_list);
    $CI->db->where('p.part_level_id',1);
    $CI->db->where_in('a.status',$status);
    $CI->db->where('a.modality_id',$modality_id);
    if($tool_id != '')
    {
        $CI->db->where('t.tool_id',$tool_id);
    }
    $res = $CI->db->get();
    $result = $res->row_array();

    if($res->num_rows()>0)
    {
        $res = $result['tool_count'];
    }
    else
    {
        $res = 0;
    }
    return $res;

}

function getIBdata($zone,$modality_id,$wh_id,$tool_id)
{
    $CI = & get_instance();
    $zone = $zone;
    $modality_id = $modality_id;
    $wh_id = $wh_id;
    $tool_id = $tool_id;
    if($modality_id == '')
    {
        if($tool_id!='')
        {
            $modality_id = $CI->Common_model->get_value('tool',array('tool_id'=>$tool_id),'modality_id');
        }
    }

    if($wh_id == ''){ $wh_list = get_wh_by_zone($zone); }
    else { $wh_list = array($wh_id); }
    $loc_list = get_locations_by_zone($zone);
    $m_list = get_all_modality($modality_id);

    $chartsData = array();
    $modality_list = array();
    $c_data = array();
    $t_data = array();
    
    foreach ($m_list as $mod) 
    {
        $modality_list[] = $mod['name'];
        $c_data[] = get_install_base($mod['modality_id'],$loc_list);
        $t_data[] = get_tool_count($mod['modality_id'],$wh_list,$tool_id);
    }

    $chartsData['modality_list'] = $modality_list;
    $chartsData['CustomerData'] = $c_data;
    $chartsData['ToolData'] = $t_data;
    $chartsData = json_encode($chartsData, JSON_NUMERIC_CHECK);
    return $chartsData;
}

function gettoolIBdata($zone,$modality_id,$wh_id,$tool_id)
{
    $CI = & get_instance();
    $zone = $zone;
    $modality_name = $modality_id;
    $wh_id = $wh_id;
    $tool_id = $tool_id;
    $tool_list = array();
    if($modality_name!='')
    {
        $modality_id = $CI->Common_model->get_value('modality',array('name'=>$modality_name),'modality_id');
    }
    else
    {
        $modality_id = '';
    }
    

    if($tool_id == '')
    {
        $tool_list = get_tool_asset_list($modality_id);
    }
    else
    {
        $tool_list[] = $CI->Common_model->get_data_row('tool',array('tool_id'=>$tool_id));
    }

    if($wh_id == ''){ $wh_list = get_wh_by_zone($zone); }
    else { $wh_list = array($wh_id); }
    $loc_list = get_locations_by_zone($zone);

    $chartsData = array();
    $tool_name_list = array();
    $c_data = array();
    $t_data = array();
    
    foreach ($tool_list as $tool) 
    {
        $tool_name_list[] = $tool['part_number'];
        $c_data[] = get_install_base($modality_id,$loc_list);
        $t_data[] = get_tool_count($modality_id,$wh_list,$tool['tool_id']);
    }

    $chartsData['tool_name_list'] = $tool_name_list;
    $chartsData['CustomerData'] = $c_data;
    $chartsData['ToolData'] = $t_data;
    $chartsData = json_encode($chartsData, JSON_NUMERIC_CHECK);
    return $chartsData;
}

//invoice number generation financially
function get_current_print_id($print_type,$wh_id)
{
    $financial_year = get_financial_year();
    $ci = & get_instance();
    $print_type = $print_type;
    $wh_id = $wh_id;
    $wh_list = get_wh_list($wh_id);

    $ci->db->select('max(running_sno) as csno');
    $ci->db->from('print_format');
    $ci->db->where('DATE(created_time)>=',$financial_year['start_date']);
    $ci->db->where('DATE(created_time)<=',$financial_year['end_date']);
    $ci->db->where_in('wh_id',$wh_list);
    if($print_type == 1)
    {
        $ci->db->where('print_type',$print_type);
    }
    else
    {
        #include dc 2, dc with sgst 3, dc with igst 4
        $ci->db->where_in('print_type',array(2,3,4));
    }
    $res = $ci->db->get();
    if($res->num_rows()>0)
    {
        $row = $res->row_array();
        $number = $row['csno']+1;
    }
    else
    {
        $number = 1;
    }

    if($print_type == 1)
    {
        $format_number = get_invoice_format_number($number,$wh_id);
    }
    else if($print_type == 2 || $print_type == 3 || $print_type == 4)
    {
        $format_number = get_delivery_challan_number($number,$wh_id);
    }
    $insert_print = array( 'format_number' => $format_number,
                           'running_sno'   => $number,
                           'wh_id'         => $wh_id,
                           'print_type'    => $print_type,
                           'created_by'    => $ci->session->userdata('sso_id'),
                           'created_time'  => date('Y-m-d H:i:s'),
                           'status'        => 1);
    $print_id = $ci->Common_model->insert_data('print_format',$insert_print);
    return $print_id;
}

function get_wh_list($wh_id)
{
    $ci = & get_instance();
    $location_id = $ci->Common_model->get_value('warehouse',array('wh_id'=>$wh_id,'status'=>1),'location_id');
    $ci->db->select('l2.location_id');
    $ci->db->from('location l');//AREA
    $ci->db->join('location l1','l1.location_id = l.parent_id','left');//CITY
    $ci->db->join('location l2','l2.location_id = l1.parent_id','left');//STATE            
    $ci->db->where('l.location_id', $location_id);         
    $res = $ci->db->get();
    $result = $res->row_array();
    $state_id = $result['location_id'];
    $wh_list = get_wh_by_state($state_id);
    return $wh_list;
}

function get_wh_by_state($state_id)
{
    $CI = & get_instance();
    $location_array = get_area_locations_array($state_id);

    $CI->db->select('w.wh_id,CONCAT(w.wh_code," -(",w.name,")") as wh_name');
    $CI->db->from('warehouse w');
    $CI->db->where('w.status',1);
    $CI->db->where_in('w.location_id',$location_array);
    $CI->db->order_by('w.country_id ASC');
    $res = $CI->db->get();
    $wh_location = $res->result_array(); 
    

    $wh_array = get_index_array_result($wh_location,'wh_id');
    return $wh_array;
}

function check_for_minus($var)
{
   return ($var < 0 ? 0 : $var);
}

function get_scanned_assets_details($ordered_tool_id)
{
    if(isset($_SESSION['scanned_assets_list'][$ordered_tool_id]))
    {
        $asset_number_arr = array(); $asset_id_arr = array();
        $session = $_SESSION['scanned_assets_list'][$ordered_tool_id];
        foreach ($session as $key => $value) 
        {
            $asset_number_arr[] = $value['asset_number'];
            $asset_id_arr[] = $value['asset_id'];
        }
        $assets_list = implode(",<br>",$asset_number_arr);
        $asset_count = count($session);
        $part_list_arr = $value['part_list_arr'];
    }
    else
    {
        $assets_list = "--";
        $asset_count = 0;
        $asset_id_arr = array(0);
        $part_list_arr = array();
    }
    return array('assets_list' => $assets_list,'asset_count' => $asset_count,'asset_id_arr'=> $asset_id_arr,'part_list_arr'=>$part_list_arr);
}

function get_rc_scanned_assets_details($rc_asset_id)
{
    if(isset($_SESSION['scanned_assets_list'][$rc_asset_id]))
    {
        $asset_number_arr = array(); $asset_id_arr = array();
        $session = $_SESSION['scanned_assets_list'][$rc_asset_id];
        
        $asset_number_arr[] = $session['asset_number'];
        $asset_id_arr[] = $session['asset_id'];
        $assets_list = implode(",<br>",$asset_number_arr);
        $asset_count = count($session);
        $part_list_arr = $session['part_list_arr'];
    }
    else
    {
        $assets_list = "--";
        $asset_count = 0;
        $asset_id_arr = array(0);
        $part_list_arr = array();
    }
    return array('assets_list' => $assets_list,'asset_count' => $asset_count,'asset_id_arr'=> $asset_id_arr,'part_list_arr'=>$part_list_arr);
}

function get_asset_cal_12_16()
{
    $ci=& get_instance();
    $ci->db->select('rc.asset_id,rc.current_stage_id');
    $ci->db->from('rc_asset rc');
    $ci->db->join('asset a','a.asset_id = rc.asset_id');
    $ci->db->where('rc.current_stage_id>=',12);
    $ci->db->where('rc.current_stage_id<=',16);
    $ci->db->where('a.status !=',4);
    $res = $ci->db->get();
    return $res->result_array();
}

function get_rca_exclude()
{
    $ci=& get_instance();
    $ci->db->select('rc.rc_asset_id,rc.rc_number,l.name as country_name,rc.current_stage_id,rc.asset_id');
    $ci->db->from('rc_asset rc');
    $ci->db->join('location l','l.location_id = rc.country_id');
    $ci->db->where('l.region_id !=',1);
    $ci->db->where_in('rc.current_stage_id',array('14,15'));
    $res = $ci->db->get();
    return $res->result_array();
}