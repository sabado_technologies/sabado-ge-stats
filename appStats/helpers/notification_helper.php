<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_upcoming_calibration_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('calibration');
	if($task_access != 0)
	{
		return $ci->Notification_m->get_upcoming_calibration_count($task_access);
	}
	else
	{
		return 0;
	}
}

function waiting_for_shipment_admin_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('calibration_report');
	if($task_access !=0)
	{
		return $ci->Notification_m->wh_calibration_total_num_rows_admin($task_access);
	}
	else
	{
		return 0;
	}
}

function at_cal_vendor_count()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('calibration_report');
	if($task_access !=0)
	{
		return $ci->Notification_m->calibration_count_notification(13,$task_access);
	}
	else
	{
		return 0;
	}
}

function at_tools_coordinator()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('open_calibration');
	if($task_access !=0)
	{
		return $ci->Notification_m->calibration_count_notification(14,$task_access);
	}
	else
	{
		return 0;
	}
}

function under_qa_approval()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('qc_calibration');
	if($task_access !=0)
	{
		return $ci->Notification_m->calibration_count_notification(15,$task_access);
	}
	else
	{
		return 0;
	}
}

function waiting_for_closing()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('open_calibration');
	if($task_access !=0)
	{
		return $ci->Notification_m->calibration_count_notification(16,$task_access);
	}
	else
	{
		return 0;
	}
}

function pending_calibration_requests()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('wh_calibration');
	if($task_access !=0)
	{
		return $ci->Notification_m->wh_calibration_total_num_rows($task_access);
	}
	else
	{
		return 0;
	}
}

function acknowledge_cal_tools_count()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	/*modified by koushik*/
	$task_access = check_page_access_notification('acknowledge_cal_tools');
	if($task_access !=0)
	{
		return $ci->Notification_m->vendor_calibration_total_num_rows($task_access);
	}
	else
	{
		return 0;
	}
}

function get_repair_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('repair_tool');
	if($task_access != 0)
	{
		return $ci->Notification_m->get_repair_count($task_access);
	}
	else
	{
		return 0;
	}
}

function waiting_for_repair_shipment_admin_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('open_repair_request');
	if($task_access !=0)
	{
		return $ci->Notification_m->wh_repair_total_num_rows_admin($task_access);
	}
	else
	{
		return 0;
	}
}

function repair_at_cal_vendor_count()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('open_repair_request');
	if($task_access !=0)
	{
		return $ci->Notification_m->repair_count_notification(22,$task_access);
	}
	else
	{
		return 0;
	}
}

function repair_at_tools_coordinator()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('open_repair_request');
	if($task_access !=0)
	{
		return $ci->Notification_m->repair_count_notification(23,$task_access);
	}
	else
	{
		return 0;
	}
}

function repair_waiting_for_closing()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('open_repair_request');
	if($task_access !=0)
	{
		return $ci->Notification_m->repair_count_notification(23,$task_access);
	}
	else
	{
		return 0;
	}
}

function pending_repair_requests()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('wh_repair');
	if($task_access !=0)
	{
		return $ci->Notification_m->wh_repair_total_num_rows($task_access);
	}
	else
	{
		return 0;
	}
}

function acknowledge_repair_tools_count()
{
	$ci = &get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('vendor_wh_repair_request');
	if($task_access !=0)
	{
		return $ci->Notification_m->vendor_repair_total_num_rows($task_access);
	}
	else
	{
		return 0;
	}
}

function defective_asset_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('defective_asset');
	if($task_access !=0)
	{
		return $ci->Notification_m->defective_asset_count($task_access);
	}
	else
	{
		return 0;
	}
}

function missed_asset_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('missed_asset');
	if($task_access !=0)
	{
		return $ci->Notification_m->missed_asset_count($task_access);
	}
	else
	{
		return 0;
	}
}

function fe_position_change_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('fe_position');
	if($task_access !=0)
	{
		return $ci->Notification_m->fe_position_change_count($task_access);
	}
	else
	{
		return 0;
	}
}

// created by maruthi on 24th'May'18
function address_change_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('address_change');
	if($task_access !=0)
	{
		return $ci->Notification_m->addressChangeResults($task_access);
	}
	else
	{
		return 0;
	}
}

// created by maruthi on 24th'May'18
function due_in_deliveries_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('crossed_expected_delivery_date');
	if($task_access !=0)
	{
		return $ci->Notification_m->crossed_expected_delivery_results($task_access);
	}
	else
	{
		return 0;
	}
}

function return_days_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('exceededOrderDuration');
	if($task_access !=0)
	{
		return $ci->Notification_m->exceededOrderDurationResults($task_access);
	}
	else
	{
		return 0;
	}
}

//koushik
function pending_fe_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('fe_delivery');
	if($task_access !=0)
	{
		return $ci->Notification_m->pending_fe_requests($task_access);
	}
	else
	{
		return 0;
	}
}

//koushik
function pending_st_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('wh_stock_transfer_list');
	if($task_access !=0)
	{
		return $ci->Notification_m->pending_st_requests($task_access);
	}
	else
	{
		return 0;
	}
}

//koushik
function pending_pickup_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('pickup_list');
	if($task_access !=0)
	{
		return $ci->Notification_m->pending_pickup_requests($task_access);
	}
	else
	{
		return 0;
	}
}

function acknowledge_fe_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('open_fe_returns');
	if($task_access !=0)
	{
		return $ci->Notification_m->acknowledge_fe_requests($task_access);
	}
	else
	{
		return 0;
	}
}

function acknowledge_wh_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('wh2_wh_receive');
	if($task_access !=0)
	{
		return $ci->Notification_m->acknowledge_wh_requests($task_access);
	}
	else
	{
		return 0;
	}
}

// created by maruthi on 19thMay'18
function tool_to_return_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('raise_pickup');
	//echo $task_access;exit;
	if($task_access !=0)
	{
		return $ci->Notification_m->owned_order_total_num_rows($task_access);
	}
	else
	{
		return 0;
	}
}

// created by maruthi on 20thMay'18
function fe2_fe_transfer_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('fe2_fe_approval');
	if($task_access !=0)
	{
		return $ci->Notification_m->fe2_fe_approval_order_results($task_access);
	}
	else
	{
		return 0;
	}
}

// created by maruthi on 20thMay'18
function ack_from_fe_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('fe2_fe_receive');
	if($task_access !=0)
	{
		return $ci->Notification_m->fe2_fe_receive_order_results($task_access);
	}
	else
	{
		return 0;
	}
}
// created by maruthi on 20thMay'18
function stock_transfer_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('raiseSTforOrder');
	if($task_access !=0)
	{
		return $ci->Notification_m->STforOrder_tool_results($task_access);
	}
	else
	{
		return 0;
	}
	//echo $CI->db->last_query(); exit;
}

// created by maruthi on21stMay'18
function return_calibration_tool_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('calibration_required_tools');
	//echo $task_access;exit;
	if($task_access !=0)
	{
		return $ci->Notification_m->calibration_required_results($task_access);
	}
	else
	{
		return 0;
	}

}

function fe_transfer_request()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('fe_transfer_request');
	if($task_access !=0)
	{
		return $ci->Notification_m->fe_transfer_request($task_access);
	}
	else
	{
		return 0;
	}
}

// created by maruthi on21stMay'18
function due_in_tool_return_count()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('crossed_return_date_orders');
	if($task_access !=0)
	{
		return $ci->Notification_m->crossedReturnDateOrdersResults($task_access);
	}
	else
	{
		return 0;
	}
}

function notification_result($result)
{
	$overall_count = 0;
	if(isset($result[0]))
	{
		foreach ($result as $key => $value) 
		{
			$overall_count+=$value['count'];
		}
		if(!isset($result[0]['display_name']))
		{
			$result = array();
		}
	}
	else
	{
		$overall_count = 0;
		$result = array();
	}
	// print_r($overall_count);
	// print_r($result);exit;
	
	return array($overall_count,$result);
}

// created by maruthi on 19thMay'18
function ack_from_wh_requests()
{
	$ci = & get_instance();
	$ci->load->model('Notification_m');
	$task_access = check_page_access_notification('receive_order');
	if($task_access !=0)
	{
		$result1 =  $ci->Notification_m->receive_order_results($task_access);
		$result2 =  $ci->Notification_m->raisedSTforOrderResults(0,$task_access);
		// echo '<pre>';print_r($result1);
		// echo '--------------------------';
		// echo '<pre>';print_r($result2);exit;
		$final = array_merge($result1,$result2);
		//echo '<pre>';print_r($final);exit;
		//echo $task_access;exit;
		if($task_access == 1 || $task_access == 2)
		{
			if(count($ci->session->userdata('countriesIndexedArray'))>1)
			{
				return returnData($final);
			}
			else
			{
				$final_arr = array();
				if(count($final)>0)
				{
					$count1 = (isset($result1[0]['count']))?$result1[0]['count']:0;
					$count2 = (isset($result2[0]['count']))?$result2[0]['count']:0;
					$count = $count1+$count2;
					if($count>0)
					{
						$final_arr[] = array(
							'count' => $count
						);
					}				
					return $final_arr;
				}
				else
				{
					return 0;
				}
			}
		}
		else
		{		
			return returnData($final);
		}
		
	}
	else
	{
		return 0;
	}
}

function returnData($final)
{
	if(count($final)>0)
	{
		$ids_arr = array();
		$location_id_arr = array();
		$location_id_arr_count = array();
		$return_arr = array();
		foreach ($final as $key => $value) {
			if(in_array($value['location_id'], $ids_arr))
			{
				$new_count = ($value['count'] + $location_id_arr_count[$value['location_id']]);
				$location_id_arr[$value['location_id']] = $value['display_name'];
				$location_id_arr_count[$value['location_id']] = $new_count;
			}
			else
			{
				$location_id_arr[$value['location_id']] = $value['display_name'];
				$location_id_arr_count[$value['location_id']] = $value['count'];
				$ids_arr[] = $value['location_id'];
			}				
		}
		if(count($location_id_arr_count)>0)
		{
			foreach ($location_id_arr_count as $key => $value) {
				$return_arr[] = array(
					'count'   		=> $value,
					'display_name'  => $location_id_arr[$key]
					);
			}
			//echo "<pre>";print_r($return_arr);exit;
			return $return_arr;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}