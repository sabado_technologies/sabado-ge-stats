<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');//Mahesh Helper


function issueAssetsAndOrders($allAssets,$country_id,$checkData)
{
	$IssueAssets = array();
	$issueOrders = array();
	$data = $checkData;
	$transAssets = $data[0];
    if(count($transAssets)>0){
        foreach ($allAssets as $key => $value) {
            if(array_key_exists($value['asset_number'], $transAssets)){
                if(!in_array($value['asset_number'], $transAssets)){
                    $issueOrders[$transAssets[$value['asset_number']]] = $value['asset_number'];
                }
                $IssueAssets[$value['asset_number']] = $transAssets[$value['asset_number']];
            }
        }            
    }
    return array($IssueAssets,$issueOrders,$checkData[2]);	
}

function InTransaction($allAssets,$country_id,$checkData)
{
	$IssueAssets = array();
    if(count($checkData)>0){
        foreach($allAssets as $key => $value) {
            if(!array_key_exists($value['asset_number'], $checkData)){
                $IssueAssets[] = $value['asset_number'];
            }
        }            
    }
    return array($IssueAssets);	
}

function checkingIssuesWithApprovals($country_id,$issueAssets)
{
	$approvalAssetsIndexed = array();
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$approvalAssets = $CI->Special_cases_m->getAssets('',$country_id,1);
	if(count($approvalAssets)>0){
		foreach ($approvalAssets as $key => $value) {
			$approvalAssetsIndexed[]= $value['asset_number'];
		}
	}
    if(count($issueAssets)>0){
        foreach($issueAssets as $key => $value) {
            if(!in_array($value, $approvalAssetsIndexed)){
                $finalIssueAssets[] = $value;
            }
        }            
    }
    return array($finalIssueAssets);		
}

function checkingOutOfCalibrationAssetsNotInOutOfCalibrationProcess($allAssets,$country_id){
	$checkData = adminOutOfCalibration($country_id);
	$finalIssues = InTransaction($allAssets,$country_id,$checkData[0]);
	return array($finalIssues[0],$checkData[2]);
}
function adminOutOfCalibration($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Calibration_m');
	$childData = $CI->Special_cases_m->calibration_results($country_id);
	$assets = array();
	$orders = array();
	$multipleTrans = array();

    if(count($childData)>0){
        foreach ($childData as $key1 => $value1) {
        	if(array_key_exists($value1['asset_number'], $assets)) {
        		if(in_array($value1['asset_number'], $multipleTrans)) {
        			$multipleTrans[$value1['rc_number']] =  $value1['asset_number'];	
        		}
        		else
        		{
        			# When Asset found in previous transactions,
        			# fetching the previoulsy pushed transaction
        			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
        				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
        			}
        			else
        			{
        				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
        			}
        			# pusing the current transaction also
        			$multipleTrans[$value1['rc_number']] = $value1['asset_number'];
        			unset($orders[$assets[$value1['asset_number']]]);
        			unset($assets[$value1['asset_number']]);
        		}
        	}
        	else
        	{
               $assets[$value1['asset_number']]  = $value1['rc_number'];
               $orders[$value1['rc_number']]  = $value1['asset_number'];
            }
        }
    }   
    return array($assets,$orders,$multipleTrans);
}


function checkingCalibraionAssetsNotInCalibrationProcess($allAssets,$country_id){
	$checkData = adminOpenCalibration($country_id);
	$finalIssues = InTransaction($allAssets,$country_id,$checkData[0]);
	return array($finalIssues[0],$checkData[2]);
}

function checkingRepairAssetsNotInRepairProcess($allAssets,$country_id){
	$checkData = adminOpenRepair($country_id);
	$finalIssues = InTransaction($allAssets,$country_id,$checkData[0]);
	return array($finalIssues[0],$checkData[2]);
}

function checkingInToolReturn($allAssets,$country_id)
{
	$checkData1 = feReturn($country_id);	
	$checkData2 = fePendingPickup($country_id);
	$checkData3 = getFDBySTAck($country_id);
	$checkData4 = feToFEAck($country_id);

	$checkDataFinal1 = $checkData1[0];
	$checkDataFinal2 = $checkData2[0];
	$checkDataFinal3 = $checkData3[0];
	$checkDataFinal4 = $checkData4[0];

	$checkDataAssets = array();
	# same assets lies in both transactions
	$issueAssets = array();
	$issueOrders = array();
	if(count($checkDataFinal1)>0){
		$checkData = $checkDataFinal1;
		if(count($checkData)>0){
			foreach ($checkDataFinal2 as $key => $value) {
				if(array_key_exists($key, $checkData)) {
					$issueAssets[$key] = $checkData[$key];
					if(array_key_exists($checkData[$key], $issueOrders)) {
						$issueOrders["-".$checkData[$key]] = $key;
					}
					else{
						$issueOrders[$checkData[$key]] = $key;
					}
					if(array_key_exists($value, $issueOrders)){
						$issueOrders["-".$value] = $key;
					}
					else{
						$issueOrders[$value] = $key;
					}
				}
				else
				{
					$checkData[$key] = $value;
				}
			}
		}
		if(count($checkDataFinal3)>0){
			foreach ($checkDataFinal3 as $key => $value) {
				if(array_key_exists($key, $checkData)) {
					$issueAssets[$key] = $checkData[$key];
					if(array_key_exists($checkData[$key], $issueOrders)) {
						$issueOrders["-".$checkData[$key]] = $key;
					}
					else{
						$issueOrders[$checkData[$key]] = $key;
					}
					if(array_key_exists($value, $issueOrders)){
						$issueOrders["-".$value] = $key;
					}
					else{
						$issueOrders[$value] = $key;
					}
				}
				else
				{
					$checkData[$key] = $value;
				}
			}	
		}
		if(count($checkDataFinal4)>0){
			foreach ($checkDataFinal4 as $key => $value) {
				if(array_key_exists($key, $checkData)) {
					$issueAssets[$key] = $checkData[$key];
					if(array_key_exists($checkData[$key], $issueOrders)) {
						$issueOrders["-".$checkData[$key]] = $key;
					}
					else{
						$issueOrders[$checkData[$key]] = $key;
					}
					if(array_key_exists($value, $issueOrders)){
						$issueOrders["-".$value] = $key;
					}
					else{
						$issueOrders[$value] = $key;
					}
				}
				else
				{
					$checkData[$key] = $value;
				}
			}	
		}
	}
	else
	{
		$checkData = $checkDataFinal2;
		if(count($checkDataFinal2)>0){
			if(count($checkDataFinal3)>0){
				foreach ($checkDataFinal3 as $key => $value) {
					if(array_key_exists($key, $checkData)) {
						$issueAssets[$key] = $checkData[$key];
						if(array_key_exists($checkData[$key], $issueOrders)) {
							$issueOrders["-".$checkData[$key]] = $key;
						}
						else{
							$issueOrders[$checkData[$key]] = $key;
						}
						if(array_key_exists($value, $issueOrders)){
							$issueOrders["-".$value] = $key;
						}
						else{
							$issueOrders[$value] = $key;
						}
					}
					else
					{
						$checkData[$key] = $value;
					}
				}	
			}
			if(count($checkDataFinal4)>0){
				foreach ($checkDataFinal4 as $key => $value) {
					if(array_key_exists($key, $checkData)) {
						$issueAssets[$key] = $checkData[$key];
						if(array_key_exists($checkData[$key], $issueOrders)) {
							$issueOrders["-".$checkData[$key]] = $key;
						}
						else{
							$issueOrders[$checkData[$key]] = $key;
						}
						if(array_key_exists($value, $issueOrders)){
							$issueOrders["-".$value] = $key;
						}
						else{
							$issueOrders[$value] = $key;
						}
					}
					else
					{
						$checkData[$key] = $value;
					}
				}	
			}
		}
		else
		{
			$checkData = $checkDataFinal3;
			if(count($checkDataFinal4)>0){
				foreach ($checkDataFinal4 as $key => $value) {
					if(array_key_exists($key, $checkData)) {
						$issueAssets[$key] = $checkData[$key];
						if(array_key_exists($checkData[$key], $issueOrders)) {
							$issueOrders["-".$checkData[$key]] = $key;
						}
						else{
							$issueOrders[$checkData[$key]] = $key;
						}
						if(array_key_exists($value, $issueOrders)){
							$issueOrders["-".$value] = $key;
						}
						else{
							$issueOrders[$value] = $key;
						}
					}
					else
					{
						$checkData[$key] = $value;
					}
				}	
			}
			else
			{
				$checkData = $checkDataFinal4;
			}
		}
	}
    $finalIssues = InTransaction($allAssets,$country_id,$checkData);
    # 0-not available in trturn and pending pickup 
    # 1,2 -Assets are available for return order and pending pickup
    # 3,4 - assets are theerre for returning more then one transaction in same order
    return array($finalIssues[0],$issueAssets,$issueOrders,$checkData1[2],$checkData2[2],$checkData3[2],$checkData4[2]);
}

function getFDBySTAck($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Order_m');
	$CI->load->model('Stock_transfer_m');
	$feAcks = $CI->Special_cases_m->fe_owned_tools_results($country_id);	
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($feAcks)>0){
        foreach ($feAcks as $key => $value){
        	$assets[$value['asset_number']]  = $value['order_number'];
	        $orders[$value['order_number']]  = $value['asset_number']; 
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function feReturn($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Order_m');
	$feReturns = $CI->Special_cases_m->owned_order_results($country_id);
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($feReturns)>0){
        foreach ($feReturns as $key => $value){
            $feReturnsOrderAssets = $CI->Order_m->getOwnedAssetsDetailsByOrderId($value['tool_order_id']);
            if(count($feReturnsOrderAssets)>0){
                foreach ($feReturnsOrderAssets as $key1 => $value1) {
                	if(array_key_exists($value1['asset_number'], $assets)) {
                		if(in_array($value1['asset_number'], $multipleTrans)) {
                			$multipleTrans[$value['order_number']] =  $value1['asset_number'];	
                		}
                		else
                		{
                			# When Asset found in previous transactions,
                			# fetching the previoulsy pushed transaction
                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
                			}
                			else
                			{
                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
                			}
                			# pusing the current transaction also
                			$multipleTrans[$value['order_number']] = $value1['asset_number'];
                			unset($orders[$assets[$value1['asset_number']]]);
                			unset($assets[$value1['asset_number']]);
                		}
                	}
                	else
                	{
	                    $assets[$value1['asset_number']]  = $value['order_number'];
	                    $orders[$value['order_number']]  = $value1['asset_number'];
	                }
                }
            }
        }
    }
    return array($assets,$orders,$multipleTrans);
}


function checkingFeReturnTools($allAssets,$country_id)
{
	$checkData = feReturn($country_id);	
    return issueAssetsAndOrders($allAssets,$country_id,$checkData);    
}

function ackFromWHIssues($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Order_m');
	$CI->load->model('Stock_transfer_m');
	$feAcks = $CI->Special_cases_m->receive_order_results($country_id);
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($feAcks)>0){
        foreach ($feAcks as $key => $value){
        	if($value['stock_transfer_id']!=''){
        		$stData = $CI->Stock_transfer_m->stnForOrder($value['tool_order_id'],1,1); 
        		if(count($stData)>0){
                    foreach ($stData as $st_key => $st_data) {
                        $st_return_assets = $CI->Order_m->wh_return_initialted_assets($st_data['tool_order_id'],1); 
                        if(count($st_return_assets)>0){
                        	foreach ($st_return_assets as $st_assets_key => $value1) {
                        		if(array_key_exists($value1['asset_number'], $assets)) {
			                		if(in_array($value1['asset_number'], $multipleTrans)) {
			                			$multipleTrans[$value['stn_number']] =  $value1['asset_number'];	
			                		}
			                		else
			                		{
			                			# When Asset found in previous transactions,
			                			# fetching the previoulsy pushed transaction
			                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
			                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
			                			}
			                			else
			                			{
			                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
			                			}
			                			# pusing the current transaction also
			                			$multipleTrans[$value['stn_number']] = $value1['asset_number'];
			                			unset($orders[$assets[$value1['asset_number']]]);
			                			unset($assets[$value1['asset_number']]);
			                		}
			                	}
			                	else
			                	{
                        			$assets[$value1['asset_number']]  = $st_data['stn_number'];
	               					$orders[$st_data['stn_number']]  = $value1['asset_number'];       
	               				}
                        	}	             			
               			}
                    }
                }               
        	}
        	else
        	{
	            $feAckAssets = $CI->Order_m->wh_return_initialted_assets($value['tool_order_id']);
	            if(count($feAckAssets)>0){
	                foreach ($feAckAssets as $key1 => $value1) {
	                	if(array_key_exists($value1['asset_number'], $assets)) {
	                		if(in_array($value1['asset_number'], $multipleTrans)) {
	                			$multipleTrans[$value['order_number']] =  $value1['asset_number'];	
	                		}
	                		else
	                		{
	                			# When Asset found in previous transactions,
	                			# fetching the previoulsy pushed transaction
	                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
	                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
	                			}
	                			else
	                			{
	                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
	                			}
	                			# pusing the current transaction also
	                			$multipleTrans[$value['order_number']] = $value1['asset_number'];
	                			unset($orders[$assets[$value1['asset_number']]]);
	                			unset($assets[$value1['asset_number']]);
	                		}
	                	}
	                	else
	                	{
		                   $assets[$value1['asset_number']]  = $value['order_number'];
		                   $orders[$value['order_number']]  = $value1['asset_number'];
		                }
	                }
	            }
	        }
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function checkingAckFromWHTools($allAssets,$country_id)
{
	$checkData = ackFromWHIssues($country_id);
    return issueAssetsAndOrders($allAssets,$country_id,$checkData);    
}

function feToFEAck($country_id,$type=3) // 4 is by courier
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Fe2fe_m');
	$feToFEAcks = $CI->Special_cases_m->fe2_fe_receive_order_results($country_id,$type);
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($feToFEAcks)>0){
        foreach ($feToFEAcks as $key => $value){
            $feToFEAckOrderAssets = $CI->Fe2fe_m->get_return_initialted_assets($value['rto_id'],1); // 1 is to get fe2 6th stage only
            if(count($feToFEAckOrderAssets)>0){
                foreach ($feToFEAckOrderAssets as $key1 => $value1) {
                	if(array_key_exists($value1['asset_number'], $assets)) {
                		if(in_array($value1['asset_number'], $multipleTrans)) {
                			$multipleTrans[$value['order_number']] =  $value1['asset_number'];	
                		}
                		else
                		{
                			# When Asset found in previous transactions,
                			# fetching the previoulsy pushed transaction
                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
                			}
                			else
                			{
                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
                			}
                			# pusing the current transaction also
                			$multipleTrans[$value['order_number']] = $value1['asset_number'];
                			unset($orders[$assets[$value1['asset_number']]]);
                			unset($assets[$value1['asset_number']]);
                		}
                	}
                	else
                	{
	                   $assets[$value1['asset_number']]  = $value['order_number'];
	                   $orders[$value['order_number']]  = $value1['asset_number'];
	                }
                }
            }
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function checkingFeToFETools($allAssets,$country_id)
{
	$checkData = feToFEAck($country_id);
	return issueAssetsAndOrders($allAssets,$country_id,$checkData);
}

function fePendingPickup($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Order_m');
	$data = $CI->Special_cases_m->feReturnInitiatedResults($country_id);
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($data)>0){
        foreach ($data as $key => $value){
            $childData = $CI->Order_m->getAssetDataByOrderStatusID($value['order_status_id']);
            if(count($childData)>0){
                foreach ($childData as $key1 => $value1) {
                	if(array_key_exists($value1['asset_number'], $assets)) {
                		if(in_array($value1['asset_number'], $multipleTrans)) {
                			$multipleTrans[$value['return_number']] =  $value1['asset_number'];	
                		}
                		else
                		{

                			# When Asset found in previous transactions,
                			# fetching the previoulsy pushed transaction
                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
                			}
                			else
                			{
                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
                			}
                			# pusing the current transaction also
                			$multipleTrans[$value['return_number']] = $value1['asset_number'];
                			unset($orders[$assets[$value1['asset_number']]]);
                			unset($assets[$value1['asset_number']]);
                			//exit;
                		}
                	}
                	else
                	{
	                   $assets[$value1['asset_number']]  = $value['return_number'];
	                   $orders[$value['return_number']]  = $value1['asset_number'];
	                }
                }
            }
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function checkingFePendingPickup($allAssets,$country_id)
{
	$checkData = fePendingPickup($country_id);
	return issueAssetsAndOrders($allAssets,$country_id,$checkData);
}

function logPickups($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Pickup_point_m');
	$data = $CI->Special_cases_m->pickup_list_results($country_id);
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($data)>0){
        foreach ($data as $key => $value){
        	$rrow = $CI->Pickup_point_m->get_return_details($value['return_order_id'],$value['rto_id']);
            $childData = $CI->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
            if(count($childData)>0){
                foreach ($childData as $key1 => $value1) {
                	if(array_key_exists($value1['asset_number'], $assets)) {
                		if(in_array($value1['asset_number'], $multipleTrans)) {
                			$multipleTrans[$value['return_number']] =  $value1['asset_number'];	
                		}
                		else
                		{
                			# When Asset found in previous transactions,
                			# fetching the previoulsy pushed transaction
                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
                			}
                			else
                			{
                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
                			}
                			# pusing the current transaction also
                			$multipleTrans[$value['return_number']] = $value1['asset_number'];
                			unset($orders[$assets[$value1['asset_number']]]);
                			unset($assets[$value1['asset_number']]);
                		}
                	}
                	else
                	{
	                   $assets[$value1['asset_number']]  = $value['return_number'];
	                   $orders[$value['return_number']]  = $value1['asset_number'];
	                }
                }
            }
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function checkingLogPickups($allAssets,$country_id)
{
	$checkData = logPickups($country_id);
	return issueAssetsAndOrders($allAssets,$country_id,$checkData);
}

function logFeAck($country_id,$onlyIntransit=0)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Pickup_point_m');
	$data = $CI->Special_cases_m->open_fe_returns_list_results($country_id,$onlyIntransit);
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($data)>0){
        foreach ($data as $key => $value){
        	$rrow = $CI->Pickup_point_m->get_return_details($value['return_order_id'],$value['rto_id']);
            $childData = $CI->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
            if(count($childData)>0){
                foreach ($childData as $key1 => $value1) {
                	if(array_key_exists($value1['asset_number'], $assets)) {
                		if(in_array($value1['asset_number'], $multipleTrans)) {
                			$multipleTrans[$value['return_number']] =  $value1['asset_number'];	
                		}
                		else
                		{
                			# When Asset found in previous transactions,
                			# fetching the previoulsy pushed transaction
                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
                			}
                			else
                			{
                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
                			}
                			# pusing the current transaction also
                			$multipleTrans[$value['return_number']] = $value1['asset_number'];
                			unset($orders[$assets[$value1['asset_number']]]);
                			unset($assets[$value1['asset_number']]);
                		}
                	}
                	else
                	{
	                   $assets[$value1['asset_number']]  = $value['return_number'];
	                   $orders[$value['return_number']]  = $value1['asset_number'];
	                }
                }
            }
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function checkingLogFEAck($allAssets,$country_id)
{
	$checkData = logFeAck($country_id);
	return issueAssetsAndOrders($allAssets,$country_id,$checkData);
}

function logSTAck($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Stock_transfer_m');
	$data = $CI->Special_cases_m->wh2_wh_receive_order_results($country_id);
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($data)>0){
        foreach ($data as $key => $value){
            $childData = $CI->Stock_transfer_m->wh_return_initialted_assets($value['tool_order_id']);
            if(count($childData)>0){
                foreach ($childData as $key1 => $value1) {
                	if(array_key_exists($value1['asset_number'], $assets)) {
                		if(in_array($value1['asset_number'], $multipleTrans)) {
                			$multipleTrans[$value['stn_number']] =  $value1['asset_number'];	
                		}
                		else
                		{
                			# When Asset found in previous transactions,
                			# fetching the previoulsy pushed transaction
                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
                			}
                			else
                			{
                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
                			}
                			# pusing the current transaction also
                			$multipleTrans[$value['stn_number']] = $value1['asset_number'];
                			unset($orders[$assets[$value1['asset_number']]]);
                			unset($assets[$value1['asset_number']]);
                		}
                	}
                	else
                	{
	                   $assets[$value1['asset_number']]  = $value['stn_number'];
	                   $orders[$value['stn_number']]  = $value1['asset_number'];
	                }
                }
            }
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function checkingLogSTAck($allAssets,$country_id)
{
	$checkData = logSTAck($country_id);
	return issueAssetsAndOrders($allAssets,$country_id,$checkData);
}

function adminOpenCalibration($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Calibration_m');
	$data = $CI->Special_cases_m->admin_calibration_results($country_id);
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($data)>0){
        foreach ($data as $key => $value){
            $childData = $CI->Calibration_m->get_asset_details($value['rc_order_id'],1);
            if(count($childData)>0){
                foreach ($childData as $key1 => $value1) {
                	if(array_key_exists($value1['asset_number'], $assets)) {
                		if(in_array($value1['asset_number'], $multipleTrans)) {
                			$multipleTrans[$value1['rc_number']] =  $value1['asset_number'];	
                		}
                		else
                		{
                			# When Asset found in previous transactions,
                			# fetching the previoulsy pushed transaction
                			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
                				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
                			}
                			else
                			{
                				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
                			}
                			# pusing the current transaction also
                			$multipleTrans[$value1['rc_number']] = $value1['asset_number'];
                			unset($orders[$assets[$value1['asset_number']]]);
                			unset($assets[$value1['asset_number']]);
                		}
                	}
                	else
                	{
	                   $assets[$value1['asset_number']]  = $value1['rc_number'];
	                   $orders[$value1['rc_number']]  = $value1['asset_number'];
	                }
                }
            }
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function checkingAdminOpenCalibration($allAssets,$country_id)
{
	$checkData = adminOpenCalibration($country_id);
	return issueAssetsAndOrders($allAssets,$country_id,$checkData);
}

function adminOpenRepair($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$CI->load->model('Repair_m');
	$data1 = array();
	$data2 = array();
	$assets = array();
	$orders = array();

	$data1 = $CI->Special_cases_m->open_repair_results($country_id);
	$data2 = $CI->Special_cases_m->repair_results($country_id);	
	$multipleTrans = array();

    if(count($data1)>0){
    	$tempData = array();
        foreach ($data1 as $key => $value){
            $childData = $CI->Repair_m->get_asset_details($value['rc_order_id']);
            if(count($childData)>0){
            	foreach ($childData as $key => $value) {
            		$tempData[] = $value;
            	}
            }
        }
        $childData = array_merge($tempData,$data2);
        if(count($childData)>0){
            foreach ($childData as $key1 => $value1) {
            	if(array_key_exists($value1['asset_number'], $assets)) {
            		if(in_array($value1['asset_number'], $multipleTrans)) {
            			$multipleTrans[$value1['rc_number']] =  $value1['asset_number'];	
            		}
            		else
            		{
            			# When Asset found in previous transactions,
            			# fetching the previoulsy pushed transaction
            			if(array_key_exists($assets[$value1['asset_number']],$multipleTrans)){
            				$multipleTrans["-".$assets[$value1['asset_number']]] =  $value1['asset_number'];	
            			}
            			else
            			{
            				$multipleTrans[$assets[$value1['asset_number']]] =  $value1['asset_number'];
            			}
            			# pusing the current transaction also
            			$multipleTrans[$value1['rc_number']] = $value1['asset_number'];
            			unset($orders[$assets[$value1['asset_number']]]);
            			unset($assets[$value1['asset_number']]);
            		}
            	}
            	else
            	{
                   $assets[$value1['asset_number']]  = $value1['rc_number'];
                   $orders[$value1['rc_number']]  = $value1['asset_number'];
                }
            }
        }
    }
    return array($assets,$orders,$multipleTrans);
}
function checkingAdminOpenRepair($allAssets,$country_id)
{
	$checkData = adminOpenRepair($country_id);
	return issueAssetsAndOrders($allAssets,$country_id,$checkData);
}

function checkingInTransitShouldBeButNotAvailable($allAssets,$country_id)
{
	$checkData1 = ackFromWHIssues($country_id);
	$checkData2 = feToFEAck($country_id,4);
	$checkData3 = logSTAck($country_id);
	$checkData4 = logFeAck($country_id,1);
	$checkDataFinal1 = $checkData1[0];
	$checkDataFinal2 = $checkData2[0];
	$checkDataFinal3 = $checkData3[0];
	$checkDataFinal4 = $checkData4[0];
	$checkDataAssets = array();
	# same assets lies in both transactions
	$issueAssets = array();
	$issueOrders = array();
	if(count($checkDataFinal1)>0){
		$checkData = $checkDataFinal1;
		if(count($checkData)>0){
			foreach ($checkDataFinal2 as $key => $value) {
				if(array_key_exists($key, $checkData)) {
					$issueAssets[$key] = $checkData[$key];
					if(array_key_exists($checkData[$key], $issueOrders)) {
						$issueOrders["-".$checkData[$key]] = $key;
					}
					else{
						$issueOrders[$checkData[$key]] = $key;
					}
					if(array_key_exists($value, $issueOrders)){
						$issueOrders["-".$value] = $key;
					}
					else{
						$issueOrders[$value] = $key;
					}
				}
				else
				{
					$checkData[$key] = $value;
				}
			}
		}
		if(count($checkDataFinal3)>0){
			foreach ($checkDataFinal3 as $key => $value) {
				if(array_key_exists($key, $checkData)) {
					$issueAssets[$key] = $checkData[$key];
					if(array_key_exists($checkData[$key], $issueOrders)) {
						$issueOrders["-".$checkData[$key]] = $key;
					}
					else{
						$issueOrders[$checkData[$key]] = $key;
					}
					if(array_key_exists($value, $issueOrders)){
						$issueOrders["-".$value] = $key;
					}
					else{
						$issueOrders[$value] = $key;
					}
				}
				else
				{
					$checkData[$key] = $value;
				}
			}	
		}
		if(count($checkDataFinal4)>0){
			foreach ($checkDataFinal4 as $key => $value) {
				if(array_key_exists($key, $checkData)) {
					$issueAssets[$key] = $checkData[$key];
					if(array_key_exists($checkData[$key], $issueOrders)) {
						$issueOrders["-".$checkData[$key]] = $key;
					}
					else{
						$issueOrders[$checkData[$key]] = $key;
					}
					if(array_key_exists($value, $issueOrders)){
						$issueOrders["-".$value] = $key;
					}
					else{
						$issueOrders[$value] = $key;
					}
				}
				else
				{
					$checkData[$key] = $value;
				}
			}	
		}
	}
	else
	{
		$checkData = $checkDataFinal2;
		if(count($checkDataFinal2)>0){
			if(count($checkDataFinal3)>0){
				foreach ($checkDataFinal3 as $key => $value) {
					if(array_key_exists($key, $checkData)) {
						$issueAssets[$key] = $checkData[$key];
						if(array_key_exists($checkData[$key], $issueOrders)) {
							$issueOrders["-".$checkData[$key]] = $key;
						}
						else{
							$issueOrders[$checkData[$key]] = $key;
						}
						if(array_key_exists($value, $issueOrders)){
							$issueOrders["-".$value] = $key;
						}
						else{
							$issueOrders[$value] = $key;
						}
					}
					else
					{
						$checkData[$key] = $value;
					}
				}	
			}
			if(count($checkDataFinal4)>0){
				foreach ($checkDataFinal4 as $key => $value) {
					if(array_key_exists($key, $checkData)) {
						$issueAssets[$key] = $checkData[$key];
						if(array_key_exists($checkData[$key], $issueOrders)) {
							$issueOrders["-".$checkData[$key]] = $key;
						}
						else{
							$issueOrders[$checkData[$key]] = $key;
						}
						if(array_key_exists($value, $issueOrders)){
							$issueOrders["-".$value] = $key;
						}
						else{
							$issueOrders[$value] = $key;
						}
					}
					else
					{
						$checkData[$key] = $value;
					}
				}	
			}
		}
		else
		{
			$checkData = $checkDataFinal3;
			if(count($checkDataFinal4)>0){
				foreach ($checkDataFinal4 as $key => $value) {
					if(array_key_exists($key, $checkData)) {
						$issueAssets[$key] = $checkData[$key];
						if(array_key_exists($checkData[$key], $issueOrders)) {
							$issueOrders["-".$checkData[$key]] = $key;
						}
						else{
							$issueOrders[$checkData[$key]] = $key;
						}
						if(array_key_exists($value, $issueOrders)){
							$issueOrders["-".$value] = $key;
						}
						else{
							$issueOrders[$value] = $key;
						}
					}
					else
					{
						$checkData[$key] = $value;
					}
				}	
			}
			else
			{
				$checkData = $checkDataFinal4;
			}
		}
	}
    $finalIssues = InTransaction($allAssets,$country_id,$checkData);
    # 0-not available in trturn and pending pickup
    # 1,2 -Assets are available for return order and pending pickup
    # 3,4 - assets are theerre for returning more then one transaction in same order
    //echo implode("','", $finalIssues[0]);exit;
    return array($finalIssues[0],$issueAssets,$issueOrders,$checkData1[2],$checkData2[2],$checkData3[2],$checkData4[2]);
}

function defectiveAssetRecords($country_id)
{
	$CI = &get_instance();
	$CI->load->model('Special_cases_m');
	$childData = $CI->Special_cases_m->defectiveAssetRecords($country_id);
	// echo "<pre>";print_r($childData);exit;
	$assets = array();
	$orders = array();
	$multipleTrans = array();
    if(count($childData)>0){
    	$df="DF";
    	$i = 1;
        foreach ($childData as $key1 => $value1) {
           $assets[$value1['asset_number']]  = $value1['type'];
           $orders[$df.$i.'--'.$value1['type']]  = $value1['asset_number'];
           $i++;            
        }
    }
    // echo "<pre>";print_r($assets);
    // echo "<pre>";print_r($orders);exit;
    return array($assets,$orders,$multipleTrans);
}
function checkingDefAssetDataNotInDefeAsset($allAssets,$country_id)
{
	// echo "<pre>";print_r($allAssets);exit;
	$checkData = defectiveAssetRecords($country_id);
	return InTransaction($allAssets,$country_id,$checkData[0]);
}

function commonConditions($status,$allAssets,$country_id,$default = array())
{
	$CI = &get_instance();
	if(count($default) == 0)
		$default = array(1,2,3,4,5,6,7,8,9);
	# FE
	if(in_array(1, $default)){
		$returnOrderIssues = checkingFeReturnTools($allAssets,$country_id);
		$returnOrderIssuesAssets =  $returnOrderIssues[0];
		if(count($returnOrderIssuesAssets)>0 || count($returnOrderIssues[2])>0 ){
		    echo "<br>Not- FE - Return Ordeer Issues <br>";
		    echo "<pre>";print_r($returnOrderIssuesAssets);
		    echo "Trans<br><pre>"; print_r($returnOrderIssues[2]);
		}
	}	
	if(in_array(2, $default)){
		$ackFromWHIssues = checkingAckFromWHTools($allAssets,$country_id);
    	$ackFromWHIssuesAssets = $ackFromWHIssues[0];	
    	if(count($ackFromWHIssuesAssets)>0 || count($ackFromWHIssues[2])>0){
		    echo "<br>Not- FE - Ack From WH <br>";
		    echo "<pre>";print_r($ackFromWHIssuesAssets);
		    echo "Trans<br><pre>"; print_r($ackFromWHIssues[2]);
		}
	}
	if(in_array(3, $default)){
		$feToFEIssues = checkingFeToFETools($allAssets,$country_id);
    	$feToFEIssuesAssets = $feToFEIssues[0];
    	if(count($feToFEIssuesAssets)>0 || count($feToFEIssues[2])>0){
		    echo "<br>Not- FE - ACK From FE<br>";
		    echo "<pre>";print_r($feToFEIssuesAssets);
		    echo "Trans<br><pre>";  print_r($feToFEIssues[2]);
		}
	}
	if(in_array(4, $default)){
		$pedningPickups = checkingFePendingPickup($allAssets,$country_id);
    	$pedningPickupsAssets = $pedningPickups[0];
    	if(count($pedningPickupsAssets)>0 || $pedningPickups[2]){
		    echo "<br>Not- FE - Pending Pickup <br>";
		    echo "<pre>";print_r($pedningPickupsAssets);
		    echo "Trans<br><pre>";  print_r($pedningPickups[2]);
		}
	}
	# Log
	if(in_array(5, $default)){
		$logPickups = checkingLogPickups($allAssets,$country_id);
    	$logPickupsAssets = $logPickups[0];
    	if(count($logPickupsAssets)>0 || count($logPickups[2]) > 0){
		    echo "<br>Not- Log - Pending Pickup <br>";
		    echo "<pre>";print_r($logPickupsAssets);
		    echo "Trans<br><pre>";  print_r($logPickups[2]);
		}
	}
	if(in_array(6, $default)){
		$logFEAck = checkingLogFEAck($allAssets,$country_id);
    	$logFEAckAssets= $logFEAck[0];
    	if(count($logFEAckAssets)>0 || count($logFEAck[2]) > 0){
		    echo "<br>Not- Log - Ack FE <br>";
		    echo "<pre>";print_r($logFEAckAssets);
		    echo "Trans<br><pre>";  print_r($logFEAck[2]);
		}
	}
	if(in_array(7, $default)){
		$logSTAck = checkingLogSTAck($allAssets,$country_id);
    	$logSTAckAssets = $logSTAck [0];    	
		if(count($logSTAckAssets)>0 || count($logSTAck[2]) > 0){
		    echo "<br>Not- Log - Ack ST <br>";
		    echo "<pre>";print_r($logSTAckAssets);
		    echo "Trans<br><pre>"; print_r($logSTAck[2]);
		}
	}
	# Admin
	if(in_array(8, $default)){
		$adminOpenCalibration = checkingAdminOpenCalibration($allAssets,$country_id);
	    $adminOpenCalibrationAssets = $adminOpenCalibration[0];
		if(count(@$adminOpenCalibrationAssets)>0|| count(@$adminOpenCalibration[2]) >0 ){
		    echo "<br>Not- Admin - Open Calibration <br>";
		    echo "<pre>";print_r($adminOpenCalibrationAssets);
		    echo "Trans<br><pre>"; print_r($adminOpenCalibration[2]);
		}    
	}
	if(in_array(9, $default)){
		$AdminOpenRepair = checkingAdminOpenRepair($allAssets,$country_id);
	    $AdminOpenRepairAssets = $AdminOpenRepair[0];
	    if(count(@$AdminOpenRepairAssets)>0 || count(@$AdminOpenRepair[2])>0 ){
		    echo "<br>Not- Admin - Open Repair <br>";
		    echo "<pre>";print_r($AdminOpenRepairAssets);
		    echo "Trans<br><pre>";  print_r($AdminOpenRepair[2]);
		}
	}
}


function assetPositionCheck($status,$allAssets)
{
	$issueAssets = array();
	switch ($status) {
		case 1: case 2:
			foreach ($allAssets as $key => $value) {
				if($value['transit'] == 1 || $value['wh_id']=='' || $value['sso_id']!='')
					$issueAssets[] = $value['asset_number'];
			}
			break;
		case 3:
			foreach ($allAssets as $key => $value) {
				if($value['transit'] == 1 || $value['wh_id']!='' || $value['sso_id']=='')
					$issueAssets[] = $value['asset_number'];
			}
		break;
		case 4: case 10: case 12:
			foreach ($allAssets as $key => $value) {
				if($value['sso_id']!='')
					$issueAssets[] = $value['asset_number'];
			}
		break;
		case 8:
			foreach ($allAssets as $key => $value) {
				if( !( ($value['wh_id']=='' && $value['sso_id']!='' && $value['transit'] == 1 )  || ($value['wh_id']!='' && $value['sso_id']=='' && $value['transit'] == 1) ) )
					$issueAssets[] = $value['asset_number'];
			}
		break;
		case 9: case 11:
			foreach ($allAssets as $key => $value) {
				if($value['transit'] == 1 || $value['wh_id']=='' || $value['sso_id']!='')
					$issueAssets[] = $value['asset_number'];
			}
		break;
		default:
			# code...
			break;
	}
	return $issueAssets;
}
