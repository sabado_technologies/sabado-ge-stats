<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tool_m extends CI_Model 
{
	public function get_active_sub_component($tool_main_id)
	{
		$this->db->select('tp.*,CONCAT(t.part_number," - ",t.part_description) AS part_desc_name,t.tool_id');
		$this->db->from('tool_part tp');
		$this->db->join('tool t','t.tool_id = tp.tool_sub_id');
		$this->db->where('tp.tool_main_id',$tool_main_id);
		$this->db->where('t.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}

public function tool_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('t.*,tl.name as tool_level_name,l.name as country');
		$this->db->from('tool t');
		$this->db->join('part_level tl','tl.part_level_id = t.part_level_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('location l','l.location_id=t.country_id');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['tool_code']!='')
			$this->db->like('t.tool_code',$searchParams['tool_code']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['part_level_id']!='')
			$this->db->where('tl.part_level_id',$searchParams['part_level_id']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('t.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('t.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('t.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('t.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
		}
		$this->db->order_by('t.tool_id ASC, t.country_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function tool_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('t.*,tl.name as tool_level_name');
		$this->db->from('tool t');
		$this->db->join('part_level tl','tl.part_level_id = t.part_level_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('location l','l.location_id=t.country_id');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['tool_code']!='')
			$this->db->like('t.tool_code',$searchParams['tool_code']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['part_level_id']!='')
			$this->db->where('tl.part_level_id',$searchParams['part_level_id']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('t.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('t.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('t.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('t.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
		}
		$this->db->order_by('t.tool_id ASC, t.country_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function get_tool_parts($tool_id)
	{
		$this->db->select('t2.tool_id,t2.part_number,t2.part_description,tp.quantity,t2.tool_code,t2.status');
		$this->db->from('tool t');
		$this->db->join('tool_part tp','t.tool_id = tp.tool_main_id');
		$this->db->join('tool t2','t2.tool_id = tp.tool_sub_id');
		$this->db->where('tp.status',1);
		$this->db->where('tp.tool_main_id',$tool_id);
		$res = $this->db->get();
		return $res->result_array();
	}


	public function check_part_number_availability($data)
	{
		$this->db->from('tool t');
		$this->db->where('part_number', $data['part_number']);
		if($data['tool_id']!=0 || $data['tool_id']!='')
		{
			$this->db->where_not_in('tool_id', @$data['tool_id']);
		}
		
		$this->db->where('country_id',$data['country_id']);
		$res = $this->db->get();
		return $res->num_rows();	
	}
	public function check_tool_code_availability($data)
	{
		$this->db->from('tool t');
		$this->db->where('tool_code', $data['tool_code']);
		if($data['tool_id']!=0 || $data['tool_id']!='')
		{
			$this->db->where_not_in('tool_id', @$data['tool_id']);
		}
		$this->db->where('country_id',$data['country_id']);
		$res = $this->db->get();
		return $res->num_rows();	
	}
	public function tool_eq_model_insert_update($tool_id,$eq_model_id)
	{
		$qry = "INSERT INTO equipment_model_tool(tool_id, eq_model_id, status) 
                    VALUES (".$tool_id.",".$eq_model_id.",'1')  
                    ON DUPLICATE KEY UPDATE status = VALUES(status);";
        $this->db->query($qry);
	}

	public function tool_part_insert_update($tool_id,$tool_sub_id,$quantity)
	{
		$qry = "INSERT INTO tool_part(tool_main_id, tool_sub_id, quantity, status) 
                    VALUES (".$tool_id.",".$tool_sub_id.",".$quantity.",'1')  
                    ON DUPLICATE KEY UPDATE status = VALUES(status), quantity = VALUES(quantity);";
        $this->db->query($qry);
	}
	public function missed_tool_upload_details()
	{
		$this->db->select('ut.*');
		$this->db->from('upload u');
		$this->db->join('upload_tool ut','u.upload_id = ut.upload_id');
		$this->db->order_by('u.upload_id DESC');
		$this->db->where('u.status',2);
		$this->db->where('u.type',1);
		//$this->db->limit('1');
		$res = $this->db->get();
		return $res->result_array();
	}
public function missed_tool_results($current_offset, $per_page, $upload_id)
	{
		
		$this->db->select('ut.*,utp.*');
		$this->db->from('upload_tool ut');
		$this->db->join('upload_tool_part utp','ut.upload_tool_id = utp.upload_tool_id');
		$this->db->where('ut.upload_id',$upload_id);
		$this->db->limit($per_page, $current_offset);
		
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function missed_tool_total_num_rows($upload_id)
	{
		$this->db->select('ut.*,utp.*');
		$this->db->from('upload_tool ut');
		$this->db->join('upload_tool_part utp','ut.upload_tool_id = utp.upload_tool_id');
		$this->db->where('ut.upload_id',$upload_id);		
		$res = $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->num_rows();
	}
	public function download_missed_tool_results($upload_id)
	{
		
		$this->db->select('ut.*,utp.*');
		$this->db->from('upload_tool ut');
		$this->db->join('upload_tool_part utp','ut.upload_tool_id = utp.upload_tool_id');
		$this->db->where('ut.upload_id',$upload_id);
		
		
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_latest_tool_code($country_id)
	{
		$this->db->select('t.tool_code');
		$this->db->from('tool t');
		$this->db->where('t.country_id',$country_id);
		$this->db->order_by('CAST(t.tool_code AS unsigned) DESC');
		$this->db->limit(1);
		$res = $this->db->get();
		$result = $res->row_array();
		if(count($result) == 0)
		{
			return get_tool_code(1);
		}
		else
		{
			$tool = (int)$result['tool_code'];
			$count = $tool+1;
			return get_tool_code($count);
		}
	}
}	