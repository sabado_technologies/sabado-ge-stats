<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_m extends CI_Model 
{
	
	public function role_total_num_rows($searchParams)
	{
		
		$this->db->select();
		$this->db->from('role');
		if($searchParams['rolename']!='')
			$this->db->like('name',$searchParams['rolename']);
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function role_results($current_offset, $per_page, $searchParams)
	{
		
		$this->db->select();
		$this->db->from('role');
		if($searchParams['rolename']!='')
			$this->db->like('name',$searchParams['rolename']);
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function get_pages_total_rows($searchParams,$role_id)
	{
		$this->db->select('p.page_id,p.name,r.name as role_name');
		$this->db->from('role_page rp');
		$this->db->join('role_login rl','rp.role_id = rl.child_role_id','LEFT');
		$this->db->join('task t','t.page_id = rp.page_id','LEFT');
		$this->db->join('page p','p.page_id = rp.page_id','LEFT');
		$this->db->join('role r','r.role_id = rl.child_role_id','LEFT');
		if($searchParams['page_id']!='')
			$this->db->where('t.page_id',$searchParams['page_id']);
		if($searchParams['task_id']!='')
			$this->db->where('t.task_id',$searchParams['task_id']);
		if($searchParams['p_role_id']!='')
			$this->db->where('rl.child_role_id',$searchParams['p_role_id']);
		$this->db->where('p.status',1);
		$this->db->where('rl.parent_role_id',$role_id);
		$this->db->group_by('p.page_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function get_pages_results($current_offset, $per_page, $searchParams,$role_id)
	{
		$this->db->select('p.page_id,p.name,p.title as task_title,r.name as role_name');
		$this->db->from('role_page rp');
		$this->db->join('role_login rl','rp.role_id = rl.child_role_id','LEFT');
		$this->db->join('task t','t.page_id = rp.page_id','LEFT');
		$this->db->join('page p','p.page_id = rp.page_id','LEFT');
		$this->db->join('role r','r.role_id = rl.child_role_id','LEFT');
		if($searchParams['page_id']!='')
			$this->db->where('t.page_id',$searchParams['page_id']);
		if($searchParams['task_id']!='')
			$this->db->where('t.task_id',$searchParams['task_id']);
		if($searchParams['p_role_id']!='')
			$this->db->where('rl.child_role_id',$searchParams['p_role_id']);
		$this->db->where('p.status',1);
		$this->db->where('rl.parent_role_id',$role_id);
		$this->db->group_by('p.page_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function insert_update($role_id,$task_id,$task_access)
	{
		$qry = "INSERT INTO role_task(role_id, task_id, status,task_access) 
                    VALUES (".$role_id.",".$task_id.",'1',".$task_access.")  
                    ON DUPLICATE KEY UPDATE status = VALUES(status);";
        $this->db->query($qry);
	}

	public function get_tasks_results($page_id)
	{
		$this->db->select('t.*,rt.task_id as role_task_id,rt.role_id,rt.task_access');
		$this->db->from('task t');
		$this->db->join('role_task rt','rt.task_id=t.task_id','left');
		$this->db->where('t.page_id',$page_id);
		$res=$this->db->get();
		return $res->result_array();
	}

	
   public function check_role_name_availability($data)
	{
		$this->db->from('role');
		$this->db->where('name', $data['name']);
		if($data['role_id']!=0 || $data['role_id']!='')
		{
			$this->db->where_not_in('role_id', $data['role_id']);
		}
		$res = $this->db->get();
		return $res->num_rows();	
	}

	public function get_task_pages_total_rows($searchParams)
	{
		$this->db->select();
		$this->db->from('page');
		if($searchParams['page_id']!='')
			$this->db->where('page_id',$searchParams['page_id']);
		$this->db->where('status',1);
		$res=$this->db->get();
		return $res->num_rows();
	}

	public function get_task_pages_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select();
		$this->db->from('page');
		if($searchParams['page_id']!='')
			$this->db->where('page_id',$searchParams['page_id']);
		$this->db->where('status',1);
		$this->db->limit($per_page, $current_offset);
		$res=$this->db->get();
		return $res->result_array();
	}

	public function get_roles_login_total_rows($searchParams)
	{
		$this->db->select();
		$this->db->from('role');
		if($searchParams['p_role']!='')
			$this->db->like('name',$searchParams['p_role']);
		$this->db->where('status',1);
		$res=$this->db->get();
		return $res->num_rows();
	}

	public function get_roles_login_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select();
		$this->db->from('role');
		if($searchParams['p_role']!='')
			$this->db->like('name',$searchParams['p_role']);
		$this->db->where('status',1);
		$this->db->limit($per_page, $current_offset);
		$res=$this->db->get();
		return $res->result_array();
	}

	public function get_child_roles($parent_role_id)
	{
		$this->db->select('r.name,r.role_id');
		$this->db->from('role r');
		$this->db->join('role_login rl','r.role_id = rl.child_role_id');
		$this->db->where('rl.parent_role_id',$parent_role_id);
		$this->db->where('rl.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}

}