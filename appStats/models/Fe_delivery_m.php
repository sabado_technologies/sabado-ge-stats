<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fe_delivery_m extends CI_Model 
{
	public function get_latest_record()
	{
		$this->db->select('order_number');
		$this->db->from('tool_order');
		$this->db->where('order_type',2);
		$this->db->order_by('tool_order_id DESC');
		$res = $this->db->get();
		return $res->row_array();
	}
	public function fe_delivery_list_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.tool_order_id');
		$this->db->from('tool_order to');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l1','l1.location_id = to.country_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('service_type st','st.service_type_id = to.service_type_id');
		if(@$searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if(@$searchParams['courier_date']!='')
			$this->db->like('to.request_date',$searchParams['courier_date']);
		if(@$searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($searchParams['service_type_id']!='')
			$this->db->where('to.service_type_id',$searchParams['service_type_id']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in("to.wh_id",$_SESSION['whsIndededArray']); 
				}
				else
				{
					$this->db->where('to.wh_id',$this->session->userdata('s_wh_id'));	
				}
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

		}
		//$this->db->where('to.wh_id',$wh_id);
		$this->db->where('to.current_stage_id',5);
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->order_by('to.request_date ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function fe_delivery_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('date_sub(to.request_date, INTERVAL 2 DAY) AS courier_date,to.tool_order_id,to.order_number,to.request_date,u.name as user_name,u.sso_id,CONCAT(oa.address3,"," ,oa.address4) as location_name,to.fe_check,l1.name as country,concat(wh.wh_code," -(",wh.name,")") as wh_name,st.name as service_type_name');
		$this->db->from('tool_order to');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l1','l1.location_id = to.country_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('service_type st','st.service_type_id = to.service_type_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['courier_date']!='')
			$this->db->where('to.request_date',$searchParams['courier_date']);
		if($searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($searchParams['service_type_id']!='')
			$this->db->where('to.service_type_id',$searchParams['service_type_id']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in("to.wh_id",$_SESSION['whsIndededArray']); 
				}
				else	
				{			
					$this->db->where('to.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
			
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('to.current_stage_id',5);
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->order_by('to.request_date ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_tool_order_details($tool_order_id)
	{
		$this->db->select('to.wh_id,to.order_number,to.request_date,to.return_date,
			u.sso_id,u.name,oa.*,to.order_delivery_type_id,to.country_id,to.notes');
		$this->db->from('tool_order to');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_ordered_tool($tool_order_id)
	{
		$this->db->select('to.tool_order_id,ot.ordered_tool_id,t.part_number,
				           t.part_description,ot.quantity as requested_qty,
						   ot.ordered_tool_id,ot.tool_id,to.wh_id');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('ot.status',1); //only active tools
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_oa_by_ot($ordered_tool_id)
	{
		$this->db->select('GROUP_CONCAT(a.asset_number SEPARATOR ",") as assets,
						   count(a.asset_number) as asset_count,GROUP_CONCAT(p.serial_number SEPARATOR ",") as serial_nos');
		$this->db->from('ordered_asset oa');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->where('oa.ordered_tool_id',$ordered_tool_id);
		$this->db->where('oa.status',1);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_count($ordered_tool_id)
	{
		$this->db->select('count(a.asset_number) as qty');
		$this->db->from('ordered_asset oa');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->where('oa.ordered_tool_id',$ordered_tool_id);
		$this->db->where('oa.status',2);
		$this->db->group_by('oa.ordered_tool_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_avail_tool($tool_id,$tool_order_wh_id)
	{
		$status = array(1,2);// available, lock in period
		$this->db->select('count(a.asset_id) as avail_qty');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('a.approval_status',0);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->where('p.part_level_id',1); // 1 = L0 level
		$this->db->where_in('a.status',$status);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_ordered_tool_detail($ordered_tool_id)
	{
		$this->db->select('to.tool_order_id,t.part_number,t.part_description,
						   to.order_number,u.sso_id,u.name,t.tool_id,
						   ot.ordered_tool_id,ot.tool_id,to.wh_id');
		$this->db->from('ordered_tool ot');
		$this->db->join('tool_order to','to.tool_order_id = ot.tool_order_id');
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('ot.ordered_tool_id',$ordered_tool_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_sub_inventory_asset($tool_id,$tool_order_wh_id,$asset_id_arr)
	{
		$status = array(1,2);
		$this->db->select('as.name as asset_status,t.part_number,p.serial_number,
						   t.part_description,a.asset_number,a.sub_inventory,a.cal_due_date');
		$this->db->from('tool t');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('p.part_level_id',1);
		$this->db->where('a.approval_status',0);
		$this->db->where_in('a.status',$status);
		$this->db->where_not_in('a.asset_id',$asset_id_arr);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->order_by('a.status DESC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function check_asset($tool_id,$asset_number,$tool_order_wh_id,$asset_id_arr)
	{
		$status = array(1,2);// available, lock in period
		$this->db->select('a.asset_id');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->where('a.approval_status',0);
		$this->db->where('p.part_level_id',1); // 1 = L0 level
		$this->db->where('a.asset_number',$asset_number);
		$this->db->where_in('a.status',$status);
		$this->db->where_not_in('a.asset_id',$asset_id_arr);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_asset_part_details($asset_id)
	{
		$status = array(1,2);// available, lock in period
		$this->db->select('a.asset_id,a.asset_number,p.part_id,
						  pl.name as part_level_name,t.part_number,
						  t.part_description,p.quantity,p.status,t.tool_id, a.status as asset_status,p.serial_number,a.sub_inventory');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		//$this->db->where('a.wh_id',$wh_id);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('a.approval_status',0); // not for approval
		$this->db->where_in('a.status',$status);
		$this->db->where('p.part_status',1);
		$this->db->order_by('p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	/*modified by koushik 11-10-2018*/
	public function get_first_asset_avail($tool_id,$tool_order_wh_id)
	{
		$getAssetsInTransferProcess = getAssetsInTransferProcess(array());
		$this->db->select('a.asset_id');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('a.approval_status',0);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->where('p.part_level_id',1); // 1 = L0 level
		$this->db->where('a.status',2);
		if(count($getAssetsInTransferProcess)>0)
		{
			$this->db->where_not_in('a.asset_id',$getAssetsInTransferProcess);
		}
		$res = $this->db->get();
		return $res->row_array();
	}

	//open fe delivery list
	public function open_fe_delivery_list_total_num_rows($searchParams)
	{
		$wh_id = $this->session->userdata('swh_id');
		$this->db->select('to.*,u.name as user_name,l.name as location_name,cs.name as current_stage');
		$this->db->from('tool_order to');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = oa.location_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['ssoid']!='')
			$this->db->like('to.sso_id',$searchParams['ssoid']);
		$this->db->where('to.wh_id',$wh_id);
		$this->db->where('to.current_stage_id',6);//6-at transit from wh to fe
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->order_by('to.request_date ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function open_fe_delivery_list_results($current_offset, $per_page, $searchParams)
	{
		$wh_id = $this->session->userdata('swh_id');
		$this->db->select('to.*,u.name as user_name,l.name as location_name,cs.name as current_stage');
		$this->db->from('tool_order to');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = oa.location_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['ssoid']!='')
			$this->db->like('to.sso_id',$searchParams['ssoid']);
		$this->db->where('to.wh_id',$wh_id);
		$this->db->where('to.current_stage_id',6);//6-at transit from wh to fe
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->order_by('to.request_date ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	//closed fe delivery list modified by Gowri for SSO dropdown on 6thAug'18
	public function closed_fe_delivery_list_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.*,u.name as user_name,l.name as location_name,cs.name as current_stage,osh.created_time as delivery_date,l1.name as country');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = oa.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = to.country_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if(@$searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('to.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('osh.current_stage_id',6);//6-at transit from wh to fe
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where('osh.rto_id IS NULL');
		$this->db->order_by('to.tool_order_id DESC');
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}
	//closed fe delivery list modified by Gowri for SSO dropdown on 6thAug'18
	public function closed_fe_delivery_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,u.name as user_name,l.name as location_name,cs.name as current_stage,od.expected_delivery_date as delivery_date,to.current_stage_id as tcs,l1.name as country,concat(wh.wh_code," (",wh.name,")") as wh_name');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = oa.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = to.country_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if(@$searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('to.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('osh.current_stage_id',6);//6-at transit from wh to fe
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where('osh.rto_id IS NULL');
		$this->db->order_by('to.tool_order_id DESC');
		$this->db->group_by('to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_assets_by_tool_order($tool_order_id)
	{
		$this->db->select('to.sso_id,oa.asset_id');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('ot.status',1);
		$this->db->where('oa.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}

	// modified by maruthi on 7th dec'17
	public function get_fe_address($tool_order_id)
	{
		$this->db->select('oa.*,concat(u.sso_id," (",u.name,")") as sso,u.email,u.name as sso_name,to.order_number,wh.address1 as wh_add1,wh.address2 as wh_add2, wh.address3 as wh_add3,wh.address4 as wh_add4, l.name as location_name, l2.name as wh_location,wh.pin_code as wh_pin_code, wh.gst_number as gst,wh.pan_number as pan,wh.tin_number as tin');
		$this->db->from('tool_order to');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = oa.location_id','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('location l1','l1.location_id = wh.location_id','LEFT');
		$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('oa.status',1);
		$this->db->where('to.tool_order_id',$tool_order_id);
		if(sendMailsEvenFEDeactivated()==1)
			$this->db->where('u.status',1);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_invoiced_tools($tool_order_id)
	{
		$this->db->select('ot.ordered_tool_id,count(ot.tool_id) as quantity,t.part_number,t.part_description,t.hsn_code,t.gst_percent,sum(a.cost_in_inr) as cost_in_inr, GROUP_CONCAT(CONCAT(p.serial_number," : ",a.asset_number) SEPARATOR ",") as asset_number_list');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','oa.asset_id = a.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('ot.tool_order_id',$tool_order_id);
		$this->db->where('p.part_level_id',1);
		$this->db->where('ot.status',1);// modified by maruthi on 16th sep'18
		$this->db->where('oa.status',1);// modified by maruthi on 16th sep'18
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}
	// created by maruthi on 7th dec'17 
	public function get_invoiced_assets($tool_order_id)
	{
		$this->db->select('ot.ordered_tool_id,a.asset_number,t.part_number,t.part_description,t.hsn_code,t.gst_percent,sum(a.cost_in_inr) as cost_in_inr');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','oa.asset_id = a.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->where('ot.tool_order_id',$tool_order_id);
		$this->db->group_by('p.asset_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('ot.status',1);// modified by maruthi on 16th sep'18
		$this->db->where('oa.status',1); // modified by maruthi on 16th sep'18
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_delivery_details($tool_order_id)
	{
		$this->db->select('pf.format_number,
						   pf.created_time,
			               pf.print_type,
						   u.sso_id,
						   u.mobile_no,
						   u.name as user_name,
						   od.courier_type,
						   od.courier_number,
						   od.vehicle_number,
						   to.order_number,
						   to.siebel_number,
						   od.billed_to,
						   od.remarks,
						   od.courier_name,
						   od.contact_person,
						   od.phone_number');
		$this->db->from('order_delivery od');
		$this->db->join('tool_order to','to.tool_order_id = od.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('print_format pf','pf.print_id = od.print_id');
		$this->db->where('od.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function check_for_scanned_assets($tool_order_id)
	{
		$this->db->select('');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('to.current_stage_id',5);
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function get_for_scanned_assets()
	{
		$this->db->select('oa.ordered_asset_id,oa.asset_id,to.order_number,to.tool_order_id');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->where('to.current_stage_id',5);
		$res = $this->db->get();
		return $res->result_array();
	}

	// created by maruthi on 7thSep'18
	public function getDataForTransit($tool_order_id)
	{
		$this->db->select('oah.*');
		$this->db->from('order_status_history osh');		
		$this->db->join('order_asset_history oah','osh.order_status_id = oah.order_status_id');		
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->where('oa.status<',3);
		$this->db->where('osh.current_stage_id',5);
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->result_array();
	}
}
?>