<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fe_position_change_m extends CI_Model 
{
	public function fe_position_change_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('u.sso_id');
		$this->db->from('fe_position_change fpc');
		$this->db->join('user u' ,'u.sso_id=fpc.sso_id');
		$this->db->join('location l','l.location_id=fpc.old_fe_position');
		$this->db->join('location s1','s1.location_id = l.parent_id');
		$this->db->join('location l1','l1.location_id=fpc.new_fe_position');
		$this->db->join('location s2','s2.location_id = l1.parent_id');
		$this->db->join('warehouse w','w.wh_id=u.wh_id');
		$this->db->join('location l2','l2.location_id=u.country_id');
		if(@$searchParams['ssoid']!='')
			$this->db->like('fpc.sso_id',$searchParams['ssoid']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('u.country_id',$this->session->userdata('s_country_id'));
		}
		else if(@$task_access == 3)
		{

			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('u.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('u.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('u.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
		}
		$this->db->where('fpc.approval_status',1);
		$this->db->order_by('fpc.fpc_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function fe_position_change_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('u.name as user_name,u.wh_id,l2.name as country_name,CONCAT(l.name,", ",s1.name) as old_location,CONCAT(l1.name,", ",s2.name) as new_location,u.sso_id,CONCAT(w.wh_code," -(",w.name,")") as warehouse,fpc.fpc_id,fpc.old_fe_position,fpc.new_fe_position,u.country_id');
		$this->db->from('fe_position_change fpc');
		$this->db->join('user u' ,'u.sso_id=fpc.sso_id');
		$this->db->join('location l','l.location_id=fpc.old_fe_position');
		$this->db->join('location s1','s1.location_id = l.parent_id');
		$this->db->join('location l1','l1.location_id=fpc.new_fe_position');
		$this->db->join('location s2','s2.location_id = l1.parent_id');
		$this->db->join('warehouse w','w.wh_id=u.wh_id');
		$this->db->join('location l2','l2.location_id=u.country_id');
		if(@$searchParams['ssoid']!='')
			$this->db->like('fpc.sso_id',$searchParams['ssoid']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('u.country_id',$this->session->userdata('s_country_id'));
		}
		else if(@$task_access == 3)
		{

			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('u.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('u.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('u.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
		}
		$this->db->where('fpc.approval_status',1);
		$this->db->order_by('fpc.fpc_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function get_country_wise_warehouse($wh_id,$country_id)
	{
		$this->db->select('w.*,c2.name as city_name,s3.name as state_name');
		$this->db->from('warehouse w');
		$this->db->join('location a1','a1.location_id = w.location_id');
		$this->db->join('location c2','c2.location_id = a1.parent_id');
		$this->db->join('location s3','s3.location_id = c2.parent_id');
		if(@$country_id!='')
		{
			$this->db->where('w.country_id',$country_id);
		}
		if(@$wh_id!='')
		{
			$this->db->where_not_in('w.wh_id',$wh_id);
		}
		$this->db->where('w.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}
}