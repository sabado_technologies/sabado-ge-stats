<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eq_model_model extends CI_Model 
{
	//starting of Equipment model crud methods//
	public function eq_model_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('m.*,m.name as eq_model_name');
		$this->db->from('equipment_model m');
		if($searchParams['eq_model_name']!='')
			$this->db->like('m.name',$searchParams['eq_model_name']);
		$this->db->limit($per_page, $current_offset);
		$this->db->order_by('m.eq_model_id','DESC');
		$res = $this->db->get();
		return $res->result_array();
	}
	public function eq_model_total_num_rows($searchParams)
	{
		$this->db->select('m.*,m.name as eq_model_name');
		$this->db->from('equipment_model m');
		if($searchParams['eq_model_name']!='')
			$this->db->like('m.name',$searchParams['eq_model_name']);
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function get_eq_model($eq_model_id)
	{
		$this->db->select('m.*,m.name as eq_model_name');
		$this->db->from('equipment_model m');
		$res = $this->db->get();
		return $res->num_rows();
	}
//uniqueness of Equipment model name
	public function is_eq_model_nameExist($data)
    {       
        $this->db->select();
        $this->db->from('equipment_model m');
        $this->db->where('m.name',$data['name']);
        if($data['eq_model_id']!='' || $data['eq_model_id']!=0)
        $this->db->where_not_in('m.eq_model_id',$data['eq_model_id']);
        $query = $this->db->get();
        return ($query->num_rows()>0)?1:0;
    }
}	