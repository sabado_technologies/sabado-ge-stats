<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asset_m extends CI_Model 
{	
	public function get_wh_country_1_2($country_id)
	{
		$arr = array(1,3);
		$this->db->select('*');
		$this->db->from('warehouse');
		$this->db->where('country_id',$country_id);
		$this->db->where_in('status',$arr);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_gps_tracker_list($country_id)
	{
		$this->db->select('g.name,g.gps_tool_id,g.uid_number');
		$this->db->from('gps_tool g');
		$this->db->join('asset a','a.gps_tool_id = g.gps_tool_id','LEFT');
		$this->db->where('a.gps_tool_id IS NULL');
		$this->db->where('g.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function asset_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('a.*, wh.name as warehouse, wh.wh_code as wh_code, s.name as status_name, m.name as modality_name, tt.name as tool_type_name, t.part_number, t.part_description, l.name as location, p.serial_number, l.region_id, t.cal_type_id');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('asset_status s','s.asset_status_id =a.status');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['cal_type_id']!='')
			$this->db->where('t.cal_type_id',$searchParams['cal_type_id']);
		if($searchParams['a_asset_no']!='')
			$this->db->like('a.asset_number',$searchParams['a_asset_no']);
		if($searchParams['asset_modality_arr']!='')
			$this->db->where_in('t.modality_id',$searchParams['asset_modality_arr']);
		if($searchParams['asset_wh_arr']!='')
			$this->db->where_in('a.wh_id',$searchParams['asset_wh_arr']);
		if($searchParams['asset_status_arr']!='')
			$this->db->where_in('a.status',$searchParams['asset_status_arr']);
		if($searchParams['asset_part_no']!='')
			$this->db->like('t.part_number',$searchParams['asset_part_no']);
		if($searchParams['asset_part_desc']!='')
			$this->db->like('t.part_description',$searchParams['asset_part_desc']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('a.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['asset_country_id']!='')
				{
					$this->db->where('a.country_id',$searchParams['asset_country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('a.asset_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	#kousik
	public function asset_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('a.*,wh.name as warehouse,wh.wh_code as wh_code,s.name as status_name,m.name as modality_name,tt.name as tool_type_name,t.part_number,t.part_description,l.name as location');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('asset_status s','s.asset_status_id =a.status');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['cal_type_id']!='')
			$this->db->where('t.cal_type_id',$searchParams['cal_type_id']);
		if($searchParams['a_asset_no']!='')
			$this->db->like('a.asset_number',$searchParams['a_asset_no']);
		if($searchParams['asset_modality_arr']!='')
			$this->db->where_in('t.modality_id',$searchParams['asset_modality_arr']);
		if($searchParams['asset_wh_arr']!='')
			$this->db->where_in('a.wh_id',$searchParams['asset_wh_arr']);
		if($searchParams['asset_status_arr']!='')
			$this->db->where_in('a.status',$searchParams['asset_status_arr']);
		if($searchParams['asset_part_no']!='')
			$this->db->like('t.part_number',$searchParams['asset_part_no']);
		if($searchParams['asset_part_desc']!='')
			$this->db->like('t.part_description',$searchParams['asset_part_desc']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('a.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['asset_country_id']!='')
				{
					$this->db->where('a.country_id',$searchParams['asset_country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('a.asset_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	#koushik
    public function asset_details_view($asset_id,$tool_id)
	{
		$this->db->select('t.part_description,t.part_number,t.tool_code,
			p.serial_number,ac.name as asset_condition_name,
			p.remarks as part_remarks,a.*,t.kit as kit,m.name as modality_name,
			tt.name as tool_type_name,wh.wh_code as warehouse_code,
			a.sub_inventory as sub_inventory,at.name as asset_type,
			s.name as supplier_name,a.remarks as asset_remarks,ct.cal_type_id,
			ct.name as calibration,cs.name as cs_name,a.cal_due_date as due_date,
			t.manufacturer,a.po_number as po,a.earpe_number as earpe,
			a.date_of_use as date_of_used,ad.name as document,
			t.part_number as part_num,t.tool_code as toolcode,
			t.part_description as description,p.serial_number as serialnumber,
			p.remarks as remark,p.status as statuss,
			al.name as asset_level_name,p.quantity,CONCAT(gpt.uid_number, " - ",gpt.name) AS gps_tracker');
		$this->db->from('asset a');
		$this->db->join('gps_tool gpt','gpt.gps_tool_id = a.gps_tool_id','LEFT');
		$this->db->join('part p','a.asset_id = p.asset_id');
		$this->db->join('tool t','p.tool_id =t.tool_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id =t.tool_type_id');
		$this->db->join('asset_type at','at.asset_type_id =t.asset_type_id');
		$this->db->join('asset_document ad','ad.asset_id =a.asset_id','left');		
		$this->db->join('modality m','m.modality_id =a.modality_id ');
		$this->db->join('supplier cs','cs.supplier_id = a.cal_supplier_id','left');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('supplier s','s.supplier_id=t.supplier_id');
		$this->db->join('warehouse wh','a.wh_id = wh.wh_id');
		$this->db->join('asset_status as','as.asset_status_id =a.status ');
		$this->db->join('part_level pl','pl.part_level_id= p.part_level_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = p.status');
		$this->db->where('p.asset_id',$asset_id);
		$this->db->where('p.tool_id',$tool_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	#koushik
    public function asset_details_view1($asset_id)
	{
		$this->db->select('t.part_description,t.part_number,t.tool_code,
			p.serial_number,ac.name as asset_condition_name,
			p.remarks as part_remarks,a.*,t.kit as kit,m.name as modality_name,
			tt.name as tool_type_name,wh.wh_code as warehouse_code,
			a.sub_inventory as sub_inventory,at.name as asset_type,
			s.name as supplier_name,a.remarks as asset_remarks,
			ct.name as calibration,cs.name as cs_name,a.cal_due_date as due_date,
			t.manufacturer,a.po_number as po,a.earpe_number as earpe,
			a.date_of_use as date_of_used,ad.name as document,
			t.part_number as part_num,t.tool_code as toolcode,
			t.part_description as description,p.serial_number as serialnumber,
			p.remarks as remark,p.status as statuss,
			al.name as asset_level_name,p.quantity');
		$this->db->from('asset a');
		$this->db->join('part p','a.asset_id = p.asset_id');
		$this->db->join('tool t','p.tool_id =t.tool_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id =t.tool_type_id');
		$this->db->join('asset_type at','at.asset_type_id =t.asset_type_id');
		$this->db->join('asset_document ad','ad.asset_id =a.asset_id','left');		
		$this->db->join('modality m','m.modality_id =a.modality_id ');
		$this->db->join('supplier cs','cs.supplier_id = a.cal_supplier_id','left');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('supplier s','s.supplier_id=t.supplier_id');
		$this->db->join('warehouse wh','a.wh_id = wh.wh_id');
		$this->db->join('asset_status as','as.asset_status_id =a.status ');
		$this->db->join('part_level pl','pl.part_level_id= p.part_level_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = p.status');
		$this->db->where('p.asset_id',$asset_id);
		$this->db->where('p.part_status',1);
		$this->db->where('p.part_level_id',1);
		$res = $this->db->get();
		return $res->row_array();
	}
	
	#koushik
	public function get_l1_sub_component_details($asset_id)
	{
		$this->db->select('t.part_number,t.part_description,t.tool_code,p.*,ac.name as asset_condition_name');
		$this->db->from('asset a');
		$this->db->join('part p','a.asset_id = p.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = p.status');
		$this->db->where('p.part_status',1);
		$this->db->where('p.part_level_id',2);
		$this->db->where('p.asset_id',$asset_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	#kousik
	public function get_tool_related_info($tool_id)
	{
		$this->db->select('t.*,al.name as asset_level_name,
						  tt.name as tool_type_name,s.name as supplier_name,m.name as modality_name');
		$this->db->from('tool t');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('part_level pl','pl.part_level_id = t.part_level_id');
		$this->db->join('supplier s','s.supplier_id = t.supplier_id');
		$this->db->join('asset_type at','at.asset_type_id = t.asset_type_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->where('t.tool_id',$tool_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	#kousik
	public function get_eq_model_list($tool_id)
	{
		$this->db->select('GROUP_CONCAT(eqm.name SEPARATOR ", ") as eq_model');
		$this->db->from('equipment_model_tool eqt');
		$this->db->join('tool t','t.tool_id = eqt.tool_id');
		$this->db->join('equipment_model eqm','eqm.eq_model_id = eqt.eq_model_id');
		$this->db->where('t.tool_id',$tool_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	#kousik
	public function get_asset_sub_component_details($tool_id,$asset_id)
	{
		$this->db->select('tp.quantity,t.*,p.part_id');
		$this->db->from('tool_part tp');
		$this->db->join('part p','p.tool_id = tp.tool_sub_id');
		$this->db->join('tool t','t.tool_id = tp.tool_sub_id');
		$this->db->where('tp.tool_main_id',$tool_id);
		$this->db->where('p.asset_id',$asset_id);
		$this->db->where('tp.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}

	#koushik
	public function get_sub_component_details($tool_id)
	{
		$this->db->select('tp.quantity,t.*');
		$this->db->from('tool_part tp');
		$this->db->join('tool t','t.tool_id = tp.tool_sub_id');
		$this->db->where('tp.tool_main_id',$tool_id);
		$this->db->where('tp.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}

	#kohsik
	public function get_top_tool_id($asset_id)
	{
		$this->db->select('p.tool_id');
		$this->db->from('part p');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->where('a.asset_id',$asset_id);
		$this->db->order_by('p.part_id ASC');
		$res = $this->db->get();
		$result = $res->row_array();
		return $result['tool_id'];
	}



	#koushik
	public function check_serial_number_availability($data)
	{
		$this->db->from('part p');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.serial_number', $data['serial_number']);
		$this->db->where('p.tool_id', $data['tool_id']);
		$this->db->where('t.country_id',$data['country_id']);
		if($data['tool_part_id']!='' || $data['tool_part_id']!=0)
		{
			$this->db->where_not_in('p.part_id', @$data['tool_part_id']);
		}
		$res = $this->db->get();
		return $res->num_rows();
	}
	// parent asset details
	public function parent_asset_details($asset_id)
	{
		$this->db->select('a.*,a.remarks as remarks,p.status as part_status,p.remarks as part_remarks,
			p.*,adoc.name as po_doc_name,adoc.asset_doc_id,t.*,a.remarks as asset_remarks');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','p.tool_id =t.tool_id ');
		$this->db->join('asset_documents adoc','adoc.asset_id = a.asset_id','left');
		
		$this->db->where('p.part_level_id',1);// only getting Lo items
		$this->db->where('a.asset_id',$asset_id);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function child_asset_details($asset_id)
	{
		$this->db->select('a.*,a.remarks as remarks,p.status as part_status,p.remarks as part_remarks,
			p.*,adoc.name as po_doc_name,adoc.asset_doc_id,t.*');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','p.tool_id =t.tool_id ');
		$this->db->join('asset_documents adoc','adoc.asset_id = a.asset_id','left');
		
		$this->db->where('p.part_level_id',2);// only getting Lo items
		$this->db->where('a.asset_id',$asset_id);
		$res = $this->db->get();
		
		return $res->result_array();
	}
	public function update_todate($asset_id)
	{
		$da= date('Y-m-d');
		$qry = "UPDATE asset_history SET to_date = '".$da."'
				WHERE asset_id ='".$asset_id."' AND to_date IS NULL ";
		$this->db->query($qry);

	}
	
	public function asset_details($searchParams)
	{
		
		$this->db->select('b.*');
		$this->db->from('bank b');
		if($searchParams['bank_name']!='')
			$this->db->like('b.bank_name',$searchParams['bank_name']);
		$this->db->order_by('b.bank_id','DESC');
		$res = $this->db->get();
		return $res->result_array();
	}


	public function asset_upload_details($upload_id)
	{
		$this->db->select('ua.*,uap.*');
		$this->db->from('upload u');
		$this->db->join('upload_asset ua','u.upload_id = ua.upload_id');
		$this->db->join('upload_asset_part uap','uap.upload_asset_id = ua.upload_asset_id');
		$this->db->where('u.upload_id',$upload_id);
		//$this->db->limit('1');
		$res = $this->db->get();
		return $res->result_array();
	}
	public function missed_asset_results($current_offset, $per_page, $upload_id)
	{
		
		$this->db->select('ua.*,uap.*');
		$this->db->from('upload_asset ua');
		$this->db->join('upload_asset_part uap','ua.upload_asset_id = uap.upload_asset_id');
		$this->db->limit($per_page, $current_offset);
		$this->db->where('ua.upload_id',$upload_id);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function missed_asset_total_num_rows($upload_id)
	{
		$this->db->select('ua.*,uap.*');
		$this->db->from('upload_asset ua');
		$this->db->join('upload_asset_part uap','ua.upload_asset_id = uap.upload_asset_id');
		$this->db->where('ua.upload_id',$upload_id);		
		$res = $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->num_rows();
	}

	// Mahesh 24th June 2017 10:49 PM
	public function get_asset_info($asset_id)
	{
		if($asset_id!='')
		{
			$this->db->select('a.asset_number,t.part_number,t.part_description,p.serial_number');
			$this->db->from('asset a');
			$this->db->join('part p','p.asset_id = a.asset_id');
			$this->db->join('tool t','t.tool_id = p.tool_id');
			$this->db->where('p.part_level_id',1); // L0 Item
			$this->db->where('a.asset_id',$asset_id);
			$res = $this->db->get();
			if($res->num_rows()>0)
			{
				$row = $res->row_array();
				return $row;
			}
		}
	}

	// Mahesh 25th June 2017 11:30 AM
	public function get_asset_part_details($asset_number)
	{
		if($asset_number!='')
		{
			$this->db->select('a.asset_number,t.part_number,t.part_description,p.serial_number,p.quantity,pl.name as part_level');
			$this->db->from('asset a');
			$this->db->join('part p','p.asset_id = a.asset_id');
			$this->db->join('tool t','t.tool_id = p.tool_id');
			$this->db->join('part_level pl','p.part_level_id = pl.part_level_id');
			$this->db->where('a.asset_number',$asset_number);
			$this->db->order_by('p.part_level_id');
			$res = $this->db->get();
			if($res->num_rows()>0)
			{
				$row = $res->result_array();
				return $row;
			}
		}
	}

	public function get_asset_item_list($asset_id)
	{
		$this->db->select('p.serial_number, t.part_number, t.part_description, p.quantity, ac.name as asset_condition_name, pl.name as part_level_name, t.tool_code');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = p.status');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->where('p.part_status',1);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->order_by('p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_cr_status($asset_id)
	{
		$this->db->select('ec.ec_id, ec.cal_status_id');
		$this->db->from('egm_calibration ec');
		$this->db->where('ec.asset_id',$asset_id);
		$this->db->where_in('ec.cal_status_id',array(1,2));
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_cal_status_list($match_arr)
	{
		$this->db->select('*');
		$this->db->from('egm_cal_status');
		if(count($match_arr)>0)
		{
			$this->db->where_in('cal_status_id',$match_arr);
		}
		$res = $this->db->get();
		return $res->result_array();
	}
}	