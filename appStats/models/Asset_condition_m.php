<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// created by Srilekha on 29th july 2017 07:26PM
class Asset_condition_m extends CI_Model 
{
	/*Asset Results
	Author: Srilekha
	Time:07:27PM Date:29-07-2017*/
	public function asset_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$status=array(1,2,5,9,11);
		$this->db->select('a.asset_id,a.asset_number,l.name as country_name,t.part_number,t.part_description,m.name as modality_name,tt.name as tool_type_name,as.name as asset_status,p.serial_number');
		$this->db->from('asset a');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		$this->db->join('asset_status s','s.asset_status_id =a.status');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($task_access == 1)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whid']=='')
				{
					$this->db->where_in('a.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
		}
		$this->db->where('a.approval_status',0);
		$this->db->where_in('a.status',$status);
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('t.tool_id ASC,a.asset_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	/*Asset Results
	Author: Srilekha
	Time:07:27PM Date:29-07-2017*/
	public function asset_total_num_rows($searchParams,$task_access)
	{
		//$wh_id = $this->session->userdata('swh_id');
		$status=array(1,2,5,9,11);
		$this->db->select('a.asset_id,a.asset_number,l.name as country_name,t.part_number,t.part_description,m.name as modality_name,tt.name as tool_type_name,as.name as asset_status');
		$this->db->from('asset a');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		$this->db->join('asset_status s','s.asset_status_id =a.status');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		/*if($searchParams['whid']!='')
			$this->db->where('a.wh_id',$searchParams['whid']);*/
		if($task_access == 1)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whid']=='')
				{
					$this->db->where_in('a.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
		}
		$this->db->where('a.approval_status',0);
		//$this->db->where('a.wh_id',$wh_id);
		$this->db->where_in('a.status',$status);
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('t.tool_id ASC,a.asset_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	
	/*Get Tool Information
	Author: Srilekha
	Time:08:02PM Date:29-07-2017*/
	public function get_tool_related_info($tool_id)
	{
		$this->db->select('t.*,al.name as asset_level_name,
						  tt.name as tool_type_name,s.name as supplier_name,m.name as modality_name');
		$this->db->from('tool t');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('part_level pl','pl.part_level_id = t.part_level_id');
		$this->db->join('supplier s','s.supplier_id = t.supplier_id');
		$this->db->join('asset_type at','at.asset_type_id = t.asset_type_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->where('t.tool_id',$tool_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	
	/*Get asset Part details
	Author:Srilekha
	Time:08:18PM Date:29-07-2017*/ 
	public function get_asset_part_details($asset_id)
	{
		//$wh_id = $this->session->userdata('swh_id');
		$status=array(1,2,5,9,11);
		$this->db->select('a.asset_id,a.asset_number,p.part_id,
						  pl.name as part_level_name,t.part_number,
						  t.part_description,p.quantity,p.status,t.tool_id, a.status as asset_status,p.serial_number,p.remarks as p_remarks,p.status as part_status');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		//$this->db->where('a.wh_id',$wh_id);
		$this->db->where('p.part_status',1);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('a.approval_status',0); // not for approval
		$this->db->where_in('a.status',$status);
		$this->db->order_by('p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}
}