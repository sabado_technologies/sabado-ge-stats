<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modality_model extends CI_Model 
{
	//starting of modality crud methods//
	public function modality_results($current_offset, $per_page, $searchParams)
	{
		
		$this->db->select('m.*');
		$this->db->from('modality m');
		if($searchParams['modality_name']!='')
			$this->db->like('m.name',$searchParams['modality_name']);
		if($searchParams['modality_code']!='')
			$this->db->like('m.modality_code',$searchParams['modality_code']);
		$this->db->limit($per_page, $current_offset);
		$this->db->order_by('m.modality_id','DESC');
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function modality_total_num_rows($searchParams)
	{
		$this->db->select('m.*');
		$this->db->from('modality m');
		if($searchParams['modality_name']!='')
			$this->db->like('m.name',$searchParams['modality_name']);
		if($searchParams['modality_code']!='')
			$this->db->like('m.modality_code',$searchParams['modality_code']);
		$res = $this->db->get();
		return $res->num_rows();
	}
	//Uniqueness of Modality name //
    public function is_modalitynameExist($name,$modality_id)
    {       
        $this->db->select();
        $this->db->from('modality m');
        $this->db->where('m.name',$name);
        $this->db->where_not_in('m.modality_id',$modality_id);
        $query = $this->db->get();
        return ($query->num_rows()>0)?1:0;
    }
    //Uniqueness of Modality Code //
    public function is_modalitycodeExist($code,$modality_id)
    {       
        $this->db->select();
        $this->db->from('modality m');
        $this->db->where('m.modality_code',$code);
        $this->db->where_not_in('m.modality_id',$modality_id);
        $query = $this->db->get();
        return ($query->num_rows()>0)?1:0;
    }
}	