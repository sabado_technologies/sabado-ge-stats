<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Designation_m extends CI_Model 
{
	
	public function designation_total_num_rows($searchParams)
	{
		
		$this->db->select('rd.*,d.name as designation_name,r.name as role_name' );
		$this->db->from('role_designation rd');
		$this->db->join('role r','r.role_id = rd.role_id');
		$this->db->join('designation d','rd.designation_id = d.designation_id');
		if($searchParams['designame']!='')
			$this->db->like('d.name',$searchParams['designame']);
		if($searchParams['roleid']!='')
			$this->db->where('r.role_id',$searchParams['roleid']);
		$this->db->where('rd.status', 1);
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function designation_results($current_offset, $per_page, $searchParams)
	{
		
		$this->db->select('rd.*,d.name as designation_name,r.name as role_name,d.status' );
		$this->db->from('role_designation rd');
		$this->db->join('role r','r.role_id = rd.role_id');
		$this->db->join('designation d','rd.designation_id = d.designation_id');
		if($searchParams['designame']!='')
			$this->db->like('d.name',$searchParams['designame']);
		if($searchParams['roleid']!='')
			$this->db->like('r.role_id',$searchParams['roleid']);
		$this->db->where('rd.status', 1);
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function get_designation_details($designation_id)
	{
		$this->db->select('rd.*,d.name as designation_name,r.name as role_name,d.status' );
		$this->db->from('role_designation rd');
		$this->db->join('role r','r.role_id = rd.role_id');
		$this->db->join('designation d','rd.designation_id = d.designation_id');
		$this->db->where('d.designation_id',$designation_id);
		$this->db->where('rd.status', 1);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function insert_update($role_id,$designation_id)
	{
		$qry = "INSERT INTO role_designation(role_id, designation_id, status) 
                    VALUES (".$role_id.",".$designation_id.",'1')  
                    ON DUPLICATE KEY UPDATE status = VALUES(status);";
        $this->db->query($qry);
	}
	/*Author= aswini 
	 date = 14/06/17, modified by gowri 19th june 5:20pm
	 method to check unique validation for designation name
	*/
   public function check_designation_name_availability($data)
	{
		$this->db->from('designation d');
		$this->db->where('d.name', $data['name']);
		if($data['designation_id']!='' || $data['designation_id']!=0)
		$this->db->where_not_in('d.designation_id', $data['designation_id']);
		$res = $this->db->get();
		return $res->num_rows();	
	}
}