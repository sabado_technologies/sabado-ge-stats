<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// created by Srilekha on 2nd August 2017 05:49PM
class Defective_asset_m extends CI_Model
{
	/*Defective Asset No of Rows.
    Author:Srilekha
    Time:05.50PM Date:2-08-2017*/
	public function defective_asset_total_num_rows($searchParams,$task_access)
	{
		$status = array(1,2);
		$this->db->select('t.part_number,a.asset_id,
						   a.asset_number,wh.name as warehouse,wh.wh_code,
						   da.defective_asset_id,
						   t.part_description,p.part_level_id,a.wh_id,
						   l.name as country_name');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id=da.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if(@$searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if(@$searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if(@$searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if(@$task_access == 1)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if(@$task_access == 2)
		{
			if(@$searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
		}
		$this->db->where('da.type',1);
		$this->db->where('a.approval_status',1);
		$this->db->where('da.request_handled_type',0);
		$this->db->where('p.part_level_id',1);
		$this->db->where_in('da.status',$status);
		$this->db->order_by('da.defective_asset_id DESC');
		$res=$this->db->get();
		return $res->num_rows();
	}
	/*defective Asset results.
    Author:Srilekha
    Time:05.51PM Date:2-08-2017*/
	public function defective_asset_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$status = array(1,2);
		$this->db->select('t.part_number,a.asset_id,
						   a.asset_number,wh.name as warehouse,wh.wh_code,
						   da.defective_asset_id,
						   t.part_description,p.part_level_id,a.wh_id,t.cal_type_id,
						   l.name as country_name,p.serial_number,l.region_id');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id=da.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if(@$searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if(@$searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if(@$searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if(@$task_access == 1)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if(@$task_access == 2)
		{
			if(@$searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
		}
		$this->db->where('da.type',1);
		$this->db->where('a.approval_status',1);
		$this->db->where('da.request_handled_type',0);
		$this->db->where('p.part_level_id',1);
		$this->db->where_in('da.status',$status);
		$this->db->order_by('da.defective_asset_id DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_asset_part_list($defective_asset_id,$subComponent=0)
	{
		$this->db->select('t.part_number,t.part_description,
						   p.serial_number,p.quantity,pl.name as part_level,
						   ac.name as asset_condition,dah.remarks,
						   dah.remarks as part_remarks,t.tool_code,
						   ac.name as asset_condition_name');
		$this->db->from('defective_asset_health dah');
		$this->db->join('part p','p.part_id=dah.part_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id=dah.asset_condition_id');
		$this->db->join('part_level pl','pl.part_level_id=p.part_level_id');
		$this->db->where('dah.defective_asset_id',$defective_asset_id);
		$this->db->where('p.part_status',1);
		if($subComponent==1)
			$this->db->where('p.part_level_id',2);
		$res=$this->db->get();
		return $res->result_array();
	}
	
	public function asset_details_view($asset_id,$tool_id,$defective_asset_id)
	{
		$this->db->select('t.part_description,t.part_number,t.tool_code,
			p.serial_number,ac.name as asset_condition_name,
			p.remarks as part_remarks,a.*,t.kit as kit,m.name as modality_name,
			tt.name as tool_type_name,wh.wh_code as warehouse_code,
			a.sub_inventory as sub_inventory,at.name as asset_type,
			s.name as supplier_name,a.remarks as asset_remarks,ct.cal_type_id,
			ct.name as calibration,cs.name as cs_name,a.cal_due_date as due_date,
			t.manufacturer,a.po_number as po,a.earpe_number as earpe,
			a.date_of_use as date_of_used,ad.name as document,
			t.part_number as part_num,t.tool_code as toolcode,
			t.part_description as description,p.serial_number as serialnumber,
			p.remarks as remark,p.status as statuss,
			al.name as asset_level_name,p.quantity');
		$this->db->from('asset a');
		$this->db->join('defective_asset da','da.asset_id = a.asset_id');		
		$this->db->join('part p','a.asset_id = p.asset_id');
		$this->db->join('tool t','p.tool_id =t.tool_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id =t.tool_type_id');
		$this->db->join('asset_type at','at.asset_type_id =t.asset_type_id');
		$this->db->join('asset_document ad','ad.asset_id =a.asset_id','left');		
		$this->db->join('modality m','m.modality_id =a.modality_id ');
		$this->db->join('supplier cs','cs.supplier_id = a.cal_supplier_id','left');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('supplier s','s.supplier_id=t.supplier_id');
		$this->db->join('warehouse wh','a.wh_id = wh.wh_id');
		$this->db->join('asset_status as','as.asset_status_id =a.status ');
		$this->db->join('part_level pl','pl.part_level_id= p.part_level_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = p.status');
		$this->db->where('p.asset_id',$asset_id);
		$this->db->where('p.tool_id',$tool_id);
		$this->db->where('da.defective_asset_id',$defective_asset_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function missed_asset_total_num_rows($searchParams,$task_access)
	{
		$status = array(1,3);
		$this->db->select('t.part_number,a.asset_id,
						   a.asset_number,wh.name as warehouse,wh.wh_code,
						   da.defective_asset_id,
						   t.part_description,p.part_level_id');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id=da.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id = a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if(@$searchParams['ma_tool_no']!='')
			$this->db->like('t.part_number',$searchParams['ma_tool_no']);
		if(@$searchParams['ma_tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['ma_tool_desc']);
		if(@$searchParams['ma_asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['ma_asset_number']);
		if($task_access == 1 )
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['ma_wh_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['ma_wh_id']);
			}

			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('a.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['ma_wh_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['ma_wh_id']);
			}

		}
		$this->db->where('da.type',2);
		$this->db->where('a.approval_status',1);
		$this->db->where('p.part_level_id',1);
		$this->db->where_in('da.status',$status);
		$this->db->order_by('da.defective_asset_id DESC');
		$res=$this->db->get();
		return $res->num_rows();
	}
	/*defective Asset results.
    Author:Srilekha
    Time:05.51PM Date:2-08-2017*/
	public function missed_asset_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$status = array(1,3);
		$this->db->select('t.part_number,a.asset_id,
						   a.asset_number,wh.name as warehouse,wh.wh_code,
						   da.defective_asset_id,
						   t.part_description,p.part_level_id,l.name as country_name,p.serial_number');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id=da.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id = a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if(@$searchParams['ma_tool_no']!='')
			$this->db->like('t.part_number',$searchParams['ma_tool_no']);
		if(@$searchParams['ma_tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['ma_tool_desc']);
		if(@$searchParams['ma_asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['ma_asset_number']);

		if($task_access == 1 )
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['ma_wh_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['ma_wh_id']);
			}

			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('a.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['ma_wh_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['ma_wh_id']);
			}

		}
		$this->db->where('da.type',2);
		$this->db->where('a.approval_status',1);
		$this->db->where('p.part_level_id',1);
		$this->db->where_in('da.status',$status);
		$this->db->order_by('da.defective_asset_id DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
}
