<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class XL_downloads_m extends CI_Model 
{
	public function downloadOpenOrders($searchParams,$task_access)
	{
		$this->db->select('to.tool_order_id,to.order_number,concat(u.sso_id,"- (",u.name," )") as sso,odt.name as order_type,
			to.deploy_date as request_date,to.request_date as need_date,to.return_date as return_date,cst.name as name,
			st.name as service_type,to.siebel_number as siebel_sr_number,to.fe_check,oad.system_id,oad.site_id,to.wh_id,
			oad.address1,oad.address2,oad.address3,oad.address4,oad.pin_code,to.current_stage_id,to.order_delivery_type_id,l.name as country_name');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		$this->db->join('service_type st','st.service_type_id = to.service_type_id');
		$this->db->join('order_address oad','oad.tool_order_id = to.tool_order_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		$this->db->where('to.current_stage_id!=',10);
		if($searchParams['search_fe_sso_id']!='')
		{
			$this->db->where('to.sso_id',$searchParams['search_fe_sso_id']);
		}
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access ==1 )
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['ro_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['ro_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));	
			}
			
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ro_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['ro_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('to.order_type',2);
		$this->db->order_by('to.country_id,to.tool_order_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function downloadClosedOrders($searchParams,$task_access)
	{
		$this->db->select('to.tool_order_id,to.order_number,concat(u.sso_id,"- (",u.name," )") as sso,odt.name as order_type,
			to.deploy_date as request_date,to.request_date as need_date,to.return_date as return_date,cst.name as name,
			st.name as service_type,to.siebel_number as siebel_sr_number,to.fe_check,oad.system_id,oad.site_id,to.wh_id,
			oad.address1,oad.address2,oad.address3,oad.address4,oad.pin_code,to.current_stage_id,to.order_delivery_type_id,l.name as country_name,
			DATE(to.modified_time) as closed_date,DATE(oddl.created_time) as shipped_date,to.status as final_status
			');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		$this->db->join('service_type st','st.service_type_id = to.service_type_id');
		$this->db->join('order_address oad','oad.tool_order_id = to.tool_order_id');
		$this->db->join('order_delivery oddl','oddl.tool_order_id = to.tool_order_id','left');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);		
		if($searchParams['status']!='')
			$this->db->where('to.status',$searchParams['status']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));	
		if($searchParams['closed_fe_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['closed_fe_sso_id']);		
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access ==1 )
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['co_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['co_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));	
			}			
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['co_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['co_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('to.current_stage_id',10);
		$this->db->where('to.order_type',2);
		$this->db->order_by('to.country_id,to.tool_order_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function download_tool($searchParams,$task_access)
	{
		$this->db->select('t.*,tl.name as tool_level_name,tt.name as tool_type_name,asl.name as asset_level_name,l.name as country,s.supplier_code as supplier,s.name as cal_supplier_id');
		$this->db->from('tool t');
		$this->db->join('part_level tl','tl.part_level_id = t.part_level_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('location l','l.location_id=t.country_id');
		$this->db->join('supplier s','s.supplier_id=t.supplier_id');
		$this->db->join('asset_level asl','asl.asset_level_id=t.asset_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id=t.tool_type_id');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['tool_code']!='')
			$this->db->like('t.tool_code',$searchParams['tool_code']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['part_level_id']!='')
			$this->db->where('tl.part_level_id',$searchParams['part_level_id']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('t.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('t.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('t.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('t.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
		}
		$this->db->order_by('t.tool_id ASC, t.country_id ASC');
       	$res = $this->db->get();
        return $res->result_array();
	}
	public function get_tool_parts($tool_id)
	{
		$this->db->select('t.*,tl.name as tool_level_name,tt.name as tool_type_name,asl.name as asset_level_name,l.name as country,s.supplier_code as supplier,s.name as cal_supplier_id,t2.tool_id,t2.part_number,t2.part_description,tp.quantity,t2.tool_code,t2.status');
		$this->db->from('tool t');
		$this->db->join('part_level tl','tl.part_level_id = t.part_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id=t.tool_type_id');
		$this->db->join('supplier s','s.supplier_id=t.supplier_id');
		$this->db->join('asset_level asl','asl.asset_level_id=t.asset_level_id');
		$this->db->join('location l','l.location_id=t.country_id');
		$this->db->join('tool_part tp','t.tool_id = tp.tool_main_id');
		$this->db->join('tool t2','t2.tool_id = tp.tool_sub_id');
		$this->db->where('tp.status',1);
		$this->db->where('tp.tool_main_id',$tool_id);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function download_user($searchParams,$task_access)
	{
		$this->db->select('u.*,b.name as branch_name,r.name as role,d.name as designation_name,l.name as location_name,concat(wh.wh_code, "-(" ,wh.name,")") as wh,l1.name as country,GROUP_CONCAT(concat(wh1.wh_code, "-(" ,wh1.name,")") SEPARATOR ",<br>") as wh_arr');
		$this->db->from('user u');
		$this->db->join('user_wh uwh','uwh.sso_id = u.sso_id AND uwh.status = 1','LEFT');
		$this->db->join('warehouse wh1','wh1.wh_id = uwh.wh_id','LEFT');
		$this->db->join('role r','r.role_id = u.role_id','left');
		$this->db->join('designation d','d.designation_id = u.designation_id','left');
		$this->db->join('branch b','b.branch_id = u.branch_id','left');
		$this->db->join('location l','l.location_id = u.fe_position','left');
		$this->db->join('location l1','l1.location_id=u.country_id','left');
		$this->db->join('warehouse wh','wh.wh_id = u.wh_id','left');
		if($searchParams['ssonum']!='')
			$this->db->like('u.sso_id',$searchParams['ssonum']);
		if($searchParams['username']!='')
			$this->db->like('u.name',$searchParams['username']);
		if($searchParams['branchnum']!='')
			$this->db->where('u.branch_id',$searchParams['branchnum']);
		if($searchParams['designationnum']!='')
			$this->db->where('u.designation_id',$searchParams['designationnum']);
		if($searchParams['roleid']!='')
			$this->db->where('u.role_id',$searchParams['roleid']);
		if($searchParams['fe_position']!='')
			$this->db->like('l.name',$searchParams['fe_position']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('u.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('u.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('u.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('u.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		if($searchParams['whid']!='')
		{
			$where = '((`uwh`.`wh_id` = '.$searchParams['whid'].') OR (`u`.`wh_id` = '.$searchParams['whid'].'))';
			$this->db->where($where);
		}
		$this->db->order_by('u.sso_id','ASC');
		$this->db->group_by('u.sso_id');
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_install_base($searchParams,$task_access)
	{
		$this->db->select('c.*,ib.*,cs.*,ib.status as ib_status,l2.name as country_name,m.name as modality_name');
		$this->db->from('customer c');
		$this->db->join('customer_site cs','cs.customer_id = c.customer_id');
		$this->db->join('location l','l.location_id = cs.location_id');
		$this->db->join('location l1','l1.location_id=c.country_id');
		$this->db->join('location l2','l2.location_id=c.country_id');
		$this->db->join('install_base ib','ib.customer_site_id = cs.customer_site_id');
		$this->db->join('modality m','m.modality_id=ib.modality_id');
		if($searchParams['customer_name']!='')
			$this->db->like('c.name',$searchParams['customer_name']);
		if($searchParams['customer_number']!='')
			$this->db->like('c.customer_number',$searchParams['customer_number']);
		if($searchParams['site_id']!='')
			$this->db->like('cs.site_id',$searchParams['site_id']);
		if($searchParams['system_id']!='')
			$this->db->like('ib.system_id',$searchParams['system_id']);
		if($searchParams['asset_number']!='')
			$this->db->like('ib.asset_number',$searchParams['asset_number']);
		if($searchParams['status']!='')
			$this->db->like('ib.status',$searchParams['status']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('c.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('c.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('c.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('c.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('c.country_id ASC','ib.install_base_id ASC');
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_install_base_xlsx($searchParams,$task_access)
	{
		$this->db->select('ib.asset_number,ib.system_id,m.modality_code as modality_name, ib.product_description,ib.model_type,c.customer_number,c.name,cs.site_id,cs.address1,cs.address2,cs.address3,cs.address4,cs.zip_code,cs.address3 as city,c.service_region,l2.name as country_name');
		$this->db->from('customer c');
		$this->db->join('customer_site cs','cs.customer_id = c.customer_id');
		$this->db->join('location l','l.location_id = cs.location_id');
		$this->db->join('location l1','l1.location_id=c.country_id');
		$this->db->join('location l2','l2.location_id=c.country_id');
		$this->db->join('install_base ib','ib.customer_site_id = cs.customer_site_id');
		$this->db->join('modality m','m.modality_id=ib.modality_id');
		if($searchParams['customer_name']!='')
			$this->db->like('c.name',$searchParams['customer_name']);
		if($searchParams['customer_number']!='')
			$this->db->like('c.customer_number',$searchParams['customer_number']);
		if($searchParams['site_id']!='')
			$this->db->like('cs.site_id',$searchParams['site_id']);
		if($searchParams['system_id']!='')
			$this->db->like('ib.system_id',$searchParams['system_id']);
		if($searchParams['asset_number']!='')
			$this->db->like('ib.asset_number',$searchParams['asset_number']);
		if($searchParams['status']!='')
			$this->db->like('ib.status',$searchParams['status']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('ib.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('ib.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('ib.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('ib.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_supplier($searchParams,$task_access)
	{
		$status_supplier = array(1,2);
		$this->db->select('s.*,GROUP_CONCAT(m.name SEPARATOR",") as modality_name,l.name as country_name');
		$this->db->from('supplier s');
		$this->db->join('modality_supplier ms','ms.supplier_id=s.supplier_id AND ms.status=1','left');
		$this->db->join('modality m','ms.modality_id=m.modality_id AND m.status=1','left');
		$this->db->join('location l','l.location_id=s.country_id');
		if($searchParams['name']!='')
			$this->db->like('s.name',$searchParams['name']);		
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['calibration']!='')
			$this->db->where('s.calibration',$searchParams['calibration']);		
		if($searchParams['sup_type_id']!='')
			$this->db->where('s.sup_type_id',$searchParams['sup_type_id']);	
       if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('s.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('s.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('s.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('s.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}	
		$this->db->where_in('s.status',$status_supplier);
		$this->db->group_by('s.supplier_id');
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_branch($searchParams,$task_access)
	{
		$this->db->select('w.*,l.name as location_name,l2.name as country_name');
		$this->db->from('warehouse w');
		$this->db->join('location l','l.location_id=w.location_id');
		$this->db->join('location l2','l2.location_id=w.country_id');
		if($searchParams['name']!='')
			$this->db->like('w.name',$searchParams['name']);
		if($searchParams['wh_code']!='')
			$this->db->like('w.wh_code',$searchParams['wh_code']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('w.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('w.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('w.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('w.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		//$this->db->where_in('s.status',$status_supplier);
		$this->db->order_by('w.country_id ASC');
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_branch_office($searchParams,$task_access)
	{
		$this->db->select('b.*,l.name as location_name,b.country_id as country_name,l2.name as country_name');
		$this->db->from('branch b');
		$this->db->join('location l','l.location_id=b.location_id');
		$this->db->join('location l2','l2.location_id=b.country_id');
		if($searchParams['name']!='')
			$this->db->like('b.name',$searchParams['name']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('b.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('b.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('b.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('b.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('b.country_id ASC');
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_modality($searchParams,$task_access)
	{
		$this->db->select('m.*');
		$this->db->from('modality m');
		if($searchParams['modality_name']!='')
			$this->db->like('m.name',$searchParams['modality_name']);
		if($searchParams['modality_code']!='')
			$this->db->like('m.modality_code',$searchParams['modality_code']);
		$this->db->order_by('m.modality_id','DESC');
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_eq_model($searchParams,$task_access)
	{
		$this->db->select('m.*,m.name as eq_model_name');
		$this->db->from('equipment_model m');
		if($searchParams['eq_model_name']!='')
			$this->db->like('m.name',$searchParams['eq_model_name']);
		$this->db->order_by('m.eq_model_id','DESC');
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_country($searchParams,$task_access)
	{
		$this->db->select('l.*,c.name as currency_name');
		$this->db->from('location l');
		$this->db->join('territory_level tl ','tl.level_id = l.level_id');
		$this->db->join('currency c','c.location_id = l.location_id','LEFT');
		if(@$searchParams['country_name']!='')
			$this->db->like('l.name',$searchParams['country_name']);
		if(@$searchParams['currency_name']!='')
			$this->db->like('c.name',$searchParams['currency_name']);
		$this->db->where('tl.level_id',2);	
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_zone($searchParams,$task_access)
	{
		$this->db->select('l.location_id, l.name, l1.name as country_name,l.status');
			$this->db->from('location l');
			$this->db->join('location l1','l1.location_id = l.parent_id','left');
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',3);
			if(@$searchParams['zone_name']!='')
				$this->db->like('l.name',$searchParams['zone_name']);
			if(@$task_access == 1 || $task_access == 2)
			{
				$this->db->where('l.parent_id',$this->session->userdata('s_country_id'));
			}
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l.parent_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l.parent_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l.parent_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}	
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_state($searchParams,$task_access)
	{
		$this->db->select('l.location_id, l.name, concat(l1.name, "," ,l2.name) as zone_name,l.short_name,l.status');
		$this->db->from('location l');//state
		$this->db->join('location l1','l1.location_id = l.parent_id','left');//zone
		$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//country
		$this->db->join('territory_level tl ','tl.level_id = l.level_id');
		$this->db->where('tl.level_id',4);
		if(@$searchParams['state_name']!='')
			$this->db->like('l.name',$searchParams['state_name']);
		if(@$searchParams['zone_id']!='')
			{
				$this->db->where('l.parent_id',$searchParams['zone_id']);
			}
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l2.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l2.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('l2.location_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('l2.location_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_city($searchParams,$task_access)
	{
		$this->db->select('l.location_id, l.name, l1.name as state_name,l2.name as zone_name, l3.name as country_name,l.status');
		$this->db->from('location l');//city
		$this->db->join('location l1','l1.location_id = l.parent_id','left');//state
		$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//zone
		$this->db->join('location l3','l3.location_id = l2.parent_id','LEFT');//country
		$this->db->join('territory_level tl ','tl.level_id = l.level_id');
		$this->db->where('tl.level_id',5);
		if(@$searchParams['city_name']!='')
			$this->db->like('l.name',$searchParams['city_name']);
		if(@$searchParams['state_id']!='')
		{
			$this->db->where('l1.location_id',$searchParams['state_id']);
		}
		if(@$searchParams['zone_id']!='')
		{
			$this->db->where('l2.location_id',$searchParams['zone_id']);
		}
		if(@$searchParams['country_id']!='')
		{
			$this->db->where('l3.location_id',$searchParams['country_id']);
		}
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l3.location_id',$this->session->userdata('s_country_id'));
		}		
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l3.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('l3.location_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('l3.location_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_area($searchParams,$task_access)
	{
		$this->db->select('l.location_id, l.name, l1.name as city_name,l2.name as state_name,l3.name as zone_name, l4.name as country_name, l.status');
		$this->db->from('location l');//area
		$this->db->join('location l1','l1.location_id = l.parent_id','left');//city
		$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//state
		$this->db->join('location l3','l3.location_id = l2.parent_id','LEFT');//zone
		$this->db->join('location l4','l4.location_id = l3.parent_id','LEFT');//country
		$this->db->join('territory_level tl ','tl.level_id = l.level_id');
		$this->db->where('tl.level_id',6);
		if(@$searchParams['area_name']!='')
			$this->db->like('l.name',$searchParams['area_name']);
		if(@$searchParams['city_id']!='')
		{
			$this->db->where('l1.location_id',$searchParams['city_id']);
		}
		if(@$searchParams['state_id']!='')
		{
			$this->db->where('l2.location_id',$searchParams['state_id']);
		}
		if(@$searchParams['zone_id']!='')
		{
			$this->db->where('l3.location_id',$searchParams['zone_id']);
		}
		if(@$searchParams['country_id']!='')
		{
			$this->db->where('l4.location_id',$searchParams['country_id']);
		}
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l4.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l4.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('l4.location_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('l4.location_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_document_type($searchParams,$task_access)
	{
		$this->db->select('d.*');
		$this->db->from('document_type d');
		if($searchParams['name']!='')
			$this->db->like('d.name',$searchParams['name']);
		if($searchParams['workflow_type']!='')
			$this->db->like('d.workflow_type',$searchParams['workflow_type']);
		$this->db->order_by('d.workflow_type ASC');
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_designation($searchParams,$task_access)
	{
		$this->db->select('rd.*,d.name as designation_name,r.name as role_name' );
		$this->db->from('role_designation rd');
		$this->db->join('role r','r.role_id = rd.role_id');
		$this->db->join('designation d','rd.designation_id = d.designation_id');
		if($searchParams['designame']!='')
			$this->db->like('d.name',$searchParams['designame']);
		if($searchParams['roleid']!='')
			$this->db->where('r.role_id',$searchParams['roleid']);
		$this->db->where('rd.status', 1);
       	$res = $this->db->get();
        return $res->result_array();
	}

	public function download_gps($searchParams,$task_access)
    {
        $this->db->select('gps.*,l.name as country_name');
        $this->db->from('gps_tool gps');
        $this->db->join('location l','l.location_id=gps.country_id');
        if($searchParams['gps_name']!='')
            $this->db->like('gps.name',$searchParams['gps_name']);
        if($searchParams['gps_uid']!='')
            $this->db->like('gps.uid_number',$searchParams['gps_uid']);
        if($task_access == 1 || $task_access == 2)
        {
            $this->db->where('gps.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('gps.country_id',$this->session->userdata('header_country_id')); 
            }
            else
            {
                if($searchParams['gps_country']!='')
                {
                    $this->db->where('gps.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('gps.country_id',$this->session->userdata('countriesIndexedArray'));  
                }
            }
        }
        $this->db->order_by('gps_tool_id','ASC');
        $res = $this->db->get();
        return $res->result_array();
    }

    public function receive_order_results($searchParams,$task_access)
	{
		$this->db->select('
			to.tool_order_id,to.order_number,concat(u.sso_id,"- (",u.name," )") as sso,odt.name as order_type,
			to.deploy_date as request_date,to.request_date as need_date,to.return_date as return_date,cst.name as name,
			st.name as service_type,to.siebel_number as siebel_sr_number,to.fe_check,oad.system_id,oad.site_id,to.wh_id,
			oad.address1,oad.address2,oad.address3,oad.address4,oad.pin_code,to.current_stage_id,to.order_delivery_type_id,l.name as country_name,
			,sto.stock_transfer_id');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id','LEFT');
		$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','LEFT');
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','to.country_id = l.location_id');
		$this->db->join('order_address oad','oad.tool_order_id = to.tool_order_id');
		$this->db->join('service_type st','st.service_type_id = to.service_type_id');

		$search_query = '';
		if(@$searchParams['order_number']!='')
		{
			$search_query.='to.order_number = "'.$searchParams['order_number'].'" AND ';
		}
		if(@$searchParams['order_delivery_type_id']!='')
		{
			$search_query.='to.order_delivery_type_id = '.$searchParams['order_delivery_type_id'].' AND ';
		}
		if(@$searchParams['deploy_date']!='')
		{
			$search_query.='to.deploy_date >= "'.format_date($searchParams['deploy_date']).'" AND ';
		}
		if(@$searchParams['return_date']!='')
		{
			$search_query.='to.return_date <= "'.format_date($searchParams['return_date']).'" AND ';
		}
		if($searchParams['rec_sso_id']!='')
		{
			$search_query.='to.sso_id = '.$searchParams['rec_sso_id'].' AND ';
		}

		if($task_access == 1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$search_query.='to.country_id ='.$this->session->userdata('header_country_id').' AND ';
				}
				else
				{
					if($searchParams['rec_country_id']!='')
					{
						$search_query.= 'to.country_id ='.$this->session->userdata('rec_country_id').' AND ';
					}
					else
					{
						$country_arr = $this->session->userdata('countriesIndexedArray');
						$country_val = implode(',', $country_arr);
						$search_query.='to.country_id IN ('.$country_val.') AND ';
					}
				}	
			}
			$search_query.='to.sso_id ='.$this->session->userdata('sso_id').' AND ';
		}
		else if($task_access == 2)
		{
			$search_query.='to.country_id ='.$this->session->userdata('s_country_id').' AND ';
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$search_query.='to.country_id ='.$this->session->userdata('header_country_id').' AND ';
			}
			else
			{
				if($searchParams['rec_country_id']!='')
				{
					$search_query.= 'to.country_id ='.$this->session->userdata('rec_country_id').' AND ';
				}
				else
				{
					$country_arr = $this->session->userdata('countriesIndexedArray');
					$country_val = implode(',', $country_arr);
					$search_query.='to.country_id IN ('.$country_val.') AND ';
				}
			}
		}

		$str1 = '('.$search_query.' osh.current_stage_id = 6 AND to.current_stage_id = 6 AND to.status<2 AND to.order_type = 2 AND osh.rto_id IS NULL AND osh.end_time IS NULL)';
		$this->db->where($str1);
		$str2 = '('.$search_query.' to.current_stage_id =4 AND stn.current_stage_id =2 AND stn.fe_check = 1)';
		$this->db->or_where($str2);
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->result_array();
	}
	public function owned_order_results($searchParams,$task_access)
	{
		$this->db->select('
			to.tool_order_id,to.order_number,concat(u.sso_id,"- (",u.name," )") as sso,odt.name as order_type,
			to.deploy_date as request_date,to.request_date as need_date,to.return_date as return_date,cst.name as name,
			st.name as service_type,to.siebel_number as siebel_sr_number,to.fe_check,oad.system_id,oad.site_id,to.wh_id,
			oad.address1,oad.address2,oad.address3,oad.address4,oad.pin_code,to.current_stage_id,to.order_delivery_type_id,l.name as country_name
			');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('location l','l.location_id=to.country_id');
		$this->db->join('service_type st','st.service_type_id = to.service_type_id');
		$this->db->join('order_address oad','oad.tool_order_id = to.tool_order_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		$this->db->where('to.current_stage_id',7);			
		$this->db->where('to.order_type',2);
		if($searchParams['rt_sso_id']!='')
			$this->db->where('to.sso_id',($searchParams['rt_sso_id']));
		$this->db->where('to.order_type',2);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['rt_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['rt_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}

				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['rt_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['rt_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}		
		$this->db->order_by('to.country_id,to.tool_order_id');
		$res = $this->db->get();
		return $res->result_array();
	}
}