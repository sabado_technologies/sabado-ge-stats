<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools_inventory_m extends CI_Model 
{
	public function t_inventory_total_num_rows($searchParams,$task_access)
	{
		if(@$searchParams['zone_id']!='')
		{
			$wh_arr = get_wh_by_zone_tools_inventory(@$searchParams['zone_id'],$task_access);
			$wh_list = get_index_array_result($wh_arr,'wh_id');
		}
		$this->db->select('t.part_number,t.part_description,
						   a.asset_id,a.asset_number,a.availability_status,a.sub_inventory,
						   m.name as modality_name,as.name as asset_status,
						   wh.wh_code,wh.name as wh_name,a.approval_status,a.asset_id,
						   l.name as country_name');
		$this->db->from('asset a');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id','LEFT');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['created_date'])
		{
			$this->db->where('a.created_time >=',format_date($searchParams['created_date'])." 00:00:00");
			$this->db->where('a.created_time <=',format_date($searchParams['created_date'])." 23:59:59");
		}
		if($searchParams['user_id']!='')
		{
			$this->db->like('ap.sso_id',$searchParams['user_id']);
			$this->db->where('ap.to_date',NULL);
		}
		if($searchParams['asset_number']!='')
			$this->db->where('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['zone_id']!='')
			$this->db->where_in('a.wh_id',$wh_list);
		if($searchParams['warehouse']!='')
			$this->db->where('a.wh_id',$searchParams['warehouse']);
		if($searchParams['asset_status']!='')
			$this->db->where('a.status',$searchParams['asset_status']);
		if($searchParams['asset_type']!='')
			$this->db->where('a.availability_status',$searchParams['asset_type']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh.status',$status);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('a.asset_id ASC');
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function t_inventory_results($current_offset, $per_page, $searchParams,$task_access)
	{
		if(@$searchParams['zone_id']!='')
		{
			$wh_arr = get_wh_by_zone_tools_inventory(@$searchParams['zone_id'],$task_access);
			$wh_list = get_index_array_result($wh_arr,'wh_id');
		}
		$this->db->select('t.part_number,t.part_description,
						   a.asset_id,a.asset_number,a.availability_status,a.sub_inventory,
						   m.name as modality_name,as.name as asset_status,
						   wh.wh_code,wh.name as wh_name,a.approval_status,a.asset_id,
						   l.name as country_name');
		$this->db->from('asset a');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id','LEFT');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['created_date'])
		{
			$this->db->where('a.created_time >=',format_date($searchParams['created_date'])." 00:00:00");
			$this->db->where('a.created_time <=',format_date($searchParams['created_date'])." 23:59:59");
		}
		if($searchParams['user_id']!='')
		{
			$this->db->like('ap.sso_id',$searchParams['user_id']);
			$this->db->where('ap.to_date',NULL);
		}
		if($searchParams['asset_number']!='')
			$this->db->where('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['zone_id']!='')
			$this->db->where_in('a.wh_id',$wh_list);
		if($searchParams['warehouse']!='')
			$this->db->where('a.wh_id',$searchParams['warehouse']);
		if($searchParams['asset_status']!='')
			$this->db->where('a.status',$searchParams['asset_status']);
		if($searchParams['asset_type']!='')
			$this->db->where('a.availability_status',$searchParams['asset_type']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh.status',$status);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('a.asset_id ASC');
		$this->db->group_by('a.asset_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_asset_items($asset_id)
	{
		$this->db->select('t.part_number,t.part_description,pl.name as part_level_name,p.serial_number,p.status as p_status, p.quantity,p.remarks');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('p.part_status',1);
		$this->db->order_by('p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function download_t_inventory($searchParams,$task_access)
	{
		if(@$searchParams['zone_id']!='')
		{
			$wh_arr = get_wh_by_zone_tools_inventory(@$searchParams['zone_id'],$task_access);
			$wh_list = get_index_array_result($wh_arr,'wh_id');
		}
		$this->db->select('a.asset_id,
						   a.asset_number,
						   t.part_number,
						   t.part_description,
						   p.quantity,
						   p.serial_number,
						   p.status,
						   t.hsn_code,
						   t.gst_percent,
						   t.tool_code,
						   tt.name as tool_type_name,
						   m.name as modality_name,
						   t.model,
						   sup.name as supplier_name,
						   ct.name as cal_type_name,
						   cs.name as calibration_supplier,
						   t.length,
						   t.breadth,
						   t.height,
						   t.weight,
						   t.asset_type_id,
						   t.kit,
						   t.manufacturer,
						   wh.wh_code,wh.name as wh_name,
						   a.sub_inventory,
						   a.cost,
						   c.name as currency_name,
						   a.cost_in_inr,
						   a.earpe_number,
						   a.po_number,
						   a.date_of_use,
						   a.calibrated_date,
						   a.cal_due_date,
						   a.asset_id,
						   a.availability_status,
						   as.name as asset_status,
						   a.approval_status,l.name as country_name');
		$this->db->from('asset a');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id','LEFT');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('supplier sup','sup.supplier_id = t.supplier_id');
		$this->db->join('supplier cs','cs.supplier_id = t.cal_supplier_id','LEFT');
		$this->db->join('currency c','c.currency_id = a.currency_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['user_id']!='')
		{
			$this->db->like('ap.sso_id',$searchParams['user_id']);
			$this->db->where('ap.to_date',NULL);
		}
		if($searchParams['created_date'])
		{
			$this->db->where('a.created_time >=',format_date($searchParams['created_date'])." 00:00:00");
			$this->db->where('a.created_time <=',format_date($searchParams['created_date'])." 23:59:59");
		}
		if(@$searchParams['asset_number']!='')
			$this->db->where('a.asset_number',$searchParams['asset_number']);
		if(@$searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if(@$searchParams['warehouse']!='')
			$this->db->where('a.wh_id',$searchParams['warehouse']);
		if(@$searchParams['zone_id']!='')
			$this->db->where_in('a.wh_id',$wh_list);
		if(@$searchParams['asset_status']!='')
			$this->db->where('a.status',$searchParams['asset_status']);
		if(@$searchParams['asset_type']!='')
			$this->db->where('a.availability_status',$searchParams['asset_type']);
		if(@$searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if(@$searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh.status',$status);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('a.asset_id ASC');
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_sub_asset_detials($asset_id)
	{
		$this->db->select('p.serial_number,p.quantity,t.part_number,t.part_description,p.status');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_status',1);
		$this->db->where('p.part_level_id',2);
		$this->db->where('a.asset_id',$asset_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	    // created by maruthi on 23rd sep tool utilization report
    public function tool_utilization_total_num_rows($searchParams,$task_access)
    {
        $this->db->select('t.part_number,t.part_description,
                           a.asset_number,m.name as modality,tt.name as tool_type,concat(u.sso_id," - (",u.name,")") as sso,ap.from_date as from_date,ap.to_date as to_date');
        $this->db->from('asset a');
        $this->db->join('asset_position ap','ap.asset_id = a.asset_id');
        $this->db->join('part p','p.asset_id = a.asset_id');
        $this->db->join('tool t','t.tool_id = p.tool_id');
        $this->db->join('modality m','m.modality_id = t.modality_id');
        $this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
        $this->db->join('user u','u.sso_id = ap.sso_id');  
        $this->db->join('location l','l.location_id = u.country_id');      
        if($searchParams['asset_number']!='')
            $this->db->like('a.asset_number',$searchParams['asset_number']);
        if($searchParams['modality_id']!='')
            $this->db->where('t.modality_id',$searchParams['modality_id']);
        if($searchParams['tool_type_id']!='')
            $this->db->where('t.tool_type_id',$searchParams['tool_type_id']);
        if($searchParams['tool_number']!='')
            $this->db->where('t.part_number',$searchParams['tool_number']);
        if($searchParams['tool_description']!='')
            $this->db->like('t.part_description',$searchParams['tool_description']);
        if($searchParams['fe_sso_id']!='')
            $this->db->where('ap.sso_id',$searchParams['fe_sso_id']);
        if($searchParams['from_date']!='')
            $this->db->where('ap.from_date>=',format_date($searchParams['from_date']));
        if($searchParams['to_date']!='')
            $this->db->where('ap.to_date<=',format_date($searchParams['to_date']));
        if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
        if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('u.country_id',$_SESSION['s_country_id']);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('u.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('u.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('u.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
		}
        $this->db->where('p.part_level_id',1);
        $this->db->where('ap.sso_id IS NOT NULL');
        $this->db->where('ap.transit',0);
        $this->db->order_by('a.asset_id ASC,ap.asset_position_id,ap.from_date ASC');
        $res = $this->db->get();
        return $res->num_rows();
    }

    public function tool_utilization_results($current_offset, $per_page, $searchParams,$task_access)
    {
        $this->db->select('t.part_number,t.part_description,
                           a.asset_number,m.name as modality,tt.name as tool_type,concat(u.sso_id," - (",u.name,")") as sso,ap.from_date as from_date,ap.to_date as to_date,l.name as country_name,p.serial_number');
        $this->db->from('asset a');
        $this->db->join('asset_position ap','ap.asset_id = a.asset_id');
        $this->db->join('part p','p.asset_id = a.asset_id');
        $this->db->join('tool t','t.tool_id = p.tool_id');
        $this->db->join('modality m','m.modality_id = t.modality_id');
        $this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
        $this->db->join('user u','u.sso_id = ap.sso_id');  
        $this->db->join('location l','l.location_id = u.country_id');     
        if($searchParams['asset_number']!='')
            $this->db->like('a.asset_number',$searchParams['asset_number']);
        if($searchParams['modality_id']!='')
            $this->db->where('t.modality_id',$searchParams['modality_id']);
        if($searchParams['tool_type_id']!='')
            $this->db->where('t.tool_type_id',$searchParams['tool_type_id']);
        if($searchParams['tool_number']!='')
            $this->db->where('t.part_number',$searchParams['tool_number']);
        if($searchParams['tool_description']!='')
            $this->db->like('t.part_description',$searchParams['tool_description']);
        if($searchParams['fe_sso_id']!='')
            $this->db->where('ap.sso_id',$searchParams['fe_sso_id']);
        if($searchParams['from_date']!='')
            $this->db->where('ap.from_date>=',format_date($searchParams['from_date']));
        if($searchParams['to_date']!='')
            $this->db->where('ap.to_date<=',format_date($searchParams['to_date']));
        if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
        if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('u.country_id',$_SESSION['s_country_id']);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('u.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('u.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('u.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
		}
        $this->db->where('p.part_level_id',1);
        $this->db->where('ap.sso_id IS NOT NULL');
        $this->db->where('ap.transit',0);
        $this->db->order_by('a.asset_id ASC,ap.asset_position_id,ap.from_date ASC');
        $this->db->limit($per_page, $current_offset);
        $res = $this->db->get();
        return $res->result_array();
    }

    public function get_wh_list($task_access)
    {
    	$this->db->select('w.wh_id,CONCAT(w.wh_code," -(",w.name,")") as wh_name');
    	$this->db->from('warehouse w');
    	if($task_access == 1)
    	{
    		$this->db->where('w.country_id',$_SESSION['s_country_id']);
			$this->db->where('w.status',1);
    	}
    	if($task_access == 2)
    	{
    		$this->db->where('w.country_id',$this->session->userdata('s_country_id'));
    		$status = array(1,3);
    		$this->db->where_in('status',$status);
    	}
    	else if($task_access == 3)
    	{
    		if($this->session->userdata('header_country_id')!='')
    		{
    			$this->db->where('w.country_id',$this->session->userdata('header_country_id'));
    		}
    		else
    		{
    			$this->db->where_in('w.country_id',$this->session->userdata('countriesIndexedArray'));
    		}
    		$status = array(1,3);
    		$this->db->where_in('w.status',$status);
    	}
    	$this->db->order_by('w.country_id ASC');
    	$res = $this->db->get();
    	return $res->result_array();
    }

    public function get_wh_by_zone($wh_list)
	{
		$this->db->select('wh_id,wh_code,name');
		$this->db->from('warehouse');
		$this->db->where_in('wh_id',$wh_list);
		$this->db->where('status',1);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	/*koushik*/
	public function get_zone_by_country($task_access,$country_id = 0)
	{
		$this->db->select('concat(l.name, "," ,l1.name) as name, l.location_id');
		$this->db->from('location l');//zone
		$this->db->join('location l1','l1.location_id = l.parent_id');//country
		$this->db->where('l.level_id',3);
		if($country_id != '' || $country_id != 0)
		{
			$this->db->where('l1.location_id',$country_id);
		}
		else if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l1.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l1.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				$this->db->where_in('l1.location_id',$this->session->userdata('countriesIndexedArray'));	
			}
		}

		$res = $this->db->get();
		return $res->result_array();
	}

	public function download_t_utilization($searchParams,$task_access)
	{
		
		$this->db->select('t.part_number,t.part_description,p.serial_number,
                           a.asset_number,m.name as modality,tt.name as tool_type,concat(u.sso_id," - (",u.name,")") as sso,ap.from_date as from_date,ap.to_date as to_date,l.name as country_name');
		$this->db->from('asset a');
        $this->db->join('asset_position ap','ap.asset_id = a.asset_id');
        $this->db->join('part p','p.asset_id = a.asset_id');
        $this->db->join('tool t','t.tool_id = p.tool_id');
        $this->db->join('modality m','m.modality_id = t.modality_id');
        $this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
        $this->db->join('user u','u.sso_id = ap.sso_id');  
        $this->db->join('location l','l.location_id = u.country_id');  
		 if($searchParams['asset_number']!='')
            $this->db->like('a.asset_number',$searchParams['asset_number']);
        if($searchParams['modality_id']!='')
            $this->db->where('t.modality_id',$searchParams['modality_id']);
        if($searchParams['tool_type_id']!='')
            $this->db->where('t.tool_type_id',$searchParams['tool_type_id']);
        if($searchParams['tool_number']!='')
            $this->db->where('t.part_number',$searchParams['tool_number']);
        if($searchParams['tool_description']!='')
            $this->db->like('t.part_description',$searchParams['tool_description']);
        if($searchParams['fe_sso_id']!='')
            $this->db->where('ap.sso_id',$searchParams['fe_sso_id']);
        if($searchParams['from_date']!='')
            $this->db->where('ap.from_date>=',format_date($searchParams['from_date']));
        if($searchParams['to_date']!='')
            $this->db->where('ap.to_date<=',format_date($searchParams['to_date']));
        if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
        if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('u.country_id',$_SESSION['s_country_id']);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('u.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('u.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('u.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
		}
		$this->db->where('p.part_level_id',1);
        $this->db->where('ap.sso_id IS NOT NULL');
        $this->db->where('ap.transit',0);
        $this->db->order_by('a.asset_id ASC,ap.asset_position_id,ap.from_date ASC');
       	$res = $this->db->get();
        return $res->result_array();
	}
}