<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// created by Srilekha on 21st july 2017 12:18PM

class Wh_repair_m extends CI_Model 
{
	//koushik
    public function wh_calibration_total_num_rows($searchParams,$task_access='')
    {
    	//$wh_id = $this->session->userdata('swh_id');
    	$this->db->select('rco.rc_order_id,rco.rcb_number,s.name as supplier_name,date_sub(rco.expected_delivery_date, INTERVAL 2 DAY) AS ship_by_date,rco.expected_delivery_date');
    	$this->db->from('rc_order rco');
    	$this->db->join('warehouse w','w.wh_id = rco.wh_id','LEFT');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
    	$this->db->join('asset a','a.asset_id = rca.asset_id');
    	$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
    	if($searchParams['crb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['crb_number']);
		if(@$searchParams['ship_by_date']!='')
			$this->db->like('rco.expected_delivery_date',$searchParams['ship_by_date']);
		if($searchParams['supplier_id']!='')
		{
			$this->db->where('rco.supplier_id',$searchParams['supplier_id']);
		}
		$this->db->where('rca.current_stage_id',21);
		$this->db->where('rco.rc_type',1);
	    if($task_access!='')
	    {
			if($task_access==1)
			{
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
				else
				{
					if(isset($_SESSION['whsIndededArray']) && $searchParams['whr_id']=='')
					{
						$this->db->where_in("rco.wh_id",$_SESSION['whsIndededArray']); 
					}
					else			
					{	
						$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
					}	
				}
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rco.rc_order_id');
		$this->db->order_by('rco.rc_order_id DESC');
		$res = $this->db->get();
		return $res->num_rows();
    }

    //koushik
    public function wh_calibration_results($current_offset, $per_page, $searchParams,$task_access='')
	{
		//$wh_id = $this->session->userdata('swh_id');
    	$this->db->select('rco.rc_order_id,rco.rcb_number,s.name as supplier_name,date_sub(rco.expected_delivery_date, INTERVAL 2 DAY) AS ship_by_date,rco.expected_delivery_date,s.supplier_code,rco.country_id,concat(w.wh_code,"-(",w.name,")") AS wh_name');
    	$this->db->from('rc_order rco');
    	$this->db->join('warehouse w','w.wh_id = rco.wh_id','LEFT');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
    	$this->db->join('asset a','a.asset_id = rca.asset_id');
    	$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
    	if($searchParams['crb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['crb_number']);
		if($searchParams['supplier_id']!='')
			$this->db->where('rco.supplier_id',$searchParams['supplier_id']);
		if(@$searchParams['ship_by_date']!='')
			$this->db->like('rco.expected_delivery_date',$searchParams['ship_by_date']);
		$this->db->where('rca.current_stage_id',21);
		//$this->db->where('rco.wh_id',$wh_id);
		$this->db->where('rco.rc_type',1);
		if($task_access!='')
	    {
			if($task_access==1)
			{
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
				else
				{
					if(isset($_SESSION['whsIndededArray']) && $searchParams['whr_id']=='')
					{
						$this->db->where_in("rco.wh_id",$_SESSION['whsIndededArray']); 
					}
					else			
					{	
						$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
					}	
				}
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rco.rc_order_id');
		$this->db->order_by('rco.rc_order_id DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function get_cal_asset_list($rc_order_id)
	{
		$this->db->select('rca.rc_number,a.asset_number,t.part_number,rco.rc_order_id,
			rca.rc_asset_id,t.part_description,a.asset_id,rca.rc_asset_id,p.serial_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',1);
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function get_cal_sup_detials($rc_order_id)
	{
		$this->db->select('rco.*,s.supplier_code,s.name as supplier_name');
		$this->db->from('rc_order rco');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	//koushik
	public function get_cal_asset_detail($rc_asset_id)
	{
		$this->db->select('rca.rc_number,a.asset_number,t.part_number,p.serial_number,
						   t.part_description,a.asset_id,rca.rc_asset_id,rca.rc_order_id,
						   a.sub_inventory,s.supplier_code,s.name as supplier_name');
		$this->db->from('rc_asset rca');
		$this->db->join('rc_order rco','rco.rc_order_id = rca.rc_order_id');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('rca.rc_asset_id',$rc_asset_id);
		$this->db->where('p.part_level_id',1);
		$res = $this->db->get();
		return $res->row_array();
	}

	//koushik
	public function get_rcsh_by_rc_asset($rc_asset_id,$current_stage_id)
	{
		$this->db->select('rcsh.rcsh_id');
		$this->db->from('rc_asset rca');
		$this->db->join('rc_status_history rcsh','rcsh.rc_asset_id = rca.rc_asset_id');
		$this->db->where('rca.rc_asset_id',$rc_asset_id);
		$this->db->where('rcsh.current_stage_id',$current_stage_id);
		$this->db->order_by('rcsh.rcsh_id DESC');
		$this->db->limit(1);
		$res = $this->db->get();
		return $res->row_array();
	}

	 //koushik
    public function get_asset_part_details($asset_id)
	{
		
		$this->db->select('a.asset_id,a.asset_number,p.part_id,
						  pl.name as part_level_name,t.part_number,
						  t.part_description,p.quantity,t.tool_id, a.status as asset_status,p.serial_number');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('a.status',10);
		$this->db->where('p.part_status',1);
		$this->db->order_by('p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_from_address($rc_order_id)
	{
		$this->db->select('wh.*,l.name as from_location,rcs.remarks,
						   pf.format_number,pf.created_time,pf.print_type,
						   rcs.courier_type,rcs.courier_number,rcs.billed_to,
						   GROUP_CONCAT(rca.rc_number SEPARATOR",") as crn_number,
						   GROUP_CONCAT(a.asset_number SEPARATOR",") as asset_number_list,
						   rcs.courier_name,rcs.contact_person,rcs.phone_number,s.supplier_code,s.name as supplier_name,s.contact_number as s_contact_number,
						   rcs.vehicle_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = rco.wh_id');
		$this->db->join('rc_shipment rcs','rcs.rc_order_id = rco.rc_order_id');
		$this->db->join('print_format pf','pf.print_id = rcs.print_id');
		$this->db->join('location l','l.location_id = wh.location_id');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id','LEFT');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->group_by('rco.rc_order_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_calibration_tool($rc_order_id)
	{
		$this->db->select('t.part_number,t.part_description,t.hsn_code,count(t.tool_id) as quantity,sum(a.cost_in_inr) as cost_in_inr,t.gst_percent,GROUP_CONCAT(CONCAT(p.serial_number," : ",a.asset_number) SEPARATOR ",") as asset_number_list');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function closed_cal_delivery_total_num_rows($searchParams,$task_access='')
	{
		$status=array(2,10);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		$this->db->where('rco.rc_type',1);
		$this->db->where_in('rco.status',$status);
		$this->db->where('rsh.current_stage_id',22);
		if($task_access!='')
		{
			if($task_access==1)
			{
				if($searchParams['whc_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whc_id']);
				}
				else
				{
					if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
					{
						$this->db->where_in('rco.wh_id',$_SESSION['whsIndededArray']); 
					}
					else			
					{	
						$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
					}	
				}
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whc_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whc_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whc_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whc_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id DESC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function closed_cal_delivery_results($current_offset, $per_page, $searchParams,$task_access='')
	{
		$status=array(2,10);
		$this->db->select('rco.*,w.wh_code,w.name,rca.current_stage_id');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		$this->db->where('rco.rc_type',1);
		$this->db->where_in('rco.status',$status);
		$this->db->where('rsh.current_stage_id',22);
		if($task_access!='')
		{
			if($task_access==1)
			{
				if($searchParams['whc_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whc_id']);
				}
				else
				{
					if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
					{
						$this->db->where_in('rco.wh_id',$_SESSION['whsIndededArray']); 
					}
					else			
					{	
						$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
					}	
				}
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whc_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whc_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whc_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whc_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id DESC');
		//echo $this->db->last_query(); exit;
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_vendor_closed_assets($rc_order_id)
	{
		$status=array(1,10);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where('rsh.current_stage_id',22);
		$this->db->where('rca.rca_type',1);
		$this->db->where_in('rca.status',$status);
		//$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
}