<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_m extends CI_Model 
{
	// To show When Raising Order
	public function tool_total_num_rows($searchParams,$whs)
	{
		$this->db->select('t.*,m.name as modality_name,et.eq_model_id,count(a.asset_id) as asset_count');
		$this->db->distinct('p.tool_id');
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id AND p.part_level_id = 1','left');
		$this->db->join('asset a','a.asset_id = p.asset_id AND a.wh_id IN ('.$whs.') AND a.status = 1 AND a.approval_status = 0','left');
		$this->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);		
		if($searchParams['ro_country_id']!='')
			$this->db->where('a.country_id',$searchParams['ro_country_id']);
		if($searchParams['ro_wh_id']!='')
			$this->db->where('a.wh_id',$searchParams['ro_wh_id']);
		$this->db->where('t.status',1);
		$this->db->where('t.country_id',$_SESSION['transactionCountry']);
		$this->db->order_by('asset_count DESC, t.tool_id DESC');
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function tool_results($current_offset, $per_page, $searchParams,$whs)
	{
		$this->db->select('t.*,m.name as modality_name,et.eq_model_id,count(distinct(a.asset_id)) as asset_count');
		$this->db->distinct('p.tool_id');
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id AND p.part_level_id = 1','left');
		$this->db->join('asset a','a.asset_id = p.asset_id AND a.wh_id IN ('.$whs.') AND a.status = 1 AND a.approval_status = 0','left');
		$this->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);		
		if($searchParams['ro_country_id']!='')
			$this->db->where('a.country_id',$searchParams['ro_country_id']);
		if($searchParams['ro_wh_id']!='')
			$this->db->where('a.wh_id',$searchParams['ro_wh_id']);
		$this->db->where('t.status',1);
		$this->db->where('t.country_id',$_SESSION['transactionCountry']);
		$this->db->order_by('asset_count DESC, t.tool_id DESC');
		$this->db->group_by('t.tool_id');	
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

    public function get_install_basedata($system_id,$country_id=0,$ids=array())
    {    	
    	$this->db->select('cs.*,c.name as c_name,concat(c.name, " - ",c.customer_number) as custFullName,ib.install_base_id');
		$this->db->from('install_base ib');		
		$this->db->join('customer_site cs','ib.customer_site_id = cs.customer_site_id');
		$this->db->join('customer c','c.customer_id = cs.customer_id');
		if(count($ids)>0)
		{
			$ids = convertIntoIndexedArray($ids,'country_id'); //when returnning
			$this->db->where_in('ib.country_id',$ids);
		}
		else if($country_id!=0 || $country_id != '')
		{
			$this->db->where('c.country_id',$country_id); // when ordering	
		}		
		$this->db->where('ib.system_id',$system_id);		
		$res = $this->db->get();		
		return $res->row_array();
    }

   	public function get_cust_info_by_site($site_id)
    {
    	$this->db->select('cs.*,c.name as c_name');
		$this->db->from('customer c');
		$this->db->join('customer_site cs','c.customer_id = cs.customer_id');		
		$this->db->where('cs.site_id',$site_id);
		$res = $this->db->get();
		return $res->row_array();
    }

    // Open Order Results
    public function order_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type,concat(u.sso_id,"- (",u.name," )") as sso,l.name as countryName');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['search_fe_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['search_fe_sso_id']);
		$this->db->where('to.current_stage_id!=',10);
		$this->db->where('to.status<',2);		
		$this->db->where('to.order_type',2);
		if($task_access == 1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['ro_country_id']!='')
					{
						$this->db->where('to.country_id',$searchParams['ro_country_id']);
					}
					else
					{
						$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));					
		}
		else if($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ro_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['ro_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		
		$this->db->order_by('to.tool_order_id,to.country_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function order_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.tool_order_id');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['search_fe_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['search_fe_sso_id']);
		$this->db->where('to.current_stage_id!=',10);
		$this->db->where('to.status<',2);		
		$this->db->where('to.order_type',2);
		if($task_access == 1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['ro_country_id']!='')
					{
						$this->db->where('to.country_id',$searchParams['ro_country_id']);
					}
					else
					{
						$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));					
		}
		else if($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ro_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['ro_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		
		$this->db->order_by('to.tool_order_id,to.country_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function closed_order_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,cst.*,concat(u.sso_id," - ( ",u.name," )") as sso,odt.name as order_type,to.status as final_status,
			DATE(to.modified_time) as closed_date,DATE(oddl.created_time) as shipped_date,l.name as countryName');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		$this->db->join('order_delivery oddl','oddl.tool_order_id = to.tool_order_id','left');

		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);		
		if($searchParams['status']!='')
			$this->db->where('to.status',$searchParams['status']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));	
		if($searchParams['closed_fe_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['closed_fe_sso_id']);	
		
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access ==1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['co_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['co_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}	
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['co_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['co_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('to.current_stage_id',10);
		$this->db->where('to.order_type',2);
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function closed_order_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type,to.status as final_status');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);		
		if($searchParams['status']!='')
			$this->db->where('to.status',$searchParams['status']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));	
		if($searchParams['closed_fe_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['closed_fe_sso_id']);	
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access ==1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['co_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['co_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}	
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));	
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['co_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['co_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}	
		$this->db->where('to.order_type',2);
		$res = $this->db->get();		
		return $res->num_rows();
	}
	public function getReturnsForOrder($tool_order_id)
	{
		$arr2 = array(0,2);
		$this->db->select('to.order_number,ro.return_number,ro.created_time,u.name as user_name,l.name as location_name,u.sso_id,ro.return_order_id,ro.received_date,rt.name as return_type_name');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->where('to.tool_order_id',$tool_order_id);		
		$this->db->where('osh.current_stage_id',10);//Closed fe returns
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_approval',$arr2);
		$this->db->order_by('ro.return_number DESC','ro.created_time DESC');
		$this->db->group_by('ro.return_order_id');		
		$res = $this->db->get();		
		return $res->result_array();
	}

	public function get_fe_closed_return_details($return_order_id,$rto_id)
	{		
		$this->db->select('ro.*, to.order_number, 
						   to.sso_id, u.name as user_name,
						   l.name as location_name,osh.order_status_id,to.tool_order_id,rto.rto_id');
		$this->db->from('return_order ro');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('osh.rto_id',$rto_id);
		$this->db->where('osh.current_stage_id',10);//waiting for pickup		
		$this->db->where('to.order_type',2); //tool order = 2
		$res = $this->db->get();
		return $res->row_array();
	}
	
	public function get_order_info($tool_order_id)
	{
		$this->db->select('to.*,oda.*,
			concat(u.sso_id," - ( ",u.name," )") as sso,
			concat(wh.wh_code,"- ( ",wh.name," )") as con_wh,
			concat(to_wh.wh_code,"- ( ",to_wh.name," )") as shipToWh,
			concat(c.customer_number,"- ( ",c.name," )") as shipToCustomer,
			oda.remarks as address_change_remarks
						');
		$this->db->from('tool_order to');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('warehouse to_wh','to_wh.wh_id = to.to_wh_id','left');
		$this->db->join('install_base ib','ib.install_base_id = oda.install_base_id','left');
		$this->db->join('customer_site cs','cs.customer_site_id=ib.customer_site_id','left');
		$this->db->join('customer c','c.customer_id = cs.customer_id','left');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_ordered_tools($tool_order_id,$closed=0)
	{
		$this->db->select('o.*,t.*');
		$this->db->from('ordered_tool o');
		$this->db->join('tool t','t.tool_id = o.tool_id');		
		$this->db->where('o.tool_order_id',$tool_order_id);
		if($closed == 0 )
			$this->db->where('o.status<',3);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function receive_order_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.*,odt.name as order_type,cst.name as current_stage_name,concat(u.sso_id," (-",u.name,")") as sso,l.name as countryName,sto.stock_transfer_id');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id','LEFT');
		$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','LEFT');
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','to.country_id = l.location_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$search_query = '';
		if(@$searchParams['order_number']!='')
		{
			$search_query.='to.order_number = "'.$searchParams['order_number'].'" AND ';
		}
		if(@$searchParams['order_delivery_type_id']!='')
		{
			$search_query.='to.order_delivery_type_id = '.$searchParams['order_delivery_type_id'].' AND ';
		}
		if(@$searchParams['deploy_date']!='')
		{
			$search_query.='to.deploy_date >= "'.format_date($searchParams['deploy_date']).'" AND ';
		}
		if(@$searchParams['return_date']!='')
		{
			$search_query.='to.return_date <= "'.format_date($searchParams['return_date']).'" AND ';
		}
		if($searchParams['rec_sso_id']!='')
		{
			$search_query.='to.sso_id = '.$searchParams['rec_sso_id'].' AND ';
		}

		if($task_access == 1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$search_query.='to.country_id ='.$this->session->userdata('header_country_id').' AND ';
				}
				else
				{
					if($searchParams['rec_country_id']!='')
					{
						$search_query.= 'to.country_id ='.$this->session->userdata('rec_country_id').' AND ';
					}
					else
					{
						$country_arr = $this->session->userdata('countriesIndexedArray');
						$country_val = implode(',', $country_arr);
						$search_query.='to.country_id IN ('.$country_val.') AND ';
					}
				}	
			}
			$search_query.='to.sso_id ='.$this->session->userdata('sso_id').' AND ';
		}
		else if($task_access == 2)
		{
			$search_query.='to.country_id ='.$this->session->userdata('s_country_id').' AND ';
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$search_query.='to.country_id ='.$this->session->userdata('header_country_id').' AND ';
			}
			else
			{
				if($searchParams['rec_country_id']!='')
				{
					$search_query.= 'to.country_id ='.$this->session->userdata('rec_country_id').' AND ';
				}
				else
				{
					$country_arr = $this->session->userdata('countriesIndexedArray');
					$country_val = implode(',', $country_arr);
					$search_query.='to.country_id IN ('.$country_val.') AND ';
				}
			}
		}

		$str1 = '('.$search_query.' osh.current_stage_id = 6 AND to.current_stage_id = 6 AND to.status<2 AND to.order_type = 2 AND osh.rto_id IS NULL AND osh.end_time IS NULL)';
		$this->db->where($str1);
		$str2 = '('.$search_query.' to.current_stage_id =4 AND stn.current_stage_id =2 AND stn.fe_check = 1)';
		$this->db->or_where($str2);

		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function receive_order_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,odt.name as order_type,cst.name as current_stage_name,concat(u.sso_id," -(",u.name,")") as sso,l.name as countryName,sto.stock_transfer_id');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id','LEFT');
		$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','LEFT');
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','to.country_id = l.location_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');

		$search_query = '';
		if(@$searchParams['order_number']!='')
		{
			$search_query.='to.order_number = "'.$searchParams['order_number'].'" AND ';
		}
		if(@$searchParams['order_delivery_type_id']!='')
		{
			$search_query.='to.order_delivery_type_id = '.$searchParams['order_delivery_type_id'].' AND ';
		}
		if(@$searchParams['deploy_date']!='')
		{
			$search_query.='to.deploy_date >= "'.format_date($searchParams['deploy_date']).'" AND ';
		}
		if(@$searchParams['return_date']!='')
		{
			$search_query.='to.return_date <= "'.format_date($searchParams['return_date']).'" AND ';
		}
		if($searchParams['rec_sso_id']!='')
		{
			$search_query.='to.sso_id = '.$searchParams['rec_sso_id'].' AND ';
		}

		if($task_access == 1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$search_query.='to.country_id ='.$this->session->userdata('header_country_id').' AND ';
				}
				else
				{
					if($searchParams['rec_country_id']!='')
					{
						$search_query.= 'to.country_id ='.$this->session->userdata('rec_country_id').' AND ';
					}
					else
					{
						$country_arr = $this->session->userdata('countriesIndexedArray');
						$country_val = implode(',', $country_arr);
						$search_query.='to.country_id IN ('.$country_val.') AND ';
					}
				}	
			}
			$search_query.='to.sso_id ='.$this->session->userdata('sso_id').' AND ';
		}
		else if($task_access == 2)
		{
			$search_query.='to.country_id ='.$this->session->userdata('s_country_id').' AND ';
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$search_query.='to.country_id ='.$this->session->userdata('header_country_id').' AND ';
			}
			else
			{
				if($searchParams['rec_country_id']!='')
				{
					$search_query.= 'to.country_id ='.$this->session->userdata('rec_country_id').' AND ';
				}
				else
				{
					$country_arr = $this->session->userdata('countriesIndexedArray');
					$country_val = implode(',', $country_arr);
					$search_query.='to.country_id IN ('.$country_val.') AND ';
				}
			}
		}

		$str1 = '('.$search_query.' osh.current_stage_id = 6 AND to.current_stage_id = 6 AND to.status<2 AND to.order_type = 2 AND osh.rto_id IS NULL AND osh.end_time IS NULL)';
		$this->db->where($str1);
		$str2 = '('.$search_query.' to.current_stage_id =4 AND stn.current_stage_id =2 AND stn.fe_check = 1)';
		$this->db->or_where($str2);
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->group_by('to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function raisedSTforOrderResults($current_offset,$per_page,$searchParams,$closed=0,$task_access=1) // 1 for closed
	{
		$this->db->select('to.*,l.name as countryName,oda.*,stn.fe_check as sto_fe_check,cs.name as cs_name,concat(u.sso_id, "-(" ,u.name,")") as sso,odt.name as order_type');
		$this->db->from('tool_order to');	
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');	
		$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
		$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id');
		$this->db->join('location l','to.country_id = l.location_id');
		if(@$searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if(@$searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if(@$searchParams['deploy_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['deploy_date']));
		if(@$searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['rec_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['rec_sso_id']);	
		$this->db->where('to.order_type',2);
		if($closed==1)
		{
			$this->db->where('to.current_stage_id>',4);
			$this->db->where('stn.current_stage_id >',2);
		}
		else
		{
			$this->db->where('to.current_stage_id',4);
			$this->db->where('stn.current_stage_id',2);
			$this->db->where('stn.fe_check = 1');
		}
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			else
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['rec_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['rec_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}		
		$this->db->group_by('sto.tool_order_id');
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function raisedSTforOrderResults_num_rows($searchParams,$closed=0,$task_access=1) // 1 for closed
	{
		$this->db->select('to.*,l.name as countryName,oda.*,stn.fe_check as sto_fe_check,cs.name as cs_name,concat(u.sso_id, "-(" ,u.name,")") as sso,odt.name as order_type');
		$this->db->from('tool_order to');	
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');	
		$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
		$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id');
		$this->db->join('location l','to.country_id = l.location_id');
		if(@$searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if(@$searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if(@$searchParams['deploy_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['deploy_date']));
		if(@$searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['rec_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['rec_sso_id']);	
		$this->db->where('to.order_type',2);
		if($closed==1)
		{
			$this->db->where('to.current_stage_id >',4);
			$this->db->where('stn.current_stage_id >',2);
		}
		else
		{
			$this->db->where('to.current_stage_id',4);
			$this->db->where('stn.current_stage_id',2);
			$this->db->where('stn.fe_check = 1');
		}
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['rec_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['rec_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->group_by('sto.tool_order_id');
		$this->db->order_by('to.country_id,to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function closed_fe_receive_order_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('osh.rto_id,to.*,concat(u.sso_id," - ",u.name) as sso,odt.name as order_type,cst.name as cs_name,l.name as countryName,stt.stock_transfer_id');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order stt','stt.tool_order_id = to.tool_order_id','LEFT');
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('location l','l.location_id=to.country_id','left');

		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['rec_sso_id']!='')
		{
			$this->db->where('to.sso_id',$searchParams['rec_sso_id']);	
		}
		if($searchParams['ack_from']!='')
		{
			if($searchParams['ack_from'] ==1)
			{
				$this->db->where('osh.rto_id IS NULL');		
			}
			else
			{
				$this->db->where('osh.rto_id IS NOT NULL');		
			}
		}
		$this->db->where('osh.current_stage_id',7);					
		$this->db->where('to.status!=',4);	
		$this->db->where('to.order_type',2);
				
		if($task_access == 1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['crec_country_id']!='')
					{
						$this->db->where('to.country_id',$searchParams['crec_country_id']);
					}
					else
					{
						$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
		}
		else if($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['crec_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['crec_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->group_by('to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function closed_fe_receive_order_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('osh.rto_id,to.*,concat(u.sso_id," - ",u.name) as sso,odt.name as order_type,cst.name as cs_name,l.name as countryName,stt.stock_transfer_id');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order stt','stt.tool_order_id = to.tool_order_id','LEFT');
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('location l','l.location_id=to.country_id','left');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['rec_sso_id']!='')
		{
			$this->db->where('to.sso_id',$searchParams['rec_sso_id']);	
		}
		if($searchParams['ack_from']!='')
		{
			if($searchParams['ack_from'] ==1)
			{
				$this->db->where('osh.rto_id IS NULL');		
			}
			else
			{
				$this->db->where('osh.rto_id IS NOT NULL');		
			}
		}
		$this->db->where('osh.current_stage_id',7);					
		$this->db->where('to.status!=',4);	
		$this->db->where('to.order_type',2);
				
		if($task_access == 1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['crec_country_id']!='')
					{
						$this->db->where('to.country_id',$searchParams['crec_country_id']);
					}
					else
					{
						$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
		}
		else if($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['crec_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['crec_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function getOrderedAssetsFromTransit($tool_order_id)
	{
		$this->db->select('oah.*,oa.ordered_asset_id as ordered_asset_id,a.asset_number as asset_number')	;
		$this->db->from('order_asset_history as oah');
		$this->db->join('order_status_history osh','osh.order_status_id = oah.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->where('osh.current_stage_id',6);
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_transit_asset_data($oah_id)
	{
		$this->db->select('pl.name as part_level_name,p.part_id as part_id,t.part_number as part_number,t.part_description as part_description,
			p.quantity as quantity,p.serial_number as serial_number,oahl.oa_health_id as oa_health_id,
			oahl.asset_condition_id as asset_condition_id,oahl.remarks as remarks,ac.name as health_status,p.tool_id as p_tool_id');
		$this->db->from('order_asset_health oahl');
		$this->db->join('part p','p.part_id = oahl.part_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id= p.tool_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = oahl.asset_condition_id');
		$this->db->where('oahl.oah_id',$oah_id);
		$res= $this->db->get();
		return $res->result_array();
	}

	public function owned_order_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.*,odt.name as order_type');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['rt_sso_id']!='')
			$this->db->where('to.sso_id',($searchParams['rt_sso_id']));
		$this->db->where('to.current_stage_id=',7);					
		$this->db->where('to.order_type',2);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['rt_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['rt_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['rt_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['rt_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$res = $this->db->get();		
		return $res->num_rows();
	}

	public function owned_order_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,odt.name as order_type,cst.name as current_stage_name,l.name as countryName,concat(u.sso_id,"-",u.name) as sso');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('location l','l.location_id=to.country_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		$this->db->where('to.current_stage_id',7);			
		$this->db->where('to.order_type',2);
		if($searchParams['rt_sso_id']!='')
			$this->db->where('to.sso_id',($searchParams['rt_sso_id']));
		$this->db->where('to.order_type',2);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['rt_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['rt_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}

				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['rt_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['rt_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}		
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function getOwnedAssetsDetailsByOrderId($tool_order_id)
	{
		$owned_assets = array(1,2);
		$this->db->select('oah.*,oah.status as history_status_id,oa.ordered_asset_id as ordered_asset_id,oa.asset_id as asset_id,a.asset_number as asset_number,t.part_number as part_number,t.tool_code as tool_code,p.serial_number as serial_number,a.cal_due_date, t.part_description as part_description,tt.name as tool_type, p.serial_number');
		$this->db->from('order_asset_history as oah');
		$this->db->join('order_status_history osh','osh.order_status_id = oah.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('osh.current_stage_id',7);
		$this->db->where_in('oah.status',$owned_assets);
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$this->db->group_by('p.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function wh_return_initialted_assets($tool_order_id,$fe_check=0)
	{
		$this->db->select('osh.tool_order_id as fe1_tool_order_id,osh.order_status_id as fe1_order_status_id,
							oah.*,a.asset_number,a.asset_id,t.part_number,t.part_description,as.name as asset_status,oa.ordered_tool_id,t.tool_code');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_condition as','as.asset_condition_id = osh.status');
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$this->db->where('p.part_level_id',1);
		if($fe_check == 1)
			$this->db->where('osh.current_stage_id',2);
		else
			$this->db->where('osh.current_stage_id',6);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();

	}

	public function get_return_info($tool_order_id)
	{
		$this->db->select('to.*,od.*');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_total_ordered_quantity($fe2_tool_order_id)
	{
		$this->db->select('sum(quantity) as tot_qty');
		$this->db->from('ordered_tool');
		$this->db->where('tool_order_id',$fe2_tool_order_id);
		$this->db->where('status<',3);
		$res = $this->db->get();
		$result = $res->row_array();
		return $result['tot_qty'];
	}

		// FE owned tools
	public function feReturnInitiatedResults($current_offset, $per_page, $searchParams,$task_access)
	{		
		$this->db->select('to.order_number as fe1_order_number,to.tool_order_id as from_tool_order_id,
						l.name as countryName,concat(u.sso_id," - ",u.name) as sso,ro.*,
						rt.name as return_type,osh.order_status_id,rto.rto_id as rto_id,
						to_order.order_number as to_order,concat(to_user.sso_id," - ",to_user.name) as to_sso,
						to_order.current_stage_id as to_current_stage_id,ro.remarks as return_order_remarks,
						concat(wh1.wh_code," - ",wh1.name) as wh1,concat(wh2.wh_code," - ",wh2.name) as wh2
						');		
		$this->db->from('tool_order to');	
		$this->db->join('return_tool_order rto','rto.tool_order_id = to.tool_order_id');
		$this->db->join('return_order ro','ro.return_order_id = rto.return_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = ro.country_id','left');
		$this->db->join('tool_order to_order','to_order.tool_order_id = ro.tool_order_id','left');
		$this->db->join('user to_user','to_user.sso_id = to_order.sso_id','left');
		$this->db->join('warehouse wh1','wh1.wh_id = ro.wh_id','left');
		$this->db->join('warehouse wh2','wh2.wh_id = ro.ro_to_wh_id','left');
		if($searchParams['ri_order_number']!='')
			$this->db->like('to.order_number',$searchParams['ri_order_number']);	
		if($searchParams['ri_return_number']!='')
			$this->db->like('ro.return_number',$searchParams['ri_return_number']);	
		if($searchParams['ri_return_type_id']!='')
			$this->db->like('ro.return_type_id',$searchParams['ri_return_type_id']);				
		if($searchParams['ri_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['ri_sso_id']);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('ro.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['ri_country_id']!='')
						{
							$this->db->where('ro.country_id',$searchParams['ri_country_id']);
						}
						else
						{
							$this->db->where_in('ro.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('ro.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('ro.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ri_country_id']!='')
				{
					$this->db->where('ro.country_id',$searchParams['ri_country_id']);
				}
				else
				{
					$this->db->where_in('ro.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}				
		$this->db->where('ro.status!=',10);
		$this->db->where_in('ro.return_approval',array(0,1,2));
		$this->db->group_by('ro.return_order_id');
		$this->db->order_by('to.country_id,ro.return_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();		
		return $res->result_array();
	}	

	public function feReturnInitiatedTotalNumRows($searchParams,$task_access)
	{
		$this->db->select('to.*,ro.*,rt.name as return_type');
		$this->db->from('tool_order to');	
		$this->db->join('return_tool_order rto','rto.tool_order_id = to.tool_order_id');
		$this->db->join('return_order ro','ro.return_order_id = rto.return_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		if($searchParams['ri_order_number']!='')
			$this->db->like('to.order_number',$searchParams['ri_order_number']);	
		if($searchParams['ri_return_number']!='')
			$this->db->like('ro.return_number',$searchParams['ri_return_number']);	
		if($searchParams['ri_return_type_id']!='')
			$this->db->where('ro.return_type_id',$searchParams['ri_return_type_id']);
		$this->db->where('ro.status!=',10);
		$this->db->where_in('ro.return_approval',array(0,1,2));
		if($searchParams['ri_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['ri_sso_id']);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('ro.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['ri_country_id']!='')
						{
							$this->db->where('ro.country_id',$searchParams['ri_country_id']);
						}
						else
						{
							$this->db->where_in('ro.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('ro.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('ro.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ri_country_id']!='')
				{
					$this->db->where('ro.country_id',$searchParams['ri_country_id']);
				}
				else
				{
					$this->db->where_in('ro.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->group_by('ro.return_order_id');
		$res = $this->db->get();		
		return $res->num_rows();
	}

	public function closedfeReturnInitiatedResults($current_offset, $per_page, $searchParams,$task_access)
	{		
		$this->db->select('to.order_number as fe1_order_number,to.tool_order_id as from_tool_order_id,				
				l.name as countryName,concat(u.sso_id," - ",u.name) as sso,ro.*,
				rt.name as return_type,osh.order_status_id,rto.rto_id,
				to_order.order_number as to_order,concat(to_user.sso_id," - ",to_user.name) as to_sso,
				to_order.current_stage_id as to_current_stage_id,ro.remarks as return_order_remarks,
				concat(wh1.wh_code," - ",wh1.name) as wh1,concat(wh2.wh_code," - ",wh2.name) as wh2
			');
		$this->db->from('tool_order to');	
		$this->db->join('return_tool_order rto','rto.tool_order_id = to.tool_order_id');
		$this->db->join('return_order ro','ro.return_order_id = rto.return_order_id');
		// with rto id fe1 order fe2 order linked so to avoid we are using 2 conditions
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id '); 
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('location l','l.location_id = ro.country_id','left');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('tool_order to_order','to_order.tool_order_id = ro.tool_order_id','left');
		$this->db->join('user to_user','to_user.sso_id = to_order.sso_id','left');
		$this->db->join('warehouse wh1','wh1.wh_id = ro.wh_id','left');
		$this->db->join('warehouse wh2','wh2.wh_id = ro.ro_to_wh_id','left');
		if($searchParams['ric_order_number']!='')
			$this->db->like('to.order_number',$searchParams['ric_order_number']);	
		if($searchParams['ric_return_number']!='')
			$this->db->like('ro.return_number',$searchParams['ric_return_number']);	
		if($searchParams['ric_return_type_id']!='')
			$this->db->like('ro.return_type_id',$searchParams['ric_return_type_id']);						
		$this->db->where('ro.status',10);
		if($searchParams['ric_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['ric_sso_id']);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['ric_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['ric_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ric_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['ric_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->group_by('ro.return_order_id');
		$this->db->order_by('to.country_id,ro.return_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}	
	
	public function closedfeReturnInitiatedTotalNumRows($searchParams,$task_access)
	{
		$this->db->select('to.*,ro.*,rt.name as return_type');
		$this->db->from('tool_order to');	
		$this->db->join('return_tool_order rto','rto.tool_order_id = to.tool_order_id');
		$this->db->join('return_order ro','ro.return_order_id = rto.return_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		if($searchParams['ric_order_number']!='')
			$this->db->like('to.order_number',$searchParams['ric_order_number']);	
		if($searchParams['ric_return_number']!='')
			$this->db->like('ro.return_number',$searchParams['ric_return_number']);	
		if($searchParams['ric_return_type_id']!='')
			$this->db->like('ro.return_type_id',$searchParams['ric_return_type_id']);						
		if($searchParams['ric_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['ric_sso_id']);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['ric_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['ric_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ric_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['ric_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('ro.status',10);
		$this->db->group_by('ro.return_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function getAssetDataByOrderStatusID($order_status_id,$status=1)
	{
		$this->db->select('a.asset_number,a.asset_id,t.part_number,t.part_description,pl.name as part_level,p.serial_number,p.quantity as quantity,
			tt.name as tool_type');
		$this->db->from('order_asset_history oah');
		$this->db->join('order_asset_health oahl','oah.oah_id = oahl.oah_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id AND oahl.part_id = p.part_id');		
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->where('oah.order_status_id',$order_status_id);
		$this->db->where('p.part_level_id',1);
		if($status == 3)
			$this->db->where('oah.status<',3);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_return_latest_record($country_id)
	{
		$this->db->select('return_number');
		$this->db->from('return_order');	
		$this->db->where('country_id',$country_id)	;
		$this->db->order_by('return_order_id DESC');
		$res = $this->db->get();
		return $res->row_array();
	}
	public function get_st_latest_record($country_id)
	{
		$this->db->select('stn_number');
		$this->db->from('tool_order');		
		$this->db->where('order_type',1);
		$this->db->where('country_id',$country_id);
		$this->db->order_by('tool_order_id DESC');
		$res = $this->db->get();
		return $res->row_array();
	}
	
	public function get_latest_order_record($country_id)
	{
		$this->db->select('order_number');
		$this->db->from('tool_order');
		$this->db->where('order_type',2);
		$this->db->where('country_id',$country_id);
		$this->db->order_by('tool_order_id DESC');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_locked_assets($ordered_tool_id,$available_quantity)
	{
		$this->db->select('a.asset_number,ash.*,a.status as asset_main_status');
		$this->db->from('asset_status_history ash');
		$this->db->join('asset a','a.asset_id = ash.asset_id');
		$this->db->where('ash.ordered_tool_id',$ordered_tool_id);
		$this->db->where('a.status',2); /// created by maruthi on 31st Aug'18
		$this->db->where('ash.end_time IS NULL');  // created by maruthi on 8th Oct'18
		$this->db->group_by('ash.ordered_tool_id,ash.ash_id');
		$this->db->order_by('ash.ash_id DESC');
		$this->db->limit($available_quantity);
		$res = $this->db->get();	
		if($res->num_rows()>0)
			return $res->result_array();
		else
			return FALSE;
	}

	public function get_st_locked_assets($tool_order_id,$tool_id)
	{
		$this->db->select('a.asset_number,ash.*,a.status as asset_main_status');
		$this->db->from('asset_status_history ash');
		$this->db->join('asset a','a.asset_id = ash.asset_id');
		$this->db->join('tool_order st','st.tool_order_id = ash.tool_order_id');
		$this->db->join('st_tool_order sto','sto.stock_transfer_id = st.tool_order_id');
		$this->db->join('ordered_tool odt','st.tool_order_id = odt.tool_order_id');		
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = odt.ordered_tool_id AND oa.asset_id = ash.asset_id');		
		$this->db->where('odt.tool_id',$tool_id);
		$this->db->where('sto.tool_order_id',$tool_order_id);
		$this->db->where('st.fe_check',0);
		$this->db->where('st.current_stage_id',3);
		$this->db->where('ash.current_stage_id',3);
		$this->db->where('a.status',2); /// created by maruthi on 31st Aug'18
		$this->db->where('ash.end_time IS NULL');  // created by maruthi on 8th Oct'18	
		$res = $this->db->get();
		if($res->num_rows()>0)
			return $res->result_array();
		else
			return FALSE;
	}

	public function checkMultipleReturnsForFE2Order($fe1_tool_order_id)
	{
		$this->db->select('ro.return_number,fe1.order_number as fe1_order_number,fe2.order_number as fe2_order_number');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id=ro.return_order_id');
		$this->db->join('tool_order fe1','fe1.tool_order_id = rto.tool_order_id');
		$this->db->join('tool_order fe2','fe2.tool_order_id = ro.tool_order_id');
		$this->db->where_in('ro.return_approval',array(1,2));		
		$this->db->where('ro.tool_order_id',$fe1_tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_order_asset_arr($ordered_asset_id)
	{
		$this->db->select('a.asset_id, a.asset_number, t.part_description');
		$this->db->from('ordered_asset oa');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('oa.ordered_asset_id',$ordered_asset_id);
		$this->db->group_by('oa.ordered_asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_tool_id_by_oa_id($ordered_asset_id)
	{
		$this->db->select('ot.tool_id');
		$this->db->from('ordered_asset oa');
		$this->db->join('ordered_tool ot','ot.ordered_tool_id = oa.ordered_tool_id');
		$this->db->where('oa.ordered_asset_id',$ordered_asset_id);
		$res = $this->db->get();
		$result = $res->row_array();
		return $result['tool_id'];
	}

	public function get_check_with_sso($tool_arr,$returnUserCountry)
	{
		$this->db->select('u.sso_id,u.name,u.sso_id');
		$this->db->from('tool_order to');		
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');	
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->join('user u','u.sso_id = to.sso_id');	
		$this->db->where('to.current_stage_id',4);
		$this->db->where_in('u.role_id',array(2,5,6));
		$this->db->where('ot.status',2);
		$this->db->where_in('ot.tool_id',$tool_arr);
		$this->db->where('to.country_id',$returnUserCountry);
		$this->db->order_by('u.sso_id');
		$this->db->group_by('u.sso_id');
		$res = $this->db->get();
		return $res->result_array();
	}
}	