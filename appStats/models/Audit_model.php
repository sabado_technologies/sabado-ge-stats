<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audit_model extends CI_Model {
	
	public function audit_total_num_rows($searchParams,$sent_data)
	{
		$received = explode('~', $sent_data);
		$primary_key = validate_string(storm_decode($received[0]));
		$table_name  = validate_string($received[1]);
		$work_flow   = validate_string($received[2]);
		$main_table  = validate_string($received[3]);
		$main_key    = validate_number($received[4]);

		$this->db->select('ad.ad_id');
		$this->db->from('audit_data ad');
		$this->db->join('user u','u.sso_id = ad.created_by','LEFT');
		$this->db->join('audit_master am','am.am_id = ad.am_id');
        if(@$searchParams['trans_type']!='')
            $this->db->where('ad.trans_type',$searchParams['trans_type']);
        if(@$searchParams['sso_user']!='')
            $this->db->where('ad.created_by',$searchParams['sso_user']);
        if($work_flow!='' && $primary_key !='' && $primary_key !=0)
        {
        	$this->db->where('am.work_flow',$work_flow);
		    $this->db->where('ad.primary_key',$primary_key);
		    $this->db->where('ad.table_name',$table_name);
        }
        else
        {
        	$this->db->where('ad.main_table',$main_table);
			$this->db->where('ad.main_key',$main_key);
			$this->db->where_not_in('ad.am_id',array(26));
        }
		$this->db->where('ad.parent_ad_id',NULL);
		$this->db->order_by('ad.ad_id DESC');
		$this->db->group_by('ad.ad_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function audit_results($current_offset, $per_page, $searchParams,$sent_data)
	{
		$received = explode('~', $sent_data);
		$primary_key = validate_string(storm_decode($received[0]));
		$table_name  = validate_string($received[1]);
		$work_flow   = validate_string($received[2]);
		$main_table  = validate_string($received[3]);
		$main_key    = validate_number($received[4]);
		$this->db->select('CONCAT(u.sso_id,"-(",u.name,")") AS user_name, ad.created_time, ad.ad_id, (CASE WHEN ad.trans_type = 1 THEN "Created" WHEN ad.trans_type = 2 THEN "Modified" ELSE "Removed" END) AS transaction_type, ad.am_id, ad.block_name, am.name as work_flow_name,ad.remarks');
		$this->db->from('audit_data ad');
		$this->db->join('user u','u.sso_id = ad.created_by','LEFT');
		$this->db->join('audit_master am','am.am_id = ad.am_id');
        if(@$searchParams['trans_type']!='')
            $this->db->where('ad.trans_type',$searchParams['trans_type']);
        if(@$searchParams['sso_user']!='')
            $this->db->where('ad.created_by',$searchParams['sso_user']);
		if($work_flow!='' && $primary_key !='' && $primary_key !=0)
        {
        	$this->db->where('am.work_flow',$work_flow);
		    $this->db->where('ad.primary_key',$primary_key);
		    $this->db->where('ad.table_name',$table_name);
        }
        else
        {
        	$this->db->where('ad.main_table',$main_table);
			$this->db->where('ad.main_key',$main_key);
			$this->db->where_not_in('ad.am_id',array(26));
        }
		$this->db->where('ad.parent_ad_id',NULL);
		$this->db->order_by('ad.ad_id DESC');
		$this->db->group_by('ad.ad_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_audit_trail_list($searchParams,$sent_data)
	{
		$received = explode('~', $sent_data);
		$primary_key = validate_string(storm_decode($received[0]));
		$table_name  = validate_string($received[1]);
		$work_flow   = validate_string($received[2]);

		$this->db->select('am.name as work_flow_name, 
			               CONCAT(u.sso_id,"-(",u.name,")") AS user_name,
			               ad.created_time,
			               ad.ad_id,
			               (CASE
    						WHEN ad.trans_type = 1 THEN "Create"
    						WHEN ad.trans_type = 2 THEN "Modify"
    						ELSE "Removed"
							END) AS transaction_type');
		$this->db->from('audit_data ad');
		$this->db->join('audit_child_data acd','acd.ad_id = ad.ad_id');
		$this->db->join('user u','u.sso_id = ad.created_by','LEFT');
		$this->db->join('audit_master am','am.am_id = ad.am_id');
		if(@$searchParams['old_value']!='')
            $this->db->like('acd.old_value',$searchParams['old_value']);
        if(@$searchParams['new_value']!='')
            $this->db->like('acd.new_value',$searchParams['new_value']);
        if(@$searchParams['column_name']!='')
            $this->db->where('acd.column_name',$searchParams['column_name']);
        if(@$searchParams['trans_type']!='')
            $this->db->where('ad.trans_type',$searchParams['trans_type']);
        if(@$searchParams['sso_user']!='')
            $this->db->where('ad.created_by',$searchParams['sso_user']);
		$this->db->where('am.work_flow',$work_flow);
		$this->db->where('ad.primary_key',$primary_key);
		$this->db->where('ad.table_name',$table_name);
		$this->db->order_by('ad.ad_id DESC');
		$this->db->group_by('ad.ad_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_trans_type()
	{
		$trans_arr = array();
		$trans_arr[] = array('trans_id'=>1,'name'=>'Created');
		$trans_arr[] = array('trans_id'=>2,'name'=>'Modified');
		$trans_arr[] = array('trans_id'=>3,'name'=>'Removed');
		return $trans_arr;
	}

	public function get_audit_master_name($ad_id)
	{
		$this->db->select('am.name');
		$this->db->from('audit_data ad');
		$this->db->join('audit_master am','am.am_id = ad.am_id');
		$this->db->where('ad.ad_id',$ad_id);
		$res = $this->db->get();
		$result = $res->row_array();
		return $result['name']; 

	}
	public function get_audit_child_data($ad_id,$searchParams)
	{
		$this->db->select('ad.block_name,ad.remarks,acd.*');
		$this->db->from('audit_data ad');
		$this->db->join('user u','u.sso_id = ad.created_by','LEFT');
		$this->db->join('audit_child_data acd','acd.ad_id = ad.ad_id');
		if(@$searchParams['old_value']!='')
            $this->db->like('acd.old_value',$searchParams['old_value']);
        if(@$searchParams['new_value']!='')
            $this->db->like('acd.new_value',$searchParams['new_value']);
        if(@$searchParams['column_name']!='')
            $this->db->where('acd.column_name',$searchParams['column_name']);
        if(@$searchParams['trans_type']!='')
            $this->db->where('ad.trans_type',$searchParams['trans_type']);
        if(@$searchParams['sso_user']!='')
            $this->db->where('ad.created_by',$searchParams['sso_user']);
		$this->db->where('ad.ad_id',$ad_id);
		$this->db->order_by('ad.ad_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}
}