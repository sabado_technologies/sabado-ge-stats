<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * created by Srilekha on 28th july 2017 04:57PM
*/
class Document_type_m extends CI_Model 
{
	/*Document Type results
    Author:Srilekha*/
	public function documenttype_results($current_offset, $per_page, $searchParams)
	{
		
		$this->db->select('d.*');
		$this->db->from('document_type d');
		if($searchParams['name']!='')
			$this->db->like('d.name',$searchParams['name']);
		if($searchParams['workflow_type']!='')
			$this->db->like('d.workflow_type',$searchParams['workflow_type']);
		$this->db->order_by('d.workflow_type ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	/*Document type number of rows.
    Author:Srilekha*/
	public function documenttype_total_num_rows($searchParams)
	{
		$this->db->select('d.*');
		$this->db->from('document_type d');
		if($searchParams['name']!='')
			$this->db->like('d.name',$searchParams['name']);
		if($searchParams['workflow_type']!='')
			$this->db->like('d.workflow_type',$searchParams['workflow_type']);
		$this->db->order_by('d.workflow_type ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	/*Document Type name Uniqueness.
    Author:Roopa*/
	public function is_documenttypeExist($name,$document_type_id)
	{
		$this->db->select('d.name');
		$this->db->from('document_type d');
		$this->db->where('d.name',$name);
		$this->db->where_not_in('d.document_type_id',$document_type_id);			
		$this->db->where('d.status',1);
		$query = $this->db->get();
		return ($query->num_rows()>0)?1:0;
	}
}	