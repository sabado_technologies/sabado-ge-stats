<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * created by Roopa on 13th june 2017 11:00AM
*/
class Location_m extends CI_Model 
{	
    /*Country results
    Author:Roopa*/
		public function country_results($current_offset, $per_page, $search_params)
		{
			$this->db->select('l.*,c.name as currency_name');
			$this->db->from('location l');
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->join('currency c','c.location_id = l.location_id','LEFT');
			if($search_params['country_name']!='')
				$this->db->like('l.name',$search_params['country_name']);
			if($search_params['currency_name']!='')
				$this->db->like('c.name',$search_params['currency_name']);			
			$this->db->where('tl.level_id',2);
			$this->db->limit($per_page, $current_offset);
			$res = $this->db->get();
			return $res->result_array();
		}
	/*Country number of rows..
    Author:Roopa*/
		public function country_total_num_rows($search_params)
		{
			$this->db->select();
			$this->db->from('location l');
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->join('currency c','c.location_id = l.location_id','LEFT');
			if($search_params['country_name']!='')
				$this->db->like('l.name',$search_params['country_name']);
			if($search_params['currency_name']!='')
				$this->db->like('c.name',$search_params['currency_name']);			
			$this->db->where('tl.level_id',2);
			$res = $this->db->get();
			return $res->num_rows();
		}	
	/*Country Name unique check..
    Author:Roopa*/	
	public function check_country($data)
	{		
		$this->db->select();
		$this->db->from('location l');
		$this->db->where('l.parent_id',1);
		$this->db->where('l.name',$data['name']);		
		if($data['location_id']!='' || $data['location_id']!=0)	
		$this->db->where_not_in('l.location_id',$data['location_id']);
		$query = $this->db->get();
		return ($query->num_rows()>0)?1:0;
	}

	/*Zone results..
    Author:Roopa*/
		public function zone_results($current_offset, $per_page, $searchParams,$task_access)
		{
			$this->db->select('l.location_id, l.name, l1.name as country_name,l.status');
			$this->db->from('location l');
			$this->db->join('location l1','l1.location_id = l.parent_id','left');
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',3);
			if($searchParams['zone_name']!='')
				$this->db->like('l.name',$searchParams['zone_name']);
			if($task_access == 1 || $task_access == 2)
			{
				$this->db->where('l.parent_id',$this->session->userdata('s_country_id'));
			}
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l.parent_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l.parent_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l.parent_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$this->db->limit($per_page, $current_offset);
			$res = $this->db->get();			
			return $res->result_array();
		}
	/*Zone total number of rows..
    Author:Roopa*/
		public function zone_total_rows($searchParams,$task_access)
		{
			$this->db->select('l.location_id, l.name, l1.name as country_name,l.status');
			$this->db->from('location l');
			$this->db->join('location l1','l1.location_id = l.parent_id','left');
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',3);
			if($searchParams['zone_name']!='')
				$this->db->like('l.name',$searchParams['zone_name']);
			if($task_access == 1 || $task_access == 2)
			{
				$this->db->where('l.parent_id',$this->session->userdata('s_country_id'));
			}
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l.parent_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l.parent_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l.parent_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$res = $this->db->get();
			return $res->num_rows();
		}
	/*Zone Name Unique check..
    Author:Roopa*/		
		public function check_zone($data)
		{		
			$this->db->select();
			$this->db->from('location l');
			$this->db->join('location l1','l1.location_id = l.parent_id','LEFT');
			$this->db->where('l.parent_id',$data['parent_id']);
			$this->db->where('l.name',$data['name']);
			$this->db->where('l1.location_id',$data['country_id']);
			if($data['location_id']!='' || $data['location_id']!=0)	
			$this->db->where_not_in('l.location_id',$data['location_id']);
			$query = $this->db->get();
			return ($query->num_rows()>0)?1:0;
		}
	/*State results..
    Author:Roopa*/	
		public function state_results($current_offset, $per_page, $searchParams,$task_access)
		{
			$this->db->select('l.location_id, l.name, concat(l1.name, "," ,l2.name) as zone_name,l.short_name,l.status');
			$this->db->from('location l');//state
			$this->db->join('location l1','l1.location_id = l.parent_id','left');//zone
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//country
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',4);
			if($searchParams['state_name']!='')
				$this->db->like('l.name',$searchParams['state_name']);
			if($searchParams['zone_id']!='')
				{
					$this->db->where('l.parent_id',$searchParams['zone_id']);
				}
			if($task_access == 1 || $task_access == 2)
			{
				$this->db->where('l2.location_id',$this->session->userdata('s_country_id'));
			}
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l2.location_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l2.location_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l2.location_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$this->db->limit($per_page, $current_offset);
			$res = $this->db->get();			
			return $res->result_array();
		}
		
	/*State total number of rows..
    Author:Roopa*/
		public function state_total_num_rows($searchParams,$task_access)
		{
			$this->db->select('l.location_id, l.name, l1.name as zone_name,l.status');
			$this->db->from('location l');//state
			$this->db->join('location l1','l1.location_id = l.parent_id','left');//zone
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//country
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',4);
			if($searchParams['state_name']!='')
				$this->db->like('l.name',$searchParams['state_name']);
			if($searchParams['zone_id']!='')
				{
					$this->db->where('l.parent_id',$searchParams['zone_id']);
				}
			if($task_access == 1 || $task_access == 2)
			{
				$this->db->where('l2.location_id',$this->session->userdata('s_country_id'));
			}
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l2.location_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l2.location_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l2.location_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$res = $this->db->get();
			return $res->num_rows();
		}


	/*State Name unique check..
    Author:Roopa*/	
		public function check_state($data)
		{		
			$this->db->select();
			$this->db->from('location l');//state
			$this->db->join('location l1','l1.location_id = l.parent_id','LEFT');//zone
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//country
			$this->db->where('l.parent_id',$data['parent_id']);
			$this->db->where('l.name',$data['name']);
			$this->db->where('l2.location_id',$data['country_id']);
			if($data['location_id']!='' || $data['location_id']!=0)
			{
				$this->db->where_not_in('l.location_id',$data['location_id']);	
			}		
			$query = $this->db->get();
			return ($query->num_rows()>0)?1:0;
		}

		public function is_shortNameExist($data)
		{		
			$this->db->select();
			$this->db->from('location l');//state
			$this->db->join('location l1','l1.location_id = l.parent_id','LEFT');//zone
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//country
			$this->db->where('l.parent_id',$data['parent_id']);
			$this->db->where('l.short_name',$data['short_name']);
			$this->db->where('l2.location_id',$data['country_id']);
			if($data['location_id']!='' || $data['location_id']!=0)
			{
				$this->db->where_not_in('l.location_id',$data['location_id']);	
			}		
			$this->db->where('l.status',1);
			$query = $this->db->get();
			return ($query->num_rows()>0)?1:0;
		}

    /*City results..
    Author:Roopa*/
		public function city_results($current_offset, $per_page, $searchParams,$task_access)
		{
			$this->db->select('l.location_id, l.name, l1.name as state_name,l2.name as zone_name, l3.name as country_name,l.status');
			$this->db->from('location l');//city
			$this->db->join('location l1','l1.location_id = l.parent_id','left');//state
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//zone
			$this->db->join('location l3','l3.location_id = l2.parent_id','LEFT');//country
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',5);
			if($searchParams['city_name']!='')
				$this->db->like('l.name',$searchParams['city_name']);
			if($searchParams['state_id']!='')
			{
				$this->db->where('l1.location_id',$searchParams['state_id']);
			}
			if($searchParams['zone_id']!='')
			{
				$this->db->where('l2.location_id',$searchParams['zone_id']);
			}
			if($searchParams['country_id']!='')
			{
				$this->db->where('l3.location_id',$searchParams['country_id']);
			}
			if($task_access == 1 || $task_access == 2)
			{
				$this->db->where('l3.location_id',$this->session->userdata('s_country_id'));
			}		
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l3.location_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l3.location_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l3.location_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$this->db->limit($per_page, $current_offset);
			$res = $this->db->get();			
			return $res->result_array();
		}
	/*City total number of rows..
    Author:Roopa*/
		public function city_total_num_rows($searchParams,$task_access)
		{
			$this->db->select('l.location_id, l.name, l1.name as state_name,l2.name as zone_name, l3.name as country_name,l.status');
			$this->db->from('location l');//city
			$this->db->join('location l1','l1.location_id = l.parent_id','left');//state
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//zone
			$this->db->join('location l3','l3.location_id = l2.parent_id','LEFT');//country
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',5);
			if($searchParams['city_name']!='')
				$this->db->like('l.name',$searchParams['city_name']);
			if($searchParams['state_id']!='')
			{
				$this->db->where('l1.location_id',$searchParams['state_id']);
			}
			if($searchParams['zone_id']!='')
			{
				$this->db->where('l2.location_id',$searchParams['zone_id']);
			}
			if($searchParams['country_id']!='')
			{
				$this->db->where('l3.location_id',$searchParams['country_id']);
			}
			if($task_access == 1 || $task_access == 2)
			{
				$this->db->where('l3.location_id',$this->session->userdata('s_country_id'));
			}
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l3.location_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l3.location_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l3.location_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$res = $this->db->get();
			return $res->num_rows();
		}
	/*City Name Unique check..
    Author:Roopa*/
		public function check_city($data)
		{		
			$this->db->select();
			$this->db->from('location l');//city
			$this->db->join('location l1','l1.location_id = l.parent_id','LEFT');//state
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//zone
			$this->db->join('location l3','l3.location_id = l2.parent_id','LEFT');//country
			$this->db->where('l.parent_id',$data['parent_id']);
			$this->db->where('l.name',$data['name']);
			$this->db->where('l3.location_id',$data['country_id']);
			if($data['location_id']!='' || $data['location_id']!=0)
			{
				$this->db->where_not_in('l.location_id',$data['location_id']);	
			}		
			$query = $this->db->get();
			return ($query->num_rows()>0)?1:0;
		}
	/*Area results..
    Author:Roopa*/
		public function area_results($current_offset, $per_page, $searchParams,$task_access)
		{
			$this->db->select('l.location_id, l.name, l1.name as city_name,l2.name as state_name,l3.name as zone_name, l4.name as country_name, l.status');
			$this->db->from('location l');//area
			$this->db->join('location l1','l1.location_id = l.parent_id','left');//city
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//state
			$this->db->join('location l3','l3.location_id = l2.parent_id','LEFT');//zone
			$this->db->join('location l4','l4.location_id = l3.parent_id','LEFT');//country
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',6);
			if($searchParams['area_name']!='')
				$this->db->like('l.name',$searchParams['area_name']);
			if($searchParams['city_id']!='')
			{
				$this->db->where('l1.location_id',$searchParams['city_id']);
			}
			if($searchParams['state_id']!='')
			{
				$this->db->where('l2.location_id',$searchParams['state_id']);
			}
			if($searchParams['zone_id']!='')
			{
				$this->db->where('l3.location_id',$searchParams['zone_id']);
			}
			if($searchParams['country_id']!='')
			{
				$this->db->where('l4.location_id',$searchParams['country_id']);
			}
			if($task_access == 1 || $task_access == 2)
			{
				$this->db->where('l4.location_id',$this->session->userdata('s_country_id'));
			}
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l4.location_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l4.location_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l4.location_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$this->db->limit($per_page, $current_offset);
			$res = $this->db->get();			
			return $res->result_array();
		}
	/*Area total number of rows..
    Author:Roopa*/
		public function area_total_num_rows($searchParams,$task_access)
		{
			$this->db->select('l.location_id');
			$this->db->from('location l');//area
			$this->db->join('location l1','l1.location_id = l.parent_id','left');//city
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//state
			$this->db->join('location l3','l3.location_id = l2.parent_id','LEFT');//zone
			$this->db->join('location l4','l4.location_id = l3.parent_id','LEFT');//country
			$this->db->join('territory_level tl ','tl.level_id = l.level_id');
			$this->db->where('tl.level_id',6);
			if($searchParams['area_name']!='')
				$this->db->like('l.name',$searchParams['area_name']);
			if($searchParams['city_id']!='')
			{
				$this->db->where('l1.location_id',$searchParams['city_id']);
			}
			if($searchParams['state_id']!='')
			{
				$this->db->where('l2.location_id',$searchParams['state_id']);
			}
			if($searchParams['zone_id']!='')
			{
				$this->db->where('l3.location_id',$searchParams['zone_id']);
			}
			if($searchParams['country_id']!='')
			{
				$this->db->where('l4.location_id',$searchParams['country_id']);
			}
			if($task_access == 1 || $task_access == 2)
			{
				$this->db->where('l4.location_id',$this->session->userdata('s_country_id'));
			}
			else if($task_access == 3)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('l4.location_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('l4.location_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('l4.location_id',$this->session->userdata('countriesIndexedArray'));	
					}
				}
			}
			$res = $this->db->get();
			return $res->num_rows();
		}
	/*Area Name Unique check..
    Author:Roopa*/
		public function check_area($data)
		{		
			$this->db->select();
			$this->db->from('location l');//area
			$this->db->join('location l1','l1.location_id = l.parent_id','left');//city
			$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');//state
			$this->db->join('location l3','l3.location_id = l2.parent_id','LEFT');//zone
			$this->db->join('location l4','l4.location_id = l3.parent_id','LEFT');//country
			$this->db->where('l.parent_id',$data['parent_id']);
			$this->db->where('l.name',$data['name']);
			$this->db->where('l4.location_id',$data['country_id']);
			if($data['location_id']!='' || $data['location_id']!=0)
			{
				$this->db->where_not_in('l.location_id',$data['location_id']);	
			}		
			$query = $this->db->get();
			return ($query->num_rows()>0)?1:0;
		}

	/*koushik*/
	public function get_zone_by_country($task_access,$country_id = 0)
	{
		$this->db->select('concat(l.name, "," ,l1.name) as name, l.location_id');
		$this->db->from('location l');//zone
		$this->db->join('location l1','l1.location_id = l.parent_id');//country
		$this->db->where('l.level_id',3);
		if($country_id != '' || $country_id != 0)
		{
			$this->db->where('l1.location_id',$country_id);
		}
		else if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l1.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l1.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				$this->db->where_in('l1.location_id',$this->session->userdata('countriesIndexedArray'));	
			}
		}

		$res = $this->db->get();
		return $res->result_array();
	}	

	/*koushik*/
	public function get_state_by_country($task_access,$country_id = 0)
	{
		$this->db->select('concat(l.name, "," ,l1.name) as name, l.location_id');
		$this->db->from('location l');//state
		$this->db->join('location l1','l1.location_id = l.parent_id');//zone
		$this->db->join('location l2','l2.location_id = l1.parent_id');//country
		$this->db->where('l.level_id',4);
		if($country_id != '' || $country_id != 0)
		{
			$this->db->where('l2.location_id',$country_id);
		}
		else if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l2.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l2.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				$this->db->where_in('l2.location_id',$this->session->userdata('countriesIndexedArray'));	
			}
		}

		$res = $this->db->get();
		return $res->result_array();
	}

	/*koushik*/
	public function get_city_by_country($task_access,$country_id = 0)
	{
		$this->db->select('concat(l.name, "," ,l1.name) as name, l.location_id');
		$this->db->from('location l');//city
		$this->db->join('location l1','l1.location_id = l.parent_id');//state
		$this->db->join('location l2','l2.location_id = l1.parent_id');//zone
		$this->db->join('location l3','l3.location_id = l2.parent_id');//country
		$this->db->where('l.level_id',5);
		if($country_id != '' || $country_id != 0)
		{
			$this->db->where('l3.location_id',$country_id);
		}
		else if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l3.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l3.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				$this->db->where_in('l3.location_id',$this->session->userdata('countriesIndexedArray'));	
			}
		}

		$res = $this->db->get();
		return $res->result_array();
	}
}	