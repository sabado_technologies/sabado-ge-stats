<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Replacement_m extends CI_Model
{
	/*Asset Details.
    Author:Srilekha
    Time:1.06PM Date:2-08-2017*/
	public function asset_details_view($asset_id)
	{
		$this->db->select('t.part_description,t.part_number,t.tool_code,
			p.serial_number,a.cost_in_inr,ac.name as asset_condition_name,p.part_id,
			p.remarks as part_remarks,t.length,t.breadth,t.height,a.*,t.kit as kit,m.name as modality_name,
			tt.name as tool_type_name,wh.wh_code as warehouse_code,
			a.sub_inventory as sub_inventory,at.name as asset_type,
			s.name as supplier_name,a.remarks as asset_remarks,
			ct.name as calibration,cs.name as cs_name,a.cal_due_date as due_date,
			t.manufacturer,a.po_number as po,a.earpe_number as earpe,
			a.date_of_use as date_of_used,ad.name as document,
			t.part_number as part_num,t.tool_code as toolcode,
			t.part_description as description,p.serial_number as serialnumber,
			p.remarks as remark,p.status as statuss,t.length as p_length,
			t.breadth as p_breadth,t.height as p_height,a.cost as p_cost,
			al.name as asset_level_name,p.quantity');
		$this->db->from('asset a');
		$this->db->join('part p','a.asset_id = p.asset_id');
		$this->db->join('tool t','p.tool_id =t.tool_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id =t.tool_type_id');
		$this->db->join('asset_type at','at.asset_type_id =t.asset_type_id');
		$this->db->join('asset_document ad','ad.asset_id =a.asset_id','left');		
		$this->db->join('modality m','m.modality_id =a.modality_id ');
		$this->db->join('supplier cs','cs.supplier_id = a.cal_supplier_id','left');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('supplier s','s.supplier_id=t.supplier_id');
		$this->db->join('warehouse wh','a.wh_id = wh.wh_id');
		$this->db->join('asset_status as','as.asset_status_id =a.status ');
		$this->db->join('part_level pl','pl.part_level_id= p.part_level_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = p.status');
		$this->db->where('p.asset_id',$asset_id);
		$this->db->where('p.part_level_id',1);
		$res = $this->db->get();
		return $res->row_array();
	}
	/*L1 Asset Details.
    Author:Srilekha
    Time:1.08PM Date:2-08-2017*/
	public function get_sub_component_details($asset_id)
	{
		$this->db->select('t.part_number,t.part_description,p.quantity,p.serial_number,p.part_id,t.tool_id');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('p.part_level_id',2);
		$this->db->order_by('p.part_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_serial_number($tool_id,$part_id,$country_id)
	{
		$this->db->select('asset_id');
		$this->db->from('rc_asset');
		$this->db->where('current_stage_id>=',11);
		$this->db->where('current_stage_id<=',16);
		$res=$this->db->get();
		$result = $res->result_array();
		$avoid = array();
		if(count($result)>0)
		{
			foreach ($result as $key => $value) 
			{
				$avoid[] = $value['asset_id'];
			}
		}
		else
		{
			$avoid = array(0);
		}
		$status = array(1,2);
		$this->db->select('p.part_id,p.serial_number,a.asset_id,concat(wh.wh_code, "-(" ,wh.name,")") as wh_name,t.tool_id,a.asset_number');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('a.country_id',$country_id);
		$this->db->where('a.availability_status',1);
		$this->db->where_in('a.status',$status);
		$this->db->where_not_in('p.part_id',$part_id);
		$this->db->where_not_in('a.asset_id',$avoid);
		$this->db->where('p.part_level_id',2);//09-01-2019
		$this->db->order_by('p.part_id ASC');
		$res= $this->db->get();
		return $res->result_array();
	}
	public function asset_country($defective_asset_id)
	{
		$this->db->select('a.country_id');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','da.asset_id=a.asset_id');
		$this->db->where('da.defective_asset_id',$defective_asset_id);
		$res= $this->db->get();
		return $res->row_array();
		//$this->db->join('location l','l.location_id=a.country_id');
	}

	public function get_sub_component_tool($tool_id,$defective_asset_id)
	{
		$this->db->select('dah.asset_condition_id,t.part_number,t.part_description,p.serial_number,p.quantity,p.part_id');
		$this->db->from('defective_asset_health dah');
		$this->db->join('part p','p.part_id = dah.part_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('dah.defective_asset_id',$defective_asset_id);
		$this->db->where('p.tool_id',$tool_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	/*Closed Replaced Tool no of rows
    Author:Srilekha
    Time:09.12PM Date:2-08-2017*/
    public function closed_tool_total_num_rows($searchParams,$task_access)
    {
    	$this->db->select('na.asset_number as new_asset_number,
    					   r.created_time,r.reason,
    					   t.part_number,t.part_description,
    					   a.asset_number as old_asset_number,
    					   p.serial_number as old_serial_number,
    					   np.serial_number as new_serial_number,l.name as country_name');
    	$this->db->from('replacement r');
    	$this->db->join('replaced_tool rt','rt.replacement_id = r.replacement_id');
    	$this->db->join('part p','p.part_id = rt.old_part_id');
    	$this->db->join('tool t','p.tool_id = t.tool_id');
    	$this->db->join('part np','np.part_id = rt.new_part_id');
    	$this->db->join('asset a','a.asset_id = r.asset_id');
    	$this->db->join('asset na','na.asset_id = rt.new_asset_id');
    	$this->db->join('location l','l.location_id = a.country_id');
    	if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['serial_number']!='')
			$this->db->like('p.serial_number',$searchParams['serial_number']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$this->session->userdata('header_country_id'));
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('a.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$this->session->userdata('countriesIndexedArray'));
				}
			}
		}
		$this->db->order_by('r.replacement_id DESC');
		$res=$this->db->get();
		return $res->num_rows();
    }
    /*Closed Replaced Tool results
    Author:Srilekha
    Time:09.26PM Date:2-08-2017*/
    public function closed_tool_results($current_offset, $per_page, $searchParams,$task_access)
    {
    	$this->db->select('na.asset_number as new_asset_number,
    					   r.created_time,r.reason,
    					   t.part_number,t.part_description,
    					   a.asset_number as old_asset_number,
    					   p.serial_number as old_serial_number,
    					   np.serial_number as new_serial_number,l.name as country_name');
    	$this->db->from('replacement r');
    	$this->db->join('replaced_tool rt','rt.replacement_id = r.replacement_id');
    	$this->db->join('part p','p.part_id = rt.old_part_id');
    	$this->db->join('tool t','p.tool_id = t.tool_id');
    	$this->db->join('part np','np.part_id = rt.new_part_id');
    	$this->db->join('asset a','a.asset_id = r.asset_id');
    	$this->db->join('asset na','na.asset_id = rt.new_asset_id');
    	$this->db->join('location l','l.location_id = a.country_id');
    	if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['serial_number']!='')
			$this->db->like('p.serial_number',$searchParams['serial_number']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$this->session->userdata('header_country_id'));
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('a.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$this->session->userdata('countriesIndexedArray'));
				}
			}
		}
		$this->db->order_by('r.replacement_id DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
    }
}