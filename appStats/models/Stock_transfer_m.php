<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock_transfer_m extends CI_Model 
{

	/*koushik 16-08-2018*/	
	public function STforOrder_tool_total_num_rows($searchParams,$task_access)
	{
		$tool_order_id_arr = NeedStockTransferOrWhStockTransferApprovalIds($task_access);
		$str ='(to.order_number ="'.$searchParams['st_order_number'].'" OR to.stn_number = "'.$searchParams['st_order_number'].' " )';
		$this->db->select('to.*,oda.*,l.name as country_name');
		$this->db->from('tool_order to');	
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('st_tool_order sto','to.tool_order_id = sto.tool_order_id','left');
		$this->db->join('tool_order stn','sto.stock_transfer_id = stn.tool_order_id','left');
		$this->db->join('user u','to.sso_id = u.sso_id');
		$this->db->join('location l','l.location_id = to.country_id','LEFT');				
		if(@$searchParams['st_order_number']!='')
			$this->db->where($str);
		if(@$searchParams['st_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['st_sso_id']);
		if(@$searchParams['st_request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['st_request_date']));
		if(@$searchParams['st_return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['st_return_date']));
		$this->db->where('to.current_stage_id',4);	
		$this->db->where_in('to.order_type',array(1,2));
		$this->db->where('oda.check_address',0);
		$this->db->where('to.fe_check',0);
		$this->db->where_in('to.tool_order_id',$tool_order_id_arr);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			else
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['st_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['st_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('to.order_type ASC, to.country_id ASC,to.tool_order_id ASC');
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

    // List Of Orderes Waiting For Admin
    public function STforOrder_tool_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$tool_order_id_arr = NeedStockTransferOrWhStockTransferApprovalIds($task_access);
		$str ='(to.order_number ="'.$searchParams['st_order_number'].'" OR to.stn_number = "'.$searchParams['st_order_number'].' " )';
		$this->db->select('to.*,oda.*,l.name as country_name');
		$this->db->from('tool_order to');	
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('st_tool_order sto','to.tool_order_id = sto.tool_order_id','left');
		$this->db->join('tool_order stn','sto.stock_transfer_id = stn.tool_order_id','left');
		$this->db->join('user u','to.sso_id = u.sso_id');
		$this->db->join('location l','l.location_id = to.country_id','LEFT');				
		if(@$searchParams['st_order_number']!='')
			$this->db->where($str);
		if(@$searchParams['st_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['st_sso_id']);
		if(@$searchParams['st_request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['st_request_date']));
		if(@$searchParams['st_return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['st_return_date']));
		$this->db->where('to.current_stage_id',4);	
		$this->db->where_in('to.order_type',array(1,2));
		$this->db->where('oda.check_address',0);
		$this->db->where('to.fe_check',0);
		$this->db->where_in('to.tool_order_id',$tool_order_id_arr);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			else
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['st_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['st_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('to.order_type ASC, to.country_id ASC,to.tool_order_id ASC');
		$this->db->group_by('to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_ordered_tool_info($tool_order_id,$onlyStatus=0,$available_quantity = 0)
	{
		$this->db->select('t.part_number,t.part_description,odt.quantity,odt.available_quantity,
			odt.ordered_tool_id,odt.tool_id,odt.status,to.tool_order_id,to.sso_id,to.wh_id,to.order_delivery_type_id');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool odt','to.tool_order_id = odt.tool_order_id');
		$this->db->join('tool t','t.tool_id = odt.tool_id');
		$this->db->where('odt.tool_order_id',$tool_order_id);
		if($onlyStatus == 0)
			$this->db->where_in('odt.status',array(1,2));
		else
			$this->db->where('odt.status',$onlyStatus);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	// Raised ST Details For Order
    public function raisedSTforOrderResults($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,oda.*,stn.fe_check as sto_fe_check,cs.name as cs_name,concat(u.sso_id, "-(" ,u.name,")") as sso,l.name as countryName');
		$this->db->from('tool_order to');	
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['st_type'] == 2)
		{	
			$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
			$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','left');
			if($searchParams['order_number']!='')
				$this->db->like('to.order_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',2);
			$this->db->where('to.current_stage_id',4);
			$this->db->where('sto.status<',2);
			$this->db->group_by('sto.tool_order_id');
		}
		else
		{
			$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','left');
			$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','left');
			if($searchParams['order_number']!='')
				$this->db->like('to.stn_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',1);
			$this->db->where('sto.tool_order_id IS NULL');
			$this->db->where('to.current_stage_id<',3);
		}
		if($searchParams['st_sso_id']!='')
				$this->db->where('to.sso_id',$searchParams['st_sso_id']);		
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['st_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['st_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function raisedSTforOrderTotalRows($searchParams,$task_access)
	{
		$this->db->select('to.*,oda.*,stn.fe_check as sto_fe_check,cs.name as cs_name,concat(u.sso_id, "-(" ,u.name,")") as sso,l.name as countryName');
		$this->db->from('tool_order to');	
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = to.country_id');
		
		if($searchParams['st_type'] == 2)
		{	
			$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
			$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','left');
			if($searchParams['order_number']!='')
				$this->db->like('to.order_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',2);
			$this->db->where('to.current_stage_id',4);
			$this->db->where('sto.status<',2);
			//$this->db->where('sto.status<',3);
			$this->db->group_by('sto.tool_order_id');
		}
		else
		{
			$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','left');
			$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','left');
			if($searchParams['order_number']!='')
				$this->db->like('to.stn_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',1);
			$this->db->where('sto.tool_order_id IS NULL');
			$this->db->where('to.current_stage_id<',3);
		}
		if($searchParams['st_sso_id']!='')
				$this->db->where('to.sso_id',$searchParams['st_sso_id']);		
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['st_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['st_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		//$this->db->where('a.status',1);
		$res = $this->db->get();
		/*//echo $this->db->last_query();exit;
		echo '<pre>';print_r($res->result_array());exit;*/
		return $res->num_rows();
	}

		// Raised ST Details For Order
	public function closedSTforOrderResults($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,concat(u.sso_id, "-(" ,u.name,")") as sso,cs.name as cs_name,l.name as countryName,oda.*');
		$this->db->from('tool_order to');	
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('location l','l.location_id = to.country_id');
		$this->db->join('order_address oda','oda.tool_order_id =to.tool_order_id');
		if($searchParams['st_type'] == 2)
		{
			$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
			if($searchParams['order_number']!='')
				$this->db->like('to.order_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',2);
			$this->db->where('to.current_stage_id>',4);
			//$this->db->where('sto.status',3);
			$this->db->group_by('sto.tool_order_id');
		}
		else
		{
			$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','left');
			
			if($searchParams['order_number']!='')
				$this->db->like('to.order_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',1);
			$this->db->where('sto.tool_order_id IS NULL');
			$this->db->where('to.current_stage_id',3);
		}
		if($searchParams['cst_sso_id']!='')
				$this->db->where('to.sso_id',$searchParams['cst_sso_id']);		
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['cst_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['cst_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		//$this->db->where('a.status',1);
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		//echo $this->
		//echo $this->db->last_query();exit;
		//echo '<pre>';print_r($res->result_array());exit;
		return $res->result_array();
	}
	
	public function closedSTforOrderTotalRows($searchParams,$task_access)
	{
		$this->db->select('to.*');
		$this->db->from('tool_order to');	
		if($searchParams['st_type'] == 2)
		{
			$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
			if($searchParams['order_number']!='')
				$this->db->like('to.order_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',2);
			$this->db->where('to.current_stage_id>',4);
			//$this->db->where('sto.status',3);
			$this->db->group_by('sto.tool_order_id');
		}
		else
		{
			$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','left');			
			if($searchParams['order_number']!='')
				$this->db->like('to.stn_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',1);
			$this->db->where('sto.tool_order_id IS NULL');
			$this->db->where('to.current_stage_id',3);
		}
		if($searchParams['cst_sso_id']!='')
				$this->db->where('to.sso_id',$searchParams['cst_sso_id']);		
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['cst_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['cst_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		//$this->db->where('a.status',1);
		$res = $this->db->get();
		//echo $this->db->last_query();exit;
		//echo '<pre>';print_r($res->result_array());exit;*/
		return $res->num_rows();
	}


	public function stnForOrder($tool_order_id,$fe_check=0,$onlyInTransitForFeAckCount=0)
	{
		$this->db->select('to.*,oa.address1,oa.address2,oa.address3,oa.address4,oa.pin_code,concat(wh.wh_code, "-(" ,wh.name,")") as wh,cs.name as cs_name');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id AND oa.status = 1','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->where('sto.tool_order_id',$tool_order_id);
		if($fe_check == 1)
		{
			$this->db->where('to.fe_check',1);
			if($onlyInTransitForFeAckCount ==0)
			{
				$str = '(to.current_stage_id =2 OR ((to.current_stage_id =3 AND to.status = 5) OR (to.current_stage_id =3 AND to.status = 6) OR (to.current_stage_id =3 AND to.status = 10) ) )';
			}
			else
			{
				$str = '(to.current_stage_id = 2)';
			}
        	$this->db->where($str);
		}
		$res = $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->result_array();
	}

	public function wh2_wh_receive_order_results($current_offset, $per_page, $searchParams,$task_access)
    {
        
        $this->db->select('to.*,
                           concat(wh.wh_code, "-(" ,wh.name,")") as from_wh_name,
                           concat(wh_to.wh_code, "-(" ,wh_to.name,")") as to_wh_name,od.expected_delivery_date');
        $this->db->from('tool_order to');  
        $this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');     
        $this->db->join('warehouse wh','wh.wh_id = to.wh_id');
        $this->db->join('warehouse wh_to','wh_to.wh_id = to.to_wh_id','LEFT');
        if($searchParams['r_stn_number']!='')
            $this->db->like('to.stn_number',$searchParams['r_stn_number']);
        if($searchParams['r_wh_id']!='')
            $this->db->where('to.wh_id',$searchParams['r_wh_id']);      
    
        if($task_access==1)
        {
            if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['to_wh_id']=='')
				{
					$this->db->where_in('to.to_wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.to_wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
        }
        elseif($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            if($searchParams['to_wh_id']!='')
            {
                $this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
            }
        }
        else if($task_access == 3)
        {
            if($_SESSION['header_country_id']!='')
            {
                $this->db->where('to.country_id',$_SESSION['header_country_id']);   
            }
            else
            {
                if($searchParams['country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                }
            }
            if($searchParams['to_wh_id']!='')
            {
                $this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
            }
        }       
        $this->db->where('to.current_stage_id',2);          
        $this->db->where('to.order_type',1);
        $this->db->where('to.fe_check',0);  
        $this->db->limit($per_page, $current_offset);
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function wh2_wh_receive_order_total_num_rows($searchParams,$task_access)
    {
        $this->db->select('to.*');
        $this->db->from('tool_order to');   
        $this->db->join('warehouse wh','wh.wh_id = to.wh_id');  
        $this->db->join('warehouse wh_to','wh_to.wh_id = to.to_wh_id','LEFT');
        if(@$searchParams['r_stn_number']!='')
            $this->db->like('to.stn_number',$searchParams['r_stn_number']);
        if(@$searchParams['r_wh_id']!='')
            $this->db->where('to.wh_id',$searchParams['r_wh_id']);      
        
        if($task_access==1)
        {
            if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['to_wh_id']=='')
				{
					$this->db->where_in('to.to_wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.to_wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
        }
        elseif($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            if($searchParams['to_wh_id']!='')
            {
                $this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
            }
        }
        else if($task_access == 3)
        {
            if($_SESSION['header_country_id']!='')
            {
                $this->db->where('to.country_id',$_SESSION['header_country_id']);   
            }
            else
            {
                if($searchParams['country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                }
            }
            if($searchParams['to_wh_id']!='')
            {
                $this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
            }
        }       
        $this->db->where('to.current_stage_id',2);          
        $this->db->where('to.order_type',1);
        $this->db->where('to.fe_check',0);  
        $res = $this->db->get();
        return $res->num_rows();
    }

	public function get_warehouse_list($task_access,$searchParams)
	{
		
		$this->db->select('wh_id,name,wh_code');
		$this->db->from('warehouse');
		$this->db->where('status',1);
        if($task_access==1)
		{
			$this->db->where('country_id',$this->session->userdata('s_country_id'));
			$swh_id = $this->session->userdata('swh_id');
			$this->db->where_not_in('wh_id',$swh_id);
		}
		elseif($task_access == 2)
		{
			$this->db->where('country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
		}
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_actual_order_info($tool_order_id)
	{
		$this->db->select('to.*,concat(u.sso_id, "-(" ,u.name,")") as sso');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('sto.stock_transfer_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function wh_return_initialted_assets($tool_order_id)
	{
		$this->db->select('osh.tool_order_id as fe1_tool_order_id, osh.order_status_id as fe1_order_status_id, oah.*, a.asset_number, a.asset_id, t.part_number, t.part_description, as.name as asset_status, oa.ordered_tool_id, p.serial_number');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_condition as','as.asset_condition_id = osh.status');
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_return_info($tool_order_id)
	{
		$this->db->select('to.*,od.*');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_tool_id_by($ordered_asset_id)
	{
		$this->db->select('ot.*');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->where('oa.ordered_asset_id',$ordered_asset_id);
		$this->db->where('ot.status<',3);
		$res = $this->db->get();
		$result =  $res->row_array();
		return $result['tool_id'];
	}

	public function wh2_wh_closed_order_results($current_offset, $per_page, $searchParams,$task_access)
	{
		
		$this->db->select('to.*,concat(wh.wh_code, "-(" ,wh.name,")") as from_wh_name,concat(wh_to.wh_code, "-(" ,wh_to.name,")") as to_wh_name');
		$this->db->from('tool_order to');	
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');	
		$this->db->join('warehouse wh_to','wh_to.wh_id = to.to_wh_id','LEFT');	
		if($searchParams['stn_number']!='')
			$this->db->like('to.stn_number',$searchParams['stn_number']);
		if($searchParams['wh_id']!='')
			$this->db->where('to.wh_id',$searchParams['wh_id']);		
		
		if($task_access==1)
		{
			if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['to_wh_id']=='')
				{
					$this->db->where_in('to.to_wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.to_wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
		}		
		$this->db->where('to.current_stage_id=',3);			
		$this->db->where('to.order_type',1);	
		$this->db->where('to.fe_check',0);  	
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function wh2_wh_closed_order_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.*');
		$this->db->from('tool_order to');	
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');	
		$this->db->join('warehouse wh_to','wh_to.wh_id = to.to_wh_id','LEFT');	
		if($searchParams['stn_number']!='')
			$this->db->like('to.stn_number',$searchParams['stn_number']);
		if($searchParams['wh_id']!='')
			$this->db->where('to.wh_id',$searchParams['wh_id']);		
		
		if($task_access==1)
		{
			if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['to_wh_id']=='')
				{
					$this->db->where_in('to.to_wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.to_wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
		}		
		$this->db->where('to.current_stage_id=',3);			
		$this->db->where('to.order_type',1);
		$this->db->where('to.fe_check',0);  
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function getOldFeCheck($tool_order_id)
	{
		$this->db->select('stn.fe_check');
		$this->db->from('st_tool_order sto');
		$this->db->join('tool_order stn','sto.stock_transfer_id = stn.tool_order_id');
		$this->db->where('sto.tool_order_id',$tool_order_id);
		$this->db->where('sto.status',1);
		$res = $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->result_array();
	}

	public function downloadSTforOrderResults($searchParams,$task_access)
	{
		$this->db->select('to.*,oda.*,stn.fe_check as sto_fe_check,cs.name as cs_name,concat(u.sso_id, "-(" ,u.name,")") as sso,l.name as countryName');
		$this->db->from('tool_order to');	
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['st_type'] == 2)
		{	
			$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
			$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','left');
			if($searchParams['order_number']!='')
				$this->db->like('to.order_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',2);
			$this->db->where('to.current_stage_id',4);
			$this->db->where('sto.status<',2);
			//$this->db->where('sto.status<',3);
			$this->db->group_by('sto.tool_order_id');
		}
		else
		{
			$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','left');
			$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','left');
			if($searchParams['order_number']!='')
				$this->db->like('to.stn_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',1);
			$this->db->where('sto.tool_order_id IS NULL');
			$this->db->where('to.current_stage_id<',3);
		}

		if($searchParams['st_sso_id']!='')
				$this->db->where('to.sso_id',$searchParams['st_sso_id']);		
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['st_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['st_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_ordered_tools($tool_order_id)
	{
		$this->db->select('t.*,a.asset_number,to.current_stage_id,to.tool_order_id,
			as.name as status_name,a.status as asset_status_id,a.asset_id,ot.ordered_tool_id,oa.ordered_asset_id,GROUP_CONCAT(a.asset_number SEPARATOR ",") as assets,ot.quantity,ot.available_quantity');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('tool_order to','to.tool_order_id= ot.tool_order_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->where('p.part_level_id',1);
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('ot.status<',3);
		$this->db->where('oa.status<',3);
		$this->db->group_by('ot.ordered_tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_ordered_tools_for_tool_order($tool_order_id)
	{
		$this->db->select('to.tool_order_id, t.*,to.current_stage_id,to.tool_order_id,ot.ordered_tool_id,
			               ot.quantity,ot.available_quantity');
		$this->db->from('ordered_tool ot');
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->join('tool_order to','to.tool_order_id= ot.tool_order_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('ot.status<',3);
		$this->db->group_by('ot.ordered_tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}


	public function downloadclosedSTResults($searchParams,$task_access)
	{
		$this->db->select('to.*,concat(u.sso_id, "-(" ,u.name,")") as sso,cs.name as cs_name,l.name as countryName,oda.*');
		$this->db->from('tool_order to');	
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('location l','l.location_id = to.country_id');
		$this->db->join('order_address oda','oda.tool_order_id =to.tool_order_id');
		if($searchParams['st_type'] == 2)
		{
			$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
			if($searchParams['order_number']!='')
				$this->db->like('to.order_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',2);
			$this->db->where('to.current_stage_id>',4);
			//$this->db->where('sto.status',3);
			$this->db->group_by('sto.tool_order_id');
		}
		else
		{
			$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','left');
			
			if($searchParams['order_number']!='')
				$this->db->like('to.order_number',$searchParams['order_number']);		
			if($searchParams['to_wh_id']!='')
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);		
			$this->db->where('to.order_type',1);
			$this->db->where('sto.tool_order_id IS NULL');
			$this->db->where('to.current_stage_id',3);
		}
		if($searchParams['cst_sso_id']!='')
				$this->db->where('to.sso_id',$searchParams['cst_sso_id']);		
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['cst_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['cst_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$res = $this->db->get();
		return $res->result_array();
	}


	public function get_closed_st_assets($tool_order_id)
	{
		$this->db->select('t.part_number,t.part_description,pl.name as part_level,p.serial_number,p.quantity as quantity,t.tool_code,
			tt.name as tool_type,ac.name as ack_status,GROUP_CONCAT(a.asset_number SEPARATOR ",") as assets,count(oa.ordered_asset_id) as request_quantity');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = oah.status');
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$this->db->where('osh.current_stage_id',3);
		$this->db->group_by('osh.order_status_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function download_open_st_returns($searchParams,$task_access)
    {
        
        $this->db->select('to.tool_order_id,to.stn_number,to.country_id,
                           concat(wh.wh_code, "-(" ,wh.name,")") as from_wh_name,
                           concat(wh_to.wh_code, "-(" ,wh_to.name,")") as to_wh_name,od.expected_delivery_date');
        $this->db->from('tool_order to');
        $this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');
        $this->db->join('warehouse wh','wh.wh_id = to.wh_id');
        $this->db->join('warehouse wh_to','wh_to.wh_id = to.to_wh_id','LEFT');
        if($searchParams['r_stn_number']!='')
            $this->db->like('to.stn_number',$searchParams['r_stn_number']);
        if($searchParams['r_wh_id']!='')
            $this->db->where('to.wh_id',$searchParams['r_wh_id']);      
    
        if($task_access==1)
        {
            if($searchParams['to_wh_id']!='')
			{
				$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['to_wh_id']=='')
				{
					$this->db->where_in('to.to_wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.to_wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
        }
        elseif($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            if($searchParams['to_wh_id']!='')
            {
                $this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
            }
        }
        else if($task_access == 3)
        {
            if($_SESSION['header_country_id']!='')
            {
                $this->db->where('to.country_id',$_SESSION['header_country_id']);   
            }
            else
            {
                if($searchParams['country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                }
            }
            if($searchParams['to_wh_id']!='')
            {
                $this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
            }
        }       
        $this->db->where('to.current_stage_id',2);          
        $this->db->where('to.order_type',1);
        $this->db->where('to.fe_check',0);
        $res = $this->db->get();
        return $res->result_array();
    }

    public function get_assets_in_open_st_returns($tool_order_id)
	{
		$this->db->select('t.part_number, t.part_description, a.asset_number,as.name as asset_status, a.asset_id');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('tool_order to','to.tool_order_id= ot.tool_order_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->where('p.part_level_id',1);
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('ot.status<',3);
		$this->db->where('oa.status<',3);
		$this->db->group_by('p.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
}	