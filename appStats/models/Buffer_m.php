<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Buffer_m extends CI_Model
{
	public function buffer_total_num_rows($searchParams,$country_id,$buffer_wh_id)
	{
		$this->db->select('t.part_number,t.part_description,m.name as modality_name,a.asset_number,a.asset_id,as.name as asset_status');	
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		$this->db->where('p.part_level_id',1);		
		$this->db->where('a.wh_id',$buffer_wh_id);
		$this->db->where('a.country_id',$country_id);
		$this->db->order_by('t.tool_id ASC, a.asset_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function buffer_results($current_offset, $per_page, $searchParams,$country_id,$buffer_wh_id)
	{
		$this->db->select('t.part_number,t.part_description,m.name as modality_name,a.asset_number,a.asset_id,as.name as asset_status,p.serial_number');	
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		$this->db->where('p.part_level_id',1);		
		$this->db->where('a.wh_id',$buffer_wh_id);
		$this->db->where('a.country_id',$country_id);
		$this->db->order_by('t.tool_id ASC, a.asset_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_asset_based_on_id($asset_id)
	{
		$this->db->select('t.part_number,t.part_description,m.name as modality_name,a.asset_number,a.asset_id,as.name as asset_status,p.serial_number');	
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->where('p.part_level_id',1);		
		//$this->db->where('a.wh_id',$wh_id);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->order_by('t.tool_id ASC, a.asset_id ASC');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_asset_based_on_id_ib($asset_id)
	{
		$status = array(1,5,9,11,12);
		$this->db->select('t.part_number,t.part_description,m.name as modality_name,a.asset_number,a.asset_id,as.name as asset_status,p.serial_number');	
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->where('p.part_level_id',1);		
		//$this->db->where('a.wh_id',$wh_id);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where_in('a.status',$status);
		$this->db->order_by('t.tool_id ASC, a.asset_id ASC');
		$res = $this->db->get();
		return $res->row_array();
	}


	public function inventory_buffer_total_num_rows($searchParams,$wh_id)
	{
		$status = array(1,5,9,11);
		$this->db->select('t.part_number,t.part_description,m.name as modality_name,a.asset_number,a.asset_id,as.name as asset_status');	
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		$this->db->where('p.part_level_id',1);		
		$this->db->where('a.wh_id',$wh_id);
		$this->db->where('a.approval_status',0);
		$this->db->where_in('a.status',$status);
		$this->db->order_by('t.tool_id ASC, a.asset_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function inventory_buffer_results($current_offset, $per_page, $searchParams,$wh_id)
	{
		$status = array(1,5,9,11);
		$this->db->select('t.part_number,t.part_description,m.name as modality_name,a.asset_number,a.asset_id,as.name as asset_status,p.serial_number');	
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		$this->db->where('p.part_level_id',1);		
		$this->db->where('a.wh_id',$wh_id);
		$this->db->where('a.approval_status',0);
		$this->db->where_in('a.status',$status);
		$this->db->order_by('t.tool_id ASC, a.asset_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_warehouse_by_country($country_id)
	{
		$this->db->select('*');
		$this->db->from('warehouse');
		$this->db->where('country_id',$country_id);
		$this->db->where('status',1);
		$res=$this->db->get();
		$results='';
		if($res->num_rows()>0)
	    {
	    	$results.='<option value="">- Select Warehouse -</option>'; 
	        foreach($res->result_array() as $row)
	        {
	            $results.='<option value="'.$row['wh_id'].'">'.$row['wh_code'].' - ('.$row['name'].')</option>';      
	        }
	    }
	    else
	    {
	        $results.="<option value=''>-No Data Found-</option>"; 
	    } 
	    return $results;
	}
}