<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Special_cases_m extends CI_Model 
{
	public function getAssets($status,$country_id,$approval_status=2)
	{
		$this->db->select('*');
		$this->db->from('asset a');
		if($country_id!='')
			$this->db->where('a.country_id',$country_id);
		if($status!='')
			$this->db->where('a.status',$status);
		if($approval_status != 2)
			$this->db->where('a.approval_status',$approval_status);
		$res = $this->db->get();
		return $res->result_array();
	}

	# FE Queries
	public function owned_order_results($country_id)
	{
		$this->db->select('to.*,odt.name as order_type,cst.name as current_stage_name,l.name as countryName,concat(u.sso_id,"-",u.name) as sso');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('location l','l.location_id=to.country_id');		
		$this->db->where('to.current_stage_id',7);			
		$this->db->where('to.order_type',2);
		$this->db->where('to.order_type',2);
		$this->db->order_by('to.country_id,to.tool_order_id');
		if($country_id!='')
			$this->db->where('to.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function receive_order_results($country_id)
	{
		$this->db->select('to.*,odt.name as order_type,cst.name as current_stage_name,concat(u.sso_id," -(",u.name,")") as sso,l.name as countryName,sto.stock_transfer_id');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id','LEFT');
		$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id','LEFT');
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','to.country_id = l.location_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$search_query = '';
		$str1 = '('.$search_query.' osh.current_stage_id = 6 AND to.current_stage_id = 6 AND to.status<2 AND to.order_type = 2 AND osh.rto_id IS NULL AND osh.end_time IS NULL)';
		$this->db->where($str1);
		$str2 = '('.$search_query.' to.current_stage_id =4 AND stn.current_stage_id =2 AND stn.fe_check = 1)';
		$this->db->or_where($str2);
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->group_by('to.tool_order_id');
		if($country_id!='')
			$this->db->where('to.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();	
	}
	public function fe2_fe_receive_order_results($country_id,$type)
	{  
		$return_type = array(3,4);
		$this->db->select('to.*,rto.rto_id,odt.name as order_type,ro.return_number,rt.name as return_type,
			to.order_number as to_order,concat(u.sso_id,"-",u.name) as to_sso,
			from_to.order_number as from_order,concat(from_user.sso_id,"-",from_user.name) as from_sso');			
		$this->db->from('tool_order to');		
		$this->db->join('return_order ro','ro.tool_order_id = to.tool_order_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');				
		$this->db->join('order_status_history osh','osh.rto_id=rto.rto_id');	
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('tool_order from_to','from_to.tool_order_id = rto.tool_order_id');
		$this->db->join('user from_user','from_user.sso_id=from_to.sso_id');
		if($type == 4)
			$this->db->where_in('ro.return_type_id',$return_type);
		else
			$this->db->where('ro.return_type_id',4);
		$this->db->where('ro.return_approval',2);
		$this->db->where('ro.status',10);
		$this->db->where('osh.current_stage_id',6);	
		$this->db->where('osh.end_time IS NULL');
		$this->db->order_by('to.tool_order_id ASC');
		if($country_id!='')
			$this->db->where('to.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function feReturnInitiatedResults($country_id)
	{		
		$this->db->select('to.order_number as fe1_order_number,to.tool_order_id as from_tool_order_id,ro.return_order_id,
						l.name as countryName,concat(u.sso_id," - ",u.name) as sso,ro.*,
						rt.name as return_type,osh.order_status_id,rto.rto_id as rto_id,
						to_order.order_number as to_order,concat(to_user.sso_id," - ",to_user.name) as to_sso,
						to_order.current_stage_id as to_current_stage_id,ro.remarks as return_order_remarks,
						concat(wh1.wh_code," - ",wh1.name) as wh1,concat(wh2.wh_code," - ",wh2.name) as wh2
						');		
		$this->db->from('tool_order to');	
		$this->db->join('return_tool_order rto','rto.tool_order_id = to.tool_order_id');
		$this->db->join('return_order ro','ro.return_order_id = rto.return_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = ro.country_id','left');
		$this->db->join('tool_order to_order','to_order.tool_order_id = ro.tool_order_id','left');
		$this->db->join('user to_user','to_user.sso_id = to_order.sso_id','left');
		$this->db->join('warehouse wh1','wh1.wh_id = ro.wh_id','left');
		$this->db->join('warehouse wh2','wh2.wh_id = ro.ro_to_wh_id','left');
		$this->db->where('ro.status!=',10);
		$this->db->where_in('ro.return_approval',array(0,1,2));
		$this->db->group_by('ro.return_order_id');
		$this->db->order_by('to.country_id,ro.return_order_id');
		if($country_id!='')
			$this->db->where('ro.country_id',$country_id);
		$res = $this->db->get();		
		return $res->result_array();
	}

	public function pickup_list_results($country_id)
	{
		$return_type = array(1,2,4);
		$approval_status = array(0,2);
		$this->db->select('to.order_number,ro.return_number,ro.created_time,rto.rto_id,
						   concat(ro.address3,", ",ro.address4) as location_name,ro.return_order_id,l1.name as country_name,
						   concat(wh.wh_code," -(",wh.name,")") as wh_name,
						   concat(u.sso_id," -(",u.name,")") as sso_name,
						   rt.name as return_type_name');
		$this->db->from('return_order ro');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l1','l1.location_id = to.country_id','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = ro.wh_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('osh.current_stage_id',8);//waiting for pickup
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where('address_check!=',1);
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where_in('ro.return_approval',$approval_status);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		if($country_id!='')
			$this->db->where('to.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function open_fe_returns_list_results($country_id,$onlyIntransitAssets=0)
	{
		$return_type = array(1,2);
		$this->db->select('to.order_number,ro.return_number,ro.expected_arrival_date,l.name as location_name,ro.return_order_id,l1.name as country,
			concat(wh.wh_code," -(",wh.name,")") as wh_name,rto.rto_id,
			concat(u.sso_id," -(",u.name,")") as sso_name');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = ro.country_id');
		$this->db->join('warehouse wh','wh.wh_id = ro.ro_to_wh_id'); 
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('osh.current_stage_id',9);//At Transit from fe to wh
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_type_id',$return_type);
		if($country_id!='')
			$this->db->where('ro.country_id',$country_id);
		if($onlyIntransitAssets == 1)
		{
			$str1 = '(ro.order_delivery_type_id IN (1,3) OR (ro.order_delivery_type_id = 2 AND ro.wh_id != ro.ro_to_wh_id) )';
			$this->db->where($str1);
		}
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');		
		$res = $this->db->get();
		return $res->result_array();
	}
	public function wh2_wh_receive_order_results($country_id)
    {        
        $this->db->select('to.*,concat(wh.wh_code, "-(" ,wh.name,")") as from_wh_name,
                           concat(wh_to.wh_code, "-(" ,wh_to.name,")") as to_wh_name,od.expected_delivery_date');
        $this->db->from('tool_order to');  
        $this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');     
        $this->db->join('warehouse wh','wh.wh_id = to.wh_id');
        $this->db->join('warehouse wh_to','wh_to.wh_id = to.to_wh_id','LEFT');
        $this->db->where('to.current_stage_id',2);          
        $this->db->where('to.order_type',1);
        $this->db->where('to.fe_check',0);  
        if($country_id!='')
        	$this->db->where('to.country_id',$country_id);
        $res = $this->db->get();
        return $res->result_array();
    }
    public function admin_calibration_results($country_id)
	{
		$status=array(1,2);
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		$this->db->where('rco.rc_type',2);
		$this->db->where_in('rco.status',$status);
		$this->db->where_not_in('rca.current_stage_id',11);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		if($country_id!='')
			$this->db->where('rco.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function open_repair_results($country_id)
	{
		$status=array(1,2);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->where('rco.rc_type',1);
		$this->db->where_in('rco.status',$status);
		$this->db->where_not_in('rca.current_stage_id',20);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		if($country_id!='')
			$this->db->where('rco.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}
		// FE owned tools
	public function fe_owned_tools_results($country_id)
	{
		$this->db->select('t.part_number,t.part_description,a.asset_number,a.cal_due_date,t.cal_type_id,to.order_number,a.asset_id,to.country_id,to.tool_order_id');		
		$this->db->from('tool_order to');	
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');	
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id AND to.sso_id = ap.sso_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('ap.transit',0);
		$this->db->where('a.status!=',7);
		$this->db->where('to.current_stage_id!=10');
		$this->db->order_by('to.tool_order_id ASC');
		$this->db->group_by('ap.asset_id');
		if($country_id!='')
			$this->db->where('to.country_id',$country_id);
		$this->db->where('to.current_stage_id',4);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function repair_results($country_id)
	{
		$this->db->select('rc.*,a.asset_id,a.asset_number,t.part_number,t.part_description,w.name as warehouse,w.wh_code,w.wh_id,a.wh_id,a.approval_status,a.status as asset_status,as.name as asset_status_name');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.current_stage_id',20);
		$this->db->where('rc.rca_type',1);
		$this->db->where('rc.status',1);
		$this->db->group_by('rc.rc_asset_id');
		$this->db->order_by('rc.rc_asset_id ASC');
		if($country_id!='')
			$this->db->where('rc.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function calibration_results($country_id)
	{
		$this->db->select('rc.*,a.asset_id,a.asset_number,t.part_number,t.part_description,w.name as warehouse,w.wh_code,a.wh_id,l.name as country,rc.country_id,a.approval_status,a.status as asset_status,as.name as asset_status_name');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->join('location l','l.location_id=rc.country_id');		
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.current_stage_id',11);
		$this->db->where('rc.rca_type',2);
		$this->db->where('rc.status',1);
		if($country_id!='')
			$this->db->where('rc.country_id',$country_id);
		$this->db->group_by('rc.rc_asset_id');
		$this->db->order_by('rc.rc_asset_id ASC');		
		$res = $this->db->get();
		return $res->result_array();
	}

	public function defectiveAssetRecords($country_id)
	{
		$this->db->select('da.*,a.asset_number');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id= da.asset_id');
		$this->db->where('da.status<',10);
		if($country_id!='')
			$this->db->where('a.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function getAssetPosition($country_id,$status)
	{
		$this->db->select('ap.asset_position_id,a.asset_id,ap.wh_id,ap.transit,ap.sso_id,ap.supplier_id,a.asset_number');
		$this->db->from('asset_position ap');
		$this->db->join('asset a','a.asset_id=ap.asset_id');
		if($country_id!='')
			$this->db->where('a.country_id',$country_id);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('a.status',$status);
		$this->db->group_by('ap.asset_id');
		$res = $this->db->get();
		return $res->result_array();	
	}	

	public function getOrderedToolsOa($country_id)
	{
		$this->db->select('to.order_number,odt.ordered_tool_id,oa.ordered_asset_id');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool odt','odt.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = odt.ordered_tool_id');
		$this->db->join('order_status_history osh','osh.tool_order_id= to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id= osh.order_status_id');
		$this->db->where('osh.current_stage_id',7);
		$this->db->where('odt.status<',3);
		$this->db->where('to.current_stage_id>=',7);
		$this->db->where('to.current_stage_id<=',10);
		$this->db->where('osh.rto_id IS NOT NULL');
		$this->db->where('to.country_id',$country_id);
		$this->db->group_by('odt.ordered_tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function getOahOa($country_id)
	{
		$this->db->select('to.order_number,oah.ordered_asset_id');
		$this->db->from('tool_order to');
		$this->db->join('order_status_history osh','osh.tool_order_id= to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
		$this->db->where('osh.current_stage_id',7);
		$this->db->where('to.current_stage_id>=',7);
		$this->db->where('to.current_stage_id<=',10);
		$this->db->where('osh.rto_id IS NOT NULL');
		$this->db->where('to.country_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}
}	