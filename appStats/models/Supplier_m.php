<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier_m extends CI_Model 
{
	
	public function supplier_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$status_supplier = array(1,2);
		$this->db->select('s.*,GROUP_CONCAT(m.name SEPARATOR",") as modality_name,l.name as country_name');
		$this->db->from('supplier s');
		$this->db->join('modality_supplier ms','ms.supplier_id=s.supplier_id AND ms.status=1','left');
		$this->db->join('modality m','ms.modality_id=m.modality_id AND m.status=1','left');
		$this->db->join('location l','l.location_id=s.country_id');
		if($searchParams['name']!='')
			$this->db->like('s.name',$searchParams['name']);		
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['calibration']!='')
			$this->db->where('s.calibration',$searchParams['calibration']);		
		if($searchParams['sup_type_id']!='')
			$this->db->where('s.sup_type_id',$searchParams['sup_type_id']);	
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('s.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('s.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('s.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('s.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}	
		$this->db->where_in('s.status',$status_supplier);
		$this->db->group_by('s.supplier_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function supplier_total_num_rows($searchParams,$task_access)
	{
		$status_supplier = array(1,2);
		$this->db->select('s.*,GROUP_CONCAT(m.name SEPARATOR",") as modality_name,l.name as country_name');
		$this->db->from('supplier s');
		$this->db->join('modality_supplier ms','ms.supplier_id=s.supplier_id AND ms.status=1','left');
		$this->db->join('modality m','ms.modality_id=m.modality_id AND m.status=1','left');
		$this->db->join('location l','l.location_id=s.country_id');
		if($searchParams['name']!='')
			$this->db->like('s.name',$searchParams['name']);		
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['calibration']!='')
			$this->db->where('s.calibration',$searchParams['calibration']);
		if($searchParams['sup_type_id']!='')
			$this->db->where('s.sup_type_id',$searchParams['sup_type_id']);	
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('s.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('s.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('s.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('s.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}			
		$this->db->where_in('s.status',$status_supplier);
		$this->db->group_by('s.supplier_id');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function supplier_name_availability($data)
	{
		$this->db->from('supplier s');
		$this->db->where('s.name', $data['name']);
		$this->db->where('s.country_id',$data['country_id']);
		if($data['supplier_id']!='' || $data['supplier_id']!=0)
		{
			$this->db->where_not_in('s.supplier_id', @$data['supplier_id']);
		}
		$this->db->where('s.status',1);
		$query = $this->db->get();
		return ($query->num_rows()>0)?1:0;
	}
	public function modality_supplier_insert_update($supplier_id,$modality_id)
	{
		$qry = "INSERT INTO modality_supplier(supplier_id, modality_id, status)
		               VALUES(".$supplier_id.",".$modality_id.",'1')
		               ON DUPLICATE KEY UPDATE status = VALUES(status);";
		$this->db->query($qry);
	}

	public function get_latest_record($country_id)
	{
		$this->db->select('supplier_code');
		$this->db->from('supplier s');
		$this->db->where('s.country_id',$country_id);
		$this->db->order_by('supplier_id DESC');
		$res = $this->db->get();
		return $res->row_array();
	}
}	