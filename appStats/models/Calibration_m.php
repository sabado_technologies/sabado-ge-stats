<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// created by Srilekha on 21st july 2017 12:18PM

class Calibration_m extends CI_Model 
{

	public function get_latest_qa_reason($rc_asset_id)
	{
		$this->db->select('qa_status_id');
		$this->db->from('rc_status_history');
		$this->db->where('rc_asset_id',$rc_asset_id);
		$this->db->where('current_stage_id',15);
		$this->db->order_by('rcsh_id DESC');
		$this->db->limit(1);
		$res = $this->db->get();
		return $res->row_array();
	}

	/*Calibration Num of Rows.
    Author:Srilekha
    Time:04.46PM Date:09-09-2017*/
	public function calibration_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('rc.rc_asset_id');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id = rc.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->join('location l','l.location_id = rc.country_id');
		if($searchParams['crn']!='')
			$this->db->like('rc.rc_number',$searchParams['crn']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		
		if($task_access == 1 )
		{
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whc_id']);
			}
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rc.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rc.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rc.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whc_id']);
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.current_stage_id',11);
		$this->db->where('rc.rca_type',2);
		$this->db->where('rc.status',1);
		$this->db->group_by('rc.rc_asset_id');
		$this->db->order_by('rc.rc_asset_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('rc.*,a.asset_id,a.asset_number,t.part_number,t.part_description,w.name as warehouse,w.wh_code,a.wh_id,l.name as country,rc.country_id,a.approval_status,a.status as asset_status,as.name as asset_status_name,p.serial_number');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->join('location l','l.location_id=rc.country_id');
		if($searchParams['crn']!='')
			$this->db->like('rc.rc_number',$searchParams['crn']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rc.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rc.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rc.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.current_stage_id',11);
		$this->db->where('rc.rca_type',2);
		$this->db->where('rc.status',1);
		$this->db->group_by('rc.rc_asset_id');
		$this->db->order_by('rc.rc_asset_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_rc_details($rc_asset_id)
	{
		$this->db->select('rc.*, a.asset_id, a.asset_number, a.wh_id, t.part_number, t.part_description, w.name as warehouse, w.wh_code,p.serial_number');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.current_stage_id',11);
		$this->db->where('rc.rc_asset_id',$rc_asset_id);
		$this->db->where('rc.status',1);
		$this->db->group_by('rc.rc_asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}
	public function admin_calibration_total_num_rows($searchParams,$task_access)
	{
		$status=array(1,2);
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('rco.rc_type',2);
		$this->db->where_in('rco.status',$status);
		$this->db->where_not_in('rca.current_stage_id',11);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function admin_calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$status=array(1,2);
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('rco.rc_type',2);
		$this->db->where_in('rco.status',$status);
		$this->db->where('p.part_level_id',1);
		$this->db->where_not_in('rca.current_stage_id',11);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_asset_details($rc_order_id,$issue=0)
	{
		$status=array(1,2);
		$this->db->select('rca.*, a.asset_id, a.asset_number, a.wh_id, t.part_number, t.part_description, c.name as current_stage, tt.name as tool_type,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('tool_type tt','tt.tool_type_id=t.tool_type_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where_not_in('rca.current_stage_id',11);
		$this->db->where('rca.rca_type',2);
		if($issue == 1)
			$this->db->where('rca.status',1); # updated by maruthi to get the only the assets which are in calibration workflow not sent to repair flows
		else
			$this->db->where_in('rca.status',$status);
		$res = $this->db->get();
		return $res->result_array();

	}
	public function get_asset_details_row($rc_order_id,$asset_id)
	{
		$status=array(1,2);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,tt.name as tool_type,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('tool_type tt','tt.tool_type_id=tt.tool_type_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where_not_in('rca.current_stage_id',11);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('rca.rca_type',2);
		$this->db->where_in('rca.status',$status);
		$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}
	public function get_asset_details_for_qc($rc_order_id)
	{
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,tt.name as tool_type,a.calibrated_date,a.cal_due_date,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('tool_type tt','tt.tool_type_id=t.tool_type_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where('rca.current_stage_id',15);
		$this->db->where('rca.rca_type',2);
		$this->db->where('rca.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_asset_details_for_qc_row($rc_order_id,$asset_id)
	{
		
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,tt.name as tool_type,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('tool_type tt','tt.tool_type_id=t.tool_type_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('rca.current_stage_id',15);
		$this->db->where('rca.rca_type',2);
		$this->db->where('rca.status',1);
		$res = $this->db->get();
		return $res->row_array();

	}

	/*Tool ID.
    Author:Srilekha
    Time:12.58PM Date:21-07-2017*/
	public function get_top_tool_id($asset_id)
	{
		$this->db->select('p.tool_id');
		$this->db->from('part p');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->where('a.asset_id',$asset_id);
		$this->db->order_by('p.part_id ASC');
		$res = $this->db->get();
		$result = $res->row_array();
		return $result['tool_id'];
	}
	/*Asset Details.
    Author:Srilekha
    Time:1.00PM Date:21-07-2017*/
	public function asset_details_view($asset_id,$tool_id)
	{
		$this->db->select('t.part_description,t.part_number,t.tool_code,
			p.serial_number,p.cost,ac.name as asset_condition_name,
			p.remarks as part_remarks,p.length,p.breadth,p.height,a.*,t.kit as kit,m.name as modality_name,
			tt.name as tool_type_name,wh.wh_code as warehouse_code,
			a.sub_inventory as sub_inventory,at.name as asset_type,
			s.name as supplier_name,a.remarks as asset_remarks,
			ct.name as calibration,cs.name as cs_name,a.cal_due_date as due_date,
			t.manufacturer,t.po_number as po,t.earpe_number as earpe,
			a.date_of_use as date_of_used,ad.name as document,
			t.part_number as part_num,t.tool_code as toolcode,
			t.part_description as description,p.serial_number as serialnumber,
			p.remarks as remark,p.status as statuss,p.length as p_length,
			p.breadth as p_breadth,p.height as p_height,p.cost as p_cost,
			al.name as asset_level_name,p.quantity,cs.*');
		$this->db->from('asset a');
		$this->db->join('part p','a.asset_id = p.asset_id');
		$this->db->join('tool t','p.tool_id =t.tool_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id =t.tool_type_id');
		$this->db->join('asset_type at','at.asset_type_id =t.asset_type_id');
		$this->db->join('asset_document ad','ad.asset_id =a.asset_id','left');		
		$this->db->join('modality m','m.modality_id =a.modality_id ');
		$this->db->join('supplier cs','cs.supplier_id = a.cal_supplier_id','left');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('supplier s','s.supplier_id=t.supplier_id');
		$this->db->join('warehouse wh','a.wh_id = wh.wh_id');
		$this->db->join('asset_status as','as.asset_status_id =a.status ');
		$this->db->join('part_level pl','pl.part_level_id= p.part_level_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = p.status');
		$this->db->where('p.asset_id',$asset_id);
		$this->db->where('p.tool_id',$tool_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	/*Calibration Details.
    Author:Srilekha
    Time:11.27AM Date:24-07-2017*/
    public function cal_details_view($calibration_id)
    {
    	$this->db->select('c.*,wh.name as ware_house,s.name as supplier_name,cs.name as current_stage');
    	$this->db->from('calibration c');
    	$this->db->join('current_stage cs','cs.current_stage_id=c.current_stage_id');
    	$this->db->join('warehouse wh','wh.wh_id=c.wh_id');
    	$this->db->join('supplier s','s.supplier_id=c.supplier_id');
    	$this->db->where('c.calibration_id',$calibration_id);
    	$res=$this->db->get();
    	return $res->row_array();
    }

	/*Closed Calibration Num of Rows.
    Author:Srilekha
    Time:10.10AM Date:24-07-2017*/
	public function closed_calibration_total_num_rows($searchParams,$task_access)
	{
		$c_stage=array(17,18,19,27);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('rco.rc_type',2);
		$this->db->where('p.part_level_id',1);
		$this->db->where_in('rca.current_stage_id',$c_stage);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id DESC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	/*Closed Calibration Results.
    Author:Srilekha
    Time:10.18AM Date:24-07-2017*/
	public function closed_calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{	
		$c_stage=array(17,18,19,27);
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$this->db->where_in('rca.current_stage_id',$c_stage);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_closed_asset_details($rc_order_id)
	{
		$c_stage=array(17,18,19,27);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where_in('rca.current_stage_id',$c_stage);
		$this->db->where('rca.rca_type',2);
		$this->db->where('rca.status',10);
		$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->result_array();

	}
	/*Get Ordered Tool Id from Order Asset.
    Author:Srilekha
    Time:05.18PM Date:24-07-2017*/
    public function get_ordered_tool_id($asset_id)
    {
    	$this->db->select('ordered_tool_id');
    	$this->db->from('ordered_asset');
    	$this->db->where('asset_id',$asset_id);
    	$this->db->order_by('ordered_asset_id DESC');
    	$res= $this->db->get();
    	return $res->row_array();
    }
    //koushik
public function get_asset_part_details($asset_id)
	{
		$this->db->select('a.asset_id,a.asset_number,p.part_id,
						  pl.name as part_level_name,t.part_number,
						  t.part_description,p.quantity,t.tool_id, a.status as asset_status,p.serial_number');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('a.asset_id',$asset_id);
		//$this->db->where('a.status',4);
		$this->db->where('p.part_status',1);
		$this->db->order_by('p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function get_rcsh_by_rc_asset($rc_asset_id,$current_stage_id)
	{
		$this->db->select('rcsh.rcsh_id');
		$this->db->from('rc_asset rca');
		$this->db->join('rc_status_history rcsh','rcsh.rc_asset_id = rca.rc_asset_id');
		$this->db->where('rca.rc_asset_id',$rc_asset_id);
		$this->db->where('rcsh.current_stage_id',$current_stage_id);
		$this->db->order_by('rcsh.rcsh_id DESC');
		$this->db->limit(1);
		$res = $this->db->get();
		return $res->row_array();
	}

    //koushik
    public function wh_calibration_total_num_rows($searchParams,$task_access)
    {
    	$this->db->select('rco.rc_order_id,rco.rcb_number,
    					   s.name as supplier_name,
    					   date_sub(rco.expected_delivery_date, 
    					   INTERVAL 2 DAY) AS ship_by_date,
    					   rco.expected_delivery_date,
    					   l.name as country');
    	$this->db->from('rc_order rco');
    	$this->db->join('warehouse w','w.wh_id = rco.wh_id','LEFT');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('asset a','a.asset_id = rca.asset_id');
    	$this->db->join('part p','p.asset_id = a.asset_id');
    	$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
    	$this->db->join('location l','l.location_id=rco.country_id');
    	if($searchParams['crb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['crb_number']);
		if($searchParams['supplier_id']!='')
			$this->db->where('rco.supplier_id',$searchParams['supplier_id']);
		if(@$searchParams['ship_by_date']!='')
			$this->db->like('rco.expected_delivery_date',$searchParams['ship_by_date']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in("rco.wh_id",$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}

		$this->db->where('rca.current_stage_id',12);
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$this->db->group_by('rco.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
    }
    
    //koushik
    public function wh_calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{
    	$this->db->select('rco.rc_order_id,rco.rcb_number,s.name as supplier_name,date_sub(rco.expected_delivery_date, INTERVAL 2 DAY) AS ship_by_date,rco.expected_delivery_date,s.supplier_code,l.name as country,concat(w.wh_code,"-(",w.name,")") AS wh_name');
    	$this->db->from('rc_order rco');
    	$this->db->join('warehouse w','w.wh_id = rco.wh_id','LEFT');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
    	$this->db->join('asset a','a.asset_id = rca.asset_id');
    	$this->db->join('part p','p.asset_id = a.asset_id');
    	$this->db->join('location l','l.location_id=rco.country_id');
    	if($searchParams['crb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['crb_number']);
		if($searchParams['supplier_id']!='')
			$this->db->where('rco.supplier_id',$searchParams['supplier_id']);
		if(@$searchParams['ship_by_date']!='')
			$this->db->like('rco.expected_delivery_date',$searchParams['ship_by_date']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in("rco.wh_id",$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.current_stage_id',12);
		$this->db->where('rco.rc_type',2);
		$this->db->group_by('rco.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function get_cal_asset_list($rc_order_id)
	{
		$this->db->select('rca.rc_number,a.asset_number,t.part_number,rco.rc_order_id,rca.rc_asset_id,
						   t.part_description,a.asset_id,rca.rc_asset_id,p.serial_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function get_cal_asset_detail($rc_asset_id)
	{
		$this->db->select('rca.rc_number,a.asset_number,t.part_number,p.serial_number,
						   t.part_description,a.asset_id,rca.rc_asset_id,rca.rc_order_id,
						   a.sub_inventory,s.supplier_code,s.name as supplier_name');
		$this->db->from('rc_asset rca');
		$this->db->join('rc_order rco','rco.rc_order_id = rca.rc_order_id');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('rca.rc_asset_id',$rc_asset_id);
		$this->db->where('p.part_level_id',1);
		$res = $this->db->get();
		return $res->row_array();
	}

	//koushik
	public function get_cal_sup_detials($rc_order_id)
	{
		$this->db->select('rco.*,s.supplier_code,s.name as supplier_name');
		$this->db->from('rc_order rco');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	/*Get asset Part details
	Author:Srilekha
	Time:04:01PM Date:25-07-2017*/ 
	

	/*Get assetAvailability Status
	Author:Srilekha
	Time:02:54PM Date:26-07-2017*/ 
	public function get_first_asset_avail($tool_id)
	{
		$wh_id = $this->session->userdata('swh_id');
		$this->db->select('a.asset_id');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('a.approval_status',0);
		$this->db->where('a.wh_id',$wh_id);
		$this->db->where('p.part_level_id',1); // 1 = L0 level
		$this->db->where('a.status',4);
		$res = $this->db->get();
		return $res->row_array();
	}
	// get Calibration asset health
	public function get_cal_asset_health($cah_id,$part_id)
	{
		$this->db->select('ac.name as asset_condition_name,cah.remarks');
		$this->db->from('cal_asset_health cah');
		$this->db->join('asset_condition ac','ac.asset_condition_id = cah.asset_condition_id');
		$this->db->where('cah.cah_id',$cah_id);
		$this->db->where('cah.part_id',$part_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	/*Closed Calibration Request Num of Rows at Vendor.
    Author:Srilekha
    Time:01.09PM Date:22-07-2017*/
	public function closed_cal_delivery_total_num_rows($searchParams,$task_access)
	{
		$status=array(2,10);
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('rco.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$this->db->where_in('rco.status',$status);
		$this->db->where('rsh.current_stage_id',13);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	/*Closed Calibration Request Results At Vendor.
    Author:Srilekha
    Time:12.33PM Date:21-07-2017*/
	public function closed_cal_delivery_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$status=array(2,10);
		$this->db->select('rco.*,w.wh_code,w.name,rca.current_stage_id,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('rco.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$this->db->where_in('rco.status',$status);
		$this->db->where('rsh.current_stage_id',13);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_vendor_closed_assets($rc_order_id)
	{
		$status=array(1,10);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where('rsh.current_stage_id',13);
		$this->db->where('rca.rca_type',2);
		$this->db->where_in('rca.status',$status);
		$res = $this->db->get();
		return $res->result_array();
	}

	/*Open Calibration Request Num of Rows at Vendor.
    Author:Srilekha
    Time:01.09PM Date:22-07-2017*/
	public function vendor_calibration_total_num_rows($searchParams,$task_access)
	{
		
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('rco.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$this->db->where('rco.status',2);
		$this->db->where('rca.current_stage_id',13);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	/*open Calibration Request Results At Vendor.
    Author:Srilekha
    Time:12.33PM Date:21-07-2017*/
	public function vendor_calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{
		
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('rco.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$this->db->where('rco.status',2);
		$this->db->where('rca.current_stage_id',13);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_vendor_assets($rc_order_id)
	{
		$c_stage=array(13,14);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where_in('rca.current_stage_id',$c_stage);
		$this->db->where('rca.rca_type',2);
		$this->db->where('rca.status',1);
		//$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_vendor_assets_row($rc_order_id,$asset_id)
	{
		
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where('rca.current_stage_id',13);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('rca.rca_type',2);
		$this->db->where('rca.status',1);
		//$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function check_asset_number($rc_asset_id,$asset_number)
	{
		$this->db->select('a.asset_number,a.asset_id');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->where('rc.rc_asset_id',$rc_asset_id);
		$this->db->where('a.asset_number',$asset_number);
		$res=$this->db->get();
		return $res->row_array();
	}

	/*Get Attached Documents by Stage
	Author:Srilekha
	Time:11:06AM Date:28-07-2017*/
	public function get_documents_by_stage($rc_order_id)
	{
		$this->db->select('rcd.*,rc.rc_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rc','rc.rc_order_id=rco.rc_order_id');
		$this->db->join('rca_doc rcd','rcd.rc_asset_id=rc.rc_asset_id');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->order_by('rc.rc_asset_id ASC');
		$res= $this->db->get();
		return $res->result_array();
	}
	public function get_from_address($rc_order_id)
	{
		$this->db->select('wh.*,l.name as from_location,rcs.remarks,
						   pf.format_number,pf.created_time,pf.print_type,
						   rcs.courier_type,rcs.courier_number,rcs.billed_to,
						   GROUP_CONCAT(rca.rc_number SEPARATOR",") as crn_number, 
						   rcs.courier_name,rcs.contact_person,rcs.phone_number,s.supplier_code,s.name as supplier_name,s.contact_number as s_contact_number,
						   rcs.vehicle_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = rco.wh_id');
		$this->db->join('rc_shipment rcs','rcs.rc_order_id = rco.rc_order_id');
		$this->db->join('print_format pf','pf.print_id = rcs.print_id');
		$this->db->join('location l','l.location_id = wh.location_id');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id','LEFT');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->group_by('rco.rc_order_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_calibration_tool($rc_order_id)
	{
		$this->db->select('t.part_number,t.part_description,t.hsn_code,count(t.tool_id) as quantity,sum(a.cost_in_inr) as cost_in_inr,t.gst_percent,GROUP_CONCAT(CONCAT(p.serial_number," : ",a.asset_number) SEPARATOR ",") as asset_number_list');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function calibration_required_total_num_rows($searchParams,$task_access)
	{
		$status=array(1,2);
		$sso_id=$this->session->userdata('sso_id');
		$this->db->from('tool_order to');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');
		if($searchParams['order']!='')
			$this->db->like('to.order_number',$searchParams['order']);
		if($searchParams['order_type']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_type']);
		if($searchParams['crt_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['crt_sso_id']);
		$this->db->where_in('oah.status',$status);
		$this->db->where('to.current_stage_id',7);
		// if($this->session->userdata('role_id')==2 || $this->session->userdata('role_id')==5 || $this->session->userdata('role_id')==6)
		// 	$this->db->where('to.sso_id',$sso_id);
		$this->db->join('ordered_asset oa','oa.ordered_asset_id=oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id=oa.asset_id');
		$this->db->join('rc_asset ra','ra.asset_id=a.asset_id');
		#$this->db->join('part p','p.asset_id = a.asset_id');
		#$this->db->join('tool t','t.tool_id = p.tool_id');
		#$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->join('user u','u.sso_id = to.sso_id');		
		$this->db->where('osh.current_stage_id',7);
		
		$this->db->where('ra.status',1);
		$this->db->where('ra.rca_type',2);
		$this->db->where('ra.current_stage_id',11);
		#$this->db->or_where('a.status',12);
		#$this->db->where('t.cal_type_id',1);
		#$this->db->where('p.part_level_id',1);
		$this->db->group_by('to.tool_order_id');		
		$this->db->order_by('to.tool_order_id ASC');	
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['crt_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['crt_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['crt_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['crt_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}				
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function calibration_required_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$status=array(1,2);
		$this->db->select('to.*,concat(u.sso_id,"-",u.name) as sso,l.name as countryName');
		$this->db->from('tool_order to');
		$this->db->join('location l','l.location_id = to.country_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');
		if($searchParams['order']!='')
			$this->db->like('to.order_number',$searchParams['order']);
		if($searchParams['order_type']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_type']);
		if($searchParams['crt_sso_id']!='')
			$this->db->where('to.sso_id',$searchParams['crt_sso_id']);
		$this->db->join('ordered_asset oa','oa.ordered_asset_id=oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id=oa.asset_id');
		$this->db->join('rc_asset ra','ra.asset_id=a.asset_id');
		$this->db->join('user u','u.sso_id = to.sso_id');		
		$this->db->where('osh.current_stage_id',7);
		$this->db->where_in('oah.status',$status);
		$this->db->where('to.current_stage_id',7);
		$this->db->where('ra.status',1);
		$this->db->where('ra.rca_type',2);
		$where_str = '(ra.current_stage_id = 11 OR a.status = 12)';
		$this->db->where($where_str);
		$this->db->group_by('to.tool_order_id');		
		$this->db->order_by('to.tool_order_id ASC');		
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['crt_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['crt_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['crt_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['crt_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();		
		return $res->result_array();
	}

	
	// dependencies for this functions are Alerts controller,maruthi helper plea
	// please check before change this function
	// modified by maruthi on 15th august'17
	public function cal_required_tools($tool_order_id)
	{
		// plase read above comments
		$status=array(1,2);
		//$ass_cal_status = array(4,12);
		$this->db->select('a.asset_number,p.serial_number,t.part_number,t.part_description,
				osh.order_status_id,oah.status,osh.tool_order_id,a.asset_id,
				DATEDIFF(a.cal_due_date,now())as days,a.cal_due_date,w.name as warehouse');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id=oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id=oa.asset_id');
		$this->db->join('rc_asset ra','ra.asset_id=a.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$this->db->where('osh.current_stage_id',7);
		$this->db->where_in('oah.status',$status);		
		$this->db->where('ra.status',1);				
		$this->db->where('t.cal_type_id',1);
		$this->db->where('p.part_level_id',1);		
		$where_str = '(ra.current_stage_id = 11 OR a.status = 12)';
		$this->db->where($where_str);
		$this->db->group_by('oa.ordered_asset_id');
		$res = $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->result_array();
	}
	
	public function crossed_expected_delivery_total_num_rows($searchParams)
	{
		$this->db->select('to.*');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id=to.tool_order_id');
		if(@$searchParams['order']!='')
			$this->db->like('to.order_number',$searchParams['order']);
		if(@$searchParams['order_type']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_type']);
		$this->db->where('to.current_stage_id',6);
		$this->db->where('od.expected_delivery_date<',date('Y-m-d'));
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function crossed_expected_delivery_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('to.*,od.expected_delivery_date as exp_d_date');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id=to.tool_order_id');
		if($searchParams['order']!='')
			$this->db->like('to.order_number',$searchParams['order']);
		if($searchParams['order_type']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_type']);
		$this->db->where('to.current_stage_id',6);
		$this->db->where('od.expected_delivery_date<',date('Y-m-d'));
		$this->db->group_by('to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function fe_not_acknowledged_tools($tool_order_id)
	{
		$this->db->select('a.asset_number,p.serial_number,
				pl.name as part_level,t.part_number,t.part_description');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id=oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id=oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('part_level pl','p.part_level_id = p.part_level_id');
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('oa.ordered_asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_calibration_orders_info($orders_arr)
	{
		$this->db->select('to.*,concat(u.sso_id, "-(" ,u.name,")") as sso');
		$this->db->from('tool_order to');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where_in('to.tool_order_id',$orders_arr);
		$res= $this->db->get();
		return $res->result_array();
	}
	/*QC Calibration Request Num of Rows.
    Author:Srilekha
    Time:08.53PM Date:17-08-2017*/
	public function qc_calibration_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$this->db->where('rco.status',2);
		$this->db->where('rca.current_stage_id',15);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id DESC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	/*QC Calibration Request Results.
    Author:Srilekha
    Time:08.53PM Date:17-08-2017*/
	public function qc_calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access == 1 )
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));

		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_type',2);
		$this->db->where('rco.status',2);
		$this->db->where('rca.current_stage_id',15);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function closed_qc_calibration_total_num_rows($searchParams,$task_access)
	{
		//$status=array(1,2);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		
		if($task_access == 1 )
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('rco.rc_type',2);
		$this->db->where('p.part_level_id',1);
		$this->db->where('rsh.current_stage_id',15);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function closed_qc_calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{
		//$status=array(1,2);
		$this->db->select('rco.*,w.wh_code,w.name,l.name as country');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('location l','l.location_id=rco.country_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		
		if($task_access == 1 )
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rco.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('rco.wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('rco.rc_type',2);
		$this->db->where('p.part_level_id',1);
		$this->db->where('rsh.current_stage_id',15);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_qc_closed_assets($rc_order_id)
	{
		$status=array(1,2,10);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where('rsh.current_stage_id',15);
		$this->db->where('rca.rca_type',2);
		$this->db->where_in('rca.status',$status);
		//$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	//notification
	public function wh_calibration_total_num_rows_admin()
    {
    	//$wh_id = $this->session->userdata('swh_id');
    	$this->db->select('rco.rc_order_id');
    	$this->db->from('rc_order rco');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('asset a','a.asset_id = rca.asset_id');
    	$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
		$this->db->where('rca.current_stage_id',12);
		//$this->db->where('rco.wh_id',$wh_id);
		$this->db->where('rco.rc_type',2);
		$this->db->group_by('rco.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
    }

    public function calibration_count_notification($current_stage_id)
    {
    	//$wh_id = $this->session->userdata('swh_id');
    	$this->db->select('rco.rc_order_id');
    	$this->db->from('rc_order rco');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('asset a','a.asset_id = rca.asset_id');
    	$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
		$this->db->where('rca.current_stage_id',$current_stage_id);
		//$this->db->where('rco.wh_id',$wh_id);
		$this->db->where('rco.rc_type',2);
		//$this->db->group_by('rco.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
    }

    public function get_country_from_rc_asset($rc_asset_id)
    {
    	$this->db->select('wh.country_id');
    	$this->db->from('rc_asset rca');
    	$this->db->join('rc_order rco','rco.rc_order_id = rca.rc_order_id','LEFT');
    	$this->db->join('warehouse wh','wh.wh_id = rco.wh_id','LEFT');
    	$this->db->where('rca.rc_asset_id',$rc_asset_id);
    	$res = $this->db->get();
    	$result = $res->row_array();
    	return $result['country_id'];
    }

    public function get_scanned_asset_list()
	{
		$this->db->select('rcah.rcah_id,rco.rcb_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('rc_status_history rcsh','rcsh.rc_asset_id = rca.rc_asset_id');
		$this->db->join('rc_asset_history rcah','rcah.rc_asset_id = rca.rc_asset_id AND rcsh.rcsh_id = rcah.rcsh_id');
		$this->db->where('rcsh.current_stage_id',12);
		$this->db->where('rcsh.modified_time',NULL);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function check_scanned_asset_list($rc_order_id)
	{
		$this->db->select('rcah.rcah_id');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('rc_status_history rcsh','rcsh.rc_asset_id = rca.rc_asset_id');
		$this->db->join('rc_asset_history rcah','rcah.rc_asset_id = rca.rc_asset_id AND rcsh.rcsh_id = rcah.rcsh_id');
		$this->db->where('rcsh.current_stage_id',12);
		$this->db->where('rcsh.modified_time',NULL);
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$res = $this->db->get();
		return $res->num_rows();
	}
}
