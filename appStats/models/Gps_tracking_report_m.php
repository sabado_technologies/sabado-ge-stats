<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gps_tracking_report_m extends CI_Model 
{
	public function gps_report_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('gps.gps_tool_id');
		$this->db->from('gps_tool gps');
		$this->db->join('location l','l.location_id = gps.country_id');
		$this->db->join('asset a','a.gps_tool_id = gps.gps_tool_id','LEFT');
		$this->db->join('part p','p.asset_id = a.asset_id','LEFT');
		$this->db->join('tool t','t.tool_id = p.tool_id','LEFT');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_number']!='')
			$this->db->like('t.part_number',$searchParams['tool_number']);
		if($searchParams['tool_description']!='')
			$this->db->like('t.part_description',$searchParams['tool_description']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['uid_number']!='')
			$this->db->like('gps.uid_number',$searchParams['uid_number']);
		if($searchParams['gps_name']!='')
			$this->db->like('gps.name',$searchParams['gps_name']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('gps.country_id',$_SESSION['s_country_id']);
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('gps.country_id',$_SESSION['header_country_id']);
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('gps.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('gps.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('gps.gps_tool_id ASC');
		$this->db->group_by('gps.gps_tool_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function gps_report_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('gps.name,gps.uid_number,a.asset_number,CONCAT(t.part_number," - ",t.part_description) AS tool_desc,l.name as country_name,a.asset_id,p.serial_number');
		$this->db->from('gps_tool gps');
		$this->db->join('location l','l.location_id = gps.country_id');
		$this->db->join('asset a','a.gps_tool_id = gps.gps_tool_id','LEFT');
		$this->db->join('part p','p.asset_id = a.asset_id','LEFT');
		$this->db->join('tool t','t.tool_id = p.tool_id','LEFT');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_number']!='')
			$this->db->like('t.part_number',$searchParams['tool_number']);
		if($searchParams['tool_description']!='')
			$this->db->like('t.part_description',$searchParams['tool_description']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['uid_number']!='')
			$this->db->like('gps.uid_number',$searchParams['uid_number']);
		if($searchParams['gps_name']!='')
			$this->db->like('gps.name',$searchParams['gps_name']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('gps.country_id',$_SESSION['s_country_id']);
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('gps.country_id',$_SESSION['header_country_id']);
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('gps.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('gps.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('gps.gps_tool_id ASC');
		$this->db->group_by('gps.gps_tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function download_gps_list($searchParams,$task_access)
	{
		$this->db->select('gps.name,gps.uid_number,a.asset_number,CONCAT(t.part_number," - ",t.part_description) AS tool_desc,l.name as country_name,a.asset_id,p.serial_number');
		$this->db->from('gps_tool gps');
		$this->db->join('location l','l.location_id = gps.country_id');
		$this->db->join('asset a','a.gps_tool_id = gps.gps_tool_id','LEFT');
		$this->db->join('part p','p.asset_id = a.asset_id','LEFT');
		$this->db->join('tool t','t.tool_id = p.tool_id','LEFT');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_number']!='')
			$this->db->like('t.part_number',$searchParams['tool_number']);
		if($searchParams['tool_description']!='')
			$this->db->like('t.part_description',$searchParams['tool_description']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['uid_number']!='')
			$this->db->like('gps.uid_number',$searchParams['uid_number']);
		if($searchParams['gps_name']!='')
			$this->db->like('gps.name',$searchParams['gps_name']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('gps.country_id',$_SESSION['s_country_id']);
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('gps.country_id',$_SESSION['header_country_id']);
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('gps.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('gps.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('gps.gps_tool_id ASC');
		$this->db->group_by('gps.gps_tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_gps_linked_assets($task_access)
	{
		$this->db->select('gps.name,gps.uid_number,a.asset_number,CONCAT(t.part_number," - ",t.part_description) AS tool_desc,l.name as country_name,a.asset_id');
		$this->db->from('gps_tool gps');
		$this->db->join('location l','l.location_id = gps.country_id');
		$this->db->join('asset a','a.gps_tool_id = gps.gps_tool_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('gps.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('gps.country_id',$this->session->userdata('header_country_id'));
			}
			else
			{
				$this->db->where_in('gps.country_id',$this->session->userdata('countriesIndexedArray'));
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('gps.gps_tool_id ASC');
		$this->db->group_by('gps.gps_tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}
} ?>