<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Toolreturn_model extends CI_Model 
{

   
	// For Num of rows goto helpers
	public function crossedReturnDateOrdersResults($searchParams,$task_access)
	{
		$this->db->select('to.*,odt.name as order_type,oda.*,l.name as country_name,concat(u.sso_id,"- ( ",u.name," ) ") as sso,cst.name as name');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['cr_order_number']!='')
			$this->db->like('to.order_number',$searchParams['cr_order_number']);
		if($searchParams['cr_order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['cr_order_delivery_type_id']);
		$this->db->where('to.return_date<',date('Y-m-d'));
		$this->db->where('to.order_type',2);
		$this->db->where('to.current_stage_id',7);
		if(@$searchParams['crd_sso_id']!='')
        {
            $this->db->where('to.sso_id',@$searchParams['crd_sso_id']);
        }
        if($task_access == 1 || $task_access == 2)
        {
            if($task_access == 1)
                $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
            else
                $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
                if($searchParams['crd_country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['crd_country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray')); 
                }
            }
        }
        $this->db->order_by('to.country_id,to.tool_order_id');
		$res = $this->db->get();
		//echo $this->
		//echo $this->db->last_query();exit;
		//echo '<pre>';print_r($res->result_array());exit;
		return $res->result_array();
	}
}	