<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model 
{
	public function getUserRole($user_id = 0)
	{
		$this->db->select('role_id');
		$this->db->from('user');
		$this->db->where('user_id',$user_id);
		$res = $this->db->get();
		if($res->num_rows() > 0)
		{
			$data = $res->row_array();
			$ret = $data['role_id'];
		}
		else $ret = 0;
		return $ret;
	}

	public function getUserDetails($sso_id = 0)
	{
		$dis = array(1,3);//for active users and admin,superadmin
		$this->db->select('u.*, b.name as branch_name,u.location_id as location_id,d.name as designation_name, r.name as role_name,w.name as wh_name,w.wh_id as wh_id,l.name as country_name,l.region_id');
		$this->db->from('user u');
		$this->db->join('role r','u.role_id = r.role_id');
		$this->db->join('designation d','d.designation_id = u.designation_id');
		$this->db->join('branch b','b.branch_id = u.branch_id');
		$this->db->join('warehouse w','w.wh_id = u.wh_id');
		$this->db->join('location l','l.location_id = u.country_id');
		$this->db->where('u.sso_id',$sso_id);
		//$this->db->where('d.status',1);
		//$this->db->where('b.status',1);
		//$this->db->where('r.status',1);
		//$this->db->where('l.status',1);
		$this->db->where('w.status',1);
		$this->db->where_in('u.status',$dis);
		$res = $this->db->get();
		if($res->num_rows() > 0)
		{
			$ret = $res->row_array();
		}
		else $ret = array();
		return $ret;
	}

	/*koushik When logs in */
	public function get_child_roles($parent_role_id)
	{
		$this->db->select('rl.child_role_id,r.name');
		$this->db->from('role_login rl');
		$this->db->join('role r','rl.child_role_id = r.role_id','LEFT');
		$this->db->where('rl.parent_role_id',$parent_role_id);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	// created by maruthi to get list of countries can user access
	public function get_user_countries($sso_id)
	{
		$this->db->select('l.name,l.location_id');
		$this->db->from('location l');
		$this->db->join('user_country uc','uc.country_id = l.location_id');
		$this->db->where('uc.sso_id',$sso_id);
		$this->db->where('uc.status',1);
		$res = $this->db->get();
		return $res->result_array();	
	}

	public function get_user_whs($sso_id)
	{
		$this->db->select('wh.name,wh.wh_id');
		$this->db->from('warehouse wh');
		$this->db->join('user_wh uwh','uwh.wh_id = wh.wh_id');
		$this->db->where('uwh.sso_id',$sso_id);
		$this->db->where('uwh.status',1);
		$res = $this->db->get();
		return $res->result_array();	
	}

	public function getUserBranch($user_id = 0)
	{
		$this->db->select('branch_id');
		$this->db->from('user_branch');
		$this->db->where('user_id',$user_id);
		$res = $this->db->get();
		if($res->num_rows() > 0)
		{
			$data = $res->row_array();
			$ret = $data['branch_id'];
		}
		else $ret = 0;
		return $ret;
	}	

	public function getCustomerFromBranch($branch_id = 0)
	{
		$this->db->select('customer_id');
		$this->db->from('branch');
		$this->db->where('branch_id',$branch_id);
		$res = $this->db->get();
		if($res->num_rows() > 0)
		{
			$data = $res->row_array();
			$ret = $data['customer_id'];
		}
		else $ret = 0;
		return $ret;
	}

	public function getCustomerBranches($customer_id = 0)
	{
		$branch = array();
		$this->db->select('branch_id');
		$this->db->from('branch');
		$this->db->where('customer_id',$customer_id);
		$res = $this->db->get();
		if($res->num_rows() > 0)
		{
			$data = $res->result_array();
			$i = 0;
			foreach($data as $row)
			{
				$branch[$i] = $row['branch_id'];
				$i++;
			}
			$ret = $data['customer_id'];
		}
		return $branch;
	}

	public function is_usernameExist($username)
	{		
		$this->db->select();
		$this->db->where('username',$username);
		$query = $this->db->get('user');
		return ($query->num_rows()>0)?1:0;
	}
}