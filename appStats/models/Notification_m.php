<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification_m extends CI_Model 
{
	public function get_upcoming_calibration_count($task_access)
	{
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->select('count(rc.asset_id) as count');
		}
		if($task_access == 3)
		{
			$this->db->select('l.name as display_name,count(rc.rc_asset_id)as count');
		}
		$this->db->from('rc_asset rc');
		$this->db->join('location l','l.location_id=rc.country_id');
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rc.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rc.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		$this->db->where('rc.current_stage_id',11);
		$this->db->where('rc.rca_type',2);
		$this->db->where('rc.status',1);
		$this->db->group_by('rc.country_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function wh_calibration_total_num_rows_admin($task_access)
    {
    	if($task_access == 1)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id))as count');
    	}
    	else if($task_access == 2)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	else if($task_access == 3)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,l.name as display_name');
    	}
    	$this->db->from('rc_order rco');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('warehouse w','w.wh_id=rco.wh_id');
    	$this->db->join('location l','l.location_id=rco.country_id');
    	
    	if($task_access == 1)
		{
			$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
		}
    	else if($task_access == 2)
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access ==3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
    	$this->db->where('rca.current_stage_id',12);
		$this->db->where('rco.rc_type',2);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('rco.wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('rco.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
    }

    public function calibration_count_notification($current_stage_id,$task_access)
    {
    	if($task_access ==1)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id))as count');
    	}
    	if($task_access ==2)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id))as count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	if($task_access ==3)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id))as count,l.name as display_name');
    	}
    	$this->db->from('rc_order rco');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('warehouse w','w.wh_id=rco.wh_id');
    	$this->db->join('location l','l.location_id=rco.country_id');

    	if($task_access == 1)
    	{
    		$this->db->where('rco.wh_id',$_SESSION['swh_id']);
    	}
    	else if($task_access == 2)
    	{
    		$this->db->where('rco.country_id',$_SESSION['s_country_id']);
    	}
		else if($task_access ==3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		$this->db->where('rca.current_stage_id',$current_stage_id);
		$this->db->where('rco.rc_type',2);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('rco.wh_id');
		}
		if($task_access ==3)
		{
			$this->db->group_by('rco.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
    }

    public function wh_calibration_total_num_rows($task_access)
    {
    	if($task_access ==1)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	if($task_access ==2)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	if($task_access ==3)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) as count,l.name as display_name');
    	}
    	$this->db->from('rc_order rco');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('warehouse w','w.wh_id=rco.wh_id');
    	$this->db->join('location l','l.location_id=rco.country_id');

    	if($task_access == 1)
    	{
    		if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("rco.wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
			}
    	}
		else if($task_access == 2)
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access ==3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);
			}
		}

		$this->db->where('rca.current_stage_id',12);
		$this->db->where('rco.rc_type',2);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('rco.wh_id');
		}
		if($task_access ==3)
		{
			$this->db->group_by('rco.country_id');
		}

		$res = $this->db->get();
		return $res->result_array();
    }
    public function vendor_calibration_total_num_rows($task_access)
	{
		if($task_access ==1)
    	{
    		$this->db->select('count(rco.rc_order_id)as count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(rco.rc_order_id)as count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(rco.rc_order_id)as count,l.name as display_name');
    	}
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
    	$this->db->join('location l','l.location_id=rco.country_id');

    	if($task_access == 1)
		{
			if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("rco.wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
			}
		}
    	else if($task_access == 2)
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access ==3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		
		$this->db->where('rco.rc_type',2);
		$this->db->where('rco.status',2);
		$this->db->where('rca.current_stage_id',13);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('rco.wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('rco.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_repair_count($task_access)
	{
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->select('count(rc.asset_id) as count');
		}
		if($task_access == 3)
		{
			$this->db->select('l.name as display_name,count(rc.rc_asset_id)as count');
		}
		$this->db->from('rc_asset rc');
		$this->db->join('location l','l.location_id=rc.country_id');
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rc.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rc.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		$this->db->where('rc.current_stage_id',20);
		$this->db->where('rc.rca_type',1);
		$this->db->where('rc.status',1);
		$this->db->group_by('rc.country_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function wh_repair_total_num_rows_admin($task_access)
    {
    	if($task_access ==1)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id))as count');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,l.name as display_name');
    	}
    	$this->db->from('rc_order rco');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('warehouse w','w.wh_id=rco.wh_id');
    	$this->db->join('location l','l.location_id=rco.country_id');
    	
    	if($task_access == 1)
    	{
    		$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
    	}
    	else if($task_access == 2)
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access ==3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
    	$this->db->where('rca.current_stage_id',21);
		$this->db->where('rco.rc_type',1);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('rco.wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('rco.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
    }

    public function repair_count_notification($current_stage_id,$task_access)
    {
    	if($task_access ==1)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) as count');
    	}
    	if($task_access ==2)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id))as count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	if($task_access ==3)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id))as count,l.name as display_name');
    	}
    	$this->db->from('rc_order rco');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('warehouse w','w.wh_id=rco.wh_id');
    	$this->db->join('location l','l.location_id=rco.country_id');
		$this->db->where('rca.current_stage_id',$current_stage_id);
		if($task_access == 1)
    	{
    		$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
    	}
    	else if($task_access == 2)
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		if($task_access ==3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		$this->db->where('rco.rc_type',1);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('rco.wh_id');
		}
		if($task_access ==3)
		{
			$this->db->group_by('rco.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
    }

    public function wh_repair_total_num_rows($task_access)
    {
    	if($task_access ==1)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(DISTINCT(rco.rc_order_id)) AS count,l.name as display_name');
    	}
    	$this->db->from('rc_order rco');
    	$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
    	$this->db->join('warehouse w','w.wh_id=rco.wh_id');
    	$this->db->join('location l','l.location_id=rco.country_id');

    	if($task_access == 1)
		{
			if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("rco.wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
			}
		}
		else if($task_access == 2)
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access ==3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);
			}
		}

		$this->db->where('rca.current_stage_id',21);
		$this->db->where('rco.rc_type',1);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('rco.wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('rco.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
    }

    public function vendor_repair_total_num_rows($task_access)
	{
		if($task_access ==1)
    	{
    		$this->db->select('count(rco.rc_order_id)as count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(rco.rc_order_id)as count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(rco.rc_order_id)as count,l.name as display_name');
    	}
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
    	$this->db->join('location l','l.location_id=rco.country_id');

    	if($task_access == 1)
    	{
    		if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("rco.wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
			}
    	}
    	else if($task_access == 2)
		{
			$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access ==3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		
		$this->db->where('rco.rc_type',1);
		$this->db->where('rco.status',2);
		$this->db->where('rca.current_stage_id',22);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('rco.wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('rco.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	public function defective_asset_count($task_access)
	{
		$status = array(1,2);
		if($task_access ==1)
    	{
    		$this->db->select('count(da.defective_asset_id)as count');
    	}
    	if($task_access ==2)
    	{
    		$this->db->select('count(da.defective_asset_id)as count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	if($task_access ==3)
    	{
    		$this->db->select('count(da.defective_asset_id)as count,l.name as display_name');
    	}
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id = da.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('warehouse w','w.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id = a.country_id');
		if($task_access == 1 || $task_access ==2)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		if($task_access == 3)
		{

			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		$this->db->where('da.type',1);
		$this->db->where('a.approval_status',1);
		$this->db->where('da.request_handled_type',0);
		$this->db->where('p.part_level_id',1);
		$this->db->where_in('da.status',$status);
		if($task_access == 2)
		{
			$this->db->group_by('da.wh_id');
		}
		if($task_access ==3)
		{
			$this->db->group_by('a.country_id');
		}
		$res=$this->db->get();
		return $res->result_array();
	}

	public function missed_asset_count($task_access)
	{
		$status = array(1,3);
		if($task_access ==1)
    	{
    		$this->db->select('count(da.defective_asset_id)as count');
    	}
    	if($task_access ==2)
    	{
    		$this->db->select('count(da.defective_asset_id)as count,concat(w.wh_code, "-(" ,w.name,")") as display_name');
    	}
    	if($task_access ==3)
    	{
    		$this->db->select('count(da.defective_asset_id)as count,l.name as display_name');
    	}
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id=da.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
    	if($task_access == 1 || $task_access ==2)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		if(@$task_access == 3)
		{

			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);
			}
		}

		$this->db->where('da.type',2);
		$this->db->where('a.approval_status',1);
		$this->db->where('p.part_level_id',1);
		$this->db->where_in('da.status',$status);
		if($task_access ==2)
		{
			$this->db->group_by('a.wh_id');
		}
		if($task_access ==3)
		{
			$this->db->group_by('a.country_id');
		}
		$res=$this->db->get();
		return $res->result_array();
	}

	public function fe_position_change_count($task_access)
	{
		if($task_access ==1 || $task_access ==2)
    	{
    		$this->db->select('count(fpc.fpc_id)as count');
    	}
    	if($task_access ==3)
    	{
    		$this->db->select('count(fpc.fpc_id)as count,l.name as display_name');
    	}
		$this->db->from('fe_position_change fpc');
		$this->db->join('user u' ,'u.sso_id = fpc.sso_id');
		$this->db->join('location l','l.location_id = u.country_id');
    	if($task_access == 1 || $task_access ==2)
		{
			$this->db->where('u.country_id',$this->session->userdata('s_country_id'));
		}
		if(@$task_access == 3)
		{

			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('u.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				$this->db->where_in('u.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		$this->db->where('fpc.approval_status',1);
		$this->db->group_by('u.country_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function pending_fe_requests($task_access)
	{
		if($task_access ==1)
    	{
    		$this->db->select('count(to.tool_order_id)as count,concat(wh.wh_code, "-(" ,wh.name,")") as display_name');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(to.tool_order_id)as count,concat(wh.wh_code, "-(" ,wh.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(to.tool_order_id)as count,l.name as display_name');
    	}
		$this->db->from('tool_order to');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($task_access == 1 )
		{
			if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("to.wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('to.wh_id',$this->session->userdata('s_wh_id'));
			}
		}
		else if($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
			}
		}
		$this->db->where('to.current_stage_id',5);
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->order_by('to.request_date ASC');
		if($task_access ==1 || $task_access ==2)
		{			
			$this->db->group_by('to.wh_id');
		}
		else if($task_access == 3)
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function pending_st_requests($task_access)
	{
		$arr = array(1,4);
		if($task_access ==1)
    	{
    		$this->db->select('count(to.tool_order_id)as count,concat(wh.wh_code, "-(" ,wh.name,")") as display_name');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(to.tool_order_id)as count,concat(wh.wh_code, "-(" ,wh.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(to.tool_order_id)as count,l.name as display_name');
    	}
		$this->db->from('tool_order to');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id','LEFT');
		$this->db->join('warehouse to_wh','to_wh.wh_id = to.to_wh_id','LEFT');
		$this->db->join('location l','l.location_id = to.country_id','LEFT');
		
		if($task_access==1)
		{
			if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("to.wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('to.wh_id',$this->session->userdata('s_wh_id'));
			}
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
			}
		}
		$this->db->where_in('to.current_stage_id',$arr);
		$this->db->where('to.order_type',1); //Stock Transfer order = 1
		$this->db->order_by('to.request_date ASC');
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('to.wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function acknowledge_wh_requests($task_access)
	{
		if($task_access ==1)
    	{
    		$this->db->select('count(to.tool_order_id)as count,concat(wh_to.wh_code, "-(" ,wh_to.name,")") as display_name');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(to.tool_order_id)as count,concat(wh_to.wh_code, "-(" ,wh_to.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(to.tool_order_id)as count,l.name as display_name');
    	}
		$this->db->from('tool_order to');	
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');	
		$this->db->join('warehouse wh_to','wh_to.wh_id = to.to_wh_id','LEFT');
		$this->db->join('location l','l.location_id = to.country_id','LEFT');		
		
		if($task_access==1)
		{
			if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("to.to_wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('to.to_wh_id',$this->session->userdata('s_wh_id'));
			}
		}
		else if($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));

		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
			}
		}		
		$this->db->where('to.current_stage_id',2);			
		$this->db->where('to.order_type',1);
		$this->db->where('to.fe_check',0);
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('to.to_wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	//koushik
	public function pending_pickup_requests($task_access)
	{
		$return_type = array(1,2,4);
		$approval_status = array(0,2);

		if($task_access ==1)
    	{
    		$this->db->select('count(ro.return_order_id)as count,concat(wh.wh_code, "-(" ,wh.name,")") as display_name');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(ro.return_order_id)as count,concat(wh.wh_code, "-(" ,wh.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(ro.return_order_id)as count,l.name as display_name');
    	}
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('warehouse wh','wh.wh_id = ro.wh_id','LEFT');
		$this->db->join('location l','l.location_id = to.country_id','LEFT');
		if($task_access == 1 )
		{
			if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("ro.wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('ro.wh_id',$this->session->userdata('s_wh_id'));
			}
		}
		else if($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		$this->db->where('osh.current_stage_id',8);//waiting for pickup
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where('address_check!=',1);
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where_in('ro.return_approval',$approval_status);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('ro.wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	public function acknowledge_fe_requests($task_access)
	{
		$return_type = array(1,2);
		if($task_access ==1)
    	{
    		$this->db->select('count(ro.return_order_id)as count,concat(wh.wh_code, "-(" ,wh.name,")") as display_name');
    	}
    	else if($task_access ==2)
    	{
    		$this->db->select('count(ro.return_order_id)as count,concat(wh.wh_code, "-(" ,wh.name,")") as display_name');
    	}
    	else if($task_access ==3)
    	{
    		$this->db->select('count(ro.return_order_id)as count,l1.name as display_name');
    	}
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = ro.country_id');
		$this->db->join('warehouse wh','wh.wh_id = ro.ro_to_wh_id'); 
		$this->db->join('user u','u.sso_id = to.sso_id');
		
		if($task_access == 1 )
		{
			if(isset($_SESSION['whsIndededArray']))
			{
                $this->db->where_in("ro.ro_to_wh_id",$_SESSION['whsIndededArray']); 
			}
            else   
            {         	
				$this->db->where('ro.ro_to_wh_id',$this->session->userdata('s_wh_id'));
			}
		}
		else if($task_access == 2)
		{
			$this->db->where('ro.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('ro.country_id',$_SESSION['countriesIndexedArray']);
			}
		}

		//$this->db->where('ro.ro_to_wh_id',$wh_id);
		$this->db->where('osh.current_stage_id',9);//At Transit from fe to wh
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		if($task_access ==1 || $task_access ==2)
		{
			$this->db->group_by('ro.ro_to_wh_id');
		}
		else if($task_access ==3)
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}


		// created by maruthi on 19thMay'18
	public function owned_order_total_num_rows($task_access)
	{	
		if($task_access ==1 || $task_access == 2)
    	{
    		if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				$this->db->select('count(to.tool_order_id)as count,l.name as display_name');
			}
			else
			{
				$this->db->select('count(to.tool_order_id) as count');	
			}    		
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(to.tool_order_id)as count,l.name as display_name');
    	}
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');		
		$this->db->join('location l','l.location_id = to.country_id','LEFT');	
		$this->db->where('to.current_stage_id=',7);					
		$this->db->where('to.order_type',2);
		if($task_access==1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('to.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
			}
		}
		if($task_access ==3 ||  count($this->session->userdata('countriesIndexedArray'))>1 )
		{
			$this->db->group_by('to.country_id');
		}		
		$res = $this->db->get();
		return $res->result_array();				
	}

	// created by maruthi on 20thMAy'18
	public function fe2_fe_approval_order_results($task_access)
	{
		$return_type = array(3,4);
		//$current_stage = array(4,5,6);
		if($task_access ==1 || $task_access == 2)
    	{
    		$this->db->select('count(from_to.tool_order_id)as count');
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(from_to.tool_order_id)as count,l.name as display_name');
    	}
		$this->db->from('tool_order to');		
		$this->db->join('return_order ro','ro.tool_order_id = to.tool_order_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');		
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('tool_order from_to','from_to.tool_order_id = rto.tool_order_id');
		$this->db->join('user from_user','from_user.sso_id=from_to.sso_id');
		$this->db->join('location l','l.location_id = from_to.country_id','LEFT');			
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where('ro.return_approval',1);	
		$this->db->where('ro.status',1);	
		if($task_access==1)
		{
			$this->db->where('from_to.sso_id',$this->session->userdata('sso_id'));
		}
		elseif($task_access == 2)
		{
			$this->db->where('from_to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('from_to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('from_to.country_id',$_SESSION['countriesIndexedArray']);
			}
		}
		if($task_access == 1)
		{
			$this->db->group_by('from_to.sso_id');
		}
		else if($task_access ==2 || $task_access ==3)
		{
			$this->db->group_by('from_to.country_id');
		}	
		
		$res = $this->db->get();
		return $res->result_array();
	}

	// created by maruthi on 20thMAy'18
	public function fe2_fe_receive_order_results($task_access)
	{
		$return_type = array(3,4);		
		if($task_access ==1 || $task_access == 2)
    	{
    		if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				$this->db->select('count(DISTINCT(to.tool_order_id)) as count,l.name as display_name');
			}
			else
			{
				$this->db->select('count(DISTINCT(to.tool_order_id)) as count');
			}
    		
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(DISTINCT(to.tool_order_id)) as count,l.name as display_name');
    	}
		$this->db->from('tool_order to');		
		$this->db->join('return_order ro','ro.tool_order_id = to.tool_order_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');				
		$this->db->join('order_status_history osh','osh.rto_id=rto.rto_id');	
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('tool_order from_to','from_to.tool_order_id = rto.tool_order_id');
		$this->db->join('user from_user','from_user.sso_id=from_to.sso_id');	
		$this->db->join('location l','l.location_id = to.country_id','LEFT');		
		if($task_access==1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('to.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}	
			}
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
			}
		}
		
		if($task_access ==3 || count($this->session->userdata('countriesIndexedArray'))>1)
		{
			$this->db->group_by('to.country_id');
		}	
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where('ro.return_approval',2);	
		$this->db->where('ro.status',10);
		$this->db->where('osh.current_stage_id',6);	
		$this->db->where('osh.end_time IS NULL');		
		$res = $this->db->get();

		return $res->result_array();
	}

	// createdby maruthi oon 20thMay'18
	public function STforOrder_tool_results($task_access)
	{
		//$status = array('NULL',1,2);
		$tool_order_id_arr = NeedStockTransferOrWhStockTransferApprovalIds($task_access);
		//echo print_r($tool_order_id_arr);exit;
		//$this->db->select('to.*,oda.*,l.name as fe_position');
		if($task_access ==1 || $task_access == 2)
    	{
    		$this->db->select('count(DISTINCT(to.tool_order_id)) as count');
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(DISTINCT(to.tool_order_id)) AS count,l.name as display_name');
    	}
		$this->db->from('tool_order to');	
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('st_tool_order sto','to.tool_order_id = sto.tool_order_id','left');
		$this->db->join('tool_order stn','sto.stock_transfer_id = stn.tool_order_id','left');
		
		$this->db->join('location l','l.location_id = to.country_id');						
		$this->db->where('to.current_stage_id',4);	
		$this->db->where_in('to.order_type',array(1,2));
		$this->db->where('oda.check_address',0);
		$this->db->where('to.fe_check',0);
		$this->db->where_in('to.tool_order_id',$tool_order_id_arr);
		if($task_access==1)
		{
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
			}
		}
		// MOdified By Srilekha
		if($task_access == 1)
		{
			$this->db->group_by('to.sso_id');
		}
		else if($task_access==2 || $task_access ==3)
		{
			$this->db->group_by('to.country_id');
		}		
		$res = $this->db->get();
		return $res->result_array();
	}
	
	// createdby maruthi oon 20thMay'18
	public function calibration_required_results($task_access)
	{
        $status=array(1,2);
        if($task_access ==1 || $task_access == 2)
    	{
    		if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				$this->db->select('count(DISTINCT(to.tool_order_id))as count,l.name as display_name');		
			}
			else
			{
				$this->db->select('count(DISTINCT(to.tool_order_id)) as count');	
			}
    		
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(DISTINCT(to.tool_order_id))as count,l.name as display_name');
    	}   
        $this->db->from('tool_order to');
        $this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
        $this->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');
        $this->db->join('location l','l.location_id = to.country_id');
        
        
		if($task_access == 1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
				{
					$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));					
				}
			}

			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
		}
		else if($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));					
			}
		}
		$this->db->where_in('oah.status',$status);
        $this->db->where('to.current_stage_id',7);
		$this->db->where('to.current_stage_id',7);
		$this->db->join('ordered_asset oa','oa.ordered_asset_id=oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id=oa.asset_id');
		$this->db->join('rc_asset ra','ra.asset_id=a.asset_id');
		$this->db->join('user u','u.sso_id = to.sso_id');		
		$this->db->where('osh.current_stage_id',7);
		$this->db->where('ra.status',1);
		$this->db->where('ra.rca_type',2);
		$where_str = '(ra.current_stage_id = 11 OR a.status = 12)';
		$this->db->where($where_str);
				       
		if($task_access ==3 || count($this->session->userdata('countriesIndexedArray'))>1 )
		{
			$this->db->group_by('to.country_id');
		}
        $res = $this->db->get();
        return $res->result_array(); 
	}
	
	// createdby maruthi oon 20thMay'18
	public function crossedReturnDateOrdersResults($task_access)
	{
		if($task_access ==1 || $task_access == 2)
    	{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				$this->db->select('count(DISTINCT(to.tool_order_id)) as count,l.name as display_name');
			}
			else
			{
				$this->db->select('count(DISTINCT(to.tool_order_id)) as count');	
			}
    		
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(DISTINCT(to.tool_order_id)) as count,l.name as display_name');
    	}
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');	
		$this->db->join('location l','l.location_id = to.country_id','LEFT');
		$this->db->where('to.return_date<',date('Y-m-d'));
		$this->db->where('to.order_type',2);
		$this->db->where('to.current_stage_id',7);		
        if($task_access == 1)
        {
    		if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($this->session->userdata('header_country_id')!='')
	            {
	                $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
	            }
	            else
	            {
	               $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));
	            }	
			}

            $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        else if($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
               $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));
            }
        }

		if($task_access ==3 || count($this->session->userdata('countriesIndexedArray'))>1 )
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	// created by maruthi on 24thMay'18
	public function crossed_expected_delivery_results($task_access)
	{
		//$this->db->select('to.*,od.expected_delivery_date as exp_d_date');
		if($task_access ==1 || $task_access == 2)
    	{
    		$this->db->select('count(to.tool_order_id)as count');
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(to.tool_order_id)as count,l.name as display_name');
    	}
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id=to.tool_order_id');
		$this->db->join('location l','l.location_id=to.country_id');		
		
        if($task_access == 1)
        {
            $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        else if($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
               $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));  
            }
        }
		$this->db->where('to.current_stage_id',6);
		$this->db->where('od.expected_delivery_date<',date('Y-m-d'));
		if($task_access == 1)
		{
			$this->db->group_by('to.sso_id');
		}
		else if($task_access ==2 || $task_access ==3)
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();	
		return $res->result_array();
	}

	// created by maruthi on 24thMay'18
	public function exceededOrderDurationResults($task_access)
	{
		if($task_access ==1 || $task_access == 2)
    	{
    		$this->db->select('count(to.tool_order_id)as count');
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(to.tool_order_id)as count,l.name as display_name');
    	}
        $this->db->from('tool_order to');               
        $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');        
        $this->db->join('user u','u.sso_id = to.sso_id');
        $this->db->join('order_extended_days oed','oed.tool_order_id = to.tool_order_id');
        $this->db->join('location l','l.location_id = to.country_id');
        if($task_access == 1)
        {
            $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        else if($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
               $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));
            }
        }               
        $this->db->where('to.days_approval',1);
        $this->db->where('oed.status',2);
        if($task_access ==1)
		{
			$this->db->group_by('to.sso_id');
		}
        else if($task_access ==2 || $task_access ==3)
		{
			$this->db->group_by('to.country_id');
		}
        $res = $this->db->get();
        return $res->result_array();
	}

	// created by maruthi on 24thMay'18
	public function addressChangeResults($task_access)
	{
		if($task_access ==1 || $task_access == 2)
    	{
    		$this->db->select('count(DISTINCT(to.tool_order_id)) as count');
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(DISTINCT(to.tool_order_id)) as count,l.name as display_name,l.location_id');
    	}
		$this->db->from('tool_order to');				
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id=to.country_id');		
		$this->db->where('to.current_stage_id',4);	
		$this->db->where('oa.check_address',1);	
		$this->db->where('oa.status',1);
		$this->db->where('to.order_delivery_type_id',1);
		if($task_access == 1)
        {
        	$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        else if($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
            	$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));
            }
        } 
        if($task_access == 1)
        {
        	$this->db->group_by('to.sso_id');
        }
        else if($task_access ==2 || $task_access ==3)
		{
			$this->db->group_by('to.country_id');
		}
		$res1 = $this->db->get();
		$result1 = $res1->result_array();


		if($task_access ==1 || $task_access == 2)
    	{
    		$this->db->select('count(DISTINCT(ro.return_order_id)) as count');
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(DISTINCT(ro.return_order_id))as count,l.name as display_name,l.location_id');
    	}
		$this->db->from('return_order ro');				
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');		
		$this->db->join('user u','u.sso_id = ro.created_by');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');		
		$this->db->join('location l','l.location_id=ro.country_id');
		$this->db->where('ro.address_check',1);		
		$this->db->where('ro.order_delivery_type_id',1);
		$this->db->where('ro.status',1);

		if($task_access == 1)
        {
        	$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        else if($task_access == 2)
        {
            $this->db->where('ro.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('ro.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
               $this->db->where_in('ro.country_id',$this->session->userdata('countriesIndexedArray')); 
            }
        } 
        if($task_access == 1)
        {
        	$this->db->group_by('to.sso_id');
        }	
        if($task_access ==2 || $task_access ==3)
		{
			$this->db->group_by('ro.country_id');
		}
		$res2 = $this->db->get();
		$result2 = $res2->result_array();

		$final = array_merge($result1,$result2);
		if($task_access == 1 || $task_access == 2)
		{
			$final_arr = array();
			if(count($final)>0)
			{
				$count1 = (isset($result1[0]['count']))?$result1[0]['count']:0;
				$count2 = (isset($result2[0]['count']))?$result2[0]['count']:0;
				$count = $count1+$count2;
				if($count>0)
				{
					$final_arr[] = array(
						'count' => $count
					);
				}
				return $final_arr;
			}
			else
			{
				return FALSE;
			}
			
		}
		else
		{
			if(count($final)>0)
			{
				$ids_arr = array();
				$location_id_arr = array();
				$location_id_arr_count = array();
				$return_arr = array();
				foreach ($final as $key => $value) {	

					if(in_array($value['location_id'], $ids_arr))
					{
						$new_count = ($value['count'] + $location_id_arr_count[$value['location_id']]);
						$location_id_arr[$value['location_id']] = $value['display_name'];
						$location_id_arr_count[$value['location_id']] = $new_count;
					}
					else
					{
						$location_id_arr[$value['location_id']] = $value['display_name'];
						$location_id_arr_count[$value['location_id']] = $value['count'];
						$ids_arr[] = $value['location_id'];
					}				
				}
				if(count($location_id_arr_count)>0)
				{
					foreach ($location_id_arr_count as $key => $value) {
						$return_arr[] = array(
							'count'   		=> $value,
							'display_name'  => $location_id_arr[$key]
							);
					}
					return $return_arr;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
	}

		// created by maruthi on 19thMay'18
	public function receive_order_results($task_access)
	{
		if($task_access ==1 || $task_access == 2)
    	{
    		if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				$this->db->select('count(to.tool_order_id)as count,l.name as display_name,l.location_id');
			}
			else
			{
				$this->db->select('count(to.tool_order_id)as count');	
			}
    		
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(to.tool_order_id)as count,l.name as display_name,l.location_id');
    	}
		$this->db->from('tool_order to');
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = to.country_id','LEFT');	
		
		if($task_access==1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('to.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
			}
		}		
		$this->db->where('osh.current_stage_id=',6);			
		$this->db->where('to.current_stage_id=',6);	
		$this->db->where('to.status<',2);				
		$this->db->where('to.order_type',2);					
		$this->db->where('osh.rto_id IS NULL');
		$this->db->where('osh.end_time IS NULL');

		if($task_access ==3 || count($this->session->userdata('countriesIndexedArray'))>1 )
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();		
	}
	/*maruthi 11-06-2018*/
	public function raisedSTforOrderResults($closed=0,$task_access=1) // 1 for closed
	{
		if($task_access ==1 || $task_access == 2)
    	{
    		if(count($this->session->userdata('countriesIndexedArray'))>1 )
    		{
    			$this->db->select('count(DISTINCT(to.tool_order_id)) as count,l.name as display_name,l.location_id');
    		}
    		else
    		{
    			$this->db->select('count(DISTINCT(to.tool_order_id)) as count');	
    		}
	   		
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(DISTINCT(to.tool_order_id)) as count,l.name as display_name,l.location_id');
    	}
		$this->db->from('tool_order to');	
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');	
		$this->db->join('st_tool_order sto','sto.tool_order_id = to.tool_order_id');
		$this->db->join('tool_order stn','stn.tool_order_id = sto.stock_transfer_id');
		$this->db->join('location l','l.location_id = to.country_id','LEFT');	
		$this->db->where('to.order_type',2);		
		$this->db->where('to.current_stage_id',4);
		$this->db->where('stn.current_stage_id',2);
		$this->db->where('stn.fe_check = 1');
		if($task_access==1)
		{
			if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('to.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}	
			}
			$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
			}
		}		
		if($task_access ==3 || count($this->session->userdata('countriesIndexedArray'))>1 )
		{
			$this->db->group_by('to.country_id');
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	public function fe_transfer_request($task_access)
	{
		$this->db->select('rto.tool_order_id');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->from('tool_order to','rto.tool_order_id = to.tool_order_id');
		$this->db->join('tool_order to1','to.order_number = to1.fe1_order_number AND to1.country_id = to.country_id');
		$this->db->where('ro.status !=',10);
		$res = $this->db->get();
		$result = $res->result_array();
		$exclude = array();
		if(count($result)>0)
		{
			foreach ($result as $key => $value) 
			{
				$exclude[] = $value['tool_order_id'];
			}
		}
		else
		{
			$exclude[] = 0;
		}
		$exclude = array_unique($exclude);

		if($task_access ==1 || $task_access == 2)
    	{
    		$this->db->select('count(to.tool_order_id)as count');
    	}
    	if($task_access==3)
    	{
    		$this->db->select('count(to.tool_order_id)as count,l.name as display_name,l.location_id');
    	}
		$this->db->from('tool_order to');
		$this->db->join('tool_order to1','to.order_number = to1.fe1_order_number AND to1.country_id = to.country_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($task_access == 1)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}
		else if($task_access ==2)
		{
			$this->db->where('to.country_id',$_SESSION['country_id']);
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if(@$searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		
		$this->db->where_not_in('to.tool_order_id',$exclude);
		$this->db->where('to1.current_stage_id',4);
		$this->db->group_by('to.country_id');
		$res = $this->db->get();
		return $res->result_array();
	}
}
