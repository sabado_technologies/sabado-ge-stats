<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fe2fe_m extends CI_Model 
{

	public function get_check_with_sso($tool_arr)
	{
		$transactionUser = $_SESSION['transactionUser'];
		$task_access = getPageAccess('raise_order',getRoleByUser($transactionUser));
		$country_id = $_SESSION['transactionCountry'];
		$countriesData = get_countries_string(array('order_sso_id'=>$transactionUser),1,$task_access); 
		$this->db->select('u.sso_id,u.name,u.sso_id');
		$this->db->from('tool_order to');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');	
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id AND ap.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		// $this->db->where_in('u.role_id',array(2,5,6));
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$country_id);
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if(@$searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$countriesData);	
				}
			}
		}
		$this->db->where_in('oah.status',array(1,2));
		$this->db->where_in('t.tool_id',$tool_arr);
		$this->db->where('osh.current_stage_id',7);
		$this->db->where_not_in('a.status',array(6,7));
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('u.sso_id');
		$this->db->group_by('u.sso_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	// FE owned tools
	public function fe_owned_tools_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('t.part_number,t.part_description,a.asset_number,a.cal_due_date,t.cal_type_id,to.order_number,a.asset_id,to.country_id, p.serial_number');		
		$this->db->from('tool_order to');	
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');	
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id AND to.sso_id = ap.sso_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);	
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);	
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);	
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);				
		if($task_access==1)
        {
        	if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($_SESSION['header_country_id']!='')
		        {
		            $this->db->where('to.country_id',$_SESSION['header_country_id']);   
		        }
		        else
		        {
		            if($searchParams['myi_country_id']!='')
		            {
		                $this->db->where('to.country_id',$searchParams['myi_country_id']);
		            }
		            else
		            {
		                $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
		            }
		        }
			}
            $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        elseif($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            if($searchParams['users_id']!='')
            {
                $this->db->where('to.sso_id',$searchParams['users_id']);
            }
        }
        else if($task_access == 3)
        {
            if($_SESSION['header_country_id']!='')
            {
                $this->db->where('to.country_id',$_SESSION['header_country_id']);   
            }
            else
            {
                if($searchParams['myi_country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['myi_country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                }
            }
            if($searchParams['users_id']!='')
            {
                $this->db->where('to.sso_id',$searchParams['users_id']);
            }
        } 
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('ap.transit',0);
		$this->db->where('p.part_level_id',1);
		$this->db->where('a.status!=',7);
		$this->db->where('to.current_stage_id!=10');
		$this->db->order_by('to.tool_order_id ASC');
		$this->db->group_by('ap.asset_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}	
	
	public function fe_owned_tools_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('t.part_number,t.part_description,a.asset_number,to.order_number,a.asset_id');
		$this->db->from('tool_order to');	
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id AND to.sso_id = ap.sso_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);	
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);	
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);	
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);		
		 //$this->db->where('ap.sso_id',$_SESSION['sso_id']);
		//$this->db->where('to.sso_id',$_SESSION['sso_id']);
		if($task_access==1)
        {
        	if(count($this->session->userdata('countriesIndexedArray'))>1)
			{
				if($_SESSION['header_country_id']!='')
		        {
		            $this->db->where('to.country_id',$_SESSION['header_country_id']);   
		        }
		        else
		        {
		            if($searchParams['myi_country_id']!='')
		            {
		                $this->db->where('to.country_id',$searchParams['myi_country_id']);
		            }
		            else
		            {
		                $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
		            }
		        }
			}
            $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        elseif($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            if($searchParams['users_id']!='')
            {
                $this->db->where('to.sso_id',$searchParams['users_id']);
            }
        }
        else if($task_access == 3)
        {
            if($_SESSION['header_country_id']!='')
            {
                $this->db->where('to.country_id',$_SESSION['header_country_id']);   
            }
            else
            {
                if($searchParams['myi_country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['myi_country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                }
            }
            if($searchParams['users_id']!='')
            {
                $this->db->where('to.sso_id',$searchParams['users_id']);
            }
        }  

		$this->db->where('ap.transit',0);
		$this->db->where('p.part_level_id',1);
		$this->db->where('a.status!=',7);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('to.current_stage_id!=10');
		$this->db->group_by('ap.asset_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function toolsAvailabilityWithFEtotal_num_rows($searchParams,$tool_arr)
	{
		$transactionUser = $_SESSION['transactionUser'];
		$task_access = getPageAccess('raise_order',getRoleByUser($transactionUser));
		$country_id = $_SESSION['transactionCountry'];
		$countriesData = get_countries_string(array('order_sso_id'=>$transactionUser),1,$task_access); 
		$orderRole = getTaskRoles('raise_order');		
		$this->db->select('t.part_number,t.part_description,a.asset_number,
			to.order_number,a.asset_id,concat(u.sso_id," - (",u.name," )") as sso,l.name as countryName');
		$this->db->from('tool_order to');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');	
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id AND ap.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);	
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);	
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);	
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);		
		if($searchParams['ser_sso_id']!='')
		{
			$this->db->where('u.sso_id',$searchParams['ser_sso_id']);
			$this->db->where('to.sso_id',$searchParams['ser_sso_id']);
		}
		// $this->db->where_in('u.role_id',array(2,5,6));
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$country_id);
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if(@$searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$countriesData);	
				}
			}
		}
		$this->db->where_in('u.role_id',$orderRole);
		$this->db->where_in('oah.status',array(1,2));
		$this->db->where_in('t.tool_id',$tool_arr);		
		$this->db->where('osh.current_stage_id',7);
		$this->db->where_not_in('a.status',array(6,7));
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('oa.asset_id');
		$this->db->order_by('to.country_id,to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function toolsAvailabilityWithFEresults($current_offset, $per_page, $searchParams,$tool_arr)
	{	
		$transactionUser = $_SESSION['transactionUser'];
		$task_access = getPageAccess('raise_order',getRoleByUser($transactionUser));
		$country_id = $_SESSION['transactionCountry'];
		$countriesData = get_countries_string(array('order_sso_id'=>$transactionUser),1,$task_access); 
		$orderRole = getTaskRoles('raise_order');
		$this->db->select('t.part_number,t.part_description,a.asset_number,
			to.order_number,a.asset_id,concat(u.sso_id," - (",u.name," )") as sso,l.name as countryName');
		$this->db->from('tool_order to');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');	
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id AND ap.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);	
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);	
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);	
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);		
		if($searchParams['ser_sso_id']!='')
		{
			$this->db->where('u.sso_id',$searchParams['ser_sso_id']);
			$this->db->where('to.sso_id',$searchParams['ser_sso_id']);
		}
		
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('to.country_id',$country_id);
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if(@$searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$countriesData);	
				}
			}
		}		
		$this->db->where_in('u.role_id',$orderRole);
		$this->db->where_in('oah.status',array(1,2));
		$this->db->where_in('t.tool_id',$tool_arr);		
		$this->db->where('osh.current_stage_id',7);
		$this->db->where_not_in('a.status',array(6,7));
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('oa.asset_id');
		$this->db->order_by('to.country_id,to.tool_order_id');		
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();		
		return $res->result_array();
	}

	public function toolsNeededByFEtotal_num_rows($searchParams)
	{
		$this->db->select('to.tool_order_id');
		$this->db->from('tool_order to');		
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');	
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->join('user u','u.sso_id = to.sso_id');	
		$this->db->join('location l','l.location_id = to.country_id');		
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);	
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);	
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['ser_sso_id']!='')
			$this->db->like('u.sso_id',$searchParams['ser_sso_id']);		
		$this->db->where('to.current_stage_id',4);
		$this->db->where_in('u.role_id',array(2,5,6));
		$this->db->where('ot.status',2);
		$this->db->where('to.country_id',$searchParams['country_id']);
		$this->db->where_in('ot.tool_id',$_SESSION['return_tool_arr']);
		$this->db->order_by('to.country_id,to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}


	public function toolsNeededByFEresults($current_offset, $per_page, $searchParams)
	{
		$this->db->select('t.part_number,t.part_description,t.tool_id,
			to.order_number,concat(u.sso_id," - (",u.name," )") as sso,l.name as countryName,ot.quantity');
		$this->db->from('tool_order to');		
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');	
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->join('user u','u.sso_id = to.sso_id');	
		$this->db->join('location l','l.location_id = to.country_id');		
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);	
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);	
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['ser_sso_id']!='')
			$this->db->like('u.sso_id',$searchParams['ser_sso_id']);		
		$this->db->where('to.current_stage_id',4);	
		$this->db->where_in('u.role_id',array(2,5,6));
		$this->db->where('ot.status',2);
		$this->db->where('to.country_id',$searchParams['country_id']);
		$this->db->where_in('ot.tool_id',$_SESSION['return_tool_arr']);
		$this->db->order_by('to.country_id,to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function fe2_fe_approval_order_results($current_offset, $per_page, $searchParams,$task_access)
	{
		
		$return_type = array(3,4);
		//$current_stage = array(4,5,6);
		$this->db->select('to.*,rto.rto_id,odt.name as order_type,rt.name as return_type,ro.return_number,concat(u.sso_id,"-",u.name,", ",to.order_number) as to_sso,concat(from_user.sso_id,"-",from_user.name,", ",from_to.order_number) as from_sso,l.name as country_name');
		$this->db->from('tool_order to');	
		$this->db->join('location l','l.location_id = to.country_id','LEFT');		
		$this->db->join('return_order ro','ro.tool_order_id = to.tool_order_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');	
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');		
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('tool_order from_to','from_to.tool_order_id = rto.tool_order_id');
		$this->db->join('user from_user','from_user.sso_id=from_to.sso_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));		
		if($searchParams['fe2_fe_sso_id']!='')
			$this->db->where('from_to.sso_id',($searchParams['fe2_fe_sso_id']));	
		//$this->db->where_in('osh.current_stage_id',7);
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where('ro.return_approval',1);	
		$this->db->where('ro.status',1);	
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
				$this->db->where('from_to.sso_id',$this->session->userdata('sso_id'));
			else
				$this->db->where('from_to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('from_to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['fe2_fe_country_id']!='')
				{
					$this->db->where('from_to.country_id',$searchParams['fe2_fe_country_id']);
				}
				else
				{
					$this->db->where_in('from_to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('ro.return_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function fe2_fe_approval_order_total_num_rows($searchParams,$task_access)
	{
		$return_type = array(3,4);
		$this->db->select('ro.return_order_id');
		$this->db->from('tool_order to');
		$this->db->join('location l','l.location_id = to.country_id','LEFT');	
		$this->db->join('return_order ro','ro.tool_order_id = to.tool_order_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');		
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['deploy_date']!='')
			$this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));		
		if($searchParams['fe2_fe_sso_id']!='')
			$this->db->where('to.sso_id',($searchParams['fe2_fe_sso_id']));
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where('return_approval',1);	
		$this->db->where('ro.status',1);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			else
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['fe2_fe_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['fe2_fe_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('ro.return_order_id ASC');	
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function get_return_initialted_assets($rto_id,$ack = 0)
	{
		$this->db->select('osh.tool_order_id as fe1_tool_order_id, osh.order_status_id as fe1_order_status_id, oah.*, a.asset_number, a.asset_id, t.part_number, t.part_description, as.name as asset_status, p.serial_number');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_condition as','as.asset_condition_id = osh.status');
		$this->db->where('osh.rto_id',$rto_id);
		$this->db->where('p.part_level_id',1);
		if($ack == 1)
			$this->db->where('osh.current_stage_id',6);  // this will fetch the 8th and 6th stage data
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function fe2_fe_receive_order_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$return_type = array(3,4);
		$this->db->select('to.*,rto.rto_id,odt.name as order_type,ro.return_number,rt.name as return_type,
			to.order_number as to_order,concat(u.sso_id,"-",u.name) as to_sso,
			from_to.order_number as from_order,concat(from_user.sso_id,"-",from_user.name) as from_sso');			
		$this->db->from('tool_order to');		
		$this->db->join('return_order ro','ro.tool_order_id = to.tool_order_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');				
		$this->db->join('order_status_history osh','osh.rto_id=rto.rto_id');	
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('tool_order from_to','from_to.tool_order_id = rto.tool_order_id');
		$this->db->join('user from_user','from_user.sso_id=from_to.sso_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['ack_fe_sso_id']!='')
			$this->db->where('to.sso_id',($searchParams['ack_fe_sso_id']));
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['ack_fe_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['ack_fe_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}			
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ack_fe_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['ack_fe_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where('ro.return_approval',2);
		$this->db->where('ro.status',10);
		$this->db->where('osh.current_stage_id',6);	
		$this->db->where('osh.end_time IS NULL');
		$this->db->order_by('to.tool_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();

		return $res->result_array();
	}
	
	public function fe2_fe_receive_order_total_num_rows($searchParams,$task_access)
	{
		$return_type = array(3,4);
		$this->db->select('to.*,rto.rto_id,odt.name as order_type,ro.return_number,rt.name as return_type,
			to.order_number as to_order,concat(u.sso_id,"-",u.name) as to_sso,
			from_to.order_number as from_order,concat(from_user.sso_id,"-",from_user.name) as from_sso');			
		$this->db->from('tool_order to');		
		$this->db->join('return_order ro','ro.tool_order_id = to.tool_order_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');		
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');				
		$this->db->join('order_status_history osh','osh.rto_id=rto.rto_id');	
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('tool_order from_to','from_to.tool_order_id = rto.tool_order_id');
		$this->db->join('user from_user','from_user.sso_id=from_to.sso_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['ack_fe_sso_id']!='')
			$this->db->where('to.sso_id',($searchParams['ack_fe_sso_id']));
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['ack_fe_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['ack_fe_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}			
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ack_fe_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['ack_fe_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where('ro.return_approval',2);
		$this->db->where('ro.status',10);
		$this->db->where('osh.current_stage_id',6);	
		$this->db->where('osh.end_time IS NULL');
		$this->db->order_by('to.tool_order_id ASC');	
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function get_fe_owned_order_asset_id($fe1_tool_order_id,$ordered_asset_id)
	{
		$this->db->Select('oah.oah_id');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','osh.order_status_id = oah.order_status_id');
		$this->db->where('osh.current_stage_id',7);
		$this->db->where('osh.tool_order_id',$fe1_tool_order_id);
		$this->db->where('oah.ordered_asset_id',$ordered_asset_id);
		$res = $this->db->get();
		$result = $res->row_array();
		return $result['oah_id'];
	}

	public function fe_transfer_req_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('concat(u1.sso_id,"-",u1.name,", ",to.order_number) as from_sso,concat(u2.sso_id,"-",u2.name,", ",to1.order_number) as to_sso');
		$this->db->from('tool_order to');
		$this->db->join('tool_order to1','to.order_number = to1.fe1_order_number AND to1.country_id = to.country_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');	
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('user u1','u1.sso_id = to.sso_id');
		$this->db->join('user u2','u2.sso_id = to1.sso_id');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id AND ap.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['from_sso']!='')
			$this->db->like('to.sso_id',$searchParams['from_sso']);	
		if($searchParams['to_sso']!='')
			$this->db->like('to1.sso_id',$searchParams['to_sso']);	
		if($searchParams['from_return_date']!='')
			$this->db->like('to.return_date',$searchParams['from_return_date']);	
		if($searchParams['to_request_date']!='')
			$this->db->like('to1.request_date',$searchParams['to_request_date']);
		if($task_access == 1)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}
		else if($task_access ==2)
		{
			$this->db->where('to.country_id',$_SESSION['country_id']);
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if(@$searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where_in('oah.status',array(1,2));
		$this->db->where('osh.current_stage_id',7);
		$this->db->where('to1.current_stage_id',4);
		$this->db->where_not_in('a.status',array(6,7));
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function fe_transfer_req_results($current_offset,$per_page,$searchParams,$task_access)
	{
		$this->db->select('concat(u1.sso_id,"-",u1.name,", ",to.order_number) as from_sso,concat(u2.sso_id,"-",u2.name,", ",to1.order_number) as to_sso,to.return_date,to1.request_date,to1.tool_order_id,l.name as country_name');
		$this->db->from('tool_order to');
		$this->db->join('tool_order to1','to.order_number = to1.fe1_order_number AND to1.country_id = to.country_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');	
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('user u1','u1.sso_id = to.sso_id');
		$this->db->join('user u2','u2.sso_id = to1.sso_id');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id AND ap.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['from_sso']!='')
			$this->db->like('to.sso_id',$searchParams['from_sso']);	
		if($searchParams['to_sso']!='')
			$this->db->like('to1.sso_id',$searchParams['to_sso']);	
		if($searchParams['from_return_date']!='')
			$this->db->like('to.return_date',$searchParams['from_return_date']);	
		if($searchParams['to_request_date']!='')
			$this->db->like('to1.request_date',$searchParams['to_request_date']);
		if($task_access == 1)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}
		else if($task_access ==2)
		{
			$this->db->where('to.country_id',$_SESSION['country_id']);
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if(@$searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where_in('oah.status',array(1,2));
		$this->db->where('osh.current_stage_id',7);
		$this->db->where('to1.current_stage_id',4);
		$this->db->where_not_in('a.status',array(6,7));
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_asset_list($tool_order_id)
	{
		$this->db->select('t.part_number,t.part_description,ot.quantity as quantity');
		$this->db->from('ordered_tool ot');
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->where('ot.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function download_ack_from_fe($searchParams,$task_access)
	{
		$return_type = array(3,4);
		$this->db->select('to.tool_order_id,to.request_date,to.return_date,
			rto.rto_id,odt.name as order_type,ro.return_number,
			rt.name as return_type,
			to.order_number as to_order,
			concat(u.sso_id,"-",u.name) as to_sso,
			from_to.order_number as from_order,
			concat(from_user.sso_id,"-",from_user.name) as from_sso,
			l.name as country_name');		
		$this->db->from('tool_order to');
		$this->db->join('location l','l.location_id = to.country_id');	
		$this->db->join('return_order ro','ro.tool_order_id = to.tool_order_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');				
		$this->db->join('order_status_history osh','osh.rto_id=rto.rto_id');	
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id=to.sso_id');
		$this->db->join('tool_order from_to','from_to.tool_order_id = rto.tool_order_id');
		$this->db->join('user from_user','from_user.sso_id=from_to.sso_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		if($searchParams['ack_fe_sso_id']!='')
			$this->db->where('to.sso_id',($searchParams['ack_fe_sso_id']));
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access == 1)
			{
				if(count($this->session->userdata('countriesIndexedArray'))>1)
				{
					if($this->session->userdata('header_country_id')!='')
					{
						$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
					}
					else
					{
						if($searchParams['ack_fe_country_id']!='')
						{
							$this->db->where('to.country_id',$searchParams['ack_fe_country_id']);
						}
						else
						{
							$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
						}
					}			
				}
				$this->db->where('to.sso_id',$this->session->userdata('sso_id'));
			}
			else
			{
				$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			}
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('to.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['ack_fe_country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['ack_fe_country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where('ro.return_approval',2);
		$this->db->where('ro.status',10);
		$this->db->where('osh.current_stage_id',6);	
		$this->db->where('osh.end_time IS NULL');
		$this->db->order_by('to.tool_order_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_assets_in_ack_from_fe($rto_id)
	{
		$this->db->select('t.part_number, t.part_description, a.asset_number,as.name as asset_status, a.asset_id');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('osh.rto_id',$rto_id);
		$this->db->where('p.part_level_id',1);
		$this->db->where('osh.current_stage_id',6);  // this will fetch the 8th and 6th stage data
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
}	