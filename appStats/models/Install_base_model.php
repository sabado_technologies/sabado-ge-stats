<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Install_base_model extends CI_Model 
{
	//starting of Install Base crud methods
	public function ib_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('ib.install_base_id');
		$this->db->from('customer c');
		$this->db->join('customer_site cs','cs.customer_id = c.customer_id');
		$this->db->join('location l','l.location_id = cs.location_id');
		$this->db->join('location l1','l1.location_id=c.country_id');
		$this->db->join('install_base ib','ib.customer_site_id = cs.customer_site_id');
		if($searchParams['customer_name']!='')
			$this->db->like('c.name',$searchParams['customer_name']);
		if($searchParams['customer_number']!='')
			$this->db->like('c.customer_number',$searchParams['customer_number']);
		if($searchParams['site_id']!='')
			$this->db->like('cs.site_id',$searchParams['site_id']);
		if($searchParams['system_id']!='')
			$this->db->like('ib.system_id',$searchParams['system_id']);
		if($searchParams['asset_number']!='')
			$this->db->like('ib.asset_number',$searchParams['asset_number']);
		if($searchParams['status']!='')
			$this->db->like('ib.status',$searchParams['status']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('ib.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('ib.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('ib.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('ib.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function ib_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('c.name,c.customer_number,ib.system_id,ib.install_base_id,
				           ib.asset_number,ib.product_description,cs.site_id,
				           ib.status as ib_status,l1.name as country');
		$this->db->from('customer c');
		$this->db->join('customer_site cs','cs.customer_id = c.customer_id');
		$this->db->join('location l','l.location_id = cs.location_id');
		$this->db->join('location l1','l1.location_id=c.country_id');
		$this->db->join('install_base ib','ib.customer_site_id = cs.customer_site_id');
		if($searchParams['customer_name']!='')
			$this->db->like('c.name',$searchParams['customer_name']);
		if($searchParams['customer_number']!='')
			$this->db->like('c.customer_number',$searchParams['customer_number']);
		if($searchParams['site_id']!='')
			$this->db->like('cs.site_id',$searchParams['site_id']);
		if($searchParams['system_id']!='')
			$this->db->like('ib.system_id',$searchParams['system_id']);
		if($searchParams['asset_number']!='')
			$this->db->like('ib.asset_number',$searchParams['asset_number']);
		if($searchParams['status']!='')
			$this->db->like('ib.status',$searchParams['status']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('ib.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('ib.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('ib.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('ib.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_records_by_ib($install_base_id)
	{
		$this->db->select('c.*,ib.*,cs.*');
		$this->db->from('customer c');
		$this->db->join('customer_site cs','cs.customer_id = c.customer_id');
		$this->db->join('location l','l.location_id = cs.location_id');
		$this->db->join('install_base ib','ib.customer_site_id = cs.customer_site_id');
		$this->db->where('ib.install_base_id',$install_base_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_location_area($country_id)
	{
		$this->db->select('l.name as city_name,l1.name as state_name,l.location_id');
		$this->db->from('location l');//city
		$this->db->join('location l1','l1.location_id = l.parent_id');//state
		$this->db->join('location l2','l2.location_id = l1.parent_id');//zone
		$this->db->join('location l3','l3.location_id = l2.parent_id');//country
		$this->db->where('l.level_id',5);
		$this->db->where('l.status',1);
		if($country_id !=0)
		$this->db->where('l3.location_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function is_ib_system_idExist($data)
	{
		$this->db->from('install_base');
		$this->db->where('system_id', $data['system_id']);
		$this->db->where('country_id',$data['country_id']);
		if($data['install_base_id']!='')
		$this->db->where_not_in('install_base_id', $data['install_base_id']);
		$res = $this->db->get();
		return $res->num_rows();	
	}

	public function is_ib_site_id_Exist($data)
	{
		$this->db->from('customer_site cs');
		$this->db->join('customer c','c.customer_id = cs.customer_id');
		$this->db->where('cs.site_id', $data['site_id']);
		$this->db->where('cs.country_id',$data['country_id']);
		if($data['customer_num']!='')
		$this->db->where_not_in('c.customer_number', $data['customer_num']);
		if($data['customer_site_id']!='')
		$this->db->where_not_in('cs.customer_site_id', $data['customer_site_id']);
		$res = $this->db->get();
		return $res->num_rows();	
	}

	public function get_customer_details_by_id($customer_id,$country_id)
	{
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('customer_number',$customer_id);
		$this->db->where('country_id',$country_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function missed_ib_total_num_rows($upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_install_base');
		$this->db->where('upload_id',$upload_id);
		$this->db->order_by('upload_install_base_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function missed_ib_results($current_offset, $per_page, $upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_install_base');
		$this->db->where('upload_id',$upload_id);
		$this->db->order_by('upload_install_base_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function check_location_city($country_id,$city_name)
	{
		$this->db->select('l.location_id');
		$this->db->from('location l');//city
		$this->db->join('location l1','l1.location_id = l.parent_id');//state
		$this->db->join('location l2','l2.location_id = l1.parent_id');//zone
		$this->db->join('location l3','l3.location_id = l2.parent_id');//country
		$this->db->where('l.level_id',5);
		$this->db->where('l.status',1);
		$this->db->like('l.name',$city_name);
		if($country_id !=0)
		$this->db->where('l3.location_id',$country_id);
		$res = $this->db->get();
		$result = $res->row_array();
		if(isset($result['location_id']))
		{
			return $result['location_id'];
		}
		else
		{
			return 0;
		}
	}
}	
