<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wh_stock_transfer_m extends CI_Model 
{
	public function st_total_num_rows($searchParams,$task_access)
	{
		$arr = array(1,4);
		$this->db->select('to.tool_order_id');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','LEFT');
		$this->db->join('tool_order to2','to2.tool_order_id = sto.tool_order_id','LEFT');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id','LEFT');
		$this->db->join('warehouse to_wh','to_wh.wh_id = to.to_wh_id','LEFT');
		$this->db->join('location l','l.location_id = to_wh.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = to.country_id','LEFT');
		$this->db->join('user u','u.sso_id = to2.sso_id','LEFT');
		if($searchParams['ssoid']!='')
		{
			$this->db->where('to.fe_check',1);
			$this->db->where('to2.sso_id',$searchParams['ssoid']);
		}
		if(@$searchParams['stn_number']!='')
			$this->db->like('to.stn_number',$searchParams['stn_number']);
		if(@$searchParams['courier_date']!='')
			$this->db->like('to.request_date',format_date($searchParams['courier_date']));
		if($searchParams['to_wh_id']!='')
		{
			$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
		}
		//$this->db->where('to.wh_id',$swh_id);
		$this->db->where_in('to.current_stage_id',$arr);
		$this->db->where('to.order_type',1); //Stock Transfer order = 1
		
		if($task_access == 1 )
		{
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['from_wh_id']=='')
					$this->db->where_in("to.wh_id",$_SESSION['whsIndededArray']); 
				else				
					$this->db->where('to.wh_id',$this->session->userdata('swh_id'));	
			}
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
		}
		$this->db->where('oda.status',1);
		$this->db->order_by('to.request_date ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function st_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$arr = array(1,4);
		//$swh_id = $this->session->userdata('swh_id');
		$this->db->select('date_sub(to.request_date, INTERVAL 2 DAY) as courier_date,
						   to.*,l.name as location_name,
						   concat(oda.address3,",",oda.address4) as destination, 
						   concat(wh.wh_code, "- (" ,wh.name,")") as from_wh_name,
						   concat(to_wh.wh_code, "- (" ,to_wh.name,")") as to_wh_name,
						   concat(u.sso_id, "- (" ,u.name,")") as user_name,
						   l1.name as country_name');
		$this->db->from('tool_order to');
		$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','LEFT');
		$this->db->join('tool_order to2','to2.tool_order_id = sto.tool_order_id','LEFT');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id','LEFT');
		$this->db->join('warehouse to_wh','to_wh.wh_id = to.to_wh_id','LEFT');
		$this->db->join('location l','l.location_id = to_wh.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = to.country_id','LEFT');
		$this->db->join('user u','u.sso_id = to2.sso_id','LEFT');
		if($searchParams['ssoid']!='')
		{
			$this->db->where('to.fe_check',1);
			$this->db->where('to2.sso_id',$searchParams['ssoid']);
		}
		if($searchParams['to_wh_id']!='')
		{
			$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
		}
		if($searchParams['stn_number']!='')
			$this->db->like('to.stn_number',$searchParams['stn_number']);
		if($searchParams['courier_date']!='')
			$this->db->like('to.request_date',format_date($searchParams['courier_date']));
		//$this->db->where('to.wh_id',$swh_id);
		$this->db->where_in('to.current_stage_id',$arr);
		$this->db->where('to.order_type',1); //Stock Transfer order = 1
		if($task_access == 1 )
		{
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['from_wh_id']=='')
					$this->db->where_in("to.wh_id",$_SESSION['whsIndededArray']); 
				else				
					$this->db->where('to.wh_id',$this->session->userdata('swh_id'));	
			}
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
		}
		$this->db->where('oda.status',1);
		$this->db->order_by('to.request_date ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_st_tools($tool_order_id)
	{
		$this->db->select('t.part_number,t.part_description,ot.quantity');
		$this->db->from('ordered_tool ot');
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->where('ot.tool_order_id',$tool_order_id);
		$this->db->where('ot.status',1);
		$this->db->order_by('ot.ordered_tool_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_tool_order_details($tool_order_id)
	{
		$this->db->select('to.stn_number,to.request_date,oa.*,to.fe_check,to.to_wh_id,l.name as location_name,to.wh_id,to.country_id');
		$this->db->from('tool_order to');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('warehouse wh','to.to_wh_id =wh.wh_id','left'); // modified by maruthi to get destination when sending to warehouse
		$this->db->join('location l','l.location_id = wh.location_id','left') ; // modified by maruthi to get destination when sending to warehouse 
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	/*koushik 26-04-2018*/
	public function get_fe_sso($stock_transfer_id)
	{
		$this->db->select('concat(u.sso_id, "-(" ,u.name,")") as sso_name,u.sso_id,u.mobile_no');
		$this->db->from('tool_order to1');
		$this->db->join('st_tool_order sto','sto.stock_transfer_id = to1.tool_order_id','LEFT');
		$this->db->join('tool_order to2','to2.tool_order_id = sto.tool_order_id','LEFT');
		$this->db->join('user u','u.sso_id = to2.sso_id','LEFT');
		$this->db->where('to1.tool_order_id',$stock_transfer_id);// modified by maruthi on 28th april to get shipto address at landing screen
		$res = $this->db->get();
		return $res->row_array();
	}


	public function get_ordered_tool_count($tool_order_id)
	{
		$this->db->select('t.part_number,t.part_description,ot.quantity,ot.ordered_tool_id, GROUP_CONCAT(a.asset_number SEPARATOR ",") as assets,ot.tool_id,count(a.asset_number) as asset_count, GROUP_CONCAT(p.serial_number SEPARATOR ",") as serial_nos');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oas','oas.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oas.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('to.order_type',1);
		$this->db->where('p.part_level_id',1);
		$this->db->where('ot.status',1); //only active tools
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_ordered_tool($tool_order_id,$country_id,$check_st_type)
	{
		$this->db->select('to.tool_order_id,t.part_number,t.part_description,ot.quantity,ot.ordered_tool_id,ot.tool_id');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('tool t','t.tool_id = ot.tool_id');
		$this->db->join('ordered_asset oas','oas.ordered_tool_id = ot.ordered_tool_id','LEFT');
		$this->db->join('order_asset_history oah','oah.ordered_asset_id = oas.ordered_asset_id','LEFT');
		$this->db->join('asset a','a.asset_id = oas.asset_id','LEFT');
		$this->db->where('to.tool_order_id',$tool_order_id);
		if($check_st_type == 0)
		{
			$this->db->where('a.approval_status',0);
		}
		$this->db->where('ot.status',1); //only active tools
		$this->db->where('to.country_id',$country_id);
		$this->db->group_by('ot.ordered_tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_avail_tool_for_wh_to_wh($ordered_tool_id,$wh_id)
	{
		$this->db->select('count(a.asset_id) as num');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset_position ap','oa.asset_id = ap.asset_id');
		$this->db->join('asset a','a.asset_id = ap.asset_id');
		$this->db->where('ap.wh_id IS NOT NULL');
		$this->db->where('ap.transit',0);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('a.approval_status',0);
		$this->db->where('a.wh_id',$wh_id);
		$this->db->where('ot.ordered_tool_id',$ordered_tool_id);
		$res = $this->db->get();
		$result = $res->row_array();
		if($result['num']!='')
		{
			$number = $result['num'];
		}
		else
		{
			$number = 0;
		}
		return $number;
	}

	public function get_avail_tool($tool_id,$tool_order_wh_id)
	{
		/*kosuhik 04-06-2018*/
		$status = array(1,2);
		$this->db->select('count(a.asset_id) as num');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_position ap','a.asset_id = ap.asset_id');
		$this->db->where('ap.wh_id IS NOT NULL');
		$this->db->where('ap.transit',0);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('a.approval_status',0);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->where('p.part_level_id',1); // 1 = L0 level
		$this->db->where_in('a.status',$status);
		$res = $this->db->get();
		$result = $res->row_array();
		if($result['num']!='')
		{
			$number = $result['num'];
		}
		else
		{
			$number = 0;
		}
		return $number;
	}

	public function check_asset_scanned($ordered_tool_id,$asset_id)
	{
		$this->db->select('oah.oah_id');
		$this->db->from('ordered_asset oa');
		$this->db->join('order_asset_history oah','oah.ordered_asset_id = oa.ordered_asset_id');
		$this->db->where('oa.ordered_tool_id',$ordered_tool_id);
		$this->db->where('oa.asset_id',$asset_id);
		$res = $this->db->get();
		return $res->row();
	}
	public function get_asset_number($ordered_tool_id,$asset_id)
	{
		$this->db->select('a.asset_number');
		$this->db->from('ordered_asset oa');
		$this->db->join('order_asset_history oah','oah.ordered_asset_id = oa.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->where('oa.ordered_tool_id',$ordered_tool_id);
		$this->db->where('oa.asset_id',$asset_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_ordered_tool_detail($ordered_tool_id)
    {
        $this->db->select('to.tool_order_id,t.part_number,t.part_description,
                           to.stn_number,wh.name as wh_name,wh.wh_code,
                           l.name as location_name,
                           ot.ordered_tool_id,ot.tool_id,to.fe_check,CONCAT(oa.address3,", ",oa.address4) AS destination');
        $this->db->from('ordered_tool ot');
        $this->db->join('tool_order to','to.tool_order_id = ot.tool_order_id');
        $this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
        $this->db->join('tool t','t.tool_id = ot.tool_id');
        $this->db->join('warehouse wh','to.to_wh_id = wh.wh_id','left'); // modifieb by maruthi to get the stock tranfer to fe assets
        $this->db->join('location l','l.location_id = wh.location_id','left');  // modifieb by maruthi to get the stock tranfer to fe assets
        $this->db->where('ot.ordered_tool_id',$ordered_tool_id);
        $this->db->where('oa.status',1);
        $res = $this->db->get();
        //echo $this->db->last_query();exit;
        return $res->row_array();
    }

	public function get_tool_list($tool_id,$tool_order_wh_id)
	{
		//$swh_id = $this->session->userdata('swh_id');
		$this->db->select('a.asset_id');
		$this->db->from('tool t');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('asset_position ap','a.asset_id = ap.asset_id');
		$this->db->where('ap.wh_id!=',NULL);
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('p.part_level_id',1);
		$this->db->where('p.part_status',1);
		$this->db->where('a.approval_status',0);
		$this->db->where('a.status',1);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function get_sub_inventory_asset($tool_id,$tool_order_wh_id,$asset_arr)
	{
		$status = array(1,2);
		$this->db->select('as.name as asset_status,t.part_number,a.asset_id,p.serial_number,
						   t.part_description,a.asset_number,a.sub_inventory');
		$this->db->from('tool t');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('asset_position ap','a.asset_id = ap.asset_id');
		$this->db->where('ap.wh_id IS NOT NULL');
		$this->db->where('ap.to_date IS NULL'); // modified by maruthi because 
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('p.part_level_id',1);
		$this->db->where('p.part_status',1);
		$this->db->where('a.approval_status',0);
		$this->db->where_in('a.status',$status);
		$this->db->where_not_in('a.asset_id',$asset_arr);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_sub_inventory_for_wh_to_wh($ordered_tool_id,$asset_arr,$tool_order_wh_id)
	{
		$this->db->select('as.name as asset_status, t.part_number, a.asset_id, p.serial_number, t.part_description, a.asset_number, a.sub_inventory');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->where_not_in('a.asset_id',$asset_arr);
		$this->db->where('ap.wh_id IS NOT NULL');
		$this->db->where('ap.transit',0);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('a.approval_status',0);
		$this->db->where('ot.ordered_tool_id',$ordered_tool_id);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function check_asset($tool_id,$asset_number,$wh_id,$asset_arr)
	{
		$status = array(1,2); 
		$this->db->select('a.asset_id');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_position ap','a.asset_id = ap.asset_id');
		$this->db->where('ap.wh_id IS NOT NULL');
		$this->db->where('ap.transit',0);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('a.approval_status',0);
		$this->db->where('p.part_level_id',1); // 1 = L0 level
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('a.wh_id',$wh_id);
		$this->db->where('a.asset_number',$asset_number);
		$this->db->where_in('a.status',$status);
		$this->db->where_not_in('a.asset_id',$asset_arr);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function check_asset_1($asset_number,$asset_arr,$wh_id,$ordered_tool_id)
	{
		$this->db->select('a.asset_id');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('ap.wh_id IS NOT NULL');
		$this->db->where('ap.transit',0);
		$this->db->where('ap.to_date IS NULL');
		$this->db->where('a.approval_status',0);
		$this->db->where('a.asset_number',$asset_number);
		$this->db->where('ot.ordered_tool_id',$ordered_tool_id);
		$this->db->where('a.wh_id',$wh_id);
		$this->db->where_not_in('a.asset_id',$asset_arr);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_asset_part_details($asset_id,$tool_order_wh_id)
	{
		//$wh_id = $this->session->userdata('swh_id');
		$this->db->select('a.asset_id,a.asset_number,p.part_id,
						  pl.name as part_level_name,t.part_number,
						  t.part_description,p.quantity,p.status,t.tool_id, a.status as asset_status,p.serial_number');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('asset_position ap','a.asset_id = ap.asset_id');
		$this->db->where('ap.wh_id!=',NULL);
		$this->db->where('ap.transit',0);
		$this->db->where('ap.to_date',NULL);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('a.approval_status',0); // not for approval
		$this->db->where('p.part_status',1);
		$this->db->order_by('p.part_id ASC, p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_first_asset_avail($tool_id,$tool_order_wh_id)
	{
		$getAssetsInTransferProcess = getAssetsInTransferProcess(array());
		$this->db->select('a.asset_id');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('t.tool_id',$tool_id);
		$this->db->where('a.approval_status',0);
		$this->db->where('a.wh_id',$tool_order_wh_id);
		$this->db->where('p.part_level_id',1); // 1 = L0 level
		$this->db->where('a.status',2);
		if(count($getAssetsInTransferProcess)>0)
		{
			$this->db->where_not_in('a.asset_id',$getAssetsInTransferProcess);
		}
		$this->db->limit(1);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_assets_by_tool_order($tool_order_id)
	{
		$this->db->select('to.sso_id,oa.asset_id,to.to_wh_id,a.status,to.fe_check');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('ot.status',1);
		$this->db->where('oa.status',1);
		$res = $this->db->get();
		return $res->result_array();
	}

	//closed wh delivery list
	public function closed_wh_delivery_list_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.tool_order_id');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('warehouse wh','wh.wh_id = to.to_wh_id','LEFT');
		$this->db->join('location l','l.location_id = wh.location_id','LEFT');
		$this->db->join('warehouse wh_to','wh_to.wh_id = to.wh_id','LEFT');
		if($searchParams['to_wh_id']!='')
		{
			$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
		}
		if($searchParams['stn_number']!='')
			$this->db->like('to.stn_number',$searchParams['stn_number']);
	
		if($task_access==1)
		{
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['from_wh_id']=='')
				{
					$this->db->where_in('to.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
		}
		$this->db->where_in('osh.current_stage_id',array(2,10));//2-at transit from wh to wh
		$this->db->where('to.order_type',1); //Stock Transfer order = 1
		$this->db->where('oa.status',1);
		$this->db->order_by('to.tool_order_id DESC');
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function closed_wh_delivery_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,cs.name as current_stage,
						   od.expected_delivery_date as delivered_date,
						   to.current_stage_id as cs, 
						   concat(wh.wh_code, "- (" ,wh.name,")") as to_wh_name,
						   concat(wh_to.wh_code, "- (" ,wh_to.name,")") as from_wh_name,
						   CONCAT(oa.address3,", ",oa.address4) AS location_name');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('current_stage cs','cs.current_stage_id = to.current_stage_id');
		$this->db->join('order_status_history osh','osh.tool_order_id = to.tool_order_id');
		$this->db->join('warehouse wh','wh.wh_id = to.to_wh_id','LEFT');
		$this->db->join('location l','l.location_id = wh.location_id','LEFT');
		$this->db->join('warehouse wh_to','wh_to.wh_id = to.wh_id','LEFT');
		if($searchParams['to_wh_id']!='')
		{
			$this->db->where('to.to_wh_id',$searchParams['to_wh_id']);
		}
		if($searchParams['stn_number']!='')
			$this->db->like('to.stn_number',$searchParams['stn_number']);
		if($task_access==1)
		{
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['from_wh_id']=='')
				{
					$this->db->where_in('to.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('to.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		elseif($task_access == 2)
		{
			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['from_wh_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['from_wh_id']);
			}
		}
		$this->db->where_in('osh.current_stage_id',array(2,10));//2-at transit from wh to wh and 10 is cancelled with status 4
		$this->db->where('to.order_type',1); //Stock Transfer order = 1
		$this->db->where('oa.status',1);
		$this->db->order_by('to.tool_order_id DESC');
		$this->db->group_by('to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $res->result_array();
	}

	public function get_invoiced_tools($tool_order_id)
	{
		$this->db->select('t.part_number,t.part_description,t.hsn_code,count(t.tool_id) as quantity,sum(a.cost_in_inr) as cost_in_inr,t.gst_percent,
			GROUP_CONCAT(oa.asset_id SEPARATOR ",") as asset_id_list, GROUP_CONCAT(CONCAT(p.serial_number," : ",a.asset_number) SEPARATOR ",") as asset_number_list');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('ot.tool_order_id',$tool_order_id);
		$this->db->where('p.part_level_id',1);
		$this->db->where('p.part_status',1);
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_delivery_details($tool_order_id)
	{
		$this->db->select('pf.format_number,od.remarks,
						   pf.created_time,od.courier_type,
						   od.courier_number,to.stn_number,
						   wh.address1,wh.address2,wh.address3,
						   wh.address4,wh.tin_number,
						   wh.gst_number,wh.pan_number,
						   pf.print_type,wh.pin_code,
						   l.name as from_location,
						   od.billed_to,od.courier_name,
						   od.contact_person,od.vehicle_number,
						   od.phone_number,
						   to.to_wh_id,
						   to.fe_check');
		$this->db->from('order_delivery od');
		$this->db->join('tool_order to','to.tool_order_id = od.tool_order_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('location l','l.location_id = wh.location_id');
		$this->db->join('print_format pf','pf.print_id = od.print_id');
		$this->db->where('od.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function check_for_scanned_assets($tool_order_id)
	{
		$this->db->select('');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('order_asset_history oah','oah.ordered_asset_id = oa.ordered_asset_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('to.current_stage_id',1);
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function get_for_scanned_assets()
	{
		$this->db->select('oa.ordered_asset_id,oa.asset_id,to.tool_order_id,to.stn_number');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('order_asset_history oah','oah.ordered_asset_id = oa.ordered_asset_id');
		$this->db->where('to.current_stage_id',1);
		$res = $this->db->get();
		return $res->result_array();
	}
}