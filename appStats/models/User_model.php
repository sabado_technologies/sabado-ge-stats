<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model 
{
	public function onbehalf_fe_ajax($task_access,$sso_string,$country_id)
	{
		$fe_arr =  getTaskRoles('raise_order');
	    $limit = getDefaultSelect2Limit();
	    $this->db->select('u.sso_id,concat(u.sso_id, "-(" ,u.name,")") as name');
	    $this->db->from('user u');
	    if($sso_string!='')
	    {
	        $where = "(u.sso_id LIKE '%$sso_string%' OR u.name LIKE '%$sso_string%')";
	        $this->db->where($where);
	    }
	    if($country_id!='')
	    {
	    	$this->db->where('u.country_id',$country_id);
	    }
	    else if($task_access == 1 || $task_access == 2)
	    {
	        $this->db->where('u.country_id',$this->session->userdata('s_country_id'));
	    }
	    else if($task_access == 3)
	    {
	        if($this->session->userdata('header_country_id')!='')
	        {
	            $this->db->where('u.country_id',$this->session->userdata('header_country_id')); 
	        }
	        else
	        {
	            $this->db->where_in('u.country_id',$this->session->userdata('countriesIndexedArray'));  
	        }
	    }
	    $this->db->where('u.status',1);
	    $this->db->where_in('u.role_id',$fe_arr);
	    $this->db->order_by('u.sso_id ASC');
	    $this->db->limit($limit,0);
	    $res=$this->db->get();
	    $result = $res->result_array();
	    $data = array();
	    foreach($result as $row)
	    {
	        $sso = $row['name'];
	        $data[] = array('id'=>$row['sso_id'], 'text'=>$sso);
	    }
	    return $data;
	}

	public function sso_dropdown_list($task_access,$sso_string)
	{
		$fe_arr =  getTaskRoles('raise_order');
	    $limit = getDefaultSelect2Limit();
	    $this->db->select('u.sso_id,concat(u.sso_id, "-(" ,u.name,")") as name');
	    $this->db->from('user u');
	    if($sso_string!='')
	    {
	        $where = "(u.sso_id LIKE '%$sso_string%' OR u.name LIKE '%$sso_string%')";
	        $this->db->where($where);
	    }
	    if($task_access == 1 || $task_access == 2)
	    {
	        $this->db->where('u.country_id',$this->session->userdata('s_country_id'));
	    }
	    else if($task_access == 3)
	    {
	        if($this->session->userdata('header_country_id')!='')
	        {
	            $this->db->where('u.country_id',$this->session->userdata('header_country_id')); 
	        }
	        else
	        {
	            $this->db->where_in('u.country_id',$this->session->userdata('countriesIndexedArray'));  
	        }
	    }
	    $this->db->where('u.status',1);
	    $this->db->where_in('u.role_id',$fe_arr);
	    $this->db->order_by('u.sso_id ASC');
	    $this->db->limit($limit,0);
	    $res=$this->db->get();
	    $result = $res->result_array();
	    $data = array();
	    foreach($result as $row)
	    {
	        $sso = $row['name'];
	        $data[] = array('id'=>$row['sso_id'], 'text'=>$sso);
	    }
	    return $data;
	}

	public function all_sso_dropdown_list($sso_string,$role_id)
	{
	    $limit = getDefaultSelect2Limit();
	    $this->db->select('u.sso_id,concat(u.sso_id, "-(" ,u.name,")") as name');
	    $this->db->from('user u');
	    if($sso_string!='')
	    {
	        $where = "(u.sso_id LIKE '%$sso_string%' OR u.name LIKE '%$sso_string%')";
	        $this->db->where($where);
	    }
        if($this->session->userdata('header_country_id')!='')
        {
            $this->db->where('u.country_id',$this->session->userdata('header_country_id')); 
        }
        else
        {
            $this->db->where_in('u.country_id',$this->session->userdata('countriesIndexedArray')); 
        }
        if($role_id !=0)
        {
        	$this->db->where('u.role_id',$role_id);
        }
	    $this->db->where('u.status',1);
	    $this->db->order_by('u.sso_id ASC');
	    $this->db->limit($limit,0);
	    $res=$this->db->get();
	    $result = $res->result_array();
	    $data = array();
	    foreach($result as $row)
	    {
	        $sso = $row['name'];
	        $data[] = array('id'=>$row['sso_id'], 'text'=>$sso);
	    }
	    return $data;
	}

	/*koushik 20-09-2018*/
	public function get_reporting_manager_list($sso_string,$country_id)
	{
	    $limit = getDefaultSelect2Limit();
	    $this->db->select('u.*');
	    $this->db->from('user u');
	    if($sso_string!='')
	    {
	        $where = "(u.sso_id LIKE '%$sso_string%' OR u.name LIKE '%$sso_string%')";
	        $this->db->where($where);
	    }
	    if($country_id!=0)
	    {
	    	$this->db->where('u.country_id',$country_id);
	    }
	    $this->db->where('u.status',1);
	    // $this->db->where_not_in('u.role_id',array('2','3','7','8','9'));
	    $this->db->order_by('u.sso_id ASC');
	    $this->db->limit($limit,0);
	    $res=$this->db->get();
	    $result = $res->result_array();
	    $data = array();
	    foreach($result as $row)
	    {
	        $sso = $row['sso_id'].' -('.$row['name'].')';
	        $data[] = array('id'=>$row['sso_id'], 'text'=>$sso);
	    }
	    return $data;
	}

	/*koushik 20-09-2018*/
	public function get_fe_position_list($city_string,$country_id)
	{
		$limit = getDefaultSelect2Limit();

		$this->db->select('l.name as city_name,l1.name as state_name,l.location_id');
		$this->db->from('location l');
		$this->db->join('location l1','l1.location_id = l.parent_id');
		$this->db->join('location l2','l2.location_id = l1.parent_id');
		$this->db->join('location l3','l3.location_id = l2.parent_id');
		$this->db->where('l.level_id',5);
		$this->db->where('l.status',1);
		$this->db->like('l.name',$city_string);
		if($country_id !=0)
		{
			$this->db->where('l3.location_id',$country_id);
		}
		$this->db->order_by('l.location_id ASC');
		$this->db->limit($limit,0);
		$res = $this->db->get();
		$result =  $res->result_array();

	    $data = array();
	    foreach($result as $row)
	    {
	        $city_string = $row['city_name'].', '.$row['state_name'];
	        $data[] = array('id'=>$row['location_id'], 'text'=>$city_string);
	    }
	    return $data;
	}

	/*koushik 20-09-2018*/
	public function get_user_details_by_id($sso_id)
	{
		$this->db->select('u.*,CONCAT(u1.sso_id,"-(",u1.name,")") as reporting_manager_name,
			CONCAT(l.name,", ",l1.name) AS fe_position_name');
		$this->db->from('user u');
		$this->db->join('user u1','u1.sso_id = u.rm_id','LEFT');
		$this->db->join('location l','l.location_id = u.fe_position','LEFT');//city
		$this->db->join('location l1','l1.location_id = l.parent_id','LEFT');//state
		$this->db->where('u.sso_id',$sso_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function insert_update_uc($country_id,$sso_id,$status)
	{
		$qry = "INSERT INTO user_country(sso_id, country_id, status) 
			    VALUES (".$sso_id.",".$country_id.",1)
			    ON DUPLICATE KEY UPDATE status = VALUES(status);";
		$this->db->query($qry);
	}

	//starting of User crud methods//
	public function user_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('u.*,b.name as branch_name,r.name as rle,d.name as designation_name,l.name as location_name,concat(wh.wh_code, "-(" ,wh.name,")") as wh,l1.name as country,GROUP_CONCAT(concat(wh1.wh_code, "-(" ,wh1.name,")") SEPARATOR ",<br>") as wh_arr,l1.region_id as country_region');
		$this->db->from('user u');
		$this->db->join('user_wh uwh','uwh.sso_id = u.sso_id AND uwh.status = 1','LEFT');
		$this->db->join('warehouse wh1','wh1.wh_id = uwh.wh_id','LEFT');
		$this->db->join('role r','r.role_id = u.role_id','left');
		$this->db->join('designation d','d.designation_id = u.designation_id','left');
		$this->db->join('branch b','b.branch_id = u.branch_id','left');
		$this->db->join('location l','l.location_id = u.fe_position','left');
		$this->db->join('location l1','l1.location_id=u.country_id','left');
		$this->db->join('warehouse wh','wh.wh_id = u.wh_id','left');
		if($searchParams['ssonum']!='')
			$this->db->like('u.sso_id',$searchParams['ssonum']);
		if($searchParams['username']!='')
			$this->db->like('u.name',$searchParams['username']);
		if($searchParams['branchnum']!='')
			$this->db->where('u.branch_id',$searchParams['branchnum']);
		if($searchParams['designationnum']!='')
			$this->db->where('u.designation_id',$searchParams['designationnum']);
		if($searchParams['roleid']!='')
			$this->db->where('u.role_id',$searchParams['roleid']);
		
		if($searchParams['fe_position']!='')
			$this->db->like('l.name',$searchParams['fe_position']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('u.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('u.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('u.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('u.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		if($searchParams['whid']!='')
		{
			$where = '((`uwh`.`wh_id` = '.$searchParams['whid'].') OR (`u`.`wh_id` = '.$searchParams['whid'].'))';
			$this->db->where($where);
		}
		$this->db->order_by('u.sso_id','ASC');
		$this->db->group_by('u.sso_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function user_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('u.sso_id');
		$this->db->from('user u');
		$this->db->join('user_wh uwh','uwh.sso_id = u.sso_id AND uwh.status = 1','LEFT');
		$this->db->join('warehouse wh1','wh1.wh_id = uwh.wh_id','LEFT');
		$this->db->join('role r','r.role_id = u.role_id','left');
		$this->db->join('designation d','d.designation_id = u.designation_id','left');
		$this->db->join('branch b','b.branch_id = u.branch_id','left');
		$this->db->join('location l','l.location_id = u.fe_position','left');
		$this->db->join('location l1','l1.location_id=u.country_id','left');
		$this->db->join('warehouse wh','wh.wh_id = u.wh_id','left');
		if($searchParams['ssonum']!='')
			$this->db->like('u.sso_id',$searchParams['ssonum']);
		if($searchParams['username']!='')
			$this->db->like('u.name',$searchParams['username']);
		if($searchParams['branchnum']!='')
			$this->db->where('u.branch_id',$searchParams['branchnum']);
		if($searchParams['designationnum']!='')
			$this->db->where('u.designation_id',$searchParams['designationnum']);
		if($searchParams['roleid']!='')
			$this->db->where('u.role_id',$searchParams['roleid']);
		if($searchParams['fe_position']!='')
			$this->db->like('l.name',$searchParams['fe_position']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('u.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('u.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('u.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('u.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		if($searchParams['whid']!='')
		{
			$where = '((`uwh`.`wh_id` = '.$searchParams['whid'].') OR (`u`.`wh_id` = '.$searchParams['whid'].'))';
			$this->db->where($where);
		}
		$this->db->order_by('u.sso_id','ASC');
		$this->db->group_by('u.sso_id');
		$res = $this->db->get();
		return $res->num_rows();
	}

	//uniqueness of SSO ID
	public function is_user_sso_Exist($data)
    {       
        $this->db->select();
        $this->db->from('user u');
        $this->db->where('u.sso_id',$data['sso_num']);
        $this->db->where_not_in('u.sso_id',@$data['sso_id']);
        $query = $this->db->get();
        return ($query->num_rows()>0)?1:0;
    }

    public function get_location_city($country_id=0)
	{
		$this->db->select('l.name as city_name,l1.name as state_name,l.location_id');
		$this->db->from('location l');
		$this->db->join('location l1','l1.location_id = l.parent_id');
		$this->db->join('location l2','l2.location_id = l1.parent_id');
		$this->db->join('location l3','l3.location_id = l2.parent_id');
		$this->db->where('l.level_id',5);
		$this->db->where('l.status',1);
		if($country_id !=0)
		$this->db->where('l3.location_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_zonal_data($country_id)
	{
		$this->db->select('l.*');
		$this->db->from('location l');
		$this->db->join('location l1','l1.location_id = l.parent_id');
		$this->db->where('l.level_id',3);
		$this->db->where('l.status',1);
		if($country_id !=0)
		$this->db->where('l1.location_id',$country_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_designationList($role_id)
	{
		$this->db->select('d.designation_id,d.name');
		$this->db->from('designation d');
		$this->db->join('role_designation rd','rd.designation_id = d.designation_id');
		$this->db->where('rd.role_id',$role_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_designation_by_role($role_id)
	{
		$this->db->select('d.designation_id,d.name');
	    $this->db->from('designation d');
	    $this->db->join('role_designation rd','rd.designation_id = d.designation_id');
		$this->db->where('rd.role_id',$role_id);
		$this->db->where('d.status',1);
        $res1 = $this->db->get();
		$res = $res1->result_array();
		$count = $res1->num_rows();
		$qry_data='';
        if($count>0)
		{
			$qry_data.='<option value="">- Designation -</option>';
			foreach($res as $row1)
			{  
				$qry_data.='<option value="'.$row1['designation_id'].'">'.$row1['name'].'</option>';
			}
		} 
		else 
		{
			$qry_data.='<option value="">No Data Found</option>';
		}
		return $qry_data;
	}
	public function get_location_by_role($role_id,$country_id)
	{
		$this->db->select('l.location_id,l.name');
	    $this->db->from('location l');
	    if($role_id == 3)
	    {
	    	$this->db->join('location l1','l1.location_id = l.parent_id','LEFT');
	    	$this->db->where('l1.location_id',$country_id);
	    }
	    if($role_id == 2)
	    {
	    	$this->db->where('l.location_id',$country_id);
	    }
		$this->db->where('l.level_id',$role_id);
		$this->db->where('l.status',1);
        $res1 = $this->db->get();
		$res = $res1->result_array();
		$count = $res1->num_rows();
		$qry_data='';
        if($count>0)
		{
			if($role_id == 3)
			{
				$qry_data.='<option value="">- Select Zone -</option>';
			}
			if($role_id == 2)
			{
				$qry_data.='<option value="">- Select Country -</option>';
			}
			
			foreach($res as $row1)
			{  
				$qry_data.='<option value="'.$row1['location_id'].'">'.$row1['name'].'</option>';
			}
		} 
		else 
		{
			$qry_data.='<option value="">No Data Found</option>';
		}
		return $qry_data;
	}
	public function get_locations_by_role($role_id,$country_id)
	{
		$this->db->select('l.location_id,l.name');
	    $this->db->from('location l');
	    if($role_id == 3)
	    {
	    	$this->db->join('location l1','l1.parent_id = l.location_id','LEFT');
	    	$this->db->where('l.location_id',$country_id);
	    }
	    
		$this->db->where('l.level_id>=',$role_id);
		$this->db->where('l.status',1);
        $res1 = $this->db->get();
		$res = $res1->result_array();
		$count = $res1->num_rows();
		$qry_data='';
        if($count>0)
		{
			$qry_data.='<option value="">- Location-</option>';
			foreach($res as $row1)
			{  
				$qry_data.='<option value="'.$row1['location_id'].'">'.$row1['name'].'</option>';
			}
		} 
		else 
		{
			$qry_data.='<option value="">No Data Found</option>';
		}
		return $qry_data;
	}

	public function check_designation_by_role_id($role_id,$designation_name)
	{
		$this->db->select('d.designation_id');
		$this->db->from('designation d');
		$this->db->join('role_designation rd','rd.designation_id = d.designation_id');
		$this->db->where('rd.role_id',@$role_id);
		$this->db->where('d.name',$designation_name);
		$res = $this->db->get();
		$count = $res->num_rows();
		$result = $res->row_array();
		if($count>0)
		{
			$designation_id = $result['designation_id'];
		}
		else
		{
			$designation_id = 0;
		}
	return $designation_id;

	}

	public function missed_user_results($current_offset, $per_page, $upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_user');
		$this->db->where('upload_id',$upload_id);
		$this->db->order_by('upload_user_id','ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function missed_user_total_num_rows($upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_user');
		$this->db->where('upload_id',$upload_id);
		$this->db->order_by('upload_user_id','ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function insert_update($sso_id,$modality_id)
	{
		$qry = "INSERT INTO user_modality(sso_id, modality_id, status,to_date) 
                    VALUES (".$sso_id.",".$modality_id.",'1','".date('Y-m-d')."')   
                    ON DUPLICATE KEY UPDATE status = VALUES(status),to_date = VALUES(to_date) ;";
        $this->db->query($qry);
	}

	public function get_fe_position_id($country_id,$fe_position_name)
	{
		$this->db->select('l3.location_id');
		$this->db->from('location l');
		$this->db->join('location l1','l1.parent_id=l.location_id','LEFT');
		$this->db->join('location l2','l2.parent_id=l1.location_id','LEFT');
		$this->db->join('location l3','l3.parent_id=l2.location_id','LEFT');
		$this->db->where('l.location_id',$country_id);
		$this->db->where('l3.name',$fe_position_name);
		$res=$this->db->get();
		$result=$res->row_array();
		if(count($result)!=0)
		{
			return $result['location_id'];
		}
		else
		{
			return 0;
		}
	}
	public function get_zone_id($country_id,$assigned_location)
	{
		$this->db->select('l1.location_id');
		$this->db->from('location l');
		$this->db->join('location l1','l1.parent_id=l.location_id','LEFT');
		$this->db->where('l.location_id',$country_id);
		$this->db->where('l1.name',$assigned_location);
		$res=$this->db->get();
		$result=$res->row_array();
		if(count($result)!=0)
		{
			return $result['location_id'];
		}
		else
		{
			return 0;
		}
	}

	public function get_all_locations_by_country($task_access)
	{
		$this->db->select('l.name as city_name,l1.name as state_name,l.location_id, concat(l.name, " ," ,l1.name) as name');
		$this->db->from('location l');//city
		$this->db->join('location l1','l1.location_id = l.parent_id');//state
		$this->db->join('location l2','l2.location_id = l1.parent_id');//zone
		$this->db->join('location l3','l3.location_id = l2.parent_id');//country
		$this->db->where('l.level_id',5);
		$this->db->where('l.status',1);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l3.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('l3.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				$this->db->where_in('l3.location_id',$this->session->userdata('countriesIndexedArray'));	
			}
			
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	//created by gowri on 27/aug/18
    public function get_wh_by_zone($location_id)
	{
		$this->db->select('w.wh_id,CONCAT(w.wh_code,"-(",w.name,")") AS wh_name');
		$this->db->from('location l');//zone
		$this->db->join('location l1','l1.parent_id = l.location_id','LEFT');//state
		$this->db->join('location l2','l2.parent_id = l1.location_id','LEFT');//city
		$this->db->join('location l3','l3.parent_id = l2.location_id','LEFT');//area
		$this->db->join('warehouse w','w.location_id = l3.location_id');
		$this->db->where('l.location_id',$location_id);
		$this->db->where('l.level_id',3);
		$this->db->where('l.status',1);
        	$res1 = $this->db->get();
		$res = $res1->result_array();
		$count = $res1->num_rows();
		$qry_data='';
        	if($count>0)
		{ 
			$qry_data.='<label><input type="checkbox" id="select_all_wh" name="warehouses"> <strong>Select All</strong></label><br>'; 
			foreach($res as $row1)
			{ 
				$qry_data.='<input type="checkbox" name="warehouse_arr[]" class="checkbox_wh" value="'.$row1['wh_id'].'" >'.$row1['wh_name'].'<br>';
			}
		} 
		else 
		{
			$qry_data.='<input type="checkbox" value="">No Data Found';
		}
		return $qry_data;
	}

	//created by gowri on 27/aug/18
    public function get_wh_by_zone_id($location_id)
	{
		$this->db->select('w.wh_id,CONCAT(w.wh_code,"-(",w.name,")") AS wh_name');
		$this->db->from('location l');//zone
		$this->db->join('location l1','l1.parent_id = l.location_id','LEFT');//state
		$this->db->join('location l2','l2.parent_id = l1.location_id','LEFT');//city
		$this->db->join('location l3','l3.parent_id = l2.location_id','LEFT');//area
		$this->db->join('warehouse w','w.location_id = l3.location_id');
		$this->db->where('l.location_id',$location_id);
		$this->db->where('l.level_id',3);
		$this->db->where('l.status',1);
        $res1 = $this->db->get();
		return $res1->result_array();
	}

	//created by gowri on 27/aug/18
    public function insert_update_wh_zone($sso_id,$wh_id,$status)
	{
		$qry = "INSERT INTO user_wh(sso_id, wh_id, status) 
			    VALUES (".$sso_id.",".$wh_id.",1)
			    ON DUPLICATE KEY UPDATE status = VALUES(status);";
		$this->db->query($qry);
	}

	public function get_asean_users($region_id)
	{
		$this->db->select('u.sso_id,u.country_id,u.role_id');
		$this->db->from('user u');
		$this->db->join('location l','l.location_id = u.country_id');
		$this->db->where('l.region_id',$region_id);
		$this->db->where_not_in('u.role_id',exclude_Asean_for_roles());
		$res = $this->db->get();
		return $res->result_array();
	}
}	