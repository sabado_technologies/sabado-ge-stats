<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * created by Roopa on 19thth june 2017 11:00AM
*/
class Branch_office_m extends CI_Model 
{
	/*branch office results
    Author:Roopa*/
	public function branch_office_results($current_offset, $per_page, $searchParams,$task_access)
	{
		
		$this->db->select('b.*,l.name as location_name,b.country_id as country_name,l2.name as country_name');
		$this->db->from('branch b');
		$this->db->join('location l','l.location_id=b.location_id');
		$this->db->join('location l2','l2.location_id=b.country_id');
		if($searchParams['name']!='')
			$this->db->like('b.name',$searchParams['name']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('b.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('b.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('b.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('b.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('b.country_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	/*branch office number of rows
    Author:Roopa*/
	public function branch_office_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('b.*,l.name as location_name,b.country_id as country_name,l2.name as country_name');
		$this->db->from('branch b');
		$this->db->join('location l','l.location_id=b.location_id');
		$this->db->join('location l2','l2.location_id=b.country_id');
		if($searchParams['name']!='')
			$this->db->like('b.name',$searchParams['name']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('b.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('b.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('b.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('b.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('b.country_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	/*branch office name uniqueness..
    Author:Roopa*/
	public function is_officenameExist($name,$branch_id,$country_id)
	{
		$this->db->select();
		$this->db->from('branch b');
		$this->db->where('b.name',$name);
		$this->db->where('b.country_id',$country_id);
		if($branch_id!='' || $branch_id!=0){
			$this->db->where_not_in('b.branch_id',$branch_id);	
		}	
		$query = $this->db->get();
		return ($query->num_rows()>0)?1:0;
	}	

	public function get_area_location($country_id)
	{

		$this->db->select('concat(l1.name, " ," ,l2.name) as name,l1.location_id');
		$this->db->from('location l1');//area
		$this->db->join('location l2','l1.parent_id=l2.location_id','left');//city
		$this->db->join('location l3','l2.parent_id=l3.location_id','left');//state
		$this->db->join('location l4','l3.parent_id=l4.location_id','left');//zone
		$this->db->join('location l5','l4.parent_id=l5.location_id','left');//country
		$this->db->where('l1.status',1);
		$this->db->where('l1.level_id',6);
		$this->db->where('l5.location_id',$country_id);
		$res=$this->db->get();
		return $res->result_array();
	}
}	