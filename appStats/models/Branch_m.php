<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * created by Roopa on 17th june 2017 11:00AM
*/
class Branch_m extends CI_Model 
{
	/*branch results
    Author:Roopa*/
	public function branch_results($current_offset, $per_page, $searchParams,$task_access)
	{
		
		$this->db->select('w.*,l.name as location_name,l2.name as country_name');
		$this->db->from('warehouse w');
		$this->db->join('location l','l.location_id=w.location_id');
		$this->db->join('location l2','l2.location_id=w.country_id');
		if($searchParams['name']!='')
			$this->db->like('w.name',$searchParams['name']);
		if($searchParams['wh_code']!='')
			$this->db->like('w.wh_code',$searchParams['wh_code']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('w.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('w.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('w.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('w.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('w.country_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	/*branch number of rows.
    Author:Roopa*/
	public function branch_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('w.*,l.name as location_name,l2.name as country_name');
		$this->db->from('warehouse w');
		$this->db->join('location l','l.location_id=w.location_id');
		$this->db->join('location l2','l2.location_id=w.country_id');
		if($searchParams['name']!='')
			$this->db->like('w.name',$searchParams['name']);
		if($searchParams['wh_code']!='')
			$this->db->like('w.wh_code',$searchParams['wh_code']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('w.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('w.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('w.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('w.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		$this->db->order_by('w.country_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	/*branch name Uniqueness.
    Author:Roopa*/
	public function is_branchnameExist($name,$branch_id,$country_id)
	{
		$this->db->select();
		$this->db->from('warehouse w');
		$this->db->where('w.name',$name);
		$this->db->where('w.country_id',$country_id);
		if($branch_id!='' || $branch_id!=0){
			$this->db->where_not_in('w.wh_id',$branch_id);	
		}		
		$query = $this->db->get();
		return ($query->num_rows()>0)?1:0;
	}
	/*Ware house code uniqueness.
    Author:Roopa*/
	public function is_branchcodeExist($branch_code,$branch_id,$country_id)
	{
		$this->db->select();
		$this->db->from('warehouse w');
		$this->db->where('w.wh_code',$branch_code);
		$this->db->where('w.country_id',$country_id);
		if($branch_id!='' || $branch_id!=0){
			$this->db->where_not_in('w.wh_id',$branch_id);	
		}
		$query = $this->db->get();
		return ($query->num_rows()>0)?1:0;
	}
	public function get_area_location($country_id)
	{
		$this->db->select('concat(l1.name, " ," ,l2.name) as name,l1.location_id');
		$this->db->from('location l1');//area
		$this->db->join('location l2','l1.parent_id=l2.location_id','left');//city
		$this->db->join('location l3','l2.parent_id=l3.location_id','left');//state
		$this->db->join('location l4','l3.parent_id=l4.location_id','left');//zone
		$this->db->join('location l5','l4.parent_id=l5.location_id','left');//country
		$this->db->where('l1.level_id',6);
		$this->db->where('l5.location_id',$country_id);
		$res=$this->db->get();
		return $res->result_array();
	}
}	