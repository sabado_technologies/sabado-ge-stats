<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alerts_m extends CI_Model 
{
    public function get_country_from_to($tool_order_id)
    {
        $this->db->select('u.country_id');
        $this->db->from('tool_order to');          
        $this->db->join('user u','u.sso_id = to.sso_id');
        $this->db->where('to.tool_order_id',$tool_order_id);
        $res = $this->db->get();
        return $res->row_array();
    }
	public function exceededOrderDurationResults($current_offset, $per_page, $searchParams,$task_access)
    {
        $this->db->select('to.*,odt.name as order_type,oed.return_date as exd_return_date,concat(u.sso_id, "- (" ,u.name," )") as sso,l.name as countryName');
        $this->db->from('tool_order to');               
        $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');        
        $this->db->join('user u','u.sso_id = to.sso_id');
        $this->db->join('order_extended_days oed','oed.tool_order_id = to.tool_order_id');
        $this->db->join('location l','l.location_id = to.country_id');
        if(@$searchParams['order_number']!='')
            $this->db->like('to.order_number',$searchParams['order_number']);
        if(@$searchParams['order_delivery_type_id']!='')
            $this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
        if(@$searchParams['request_date']!='')
            $this->db->where('to.request_date>=',$searchParams['request_date']);
        if(@$searchParams['return_date']!='')
            $this->db->where('to.return_date<=',$searchParams['return_date']);
        if($task_access==1)
        {
            $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        elseif($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            if($searchParams['users_id']!='')
            {
                $this->db->where('to.sso_id',$searchParams['users_id']);
            }
        }
        else if($task_access == 3)
        {
            if($_SESSION['header_country_id']!='')
            {
                $this->db->where('to.country_id',$_SESSION['header_country_id']);   
            }
            else
            {
                if($searchParams['country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                }
            }
            if($searchParams['users_id']!='')
            {
                $this->db->where('to.sso_id',$searchParams['users_id']);
            }
        }               
        $this->db->where('to.days_approval',1);
        $this->db->where('oed.status',2);
        $this->db->order_by('to.tool_order_id ASC');
        $this->db->limit($per_page, $current_offset);
        $res = $this->db->get();
        return $res->result_array();
    }

    public function addressChangeResults($current_offset, $per_page, $searchParams,$task_access)
    {
        if($searchParams['request_type'] == 1)
        {
            $this->db->select('to.*,to.order_number as final_order_number,odt.name as order_type,concat(u.sso_id, "-(" ,u.name,")") as sso,l.name as country');
            $this->db->from('tool_order to');               
            $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
            $this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
            $this->db->join('user u','u.sso_id = to.sso_id');
            $this->db->join('location l','l.location_id=to.country_id');
            if(@$searchParams['order_number']!='')
                $this->db->like('to.order_number',$searchParams['order_number']);
            if(@$searchParams['deploy_date']!='')
                $this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
            if(@$searchParams['return_date']!='')
                $this->db->where('to.return_date<=',format_date($searchParams['return_date']));
            if($task_access == 1 )
            {
                $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
            }
            else if($task_access == 2)
            {
                if(@$searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

                $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            }
            else if($task_access == 3)
            {
                
                if($_SESSION['header_country_id']!='')
                {
                    $this->db->where('to.country_id',$_SESSION['header_country_id']);   
                }
                else
                {
                    if(@$searchParams['country_id']!='')
                    {
                        $this->db->where('to.country_id',$searchParams['country_id']);
                    }
                    else
                    {
                        $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                    }
                    
                }
                if(@$searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

            }
            $this->db->where('to.current_stage_id',4);  
            $this->db->where('oa.check_address',1); 
            $this->db->where('oa.status',1);
            $this->db->where('to.order_delivery_type_id',1);
            $this->db->limit($per_page, $current_offset);
            $res1 = $this->db->get();
            $result = $res1->result_array();
        }
        else
        {
            $this->db->select('ro.*,to.order_number as final_order_number,concat(u.sso_id, "-(" ,u.name,")") as ro_sso,odt.name as order_type,to.deploy_date,to.return_date,l.name as country');
            $this->db->from('return_order ro');             
            $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');
            $this->db->join('user u','u.sso_id = ro.created_by');
            $this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
            $this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
            $this->db->join('location l','l.location_id = to.country_id');
            if(@$searchParams['return_number']!='')
                $this->db->like('ro.return_number',$searchParams['return_number']);
            if(@$searchParams['order_number']!='')
                $this->db->like('to.order_number',$searchParams['order_number']);
            if($task_access == 1 )
            {
                $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
            }
            else if($task_access == 2)
            {
                if(@$searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

                $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            }
            else if($task_access == 3)
            {
                
                if($_SESSION['header_country_id']!='')
                {
                    $this->db->where('to.country_id',$_SESSION['header_country_id']);   
                }
                else
                {
                    if(@$searchParams['country_id']!='')
                    {
                        $this->db->where('to.country_id',$searchParams['country_id']);
                    }
                    else
                    {
                        $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                    }
                    
                }
                if(@$searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

            }
                        
            $this->db->where('ro.address_check',1);     
            $this->db->where('ro.order_delivery_type_id',1);
            $this->db->where('ro.status',1);
            $this->db->limit($per_page, $current_offset);
            $res2 = $this->db->get();
            $result = $res2->result_array();            
        }
        return $result;
        
    }
    
    public function addressChangeTotalNumRows($searchParams,$task_access)
    {
        if($searchParams['request_type']==1)
        {
            $this->db->select('to.*,odt.name as order_type');
            $this->db->from('tool_order to');               
            $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
            $this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
            $this->db->join('user u','u.sso_id = to.sso_id');
            $this->db->join('location l','l.location_id=to.country_id');
            if($searchParams['order_number']!='')
                $this->db->like('to.order_number',$searchParams['order_number']);
            if($searchParams['deploy_date']!='')
                $this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
            if($searchParams['return_date']!='')
                $this->db->where('to.return_date<=',format_date($searchParams['return_date']));
            if($task_access == 1 )
            {
                $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
            }
            else if($task_access == 2)
            {
                if($searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

                $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            }
            else if($task_access == 3)
            {
                
                if($_SESSION['header_country_id']!='')
                {
                    $this->db->where('to.country_id',$_SESSION['header_country_id']);   
                }
                else
                {
                    if($searchParams['country_id']!='')
                    {
                        $this->db->where('to.country_id',$searchParams['country_id']);
                    }
                    else
                    {
                        $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                    }
                    
                }
                if($searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

            }
            $this->db->where('to.current_stage_id',4);  
            $this->db->where('oa.check_address',1); 
            $this->db->where('oa.status',1);
            $this->db->where('to.order_delivery_type_id',1);
            $res1 = $this->db->get();
            $num_rows = $res1->num_rows();
        }
        else
        {
            $this->db->select('ro.*,odt.name as order_type');
            $this->db->from('return_order ro');             
            $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');
            $this->db->join('user u','u.sso_id = ro.created_by');
            $this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
            $this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
            $this->db->join('location l','l.location_id = to.country_id');
            if($searchParams['return_number']!='')
                $this->db->like('ro.return_number',$searchParams['return_number']);
            if($searchParams['order_number']!='')
                $this->db->like('to.order_number',$searchParams['order_number']);
            $this->db->where('ro.address_check',1);     
            $this->db->where('ro.order_delivery_type_id',1);
            $this->db->where('ro.status',1);
            if($task_access == 1 )
            {
                $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
            }
            else if($task_access == 2)
            {
                if($searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

                $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            }
            else if($task_access == 3)
            {
                
                if($_SESSION['header_country_id']!='')
                {
                    $this->db->where('to.country_id',$_SESSION['header_country_id']);   
                }
                else
                {
                    if($searchParams['country_id']!='')
                    {
                        $this->db->where('to.country_id',$searchParams['country_id']);
                    }
                    else
                    {
                        $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                    }
                    
                }
                if($searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

            }
            $res2 = $this->db->get();
            $num_rows = $res2->num_rows();
        }
        return $num_rows;
    }
	
	public function get_order_info($tool_order_id)
	{
		$this->db->select('to.*,oda.*,concat(u.sso_id,"- ( ",u.name," )") as sso,concat(wh.wh_code,"- ( ",wh.name," )") as con_wh
							 
						');
		$this->db->from('tool_order to');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	public function get_return_initiated_assets($return_order_id)
	{
		$this->db->select('a.*,ro.*,t.*,concat(u.sso_id,"- (",u.name," )") as sso');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');		
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('order_asset_history oah','oah.order_status_id = osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('user u','u.sso_id = ro.created_by');
		$this->db->where('p.part_level_id',1);
		$this->db->where('ro.return_order_id',$return_order_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function closed_order_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type,to.status as final_status');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['clsd_o_order_number']!='')
			$this->db->like('to.order_number',$searchParams['clsd_o_order_number']);
		if($searchParams['clsd_o_order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['clsd_o_order_delivery_type_id']);
		if($searchParams['clsd_o_request_date']!='')
			$this->db->where('to.request_date>=',$searchParams['clsd_o_request_date']);
		if($searchParams['clsd_o_return_date']!='')
			$this->db->where('to.return_date<=',$searchParams['clsd_o_return_date']);	
		$this->db->limit($per_page, $current_offset);
		if($_SESSION['role_id'] ==2 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}
		$this->db->where('to.current_stage_id',10);
		$this->db->where('to.order_type',2);
		//$this->db->where('a.status',1);
		$res = $this->db->get();
		/*echo $this->db->last_query();exit;
		echo '<pre>';print_r($res->result_array());exit;*/
		return $res->result_array();
	}
	public function closed_order_total_num_rows($searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type,to.status as final_status');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['clsd_o_order_number']!='')
			$this->db->like('to.order_number',$searchParams['clsd_o_order_number']);
		if($searchParams['clsd_o_order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['clsd_o_order_delivery_type_id']);
		/*if($searchParams['clsd_o_current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['clsd_o_current_stage_id']);
		if($searchParams['clsd_o_status']!='')
			$this->db->where('to.status',$searchParams['clsd_o_status']);*/
		if($searchParams['clsd_o_request_date']!='')
			$this->db->where('to.request_date>=',$searchParams['clsd_o_request_date']);
		if($searchParams['clsd_o_return_date']!='')
			$this->db->where('to.return_date<=',$searchParams['clsd_o_return_date']);	
		if($_SESSION['role_id'] ==2 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}				
		$this->db->where('to.current_stage_id',10);
		$this->db->where('to.order_type',2);
		$res = $this->db->get();
		/*echo $this->db->last_query();exit;
		echo '<pre>';print_r($res->result_array());exit;*/
		return $res->num_rows();
	}

	// For Num of rows goto helpers
    public function crossedReturnDateOrdersResults($current_offset, $per_page, $searchParams,$task_access)
    {
        $this->db->select('to.*,odt.name as order_type,CONCAT(u.sso_id," - (",u.name,")") AS user_name,l.name as country_name');
        $this->db->from('tool_order to');   
        $this->db->join('user u','u.sso_id = to.sso_id','LEFT');
        $this->db->join('location l','l.location_id = to.country_id','LEFT');   
        $this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
        $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
        if($searchParams['cr_order_number']!='')
            $this->db->like('to.order_number',$searchParams['cr_order_number']);
        if($searchParams['cr_order_delivery_type_id']!='')
            $this->db->where('to.order_delivery_type_id',$searchParams['cr_order_delivery_type_id']);
        $this->db->where('to.return_date<',date('Y-m-d'));
        $this->db->where('to.order_type',2);
        $this->db->where('to.current_stage_id',7);
        if(@$searchParams['crd_sso_id']!='')
        {
            $this->db->where('to.sso_id',@$searchParams['crd_sso_id']);
        }
        if($task_access == 1 || $task_access == 2)
        {
            if($task_access == 1)
            {
                if(count($this->session->userdata('countriesIndexedArray'))>1)
                {
                    if($this->session->userdata('header_country_id')!='')
                    {
                        $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
                    }
                    else
                    {
                        if($searchParams['crd_country_id']!='')
                        {
                            $this->db->where('to.country_id',$searchParams['crd_country_id']);
                        }
                        else
                        {
                            $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray')); 
                        }
                    }  
                }
                $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
            }
            else
            {
                $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            }
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
                if($searchParams['crd_country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['crd_country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray')); 
                }
            }
        }               
        $this->db->limit($per_page, $current_offset);
        $this->db->order_by('to.country_id,to.tool_order_id');
        $res = $this->db->get();        
        return $res->result_array();
    }
	
	public function getDefectiveAssetInfo($defective_asset_id)
	{
		$this->db->select('a.asset_number,a.country_id,a.asset_id');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id = da.asset_id');
		$this->db->where('da.defective_asset_id',$defective_asset_id);
		$res= $this->db->get();
		return $res->row_array();
	}
	public function defectiveOrMissedAssetInfo($defective_asset_id)
	{
		$this->db->select('t.part_number,t.part_description,p.serial_number,ac.name as condition,
			tt.name as tool_type,pl.name as part_level,p.quantity as qty');
		$this->db->from('defective_asset_health dah');
		$this->db->join('part p','p.part_id = dah.part_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = dah.asset_condition_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->where('dah.defective_asset_id',$defective_asset_id);
		$res = $this->db->get();
		return $res->result_array();

	}
	public function getCalibrationOwnedAssetsDetailsByOrderId($tool_order_id,$asset_ids)
	{
		
		$this->db->select('oah.*,oah.status as history_status_id,oa.ordered_asset_id as ordered_asset_id,oa.asset_id as asset_id,
			a.asset_number as asset_number,t.part_number as part_number,t.part_description as part_description')	;
		$this->db->from('order_asset_history as oah');
		$this->db->join('order_status_history osh','osh.order_status_id = oah.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
        $this->db->where('p.part_level_id',1);
		$this->db->where('osh.current_stage_id',7);
		$this->db->where_in('oa.asset_id',$asset_ids);
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$this->db->group_by('p.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
		public function crossed_expected_delivery_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('to.*');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id=to.tool_order_id');
        $this->db->join('location l','l.location_id = to.country_id');
		if(@$searchParams['order']!='')
			$this->db->like('to.order_number',$searchParams['order']);
		if(@$searchParams['order_type']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_type']);
        if($task_access == 1 || $task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id')); 
            }
            else
            {
                if($searchParams['country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));  
                }
            }
        }
		$this->db->where('to.current_stage_id',6);
		$this->db->where('od.expected_delivery_date<',date('Y-m-d'));
		$this->db->group_by('to.tool_order_id');
		$res = $this->db->get();
		return $res->num_rows();
	}
	
	public function crossed_expected_delivery_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('to.*,od.expected_delivery_date as exp_d_date,l.name as country_name');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id=to.tool_order_id');
        $this->db->join('location l','l.location_id = to.country_id');
		if($searchParams['order']!='')
			$this->db->like('to.order_number',$searchParams['order']);
		if($searchParams['order_type']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_type']);
        if($task_access == 1 || $task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id')); 
            }
            else
            {
                if($searchParams['country_id']!='')
                {
                    $this->db->where('to.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));  
                }
            }
        }
		$this->db->where('to.current_stage_id',6);
		$this->db->where('od.expected_delivery_date<',date('Y-m-d'));
		$this->db->group_by('to.tool_order_id');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function fe_not_acknowledged_tools($tool_order_id)
	{
		$this->db->select('a.asset_number,p.serial_number,
				pl.name as part_level,t.part_number,t.part_description');
		$this->db->from('order_status_history osh');
		$this->db->join('order_asset_history oah','oah.order_status_id=osh.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id=oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id=oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('part_level pl','p.part_level_id = p.part_level_id');
		$this->db->where('osh.tool_order_id',$tool_order_id);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('oa.ordered_asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
}	