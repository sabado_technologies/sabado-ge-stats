<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// created by Srilekha on 29st july 2017 12:39PM
class Repair_m extends CI_Model
{
	/*Repair Tool No of Rows.
    Author:Srilekha
    Time:12.40PM Date:29-07-2017*/
	public function repair_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('rc.rc_asset_id');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		if($searchParams['crn']!='')
			$this->db->like('rc.rc_number',$searchParams['crn']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($task_access==1)
		{
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		elseif($task_access == 2)
		{
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
			if($searchParams['whr_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whr_id']);
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rc.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rc.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rc.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['whr_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whr_id']);
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.current_stage_id',20);
		$this->db->where('rc.rca_type',1);
		$this->db->where('rc.status',1);
		$this->db->group_by('rc.rc_asset_id');
		$this->db->order_by('rc.rc_asset_id ASC');
		$res=$this->db->get();
		return $res->num_rows();
	}
	/*Repair Tool results.
    Author:Srilekha
    Time:12.42PM Date:29-07-2017*/
	public function repair_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('rc.*,a.asset_id,a.asset_number,t.part_number,t.part_description,w.name as warehouse,w.wh_code,w.wh_id,a.wh_id,a.approval_status,a.status as asset_status,as.name as asset_status_name,p.serial_number');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		if($searchParams['crn']!='')
			$this->db->like('rc.rc_number',$searchParams['crn']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.current_stage_id',20);
		$this->db->where('rc.rca_type',1);
		$this->db->where('rc.status',1);
		if($task_access==1)
		{
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
			//$this->db->where('a.wh_id',$this->session->userdata('s_wh_id'));
		}
		elseif($task_access == 2)
		{
			$this->db->where('rc.country_id',$this->session->userdata('s_country_id'));
			if($searchParams['whr_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whr_id']);
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('rc.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('rc.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('rc.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['whr_id']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whr_id']);
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rc.rc_asset_id');
		$this->db->order_by('rc.rc_asset_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_rc_details($rc_asset_id)
	{
		$this->db->select('rc.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,w.name as warehouse,w.wh_code,p.serial_number');
		$this->db->from('rc_asset rc');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.current_stage_id',20);
		$this->db->where('rc.rc_asset_id',$rc_asset_id);
		$this->db->where('rc.status',1);
		$this->db->group_by('rc.rc_asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	
	/*Tool ID.
    Author:Srilekha
    Time:03.55PM Date:29-07-2017*/
	public function get_top_tool_id($asset_id)
	{
		$this->db->select('p.tool_id');
		$this->db->from('part p');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->where('a.asset_id',$asset_id);
		$this->db->order_by('p.part_id ASC');
		$res = $this->db->get();
		$result = $res->row_array();
		return $result['tool_id'];
	}
	/*Asset Details.
    Author:Srilekha
    Time:03.56PM Date:29-07-2017*/
	public function asset_details_view($asset_id,$tool_id)
	{
		$this->db->select('t.part_description,t.part_number,t.tool_code,
			p.serial_number,p.cost,ac.name as asset_condition_name,
			p.remarks as part_remarks,p.length,p.breadth,p.height,a.*,t.kit as kit,m.name as modality_name,
			tt.name as tool_type_name,wh.wh_code as warehouse_code,wh.wh_id,
			a.sub_inventory as sub_inventory,at.name as asset_type,
			s.name as supplier_name,a.remarks as asset_remarks,
			ct.name as calibration,cs.name as cs_name,a.cal_due_date as due_date,
			t.manufacturer,t.po_number as po,t.earpe_number as earpe,
			a.date_of_use as date_of_used,ad.name as document,
			t.part_number as part_num,t.tool_code as toolcode,
			t.part_description as description,p.serial_number as serialnumber,
			p.remarks as remark,p.status as statuss,p.length as p_length,
			p.breadth as p_breadth,p.height as p_height,p.cost as p_cost,
			al.name as asset_level_name,p.quantity,cs.*');
		$this->db->from('asset a');
		$this->db->join('part p','a.asset_id = p.asset_id');
		$this->db->join('tool t','p.tool_id =t.tool_id');
		$this->db->join('asset_level al','al.asset_level_id = t.asset_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id =t.tool_type_id');
		$this->db->join('asset_type at','at.asset_type_id =t.asset_type_id');
		$this->db->join('asset_document ad','ad.asset_id =a.asset_id','left');		
		$this->db->join('modality m','m.modality_id =a.modality_id ');
		$this->db->join('supplier cs','cs.supplier_id = a.cal_supplier_id','left');
		$this->db->join('calibration_type ct','ct.cal_type_id = t.cal_type_id');
		$this->db->join('supplier s','s.supplier_id=t.supplier_id');
		$this->db->join('warehouse wh','a.wh_id = wh.wh_id');
		$this->db->join('asset_status as','as.asset_status_id =a.status ');
		$this->db->join('part_level pl','pl.part_level_id= p.part_level_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = p.status');
		$this->db->where('p.asset_id',$asset_id);
		$this->db->where('p.tool_id',$tool_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	/*OPen Repair Request Tool No of Rows.
    Author:Srilekha
    Time:11.23PM Date:31-07-2017*/
	public function open_repair_total_num_rows($searchParams,$task_access='')
	{
		$status=array(1,2);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		$this->db->where('rco.rc_type',1);
		$this->db->where_in('rco.status',$status);
		$this->db->where_not_in('rca.current_stage_id',20);
		if($task_access!='')
		{
			if($task_access==1)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res=$this->db->get();
		return $res->num_rows();
	}
	/*Open Repair Request Tool results.
    Author:Srilekha
    Time:11.27PM Date:31-07-2017*/
	public function open_repair_results($current_offset, $per_page, $searchParams,$task_access='')
	{
		$status=array(1,2);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		$this->db->where('rco.rc_type',1);
		$this->db->where_in('rco.status',$status);
		$this->db->where_not_in('rca.current_stage_id',20);
		if($task_access!='')
		{
			if($task_access==1)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_asset_details($rc_order_id)
	{
		$status=array(1,2);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where_not_in('rca.current_stage_id',20);
		$this->db->where('rca.rca_type',1);
		$this->db->where_in('rca.status',$status);
		$res = $this->db->get();
		return $res->result_array();

	}
	public function get_asset_details_row($rc_order_id,$asset_id)
	{
		$status=array(1,2);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,tt.name as tool_type,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('tool_type tt','tt.tool_type_id=t.tool_type_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where_not_in('rca.current_stage_id',20);
		$this->db->where('rca.rca_type',1);
		$this->db->where_in('rca.status',$status);
		$res = $this->db->get();
		return $res->row_array();

	}
	/*Repair Details.
    Author:Srilekha
    Time:11.49PM Date:31-07-2017*/
    public function repair_details_view($repair_id)
    {
    	$this->db->select('r.*,wh.name as ware_house,s.name as supplier_name,cs.name as current_stage');
    	$this->db->from('repair r');
    	$this->db->join('current_stage cs','cs.current_stage_id=r.current_stage_id');
    	$this->db->join('warehouse wh','wh.wh_id=r.wh_id');
    	$this->db->join('supplier s','s.supplier_id=r.supplier_id');
    	$this->db->where('r.repair_id',$repair_id);
    	$res=$this->db->get();
    	return $res->row_array();
    }
    /*Warehouse Repair Request Tool No of Rows.
    Author:Srilekha
    Time:12.36AM Date:1-08-2017*/
	public function wh_repair_total_num_rows($searchParams)
	{
		$this->db->select('t.part_number,a.asset_id,ra.asset_id as repair_asset,
						   a.asset_number,wh.name as warehouse,cs.name as current_stage,wh.wh_code,
						   t.part_description,p.part_level_id,r.*');
		$this->db->from('repair r');
		$this->db->join('repair_asset ra','ra.repair_id=r.repair_id');
		$this->db->join('asset a','a.asset_id=ra.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage cs','cs.current_stage_id=r.current_stage_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		if($searchParams['wh_id']!='')
			$this->db->where('a.wh_id',$searchParams['wh_id']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		$this->db->where('a.approval_status',1);
		$this->db->where('p.part_level_id',1);
		$this->db->where('r.current_stage_id',17);
		$this->db->group_by('r.repair_id');
		$res=$this->db->get();
		return $res->num_rows();
	}
	/*Warehouse Repair Request Tool results.
    Author:Srilekha
    Time:12.37AM Date:1-08-2017*/
	public function wh_repair_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('t.part_number,a.asset_id,ra.asset_id as repair_asset,
						   a.asset_number,wh.name as warehouse,cs.name as current_stage,wh.wh_code,
						   t.part_description,p.part_level_id,r.*');
		$this->db->from('repair r');
		$this->db->join('repair_asset ra','ra.repair_id=r.repair_id');
		$this->db->join('asset a','a.asset_id=ra.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('current_stage cs','cs.current_stage_id=r.current_stage_id');
		if($searchParams['wh_id']!='')
			$this->db->where('a.wh_id',$searchParams['wh_id']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		$this->db->where('a.approval_status',1);
		$this->db->where('p.part_level_id',1);
		$this->db->where('r.current_stage_id',17);
		$this->db->group_by('r.repair_id');
		$this->db->order_by('r.repair_id DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	/*Repair Asset Health
    Author:Srilekha
    Time:12.58AM Date:1-08-2017*/
	public function get_repair_asset_health($rah_id,$part_id)
	{
		$this->db->select('ac.name as asset_condition_name,rah.remarks');
		$this->db->from('repair_asset_health rah');
		$this->db->join('asset_condition ac','ac.asset_condition_id = rah.asset_condition_id');
		$this->db->where('rah.rah_id',$rah_id);
		$this->db->where('rah.part_id',$part_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	/*Get asset Part details
	Author:Srilekha
	Time:01:05AM Date:01-08-2017*/ 
	public function get_asset_part_details($asset_id)
	{
		
		$this->db->select('a.asset_id,a.asset_number,p.part_id,
						  pl.name as part_level_name,t.part_number,
						  t.part_description,p.quantity,p.status,t.tool_id, a.status as asset_status,p.serial_number');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('a.status',10);
		$this->db->order_by('p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}
	public function closed_wh_repair_total_num_rows($searchParams)
	{
		$status=array(2,10);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['whr_id']!='')
			$this->db->like('rco.wh_id',$searchParams['whr_id']);
		$this->db->where('rco.rc_type',1);
		$this->db->where_in('rco.status',$status);
		$this->db->where('rsh.current_stage_id',22);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res=$this->db->get();
		return $res->num_rows();
	}

	public function closed_wh_repair_results($current_offset, $per_page, $searchParams)
	{
		$status=array(2,10);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		if($searchParams['whr_id']!='')
			$this->db->like('rco.wh_id',$searchParams['whr_id']);
		$this->db->where('rco.rc_type',1);
		$this->db->where_in('rco.status',$status);
		$this->db->where('rsh.current_stage_id',22);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_vendor_closed_assets($rc_order_id)
	{
		$status=array(1,10);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->join('rc_status_history rsh','rsh.rc_asset_id=rca.rc_asset_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where('rsh.current_stage_id',22);
		$this->db->where('rca.rca_type',1);
		$this->db->where_in('rca.status',$status);
		//$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
	/*Warehouse Vendor Repair Request Tool No of Rows.
    Author:Srilekha
    Time:01.02PM Date:1-08-2017*/
	public function wh_vendor_repair_total_num_rows($searchParams,$task_access='')
	{
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		$this->db->where('rco.rc_type',1);
		$this->db->where('rco.status',2);
		$this->db->where('rca.current_stage_id',22);
		if($task_access!='')
		{
			if($task_access==1)
			{
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
				else
				{
					if(isset($_SESSION['whsIndededArray']) && $searchParams['whr_id']=='')
					{
						$this->db->where_in('rco.wh_id',$_SESSION['whsIndededArray']); 
					}
					else			
					{	
						$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
					}	
				}
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res=$this->db->get();
		return $res->num_rows();
	}
	/*Warehouse Vendor Repair Request Tool results.
    Author:Srilekha
    Time:01.03PM Date:1-08-2017*/
	public function wh_vendor_repair_results($current_offset, $per_page, $searchParams,$task_access='')
	{
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		$this->db->where('rco.rc_type',1);
		$this->db->where('rco.status',2);
		$this->db->where('rca.current_stage_id',22);
		if($task_access!='')
		{
			if($task_access==1)
			{
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
				else
				{
					if(isset($_SESSION['whsIndededArray']) && $searchParams['whr_id']=='')
					{
						$this->db->where_in('rco.wh_id',$_SESSION['whsIndededArray']); 
					}
					else			
					{	
						$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
					}	
				}
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_vendor_assets($rc_order_id)
	{
		$c_stage=array(22,23);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where_in('rca.current_stage_id',$c_stage);
		$this->db->where('rca.rca_type',1);
		$this->db->where('rca.status',1);
		//$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_vendor_assets_row($rc_order_id,$asset_id)
	{
		
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where('rca.current_stage_id',22);
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('rca.rca_type',1);
		$this->db->where('rca.status',1);
		//$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function check_asset_number($rc_asset_id,$asset_number)
	{
		$this->db->select('a.asset_number,a.asset_id');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rc','rc.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id=rc.asset_id');
		$this->db->where('rc.rc_asset_id',$rc_asset_id);
		$this->db->where('a.asset_number',$asset_number);
		$res=$this->db->get();
		return $res->row_array();
	}



	/*Get Attached Documents by Stage
	Author:Srilekha
	Time:05:32PM Date:01-08-2017*/
	public function get_documents_by_stage($rc_order_id)
	{
		$this->db->select('rcd.*,rca.rc_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('rca_doc rcd','rcd.rc_asset_id=rca.rc_asset_id');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->order_by('rca.rc_asset_id ASC');
		$res= $this->db->get();
		return $res->result_array();
	}
	/*Closed Repair Request Tool No of Rows.
    Author:Srilekha
    Time:06.07PM Date:1-08-2017*/
	public function closed_repair_total_num_rows($searchParams,$task_access='')
	{
		$c_stage=array(24,25,26);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		$this->db->where('rco.rc_type',2);
		$this->db->where_in('rca.current_stage_id',$c_stage);
		if($task_access!='')
		{
			if($task_access==1)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				//$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$res=$this->db->get();
		return $res->num_rows();
	}
	/*Closed Repair Request Tool results.
    Author:Srilekha
    Time:06.08PM Date:1-08-2017*/
	public function closed_repair_results($current_offset, $per_page, $searchParams,$task_access='')
	{
		$c_stage=array(24,25,26);
		$this->db->select('rco.*,w.wh_code,w.name');
		$this->db->from('rc_order rco');
		$this->db->join('warehouse w','w.wh_id=rco.wh_id');
		$this->db->join('rc_asset rca','rca.rc_order_id=rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['rc_number']!='')
			$this->db->like('rca.rc_number',$searchParams['rc_number']);
		if($searchParams['rcb_number']!='')
			$this->db->like('rco.rcb_number',$searchParams['rcb_number']);
		$this->db->where('rco.rc_type',1);
		$this->db->where_in('rca.current_stage_id',$c_stage);
		if($task_access!='')
		{
			if($task_access==1)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				//$this->db->where('rco.wh_id',$this->session->userdata('s_wh_id'));
			}
			elseif($task_access == 2)
			{
				$this->db->where('rco.country_id',$this->session->userdata('s_country_id'));
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
			else if($task_access == 3)
			{
				if($_SESSION['header_country_id']!='')
				{
					$this->db->where('rco.country_id',$_SESSION['header_country_id']);	
				}
				else
				{
					if($searchParams['country_id']!='')
					{
						$this->db->where('rco.country_id',$searchParams['country_id']);
					}
					else
					{
						$this->db->where_in('rco.country_id',$_SESSION['countriesIndexedArray']);	
					}
				}
				if($searchParams['whr_id']!='')
				{
					$this->db->where('rco.wh_id',$searchParams['whr_id']);
				}
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('rca.rc_order_id');
		$this->db->order_by('rco.rc_order_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_closed_asset_details($rc_order_id)
	{
		$c_stage=array(24,25,26);
		$this->db->select('rca.*,a.asset_id,a.asset_number,a.wh_id,t.part_number,t.part_description,c.name as current_stage,p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('asset a','a.asset_id=rca.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('current_stage c','c.current_stage_id=rca.current_stage_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rc_order_id',$rc_order_id);
		$this->db->where_in('rca.current_stage_id',$c_stage);
		$this->db->where('rca.rca_type',1);
		$this->db->where('rca.status',10);
		//$this->db->group_by('rca.rc_asset_id');
		$res = $this->db->get();
		return $res->result_array();

	}
	public function get_from_address()
	{
		$swh_id = $this->session->userdata('swh_id');
		$this->db->select('wh.*,l.name as from_location,
						  in.format_number,in.created_time,in.print_type,
						  r.courier_type,r.courier_number,r.repair_id');
		$this->db->from('repair r');
		$this->db->join('warehouse wh','wh.wh_id = r.wh_id');
		$this->db->join('invoice_number in','in.invoice_number_id = r.invoice_number_id');
		$this->db->join('location l','l.location_id = wh.location_id');
		$this->db->where('wh.wh_id',$swh_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_repair_tool($repair_id)
	{
		$this->db->select('t.part_number,t.part_description,t.hsn_code,count(t.tool_id) as quantity,t.gst_percent,t.cost_in_inr');
		$this->db->from('repair r');
		$this->db->join('repair_asset ra','ra.repair_id = r.repair_id');
		$this->db->join('asset a','a.asset_id = ra.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('r.repair_id',$repair_id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_country_from_rc_asset($rc_asset_id)
    {
    	$this->db->select('wh.country_id');
    	$this->db->from('rc_asset rca');
    	$this->db->join('rc_order rco','rco.rc_order_id = rca.rc_order_id','LEFT');
    	$this->db->join('warehouse wh','wh.wh_id = rco.wh_id','LEFT');
    	$this->db->where('rca.rc_asset_id',$rc_asset_id);
    	$res = $this->db->get();
    	$result = $res->row_array();
    	return $result['country_id'];
    }

    public function get_scanned_asset_list()
	{
		$this->db->select('rcah.rcah_id,rco.rcb_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('rc_status_history rcsh','rcsh.rc_asset_id = rca.rc_asset_id');
		$this->db->join('rc_asset_history rcah','rcah.rc_asset_id = rca.rc_asset_id AND rcsh.rcsh_id = rcah.rcsh_id');
		$this->db->where('rcsh.current_stage_id',21);
		$this->db->where('rcsh.modified_time',NULL);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function check_scanned_asset_list($rc_order_id)
	{
		$this->db->select('rcah.rcah_id');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('rc_status_history rcsh','rcsh.rc_asset_id = rca.rc_asset_id');
		$this->db->join('rc_asset_history rcah','rcah.rc_asset_id = rca.rc_asset_id AND rcsh.rcsh_id = rcah.rcsh_id');
		$this->db->where('rcsh.current_stage_id',21);
		$this->db->where('rcsh.modified_time',NULL);
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$res = $this->db->get();
		return $res->num_rows();
	}
}