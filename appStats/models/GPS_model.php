<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GPS_model extends CI_Model 
{
    public function gps_results($current_offset, $per_page, $searchParams,$task_access)
    {
        
        $this->db->select('gps.*,l.name as country_name');
        $this->db->from('gps_tool gps');
        $this->db->join('location l','l.location_id=gps.country_id');
        if($searchParams['gps_name']!='')
            $this->db->like('gps.name',$searchParams['gps_name']);
        if($searchParams['gps_uid']!='')
            $this->db->like('gps.uid_number',$searchParams['gps_uid']);
        if($task_access == 1 || $task_access == 2)
        {
            $this->db->where('gps.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('gps.country_id',$this->session->userdata('header_country_id')); 
            }
            else
            {
                if($searchParams['gps_country']!='')
                {
                    $this->db->where('gps.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('gps.country_id',$this->session->userdata('countriesIndexedArray'));  
                }
            }
        }
        $this->db->limit($per_page, $current_offset);
        $this->db->order_by('gps_tool_id','ASC');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function gps_total_num_rows($searchParams,$task_access)
    {
        $this->db->select('gps.*,l.name as country_name');
        $this->db->from('gps_tool gps');
        $this->db->join('location l','l.location_id=gps.country_id');
        if($searchParams['gps_name']!='')
            $this->db->like('gps.name',$searchParams['gps_name']);
        if($searchParams['gps_uid']!='')
            $this->db->like('gps.uid_number',$searchParams['gps_uid']);
        if($task_access == 1 || $task_access == 2)
        {
            $this->db->where('gps.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('gps.country_id',$this->session->userdata('header_country_id')); 
            }
            else
            {
                if($searchParams['gps_country']!='')
                {
                    $this->db->where('gps.country_id',$searchParams['country_id']);
                }
                else
                {
                    $this->db->where_in('gps.country_id',$this->session->userdata('countriesIndexedArray'));  
                }
            }
        }
        $this->db->order_by('gps_tool_id','ASC');
        $res = $this->db->get();
        return $res->num_rows();
    }
    
    public function is_gps_uid_exist($uid_number,$gps_tool_id,$country_id)
    {       
        $this->db->select();
        $this->db->from('gps_tool');
        $this->db->where('uid_number',$uid_number);
        $this->db->where('country_id',$country_id);
        if($gps_tool_id!='' || $gps_tool_id != 0)
        {
            $this->db->where_not_in('gps_tool_id',$gps_tool_id);
        }
        $query = $this->db->get();
        return ($query->num_rows()>0)?1:0;
    }
}   