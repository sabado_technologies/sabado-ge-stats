<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_m extends CI_Model 
{
public function return_tool_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('t.*,m.name as modality_name,et.eq_model_id');
		$this->db->distinct('p.tool_id');
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['eq_model_id']!='')
			$this->db->where('et.eq_model_id',$searchParams['eq_model_id']);
		$this->db->group_by('p.asset_id');		
		$this->db->where('p.part_level_id',1);
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function return_tool_total_num_rows($searchParams)
	{
		$this->db->select('t.*,m.name as modality_name,et.eq_model_id');
		$this->db->distinct('p.tool_id');
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['eq_model_id']!='')
			$this->db->where('et.eq_model_id',$searchParams['eq_model_id']);
		$this->db->group_by('p.asset_id');
		$this->db->where('p.part_level_id',1);

		
		//$this->db->where('a.status',1);
		$res = $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->num_rows();
	}
    
    public function return_order_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',$searchParams['request_date']);
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',$searchParams['return_date']);
		$this->db->where('to.current_stage_id!=',10);
		$this->db->where('to.status<',2);
		
		//$this->db->where('a.status',1);
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		//echo $this->
		//echo $this->db->last_query();exit;
		//echo '<pre>';print_r($res->result_array());exit;
		return $res->result_array();
	}
	
	public function return_order_total_num_rows($searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',$searchParams['request_date']);
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',$searchParams['return_date']);
		$this->db->where('to.current_stage_id!=',10);	
		$this->db->where('to.status<',2);	
		//$this->db->where('a.status',1);
		$res = $this->db->get();
		/*//echo $this->db->last_query();exit;
		echo '<pre>';print_r($res->result_array());exit;*/
		return $res->num_rows();
	}

	public function return_closed_order_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type,to.status as final_status');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['status']!='')
			$this->db->where('to.status',$searchParams['status']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',$searchParams['request_date']);
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',$searchParams['return_date']);

			
		//$this->db->where('a.status',1);
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		//echo $this->
		//echo $this->db->last_query();exit;
		//echo '<pre>';print_r($res->result_array());exit;
		return $res->result_array();
	}
	
	public function return_closed_order_total_num_rows($searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type,to.status as final_status');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['status']!='')
			$this->db->where('to.status',$searchParams['status']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',$searchParams['request_date']);
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',$searchParams['return_date']);
					
		//$this->db->where('a.status',1);
		$res = $this->db->get();
		/*echo $this->db->last_query();exit;
		echo '<pre>';print_r($res->result_array());exit;*/
		return $res->num_rows();
	}
	public function return_get_order_info($tool_order_id)
	{
		$this->db->select('to.*,oda.*');
		$this->db->from('tool_order to');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');		
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	public function return_get_ordered_tools($tool_order_id)
	{
		$this->db->select('o.*,t.*');
		$this->db->from('ordered_tool o');
		$this->db->join('tool t','t.tool_id = o.tool_id');		
		$this->db->where('o.tool_order_id',$tool_order_id);
		$this->db->where('o.status<',3);
		$res = $this->db->get();
		return $res->result_array();
	}
}	