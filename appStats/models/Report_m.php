<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// created by Srilekha on 17th November 2017 12:09PM
class Report_m extends CI_Model
{
	public function get_in_calibration_data($current_stage_id,$searchParams)
	{
		$this->db->select('count(*) as count');
		$this->db->from('asset a');
		$this->db->join('rc_asset rc','rc.asset_id=a.asset_id');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('modality m','m.modality_id=a.modality_id');
		if($searchParams['modality_id']!='')
			$this->db->where('a.modality_id',$searchParams['modality_id']);
		if($searchParams['tool_id']!='')
			$this->db->where('p.tool_id',$searchParams['tool_id']);
		$this->db->where('rc.current_stage_id',$current_stage_id);
		if($current_stage_id !=11)
		{
			$this->db->where('a.status',4);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rc.status',1);
		$res = $this->db->get();
		return $res->row_array();
	}
	
	#srilekha 30-05-2018
	public function get_at_wh_count($searchParams)
	{
		//$status=array(1,2,3,8);
		$this->db->select('count(DISTINCT(a.asset_id)) as count');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('modality m','m.modality_id=a.modality_id');
		$this->db->join('asset_position ap','ap.asset_id=a.asset_id');
		if($searchParams['modality_id']!='')
			$this->db->where('a.modality_id',$searchParams['modality_id']);
		if($searchParams['tool_id']!='')
			$this->db->where('p.tool_id',$searchParams['tool_id']);
		if($searchParams['name']=='Out of Calibration')
		{
			$this->db->where('a.status',12);
			$this->db->where('ap.transit',0);
			$this->db->where('ap.wh_id!=',NULL);
			$this->db->where('ap.sso_id',NULL);
			$this->db->where('ap.to_date',NULL);
		}

		$this->db->where('a.country_id',$searchParams['country_id']);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.country_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	#srilekha 30-05-2018
	public function get_in_transit_count($searchParams)
	{
		$this->db->select('count(DISTINCT(a.asset_id)) as count');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('modality m','m.modality_id=a.modality_id');
		$this->db->join('asset_position ap','ap.asset_id=a.asset_id');
		if($searchParams['modality_id']!='')
			$this->db->where('a.modality_id',$searchParams['modality_id']);
		if($searchParams['tool_id']!='')
			$this->db->where('p.tool_id',$searchParams['tool_id']);
		if($searchParams['name']=='Out of Calibration')
		{
			$this->db->where('a.status',12);
			$this->db->where('ap.transit',1);
			$this->db->where('ap.to_date',NULL);
		}
		$this->db->where('a.country_id',$searchParams['country_id']);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.country_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	#srilekha 30-05-2018
	public function get_field_deployed_count($searchParams)
	{
		$this->db->select('count(DISTINCT(a.asset_id)) as count');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('modality m','m.modality_id=a.modality_id');
		$this->db->join('asset_position ap','ap.asset_id=a.asset_id');
		if($searchParams['modality_id']!='')
			$this->db->where('a.modality_id',$searchParams['modality_id']);
		if($searchParams['tool_id']!='')
			$this->db->where('p.tool_id',$searchParams['tool_id']);
		if($searchParams['name']=='Out of Calibration')
		{
			$this->db->where('a.status',12);
			$this->db->where('ap.sso_id!=',NULL);
			$this->db->where('ap.wh_id',NULL);
			$this->db->where('ap.to_date',NULL);
			$this->db->where('ap.transit',0);
		}
		$this->db->where('a.country_id',$searchParams['country_id']);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.country_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_at_cal_vendor_count($searchParams)
	{
		$this->db->select('count(*) as count');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('modality m','m.modality_id=a.modality_id');
		$this->db->join('asset_position ap','ap.asset_id=a.asset_id');
		if($searchParams['modality_id']!='')
			$this->db->where('a.modality_id',$searchParams['modality_id']);
		if($searchParams['tool_id']!='')
			$this->db->where('p.tool_id',$searchParams['tool_id']);
		$this->db->where('ap.supplier_id!=',NULL);
		$this->db->where('ap.to_date',NULL);
		$this->db->where('a.status',4);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	#srilekha 30-05-2018
	public function download_field_deployed_excel($searchParams)
	{
		$this->db->select('a.asset_number,a.cal_due_date,t.part_number,t.part_description,u.sso_id,tt.name as tool_type,u.name as user_name,l.name as country');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('tool_type tt','t.tool_type_id=tt.tool_type_id');
		$this->db->join('modality m','m.modality_id=a.modality_id');
		$this->db->join('asset_position ap','ap.asset_id=a.asset_id');
		$this->db->join('user u','u.sso_id=ap.sso_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['modality_id']!='')
			$this->db->where('a.modality_id',$searchParams['modality_id']);
		if($searchParams['tool_id']!='')
			$this->db->where('p.tool_id',$searchParams['tool_id']);
		$this->db->where('ap.sso_id!=','');
		$this->db->where('ap.transit',0);
		$this->db->where('ap.to_date',NULL);
		$this->db->where('a.country_id',$searchParams['country_id']);
		$this->db->where('a.status',12);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}
		
	#srilekha 30-05-2018
	public function get_all_assets_in_wh($stages,$searchParams)
	{
		$this->db->select('count(DISTINCT(a.asset_id)) as count');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('modality m','m.modality_id=a.modality_id');
		/*if($stages=='At WH')
			$this->db->where_not_in('a.status',$status);
		if($stages=='In Transit')
			$this->db->where('a.status',8);
		if($stages=='Field Deployed')
			$this->db->where('a.status',3);*/
		if($searchParams['modality_id']!='')
			$this->db->where('a.modality_id',$searchParams['modality_id']);
		if($searchParams['tool_id']!='')
			$this->db->where('p.tool_id',$searchParams['tool_id']);
		$this->db->where('a.country_id',$searchParams['country_id']);
		$this->db->where('a.status',$stages);
		$this->db->where('t.cal_type_id',1);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.country_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_all_asset_status()
	{
		$status=array(4,12);
		$this->db->select('asset_status_id,name');
		$this->db->from('asset_status');
		$this->db->where_not_in('asset_status_id',$status);
		$res=$this->db->get();
		return $res->result_array();
	}

	/*created by prasad 24-05-2018*/
	public function get_zones_based_on_country($searchParams)
	{   
		if($searchParams['category']!='')
		{
			$cat=$this->Common_model->get_value('location',array('name'=>$searchParams['category']),'location_id');
		}
		$this->db->from('location');
		$this->db->where('level_id',3);
		if($searchParams['country_id']!='')
		{
			$this->db->where('parent_id',$searchParams['country_id']);
		}
		if($searchParams['category']!='')
		{
			$this->db->where('parent_id',$cat);
		}	
		$this->db->where('status',1);
		$res=$this->db->get();
		return $res->result_array();
	}

	/*created by prasad 24-05-2018*/
	public function get_second_level_zone($category,$first_x_cat,$task_access)
	{
		if($task_access ==1 || $task_access ==2)
		{
			$x_cat = $_SESSION['s_country_id'];
		}
		else if($task_access ==3 && $_SESSION['header_country_id']!='') {
			$x_cat = $_SESSION['header_country_id'];
		}
		else
		{
			$x_cat = $this->Common_model->get_value('location',array('name'=>$first_x_cat),'location_id');
		}
		$location_id = $this->Common_model->get_value('location',array('parent_id'=>$x_cat,'name'=>$category),'location_id');
		return $location_id;
	}

	/*created by prasad 24-05-2018*/
	public function get_wh_by_wh_code($var,$country_id)
	{
		$this->db->from('warehouse');
		$this->db->where('wh_code',trim($var[0]));
		if($country_id!='')
		{
			$this->db->Where('country_id',$country_id);
		}
		$res=$this->db->get();
		$result=$res->row_array();
		return $result['wh_id'];
	}

	/*created by prasad 24-05-2018*/
	public function get_zone_details_based_on_country($searchParams)
	{   
		if($searchParams['category']!='')
		{
			$cat=$this->Common_model->get_value('location',array('name'=>$searchParams['category']),'location_id');
		}
		$this->db->from('location');
		$this->db->where('level_id',3);
		if($searchParams['country_id']!='')
		{
			$this->db->where('parent_id',$searchParams['country_id']);
		}
		if($searchParams['category']!='')
		{
			$this->db->where('parent_id',$cat);
		}	
		$this->db->where('location_id',$searchParams['zone']);
		$this->db->where('status',1);
		$res=$this->db->get();
		return $res->row_array();
	}
}