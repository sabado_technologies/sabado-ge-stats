<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pickup_point_m extends CI_Model 
{
	public function get_oah_data($order_status_id)
	{
		$this->db->select('*');
		$this->db->from('order_asset_history');
		$this->db->where('order_status_id',$order_status_id);
		$this->db->where_in('status',array(1,2));
		$res = $this->db->get();
		return $res->result_array();
	}

	public function pickup_list_total_num_rows($searchParams,$task_access)
	{
		$return_type = array(1,2,4);
		$approval_status = array(0,2);

		$this->db->select('to.order_number');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l1','l1.location_id = to.country_id','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = ro.wh_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if($searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($searchParams['date']!='')
			$this->db->like('ro.created_time',format_date($searchParams['date']));
		if($searchParams['return_type_id']!='')
			$this->db->where('ro.return_type_id',$searchParams['return_type_id']);
		if($task_access == 1 )
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whid']=='')
					$this->db->where_in("ro.wh_id",$_SESSION['whsIndededArray']); 
				else				
					$this->db->where('ro.wh_id',$this->session->userdata('s_wh_id'));	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}

		}
		$this->db->where('osh.current_stage_id',8);//waiting for pickup
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where('address_check!=',1);
		
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where_in('ro.return_approval',$approval_status);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}



	public function pickup_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$return_type = array(1,2,4);
		$approval_status = array(0,2);
		//$wh_id = $this->session->userdata('swh_id');
		$this->db->select('to.order_number,ro.return_number,ro.created_time,
						   concat(ro.address3,", ",ro.address4) as location_name,ro.return_order_id,l1.name as country_name,
						   concat(wh.wh_code," -(",wh.name,")") as wh_name,
						   concat(u.sso_id," -(",u.name,")") as sso_name,
						   rt.name as return_type_name');
		$this->db->from('return_order ro');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l1','l1.location_id = to.country_id','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = ro.wh_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if($searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($searchParams['date']!='')
			$this->db->like('ro.created_time',format_date($searchParams['date']));
		if($searchParams['return_type_id']!='')
			$this->db->where('ro.return_type_id',$searchParams['return_type_id']);
		if($task_access == 1 )
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whid']=='')
					$this->db->where_in("ro.wh_id",$_SESSION['whsIndededArray']); 
				else				
					$this->db->where('ro.wh_id',$this->session->userdata('s_wh_id'));	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}

		}
		//$this->db->where('ro.wh_id',$wh_id);
		$this->db->where('osh.current_stage_id',8);//waiting for pickup
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where('address_check!=',1);
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where_in('ro.return_approval',$approval_status);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function closed_pickup_list_total_num_rows($searchParams,$task_access)
	{
		$return_type = array(1,2,4);
		$approval_status = array(0,2);

		//$wh_id = $this->session->userdata('swh_id');
		$this->db->select('to.order_number');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = to.country_id','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = ro.wh_id','LEFT');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if($searchParams['date']!='')
			$this->db->like('ro.created_time',format_date($searchParams['date']));
		if(@$searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($searchParams['return_type_id']!='')
			$this->db->where('ro.return_type_id',$searchParams['return_type_id']);
		if($task_access == 1 )
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whid']=='')
				{
					$this->db->where_in('ro.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('ro.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whid']);
			}

		}
		//$this->db->where('ro.wh_id',$wh_id);
		$this->db->where('osh.current_stage_id',9);//waiting for pickup
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where('ro.print_id IS NOT NULL');
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where_in('ro.return_approval',$approval_status);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function closed_pickup_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$return_type = array(1,2,4);
		$approval_status = array(0,2);
		$this->db->select('to.order_number,ro.return_number,ro.created_time,ro.expected_arrival_date,
						   l.name as location_name,ro.return_order_id,l1.name as country_name,
						   concat(wh.wh_code," -(",wh.name,")") as wh_name,
						   concat(u.sso_id," -(",u.name,")") as sso_name,to.current_stage_id as tcs,rt.name as return_type_name');
		$this->db->from('return_order ro');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = to.country_id','LEFT');
		$this->db->join('warehouse wh','wh.wh_id = ro.wh_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['return_type_id']!='')
			$this->db->where('ro.return_type_id',$searchParams['return_type_id']);
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if($searchParams['date']!='')
			$this->db->like('ro.created_time',format_date($searchParams['date']));
		if(@$searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($task_access == 1 )
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whid']=='')
				{
					$this->db->where_in('ro.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('ro.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whid']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whid']);
			}

		}
		//$this->db->where('ro.wh_id',$wh_id);
		$this->db->where('osh.current_stage_id',9);//waiting for pickup
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where('ro.print_id IS NOT NULL');
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where_in('ro.return_approval',$approval_status);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_return_details($return_order_id,$rto_id)
	{
		$data = array(8,9);
		$this->db->select('ro.*, to.order_number, 
						   to.sso_id, u.name as user_name,
						   l.name as location_name,
						   osh.order_status_id,to.tool_order_id,rto.rto_id,to.country_id');
		$this->db->from('return_order ro');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('osh.rto_id',$rto_id);
		$this->db->where_in('osh.current_stage_id',$data);
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->order_by('osh.order_status_id DESC');
		$this->db->limit(1);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_asset_list_by_osh($order_status_id)
	{
		$this->db->select('a.asset_id, a.asset_number, t.part_number, t.tool_code, t.part_description, a.asset_id, oa.ordered_asset_id, count(t.tool_id) as t_quantity, p.serial_number');
		$this->db->from('order_asset_history oah');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('oah.order_status_id',$order_status_id);
		$this->db->group_by('a.asset_id');  // modified by maruthi on 20th July'18
		$res = $this->db->get();
		return $res->result_array();	
	}

	public function get_closed_return_details($return_order_id,$rto_id)
	{
		//$wh_id = $this->session->userdata('swh_id');
		$this->db->select('ro.*, to.order_number, 
						   to.sso_id, u.name as user_name,
						   l.name as location_name,osh.order_status_id,to.tool_order_id,rto.rto_id');
		$this->db->from('return_order ro');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->where('osh.rto_id',$rto_id);
		$this->db->where('osh.current_stage_id',10);//waiting for pickup
		$this->db->where('to.order_type',2); //tool order = 2
		$res = $this->db->get();
		return $res->row_array();
	}

	public function open_fe_returns_list_total_num_rows($searchParams,$task_access)
	{
		$return_type = array(1,2);
		$this->db->select('to.order_number');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = ro.country_id');
		$this->db->join('warehouse wh','wh.wh_id = ro.ro_to_wh_id'); 
		$this->db->join('user u','u.sso_id = to.sso_id');
		if(@$searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if(@$searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if(@$searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if(@$searchParams['date']!='')
			$this->db->where('ro.expected_arrival_date',format_date($searchParams['date']));
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('ro.ro_to_wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('ro.ro_to_wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}

			$this->db->where('ro.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('ro.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('ro.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}

		}

		//$this->db->where('ro.ro_to_wh_id',$wh_id);
		$this->db->where('osh.current_stage_id',9);//At Transit from fe to wh
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function open_fe_returns_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$return_type = array(1,2);
		//$wh_id = $this->session->userdata('swh_id');
		$this->db->select('to.order_number,ro.return_number,ro.expected_arrival_date,l.name as location_name,ro.return_order_id,l1.name as country,
			concat(wh.wh_code," -(",wh.name,")") as wh_name,
			concat(u.sso_id," -(",u.name,")") as sso_name');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = ro.country_id');
		$this->db->join('warehouse wh','wh.wh_id = ro.ro_to_wh_id'); 
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if($searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($searchParams['date']!='')
			$this->db->where('ro.expected_arrival_date',format_date($searchParams['date']));
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('ro.ro_to_wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('ro.ro_to_wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}

			$this->db->where('ro.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('ro.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('ro.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}

		}
		//$this->db->where('ro.ro_to_wh_id',$wh_id);
		$this->db->where('osh.current_stage_id',9);//At Transit from fe to wh
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_asset_components($asset_id,$order_status_id)
	{
		$this->db->select('p.serial_number,p.part_id,pl.name as part_level,p.quantity,t.part_number,t.part_description,p.part_id');
		$this->db->from('ordered_asset oa');
		$this->db->join('order_asset_history oah','oah.ordered_asset_id = oa.ordered_asset_id');
		$this->db->join('order_asset_health oahealth','oahealth.oah_id = oah.oah_id');
		$this->db->join('part p','p.part_id = oahealth.part_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('oa.asset_id',$asset_id);
		$this->db->where('oah.order_status_id',$order_status_id);
		$this->db->order_by('p.part_level_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function closed_fe_returns_total_num_rows($searchParams,$task_access)
	{
		$return_type = array(1,2,4);
		$approval_status = array(0,2);
		$this->db->select('ro.return_order_id');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = ro.country_id');
		$this->db->join('warehouse wh','wh.wh_id = ro.ro_to_wh_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if($searchParams['date']!='')
			$this->db->where('ro.received_date',format_date($searchParams['date']));
		if(@$searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('ro.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('ro.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

		}
		//$this->db->where('ro.ro_to_wh_id',$wh_id);
		$this->db->where('osh.current_stage_id',10);//Closed fe returns
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where_in('ro.return_approval',$approval_status);
		$this->db->order_by('ro.return_number DESC','ro.created_time DESC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function closed_fe_returns_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$return_type = array(1,2,4);
		$approval_status = array(0,2);
		$this->db->select('to.order_number,rt.name as return_type_name,ro.return_number,ro.created_time,l.name as location_name,u.sso_id,ro.return_order_id,ro.received_date,l1.name as country,
			concat(wh.wh_code," -(",wh.name,")") as wh_name,
			concat(u.sso_id," -(",u.name,")") as sso_name');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = ro.country_id');
		$this->db->join('warehouse wh','wh.wh_id = ro.ro_to_wh_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if($searchParams['date']!='')
			$this->db->where('ro.received_date',format_date($searchParams['date']));
		if(@$searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('ro.wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('ro.wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.wh_id',$searchParams['whc_id']);
			}

			$this->db->where('to.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('to.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('to.wh_id',$searchParams['whc_id']);
			}

		}
		//$this->db->where('ro.ro_to_wh_id',$wh_id);
		$this->db->where('osh.current_stage_id',10);//Closed fe returns
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->where_in('ro.return_approval',$approval_status);
		$this->db->order_by('ro.return_number DESC','ro.created_time DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_fe2_tool_count($tool_order_id)
	{
		$status = array(1,2);
		$this->db->select('count(quantity) as tool_count');
		$this->db->from('ordered_tool');
		$this->db->where('tool_order_id',$tool_order_id);
		$this->db->where_in('status',$status);
		$res = $this->db->get();
		$result = $res->row_array();
		if($result['tool_count']!='')
		{
			return $result['tool_count'];
		}
		else
		{
			return 0;
		}
	}

	public function get_fe2_asset_count($tool_order_id)
	{
		$status = array(1,2);
		$this->db->select('count(oa.ordered_asset_id) as asset_count');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->where('ot.tool_order_id',$tool_order_id);
		$this->db->where_in('ot.status',$status);
		$this->db->where('oa.status !=',3);
		$res = $this->db->get();
		$result = $res->row_array();
		if($result['asset_count']!='')
		{
			return $result['asset_count'];
		}
		else
		{
			return 0;
		}
	}

	#modified by koushik //19-05-2018
	public function get_from_address($return_order_id)
	{
		$this->db->select('ro.address1,ro.remarks,ro.address2,ro.address3,ro.address4,
						   ro.zip_code as pin_code,pf.format_number,pf.created_time,
						   ro.return_type_id,l.name as from_location,pf.print_type,
						   ro.gst_number,ro.pan_number,ro.courier_type,ro.courier_number,ro.return_number,u.name,u.sso_id,ro.return_number,ro.billed_to,ro.courier_name,ro.contact_person,ro.phone_number,c.name as currency_name,ro.vehicle_number,
						   CONCAT(u.sso_id,"-(",u.name,")") AS sso_name,u.mobile_no');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('currency c','c.location_id = to.country_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('print_format pf','pf.print_id = ro.print_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->where('ro.return_order_id',$return_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_to_address1($ro_to_wh_id)
	{
		$this->db->select('wh.address1,wh.address2,wh.address3,wh.address4,
						   wh.pin_code,l.name as to_location,wh.gst_number,wh.pan_number');
		$this->db->from('warehouse wh');
		$this->db->join('location l','l.location_id = wh.location_id');
		$this->db->where('wh_id',$ro_to_wh_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	// modified by maruthi on 7th december'17
	public function get_to_address2($return_order_id)
	{
		$this->db->select('oa.address1,oa.address2,oa.address3,oa.address4,to.order_number,
							u.name as sso_name,u.email,
						   oa.pin_code,l.name as to_location,oa.gst_number,oa.pan_number');
		$this->db->from('return_order ro');
		$this->db->join('order_address oa','ro.tool_order_id = oa.tool_order_id');
		$this->db->join('tool_order to','to.tool_order_id = oa.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('location l','l.location_id = oa.location_id','LEFT');
		if(sendMailsEvenFEDeactivated() == 1)
			$this->db->where('u.status',1);
		$this->db->where('ro.return_order_id',$return_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_requested_tools($order_status_id)
	{
		$this->db->select('count(t.tool_id) as quantity,t.part_number,t.part_description,
						   t.hsn_code,sum(a.cost_in_inr) as cost_in_inr,t.gst_percent, GROUP_CONCAT(CONCAT(p.serial_number," : ",a.asset_number) SEPARATOR ",") as asset_number_list');
		$this->db->from('order_asset_history oah');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('oah.order_status_id',$order_status_id);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_requested_assets($order_status_id)
	{
		$this->db->select('t.part_number,t.part_description,a.asset_number,
						   t.hsn_code,t.gst_percent');
		$this->db->from('order_asset_history oah');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('oah.order_status_id',$order_status_id);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('p.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function download_open_fe_returns($searchParams,$task_access)
	{
		$return_type = array(1,2);
		$this->db->select('to.order_number,ro.return_number,ro.expected_arrival_date,l.name as location_name,ro.return_order_id,l1.name as country,
			concat(wh.wh_code," -(",wh.name,")") as wh_name,
			concat(u.sso_id," -(",u.name,")") as sso_name,
			concat(ro.address1,", ",ro.address2,", ",ro.address3,", ",ro.address4,", ",ro.zip_code) as pickup_address');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('location l1','l1.location_id = ro.country_id');
		$this->db->join('warehouse wh','wh.wh_id = ro.ro_to_wh_id'); 
		$this->db->join('user u','u.sso_id = to.sso_id');
		if($searchParams['tool_order_number']!='')
			$this->db->like('to.order_number',$searchParams['tool_order_number']);
		if($searchParams['return_number']!='')
			$this->db->like('ro.return_number',$searchParams['return_number']);
		if($searchParams['ssoid']!='')
			$this->db->where('to.sso_id',$searchParams['ssoid']);
		if($searchParams['date']!='')
			$this->db->where('ro.expected_arrival_date',format_date($searchParams['date']));
		if($task_access == 1 )
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}
			else
			{
				if(isset($_SESSION['whsIndededArray']) && $searchParams['whc_id']=='')
				{
					$this->db->where_in('ro.ro_to_wh_id',$_SESSION['whsIndededArray']); 
				}
				else			
				{	
					$this->db->where('ro.ro_to_wh_id',$this->session->userdata('s_wh_id'));
				}	
			}
		}
		else if($task_access == 2)
		{
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}

			$this->db->where('ro.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('to.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('ro.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('ro.country_id',$_SESSION['countriesIndexedArray']);	
				}
				
			}
			if($searchParams['whc_id']!='')
			{
				$this->db->where('ro.ro_to_wh_id',$searchParams['whc_id']);
			}

		}
		$this->db->where('osh.current_stage_id',9);//At Transit from fe to wh
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_type_id',$return_type);
		$this->db->order_by('ro.return_number ASC','ro.created_time ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_assets_in_open_fe_returns($return_order_id)
	{
		$this->db->select('a.asset_number,a.asset_id,
			as.name as asset_status,t.part_number, t.part_description');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','ro.return_order_id = rto.return_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('order_asset_history oah','osh.order_status_id = oah.order_status_id');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('ro.return_order_id',$return_order_id);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();	
	}
}