<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calibration_report_m extends CI_Model
{
	public function calibration_total_num_rows($searchParams,$task_access)
	{
		if(@$searchParams['zone_id']!='')
		{
			$wh_arr = get_wh_by_zone_tools_inventory(@$searchParams['zone_id'],$task_access);
			$wh_list = get_index_array_result($wh_arr,'wh_id');
		}
		$this->db->select('a.asset_id,DATEDIFF(a.cal_due_date,now())as days,l.name as country_name,t.part_number,t.part_description,a.asset_number,m.name as modality_name,a.cal_due_date,cs.name as current_stage,rca.current_stage_id,a.status as asset_main_status');
		$this->db->from('rc_asset rca');
		$this->db->join('rc_order rco','rco.rc_order_id = rca.rc_order_id','LEFT');
		$this->db->join('current_stage cs','cs.current_stage_id = rca.current_stage_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('warehouse wh','wh.wh_id = rco.wh_id','LEFT');
		$this->db->join('warehouse wh1','wh1.wh_id = a.wh_id','LEFT');
		$this->db->join('modality m','m.modality_id = a.modality_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['warehouse']!='')
			$this->db->where('wh1.wh_id',$searchParams['warehouse']);
		if($searchParams['cr_number']!='')
			$this->db->where('rca.rc_number',$searchParams['cr_number']);
		if($searchParams['cr_raised_date']!='')
			$this->db->where('date(rca.created_time)',$searchParams['cr_raised_date']);
		if($searchParams['zone_id']!='')
		{
			$this->db->where_in('wh1.wh_id',$wh_list);
		}
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh1.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		if($searchParams['calibration_status']!='')
		{
			if($searchParams['calibration_status']==1)
			{
				$this->db->where('rca.current_stage_id >=',12);
				$this->db->where('rca.current_stage_id <=',16);
			}
			else if($searchParams['calibration_status']==2)
			{
				$this->db->where('rca.current_stage_id',17);
			}
			else if($searchParams['calibration_status'] == 3)
			{
				$this->db->where('rca.current_stage_id',18);
			}
			else if($searchParams['calibration_status'] == 4)
			{
				$this->db->where('rca.current_stage_id',19);
			}
			else if($searchParams['calibration_status'] == 5)
			{
				$this->db->where('rca.current_stage_id',27);
			}
			else if($searchParams['calibration_status'] == 6)
			{
				$this->db->where('a.status',12);
			}
			else if($searchParams['calibration_status'] == 7)
			{
				$this->db->where('rca.current_stage_id',28);
			}
		}
		if($searchParams['cr_status']!='')
		{
			$this->db->where('rca.current_stage_id',$searchParams['cr_status']);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rca_type',2);
		$this->db->order_by('rca.rc_asset_id DESC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{
		if(@$searchParams['zone_id']!='')
		{
			$wh_arr = get_wh_by_zone_tools_inventory(@$searchParams['zone_id'],$task_access);
			$wh_list = get_index_array_result($wh_arr,'wh_id');
		}
		$this->db->select('a.asset_id,DATEDIFF(a.cal_due_date,now())as days, l.name as country_name, t.part_number, t.part_description, a.asset_number,m.modality_code as modality_name, a.cal_due_date, cs.name as current_stage, rca.rc_number, rca.current_stage_id, a.status as asset_main_status, a.approval_status, p.serial_number');
		$this->db->from('rc_asset rca');
		$this->db->join('rc_order rco','rco.rc_order_id = rca.rc_order_id','LEFT');
		$this->db->join('current_stage cs','cs.current_stage_id = rca.current_stage_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('warehouse wh','wh.wh_id = rco.wh_id','LEFT');
		$this->db->join('warehouse wh1','wh1.wh_id = a.wh_id','LEFT');
		$this->db->join('modality m','m.modality_id = a.modality_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['warehouse']!='')
			$this->db->where('wh1.wh_id',$searchParams['warehouse']);
		if($searchParams['cr_number']!='')
			$this->db->where('rca.rc_number',$searchParams['cr_number']);
		if($searchParams['cr_raised_date']!='')
			$this->db->where('date(rca.created_time)',$searchParams['cr_raised_date']);
		if($searchParams['zone_id']!='')
			$this->db->where_in('wh1.wh_id',$wh_list);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh1.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		if($searchParams['calibration_status']!='')
		{
			if($searchParams['calibration_status']==1)
			{
				$this->db->where('rca.current_stage_id >=',12);
				$this->db->where('rca.current_stage_id <=',16);
			}
			else if($searchParams['calibration_status']==2)
			{
				$this->db->where('rca.current_stage_id',17);
			}
			else if($searchParams['calibration_status'] == 3)
			{
				$this->db->where('rca.current_stage_id',18);
			}
			else if($searchParams['calibration_status'] == 4)
			{
				$this->db->where('rca.current_stage_id',19);
			}
			else if($searchParams['calibration_status'] == 5)
			{
				$this->db->where('rca.current_stage_id',27);
			}
			else if($searchParams['calibration_status'] == 6)
			{
				$this->db->where('a.status',12);
			}
			else if($searchParams['calibration_status'] == 7)
			{
				$this->db->where('rca.current_stage_id',28);
			}
		}
		if($searchParams['cr_status']!='')
		{
			$this->db->where('rca.current_stage_id',$searchParams['cr_status']);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rca_type',2);
		$this->db->order_by('rca.rc_asset_id DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function calibration_report_downloads($searchParams,$task_access)
	{
		if(@$searchParams['zone_id']!='')
		{
			$wh_arr = get_wh_by_zone_tools_inventory(@$searchParams['zone_id'],$task_access);
			$wh_list = get_index_array_result($wh_arr,'wh_id');
		}
		$this->db->select('a.asset_id,DATEDIFF(a.cal_due_date,now())as days,l.name as country_name,t.part_number,t.part_description,a.asset_number,m.name as modality_name,a.cal_due_date,cs.name as current_stage,p.serial_number, rca.rc_number,rca.current_stage_id,a.status as asset_main_status');
		$this->db->from('rc_asset rca');
		$this->db->join('rc_order rco','rco.rc_order_id = rca.rc_order_id','LEFT');
		$this->db->join('current_stage cs','cs.current_stage_id = rca.current_stage_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('warehouse wh','wh.wh_id = rco.wh_id','LEFT');
		$this->db->join('warehouse wh1','wh1.wh_id = a.wh_id','LEFT');
		$this->db->join('modality m','m.modality_id = a.modality_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['warehouse']!='')
			$this->db->where('wh1.wh_id',$searchParams['warehouse']);
		if($searchParams['cr_number']!='')
			$this->db->where('rca.rc_number',$searchParams['cr_number']);
		if($searchParams['cr_raised_date']!='')
			$this->db->where('date(rca.created_time)',$searchParams['cr_raised_date']);
		if($searchParams['zone_id']!='')
		{
			$this->db->where_in('wh1.wh_id',$wh_list);
		}
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh1.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		
		if($searchParams['calibration_status']!='')
		{
			if($searchParams['calibration_status']==1)
			{
				$this->db->where('rca.current_stage_id >=',12);
				$this->db->where('rca.current_stage_id <=',16);
			}
			else if($searchParams['calibration_status']==2)
			{
				$this->db->where('rca.current_stage_id',17);
			}
			else if($searchParams['calibration_status'] == 3)
			{
				$this->db->where('rca.current_stage_id',18);
			}
			else if($searchParams['calibration_status'] == 4)
			{
				$this->db->where('rca.current_stage_id',19);
			}
			else if($searchParams['calibration_status'] == 5)
			{
				$this->db->where('rca.current_stage_id',27);
			}
			else if($searchParams['calibration_status'] == 6)
			{
				$this->db->where('a.status',12);
			}
			else if($searchParams['calibration_status'] == 7)
			{
				$this->db->where('rca.current_stage_id',28);
			}
		}
		if($searchParams['cr_status']!='')
		{
			$this->db->where('rca.current_stage_id',$searchParams['cr_status']);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('rca.rca_type',2);
		$this->db->order_by('rca.rc_asset_id DESC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_wh_list($task_access)
    {
    	$this->db->select('w.wh_id,CONCAT(w.wh_code," -(",w.name,")") as wh_name');
    	$this->db->from('warehouse w');
    	if($task_access == 1)
    	{
    		$this->db->where('w.country_id',$_SESSION['s_country_id']);
			$this->db->where('w.status',1);
    	}
    	if($task_access == 2)
    	{
    		$this->db->where('w.country_id',$this->session->userdata('s_country_id'));
    		$status = array(1,3);
    		$this->db->where_in('status',$status);
    	}
    	else if($task_access == 3)
    	{
    		if($this->session->userdata('header_country_id')!='')
    		{
    			$this->db->where('w.country_id',$this->session->userdata('header_country_id'));
    		}
    		else
    		{
    			$this->db->where_in('w.country_id',$this->session->userdata('countriesIndexedArray'));
    		}
    		$status = array(1,3);
    		$this->db->where_in('w.status',$status);
    	}
    	$this->db->order_by('w.country_id ASC');
    	$res = $this->db->get();
    	return $res->result_array();
    }

    /*koushik*/
	public function get_zone_by_country($task_access,$country_id = 0)
	{
		$this->db->select('concat(l.name, "," ,l1.name) as name, l.location_id');
		$this->db->from('location l');//zone
		$this->db->join('location l1','l1.location_id = l.parent_id');//country
		$this->db->where('l.level_id',3);
		if($country_id != '' || $country_id != 0)
		{
			$this->db->where('l1.location_id',$country_id);
		}
		else if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l1.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l1.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				$this->db->where_in('l1.location_id',$this->session->userdata('countriesIndexedArray'));
			}
		}

		$res = $this->db->get();
		return $res->result_array();
	}

	public function egm_calibration_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('ec.ec_id');
		$this->db->from('egm_calibration ec');
		$this->db->join('egm_cal_status ecs','ecs.cal_status_id = ec.cal_status_id');
		$this->db->join('asset a','a.asset_id = ec.asset_id');
		$this->db->join('warehouse wh1','wh1.wh_id = a.wh_id','LEFT');
		$this->db->join('modality m','m.modality_id = a.modality_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('location l','l.location_id = a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['warehouse']!='')
			$this->db->where('wh1.wh_id',$searchParams['warehouse']);
		if($searchParams['cr_number']!='')
			$this->db->where('ec.cr_number',$searchParams['cr_number']);
		if($searchParams['cr_raised_date']!='')
			$this->db->where('date(ec.created_time)',$searchParams['cr_raised_date']);
		if($searchParams['cr_status']!='')
			$this->db->where('ec.cal_status_id',$searchParams['cr_status']);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh1.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('ec.ec_id DESC');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function egm_calibration_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('a.asset_id,DATEDIFF(a.cal_due_date,now())as days, l.name as country_name, t.part_number, t.part_description, a.asset_number,m.modality_code as modality_name, a.cal_due_date, ec.cr_number, ecs.name as cal_status_name, ec.cal_status_id, as.name as asset_main_status, a.approval_status, p.serial_number');
		$this->db->from('egm_calibration ec');
		$this->db->join('egm_cal_status ecs','ecs.cal_status_id = ec.cal_status_id');
		$this->db->join('asset a','a.asset_id = ec.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('warehouse wh1','wh1.wh_id = a.wh_id','LEFT');
		$this->db->join('modality m','m.modality_id = a.modality_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('location l','l.location_id = a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['warehouse']!='')
			$this->db->where('wh1.wh_id',$searchParams['warehouse']);
		if($searchParams['cr_number']!='')
			$this->db->where('ec.cr_number',$searchParams['cr_number']);
		if($searchParams['cr_raised_date']!='')
			$this->db->where('date(ec.created_time)',$searchParams['cr_raised_date']);
		if($searchParams['cr_status']!='')
			$this->db->where('ec.cal_status_id',$searchParams['cr_status']);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh1.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('ec.ec_id DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function egm_calibration_report_downloads($searchParams,$task_access)
	{
		$this->db->select('a.asset_id,DATEDIFF(a.cal_due_date,now())as days, l.name as country_name, t.part_number, t.part_description, a.asset_number,m.modality_code as modality_name, a.cal_due_date, ec.cr_number, ecs.name as cal_status_name, ec.cal_status_id, as.name as asset_main_status, a.approval_status, p.serial_number');
		$this->db->from('egm_calibration ec');
		$this->db->join('egm_cal_status ecs','ecs.cal_status_id = ec.cal_status_id');
		$this->db->join('asset a','a.asset_id = ec.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->join('warehouse wh1','wh1.wh_id = a.wh_id','LEFT');
		$this->db->join('modality m','m.modality_id = a.modality_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('location l','l.location_id = a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['modality_id']!='')
			$this->db->where('t.modality_id',$searchParams['modality_id']);
		if($searchParams['warehouse']!='')
			$this->db->where('wh1.wh_id',$searchParams['warehouse']);
		if($searchParams['cr_number']!='')
			$this->db->where('ec.cr_number',$searchParams['cr_number']);
		if($searchParams['cr_raised_date']!='')
			$this->db->where('date(ec.created_time)',$searchParams['cr_raised_date']);
		if($searchParams['cr_status']!='')
			$this->db->where('ec.cal_status_id',$searchParams['cr_status']);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$this->db->where('wh1.status',1);
		}
		else if($task_access == 2)
		{
			$this->db->where('a.country_id',$_SESSION['s_country_id']);
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		else if(@$task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			$status = array(1,3);
			$this->db->where_in('wh1.status',$status);
		}
		$this->db->where('p.part_level_id',1);
		$this->db->order_by('ec.ec_id DESC');
		$res = $this->db->get();
		return $res->result_array();
	}
}