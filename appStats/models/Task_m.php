<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Task_m extends CI_Model 
{
	//starting of task crud methods//
	public function task_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('t.*,p.name as page_name');
		$this->db->from('task t');
		$this->db->join('page p','p.page_id = t.page_id','left');
		if($searchParams['task_name']!='')
			$this->db->like('t.name',$searchParams['task_name']);
		if($searchParams['page_id']!='')
			$this->db->where('t.page_id',$searchParams['page_id']);
		$this->db->where('p.status',1);
		$this->db->order_by('t.page_id','ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();			
		return $res->result_array();
	}
	
	public function task_total_num_rows($searchParams)
	{
		$this->db->select('t.*,p.name as page_name');
		$this->db->from('task t');
		$this->db->join('page p','p.page_id = t.page_id','left');
		if($searchParams['task_name']!='')
			$this->db->like('t.name',$searchParams['task_name']);
		if($searchParams['page_id']!='')
			$this->db->where('t.page_id',$searchParams['page_id']);
		$this->db->where('p.status',1);
		$this->db->order_by('t.page_id','ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	//Uniqueness of task name //
    public function is_tasknameExist($name,$task_id,$page_id)
    {       
        $this->db->select();
        $this->db->from('task t');
        $this->db->where('t.name',$name);
        $this->db->where('t.page_id',$page_id);
        if($task_id!=0 || $task_id!='')
        {
        	$this->db->where_not_in('t.task_id',$task_id);
        }
        $res = $this->db->get();
        return $res->num_rows();
    }
}	