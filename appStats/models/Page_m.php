<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_m extends CI_Model 
{
	
	public function page_total_num_rows($searchParams)
	{
		
		$this->db->select();
		$this->db->from('page');
		if($searchParams['page_name']!='')
			$this->db->like('name',$searchParams['page_name']);
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function page_results($current_offset, $per_page, $searchParams)
	{
		
		$this->db->select();
		$this->db->from('page');
		if($searchParams['page_name']!='')
			$this->db->like('name',$searchParams['page_name']);
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
    public function check_page_name_availability($data)
	{
		$this->db->from('page');
		$this->db->where('name', $data['name']);
		if($data['page_id']!=0 || $data['page_id']!='')
		{
			$this->db->where_not_in('page_id', $data['page_id']);
		}
		$res = $this->db->get();
		return $res->num_rows();	
	}
}