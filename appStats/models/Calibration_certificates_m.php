<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calibration_certificates_m extends CI_Model 
{
	public function calibration_certificates_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('a.asset_id,l.name as country_name,a.asset_number,t.part_number,t.part_description,w.name as warehouse,w.wh_code,a.wh_id');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
			if($task_access == 2)
			{
				if($searchParams['whid']!='')
				{
					$this->db->where('a.wh_id',$searchParams['whid']);
				}
			}
		}
		else if(@$task_access == 3)
		{

			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('t.cal_type_id',1);
		$this->db->order_by('t.tool_id ASC,a.asset_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function calibration_certificates_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('a.asset_id, l.name as country_name, a.asset_number,t.part_number, t.part_description, w.name as warehouse, w.wh_code, a.wh_id, p.serial_number');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['tool_no']!='')
			$this->db->like('t.part_number',$searchParams['tool_no']);
		if($searchParams['tool_desc']!='')
			$this->db->like('t.part_description',$searchParams['tool_desc']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
			if($task_access == 2)
			{
				if($searchParams['whid']!='')
				{
					$this->db->where('a.wh_id',$searchParams['whid']);
				}
			}
		}
		else if(@$task_access == 3)
		{

			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->where('t.cal_type_id',1);
		$this->db->order_by('t.tool_id ASC,a.asset_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_asset_data($asset_id)
	{
		$this->db->select('a.asset_id, a.asset_number, t.part_number, t.part_description, w.name as warehouse, w.wh_code, a.wh_id, p.serial_number');
		$this->db->from('asset a');
		$this->db->join('part p','p.asset_id=a.asset_id');
		$this->db->join('tool t','t.tool_id=p.tool_id');
		$this->db->join('warehouse w','w.wh_id=a.wh_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('t.cal_type_id',1);
		$this->db->where('a.asset_id',$asset_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	public function get_asset_documents($asset_id)
	{
		$this->db->select('ad.*,dt.name as document_type');
		$this->db->from('asset a');
		$this->db->join('asset_document ad','ad.asset_id=a.asset_id');
		$this->db->join('document_type dt','dt.document_type_id=ad.document_type_id');
		$this->db->where('a.asset_id',$asset_id);
		$this->db->where('dt.document_type_id',1);
		$res=$this->db->get();
		return $res->result_array();
	}
	public function get_asset_rc_documents($asset_id)
	{
		$this->db->select('rcd.*,dt.name as document_type');
		$this->db->from('rc_doc rcd');
		$this->db->join('rc_order rco','rco.rc_order_id = rcd.rc_order_id');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('document_type dt','dt.document_type_id = rcd.document_type_id');
		$this->db->where('rca.asset_id',$asset_id);
		$this->db->where('dt.document_type_id',2);
		$res=$this->db->get();
		return $res->result_array();
	}
	public function get_asset_rca_documents($asset_id)
	{
		$this->db->select('rcd.*,dt.name as document_type');
		$this->db->from('rca_doc rcd');
		$this->db->join('rc_asset rca','rca.rc_asset_id = rcd.rc_asset_id');
		$this->db->join('document_type dt','dt.document_type_id = rcd.document_type_id');
		$this->db->where('rca.asset_id',$asset_id);
		$this->db->where('dt.document_type_id',2);
		$res=$this->db->get();
		return $res->result_array();
	}
}