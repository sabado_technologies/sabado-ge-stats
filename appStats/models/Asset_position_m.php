<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asset_position_m extends CI_Model {

	public function asset_position_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('ap.*,w.name as wh_name,a.asset_number,u.sso_id as user_sso,as.name as asset_status,l.name as countryName');
		$this->db->from('asset_position ap');		
		$this->db->join('warehouse w','w.wh_id = ap.wh_id','LEFT');
		$this->db->join('asset a','a.asset_id = ap.asset_id','LEFT');
		$this->db->join('user u','u.sso_id = ap.sso_id','LEFT');
		$this->db->join('location l','l.location_id = a.country_id','LEFT');
		$this->db->join('asset_status as','as.asset_status_id = a.status','LEFT');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['a_warehouse_id']!='')
			$this->db->where('ap.wh_id',$searchParams['a_warehouse_id']);
		if($searchParams['status_id']!='')
			$this->db->where('a.status',$searchParams['status_id']);
		if($searchParams['from_date']!='')
			$this->db->where('ap.from_date',format_date($searchParams['from_date']));
		if($searchParams['to_date']!='')
			$this->db->where('ap.to_date',format_date($searchParams['to_date']));
		if($searchParams['fe_id']!='')
			$this->db->where('ap.sso_id',$searchParams['fe_id']);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access ==1)
				$this->db->where('ap.sso_id',$this->session->userdata('sso_id'));
			else
				$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('a.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['a_country_id']!='')
				{
					$this->db->where('a.country_id',$searchParams['a_country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		
		$this->db->order_by('ap.asset_position_id','DESC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function asset_position_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('ap.*,w.name as wh_name,a.asset_number,u.sso_id as user_sso,as.name as asset_status');
		$this->db->from('asset_position ap');		
		$this->db->join('warehouse w','w.wh_id = ap.wh_id','LEFT');
		$this->db->join('asset a','a.asset_id = ap.asset_id','LEFT');
		$this->db->join('user u','u.sso_id = ap.sso_id','LEFT');
		$this->db->join('location l','l.location_id = a.country_id','LEFT');
		$this->db->join('asset_status as','as.asset_status_id = a.status','LEFT');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['a_warehouse_id']!='')
			$this->db->where('ap.wh_id',$searchParams['a_warehouse_id']);
		if($searchParams['status_id']!='')
			$this->db->where('a.status',$searchParams['status_id']);
		if($searchParams['from_date']!='')
			$this->db->where('ap.from_date',format_date($searchParams['from_date']));
		if($searchParams['to_date']!='')
			$this->db->where('ap.to_date',format_date($searchParams['to_date']));
		if($searchParams['fe_id']!='')
			$this->db->where('ap.sso_id',$searchParams['fe_id']);
		if($task_access == 1 || $task_access == 2)
		{
			if($task_access ==1)
				$this->db->where('ap.sso_id',$this->session->userdata('sso_id'));
			else
				$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('a.country_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				if($searchParams['a_country_id']!='')
				{
					$this->db->where('a.country_id',$searchParams['a_country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$this->session->userdata('countriesIndexedArray'));	
				}
			}
		}
		
		$this->db->order_by('ap.asset_position_id','DESC');
		$res = $this->db->get();
		return $res->num_rows();
	}


}