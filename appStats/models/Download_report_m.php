<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download_report_m extends CI_Model 
{
	/*koushik*/
	public function get_state_by_country($task_access,$country_id = 0)
	{
		$this->db->select('concat(l.name, "," ,l1.name) as name, l.location_id');
		$this->db->from('location l');//state
		$this->db->join('location l1','l1.location_id = l.parent_id');//zone
		$this->db->join('location l2','l2.location_id = l1.parent_id');//country
		$this->db->where('l.level_id',4);
		if($country_id != '' || $country_id != 0)
		{
			$this->db->where('l2.location_id',$country_id);
		}
		else if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('l2.location_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($this->session->userdata('header_country_id')!='')
			{
				$this->db->where('l2.location_id',$this->session->userdata('header_country_id'));	
			}
			else
			{
				$this->db->where_in('l2.location_id',$this->session->userdata('countriesIndexedArray'));	
			}
		}
		$this->db->order_by('l2.location_id ASC');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_state_by_country_dropdown($country_id)
	{
		$this->db->select('concat(l.name, "," ,l1.name) as name, l.location_id');
		$this->db->from('location l');//state
		$this->db->join('location l1','l1.location_id = l.parent_id');//zone
		$this->db->join('location l2','l2.location_id = l1.parent_id');//country
		$this->db->where('l.level_id',4);
		$this->db->where('l2.location_id',$country_id);
		$this->db->where('l.status',1);
		$this->db->order_by('l2.location_id ASC');
		$res1 = $this->db->get();
		$res = $res1->result_array();
		$count = $res1->num_rows();
		$qry_data='';
        if($count>0)
		{
			$qry_data.='<option value="">- State -</option>';
			foreach($res as $row1)
			{  
				$qry_data.='<option value="'.$row1['location_id'].'">'.$row1['name'].'</option>';
			}
		} 
		else 
		{
			$qry_data.='<option value="">- No Data Found -</option>';
		}
		return $qry_data;
	}

	public function get_wh_by_state($country_id,$state_id=0)
	{
		if($country_id == '')
		{
			$wh_arr = get_wh_by_state($state_id);
		}
		
		$this->db->select('w.wh_id,CONCAT(w.wh_code," -(",w.name,")") as wh_name');
	    $this->db->from('warehouse w');
	    if($country_id !='')
	    {
	    	$this->db->where('w.country_id',$country_id);
	    }
	    else
	    {
	    	$this->db->where_in('w.wh_id',$wh_arr);
	    }
		$this->db->where('w.status',1);
        $res1 = $this->db->get();
		$res = $res1->result_array();
		$count = $res1->num_rows();
		$qry_data='';
        if($count>0)
		{
			$qry_data.='<option value="">- Warehouse -</option>';
			foreach($res as $row1)
			{  
				$qry_data.='<option value="'.$row1['wh_id'].'">'.$row1['wh_name'].'</option>';
			}
		} 
		else 
		{
			$qry_data.='<option value="">No Data Found</option>';
		}
		return $qry_data;
	}

	public function get_od($searchParams)
	{
		if($searchParams['state_id'] !='')
		{
			$wh_arr = get_wh_by_state($searchParams['state_id']);
		}
		$this->db->select('to.deploy_date as request_date_1,
						   pf.created_time as ship_date_1,
						   pf.format_number,
						   pf.created_time,
						   pf.print_type,
						   to.order_type,
						   u.sso_id,
						   u.name as sso_name,
						   concat(u.name, " & " ,u.sso_id) as fe_name,
						   u.mobile_no as fe_contact_number,
						   concat(u1.name, " & " ,u1.sso_id) as fe_name1,
						   u1.mobile_no as fe_contact_number1,
						   concat(to_wh.wh_code, "-(" ,to_wh.name,")") as to_wh_name,
						   to_wh.phone as to_wh_contact_number,
						   concat(from_wh.wh_code, "-(" ,from_wh.name,")") as from_wh_name,
						   to.fe_check,
						   to.order_number,
						   to.stn_number,
						   to.siebel_number,
						   od.tool_order_id,
						   od.courier_type,
						   od.courier_number,
						   od.billed_to,
						   od.remarks as airway_bill,
						   od.courier_name,
						   od.contact_person,
						   od.phone_number,
						   od.vehicle_number,
						   oa.address1 as to_address1,
						   oa.address2 as to_address2,
						   oa.address3 as to_address3,
						   oa.address4 as to_address4,
						   oa.pin_code as to_pin_code,
						   l.name as to_location,
						   oa.gst_number as to_gst_number,
						   oa.pan_number as to_pan_number,
						   wh1.address1 as from_address1,
						   wh1.address2 as from_address2,
						   wh1.address3 as from_address3,
						   wh1.address4 as from_address4,
						   wh1.pin_code as from_pin_code,
						   l2.name as from_location,
						   wh1.gst_number as from_gst_number,
						   wh1.pan_number as from_pan_number,
						   wh1.tin_number as from_tin_number,
						   wh2.address1 as bill_address1,
						   wh2.address2 as bill_address2,
						   wh2.address3 as bill_address3,
						   wh2.address4 as bill_address4,
						   wh2.pin_code as bill_pin_code,
						   l4.name as bill_location,
						   wh2.gst_number as bill_gst_number,
						   wh2.pan_number as bill_pan_number,
						   cur.name as currency_name,
						   cou.name as country_name');

		if($searchParams['cancel_type']==1)
		{
			$this->db->select('ph.modified_time');
			$this->db->from('print_format pf');
			$this->db->join('order_delivery od','pf.print_id = od.print_id');
			$this->db->join('print_history ph','ph.order_delivery_id = od.order_delivery_id AND pf.print_id = ph.print_id');
		}
		if($searchParams['cancel_type'] == 2 || $searchParams['cancel_type']==3)
		{
			$this->db->select('ph.modified_time');
			$this->db->from('print_history ph');
			$this->db->join('order_delivery od','od.order_delivery_id = ph.order_delivery_id');
			$this->db->join('print_format pf','pf.print_id = ph.print_id');
		}
		$this->db->join('tool_order to','to.tool_order_id = od.tool_order_id');
		$this->db->join('currency cur','cur.location_id = to.country_id');
		$this->db->join('location cou','cou.location_id = to.country_id');
		$this->db->join('user u','u.sso_id = to.sso_id','LEFT');
		$this->db->join('st_tool_order sto','sto.stock_transfer_id = to.tool_order_id','LEFT');
		$this->db->join('tool_order too','too.tool_order_id = sto.tool_order_id','LEFT');
		$this->db->join('user u1','u1.sso_id = too.sso_id','LEFT');
		$this->db->join('warehouse from_wh','from_wh.wh_id = to.wh_id');
		$this->db->join('warehouse to_wh','to_wh.wh_id = to.to_wh_id','LEFT');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		//$this->db->join('customer_site cs','cs.site_id = oa.site_id','LEFT');
		//$this->db->join('customer c','c.customer_id = cs.customer_id','LEFT');
		$this->db->join('location l','l.location_id = oa.location_id','LEFT');
		$this->db->join('warehouse wh1','wh1.wh_id = to.wh_id');
		$this->db->join('location l1','l1.location_id = wh1.location_id','LEFT');
		$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');
		$this->db->join('warehouse wh2','wh2.wh_id = od.billed_to');
		$this->db->join('location l3','l3.location_id = wh2.location_id','LEFT');
		$this->db->join('location l4','l4.location_id = l3.parent_id','LEFT');
		if($searchParams['type'] !='')
		{
			$this->db->where('pf.print_type',$searchParams['type']);
		}
		if($searchParams['country_id'] !='')
		{
			$this->db->where('to.country_id',$searchParams['country_id']);
		}
		if($searchParams['state_id'] !='')
		{
			$this->db->where_in('pf.wh_id',$wh_arr);
		}
		if($searchParams['wh_id'] !='')
		{
			$this->db->where('pf.wh_id',$searchParams['wh_id']);
		}
		if($searchParams['from_date'] !='')
		{
			$this->db->where('pf.created_time >=',$searchParams['from_date']);
		}
		if($searchParams['to_date'] !='')
		{
			$this->db->where('pf.created_time <=',$searchParams['to_date']);
		}
		if($searchParams['print_number'] !='')
		{
			$this->db->where('pf.format_number',$searchParams['print_number']);
		}
		if($searchParams['cancel_type']==3)
		{
			$this->db->where('ph.modified_time !=',NULL);
		}
		$this->db->where('oa.status',1);
		$this->db->order_by('pf.print_id ASC');
		$this->db->group_by('pf.print_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_od_tools($tool_order_id)
	{
		$this->db->select('count(ot.tool_id) as quantity,
					       t.part_number,
					       t.part_description,
					       t.hsn_code,
					       t.gst_percent,
					       sum(a.cost_in_inr) as cost_in_inr,
					       GROUP_CONCAT(CONCAT(p.serial_number," : ",a.asset_number) SEPARATOR ",") as asset_number_list');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','oa.asset_id = a.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('ot.tool_order_id',$tool_order_id);
		$this->db->where('ot.status',1);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_rc($searchParams)
	{
		if($searchParams['state_id'] !='')
		{
			$wh_arr = get_wh_by_state($searchParams['state_id']);
		}
		$this->db->select('rco.created_time as request_date_1,
		                   pf.created_time as ship_date_1,
						   pf.format_number,
						   pf.created_time,
						   pf.print_type,
						   rco.rc_type,
						   rco.rc_order_id,
						   concat(s.supplier_code, "-(" ,s.name,")") as supplier_name,
						   s.contact_number as supplier_contact_number,
						   GROUP_CONCAT(rca.rc_number SEPARATOR",") as crn_number,
						   rcs.courier_type,
						   rcs.courier_number,
						   rcs.remarks as airway_bill,
						   rcs.courier_name,
						   rcs.contact_person,
						   rcs.phone_number,
						   rcs.vehicle_number,
						   rco.address1 as to_address1,
						   rco.address2 as to_address2,
						   rco.address3 as to_address3,
						   rco.address4 as to_address4,
						   rco.pin_code as to_pin_code,
						   s.address3 as to_location,
						   rco.gst_number as to_gst_number,
						   rco.pan_number as to_pan_number,
						   concat(wh.wh_code, "-(" ,wh.name,")") as from_wh_name,
						   wh.address1 as from_address1,
						   wh.address2 as from_address2,
						   wh.address3 as from_address3,
						   wh.address4 as from_address4,
						   wh.pin_code as from_pin_code,
						   l2.name as from_location,
						   wh.gst_number as from_gst_number,
						   wh.pan_number as from_pan_number,

						   wh2.address1 as bill_address1,
						   wh2.address2 as bill_address2,
						   wh2.address3 as bill_address3,
						   wh2.address4 as bill_address4,
						   wh2.pin_code as bill_pin_code,
						   l4.name as bill_location,
						   wh2.gst_number as bill_gst_number,
						   wh2.pan_number as bill_pan_number,
						   cur.name as currency_name,
						   cou.name as country_name');

		if($searchParams['cancel_type']==1)
		{
			$this->db->select('ph.modified_time');
			$this->db->from('print_format pf');
			$this->db->join('rc_shipment rcs','rcs.print_id = pf.print_id');
			$this->db->join('print_history ph','ph.rc_shipment_id = rcs.rc_shipment_id AND pf.print_id = ph.print_id');
		}
		if($searchParams['cancel_type'] == 2 || $searchParams['cancel_type']==3)
		{
			$this->db->select('ph.modified_time');
			$this->db->from('print_history ph');
			$this->db->join('rc_shipment rcs','rcs.rc_shipment_id = ph.rc_shipment_id');
			$this->db->join('print_format pf','pf.print_id = ph.print_id');
		}
		$this->db->join('rc_order rco','rcs.rc_order_id = rco.rc_order_id');
		$this->db->join('currency cur','cur.location_id = rco.country_id');
		$this->db->join('location cou','cou.location_id = rco.country_id');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		//$this->db->join('asset a','a.asset_id = rca.asset_id','LEFT');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id');
		$this->db->join('warehouse from_wh','from_wh.wh_id = rco.wh_id');
		$this->db->join('warehouse wh','wh.wh_id = rco.wh_id');
		$this->db->join('location l1','l1.location_id = wh.location_id','LEFT');
		$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');
		$this->db->join('warehouse wh2','wh2.wh_id = rcs.billed_to');
		$this->db->join('location l3','l3.location_id = wh2.location_id','LEFT');
		$this->db->join('location l4','l4.location_id = l3.parent_id','LEFT');

		if($searchParams['type'] !='')
		{
			$this->db->where('pf.print_type',$searchParams['type']);
		}
		if($searchParams['country_id'] !='')
		{
			$this->db->where('rco.country_id',$searchParams['country_id']);
		}
		if($searchParams['state_id'] !='')
		{
			$this->db->where_in('pf.wh_id',$wh_arr);
		}
		if($searchParams['wh_id'] !='')
		{
			$this->db->where('pf.wh_id',$searchParams['wh_id']);
		}
		if($searchParams['from_date'] !='')
		{
			$this->db->where('pf.created_time >=',$searchParams['from_date']);
		}
		if($searchParams['to_date'] !='')
		{
			$this->db->where('pf.created_time <=',$searchParams['to_date']);
		}
		if($searchParams['print_number'] !='')
		{
			$this->db->where('pf.format_number',$searchParams['print_number']);
		}
		if($searchParams['cancel_type']==3)
		{
			$this->db->where('ph.modified_time !=',NULL);
		}
		$this->db->order_by('pf.print_id ASC');
		$this->db->group_by('pf.print_id');
		$this->db->group_by('rco.rc_order_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_rc_tools($rc_order_id)
	{
		$this->db->select('t.part_number,
						   t.part_description,
						   t.hsn_code,count(t.tool_id) as quantity,
						   sum(a.cost_in_inr) as cost_in_inr,
						   t.gst_percent,
						   GROUP_CONCAT(CONCAT(p.serial_number," : ",a.asset_number) SEPARATOR ",") as asset_number_list');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('asset a','a.asset_id = rca.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('p.part_level_id',1);
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_return_order_list($searchParams)
	{
		if($searchParams['state_id'] !='')
		{
			$wh_arr = get_wh_by_state($searchParams['state_id']);
		}
		$this->db->select('pf.created_time as ship_date_1,
						   ro.created_time as request_date_1,
						   pf.format_number,
						   pf.created_time,
						   pf.print_type,
						   u.sso_id,
						   u.name as sso_name,
						   concat(u.name, " & " ,u.sso_id) as fe_name,
						   u.mobile_no as fe_contact_number,
						   concat(from_wh.wh_code, "-(" ,from_wh.name,")") as wh_name,
						   rto.tool_order_id,
						   ro.return_number,
						   ro.return_order_id,
						   ro.courier_type,
						   ro.courier_number,
						   ro.return_type_id,
						   ro.billed_to,
						   ro.remarks as airway_bill,
						   ro.courier_name,
						   ro.contact_person,
						   ro.phone_number,
						   ro.vehicle_number,
						   ro.ro_to_wh_id,
						   ro.address1 as from_address1,
						   ro.address2 as from_address2,
						   ro.address3 as from_address3,
						   ro.address4 as from_address4,
						   ro.zip_code as from_pin_code,
						   ro.gst_number as from_gst_number,
						   ro.pan_number as from_pan_number,
						   wh2.address1 as bill_address1,
						   wh2.address2 as bill_address2,
						   wh2.address3 as bill_address3,
						   wh2.address4 as bill_address4,
						   wh2.pin_code as bill_pin_code,
						   l4.name as bill_location,
						   wh2.gst_number as bill_gst_number,
						   wh2.pan_number as bill_pan_number,
						   cur.name as currency_name,
						   cou.name as country_name');

		if($searchParams['cancel_type']==1)
		{
			$this->db->select('ph.modified_time');
			$this->db->from('print_format pf');
			$this->db->join('return_order ro','pf.print_id = ro.print_id');
			$this->db->join('print_history ph','ph.return_order_id = ro.return_order_id AND pf.print_id = ph.print_id');
		}
		if($searchParams['cancel_type'] == 2 || $searchParams['cancel_type']==3)
		{
			$this->db->select('ph.modified_time');
			$this->db->from('print_history ph');
			$this->db->join('return_order ro','ro.return_order_id = ph.return_order_id');
			$this->db->join('print_format pf','pf.print_id = ph.print_id');
		}
		//$this->db->join('customer_site cs','cs.site_id = ro.site_id','LEFT');
		//$this->db->join('customer c','c.customer_id = cs.customer_id','LEFT');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id','LEFT');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id','LEFT');
		$this->db->join('warehouse from_wh','from_wh.wh_id = ro.wh_id');
		$this->db->join('currency cur','cur.location_id = from_wh.country_id');
		$this->db->join('location cou','cou.location_id = from_wh.country_id');
		$this->db->join('warehouse wh2','wh2.wh_id = ro.billed_to');
		$this->db->join('location l3','l3.location_id = wh2.location_id','LEFT');
		$this->db->join('location l4','l4.location_id = l3.parent_id','LEFT');
		if($searchParams['type'] !='')
		{
			$this->db->where('pf.print_type',$searchParams['type']);
		}
		if($searchParams['country_id'] !='')
		{
			$this->db->where('from_wh.country_id',$searchParams['country_id']);
		}
		if($searchParams['state_id'] !='')
		{
			$this->db->where_in('pf.wh_id',$wh_arr);
		}
		if($searchParams['wh_id'] !='')
		{
			$this->db->where('pf.wh_id',$searchParams['wh_id']);
		}
		if($searchParams['from_date'] !='')
		{
			$this->db->where('pf.created_time >=',$searchParams['from_date']);
		}
		if($searchParams['to_date'] !='')
		{
			$this->db->where('pf.created_time <=',$searchParams['to_date']);
		}
		if($searchParams['print_number'] !='')
		{
			$this->db->where('pf.format_number',$searchParams['print_number']);
		}
		if($searchParams['cancel_type']==3)
		{
			$this->db->where('ph.modified_time !=',NULL);
		}
		//$this->db->where('to.order_type',2);
		$this->db->order_by('pf.print_id ASC');
		$this->db->group_by('pf.print_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_to_address1($ro_to_wh_id)
	{
		$this->db->select('wh1.address1 as to_address1,
						   wh1.address2 as to_address2,
						   wh1.address3 as to_address3,
						   wh1.address4 as to_address4,
						   wh1.pin_code as to_pin_code,
						   wh1.gst_number as to_gst_number,
						   wh1.pan_number as to_pan_number');
		$this->db->from('warehouse wh1');
		$this->db->where('wh1.wh_id',$ro_to_wh_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_to_address2($return_order_id)
	{
		$this->db->select('oa.address1 as to_address1,
						   oa.address2 as to_address2,
						   oa.address3 as to_address3,
						   oa.address4 as to_address4,
						   oa.pin_code as to_pin_code,
						   oa.gst_number as to_gst_number,
						   oa.pan_number as to_pan_number');
		$this->db->from('return_order ro');
		$this->db->join('order_address oa','ro.tool_order_id = oa.tool_order_id');
		$this->db->where('ro.return_order_id',$return_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_requested_tools($order_status_id)
	{
		$this->db->select('count(t.tool_id) as quantity,
						   t.part_number,
						   t.part_description,
						   t.hsn_code,
						   sum(a.cost_in_inr) as cost_in_inr,
						   t.gst_percent,
						   GROUP_CONCAT(CONCAT(p.serial_number," : ",a.asset_number) SEPARATOR ",") as asset_number_list');
		$this->db->from('order_asset_history oah');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->where('oah.order_status_id',$order_status_id);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('t.tool_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_inward_od($searchParams)
	{
		if($searchParams['state_id'] !='')
		{
			$wh_arr = get_wh_by_state($searchParams['state_id']);
		}
		$this->db->select('pf.format_number,
						   pf.created_time as ship_date_1,
						   to.deploy_date as request_date_1,
						   pf.created_time,
						   pf.print_type,
						   to.order_type,
						   from_wh.wh_code as cust_code,
						   from_wh.name as cust_name,
						   concat(from_wh.wh_code, "-(" ,from_wh.name,")") as wh_name,
						   u.sso_id,
						   u.name as sso_name,
						   concat(u.sso_id, "-(" ,u.name,")") as fe_name,
						   u.mobile_no as fe_contact_number,
						   concat(to_wh.wh_code, "-(" ,to_wh.name,")") as to_wh_name,
						   to_wh.phone as to_wh_contact_number,
						   to.stn_number,
						   od.tool_order_id,
						   od.courier_type,
						   od.courier_number,
						   od.billed_to,
						   od.remarks as airway_bill,
						   od.vehicle_number,
						   od.courier_name,
						   od.contact_person,
						   od.phone_number,
						   oa.address1 as to_address1,
						   oa.address2 as to_address2,
						   oa.address3 as to_address3,
						   oa.address4 as to_address4,
						   oa.pin_code as to_pin_code,
						   l.name as to_location,
						   oa.gst_number as to_gst_number,
						   oa.pan_number as to_pan_number,
						   wh1.address1 as from_address1,
						   wh1.address2 as from_address2,
						   wh1.address3 as from_address3,
						   wh1.address4 as from_address4,
						   wh1.pin_code as from_pin_code,
						   l2.name as from_location,
						   wh1.gst_number as from_gst_number,
						   wh1.pan_number as from_pan_number,
						   wh2.address1 as bill_address1,
						   wh2.address2 as bill_address2,
						   wh2.address3 as bill_address3,
						   wh2.address4 as bill_address4,
						   wh2.pin_code as bill_pin_code,
						   l4.name as bill_location,
						   wh2.gst_number as bill_gst_number,
						   wh2.pan_number as bill_pan_number,
						   cur.name as currency_name,
						   cou.name as country_name');

		if($searchParams['cancel_type']==1)
		{
			$this->db->select('ph.modified_time');
			$this->db->from('print_format pf');
			$this->db->join('order_delivery od','pf.print_id = od.print_id');
			$this->db->join('print_history ph','ph.order_delivery_id = od.order_delivery_id AND pf.print_id = ph.print_id');
		}
		if($searchParams['cancel_type'] == 2 || $searchParams['cancel_type']==3)
		{
			$this->db->select('ph.modified_time');
			$this->db->from('print_history ph');
			$this->db->join('order_delivery od','od.order_delivery_id = ph.order_delivery_id');
			$this->db->join('print_format pf','pf.print_id = ph.print_id');
		}
		$this->db->join('tool_order to','to.tool_order_id = od.tool_order_id');
		$this->db->join('currency cur','cur.location_id = to.country_id');
		$this->db->join('location cou','cou.location_id = to.country_id');
		$this->db->join('user u','u.sso_id = to.sso_id','LEFT');
		$this->db->join('warehouse from_wh','from_wh.wh_id = to.wh_id');
		$this->db->join('warehouse to_wh','to_wh.wh_id = to.to_wh_id','LEFT');
		$this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
		$this->db->join('location l','l.location_id = oa.location_id','LEFT');
		$this->db->join('warehouse wh1','wh1.wh_id = to.wh_id');
		$this->db->join('location l1','l1.location_id = wh1.location_id','LEFT');
		$this->db->join('location l2','l2.location_id = l1.parent_id','LEFT');
		$this->db->join('warehouse wh2','wh2.wh_id = od.billed_to');
		$this->db->join('location l3','l3.location_id = wh2.location_id','LEFT');
		$this->db->join('location l4','l4.location_id = l3.parent_id','LEFT');
		if($searchParams['type'] !='')
		{
			$this->db->where('pf.print_type',$searchParams['type']);
		}
		if($searchParams['country_id'] !='')
		{
			$this->db->where('to.country_id',$searchParams['country_id']);
		}
		if($searchParams['state_id'] !='')
		{
			$this->db->where_in('to.to_wh_id',$wh_arr);
		}
		if($searchParams['wh_id'] !='')
		{
			$this->db->where('to.to_wh_id',$searchParams['wh_id']);
		}
		if($searchParams['from_date'] !='')
		{
			$this->db->where('pf.created_time >=',$searchParams['from_date']);
		}
		if($searchParams['to_date'] !='')
		{
			$this->db->where('pf.created_time <=',$searchParams['to_date']);
		}
		if($searchParams['print_number'] !='')
		{
			$this->db->where('pf.format_number',$searchParams['print_number']);
		}
		if($searchParams['cancel_type']==3)
		{
			$this->db->where('ph.modified_time !=',NULL);
		}
		$this->db->where('oa.status',1);
		$this->db->where('to.current_stage_id >',2);
		$this->db->where('to.order_type',1);
		//$this->db->where('to.order_type',2);
		$this->db->order_by('pf.print_id ASC');
		$this->db->group_by('pf.print_id');
		$res = $this->db->get();
		return $res->result_array();
	}
}