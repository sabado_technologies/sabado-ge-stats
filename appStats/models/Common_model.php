<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model 
{   
    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION) )
        {   
            session_start();
        } 
        $this->load->model('Login_model');
    }

    function sso_login_check()
    {
        $ssoid = login_sso_id();
        #check if application session not exists
        if($this->session->userdata('sso_id') == NULL)
        {
            #Check login sso id is set
            if($ssoid == NULL)
            {
                redirect(sso_login_url()); // Redirects to SSO login page
            }
            else
            {
                $userDetails = $this->Login_model->getUserDetails($ssoid);
                if($userDetails) // Logged in user is a STATS user
                {
                    $role_details = $this->Login_model->get_child_roles($userDetails['role_id']);

                    #multiple country assigning
                    $countryList = array(); $countriesIndexedlist = ''; $header_country_id = '';
                    if($userDetails['region_id']==3 && $userDetails['role_id']==4)
                    {
                        $assign_countries_list = $this->Common_model->get_data('location',array('level_id'=>2,'region_id'=>$userDetails['region_id'],'status'=>1));
                    }
                    else
                    {
                        $assign_countries_list = $this->Login_model->get_user_countries($userDetails['sso_id']);
                    }
                    if(count(@$assign_countries_list)>1)
                    {
                        $countryList = $assign_countries_list;
                        $countriesIndexedlist = convertIntoIndexedArray($assign_countries_list,'location_id');
                    }
                    else
                    {
                        $countriesIndexedlist = array($userDetails['country_id']);
                        $header_country_id = $userDetails['country_id'];
                    }
                    
                    $sessionData = array(
                        'sso_id'         => $userDetails['sso_id'],
                        'main_sso_id'    => $userDetails['sso_id'],
                        'branch_id'      => $userDetails['branch_id'],
                        'branch_name'    => $userDetails['branch_name'],
                        'role_id'        => $userDetails['role_id'],
                        's_role_id'      => $userDetails['role_id'],
                        'main_role_id'   => $userDetails['role_id'],
                        'role_name'      => $userDetails['role_name'],
                        'userFullName'   => $userDetails['name'],
                        'desig_name'     => $userDetails['designation_name'],
                        's_wh_name'      => $userDetails['wh_name'],
                        's_wh_id'        => $userDetails['wh_id'],
                        'swh_id'         => $userDetails['wh_id'],
                        's_location_id'  => $userDetails['location_id'],
                        's_country_id'   => $userDetails['country_id'],
                        'ChildRolesList' => $role_details,
                        'countryList'    => $countryList,
                        'countriesIndexedArray' => $countriesIndexedlist,
                        'header_country_name' => $userDetails['country_name'],
                        'header_country_id' => $header_country_id,
                        'display_sso_name'  => 'SSO ID',
                        's_region_id'       => $userDetails['region_id']
                    );

                    #multiple warehouse assigning
                    $assign_whs_list = $this->Login_model->get_user_whs($userDetails['sso_id']);
                    if(count($assign_whs_list)>1)
                    {
                        $sessionData['whsIndededArray'] = convertIntoIndexedArray($assign_whs_list,'wh_id');
                    }
                    else
                    {
                        $this->session->unset_userdata('whsIndededArray');
                    }
                    $this->session->set_userdata($sessionData);

                    $user_log_data = array(
                    'sso_id'        => $ssoid,
                    'login_time'    => date('Y-m-d H:i:s'),
                    'ip_address'    => get_client_ip(),
                    'user_agent'    => $_SERVER['HTTP_USER_AGENT'],
                    'last_activity' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('user_log',$user_log_data);

                    redirect(SITE_URL.'home'); exit();
                }
                else  // Logged in user is not a STATS user
                {
                    // Redirects to access denied page
                    redirect(SITE_URL.'access_denied'); 
                }
            }
        }
    }

    function insert_data($table=0,$data)
    {
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                $this->db->insert($table,$data);
                return $this->db->insert_id();
            }
        }
        return 0;
    }    

    /**
    * Insert batch data
    * author: mahesh , created on: 22nd june 2016 04:15 PM, updated on: --
    * @param: $table(string) default:0
    * @param: $data(2D array)
    * return: true/false(boolean)
    **/
    function insert_batch_data($table=0,$data)
    {
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                $this->db->insert_batch($table,$data);
                return true;
            }
        }
        return false;
    }    
    
    //Update data
    function update_data($table = 0,$data, $where)
    {
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                $this->db->update($table, $data, $where);
                return $this->db->affected_rows();
            }
        }
        return 0;
    }
    
    function get_data($table = 0 , $where="", $select = "",$order_by = "", $type = 1)
    {
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                $this->db->select($select);
                $this->db->from($table);
                if($where != '')
                {
                    if(@$where['task_access']!='')
                    {
                        $task_access = $where['task_access'];
                        if($table =='location'){ $id = 'location_id';}
                        else{ $id = 'country_id';}
                        if($task_access == 1 || $task_access == 2)
                        {
                            if($task_access ==1)
                            {
                                if($_SESSION['header_country_id']!='')
                                {
                                    $this->db->where("$id",$this->session->userdata('header_country_id'));    
                                }
                                else
                                {
                                    if(isset($where['ids']))
                                    {   
                                        $where_in = convertIntoIndexedArray($where['ids'],$id);                                    
                                    }
                                    else
                                    {
                                        $where_in = $this->session->userdata('countriesIndexedArray');
                                    }
                                    $this->db->where_in("$id",$where_in);                                 
                                }
                                unset($where['ids']);
                            }
                            else
                            {
                                if(count($this->session->userdata('countriesIndexedArray')) > 1)
                                {
                                    if(isset($where['ids']))
                                    {   
                                        $where_in = convertIntoIndexedArray($where['ids'],$id);                                    
                                    }
                                    else
                                    {
                                        $where_in = $this->session->userdata('countriesIndexedArray');
                                    }
                                    $this->db->where_in("$id",$where_in);                                  
                                }
                                else
                                {
                                    $this->db->where("$id",$this->session->userdata('s_country_id'));
                                }
                            }
                        }
                        else if($task_access == 3)
                        {
                            if($_SESSION['header_country_id']!='')
                            {
                                $this->db->where("$id",$this->session->userdata('header_country_id'));    
                            }
                            else
                            {
                                if(isset($where['ids']))
                                {   
                                    $where_in = convertIntoIndexedArray($where['ids'],$id);                                    
                                }
                                else
                                {
                                    $where_in = $this->session->userdata('countriesIndexedArray');
                                }
                                $this->db->where_in("$id",$where_in);                                 
                            }
                            unset($where['ids']);
                        }
                        unset($where['task_access']);
                    }
                    $this->db->where($where); 
                }
                if($order_by != '')
                {
                    $this->db->order_by($order_by);
                }
                $query = $this->db->get();
                if($type == 1)
                {
                    return $query->result_array();
                }
                else
                {
                    return $query->result();
                }
            }
        }
        return 0;  
    }
    
    //Get single value
    function  get_value($table = 0,$where=array(),$column)
    {
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                if($column!=NULL)
                {
                    $this->db->select($column.' as select_data');
                    $query = $this->db->get_where($table,$where);
                    $result = $query->row_array();
                    if(count($result)>0)
                    {
                        return $result['select_data'];
                    }
                    else
                    {
                        return NULL;
                    }
                }
            }    
        }
        return 0;
    }

        //Get single value
    function  get_value_new($table = 0,$where=array(),$column)
    {
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                if($column!=NULL)
                {
                    $this->db->select($column);
                    $query = $this->db->get_where($table,$where);
                    $result= $query->row_array();
                    
                    if(count($result)>0)
                    {
                        foreach ($result as $key => $value) {
                            return $value;                            
                        }    
                    }
                    else
                    {
                        return NULL;
                    }
                }
            }    
        }
        return 0;
    }

    function get_query_result($qry, $type = "array")
    {
        if($qry)
        {
            $res = $this->db->query($qry);
            if($type == "array")
                return $res->result_array();
            else
                return $res->result();
        }
    }

    function get_no_of_rows($qry)
    {
        if($qry)
        {
            $res = $this->db->query($qry);
            return $res->num_rows();
        }
    }

    function get_dropdown($table=0, $key_column, $val_column, $where=[], $order_by='', $concat = '')
    {
        $data = [];
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                if($concat == '')
                    $this->db->select(array($key_column, $val_column));
                else
                    $this->db->select(array($key_column, $concat));
                if($order_by != '')
                {
                    $this->db->order_by($order_by);
                }
                $query = $this->db->get_where($table,$where);
                foreach($query->result_array() as $row)
                {
                    $data[$row[$key_column]] = $row[$val_column];
                }
                return $data;
            }
        }
        return 0;
    } 

    function getAutocompleteData($table=0, $column, $term)
    {
        $this->db->select($column);
        $this->db->from($table);
        $this->db->where('status',1);
        $this->db->like($column, $term);
        $this->db->limit(20, 0);
        $res = $this->db->get();
        $json=array();
        foreach($res->result_array() as $row){
             $json[]=array(
                        'value'=> $row[$column],
                        'label'=>$row[$column]
                            );
        }
        return $json;
    }

    //Get single row, Mahesh created on 25th Nov 2016 12:50 pm     
    function get_data_row($table = 0 , $where="", $select = "", $type=1 )
    {
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                $this->db->select($select);
                $query = $this->db->get_where($table, $where);
                if($type == 1)
                {
                    return $query->row_array();
                }
                else
                {
                    return $query->row();
                }
            }
        }
        return 0;  
    }     
    
    function delete_data($table=0,$where)
    {
        if($table !== 0)
        {
            if ($this->db->table_exists($table))
            {
                $this->db->delete($table,$where);
                return $this->db->insert_id();
            }
        }
        return 0;
    } 

}
