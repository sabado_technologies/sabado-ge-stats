<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cancelled_invoice_m extends CI_Model 
{
	public function cancelled_invoice_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('p.format_number,rco.rc_type,to.order_type,ph.remarks,ph.modified_time,ph.ph_id,w.wh_code,w.name,ro.return_order_id');
		$this->db->from('print_history ph');
		$this->db->join('print_format p','p.print_id=ph.print_id');
		$this->db->join('order_delivery od','od.order_delivery_id=ph.order_delivery_id','left');
		$this->db->join('tool_order to','to.tool_order_id=od.tool_order_id','left');
		$this->db->join('rc_shipment rcs','rcs.rc_shipment_id=ph.rc_shipment_id','left');
		$this->db->join('rc_order rco','rco.rc_order_id=rcs.rc_order_id','left');
		$this->db->join('return_order ro','ro.return_order_id=ph.return_order_id','left');
		$this->db->join('warehouse w','w.wh_id=p.wh_id');
		$this->db->join('location l','l.location_id = w.country_id');
		if($searchParams['invoice_no']!='')
		{
			$this->db->like('p.format_number',$searchParams['invoice_no']);
		}
		if($searchParams['cancelled_date']!='')
		{
			$this->db->where('date(ph.modified_time)',$searchParams['cancelled_date']);
		}
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('w.country_id',$this->session->userdata('s_country_id'));
			if($task_access == 2)
			{
				if($searchParams['whp_id']!='')
				{
					$this->db->where('p.wh_id',$searchParams['whp_id']);
				}
				
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('w.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('w.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('w.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['whp_id']!='')
			{
				$this->db->where('p.wh_id',$searchParams['whp_id']);
			}
		}
		
		$this->db->where('p.print_type',1);
		$this->db->where('ph.modified_time!=',NULL);
		$this->db->order_by('p.print_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function cancelled_invoice_list_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('p.format_number,rco.rc_type,to.order_type,ph.remarks,ph.modified_time,ph.ph_id,w.wh_code,w.name,ro.return_order_id,l.name as country_name');
		$this->db->from('print_history ph');
		$this->db->join('print_format p','p.print_id=ph.print_id');
		$this->db->join('order_delivery od','od.order_delivery_id=ph.order_delivery_id','left');
		$this->db->join('tool_order to','to.tool_order_id=od.tool_order_id','left');
		$this->db->join('rc_shipment rcs','rcs.rc_shipment_id=ph.rc_shipment_id','left');
		$this->db->join('rc_order rco','rco.rc_order_id=rcs.rc_order_id','left');
		$this->db->join('return_order ro','ro.return_order_id=ph.return_order_id','left');
		$this->db->join('warehouse w','w.wh_id=p.wh_id');
		$this->db->join('location l','l.location_id = w.country_id');
		if($searchParams['invoice_no']!='')
		{
			$this->db->like('p.format_number',$searchParams['invoice_no']);
		}
		if($searchParams['cancelled_date']!='')
		{
			$this->db->where('date(ph.modified_time)',$searchParams['cancelled_date']);
		}
		if($task_access == 1 || $task_access == 2)
		{
			$this->db->where('w.country_id',$this->session->userdata('s_country_id'));
			if($task_access == 2)
			{
				if($searchParams['whp_id']!='')
				{
					$this->db->where('p.wh_id',$searchParams['whp_id']);
				}
				
			}
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('w.country_id',$_SESSION['header_country_id']);	
			}
			else
			{
				if($searchParams['country_id']!='')
				{
					$this->db->where('w.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('w.country_id',$_SESSION['countriesIndexedArray']);	
				}
			}
			if($searchParams['whp_id']!='')
			{
				$this->db->where('p.wh_id',$searchParams['whp_id']);
			}
		}
		$this->db->where('p.print_type',1);
		$this->db->where('ph.modified_time!=',NULL);
		$this->db->order_by('p.print_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_delivery_details($tool_order_id,$ph_id)
	{
		$this->db->select('pf.format_number,
						   pf.created_time,
			               pf.print_type,
						   u.sso_id,
						   u.mobile_no,
						   u.name as user_name,
						   od.courier_type,
						   od.courier_number,
						   od.vehicle_number,
						   to.order_number,
						   to.siebel_number,
						   od.billed_to,
						   od.remarks,
						   od.courier_name,
						   od.contact_person,
						   od.phone_number');
		$this->db->from('order_delivery od');
		$this->db->join('tool_order to','to.tool_order_id = od.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('print_history ph','ph.order_delivery_id = od.order_delivery_id');
		$this->db->join('print_format pf','pf.print_id = ph.print_id');

		$this->db->where('od.tool_order_id',$tool_order_id);
		$this->db->where('ph.ph_id',$ph_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_delivery_details_wh_st($tool_order_id,$ph_id)
	{
		$this->db->select('pf.format_number,od.remarks,
						   pf.created_time,od.courier_type,
						   od.courier_number,to.stn_number,
						   wh.address1,wh.address2,wh.address3,
						   wh.address4,wh.tin_number,
						   wh.gst_number,wh.pan_number,
						   pf.print_type,wh.pin_code,
						   l.name as from_location,
						   od.billed_to,od.courier_name,
						   od.contact_person,od.vehicle_number,
						   od.phone_number,
						   to.to_wh_id,
						   to.fe_check');
		$this->db->from('order_delivery od');
		$this->db->join('tool_order to','to.tool_order_id = od.tool_order_id');
		$this->db->join('warehouse wh','wh.wh_id = to.wh_id');
		$this->db->join('location l','l.location_id = wh.location_id');
		$this->db->join('print_history ph','ph.order_delivery_id = od.order_delivery_id');
		$this->db->join('print_format pf','pf.print_id = ph.print_id');
		$this->db->where('od.tool_order_id',$tool_order_id);
		$this->db->where('ph.ph_id',$ph_id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_from_address($rc_order_id,$ph_id)
	{
		$this->db->select('wh.*,l.name as from_location,rcs.remarks,
						   pf.format_number,pf.created_time,pf.print_type,
						   rcs.courier_type,rcs.courier_number,rcs.billed_to,
						   GROUP_CONCAT(rca.rc_number SEPARATOR",") as crn_number,
						   rcs.courier_name,rcs.contact_person,
						   rcs.phone_number,s.supplier_code,
						   s.name as supplier_name,s.contact_number as s_contact_number,
						   rcs.vehicle_number');
		$this->db->from('rc_order rco');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('warehouse wh','wh.wh_id = rco.wh_id');
		$this->db->join('rc_shipment rcs','rcs.rc_order_id = rco.rc_order_id');
		$this->db->join('print_history ph','ph.rc_shipment_id = rcs.rc_shipment_id');
		$this->db->join('print_format pf','pf.print_id = ph.print_id');
		$this->db->join('location l','l.location_id = wh.location_id');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id','LEFT');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->where('ph.ph_id',$ph_id);
		$this->db->group_by('rco.rc_order_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_from_address_1($rc_order_id,$ph_id)
	{
		$this->db->select('wh.*,l.name as from_location,rcs.remarks,
						   pf.format_number,pf.created_time,pf.print_type,
						   rcs.courier_type,rcs.courier_number,rcs.billed_to,
						   GROUP_CONCAT(rca.rc_number SEPARATOR",") as crn_number,
						   rcs.courier_name,rcs.contact_person,rcs.phone_number,s.supplier_code,s.name as supplier_name,s.contact_number as s_contact_number,rcs.vehicle_number');
		$this->db->from('rc_order rco');
		$this->db->join('supplier s','s.supplier_id = rco.supplier_id','LEFT');
		$this->db->join('rc_asset rca','rca.rc_order_id = rco.rc_order_id');
		$this->db->join('warehouse wh','wh.wh_id = rco.wh_id');
		$this->db->join('rc_shipment rcs','rcs.rc_order_id = rco.rc_order_id');
		$this->db->join('print_history ph','ph.rc_shipment_id = rcs.rc_shipment_id');
		$this->db->join('print_format pf','pf.print_id = ph.print_id');
		$this->db->join('location l','l.location_id = wh.location_id');
		$this->db->where('rco.rc_order_id',$rc_order_id);
		$this->db->where('ph.ph_id',$ph_id);
		$this->db->group_by('rco.rc_order_id');
		$res = $this->db->get();
		return $res->row_array();
	}

	public function get_from_address_2($return_order_id,$ph_id)
	{
		$this->db->select('ro.address1,ro.remarks,ro.address2,ro.address3,
						   ro.address4,ro.zip_code as pin_code,pf.format_number,
						   pf.created_time,ro.return_type_id,l.name as from_location,
						   pf.print_type,ro.gst_number,ro.pan_number,
						   ro.courier_type,ro.courier_number,ro.return_number,
						   u.name,u.sso_id,ro.return_number,ro.billed_to,ro.courier_name,
						   ro.contact_person,ro.phone_number,ro.vehicle_number,
						   CONCAT(u.sso_id,"-(",u.name,")") AS sso_name,u.mobile_no');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('print_history ph','ph.return_order_id = ro.return_order_id');
		$this->db->join('print_format pf','pf.print_id = ph.print_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->where('ro.return_order_id',$return_order_id);
		$this->db->where('ph.ph_id',$ph_id);
		$res = $this->db->get();
		return $res->row_array();
	}
}