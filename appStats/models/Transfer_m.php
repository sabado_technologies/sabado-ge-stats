<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfer_m extends CI_Model 
{
	public function tool_total_num_rows($searchParams,$warehouse_id,$assets_list = array())
	{ 
		$getAssetsInTransferProcess = getAssetsInTransferProcess($assets_list);
		$getAssetsInCalAndRepairProcess = getAssetsInCalAndRepairProcess($assets_list);
		$this->db->select('t.*,a.asset_number,as.name as asset_status,m.name as modality_name,et.eq_model_id');
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['asset_number']!='')
			$this->db->where('a.asset_number',$searchParams['asset_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_status_id']!='')
		{
			$this->db->where_in('as.asset_status_id',$searchParams['asset_status_id']);			
		}
		$this->db->where('p.part_level_id',1);	
		$this->db->where('ap.transit',0);
		$this->db->where('a.approval_status',0);
		$this->db->where('ap.wh_id IS NOT NULL');
		$this->db->where('ap.to_date IS NULL');
		$this->db->group_start();
		$this->db->group_start();
		if($searchParams['asset_status_id']!='')
		{
			$this->db->where_in('as.asset_status_id',$searchParams['asset_status_id']);			
		}
		else
		{
			$this->db->where_in('as.asset_status_id',st_asset_status());
		}
		if(count($getAssetsInTransferProcess)>0)
			$this->db->where_not_in('a.asset_id',$getAssetsInTransferProcess);
		if(count($getAssetsInCalAndRepairProcess)>0)
			$this->db->where_not_in('a.asset_id',$getAssetsInCalAndRepairProcess);
		
		$this->db->where('a.wh_id',$warehouse_id);
		$this->db->group_end();
		if(count($assets_list)>0)
		{
			$this->db->or_group_start();
			$this->db->where_in('a.asset_id',$assets_list);
			if($searchParams['asset_status_id']!='')
			{
				$this->db->where_in('as.asset_status_id',$searchParams['asset_status_id']);			
			}
			else
			{
				$this->db->where_in('as.asset_status_id',st_asset_status_include_lock_in());
			}
			$this->db->group_end();
		}
		$this->db->group_end();
		$this->db->group_by('p.asset_id');		
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function tool_results($current_offset, $per_page, $searchParams,$warehouse_id,$assets_list = array())
	{
		// the assets which are in stock transfer process
		$getAssetsInTransferProcess = getAssetsInTransferProcess($assets_list);  
		$getAssetsInCalAndRepairProcess = getAssetsInCalAndRepairProcess($assets_list);
		$this->db->select('t.*,a.asset_number,a.asset_id,as.name as asset_status,m.name as modality_name,et.eq_model_id,p.serial_number');
		$this->db->from('tool t');		
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('part p','p.tool_id = t.tool_id');
		$this->db->join('asset a','a.asset_id = p.asset_id');
		$this->db->join('equipment_model_tool et','et.tool_id = t.tool_id','left');
		$this->db->join('asset_position ap','ap.asset_id = a.asset_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		if($searchParams['part_number']!='')
			$this->db->like('t.part_number',$searchParams['part_number']);
		if($searchParams['part_description']!='')
			$this->db->like('t.part_description',$searchParams['part_description']);
		if($searchParams['modality_id']!='')
			$this->db->where('m.modality_id',$searchParams['modality_id']);
		if($searchParams['asset_number']!='')
			$this->db->where('a.asset_number',$searchParams['asset_number']);
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_status_id']!='')
		{
			$this->db->where_in('as.asset_status_id',$searchParams['asset_status_id']);			
		}
		$this->db->where('p.part_level_id',1);	
		$this->db->where('ap.transit',0);
		$this->db->where('a.approval_status',0);
		$this->db->where('ap.wh_id IS NOT NULL');
		$this->db->where('ap.to_date IS NULL');
		$this->db->group_start();
		$this->db->group_start();
		if($searchParams['asset_status_id']!='')
		{
			$this->db->where_in('as.asset_status_id',$searchParams['asset_status_id']);
		}
		else
		{
			$this->db->where_in('as.asset_status_id',st_asset_status());
		}
		if(count($getAssetsInTransferProcess)>0)
			$this->db->where_not_in('a.asset_id',$getAssetsInTransferProcess);
		if(count($getAssetsInCalAndRepairProcess)>0)
			$this->db->where_not_in('a.asset_id',$getAssetsInCalAndRepairProcess);
		$this->db->where('a.wh_id',$warehouse_id);
		$this->db->group_end();
		if(count($assets_list)>0)
		{
			$this->db->or_group_start();
			$this->db->where_in('a.asset_id',$assets_list);
			if($searchParams['asset_status_id']!='')
			{
				$this->db->where_in('as.asset_status_id',$searchParams['asset_status_id']);			
			}
			else
			{
				$this->db->where_in('as.asset_status_id',st_asset_status_include_lock_in());
			}
			$this->db->group_end();
		}
		$this->db->group_end();
		$this->db->group_by('p.asset_id');	
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
    // Open Order Results
    public function order_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		$this->db->where('to.current_stage_id!=',10);
		if($_SESSION['role_id'] ==2 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}
		$this->db->where('to.order_type',2);
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	public function order_total_num_rows($searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['current_stage_id']!='')
			$this->db->where('cst.current_stage_id',$searchParams['current_stage_id']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));
		$this->db->where('to.current_stage_id!=',10);	
		if($_SESSION['role_id'] ==2 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}
		$this->db->where('to.status<',2);	
		$this->db->where('to.order_type',2);
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function closed_order_results($current_offset, $per_page, $searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type,to.status as final_status,DATE(to.modified_time) as closed_date ');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['status']!='')
			$this->db->where('to.status',$searchParams['status']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));	
		$this->db->limit($per_page, $current_offset);
		if($_SESSION['role_id'] ==2 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}
		$this->db->where('to.current_stage_id',10);
		$this->db->where('to.order_type',2);
		$res = $this->db->get();
		return $res->result_array();
	}
	public function closed_order_total_num_rows($searchParams)
	{
		$this->db->select('to.*,cst.*,odt.name as order_type,to.status as final_status');
		$this->db->from('tool_order to');		
		$this->db->join('current_stage cst','cst.current_stage_id = to.current_stage_id');
		$this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
		if($searchParams['order_number']!='')
			$this->db->like('to.order_number',$searchParams['order_number']);
		if($searchParams['order_delivery_type_id']!='')
			$this->db->where('to.order_delivery_type_id',$searchParams['order_delivery_type_id']);
		if($searchParams['status']!='')
			$this->db->where('to.status',$searchParams['status']);
		if($searchParams['request_date']!='')
			$this->db->where('to.request_date>=',format_date($searchParams['request_date']));
		if($searchParams['return_date']!='')
			$this->db->where('to.return_date<=',format_date($searchParams['return_date']));	
		if($_SESSION['role_id'] ==2 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6)
		{
			$this->db->where('to.sso_id',$_SESSION['sso_id']);
		}				
		$this->db->where('to.order_type',2);
		$res = $this->db->get();
		return $res->num_rows();
	}
	public function getReturnsForOrder($tool_order_id)
	{
		$arr2 = array(0,2);
		$this->db->select('to.order_number,ro.return_number,ro.created_time,u.name as user_name,l.name as location_name,u.sso_id,ro.return_order_id,ro.received_date,rt.name as return_type_name');
		$this->db->from('return_order ro');
		$this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
		$this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
		$this->db->join('order_status_history osh','osh.rto_id = rto.rto_id');
		$this->db->join('location l','l.location_id = ro.location_id','LEFT');
		$this->db->join('user u','u.sso_id = to.sso_id');
		$this->db->join('return_type rt','rt.return_type_id = ro.return_type_id');
		$this->db->where('to.tool_order_id',$tool_order_id);		
		$this->db->where('osh.current_stage_id',10);//Closed fe returns
		$this->db->where('osh.end_time IS NULL');
		$this->db->where('to.order_type',2); //tool order = 2
		$this->db->where_in('ro.return_approval',$arr2);
		$this->db->order_by('ro.return_number DESC','ro.created_time DESC');		
		$res = $this->db->get();
		return $res->result_array();
	}
	public function get_order_info($tool_order_id)
	{
		$this->db->select('to.*,oda.*');
		$this->db->from('tool_order to');
		$this->db->join('order_address oda','oda.tool_order_id = to.tool_order_id');		
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}
	public function getAssetDataByOrderStatusID($order_status_id)
	{
		$this->db->select('a.asset_number,t.part_number,t.part_description,pl.name as part_level,p.serial_number,p.quantity as quantity,
			tt.name as tool_type,ac.name as ack_status');
		$this->db->from('order_asset_history oah');
		$this->db->join('ordered_asset oa','oa.ordered_asset_id = oah.ordered_asset_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('part_level pl','pl.part_level_id = p.part_level_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('asset_condition ac','ac.asset_condition_id = oah.status');
		$this->db->where('oah.order_status_id',$order_status_id);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.asset_id');
		$res = $this->db->get();
		return $res->result_array();


	}
	public function get_ordered_tools($tool_order_id)
	{
		$this->db->select('o.*,t.*');
		$this->db->from('ordered_tool o');
		$this->db->join('tool t','t.tool_id = o.tool_id');		
		$this->db->where('o.tool_order_id',$tool_order_id);
		$this->db->where('o.status<',3);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_ordered_assets($tool_order_id,$asset_id=0)
	{
		$this->db->select('t.*,a.asset_number,to.current_stage_id,to.tool_order_id,
			as.name as status_name,a.status as asset_status_id,a.asset_id,ot.ordered_tool_id,oa.ordered_asset_id,p.serial_number');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('tool_order to','to.tool_order_id= ot.tool_order_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->where('p.part_level_id',1);
		$this->db->where('to.tool_order_id',$tool_order_id);
		if($asset_id!=0||$asset_id!='')
		{
			$this->db->where('oa.asset_id',$asset_id);
		}
		$this->db->where('ot.status<',3);
		$this->db->where('oa.status<',3);
		$this->db->group_by('p.asset_id');
		$res = $this->db->get();
		if($asset_id!=0||$asset_id!='')
		{
			return $res->row_array();
		}
		else
		{
			return $res->result_array();
		}
	}
	public function get_closed_ordered_assets($tool_order_id)
	{
		$this->db->select('t.*,a.asset_number,to.current_stage_id,to.tool_order_id,
			as.name as status_name,a.status as asset_status_id,a.asset_id,ot.ordered_tool_id,oa.ordered_asset_id,p.serial_number');
		$this->db->from('ordered_tool ot');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('tool_order to','to.tool_order_id= ot.tool_order_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->where('p.part_level_id',1);
		$this->db->where('to.tool_order_id',$tool_order_id);		
		$this->db->group_by('p.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}


	public function get_return_info($tool_order_id)
	{
		$this->db->select('to.*,od.*');
		$this->db->from('tool_order to');
		$this->db->join('order_delivery od','od.tool_order_id = to.tool_order_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$res = $this->db->get();
		return $res->row_array();
	}

		public function get_st_stage_assets($tool_order_id,$current_stage_id)
	{
		$this->db->select('t.*,a.asset_number,to.current_stage_id,to.tool_order_id,
			as.name as status_name,a.status as asset_status_id,a.asset_id,ot.ordered_tool_id,oa.ordered_asset_id');
		$this->db->from('order_status_history osh');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->join('asset a','a.asset_id = oa.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('tool_order to','to.tool_order_id= ot.tool_order_id');
		$this->db->join('asset_status as','as.asset_status_id = a.status');
		$this->db->where('p.part_level_id',1);
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('ot.status<',3);
		$this->db->group_by('p.asset_id');
		$res = $this->db->get();
		return $res->result_array();
	}

	#get list of assets under tool order id - koushik- 10-10-2018
	public function get_existing_asset_list($tool_order_id)
	{
		$this->db->select('oa.asset_id');
		$this->db->from('tool_order to');
		$this->db->join('ordered_tool ot','ot.tool_order_id = to.tool_order_id');
		$this->db->join('ordered_asset oa','oa.ordered_tool_id = ot.ordered_tool_id');
		$this->db->where('to.tool_order_id',$tool_order_id);
		$this->db->where('ot.status <',3);
		$this->db->where('oa.status <',3);
		$res = $this->db->get();
		$result = $res->result_array();
		$assets_list = convertIntoIndexedArray($result,'asset_id');
		return $assets_list;
	}
}	