<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scrap_model extends CI_Model 
{
	#mastan
	public function asset_results($current_offset, $per_page)
	{
		$this->db->select('a.*,a.asset_id,wh.name as warehouse,t.part_number as tool_number,t.part_description as tool_description,s.name as status_name');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id=da.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('asset_status s','s.asset_status_id =a.status');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['wh_id']!='')
			$this->db->where('a.wh_id',$searchParams['wh_id']);
		if($searchParams['status']!='')
			$this->db->where('a.status',$searchParams['status']);
		if($searchParams['tool_number']!='')
			$this->db->like('t.part_number',$searchParams['tool_number']);
		if($searchParams['tool_description']!='')
			$this->db->like('t.part_description',$searchParams['tool_description']);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('da.defective_asset_id');
		$this->db->order_by('a.asset_id ASC');
		$this->db->where('a.asset_id not in(select asset_id from scrap)');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	#mastan
	public function asset_total_num_rows($searchParams)
	{
		$this->db->select('a.*,wh.name as warehouse,t.part_number as tool_number,t.part_description as tool_description,s.name as status_name');
		$this->db->from('defective_asset da');
		$this->db->join('asset a','a.asset_id=da.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('asset_status s','s.asset_status_id =a.status');
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['wh_id']!='')
			$this->db->where('a.wh_id',$searchParams['wh_id']);
		if($searchParams['status']!='')
			$this->db->where('a.status',$searchParams['status']);
		if($searchParams['tool_number']!='')
			$this->db->like('t.part_number',$searchParams['tool_number']);
		if($searchParams['tool_description']!='')
			$this->db->like('t.part_description',$searchParams['tool_description']);
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('da.defective_asset_id');
		$this->db->order_by('a.asset_id ASC');
		$this->db->where('a.asset_id not in(select asset_id from scrap)');
		$res = $this->db->get();
		return $res->num_rows();
	}

	public function scraped_asset_results($current_offset, $per_page, $searchParams,$task_access)
	{
		$this->db->select('a.*,l.name as country_name,a.asset_id,wh.name as warehouse,t.part_number as tool_number,t.part_description as tool_description,s.name as status_name,date(sc.created_time) as scraped_date,sc.scrap_id,wh.wh_code');
		$this->db->from('scrap sc');
		$this->db->join('asset a','sc.asset_id=a.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		$this->db->join('asset_status s','s.asset_status_id =a.status');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['status']!='')
			$this->db->where('a.status',$searchParams['status']);
		if($searchParams['tool_number']!='')
			$this->db->like('t.part_number',$searchParams['tool_number']);
		if($searchParams['tool_description']!='')
			$this->db->like('t.part_description',$searchParams['tool_description']);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{

			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}
				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.asset_id');
		$this->db->order_by('a.asset_id ASC');
		$this->db->limit($per_page, $current_offset);
		$res = $this->db->get();
		return $res->result_array();
	}
	
	#mastan
	public function scraped_asset_total_num_rows($searchParams,$task_access)
	{
		$this->db->select('a.*,l.name as country_name,wh.name as warehouse,t.part_number as tool_number,t.part_description as tool_description,s.name as status_name,date(sc.created_time) as scraped_date,sc.scrap_id,p.serial_number');
		$this->db->from('scrap sc');
		$this->db->join('asset a','sc.asset_id=a.asset_id');
		$this->db->join('part p','p.asset_id = a.asset_id');
		$this->db->join('tool t','t.tool_id = p.tool_id');
		$this->db->join('modality m','m.modality_id = t.modality_id');
		$this->db->join('tool_type tt','tt.tool_type_id = t.tool_type_id');
		$this->db->join('warehouse wh','wh.wh_id = a.wh_id');
		$this->db->join('location l','l.location_id=a.country_id');
		$this->db->join('asset_status s','s.asset_status_id =a.status');
		if($searchParams['serial_no']!='')
			$this->db->where('p.serial_number',$searchParams['serial_no']);
		if($searchParams['asset_number']!='')
			$this->db->like('a.asset_number',$searchParams['asset_number']);
		if($searchParams['status']!='')
			$this->db->where('a.status',$searchParams['status']);
		if($searchParams['tool_number']!='')
			$this->db->like('t.part_number',$searchParams['tool_number']);
		if($searchParams['tool_description']!='')
			$this->db->like('t.part_description',$searchParams['tool_description']);
		if($task_access == 1)
		{
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 2)
		{
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
			$this->db->where('a.country_id',$this->session->userdata('s_country_id'));
		}
		else if($task_access == 3)
		{
			if($_SESSION['header_country_id']!='')
			{
				$this->db->where('a.country_id',$_SESSION['header_country_id']);     
			}
			else
			{
				if($searchParams['country_id']!='')
				{

					$this->db->where('a.country_id',$searchParams['country_id']);
				}

				else
				{
					$this->db->where_in('a.country_id',$_SESSION['countriesIndexedArray']);               
				}
			}
			if($searchParams['whid']!='')
			{
				$this->db->where('a.wh_id',$searchParams['whid']);
			}
		}
		$this->db->where('p.part_level_id',1);
		$this->db->group_by('a.asset_id');
		$this->db->order_by('a.asset_id ASC');
		$res = $this->db->get();
		return $res->num_rows();
	}
}