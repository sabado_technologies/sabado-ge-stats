<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

#database settings
$db_settings = array('hostname' => 'localhost',
				     'username' => 'root',
					 'password' => '',
				     'database' => 'stats_country_asean');
$config['db_settings'] = $db_settings;

#login url 
$config['login_url'] = SITE_URL.'login';

#logout url
$config['logout_url'] = SITE_URL.'loggedOff';

#after successfully login SM_USER variable is define
$config['SM_USER'] = @$_COOKIE['SM_USER'];

#SMTP Settings
$smtp_arr = array('smtp_host' => 'ssl://smtp.gmail.com',
				  'smtp_port' => '465',
				  'smtp_user' => 'entransys.test@gmail.com',
				  'smtp_pass' => 'test@2929');
$config['smtp_settings'] = $smtp_arr;

#unset variable when logout 
$config['unset_sso'] = 'SM_USER'; //for production define as empty

$config['asean_country_name'] = 'ASEAN';
$config['testing_mail'] = "<br><br><strong style='color:#FF0000'>This message/email was sent from an unmonitored mailbox. Please do not respond to this message/email. </strong>";
$config['subject_test'] = 'Testing Mail';
?>