<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:fe2_fe_receive

|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
// User
$route['login'] = 'Login/login';
$route['logout'] = 'Login/logout';
$route['is_usernameExist'] = 'Login/is_usernameExist';
$route['registerUser'] = 'Login/registerUser';
$route['fe_login_update']='User/fe_login_update';
$route['get_location_by_role'] = 'User/get_location_by_role';
$route['get_locations_by_role'] = 'User/get_locations_by_role';

$route['access_denied'] = 'Login/access_denied';
$route['loggedOff'] = 'Login/loggedOff';

$route['roles/(:any)'] = 'Login/roles/$1';
$route['sso_update'] = 'Login/sso_update';

//modality master - created by koushik- 12-06-17, modified by gowri 16 june 
$route['modality'] = 'Modality/modality';
$route['modality/(:any)'] = 'Modality/modality/$1';
$route['add_modality'] = 'Modality/add_modality';
$route['insert_modality'] = 'Modality/insert_modality';
$route['edit_modality/(:any)'] = 'Modality/edit_modality/$1';
$route['update_modality'] = 'Modality/update_modality';
$route['deactivate_modality/(:any)'] = 'Modality/deactivate_modality/$1';
$route['activate_modality/(:any)'] = 'Modality/activate_modality/$1';
$route['download_modality'] = 'Modality/download_modality';
//Uniqueness of Modality name 
$route['is_modalitynameExist'] = 'Modality/is_modalitynameExist';
$route['is_modalitycodeExist'] = 'Modality/is_modalitycodeExist';

// Created By gowri on 15th June 5:30PM
$route['eq_model'] = 'Eq_model/eq_model';
$route['eq_model/(:any)'] = 'Eq_model/eq_model/$1';
$route['add_eq_model'] = 'Eq_model/add_eq_model';
$route['insert_eq_model'] = 'Eq_model/insert_eq_model';
$route['edit_eq_model/(:any)'] = 'Eq_model/edit_eq_model/$1';
$route['update_eq_model'] = 'Eq_model/update_eq_model';
$route['deactivate_eq_model/(:any)'] = 'Eq_model/deactivate_eq_model/$1';
$route['activate_eq_model/(:any)'] = 'Eq_model/activate_eq_model/$1';
//Uniqueness of equipment model name 
$route['is_eq_model_nameExist'] = 'Eq_model/is_eq_model_nameExist';

/*Author= aswini
	 date = 14/06/17 modified by gowri 19th june
	 routes for designation*/
$route['designation'] = 'Designation/designation';
$route['designation/(:any)'] = 'Designation/designation/$1';
$route['add_designation'] = 'Designation/add_designation';
$route['insert_designation'] = 'Designation/insert_designation';
$route['edit_designation/(:any)'] = 'Designation/edit_designation/$1';
$route['update_designation'] = 'Designation/update_designation';
$route['deactivate_designation/(:any)'] = 'Designation/deactivate_designation/$1';
$route['activate_designation/(:any)'] = 'Designation/activate_designation/$1';
$route['check_designation_name_availability'] = 'Designation/check_designation_name_availability';

//Location -Roopa -13-06-2017
$route['location_add']='Location/location_add';

// Country -Roopa -13-06-2017
$route['country'] = 'Location/country';
$route['country/(:any)'] = 'Location/country/$1';
$route['add_country'] = 'Location/add_country';
$route['edit_country/(:any)'] = 'Location/edit_country/$1';
$route['deactivate_country/(:any)'] = 'Location/deactivate_country/$1';
$route['activate_country/(:any)'] = 'Location/activate_country/$1';
$route['check_country'] = 'Location/check_country';


// Zone  -Roopa -13-06-2017
$route['zone'] = 'Location/zone';
$route['zone/(:any)'] = 'Location/zone/$1';
$route['add_zone'] = 'Location/add_zone';
$route['edit_zone/(:any)'] = 'Location/edit_zone/$1';
$route['deactivate_zone/(:any)'] = 'Location/deactivate_zone/$1';
$route['activate_zone/(:any)'] = 'Location/activate_zone/$1';
$route['check_zone'] = 'Location/check_zone';

// State  -Roopa -14-06-2017
$route['state'] = 'Location/state';
$route['state/(:any)'] = 'Location/state/$1';
$route['add_state'] = 'Location/add_state';
$route['edit_state/(:any)'] = 'Location/edit_state/$1';
$route['deactivate_state/(:any)'] = 'Location/deactivate_state/$1';
$route['activate_state/(:any)'] = 'Location/activate_state/$1';
$route['check_state'] = 'Location/check_state';
$route['is_shortNameExist'] = 'Location/is_shortNameExist';

// City  -Roopa -14-06-2017
$route['city'] = 'Location/city';
$route['city/(:any)'] = 'Location/city/$1';
$route['add_city'] = 'Location/add_city';
$route['edit_city/(:any)'] = 'Location/edit_city/$1';
$route['deactivate_city/(:any)'] = 'Location/deactivate_city/$1';
$route['activate_city/(:any)'] = 'Location/activate_city/$1';
$route['check_city'] = 'Location/check_city';

// Area  -Roopa -15-06-2017
$route['area'] = 'Location/area';
$route['area/(:any)'] = 'Location/area/$1';
$route['add_area'] = 'Location/add_area';
$route['edit_area/(:any)'] = 'Location/edit_area/$1';
$route['deactivate_area/(:any)'] = 'Location/deactivate_area/$1';
$route['activate_area/(:any)'] = 'Location/activate_area/$1';
$route['check_area'] = 'Location/check_area';

// Created By Maruthi on 13th June 17 8:30PM
$route['tool'] = 'Tool/tool';
$route['tool/(:any)'] = 'Tool/tool/$1';
$route['add_tool'] = 'Tool/add_tool';
$route['insert_tool'] = 'Tool/insert_tool';
$route['edit_tool/(:any)'] = 'Tool/edit_tool/$1';
$route['update_tool'] = 'Tool/update_tool';
$route['deactivate_tool/(:any)'] = 'Tool/deactivate_tool/$1';
$route['activate_tool/(:any)'] = 'Tool/activate_tool/$1';
$route['download_tool'] = 'Tool/download_tool';
$route['deactivate_tool_sub_component'] = 'Tool/deactivate_tool_sub_component';
// tool bulk uploads
$route['bulkupload_tool'] = 'Tool/bulkupload_tool';
$route['bulkupload_tool/(:any)'] = 'Tool/bulkupload_tool/$1';
$route['submit_toolupload'] ='Tool/submit_toolupload';
$route['download_missed_tool_uploads'] = 'Tool/download_missed_tool_uploads';
$route['missed_bulkupload_tool_view/(:any)'] = 'Tool/missed_bulkupload_tool_view/$1';
$route['missed_bulkupload_tool_view/(:any)/(:any)'] = 'Tool/missed_bulkupload_tool_view/$1/$2';

// Wge Tool Uploads
$route['wge_submit_toolupload'] ='Tool/wge_submit_toolupload';
// $route['wge_submit_toolupload/'] ='Tool/wge_submit_toolupload';
//Uniqueness of Part Number
$route['check_part_number_availability'] = 'Tool/check_part_number_availability';
$route['check_tool_code_availability'] = 'Tool/check_tool_code_availability';


// Created By Maruthi on 20th June 17 5:40PM
$route['asset'] = 'Asset/asset';
$route['asset/(:any)'] = 'Asset/asset/$1';
$route['add_asset'] = 'Asset/add_asset';
$route['insert_asset'] = 'Asset/insert_asset';
$route['edit_asset/(:any)'] = 'Asset/edit_asset/$1';
$route['update_asset'] = 'Asset/update_asset';
$route['deactivate_asset/(:any)'] = 'Asset/deactivate_asset/$1';
$route['activate_asset/(:any)'] = 'Asset/activate_asset/$1';
$route['download_asset'] = 'Asset/download_asset';
// asset bulk uploads
$route['bulkupload_asset'] = 'Asset/bulkupload_asset';
$route['bulkupload_asset/(:any)'] = 'Asset/bulkupload_asset/$1';
$route['submit_assetupload'] ='Asset/submit_assetupload';
$route['download_missed_asset_uploads'] = 'Asset/download_missed_asset_uploads';
$route['search_by_part'] = 'Asset/search_by_part';
$route['check_serial_number_availability'] = 'Asset/check_serial_number_availability';
$route['deactivate_attached_record'] = 'Asset/deactivate_attached_record';
$route['activate_attached_record'] = 'Asset/activate_attached_record';

// Wge Asset Bulk Uploads
$route['wge_bulkupload_asset'] = 'Asset/wge_bulkupload_asset';
$route['wge_bulkupload_asset/(:any)'] = 'Asset/wge_bulkupload_asset/$1';
$route['wge_submit_assetupload'] ='Asset/wge_submit_assetupload';
//$route['wge_download_missed_asset_uploads'] = 'Asset/wge_download_missed_asset_uploads';
$route['missed_bulkupload_asset_view/(:any)'] = 'Asset/missed_bulkupload_asset_view/$1';
$route['missed_bulkupload_asset_view/(:any)/(:any)'] = 'Asset/missed_bulkupload_asset_view/$1/$1';
$route['download_missed_asset_uploads'] = 'Asset/download_missed_asset_uploads';

// Upload With Asset Number
$route['asset_wge_bulkupload_asset'] = 'Asset/asset_wge_bulkupload_asset';
$route['asset_wge_bulkupload_asset/(:any)'] = 'Asset/asset_wge_bulkupload_asset/$1';
$route['asset_wge_submit_assetupload'] ='Asset/asset_wge_submit_assetupload';
$route['asset_missed_bulkupload_asset_view/(:any)'] = 'Asset/asset_missed_bulkupload_asset_view/$1';
$route['asset_missed_bulkupload_asset_view/(:any)/(:any)'] = 'Asset/asset_missed_bulkupload_asset_view/$1/$1';
$route['asset_download_missed_asset_uploads'] = 'Asset/asset_download_missed_asset_uploads';

// view Tools info
$route['tool_info'] = 'Asset/tool_info';
$route['tool_info/(:any)'] = 'Asset/tool_info/$1';

// Branch-Ware House  -Roopa -16-06-2017
$route['branch'] = 'Branch/branch';
$route['branch/(:any)'] = 'Branch/branch/$1';
$route['add_branch'] = 'Branch/add_branch';
$route['insert_branch'] = 'Branch/insert_branch';
$route['edit_branch/(:any)'] = 'Branch/edit_branch/$1';
$route['update_branch'] = 'Branch/update_branch';
$route['deactivate_branch/(:any)'] = 'Branch/deactivate_branch/$1';
$route['activate_branch/(:any)'] = 'Branch/activate_branch/$1';
$route['is_branchnameExist'] = 'Branch/is_branchnameExist';
$route['is_whcodeExist'] = 'Branch/is_whcodeExist';

// Branch-Office  -Roopa -17-06-2017
$route['branch_office'] = 'Branch_office/branch_office';
$route['branch_office/(:any)'] = 'Branch_office/branch_office/$1';
$route['add_branch_office'] = 'Branch_office/add_branch_office';
$route['insert_branch_office'] = 'Branch_office/insert_branch_office';
$route['edit_branch_office/(:any)'] = 'Branch_office/edit_branch_office/$1';
$route['update_branch_office'] = 'Branch_office/update_branch_office';
$route['deactivate_branch_office/(:any)'] = 'Branch_office/deactivate_branch_office/$1';
$route['activate_branch_office/(:any)'] = 'Branch_office/activate_branch_office/$1';
$route['is_officenameExist'] = 'Branch_office/is_officenameExist';

//ware house
$route['is_branchcodeExist'] = 'Branch/is_branchcodeExist';
//office
$route['is_branch1codeExist'] = 'Branch_office/is_branch1codeExist';


// Ajax_ci
 // Created by Roopa on 17th june 2017
 $route['ajax_get_zone_by_country_id']='Ajax_ci/ajax_get_zone_by_country_id';
 $route['ajax_get_state_by_zone_id']='Ajax_ci/ajax_get_state_by_zone_id';
 $route['ajax_get_city_by_state_id']='Ajax_ci/ajax_get_city_by_state_id';
 $route['ajax_get_area_by_city_id']='Ajax_ci/ajax_get_area_by_city_id';

 //aswini-supplier..
$route['supplier'] = 'Supplier/supplier';
$route['supplier/(:any)'] = 'Supplier/supplier/$1';
$route['add_supplier'] = 'Supplier/add_supplier';
$route['insert_supplier'] = 'Supplier/insert_supplier';
$route['edit_supplier/(:any)'] = 'Supplier/edit_supplier/$1';
$route['update_supplier'] = 'Supplier/update_supplier';
$route['deactivate_supplier/(:any)'] = 'Supplier/deactivate_supplier/$1';
$route['activate_supplier/(:any)'] = 'Supplier/activate_supplier/$1';
$route['supplier_name_availability'] = 'Supplier/supplier_name_availability';

//Install Base master - koushik - 19-06-17
$route['install_base'] = 'Install_base/install_base';
$route['install_base/(:any)'] = 'Install_base/install_base/$1';
$route['add_install_base'] = 'Install_base/add_install_base';
$route['insert_install_base'] = 'Install_base/insert_install_base';
$route['edit_install_base/(:any)'] = 'Install_base/edit_install_base/$1';
$route['update_install_base'] = 'Install_base/update_install_base';
$route['deactivate_install_base/(:any)'] = 'Install_base/deactivate_install_base/$1';
$route['activate_install_base/(:any)'] = 'Install_base/activate_install_base/$1';
$route['is_ib_system_idExist'] = 'Install_base/is_ib_system_idExist';
$route['get_customer_details_by_id'] = 'Install_base/get_customer_details_by_id';
$route['is_ib_site_id_Exist'] = 'Install_base/is_ib_site_id_Exist';
//Install Base - Bulk Upload
$route['bulkupload_install_base'] = 'Install_base/bulkupload_install_base';
$route['missed_ib_list/(:any)'] = 'Install_base/missed_ib_list/$1';
$route['missed_ib_list/(:any)/(:any)'] = 'Install_base/missed_ib_list/$1/$2';
$route['insert_bulkupload_install_base'] = 'Install_base/insert_bulkupload_install_base';
$route['download_missed_ib_uploads/(:any)'] = 'Install_base/download_missed_ib_uploads/$1';

//User Master Koushik on 13th June 17 8:30PM
$route['user'] = 'User/user';
$route['user/(:any)'] = 'User/user/$1';
$route['add_user'] = 'User/add_user';
$route['insert_user'] = 'User/insert_user';
$route['edit_user/(:any)'] = 'User/edit_user/$1';
$route['update_user'] = 'User/update_user';
$route['deactivate_user/(:any)'] = 'User/deactivate_user/$1';
$route['activate_user/(:any)'] = 'User/activate_user/$1';
$route['is_user_sso_Exist'] = 'User/is_user_sso_Exist';
$route['get_designation_by_role'] = 'User/get_designation_by_role';
//user - bulk upload
$route['bulkupload_user'] = 'User/bulkupload_user';
$route['insert_bulkupload_user'] = 'User/insert_bulkupload_user';
$route['missed_user_list/(:any)'] = 'User/missed_user_list/$1';
$route['missed_user_list/(:any)/(:any)'] = 'User/missed_user_list/$1/$2';
$route['download_missed_user_list/(:any)'] = 'User/download_missed_user_list/$1';

// QR Code: Mahesh 24th June 2017
$route['asset_qr/(:any)/(:any)'] = 'Asset/asset_qr/$1/$2';
$route['print_qr'] = 'Asset/print_qr';
$route['asset_info'] = 'Asset/asset_info';

//Created By Roopa on 23rd June-2017 11:00AM.
$route['assetview_details/(:any)'] = 'Asset/assetview_details/$1';
$route['assetview_details/(:any)/(:any)'] = 'Asset/assetview_details/$1/$1';

// Order Workflow Created By Maruthi on 21st July
// Raise Order Created By Maruthi on 20th july'17
$route['raise_order'] = 'Order/raise_order';
$route['raise_order/(:any)'] = 'Order/raise_order/$1';
$route['raise_order/(:any)/(:any)'] = 'Order/raise_order/$1/$1';
$route['onbehalf_fe'] = 'Order/onbehalf_fe';
$route['issue_tool'] = 'Order/issue_tool';
$route['issue_tool/(:any)'] = 'Order/issue_tool/$1';
$route['removeToolFromIssueCart'] = 'Order/removeToolFromIssueCart';
$route['insert_order'] = 'Order/insert_order';

// Raise Transfer From WH Created By Maruthi on 26th Aug
$route['onbehalf_logistic'] = 'Transfer/onbehalf_logistic';
$route['raise_transfer'] = 'Transfer/raise_transfer';
$route['raise_transfer/(:any)'] = 'Transfer/raise_transfer/$1';
$route['issue_tool_transfer'] = 'Transfer/issue_tool_transfer';
$route['removeToolFromIssueCartTransfer'] = 'Transfer/removeToolFromIssueCartTransfer';
$route['insert_transfer'] = 'Transfer/insert_transfer';

#modified by koushik 18-05-2018
$route['edit_transfer/(:any)'] = 'Transfer/edit_transfer/$1';
$route['edit_transfer/(:any)/(:any)'] = 'Transfer/edit_transfer/$1/$2';

#modified by koushik 18-05-2018
$route['update_transfer']  = 'Transfer/update_transfer';
$route['update_transfer/(:any)']  = 'Transfer/update_transfer/$1';

$route['cancel_transfer/(:any)'] = 'Transfer/cancel_transfer/$1';
// Admin Approve/Reject WH Stock Transfer
$route['approveOrRejectST/(:any)'] = 'Transfer/approveOrRejectST/$1';
$route['approveOrRejectST/(:any)/(:any)'] = 'Transfer/approveOrRejectST/$1/$1';
$route['submitApproveOrRejectST'] = 'Transfer/submitApproveOrRejectST';

// Mahesh
$route['ajax_toolsAvailabilityWithFE'] = 'Fe2fe/ajax_toolsAvailabilityWithFE';
$route['ajax_toolsAvailabilityWithFE/(:any)'] = 'Fe2fe/ajax_toolsAvailabilityWithFE/$1';

// maruthi on 18th sep 
$route['ajax_toolsNeededByFE'] = 'Fe2fe/ajax_toolsNeededByFE';
$route['ajax_toolsNeededByFE/(:any)'] = 'Fe2fe/ajax_toolsNeededByFE/$1';

// Ajax Func  Created By Maruthi on 20th July 17
$route['chackSiteIDAvailability'] = 'Order/chackSiteIDAvailability';
$route['getAddressByID'] = 'Order/getAddressByID';
$route['chackSystemIDAvailability'] = 'Order/chackSystemIDAvailability';
$route['getWarehouseAddress']='Order/getWarehouseAddress';
$route['htmlWarehouseDropdown'] = 'Order/htmlWarehouseDropdown';
// open order Created By Maruthi on 21st July 17
$route['open_order'] = 'Order/open_order';
$route['open_order/(:any)'] = 'Order/open_order/$1';

// Order Info Created By Maruthi on 29th July 17
$route['checkToolsAvailabilityAgain/(:any)'] = 'Order/checkToolsAvailabilityAgain/$1';

$route['order_info/(:any)'] = 'Order/order_info/$1';
$route['order_info/(:any)/(:any)'] = 'Order/order_info/$1/$2';
// Edit Order Created By Maruthi on 22nd July 17
$route['edit_order/(:any)'] = 'Order/edit_order/$1';
$route['update_order']  = 'Order/update_order';
// Cancel Order Created By Maruthi on 23rd July 17
$route['cancel_order/(:any)'] = 'Order/cancel_order/$1';
$route['deactivate_attached_order_rec'] = 'Order/deactivate_attached_order_rec';
$route['activate_attached_order_rec'] = 'Order/activate_attached_order_rec';
$route['closed_order'] = 'Order/closed_order';
$route['closed_order/(:any)'] = 'Order/closed_order/$1';
$route['viewFeClosedReturnDetails/(:any)'] = 'Order/viewFeClosedReturnDetails/$1';

// Receive Order Created By Maruthi on 25th July 17
$route['receive_order']= 'Order/receive_order';
$route['receive_order/(:any)']= 'Order/receive_order/$1';
$route['receive_order_details/(:any)']  = 'Order/receive_order_details/$1';
$route['insert_fe_received_order'] ='Order/insert_fe_received_order';

// Raise Pickup Created By Maruthi on 25th July 17
$route['raise_pickup'] = 'Order/raise_pickup';
$route['raise_pickup/(:any)'] = 'Order/raise_pickup/$1';
$route['owned_order_details/(:any)']  = 'Order/owned_order_details/$1';
$route['insert_fe_owned_order'] ='Order/insert_fe_owned_order';
$route['check_order_number_availability'] ='Order/check_order_number_availability';

// Open Fe Return Initaited List Created By Maruthi on 25th July 17
$route['open_fe_return_initiated_list'] ='Order/open_fe_return_initiated_list';
$route['open_fe_return_initiated_list/(:any)'] = 'Order/open_fe_return_initiated_list/$1';
$route['viewOpenReturnDetials/(:any)/(:any)'] ='Order/viewOpenReturnDetials/$1/$1';
$route['viewClosedReturnDetials/(:any)'] ='Order/viewClosedReturnDetials/$1';

// Cklosed Fe Return Initaited List Created By Maruthi on 25th July 17
$route['closed_fe_return_initiated_list'] ='Order/closed_fe_return_initiated_list';
$route['closed_fe_return_initiated_list/(:any)'] = 'Order/closed_fe_return_initiated_list/$1';

// Fe owned Created By Maruthi on 27th July 17
$route['toolsAvailabilityWithFE'] = 'Fe2fe/toolsAvailabilityWithFE';
$route['toolsAvailabilityWithFE/(:any)'] = 'Fe2fe/toolsAvailabilityWithFE/$1';

$route['fe_owned_tools'] = 'Fe2fe/fe_owned_tools';
$route['fe_owned_tools/(:any)'] = 'Fe2fe/fe_owned_tools/$1';

// Fe to Fe Approval Created By Maruthi on 29th July 17
$route['fe2_fe_approval'] = 'Fe2fe/fe2_fe_approval';
$route['fe2_fe_approval/(:any)'] = 'Fe2fe/fe2_fe_approval/$1';
$route['fe2_fe_approval_details/(:any)'] = 'Fe2fe/fe2_fe_approval_details/$1';
$route['insert_fe2_fe_approval'] = 'Fe2fe/insert_fe2_fe_approval';

// Fe to Fe Receive Created By Maruthi on 29th July 17
$route['fe2_fe_receive'] = 'Fe2fe/fe2_fe_receive';
$route['fe2_fe_receive/(:any)'] = 'Fe2fe/fe2_fe_receive/$1';
$route['fe2_fe_receive_details/(:any)'] = 'Fe2fe/fe2_fe_receive_details/$1';
$route['insert_fe2_fe_receive'] = 'Fe2fe/insert_fe2_fe_receive';

// Admin Raise Stock Transfer For Order Created By Maruthi on 1st Aug 17
$route['raiseSTforOrder'] = 'Stock_transfer/raiseSTforOrder';
$route['raiseSTforOrder/(:any)'] = 'Stock_transfer/raiseSTforOrder/$1';
$route['assignSTtoolsForOrder/(:any)'] = 'Stock_transfer/assignSTtoolsForOrder/$1';
$route['insertAssignedSTtools']	= 'Stock_transfer/insertAssignedSTtools';
// Open Stock Transfer List Created By Maruthi on 3rd Aug 17
$route['openSTforOrder']	= 'Stock_transfer/openSTforOrder';
$route['openSTforOrder/(:any)']	= 'Stock_transfer/openSTforOrder/$1';
// Closed ST For Order Created By Maruthi on 3rd Aug 17
$route['closedSTforOrder'] = 'Stock_transfer/closedSTforOrder';
$route['closedSTforOrder/(:any)'] = 'Stock_transfer/closedSTforOrder/$1';

// WH2 Receive  Created By Maruthi on 5th Aug 17
$route['wh2_wh_receive'] = 'Stock_transfer/wh2_wh_receive';
$route['wh2_wh_receive/(:any)'] = 'Stock_transfer/wh2_wh_receive/$1';
$route['wh2_wh_receive_details/(:any)'] = 'Stock_transfer/wh2_wh_receive_details/$1';
$route['insert_wh2_wh_receive'] = 'Stock_transfer/insert_wh2_wh_receive';
$route['wh2_wh_closed'] = 'Stock_transfer/wh2_wh_closed';
$route['wh2_wh_closed/(:any)'] = 'Stock_transfer/wh2_wh_closed/$1';

// Alerts
// Address Change Alert  Created By Maruthi on 12th Aug 17
$route['address_change'] = 'Alerts/address_change';
$route['address_change/(:any)'] = 'Alerts/address_change/$1';
$route['order_address_change/(:any)'] = 'Alerts/order_address_change/$1';
$route['submit_order_address_change'] = 'Alerts/submit_order_address_change';
$route['return_address_change/(:any)'] = 'Alerts/return_address_change/$1';
$route['submit_return_address_change'] = 'Alerts/submit_return_address_change';

$route['crossed_return_date_orders'] = 'Alerts/crossed_return_date_orders';
$route['crossed_return_date_orders/(:any)'] = 'Alerts/crossed_return_date_orders/$1';
$route['crossed_return_date_orders_details/(:any)'] = 'Alerts/crossed_return_date_orders_details/$1';

// created by maruthi 
$route['missed_asset']='Defective_asset/missed_asset';
$route['missed_asset/(:any)']='Defective_asset/missed_asset/$1';
$route['submit_missed_asset'] ='Defective_asset/submit_missed_asset';

//Defective Asset List By Srilekha on 2nd August 17 05:44PM
$route['defective_asset']='Defective_asset/defective_asset';
$route['defective_asset/(:any)']='Defective_asset/defective_asset/$1';
$route['cancel_defective_asset/(:any)']='Defective_asset/cancel_defective_asset/$1';

// Raise Request To Extend The days
$route['raiseDaysExtensionRequest/(:any)'] = 'Alerts/raiseDaysExtensionRequest/$1';
$route['raiseDaysExtensionRequest/(:any)/(:any)'] = 'Alerts/raiseDaysExtensionRequest/$1/$1';
$route['submitDaysExtensionRequest'] = 'Alerts/submitDaysExtensionRequest';

// Exceeded Order DurationAlert  Created By Maruthi on 11th Sep 17
$route['exceededOrderDuration'] = 'Alerts/exceededOrderDuration';
$route['exceededOrderDuration/(:any)'] = 'Alerts/exceededOrderDuration/$1';
$route['exceededOrderDurationAction/(:any)'] = 'Alerts/exceededOrderDurationAction/$1';
$route['submitexceededOrderDuration'] = 'Alerts/submitexceededOrderDuration';



// Cal Dashboard - Koushik 17-08-2017	
$route['cal_report'] = 'Dashboard/calibrationDueReport';
$route['getCalDueChart'] = 'Dashboard/getCalDueChart';
$route['getWarehousesByZone'] = 'Dashboard/getWarehousesByZone';
$route['getCalChart2'] = 'Dashboard/getCalChart2';

//ib dashboard - koushik  17-08-2017
$route['ib_dashboard'] = 'Dashboard/ib_dashboard';
$route['get_tool_ib_data'] = 'Dashboard/get_tool_ib_data';
$route['gettool_by_modality'] = 'Dashboard/gettool_by_modality';
$route['get_tool_modality_ib_data'] = 'Dashboard/get_tool_modality_ib_data';

// Inventory Report created by maruthi on 17th Aug'17
$route['tools_report'] = 'Dashboard/toolsReport';
$route['getToolStatusChart2Data'] = 'Dashboard/getToolStatusChart2Data';
$route['getToolStatusChart'] = 'Dashboard/getToolStatusChart';
$route['getToolStatusChart3Data'] = 'Dashboard/getToolStatusChart3Data';
// FE Owned Report created by maruthi on 18th Aug'17
$route['fetools_report'] = 'Dashboard/feOwnedToolsReport';
$route['getFeToolsChart'] = 'Dashboard/getFeToolsChart';
$route['getFeUsersByZone'] = 'Dashboard/getFeUsersByZone';
$route['getFeToolsChart2Data'] = 'Dashboard/getFeToolsChart2Data';


$route['get_supplier_details_by_id'] = 'Calibration/get_supplier_details_by_id';





//delivery_list_to_fe - Koushik 20-07-2017
$route['fe_delivery_list'] = 'Fe_delivery/fe_delivery_list';
$route['fe_delivery_list/(:any)'] = 'Fe_delivery/fe_delivery_list/$1';
$route['generate_fe_delivery/(:any)'] = 'Fe_delivery/generate_fe_delivery/$1';
$route['fe_delivery_asset/(:any)'] = 'Fe_delivery/fe_delivery_asset/$1';
$route['fe_asset_details'] = 'Fe_delivery/fe_asset_details';
$route['insert_fe_delivery_asset'] = 'Fe_delivery/insert_fe_delivery_asset';
$route['insert_fe_delivery'] = 'Fe_delivery/insert_fe_delivery';
$route['edit_generated_fe_details/(:any)'] = 'Fe_delivery/edit_generated_fe_details/$1';
$route['update_fe_delivery'] = 'Fe_delivery/update_fe_delivery';
$route['deactivate_order_delivery_doc'] = 'Fe_delivery/deactivate_order_delivery_doc';
$route['activate_order_delivery_doc'] = 'Fe_delivery/activate_order_delivery_doc';
$route['fe_invoice_print/(:any)'] = 'Fe_delivery/fe_invoice_print/$1';

//Closed request - fe_delivery - koushik 23-07-2017
$route['closed_fe_delivery_list'] = 'Fe_delivery/closed_fe_delivery_list';
$route['closed_fe_delivery_list/(:any)'] = 'Fe_delivery/closed_fe_delivery_list/$1';
$route['view_fe_delivery_details/(:any)'] = 'Fe_delivery/view_fe_delivery_details/$1';

//pickup request - at wh - Koushik 26-07-2017
$route['pickup_list'] = 'Pickup_point/pickup_list';
$route['pickup_list/(:any)'] = 'Pickup_point/pickup_list/$1';
$route['view_pickup_details/(:any)'] = 'Pickup_point/view_pickup_details/$1';
$route['insert_pickup_delivery_details'] = 'Pickup_point/insert_pickup_delivery_details';
$route['pickup_invoice_print/(:any)'] = 'Pickup_point/pickup_invoice_print/$1';

//closed Pickup List at wh -Koushik 04-08-2017
$route['closed_pickup_list'] = 'Pickup_point/closed_pickup_list';
$route['closed_pickup_list/(:any)'] = 'Pickup_point/closed_pickup_list/$1';
$route['view_pickup_detials/(:any)'] = 'Pickup_point/view_pickup_detials/$1';

//fe returns - at wh - koushik 28-07-2017
$route['open_fe_returns'] = 'Pickup_point/open_fe_returns';
$route['open_fe_returns/(:any)'] = 'Pickup_point/open_fe_returns/$1';
$route['acknowledge_fe_return_tools/(:any)'] = 'Pickup_point/acknowledge_fe_return_tools/$1';
$route['submit_acknowledge_fe_returns'] = 'Pickup_point/submit_acknowledge_fe_returns';

// closed fe returns -at wh - koushik 30-07-2017
$route['closed_fe_returns'] = 'Pickup_point/closed_fe_returns';
$route['closed_fe_returns/(:any)'] = 'Pickup_point/closed_fe_returns/$1';
$route['view_closed_fe_returns/(:any)'] = 'Pickup_point/view_closed_fe_returns/$1';

//stock transfer from wh to wh
$route['wh_stock_transfer_list'] = 'Wh_stock_transfer/wh_stock_transfer_list';
$route['wh_stock_transfer_list/(:any)'] = 'Wh_stock_transfer/wh_stock_transfer_list/$1';
$route['st_courier_details/(:any)'] = 'Wh_stock_transfer/st_courier_details/$1';
$route['wh_st_asset/(:any)'] = 'Wh_stock_transfer/wh_st_asset/$1';
$route['scanned_asset_detials'] = 'Wh_stock_transfer/scanned_asset_detials';
$route['insert_scanned_asset'] = 'Wh_stock_transfer/insert_scanned_asset';
$route['insert_wh_st_delivery'] = 'Wh_stock_transfer/insert_wh_st_delivery';
$route['wh_st_invoice_print/(:any)'] = 'Wh_stock_transfer/wh_st_invoice_print/$1';
// created bu maruthi
$route['wh_transfer_info/(:any)/(:any)'] = 'Transfer/wh_transfer_info/$1/$1';
$route['transfer_order_info/(:any)/(:any)'] = 'Transfer/transfer_order_info/$1/$1';

//Closed request - WH TO WH - koushik 23-07-2017
$route['closed_wh_delivery_list'] = 'Wh_stock_transfer/closed_wh_delivery_list';
$route['closed_wh_delivery_list/(:any)'] = 'Wh_stock_transfer/closed_wh_delivery_list/$1';
$route['view_wh_delivery_details/(:any)'] = 'Wh_stock_transfer/view_wh_delivery_details/$1';


// Buffer to Inventory By Srilekha on 3rd August 17 11:27PM
// Modified By Maruthi on 4th August
$route['buffer_to_inventory']='Buffer/buffer_to_inventory';
$route['buffer_to_inventory/(:any)']='Buffer/buffer_to_inventory/$1';
$route['issue_buffer_to_inventory']='Buffer/issue_buffer_to_inventory';
$route['submit_buffer_to_inventory']='Buffer/submit_buffer_to_inventory';
$route['remove_buffer_asset_from_cart'] = 'Buffer/remove_buffer_asset_from_cart';

//Asset Condition Check By Srilekha on 29th July 17 07:19PM
$route['asset_condition_check']='Asset_condition/asset_condition_check';
$route['asset_condition_check/(:any)']='Asset_condition/asset_condition_check/$1';
$route['edit_asset_condition/(:any)']='Asset_condition/edit_asset_condition/$1';
$route['insert_asset_condition']='Asset_condition/insert_asset_condition';

$route['generate_repair_process/(:any)']='Defective_asset/generate_repair_process/$1';
$route['generate_calibration_process/(:any)']='Defective_asset/generate_calibration_process/$1';
$route['generate_scrap_process/(:any)']='Defective_asset/generate_scrap_process/$1';


//Document Type By Srilekha on 28th July 17 4.30PM
$route['document_type'] = 'Document_type/document_type';
$route['document_type/(:any)'] = 'Document_type/document_type/$1';
$route['add_document_type'] = 'Document_type/add_document_type';
$route['insert_document_type'] = 'Document_type/insert_document_type';
$route['edit_document_type/(:any)'] = 'Document_type/edit_document_type/$1';
$route['update_document_type'] = 'Document_type/update_document_type';
$route['deactivate_document_type/(:any)'] = 'Document_type/deactivate_document_type/$1';
$route['activate_document_type/(:any)'] = 'Document_type/activate_document_type/$1';
$route['is_documenttypeExist'] = 'Document_type/is_documenttypeExist';




//Replacement Tool BY Srilekha on 2nd August 17 12:44PM
$route['replace_tool/(:any)'] ='Replacement/replace_tool/$1';
$route['confirm_replacement_tool']='Replacement/confirm_replacement_tool';
$route['submit_replacement']='Replacement/submit_replacement';
$route['closed_replacement_tool']='Replacement/closed_replacement_tool';
$route['closed_replacement_tool/(:any)']='Replacement/closed_replacement_tool/$1';

//scrap request - mastan 26-07-2017
$route['raise_scrap/(:any)'] = 'Scrap/raise_scrap/$1';
$route['insert_scrap'] = 'Scrap/insert_scrap';
$route['closed_scrap_list'] = 'Scrap/closed_scrap_list';
$route['closed_scrap_list/(:any)'] = 'Scrap/closed_scrap_list/$1';
$route['view_scraped_list/(:any)'] = 'Scrap/view_scraped_list/$1';


// FE Calibration Required Tools in Order By Srilekha on 14th August 17 12:04PM
$route['calibration_required_tools']='Calibration/calibration_required_tools';
$route['calibration_required_tools/(:any)']='Calibration/calibration_required_tools/$1';

// admin notification if FE not received with in time created by srilekhs modified by maruthi
$route['crossed_expected_delivery_date']='Alerts/crossed_expected_delivery_date';
$route['crossed_expected_delivery_date/(:any)']='Alerts/crossed_expected_delivery_date/$1';
$route['raisePickupForCalibration/(:any)'] = 'Alerts/raisePickupForCalibration/$1';

// Calibration Report By Srilekha on 17th August 17 12:14PM
$route['calibration_report']='Calibration_report/calibration_report';
$route['calibration_report/(:any)']='Calibration_report/Calibration_report/$1';
$route['get_cr_status']='Calibration_report/get_cr_status';
$route['calibration_download']='Calibration_report/calibration_download';

//tools inventory - koushik 18-08-2017
$route['tools_inventory'] = 'Tools_inventory/tools_inventory';
$route['tools_inventory/(:any)'] = 'Tools_inventory/tools_inventory/$1';
$route['get_asset_status_by_type'] = 'Tools_inventory/get_asset_status_by_type';
$route['get_wh_by_zone'] = 'Tools_inventory/get_wh_by_zone';
$route['download_tools_inventory'] = 'Tools_inventory/download_tools_inventory';

// Srilekha Inventory to Buffer
$route['inventory_to_buffer']='Buffer/inventory_to_buffer';
$route['inventory_to_buffer/(:any)']='Buffer/inventory_to_buffer/$1';
$route['issue_inventory_to_buffer']='Buffer/issue_inventory_to_buffer';
$route['submit_inventory_to_buffer']='Buffer/submit_inventory_to_buffer';


// Srilekha model view for order on 08-09-2017
$route['get_inventory_tools_availability']='Order/get_inventory_tools_availability';


// Repair At Admin By Srilekha
$route['repair_tool']='Repair/repair_tool';
$route['repair_tool/(:any)']='Repair/repair_tool/$1';
$route['add_repair_supplier']='Repair/add_repair_supplier';
$route['insert_repair_supplier']='Repair/insert_repair_supplier';
$route['open_repair_request']='Repair/open_repair_request';
$route['open_repair_request/(:any)']='Repair/open_repair_request/$1';
$route['edit_open_repair_request/(:any)']='Repair/edit_open_repair_request/$1';
$route['deactivate_repair_doc']='Repair/deactivate_repair_doc';
$route['activate_repair_doc']='Repair/activate_repair_doc';
$route['update_repair_request']='Repair/update_repair_request';
$route['view_open_repair_request/(:any)']='Repair/view_open_repair_request/$1';
$route['remove_repair_asset']='Repair/remove_repair_asset';
//repair routes
$route['remove_repair_all_assets/(:any)']='Repair/remove_repair_all_assets/$1';
$route['removerepairToolFromCart']='Repair/removerepairToolFromCart';

//Repair At Acknowledge 
$route['vendor_wh_repair_request']='Repair/vendor_wh_repair_request';
$route['vendor_wh_repair_request/(:any)']='Repair/vendor_wh_repair_request/$1';
$route['edit_vendor_wh_repair_request/(:any)']='Repair/edit_vendor_wh_repair_request/$1';
$route['update_vendor_wh_repair_request']='Repair/update_vendor_wh_repair_request';

$route['vendor_repair_asset_details']='Repair/vendor_repair_asset_details';
$route['edit_admin_repair_request/(:any)']='Repair/edit_admin_repair_request/$1';
$route['update_admin_repair_request']='Repair/update_admin_repair_request';

// Closed Repair Requests
$route['closed_repair_request']='Repair/closed_repair_request';
$route['closed_repair_request/(:any)']='Repair/closed_repair_request/$1';
$route['view_closed_repair_request/(:any)']='Repair/view_closed_repair_request/$1';
$route['closed_repair_delivery_list']='Repair/closed_repair_delivery_list';
$route['closed_repair_delivery_list/(:any)']='Repair/closed_repair_delivery_list/$1';
$route['view_vendor_wh_repair_request/(:any)']='Repair/view_vendor_wh_repair_request/$1';

//wh_repair = koushik
$route['wh_repair'] = 'Repair/wh_repair';
$route['wh_repair/(:any)'] = 'Repair/wh_repair/$1';
$route['wh_repair_wizard/(:any)'] = 'Repair/wh_repair_wizard/$1';
$route['scan_repair_asset/(:any)'] = 'Repair/scan_repair_asset/$1';
$route['scanned_repair_asset_detials'] = 'Repair/scanned_repair_asset_detials';
$route['insert_scanned_repair_asset'] = 'Repair/insert_scanned_repair_asset';
$route['insert_wh_repair_wizard'] = 'Repair/insert_wh_repair_wizard';
$route['repair_invoice_print/(:any)'] = 'Repair/repair_invoice_print/$1';
//cancel fe print
$route['cancel_repair_print/(:any)'] = 'Repair/cancel_repair_print/$1';
$route['insert_cancel_repair_print'] = 'Repair/insert_cancel_repair_print';
$route['update_repair_print/(:any)'] = 'Repair/update_repair_print/$1';
$route['update_repair_print_details'] = 'Repair/update_repair_print_details';


//Calibration By Srilekha on 19th July 17 11.27AM
$route['calibration']='Calibration/calibration_list';
$route['calibration/(:any)']='Calibration/calibration_list/$1';
$route['calibration_asset_details']='Calibration/calibration_asset_details';
$route['insert_calibration_request']='Calibration/insert_calibration_request';
$route['removeToolFromCart']='Calibration/removeToolFromCart';
// Admin Calibration List
$route['admin_calibration_list']='Calibration/admin_calibration_list';
$route['admin_calibration_list/(:any)']='Calibration/admin_calibration_list/$1';
$route['edit_admin_calibration/(:any)']='Calibration/edit_admin_calibration/$1';
$route['update_admin_calibration']='Calibration/update_admin_calibration';
$route['view_admin_calibration/(:any)']='Calibration/view_admin_calibration/$1';
$route['remove_admin_calibration/(:any)']='Calibration/remove_admin_calibration/$1';
$route['remove_calibration_assets']='Calibration/remove_calibration_assets';
$route['remove_asset']='Calibration/remove_asset';


//Calibration At Vendor By Srilekha
$route['vendor_calibration']='Calibration/vendor_calibration';
$route['vendor_calibration/(:any)']='Calibration/vendor_calibration/$1';
$route['edit_vendor_calibration_list/(:any)']='Calibration/edit_vendor_calibration_list/$1';
$route['vendor_asset_details']='Calibration/vendor_asset_details';
$route['update_vendor_calibration_request']='Calibration/update_vendor_calibration_request';
$route['edit_open_calibration/(:any)']='Calibration/edit_open_calibration/$1';
$route['update_calibration_request']='Calibration/update_calibration_request';
$route['vendor_calibration_delivery_view/(:any)']='Calibration/vendor_calibration_delivery_view/$1';


//Calibration At QC By Srilekha
$route['qc_calibration']='Calibration/qc_calibration';
$route['qc_calibration/(:any)']='Calibration/qc_calibration/$1';
$route['open_qc_calibration_list/(:any)']='Calibration/open_qc_calibration_list/$1';
$route['update_qc_calibration_list']='Calibration/update_qc_calibration_list';
$route['view_qc_calibration_list/(:any)']='Calibration/view_qc_calibration_list/$1';
// Calibration 13-09-2017
$route['closed_calibration']='Calibration/closed_calibration';
$route['closed_calibration/(:any)']='Calibration/closed_calibration/$1';
$route['closed_calibration_view/(:any)']='Calibration/closed_calibration_view/$1';
//closed delivery list calibration - srilekha 
$route['closed_cal_delivery_list']='Calibration/closed_cal_delivery_list';
$route['closed_cal_delivery_list/(:any)']='Calibration/closed_cal_delivery_list/$1';
$route['view_vendor_calibration_list/(:any)']='Calibration/view_vendor_calibration_list/$1';
$route['closed_qc_calibration_list']='Calibration/closed_qc_calibration_list';
$route['closed_qc_calibration_list/(:any)']='Calibration/closed_qc_calibration_list/$1';
$route['view_closed_qc_calibration_list/(:any)']='Calibration/view_closed_qc_calibration_list/$1';
$route['deactivate_calibration_delivery_doc']='Calibration/deactivate_calibration_delivery_doc';
$route['activate_calibration_delivery_doc']='Calibration/activate_calibration_delivery_doc';
$route['get_supplier_details_by_id']='Calibration/get_supplier_details_by_id';

//wh_calibration = koushik
$route['wh_calibration'] = 'Calibration/wh_calibration';
$route['wh_calibration/(:any)'] = 'Calibration/wh_calibration/$1';
$route['wh_calibration_wizard/(:any)'] = 'Calibration/wh_calibration_wizard/$1';
$route['scan_cal_asset/(:any)'] = 'Calibration/scan_cal_asset/$1';
$route['scanned_cal_asset_detials'] = 'Calibration/scanned_cal_asset_detials';
$route['insert_scanned_cal_asset'] = 'Calibration/insert_scanned_cal_asset';
$route['insert_wh_cal_wizard'] = 'Calibration/insert_wh_cal_wizard';
$route['cal_invoice_print/(:any)'] = 'Calibration/cal_invoice_print/$1';
//cancel fe print
$route['cancel_cal_print/(:any)'] = 'Calibration/cancel_cal_print/$1';
$route['insert_cancel_cal_print'] = 'Calibration/insert_cancel_cal_print';
$route['update_cal_print/(:any)'] = 'Calibration/update_cal_print/$1';
$route['update_cal_print_details'] = 'Calibration/update_cal_print_details';

// Admin Approves FE Position
$route['fe_position_change']='Fe_position_change/fe_position_change';
$route['fe_position_change/(:any)']='Fe_position_change/fe_position_change/$1';
$route['approve_fe_position_change']='Fe_position_change/approve_fe_position_change';
$route['reject_fe_position/(:any)']='Fe_position_change/reject_fe_position/$1';

//cancel fe print
$route['cancel_fe_print/(:any)'] = 'Fe_delivery/cancel_fe_print/$1';
$route['insert_cancel_fe_print'] = 'Fe_delivery/insert_cancel_fe_print';
$route['update_fe_print/(:any)'] = 'Fe_delivery/update_fe_print/$1';
$route['update_fe_print_details'] = 'Fe_delivery/update_fe_print_details';

//cancel pickup print
$route['cancel_pickup_print/(:any)'] = 'Pickup_point/cancel_pickup_print/$1';
$route['insert_cancel_pickup_print'] = 'Pickup_point/insert_cancel_pickup_print';
$route['update_pickup_print/(:any)'] = 'Pickup_point/update_pickup_print/$1';
$route['update_pickup_print_details'] = 'Pickup_point/update_pickup_print_details';

//cancel wh print
$route['cancel_wh_print/(:any)'] = 'Wh_stock_transfer/cancel_wh_print/$1';
$route['insert_cancel_wh_print'] = 'Wh_stock_transfer/insert_cancel_wh_print';
$route['update_wh_print/(:any)'] = 'Wh_stock_transfer/update_wh_print/$1';
$route['update_wh_print_details'] = 'Wh_stock_transfer/update_wh_print_details';

$route['get_fe_warehouse']='Fe_position_change/get_fe_warehouse';
$route['faq']='Faq/faq';
$route['raise_faq']='Faq/raise_faq';

// Calibration Certificates report By Srilekha on 20-09-2017
$route['calibration_certificates']='Calibration_certificates/calibration_certificates';
$route['calibration_certificates/(:any)']='Calibration_certificates/calibration_certificates/$1';
$route['view_calibration_certificates_list/(:any)']='Calibration_certificates/view_calibration_certificates_list/$1';

/*created_by_koushik 24-05-2018*/
$route['view_calibration_certificates_list/(:any)/(:any)'] = 'Calibration_certificates/view_calibration_certificates_list/$1/$2';

// Cancelled Invoice Report By Srilekha on 21-09-2017
$route['cancelled_invoice']='Cancelled_invoice/cancelled_invoice';
$route['cancelled_invoice/(:any)']='Cancelled_invoice/cancelled_invoice/$1';
$route['view_cancelled_invoice/(:any)']='Cancelled_invoice/view_cancelled_invoice/$1';

// tool utilization report created by maruthi on 23rd sep
$route['toolUtilization'] = 'Tools_inventory/toolUtilization';
$route['toolUtilization/(:any)'] = 'Tools_inventory/toolUtilization/$1';

$route['closed_wh_transfer_info/(:any)/(:any)'] = 'Transfer/closed_wh_transfer_info/$1/$2';
//invoice_report - koushik(10-10-2017)
$route['invoice_report'] = 'Invoice_report/invoice_report';
$route['wh_by_state'] = 'Invoice_report/wh_by_state';
$route['download_invoice_pdf'] = 'Invoice_report/download_invoice_pdf';
$route['download_invoice_excel'] = 'Invoice_report/download_invoice_excel';

$route['bulk_city'] = 'Bulk/bulk_city';
$route['insert_bulkupload_city'] = 'Bulk/insert_bulkupload_city';


#  routes
$routes['bulkAssetStatus'] ='Bulk/bulkAssetStatus';
$routes['insertBulkAssetStatus'] ='Bulk/insertBulkAssetStatus';


#$routes['bulkAssetStatus'] ='Upload/bulkAssetStatus';
#$routes['insertBulkAssetStatus'] ='Upload/insertBulkAssetStatus';

$route['bulk_designation'] = 'Bulk/bulk_designation';
$route['insert_bulkupload_designation'] = 'Bulk/insert_bulkupload_designation';

$route['bulk_modality'] = 'Bulk/bulk_modality';
$route['insert_bulkupload_modality'] = 'Bulk/insert_bulkupload_modality';

$route['bulk_supplier'] = 'Bulk/bulk_supplier';
$route['insert_bulkupload_supplier'] = 'Bulk/insert_bulkupload_supplier';

$route['bulk_warehouse'] = 'Bulk/bulk_warehouse';
$route['insert_bulkupload_warehouse'] = 'Bulk/insert_bulkupload_warehouse';

$route['bulk_branch'] = 'Bulk/bulk_branch';
$route['insert_bulkupload_branch'] = 'Bulk/insert_bulkupload_branch';

$route['assets_calibration_report']='Dashboard/assets_calibration_report';
$route['get_in_calibration_assets_Chart']='Dashboard/get_in_calibration_assets_Chart';
$route['get_in_calibration_Chart2Data']='Dashboard/get_in_calibration_Chart2Data';
$route['download_field_deployed_excel']='Dashboard/download_field_deployed_excel';
//invoice_report - koushik(10-10-2017)
$route['download_report'] = 'Download_report/download_report';
$route['wh_by_state_dropdown'] = 'Download_report/wh_by_state_dropdown';
$route['download_report_format'] = 'Download_report/download_report_format';

// XL Downloads
// OPen Orders XL Downloads - Created By Maruthi on 5th december
$route['downloadOpenOrders'] = 'XL_downloads/downloadOpenOrders';
$route['downloadClosedOrders'] = 'XL_downloads/downloadClosedOrders';

//new routes - koushik 21-03-2018
$route['delete_cr_request/(:any)'] = 'Calibration/delete_cr_request/$1';
$route['downloadtoolreturn'] = 'Toolreturn/downloadtoolreturn';
$route['cancel_return_request/(:any)'] = 'Order/cancel_return_request/$1';
$route['submit_cancel_return_request/(:any)'] = 'Order/submit_cancel_return_request/$1';
$route['delete_repair_cr_request/(:any)']='Repair/delete_repair_cr_request/$1';

// created by maruthi on 21stApril'18
$route['cancelSTForOrder/(:any)'] = 'Stock_transfer/cancelSTForOrder/$1';
$route['cancelChildSTForOrder/(:any)'] = 'Stock_transfer/cancelChildSTForOrder/$1';

// created by maruthi on 22ndApril'18
$route['receive_st_fe_order_details/(:any)'] = 'Order/receive_st_fe_order_details/$1';
$route['insert_st_fe_received_order'] = 'Order/insert_st_fe_received_order';

// created by maruthi on 24th April'18
$route['closed_receive_order'] = 'Order/closed_receive_order';
$route['closed_receive_order/(:any)'] = 'Order/closed_receive_order/$1';
$route['closed_receive_st_fe_order_details/(:any)'] ='Order/closed_receive_st_fe_order_details/$1';

// created by maruthi o 25th April'18
$route['unlock_asset/(:any)'] = 'Special_cases/unlock_asset/$1';

//gowri
$route['sub_task']='Task/sub_task';
$route['sub_task/(:any)']='Task/sub_task/$1';
$route['add_task']='Task/add_task';
$route['insert_task']='Task/insert_task';
$route['edit_task/(:any)']='Task/edit_task/$1';
$route['update_task']='Task/update_task';
$route['is_tasknameExist']='Task/is_tasknameExist';
$route['deactivate_task/(:any)']='Task/deactivate_task/$1';
$route['activate_task/(:any)']='Task/activate_task/$1';

//Roles
$route['role_page']='Role/role_page';
$route['role_page/(:any)']='Role/role_page/$1';
$route['add_role']='Role/add_role';
$route['insert_role']='Role/insert_role';
$route['edit_role/(:any)']='Role/edit_role/$1';
$route['update_role']='Role/update_role';
$route['is_role_exist']='Role/is_role_exist';
$route['deactivate_role/(:any)']='Role/deactivate_role/$1';
$route['activate_role/(:any)']='Role/activate_role/$1';
$route['role_task/(:any)']='Role/role_task/$1';
$route['role_task/(:any)/(:any)']='Role/role_task/$1/$2';
$route['insert_assign_task']='Role/insert_assign_task';
$route['role_task_assign/(:any)']='Role/role_task_assign/$1';
$route['role_task_assign/(:any)/(:any)']='Role/role_task_assign/$1/$2';
$route['insert_role_task']='Role/insert_role_task';
$route['role_login/(:any)']='Role/role_login/$1';
$route['role_login/(:any)/(:any)']='Role/role_login/$1/$2';
$route['insert_role_login']='Role/insert_role_login';

//Page
$route['task']='Page/task';
$route['task/(:any)']='Page/task/$1';
$route['add_page']='Page/add_page';
$route['insert_page']='Page/insert_page';
$route['edit_page/(:any)']='Page/edit_page/$1';
$route['update_page']='Page/update_page';
$route['is_page_exist']='Page/is_page_exist';
$route['deactivate_page/(:any)']='Page/deactivate_page/$1';
$route['activate_page/(:any)']='Page/activate_page/$1';

#created by koushik
$route['countries/(:any)'] = 'Login/countries/$1';
$route['get_state_by_country_dropdown'] = 'Download_report/get_state_by_country_dropdown';

#pending_fe_transfer_request
$route['fe_transfer_request'] = 'Fe2fe/fe_transfer_request';
$route['fe_transfer_request/(:any)'] = 'Fe2fe/fe_transfer_request/$1';

#13-06-2018
$route['gps_report_view'] = 'Gps_tracking_report/gps_report_view';
$route['gps_report_view/(:any)'] = 'Gps_tracking_report/gps_report_view/$1';
$route['download_gps_report'] = 'Gps_tracking_report/download_gps_report';
#gps api
$route['gps_tools_list'] = 'Gps_api/gps_assets';
$route['user_country/(:any)'] = 'User/user_country/$1';
$route['insert_user_country'] = 'User/insert_user_country';

#created by prasad
$route['ajax_get_warehouse_by_country_id']='Buffer/ajax_get_warehouse_by_country_id';
$route['getWarehousesByCountry']='Dashboard/getWarehousesByCountry';
$route['getWarehousesToolsByCountry'] = 'Dashboard/getWarehousesToolsByCountry';
$route['get_fe_users_by_country'] ='Dashboard/get_fe_users_by_country';

#created by srilekha
$route['get_country_based_tools'] = 'Asset/get_country_based_tools';
$route['get_calibration_country_based_tools'] = 'Dashboard/get_calibration_country_based_tools';
$route['get_ajax_tools'] = 'Dashboard/get_ajax_tools';
$route['get_tool_master_tools'] = 'Tool/get_tool_master_tools';
$route['get_asset_master_tools'] = 'Asset/get_asset_master_tools';
$route['get_ib_tools'] = 'Dashboard/get_ib_tools';
#gps master page
$route['gps_tracking'] = 'GPS/gps_tracking';
$route['gps_tracking/(:any)'] = 'GPS/gps_tracking/$1';
$route['add_gps_tracking'] = 'GPS/add_gps_tracking';
$route['insert_gps_tracking'] = 'GPS/insert_gps_tracking';
$route['edit_gps_tracking/(:any)'] = 'GPS/edit_gps_tracking/$1';
$route['update_gps_tracking'] = 'GPS/update_gps_tracking';
$route['deactivate_gps_tracking/(:any)'] = 'GPS/deactivate_gps_tracking/$1';
$route['activate_gps_tracking/(:any)'] = 'GPS/activate_gps_tracking/$1';
$route['is_gps_uid_exist'] = 'GPS/is_gps_uid_exist';

#created by maruthi
$route['ajax_get_users_by_country_id']= 'Ajax_ci/ajax_get_users_by_country_id';

#new routes
$route['assign_country_region/(:any)/(:any)'] = 'Location/assign_country_region/$1/$2';
$route['assign_user_country'] = 'User/assign_user_country';
$route['asset_position_data']='Asset_position/asset_position_data';
$route['asset_position_data/(:any)']='Asset_position/asset_position_data/$1';
$route['asset_position_view/(:any)']='Asset_position/asset_position_view/$1';
$route['download_tools_utilization'] = 'Tools_inventory/download_tools_utilization';

$route['update_asset_position/(:any)/(:any)/(:any)/(:any)']= 'Special_cases/update_asset_position/$1/$2/$3/$4';

$route['check_with_sso'] = 'Fe2fe/check_with_sso';

# Special Routes
$route['unlock_transit_asset/(:any)'] = 'Special_cases/unlock_transit_asset/$1';
$route['get_data/(:any)'] = 'Special_cases/get_data/$1';
$route['get_data/(:any)/(:any)/(:any)'] = 'Special_cases/get_data/$1/$2/$3';
$route['get_encoded_value/(:any)'] = 'Special_cases/get_encoded_value/$1';
$route['get_decoded_value/(:any)'] = 'Special_cases/get_decoded_value/$1';
$route['do_action/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Special_cases/do_action/$1/$2/$3/$4/$5';
$route['do_action/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Special_cases/do_action/$1/$2/$3/$4/$5/$6/$7';
$route['run_scheduler/(:any)'] = 'Special_cases/run_scheduler/$1';
$route['run_scheduler/(:any)/(:any)'] = 'Special_cases/run_scheduler/$1/$2';


//new routes 04-08-20/18
$route['unlink_scanned_assets/(:any)'] = 'Fe_delivery/unlink_scanned_assets/$1';
$route['unlink_st_scanned_assets/(:any)'] = 'Wh_stock_transfer/unlink_st_scanned_assets/$1';
$route['unlink_cal_scanned_asset/(:any)'] = 'Calibration/unlink_cal_scanned_asset/$1';
$route['unlink_repair_scanned_asset/(:any)'] = 'Repair/unlink_repair_scanned_asset/$1';

// Enhacements routes
//masters
$route['download_user'] = 'XL_downloads/download_user';
$route['download_tool'] = 'XL_downloads/download_tool';
$route['download_install_base'] = 'XL_downloads/download_install_base';
$route['download_install_base_xlsx'] = 'XL_downloads/download_install_base_xlsx';
$route['download_supplier'] = 'XL_downloads/download_supplier';
$route['download_branch'] = 'XL_downloads/download_branch';
$route['download_branch_office'] = 'XL_downloads/download_branch_office';
$route['download_modality'] = 'XL_downloads/download_modality';
$route['download_eq_model'] = 'XL_downloads/download_eq_model';
$route['download_country'] = 'XL_downloads/download_country';
$route['download_country'] = 'XL_downloads/download_country';
$route['download_zone'] = 'XL_downloads/download_zone';
$route['download_state'] = 'XL_downloads/download_state';
$route['download_city'] = 'XL_downloads/download_city';
$route['download_area'] = 'XL_downloads/download_area';
$route['download_document_type'] = 'XL_downloads/download_document_type';
$route['download_designation'] = 'XL_downloads/download_designation';
$route['download_gps'] = 'XL_downloads/download_gps';
//stock_transfer
$route['download_ST'] = 'Stock_transfer/download_ST';

$route['fe_delivery_admin'] = 'Fe_delivery/fe_delivery_admin';
$route['fe_delivery_admin/(:any)'] = 'Fe_delivery/fe_delivery_admin/$1';

// created by maruthi on 5thSep'18
$route['initiatePartialShipment/(:any)'] = 'Stock_transfer/initiatePartialShipment/$1';
$route['insertPartiallyAssignedSTtools'] = 'Stock_transfer/insertPartiallyAssignedSTtools';

// created by gowri on 7th sep 
$route['get_wh_by_zone_user'] = 'User/get_wh_by_zone_user';

#koushik 20-09-2018
$route['get_reporting_manager_list'] = 'User/get_reporting_manager_list';
$route['get_fe_position_list'] = 'User/get_fe_position_list';

$route['unlink_scanned_assets_individually/(:any)/(:any)'] = 'Fe_delivery/unlink_scanned_assets_individually/$1/$2';

$route['unlink_st_scanned_assets_individually/(:any)/(:any)'] = 'Wh_stock_transfer/unlink_st_scanned_assets_individually/$1/$2';

$route['unlink_cal_scanned_asset_individually/(:any)/(:any)'] = 'Calibration/unlink_cal_scanned_asset_individually/$1/$2';

$route['unlink_repair_scanned_asset_individually/(:any)/(:any)'] = 'Repair/unlink_repair_scanned_asset_individually/$1/$2';

#change asset to scrap -koushik 21-09-2018
$route['raise_scrap_asset/(:any)'] = 'Scrap/raise_scrap_asset/$1';
$route['insert_scrap_asset'] = 'Scrap/insert_scrap_asset';
#created by maruthi on 8thOct'18
$route['closeShipmentAsPS/(:any)']= 'Order/closeShipmentAsPS/$1';

#update stock transfer - koushik (10-10-2018)
$route['update_transfer_list/(:any)'] = 'Transfer/update_transfer_list/$1';
$route['update_transfer_list/(:any)/(:any)'] = 'Transfer/update_transfer_list/$1/$2';
#change status to 1 koushik (11-10-2018)
$route['change_status_to_at_wh/(:any)'] = 'Asset/change_status_to_at_wh/$1';


$route['download_open_st'] = 'Stock_transfer/download_open_st';
$route['download_closed_st'] = 'Stock_transfer/download_closed_st';

// Nov and December Enhancements
$route['ajax_get_countries_by_sso_id'] = 'Ajax_ci/ajax_get_countries_by_sso_id';
#audit trail data view - koushik (12-11-2018)
$route['get_audit_page/(:any)'] = 'Audit_trail/get_audit_page/$1';
$route['get_audit_page/(:any)/(:any)'] = 'Audit_trail/get_audit_page/$1/$2';
$route['download_audit/(:any)'] = 'Audit_trail/download_audit/$1';
$route['get_audit_drop_down'] = 'Audit_trail/get_audit_drop_down';
$route['ajax_audit_report'] = 'Audit_trail/ajax_audit_report';
$route['ajax_audit_report/(:any)'] = 'Audit_trail/ajax_audit_report/$1';

//raise pickup
$route['pickup_sso_dropdown'] = 'Order/pickup_sso_dropdown';
$route['check_order_matching'] = 'Order/check_order_matching';

$route['assign_asean_as_default_country'] = 'User/assign_asean_as_default_country';
$route['sso_dropdown_list'] = 'User/sso_dropdown_list';
$route['all_sso_dropdown_list'] = 'User/all_sso_dropdown_list';
$route['onbehalf_fe_ajax'] = 'User/onbehalf_fe_ajax';

$route['get_information'] = 'Special_cases/get_information';
$route['get_information_data'] = 'Special_cases/get_information_data';

$route['download_open_fe_returns'] = 'Pickup_point/download_open_fe_returns';
$route['download_open_st_returns'] = 'Stock_transfer/download_open_st_returns';
$route['download_ack_from_fe'] = 'Fe2fe/download_ack_from_fe';

$route['downloadAckFromWHOrders'] = 'XL_downloads/downloadAckFromWHOrders';
$route['downloadReturnOrders'] = 'XL_downloads/downloadReturnOrders';

$route['updateLockinAtWhToInitial'] ='Special_cases/updateLockinAtWhToInitial';
$route['updateLockinAtWhToInitialEndTime'] ='Special_cases/updateLockinAtWhToInitialEndTime';

$route['getAssetsWithIssues'] = 'Special_cases/getAssetsWithIssues';
$route['getAssetsWithIssues/(:any)'] = 'Special_cases/getAssetsWithIssues/$1';
$route['getAssetsWithIssues/(:any)/(:any)'] = 'Special_cases/getAssetsWithIssues/$1/$1';

$route['bulkAssetCalibrationUpdate'] = 'Bulk/bulkAssetCalibrationUpdate';
$route['updateBulkAssetCalibration'] = 'Bulk/updateBulkAssetCalibration';
$route['getFEtoFEIssues/(:any)'] = 'Special_cases/getFEtoFEIssues/$1';

$route['egm_calibration/(:any)'] = 'Asset/egm_calibration/$1';
$route['submit_egm_calibration'] = 'Asset/submit_egm_calibration';

//EGM Calibration Report
$route['egm_calibration_report']='Calibration_report/egm_calibration_report';
$route['egm_calibration_report/(:any)']='Calibration_report/egm_calibration_report/$1';
$route['egm_calibration_download']='Calibration_report/egm_calibration_download';

//EGM Cal Dashboard
$route['egm_calibrationDueReport'] = 'Dashboard/egm_calibrationDueReport';
$route['egm_getCalDueChart'] = 'Dashboard/egm_getCalDueChart';
$route['egm_getCalChart2'] = 'Dashboard/egm_getCalChart2';