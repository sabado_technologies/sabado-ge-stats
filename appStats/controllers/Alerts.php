<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By Maruthi on 20th july 17 8:30PM

class Alerts extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Alerts_m');
        $this->load->model('Order_m');
        $this->load->model('Calibration_m');
    }
    
    public function address_change()
    {
        $data['nestedView']['heading']="Address Change";
        $data['nestedView']['cur_page'] = 'address_change';
        $data['nestedView']['parent_page'] = 'address_change';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Address Change';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Address Change','class'=>'active','url'=>'');

        # Search Functionality       
        $psearch=validate_string($this->input->post('address_change', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'order_number'  => validate_string($this->input->post('order_number', TRUE)),
                'return_number' => validate_string($this->input->post('return_number', TRUE)),
                'request_type'  => validate_string($this->input->post('request_type', TRUE)),
                'deploy_date'   => validate_string($this->input->post('deploy_date', TRUE)),
                'return_date'   => validate_string($this->input->post('return_date', TRUE)),
                'a_sso_id'      => validate_string(@$this->input->post('sso_id',TRUE)),
                'country_id'    => validate_number(@$this->input->post('country_id',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'order_number'  => $this->session->userdata('order_number'),
                    'return_number' => $this->session->userdata('return_number'),
                    'deploy_date'   => $this->session->userdata('deploy_date'),
                    'request_type'  => $this->session->userdata('request_type'),
                    'return_date'   => $this->session->userdata('return_date'),
                    'a_sso_id'      => $this->session->userdata('a_sso_id'),
                    'country_id'    => $this->session->userdata('country_id')
                );
            }
            else 
            {
                $searchParams=array(
                    'order_number'  => '',
                    'return_number' => '',
                    'deploy_date'   => '',
                    'request_type'  => 1,
                    'return_date'   => '',
                    'a_sso_id'      => '',
                    'country_id'    => ''
                );
                $this->session->set_userdata($searchParams);
            }            
        }
        
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'address_change/';

        # Total Records
        $config['total_rows'] = $this->Alerts_m->addressChangeTotalNumRows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Alerts_m->addressChangeResults($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $this->load->view('alerts/address_change',$data);
    }

    public function order_address_change()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'address_change');
            exit;
        }        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Change Order Address";
        $data['nestedView']['cur_page'] = 'order_address_change_second';
        $data['nestedView']['parent_page'] = 'address_change';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Change Order Address';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Address','class'=>'','url'=>SITE_URL.'address_change');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Change Order Address','class'=>'active','url'=>'');

        $data['tools'] =  $this->Order_m->get_ordered_tools($tool_order_id);
        $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);        
        $attach_document = $this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document; 
        $data['flg'] = 1;
        $data['customer_info'] = $this->Common_model->get_data_row('customer_site',array('site_id'=>$data['order_info']['site_id']));
        $this->load->view('alerts/address_change',$data);
    }

    public function return_address_change()
    {
        $return_order_id=@storm_decode($this->uri->segment(2));
        if($return_order_id=='')
        {
            redirect(SITE_URL.'address_change');
            exit;
        }        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Change Return  Address";
        $data['nestedView']['cur_page'] = 'order_address_change';
        $data['nestedView']['parent_page'] = 'address_change';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Change Return Address';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Address','class'=>'','url'=>SITE_URL.'order_address');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Change Return Address','class'=>'active','url'=>'');
        $data['tools'] =  $this->Alerts_m->get_return_initiated_assets($return_order_id);
        $data['flg'] = 1;
        $data['customer_info'] = $this->Common_model->get_data_row('customer_site',array('site_id'=>$data['tools'][0]['site_id']));
        # Data
        $this->load->view('alerts/ro_address_change',$data);
    }

     public function submit_order_address_change()
    {
        #page authentication
        $task_access = page_access_check('address_change');
        $ac_submit = validate_string($this->input->post('ac_submit',TRUE));
        $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
        $site_id = validate_string($this->input->post('site_id',TRUE));
        $address1 = validate_string($this->input->post('address1',TRUE));
        $address2 = validate_string($this->input->post('address2',TRUE));
        $address3 = validate_string($this->input->post('address3',TRUE));
        $address4 = validate_string($this->input->post('address4',TRUE));
        $pin_code = validate_string($this->input->post('pin_code',TRUE));
        $remarks = validate_string($this->input->post('remarks',TRUE));
        $location_id  = validate_number($this->input->post('location_id',TRUE));
        $tool_order_data = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $country = $this->Alerts_m->get_country_from_to($tool_order_id);
        $country_id =$country['country_id'];
        $oa_arr = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id,'status'=>1));
        $install_base_id = $oa_arr['install_base_id'];
        // updating new address in customer site and inserting in customer site history
        if($ac_submit == 1)
        { 
            // updating status in order status  and updating status in tool order table
            $customer_site_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'customer_site_id');

            $customerSiteData = $this->Common_model->get_data_row('customer_site',array('customer_site_id'=>$customer_site_id,'country_id'=>$country_id));
            $up_arr = array(
                'location_id'   => @$location_id,
                'address1'      => @$address1,
                'address2'      => @$address2,
                'address3'      => @$address3,
                'address4'      => @$address4,
                'zip_code'      => @$pin_code,
                'modified_by'   => $this->session->userdata('sso_id'), 
                'modified_time' => date('Y-m-d H:i:s') 
            );

            $this->Common_model->update_data('customer_site',$up_arr,array('customer_site_id'=>$customer_site_id));

            $oldArr = array(
                'address1'      => $customerSiteData['address1'],
                'address2'      => $customerSiteData['address2'],
                'address3'      => $customerSiteData['address3'],
                'address4'      => $customerSiteData['address4'],
                'zip_code'      => $customerSiteData['zip_code']
            );

            $newArr = array(
                'address1'      => @$address1,
                'address2'      => @$address2,
                'address3'      => @$address3,
                'address4'      => @$address4,
                'zip_code'      => @$pin_code
            );            
            
            // updating and inserting in customer site historry
            $cs_history_id = $this->Common_model->get_value('cs_history',array('customer_site_id'=>$customer_site_id,'end_time'=>NULL),'cs_history_id');
            // updating cs history
            $this->Common_model->update_data('cs_history',array('end_time'=>date('Y-m-d H:i:s')),array('cs_history_id'=>$cs_history_id));
            // inserting into customer site hoistory
            unset($up_arr['modified_by']);
            unset($up_arr['modified_time']);
            $up_arr['customer_site_id'] = $customer_site_id;
            $up_arr['created_by'] = $this->session->userdata('sso_id');
            $this->Common_model->insert_data('cs_history',$up_arr);

            # Tool Order Audit Start            
            $statusArr = array('current_stage_id'=>4,'tool_order_id'=>$tool_order_id);
            $oldArr['current_stage'] = getOpenOrderStatus($statusArr);
            // updating the order addres
            
            $orderAddressRemarks  = $oa_arr['remarks'];
            $this->Common_model->update_data('order_address',array('status'=>1,'check_address'=>0,'remarks' => $remarks),array('tool_order_id'=>$tool_order_id));
            $oldArr['address_remarks'] = $orderAddressRemarks;
            $newArr['address_remarks'] = $remarks;
            $newArr['current_stage'] =  getOpenOrderStatus($statusArr);
            $blockArr = array('blockArr'=>'Tool Order');
            $remarksString = "Address Change Request for Order Number : ".$tool_order_data['order_number']." is Approved";
            $ad_id = tool_order_audit_data('tool_order',$tool_order_id,'tool_order',2,'',$newArr,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$tool_order_id,$tool_order_data['country_id']);
        }  
        else
        { 
            // updating the Order Address To Customer Site Address Based On Site ID
            $customer_site_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'customer_site_id');

            $cs_data = $this->Common_model->get_data_row('customer_site',array('customer_site_id'=>$customer_site_id,'country_id'=>$country_id));
            $u_data = array(
                'address1'  => $cs_data['address1'],
                'address2'  => $cs_data['address2'],
                'address3'  => $cs_data['address3'],
                'address4'  => $cs_data['address4'],
                'pin_code'  => $cs_data['zip_code'],
                'remarks'   => $remarks,
            );
            $this->Common_model->update_data('order_address',$u_data,array('tool_order_id'=>$tool_order_id));
            // updating status in order status  and updating status in tool order table
            
            $statusArr = array('current_stage_id'=>4,'tool_order_id'=>$tool_order_id);
            $oldArr['current_stage'] = getOpenOrderStatus($statusArr);
            # Transactoin Entry
            $this->Common_model->update_data('order_address',array('status'=>1,'check_address'=>0),array('tool_order_id'=>$tool_order_id));
            $newArr['current_stage'] =  getOpenOrderStatus($statusArr);
            $blockArr = array('blockArr'=>'Tool Order');
            $remarksString = "Address Change Request for Order Number :".$tool_order_data['order_number']." is Rejected";
            $ad_id = tool_order_audit_data('tool_order',$tool_order_id,'tool_order',2,'',$newArr,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$tool_order_id,$tool_order_data['country_id']);
        }

        // don't check the tool availability condition if fe check is enabled 
        if($tool_order_data['fe_check'] == 0)
        {
            $order_status = $this->Common_model->get_data('ordered_tool',array('status'=>2,'tool_order_id'=>$tool_order_id));
            if(count($order_status) == 0)
            {
                $updatingAuditData  = array('current_stage'=> "At WH");
                updatingAuditData($ad_id,$updatingAuditData);
                $this->Common_model->update_data('tool_order',array('current_stage_id'=>5),array('tool_order_id'=>$tool_order_id));
                $order_status_history_data = array(
                    'current_stage_id'  => 5,
                    'tool_order_id'     => $tool_order_id,
                    'remarks'           => '',
                    'created_by'        => $this->session->userdata('sso_id'),
                    'created_time'      => date('Y-m-d H:i:s'),
                    'status'            =>  1, 
                );
                $this->Common_model->insert_data('order_status_history',$order_status_history_data);                
            }
        }
        if($this->db->trans_status === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'address_change'); exit();
        }
        else
        {   
            $transactionType = ($ac_submit == 1)?"Approved":"Rejected";
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Address Change Request has been '.$transactionType.' successfully for Order No : <strong>'.$tool_order_data['order_number'].'</strong>!</div>');
            redirect(SITE_URL.'address_change'); exit();
        }
    }
    
    public function submit_return_address_change()
    {
        #page authentication
        $task_access = page_access_check('address_change');
        $ac_submit = validate_string($this->input->post('ac_submit',TRUE));
        $return_order_id = validate_number($this->input->post('return_order_id',TRUE));
        $site_id = validate_string($this->input->post('site_id',TRUE));
        $address1 = validate_string($this->input->post('address1',TRUE));
        $address2 = validate_string($this->input->post('address2',TRUE));
        $address3 = validate_string($this->input->post('address3',TRUE));
        $address4 = validate_string($this->input->post('address4',TRUE));
        $pin_code = validate_string($this->input->post('pin_code',TRUE));
        $remarks = validate_string($this->input->post('remarks',TRUE));
        $location_id  = validate_number($this->input->post('location_id',TRUE));

        $returnOrderData = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        $install_base_id = $returnOrderData['install_base_id'];
        $country_id = $returnOrderData['country_id'];
        $rtoData = $this->Common_model->get_data_row('return_tool_order',array('return_order_id'=>$return_order_id));
        $rto_id = $rtoData['rto_id'];
        $tool_order_id = $rtoData['tool_order_id'];
        $orderInfo = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        // updating new address in customer site and inserting in customer site history
        if($ac_submit == 1)
        { 
            
            // updating status in order status  and updating status in tool order table
            $customer_site_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'customer_site_id');
            $customerSiteData = $this->Common_model->get_data_row('customer_site',array('customer_site_id'=>$customer_site_id,'country_id'=>$country_id));
            $up_arr = array(
                'location_id'   => @$location_id,
                'address1'      => @$address1,
                'address2'      => @$address2,
                'address3'      => @$address3,
                'address4'      => @$address4,
                'zip_code'      => @$pin_code,
                'modified_by'   => $this->session->userdata('sso_id'), 
                'modified_time' => date('Y-m-d H:i:s') 
            );
            $this->Common_model->update_data('customer_site',$up_arr,array('customer_site_id'=>$customer_site_id));

            $oldArr = array(
                'address1'      => $customerSiteData['address1'],
                'address2'      => $customerSiteData['address2'],
                'address3'      => $customerSiteData['address3'],
                'address4'      => $customerSiteData['address4'],
                'zip_code'      => $customerSiteData['zip_code']
            );

            $newArr = array(
                'address1'      => @$address1,
                'address2'      => @$address2,
                'address3'      => @$address3,
                'address4'      => @$address4,
                'zip_code'      => @$pin_code
            );

            // updating and inserting in customer site historry
            $cs_history_id = $this->Common_model->get_value('cs_history',array('customer_site_id'=>$customer_site_id,'end_time'=>NULL),'cs_history_id');
            // updating cs history
            $this->Common_model->update_data('cs_history',array('end_time'=>date('Y-m-d H:i:s')),array('cs_history_id'=>$cs_history_id));
            // inserting into customer site hoistory
            unset($up_arr['modified_by']);
            unset($up_arr['modified_time']);
            $up_arr['customer_site_id'] = $customer_site_id;
            $up_arr['created_by'] = $this->session->userdata('sso_id');
            $this->Common_model->insert_data('cs_history',$up_arr);

            $oldArr['address_remarks'] = $returnOrderData['address_remarks'];
            $newArr['address_remarks'] = $remarks;
            
            $oldArr['return_status'] = returnOrderStatusWithRtoId($rtoData['rto_id']);
            $this->Common_model->update_data('return_order',array('address_check'=>2,'remarks'   => $remarks),array('return_order_id'=>$return_order_id));
            $newArr['return_status'] = returnOrderStatusWithRtoId($rtoData['rto_id']);
            $blockArr = array('blockArr'=>'Return Order');
            $remarksString = "Change Address Requested for Return Number".$returnOrderData['return_number']." Approved";
            $ad_id = tool_order_audit_data('return_order',$returnOrderData['return_order_id'],'return_order',2,'',$newArr,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$orderInfo['tool_order_id'],$orderInfo['country_id']);

            // updating the order address
        }  
        else
        { 

            $oldArr2['return_status'] = returnOrderStatusWithRtoId($rtoData['rto_id']);
            $oldArr2['address_remarks'] = $returnOrderData['address_remarks'];
            // updating the Order Address To Customer Site Address Based On Site ID
            $customer_site_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'customer_site_id');
            $cs_data = $this->Common_model->get_data_row('customer_site',array('customer_site_id'=>$customer_site_id,'country_id'=>$country_id));
            $u_data = array(
                'address1'  => $cs_data['address1'],
                'address2'  => $cs_data['address2'],
                'address3'  => $cs_data['address3'],
                'address4'  => $cs_data['address4'],
                'zip_code'  => $cs_data['zip_code'],
                'remarks'   => $remarks,                
                'address_check'=>3
            );
            $this->Common_model->update_data('return_order',$u_data,array('return_order_id'=>$return_order_id));
            
            $newArr2['address_remarks'] = $remarks;
            $newArr2['return_status'] = returnOrderStatusWithRtoId($rtoData['rto_id']);
            $blockArr = array('blockArr'=>'Return Order');
            $remarksString = "Change Address Requested for Return Number".$returnOrderData['return_number']." Rejected";
            $ad_id = tool_order_audit_data('return_order',$returnOrderData['return_order_id'],'return_order',2,'',$newArr2,$blockArr,$oldArr2,$remarksString,'',array(),"tool_order",$orderInfo['tool_order_id'],$orderInfo['country_id']);
        }
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'address_change'); exit();
        }
        else
        {
            $transactionType =($ac_submit ==1)?"Approved":"Rejected";
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Address Change Request has been '.$transactionType.' successfully, For Return No: <strong>'.$returnOrderData['return_number'].'</strong> </div>');
            redirect(SITE_URL.'address_change'); exit();
        }
    }

    public function raiseDaysExtensionRequest()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'raise_pickup');
            exit;
        }        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Raise Days Extension For Order";
        $data['nestedView']['cur_page'] = 'raise_pickup';
        $data['nestedView']['parent_page'] = 'raise_pickup';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/days_approval.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Raise Days Extension For Order';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Return','class'=>'','url'=>SITE_URL.'raise_pickup');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise Days Extension For Order','class'=>'active','url'=>'');

                   
        $data['tools'] =  $this->Order_m->getOwnedAssetsDetailsByOrderId($tool_order_id);
        $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);
        $data['flg'] = 1;
        $this->load->view('alerts/raise_days_extension',$data);
    }
    public function submitDaysExtensionRequest()
    {   
        $tool_order_id   = validate_number($this->input->post('tool_order_id',TRUE));
        $order_number    = validate_string($this->input->post('order_number',TRUE));
        $remarks         = validate_string($this->input->post('remarks',TRUE));
        $new_return_date = validate_string($this->input->post('new_return_date',TRUE));
        $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $this->Common_model->update_data('tool_order',array('days_approval'=>1),array('tool_order_id'=>$tool_order_id));

        $oed_data = array(
            'return_date'       => format_date($new_return_date),
            'created_by'        => $this->session->userdata('sso_id'),
            'created_time'      => date('Y-m-d H:i:s'),
            'status'            => 2, 
            'tool_order_id'     => $tool_order_id,
            'approved_remarks'  => $remarks
        );
        $this->Common_model->insert_data('order_extended_days',$oed_data);
        # Audit Start
        $oldArr = array('return_date' => $tool_order_arr['return_date']);
        $newArr = array('return_date' => $new_return_date,'remarks'=>$remarks);
        $remarksString = 'Requested for Return Days Extension for Order Number'.$order_number;
        $blockArr = array('blockName'=>"Tool Order");
        tool_order_audit_data('tool_order',$tool_order_id,"tool_order",2,'',$newArr,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$tool_order_id,$tool_order_arr['country_id']);
        # Audit End
        if($this->db->trans_status === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'raise_pickup'); exit();
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Request For Return Date Extension has been Raised Successfully For Order Number '.$order_number.' </div>');
            redirect(SITE_URL.'raise_pickup'); exit();
        }
    }

    public function exceededOrderDuration()
    {
        $data['nestedView']['heading']="Ext Return Date Approval";
        $data['nestedView']['cur_page'] = 'exceededOrderDuration';
        $data['nestedView']['parent_page'] = 'exceededOrderDuration';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = ' Ext Return Date Approval';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>' Ext Return Date Approval','class'=>'active','url'=>'');
        # Search Functionality       
        $psearch=$this->input->post('exceededOrderDuration',TRUE);
        if($psearch!='') 
        {
            $searchParams=array(
                'order_number'           => validate_string($this->input->post('order_number',TRUE)),
                'order_delivery_type_id' => validate_string($this->input->post('order_delivery_type_id',TRUE)),
                'deploy_date'            => validate_string($this->input->post('deploy_date',TRUE)),
                'return_date'            => validate_string($this->input->post('return_date',TRUE)),
                'country_id'             => validate_number(@$this->input->post('country_id',TRUE)),
                'users_id'               => validate_number(@$this->input->post('users_id',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'order_number'             => $this->session->userdata('order_number'),
                    'order_delivery_type_id'   => $this->session->userdata('order_delivery_type_id'),
                    'deploy_date'              => $this->session->userdata('deploy_date'),
                    'return_date'              => $this->session->userdata('return_date'),
                    'country_id'               => $this->session->userdata('country_id'),
                    'users_id'                 => $this->session->userdata('users_id')
                );
            }
            else 
            {
                $searchParams=array(
                    'order_number'             => '',
                    'order_delivery_type_id'   => '',
                    'deploy_date'              => '',
                    'return_date'              => '',
                    'country_id'               => '',
                    'users_id'                 => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'exceededOrderDuration/';

        # Total Records
        $config['total_rows'] = exceededOrderDurationTotalNumRows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Alerts_m->exceededOrderDurationResults($current_offset, $config['per_page'], $searchParams,$task_access);
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1));
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        # Additional data
        $data['displayResults'] = 1;
        $this->load->view('alerts/exceeded_duration',$data);
    }

    public function exceededOrderDurationAction()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'exceededOrderDuration');
            exit;
        }        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']=" Ext Return Date Approval Details";
        $data['nestedView']['cur_page'] = 'exceededOrderDuration';
        $data['nestedView']['parent_page'] = 'exceededOrderDuration';
        
        #page authentication
        $task_access = page_access_check('exceededOrderDuration');
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/days_approval.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Ext Return Date Approval Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ext Return Date Approval','class'=>'','url'=>SITE_URL.'exceededOrderDuration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ext Return Date Approval Details','class'=>'active','url'=>'');
        $data['tools'] =  $this->Order_m->getOwnedAssetsDetailsByOrderId($tool_order_id);
        $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);
        $data['exd_date_details']  = $this->Common_model->get_data_row('order_extended_days',array('tool_order_id'=>$tool_order_id,'status'=>2));
        $data['flg'] = 1;
        $this->load->view('alerts/exceeded_duration',$data);
    }

    public function submitexceededOrderDuration()
    {
        $ac_submit          = validate_string($this->input->post('ac_submit',TRUE));
        $tool_order_id      = validate_number($this->input->post('tool_order_id',TRUE));
        $order_number       = validate_string($this->input->post('order_number',TRUE));
        $new_return_date    = validate_string(format_date($this->input->post('new_return_date',TRUE)));
        $remarks            = validate_string($this->input->post('approved_remarks',TRUE));
        $reason             = validate_string($this->input->post('reason',TRUE));
        $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $fe_approved_remarks = $this->Common_model->get_value('order_extended_days',array('tool_order_id'=>$tool_order_id,'status'=>2),'approved_remarks');
        // updating new address in customer site and inserting in customer site history
        if($ac_submit == 1)
        { 
            $oldArr = array(
                'approval_status'   => "Return Days Extension Requested",
                'return_date'       => $tool_order_arr['return_date'],
                'remarks'           => $fe_approved_remarks
            );
            $u_data = array(
                'days_approval'           =>2, // 2 for approved
                'return_date'             =>$new_return_date,
                'days_approval_remarks'   =>$approved_remarks
            );
            $this->Common_model->update_data('tool_order',$u_data,array('tool_order_id'=>$tool_order_id)); // 3 approved            
            $final_remarks = 'FE Remarks :'.$fe_approved_remarks.'| Admin Approved Remarks: '.$remarks;
            $this->Common_model->update_data('order_extended_days',array('exd_approved_date'=>$new_return_date,'status'=>3,'approved_remarks'=>$final_remarks),array('tool_order_id'=>$tool_order_id,'status'=>2));
            # Audit Start
            $newArr = array(
                'approval_status'=>'Approved',
                'return_date'    => $new_return_date,
                'remarks'        => $remarks
            
            );
            $remarksString = 'Approved the Return Days Extension Request for Order Number'.$order_number;
            $blockArr = array('blockName'=>"Tool Order");
            tool_order_audit_data('tool_order',$tool_order_id,"tool_order",2,'',$newArr,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$tool_order_id,$tool_order_arr['country_id']);
            # Audit End
        }
        else
        {  
            $oldArr = array(
                'approval_status' => "Return Days Extension Requested",
                'return_date'     => $tool_order_arr['return_date'],
                'remarks'         => $fe_approved_remarks
            );

            $u_data = array('days_approval' => 3); // 3 for rejected// 4 for rejected                
            $this->Common_model->update_data('tool_order',$u_data,array('tool_order_id'=>$tool_order_id));            
            $final_remarks = 'FE Remarks :'.$fe_approved_remarks.'| Admin Approved Remarks :'.$remarks;
            $this->Common_model->update_data('order_extended_days',array('status'=>4,'approved_remarks'=>$final_remarks),array('tool_order_id'=>$tool_order_id,'status'=>2));
            # Audit Start
            $newArr = array(
                'approval_status' => 'Rejected',
                'return_date'     => $new_return_date,
                'remarks'         => $remarks
            );
            $remarksString = 'Rejected the Return Days Extension Request for Order Number'.$order_number;
            $blockArr = array('blockName'=>"Tool Order");
            tool_order_audit_data('tool_order',$tool_order_id,"tool_order",2,'',$newArr,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$tool_order_id,$tool_order_arr['country_id']);
            # Audit End
        }

        if($this->db->trans_status === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'exceededOrderDuration'); exit();
        }
        else
        {
            $this->db->trans_commit();
            if($ac_submit == 1)
                $reason = "Approved";
            else            
                $reason = "Rejected";
            
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> For Order Number : <strong>'.$order_number.'</strong> Return Date Extension has been <strong>'.$reason.'</strong> successfully !</div>');
            redirect(SITE_URL.'exceededOrderDuration'); exit();
        }
    }

    public function crossed_return_date_orders()
    {
        $data['nestedView']['heading']="Due In for Tool Return";
        $data['nestedView']['cur_page'] = 'crossed_return_date_orders';
        $data['nestedView']['parent_page'] = 'crossed_return_date_orders';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Due In for Tool Return';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Due In for Tool Return','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('crossed_return_date_orders', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'cr_order_number'          => validate_string($this->input->post('order_number', TRUE)),
            'cr_order_delivery_type_id'=> validate_string($this->input->post('order_delivery_type_id',TRUE)),
            'crd_sso_id'               => validate_string($this->input->post('crd_sso_id', TRUE)),
            'crd_country_id'           => validate_string($this->input->post('crd_country_id', TRUE)),
                               
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'cr_order_number'           => $this->session->userdata('cr_order_number'),
                'cr_order_delivery_type_id' => $this->session->userdata('cr_order_delivery_type_id'),
                'crd_sso_id'                => $this->session->userdata('crd_sso_id'),
                'crd_country_id'            => $this->session->userdata('crd_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'cr_order_number'           => '',
                'cr_order_delivery_type_id' => '',
                'crd_sso_id'                => '',
                'crd_country_id'            => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL.'crossed_return_date_orders/';

        # Total Records
        $config['total_rows'] = crossedReturnDateOrdersTotalNumRows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Alerts_m->crossedReturnDateOrdersResults($current_offset, $config['per_page'], $searchParams,$task_access);
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access)); 
        $data['task_access'] = $task_access;
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1)); 
        # Additional data
        $data['displayResults'] = 1;
        $this->load->view('alerts/crossed_return_date',$data);

    }
    
    public function crossed_return_date_orders_details()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'crossed_return_date_orders');
            exit;
        }
        $data['tool_order_id'] = $tool_order_id;
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Due In for Tool Return Details";
        $data['nestedView']['cur_page'] = 'crossed_return_date_orders';
        $data['nestedView']['parent_page'] = 'crossed_return_date_orders';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/receive_order.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Due In for Tool Return';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Due In for Tool Return','class'=>'','url'=>SITE_URL.'crossed_return_date_orders');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Due In for Tool Return Details','class'=>'active','url'=>'');

        # Additional data
        $data['cancel_form_action'] =  SITE_URL.'crossed_return_date_orders';  
        $data['owned_assets'] = $this->Order_m->getOwnedAssetsDetailsByOrderId($tool_order_id);
        $data['wh_data'] = $this->Common_model->get_data('warehouse',array('wh_id!='=>1));
        $data['return_type'] = $this->Common_model->get_data('return_type',array('status'=>1));
        $data['wh_id'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');
        $data['flg'] = 1;
        $this->load->view('order/owned_order_details',$data);
    }
    public function raisePickupForCalibration()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'calibration_required_tools');
            exit;
        }
        $data['tool_order_id'] = $tool_order_id;
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Initiate Return For Calibration Required Tools ";
        $data['nestedView']['cur_page'] = 'calibration_required_tools';
        $data['nestedView']['parent_page'] = 'calibration_required_tools';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/receive_order.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Initiate Return For Calibration Required Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Initiate Return For Calibration Required Tools','class'=>'','url'=>SITE_URL.'calibration_required_tools');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Initiate Return For Calibration Required Tools','class'=>'active','url'=>'');

        # Additional data
        $data['cancel_form_action'] =  SITE_URL.'calibration_required_tools';  
        $cal_required_tools=$this->Calibration_m->cal_required_tools($tool_order_id);
        $assets_arr = array();
        foreach($cal_required_tools as  $row1)
        {
            if($row1['days']<=45)
            {
                $assets_arr[] = $row1['asset_id'];
            }                
        }
        $data['total_owned_assets'] =$this->Order_m->getOwnedAssetsDetailsByOrderId($tool_order_id);
        $data['owned_assets'] = $this->Alerts_m->getCalibrationOwnedAssetsDetailsByOrderId($tool_order_id,$assets_arr);
        $data['wh_data'] = $this->Common_model->get_data('warehouse',array('wh_id!='=>1));
        $data['return_type'] = $this->Common_model->get_data('return_type',array('status'=>1,'return_type_id'=>2));
        $data['wh_id'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');
        $data['flg'] = 1;
        $this->load->view('alerts/calibration_pickup',$data);
    }

    public function crossed_expected_delivery_date()
    {
        $data['nestedView']['heading']="Due In Deliveries";
        $data['nestedView']['cur_page'] = 'crossed_expected_delivery_date';
        $data['nestedView']['parent_page'] = 'crossed_expected_delivery_date';

        #Page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Due In Deliveries';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Due In Deliveries','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            
            $searchParams=array(
                'order'          => validate_string($this->input->post('order_no', TRUE)),
                'order_type'     => validate_string($this->input->post('order_type', TRUE)),
                'country_id'     => validate_string($this->input->post('country_id', TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'order'      => $this->session->userdata('order'),
                    'order_type' => $this->session->userdata('order_type'),
                    'country_id' => $this->session->userdata('country_id')
                );
            }
            else 
            {
                $searchParams=array(
                    'order'      => '',
                    'order_type' => '',
                    'country_id' => ''
                );
                $this->session->set_userdata($searchParams);
            }            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'crossed_expected_delivery_date/';
        # Total Records
        $config['total_rows'] = $this->Alerts_m->crossed_expected_delivery_total_num_rows($searchParams,$task_access); 
        //echo $config['total_rows']; exit;

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Alerts_m->crossed_expected_delivery_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        foreach($calibration_results as $key=>$value)
        {
            $calibration_results[$key]['expected_delivery_date']=$this->Alerts_m->fe_not_acknowledged_tools($value['tool_order_id']);
        }
        $data['calibration_results'] = $calibration_results;
        $data['order_delivery_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1));
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('task_access'=>$task_access,'status'=>1,'level_id'=>2));
        $this->load->view('alerts/crossed_expected_delivery_date_list',$data);
    }
}