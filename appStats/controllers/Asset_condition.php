<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Asset_condition extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Asset_condition_m');
	}
	// Srilekha
	public function asset_condition_check()
	{
		$data['nestedView']['heading']="Defective Asset Request";
        $data['nestedView']['cur_page'] = 'asset_condition_check';
        $data['nestedView']['parent_page'] = 'asset_condition_check';
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Defective Asset Request';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Defective Asset Request','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchasset', TRUE));
        if($psearch!='') 
        {
            $searchParams = array(
                'part_number'     => validate_string($this->input->post('part_number', TRUE)),
                'part_description'=> validate_string($this->input->post('part_description',TRUE)),
                'asset_number'    => validate_string($this->input->post('asset_number', TRUE)),
                'modality_id'     => validate_number($this->input->post('modality_id',TRUE)),
                'whid'            => validate_number($this->input->post('wh_id',TRUE)),
                'country_id'      => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'part_number'      => $this->session->userdata('part_number'),
                    'part_description' => $this->session->userdata('part_description'),
                    'asset_number'     => $this->session->userdata('asset_number'),
                    'modality_id'      => $this->session->userdata('modality_id'),
                    'whid'             => $this->session->userdata('whid'),
                    'country_id'       => $this->session->userdata('country_id'),
                    'serial_no'        => $this->session->userdata('serial_no')  
                );
            }
            else 
            {
                $searchParams=array(                                   
                    'asset_number'    => '',
                    'modality_id'     => '',
                    'part_description'=> '',
                    'part_number'     => '',
                    'whid'            => '',
                    'country_id'      => '',
                    'serial_no'       => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'asset_condition_check/';
        # Total Records
        $config['total_rows'] = $this->Asset_condition_m->asset_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['assetResults'] = $this->Asset_condition_m->asset_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['displayResults'] = 1;
        $data['flg'] = 1;
        $data['status_data'] = $this->Common_model->get_data('asset_status');
        $data['whList'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access));
        $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;
        $this->load->view('asset_condition/asset_condition_view',$data);
	}

	// Srilekha
	public function edit_asset_condition()
	{
		$asset_id=@storm_decode($this->uri->segment(2));
        if($asset_id=='')
        {
            redirect(SITE_URL.'asset');
            exit;
        }        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Defective Asset Details";
        $data['nestedView']['cur_page'] = 'check_asset_condition_check';
        $data['nestedView']['parent_page'] = 'asset_condition_check';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/asset_condition.js"></script>';
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Defective Asset Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Defective Asset Request','class'=>'','url'=>SITE_URL.'asset_condition_check');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Defective Asset Details','class'=>'active','url'=>'');

        # Additional data
        $data['asset_details']=$this->Asset_condition_m->get_asset_part_details($asset_id);
        $assetList = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $country_id = $assetList['country_id'];
        $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
        $data['asset_number'] = $assetList['asset_number'];
        $data['asset_id'] = $asset_id;
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'insert_asset_condition';
        $data['displayResults'] = 0;
        $this->load->view('asset_condition/asset_condition_view',$data);
	}

	// Srilekha
	public function insert_asset_condition()
	{
        $task_access = page_access_check('asset_condition_check');
		$asset_id= validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        if($asset_id == '')
        {
            redirect(SITE_URL.'asset_condition_check'); exit();
        }
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

        #audit data
        $old_data = array(get_da_key() => get_da_status($asset_id,$asset_arr['approval_status']));
		$wh_id = $asset_arr['wh_id'];
        $asset_condition_arr = $this->input->post('asset_condition_id',TRUE);
        $remarks_arr =  $this->input->post('remarks',TRUE);
        $asset_details=$this->Asset_condition_m->get_asset_part_details($asset_id);
        $asset_status_condition=0;
        $check = 0;
        foreach($asset_details as $key=>$value)
        {
            if($asset_condition_arr[$value['part_id']]!=1)
            {
               $asset_status_condition++;
            }
            if($asset_condition_arr[$value['part_id']]==3)
            {
                $check++;
            }
        }
        if($asset_status_condition == 0)
        {
            $asset_number = $asset_arr['asset_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Please Select Atleast one defective Part for asset Number :<strong> '.$asset_number.'!</strong> </div>'); 
            redirect(SITE_URL.'edit_asset_condition/'.storm_encode($asset_id)); exit();
        }

        if(count($asset_condition_arr) != $check)
        {
            $type = 1;
        }
        else
        {
            $type = 2;
        }

		$defective_asset=array(
        'type'		   => $type,
        'remarks'	   => validate_string($this->input->post('type_remarks',TRUE)),
        'asset_id'	   => $asset_id,
        'wh_id'		   => $wh_id,
        'created_by'   => $this->session->userdata('sso_id'),
        'created_time' => date('Y-m-d H:i:s'),
        'status'       => 1);
        
		$this->db->trans_begin();
        $defective_asset_id=$this->Common_model->insert_data('defective_asset',$defective_asset);
        
        foreach ($asset_details as $key => $value)
        {
            $remarks1 = $remarks_arr[$value['part_id']];
            if($remarks1 == '') $remarks = NULL; 
            $insert_data3 = array(
            'defective_asset_id' => $defective_asset_id,
            'part_id'            => $value['part_id'],
            'asset_condition_id' => $asset_condition_arr[$value['part_id']],
            'remarks'            => $remarks1
            );
            $this->Common_model->insert_data('defective_asset_health',$insert_data3);
        }

        $this->Common_model->update_data('asset',array('approval_status'=>1),array('asset_id'=>$asset_id));

        #audit data
        $new_data = array(get_da_key() => get_da_status($asset_id,2));
        $remarks = "Asset Found Defective at Wh";
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'defective_asset',$defective_asset_id,$asset_arr['country_id']);

        // send defective/missed asset mail    
        $country_id = $asset_arr['country_id'];
        sendDefectiveOrMissedAssetMail($defective_asset_id,$type,$country_id,1,$_SESSION['sso_id']);
       
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'asset_condition_check'); exit(); 
        }
        else
        {
            $asset_number = $asset_arr['asset_number'];
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Defective Asset request has been raised successfully for Asset Number : <strong>'.$asset_number.'</strong> !</div>');
            redirect(SITE_URL.'asset_condition_check'); exit;
        }
	}
}