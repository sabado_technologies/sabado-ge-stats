<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
 * created by Mastan on 26th july 2017 14:35PM
*/
class Scrap extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Scrap_model');
		$this->load->model('Asset_m');
	}

	public function raise_scrap()
	{
		$defective_asset_id=@storm_decode($this->uri->segment(2));
        if($defective_asset_id == '')
        {
            redirect(SITE_URL.'defective_asset'); exit();
        }
        $asset_id=$this->Common_model->get_value('defective_asset',array('defective_asset_id'=>$defective_asset_id),'asset_id');
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Scrap Tools";
        $data['nestedView']['cur_page'] = 'check_scrap';
        $data['nestedView']['parent_page'] = 'defective_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/scrap.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Scrap Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Defective Asset Requests','class'=>'','url'=>SITE_URL.'defective_asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scrap Tools','class'=>'active','url'=>'');

        #Data
        $data['asset_id']= $asset_id;
        $data['defective_asset_id']= $defective_asset_id;
        $data['cancel_btn'] = 'defective_asset';
        $data['form_action'] = SITE_URL.'insert_scrap';
        $data['lrow'] = $this->Asset_m->asset_details_view1($asset_id);
        $data['subComponent'] = $this->Asset_m->get_l1_sub_component_details($asset_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>6));
        $this->load->view('scrap/raise_scrap',$data);
	}

    public function insert_scrap()
    {
        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));

        #check for EGM Calibration Entry if available cancel the request
        cancel_egm_cr_entry($asset_id);

        $defective_asset_id = validate_number(storm_decode($this->input->post('defective_asset_id',TRUE)));
        if($asset_id == '' || $defective_asset_id == '')
        {
            redirect(SITE_URL.'defective_asset'); exit();
        }

        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $wh_id = $asset_arr['wh_id'];
        $asset_number = $asset_arr['asset_number'];

        #audit data
        $old_data = array(
            'availability_status' => $asset_arr['availability_status'],
            'asset_status_id'     => $asset_arr['status'],
            'approval_status'     => get_da_status($asset_id,$asset_arr['approval_status'])
        );

        $remarks=validate_string($this->input->post('remarks',TRUE));
        $scrap_data = array(
            'asset_id'      => $asset_id,
            'wh_id'         => $wh_id,
            'sub_inventory' => 'NULL',
            'remarks'       => $remarks,
            'created_by'    => $this->session->userdata('sso_id'),
            'created_time'  => date('Y-m-d H:i:s'),
            'status'        => 1,
            'defective_asset_id' => $defective_asset_id
        );
        $this->db->trans_begin();
        $scrap_id = $this->Common_model->insert_data('scrap',$scrap_data);

        $this->Common_model->update_data('defective_asset',array('request_handled_type'=>4),array('defective_asset_id'=>$defective_asset_id));

        //getting scrap support document array
        $document_type = $this->input->post('document_type',TRUE);
        foreach ($document_type as $key => $value) 
        {
            if($_FILES['support_document_'.$key]['name']!='')
            {
                $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                $config['upload_path'] = scrap_document_path();
                $config['allowed_types'] = 'gif|jpg|png|pdf';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('support_document_'.$key);
                $fileData = $this->upload->data();
                $support_document = $config['file_name'];
                $original_name    = $config['org_name'];
            }
            else
            {
                $support_document = '';
            }
            $insert_doc = array(
                                'document_type_id' => $value,
                                'name'             => $support_document,
                                'scrap_id'         => $scrap_id,
                                'doc_name'         => $original_name,
                                'created_by'       => $this->session->userdata('sso_id'),
                                'created_time'     => date('Y-m-d H:i:s'),
                                'status'           => 1
                                );

            if($value!='' && $support_document != '')
            {
                //Insert data into scrap_doc
                $this->Common_model->insert_data('scrap_doc',$insert_doc);
            }
        }

        $update_asset = array(
            'availability_status' => 2,
            'status'              => 11,
            'approval_status'     => 0,
            'modified_by'         => $this->session->userdata('sso_id'),
            'modified_time'       => date('Y-m-d H:i:s')
        );
        $update_asset_where = array('asset_id' => $asset_id);
        $this->Common_model->update_data('asset',$update_asset,$update_asset_where);

        #audit data
        $new_data = array(
            'availability_status' => 2,
            'asset_status_id'     => 11,
            'approval_status'     => get_da_status($asset_id,0)
        );
        $remarks = "Asset has been scrapped";
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'scrap',$scrap_id,$asset_arr['country_id']);

        $update_ash = array('end_time' => date('Y-m-d H:i:s'));
        $update_ash_where = array('asset_id' => $asset_id,'end_time'=>NULL);
        $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

        $insert_ash = array(
            'asset_id'     => $asset_id,
            'status'       => 11,
            'created_by'   => $this->session->userdata('sso_id'),
            'created_time' => date('Y-m-d H:i:s')
        );
        $this->Common_model->insert_data('asset_status_history',$insert_ash);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'defective_asset');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Asset Number : <strong>'.$asset_number.'</strong> has been Scrapped successfully!
             </div>');
            redirect(SITE_URL.'defective_asset');
        } 
    }

    public function closed_scrap_list()
    {
        $data['nestedView']['heading']="Closed Scrap Requests";
        $data['nestedView']['cur_page'] = 'closed_scrap_list';
        $data['nestedView']['parent_page'] = 'closed_scrap_list';
        
        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Scrap Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Scrap Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchasset', TRUE));
        if($psearch!='') 
        {
            $searchParams = array(
                'tool_number'     => validate_string($this->input->post('tool_number', TRUE)),
                'tool_description'=> validate_string($this->input->post('tool_description',TRUE)),
                'asset_number'    => validate_string($this->input->post('asset_number', TRUE)),
                'status'          => validate_number($this->input->post('status', TRUE)),
                'whid'            => validate_number($this->input->post('wh_id',TRUE)),
                'country_id'      => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'       => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'tool_number'      => $this->session->userdata('tool_number'),
                    'tool_description' => $this->session->userdata('tool_description'),
                    'asset_number'     => $this->session->userdata('asset_number'),
                    'status'           => $this->session->userdata('status'),
                    'whid'             => $this->session->userdata('whid'),
                    'country_id'       => $this->session->userdata('country_id'),
                    'serial_no'        => $this->session->userdata('serial_no')   
                );
            }
            else 
            {
                $searchParams=array(      
                    'tool_number'      => '',
                    'tool_description' => '',
                    'asset_number'     => '',
                    'status'           => '',
                    'whid'             => '',
                    'country_id'       => '',
                    'serial_no'        => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_scrap_list/';
        # Total Records
        $config['total_rows'] = $this->Scrap_model->scraped_asset_total_num_rows($searchParams,$task_access);
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['assetResults'] = $this->Scrap_model->scraped_asset_results($current_offset, $config['per_page'], $searchParams,$task_access);
        # Additional data
        $data['displayResults'] = 1;
        $data['status_data'] = $this->Common_model->get_data('asset_status');
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['whList']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));

        $this->load->view('scrap/closed_scrap_list',$data);
    }

    public function view_scraped_list()
    {
        $scrap_id=@storm_decode($this->uri->segment(2));
        if($scrap_id == '')
        {
            redirect(SITE_URL.'scrap'); exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Closed Scrap Details";
        $data['nestedView']['cur_page'] = 'closed_scrap_list';
        $data['nestedView']['parent_page'] = 'closed_scrap_list';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/scrap.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Scrap Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Scrap Requests','class'=>'','url'=>SITE_URL.'closed_scrap_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Scrap Details','class'=>'active','url'=>'');

        #Data
        $asset_id = $this->Common_model->get_value('scrap',array('scrap_id'=>$scrap_id),'asset_id');
        $data['asset_id']= $asset_id;
        $tool_id = $this->Asset_m->get_top_tool_id($asset_id);
        $data['lrow'] = $this->Asset_m->asset_details_view1($asset_id);
        $data['subComponent'] = $this->Asset_m->get_l1_sub_component_details($asset_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('scrap_doc',array('scrap_id'=>$scrap_id,'status'=>1));

        $this->load->view('scrap/scraped_list_details',$data);
    }

    public function raise_scrap_asset()
    {
        $asset_id = @storm_decode($this->uri->segment(2));
        if($asset_id == '')
        {
            redirect(SITE_URL.'asset'); exit();
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Scrap Tools";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/scrap.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Scrap Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scrap Tools','class'=>'active','url'=>'');

        #Data
        $data['asset_id']= $asset_id;
        $data['defective_asset_id']= '';
        $data['cancel_btn'] = 'asset';
        $data['form_action'] = SITE_URL.'insert_scrap_asset';
        $data['lrow'] = $this->Asset_m->asset_details_view1($asset_id);
        $data['subComponent'] = $this->Asset_m->get_l1_sub_component_details($asset_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>6));
        $this->load->view('scrap/raise_scrap',$data);
    }

    #mastan -- inserting scrap request
    public function insert_scrap_asset()
    {
        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        if($asset_id == '')
        {
            redirect(SITE_URL.'asset'); exit();
        }

        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $wh_id = $asset_arr['wh_id'];
        $asset_number = $asset_arr['asset_number'];

        #audit data
        $old_data = array(
            'availability_status' => $asset_arr['availability_status'],
            'asset_status_id'     => $asset_arr['status'],
            'approval_status'     => get_da_status($asset_id,$asset_arr['approval_status'])
        );
        
        $remarks=validate_string($this->input->post('remarks',TRUE));
        $scrap_data = array('asset_id'      => $asset_id,
                            'wh_id'         => $wh_id,
                            'sub_inventory' => 'NULL',
                            'remarks'       => $remarks,
                            'created_by'    => $this->session->userdata('sso_id'),
                            'created_time'  => date('Y-m-d H:i:s'),
                            'status'        => 1,
                            'defective_asset_id' => NULL
                            );
        $this->db->trans_begin();
        $scrap_id = $this->Common_model->insert_data('scrap',$scrap_data);

        //getting scrap support document array
        $document_type = $this->input->post('document_type',TRUE);
        foreach ($document_type as $key => $value) 
        {
            if($_FILES['support_document_'.$key]['name']!='')
            {
                $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                $config['upload_path'] = scrap_document_path();
                $config['allowed_types'] = 'gif|jpg|png|pdf';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('support_document_'.$key);
                $fileData = $this->upload->data();
                $support_document = $config['file_name'];
                $original_name    = $config['org_name'];
            }
            else
            {
                $support_document = '';
            }
            $insert_doc = array(
                'document_type_id' => $value,
                'name'             => $support_document,
                'scrap_id'         => $scrap_id,
                'doc_name'         => $original_name,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1
            );

            if($value!='' && $support_document != '')
            {
                //Insert data into scrap_doc
                $this->Common_model->insert_data('scrap_doc',$insert_doc);
            }
        }

        $update_asset = array(
            'availability_status' => 2,
            'status'              => 11,
            'approval_status'     => 0,
            'modified_by'         => $this->session->userdata('sso_id'),
            'modified_time'       => date('Y-m-d H:i:s')
        );
        $update_asset_where = array('asset_id' => $asset_id);
        $this->Common_model->update_data('asset',$update_asset,$update_asset_where);

        #audit data
        $new_data = array(
            'availability_status' => 2,
            'asset_status_id'     => 11,
            'approval_status'     => get_da_status($asset_id,0)
        );
        $remarks = "Asset has been scrapped";
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'scrap',$scrap_id,$asset_arr['country_id']);

        $update_ash = array('end_time' => date('Y-m-d H:i:s'));
        $update_ash_where = array('asset_id' => $asset_id,'end_time'=>NULL);
        $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

        $insert_ash = array(
            'asset_id'     => $asset_id,
            'status'       => 11,
            'created_by'   => $this->session->userdata('sso_id'),
            'created_time' => date('Y-m-d H:i:s')
        );
        $this->Common_model->insert_data('asset_status_history',$insert_ash);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Asset Number : <strong>'.$asset_number.'</strong> has been Scrapped successfully!
             </div>');
        } 
        redirect(SITE_URL.'asset');
    }
}