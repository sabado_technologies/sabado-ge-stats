<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Designation extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Designation_m');
	}

	public function designation()
    {
        
        $data['nestedView']['heading']=" Manage Designation";
		$data['nestedView']['cur_page'] = 'designation';
		$data['nestedView']['parent_page'] = 'designation';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = ' Manage Designation';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Designation','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchdesignation', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'roleid'               => validate_number($this->input->post('roleid', TRUE)),
                               'designame'        => validate_string($this->input->post('designation_name',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                                'roleid'              => validate_number($this->input->post('roleid', TRUE)),
                               'designame'        => validate_string($this->input->post('designame',TRUE))
                               );
            }
            else {
                $searchParams=array(
                                    'designame' => '',
                                    'roleid'    => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'designation/';
        # Total Records
        $config['total_rows'] = $this->Designation_m->designation_total_num_rows($searchParams);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
         $data['name'] = array(''=>'Select role')+$this->Common_model->get_dropdown('role','role_id','name');
        # Loading the data array to send to View
        $data['designationResults'] = $this->Designation_m->designation_results($current_offset, $config['per_page'], $searchParams);
        
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('designation/designation_view',$data);

    }
    public function add_designation()
    {
       
        $data['nestedView']['heading']="Add New Designation";
		$data['nestedView']['cur_page'] = 'check_designation';
		$data['nestedView']['parent_page'] = 'designation';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/designation.js"></script>';

		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Add New Designation';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Designation','class'=>'','url'=>SITE_URL.'designation');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Designation','class'=>'active','url'=>'');
        $data['name'] = array(''=>'Select role')+$this->Common_model->get_dropdown('role','role_id','name');
        # Additional data
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_designation';
        $data['displayResults'] = 0;
        $this->load->view('designation/designation_view',$data);
    }

    public function insert_designation()
    
    {
        #page authentication
        $task_access = page_access_check('designation');
        $desig_name = validate_string($this->input->post('designation_name',TRUE));
        $desig_data = array('name' => $desig_name,'designation_id'=>0);
        $unique = $this->Designation_m->check_designation_name_availability($desig_data);
        if($unique>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> '.$desig_name.' already exist! please check.
                                     </div>'); 
            redirect(SITE_URL.'designation'); exit();
        }
        if($unique==0)
        {
            $data=array(
                        'name'                  =>     $desig_name,
                        'status'                =>     1,
                        'created_by'            =>     $this->session->userdata('sso_id'),
                        'created_time'          =>     date('Y-m-d H:i:s')
                        );
            //echo "<pre>"; print_r($data); exit;
            $this->db->trans_begin();    
            $designation = $this->Common_model->insert_data('designation',$data);        
            $data1=array(
                    'role_id'                    =>     validate_number($this->input->post('role_id',TRUE)),
                    'designation_id'             =>     $designation
                    );
            $role_designation = $this->Common_model->insert_data('role_designation',$data1);

            if ($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>'); 
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Designation has been Added successfully!</div>');
                      
            }
        }
           redirect(SITE_URL.'designation');
    }   

    public function edit_designation()
    {
        
        $designation_id=@storm_decode($this->uri->segment(2));
        if($designation_id=='')
        {
            redirect(SITE_URL.'designation');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Designation ";
		$data['nestedView']['cur_page'] = 'check_designation';
		$data['nestedView']['parent_page'] = 'designation';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/designation.js"></script>';

		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Edit Designation ';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Designation','class'=>'','url'=>'Designation');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Designation','class'=>'active','url'=>'');
        $data['name'] = array(''=>'Select role')+$this->Common_model->get_dropdown('role','role_id','name');        
        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_designation';
        $data['displayResults'] = 0;
       
        # Data
        $row = $this->Designation_m->get_designation_details($designation_id);
        $data['lrow'] = $row[0];
        $this->load->view('designation/designation_view',$data);
    }
    public function update_designation()
    {
        #page authentication
        $task_access = page_access_check('designation');
        $designation_id=validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($designation_id==''){
            redirect(SITE_URL.'designation');
            exit;
        }
        $desig_name = validate_string($this->input->post('designation_name',TRUE));
        $data = array('name'=>$desig_name,'designation_id'=>$designation_id);
        $unique = $this->Designation_m->check_designation_name_availability($data);
        if($unique>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> '.$desig_name.' already exist! please check.
                                     </div>'); 
            redirect(SITE_URL.'designation'); exit();
        }
        if($unique==0)
        {
            // GETTING INPUT TEXT VALUES
            $data = array( 
            'name'                  =>     validate_string($this->input->post('designation_name',TRUE)),
            'status'                =>     1,
            'modified_by'           =>     $this->session->userdata('sso_id'),
            'modified_time'         =>     date('Y-m-d H:i:s')
                    );
            $where = array('designation_id'=>$designation_id);
            $this->db->trans_begin();
            $this->Common_model->update_data('designation',$data,$where);
            @$role_id = validate_number($this->input->post('role_id',TRUE));
            //Deactivating all the roles with that designation
            $where1 = array('designation_id'=>$designation_id);
            $data1 = array('status'=>2);
            $this->Common_model->update_data('role_designation',$data1, $where1);

            if(@count($role_id)>0)
            {
                //UPDATE EXIST DEPARTMENTS AND INSERTING NEW DEPARTMENTS
                $this->Designation_m->insert_update($role_id,$designation_id);
               
            }

            if($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>');  
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Designation  has been updated successfully! </div>');
            }
        
                
        }
        redirect(SITE_URL.'designation');
    }

    public function deactivate_designation($encoded_id)
    {
        #page authentication
        $task_access = page_access_check('designation');
        $designation_id=@storm_decode($encoded_id);
        if($designation_id==''){
            redirect(SITE_URL.'designation');
            exit;
        }
        $where = array('designation_id' => $designation_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('designation',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
										<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
										<div class="icon"><i class="fa fa-check"></i></div>
										<strong>Success!</strong> Designation has been deactivated successfully!
									 </div>');
        redirect(SITE_URL.'designation');

    }

    public function activate_Designation($encoded_id)
    {
       #page authentication
        $task_access = page_access_check('designation');
        $designation_id=@storm_decode($encoded_id);
        if($designation_id==''){
            redirect(SITE_URL.'designation');
            exit;
        }
        $where = array('designation_id' => $designation_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('designation',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
										<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
										<div class="icon"><i class="fa fa-check"></i></div>
										<strong>Success!</strong> Designation has been Activated successfully!
									 </div>');
        redirect(SITE_URL.'designation');
	}
    /*Author= aswini
     date = 14/06/17 
     method to check unique validation for calibration supplier name
    */
    public function check_designation_name_availability()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $designation_id = validate_number($this->input->post('designation_id',TRUE));
        $data = array('name'=>$name,'designation_id'=>$designation_id);
        $result = $this->Designation_m->check_designation_name_availability($data);
        echo $result;
    }
}