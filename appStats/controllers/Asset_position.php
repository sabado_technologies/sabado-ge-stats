<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By Maruthi on 13th June 17 8:30PM

class Asset_position extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Asset_position_m');
    }

    public function asset_position_data()
    {
        $data['nestedView']['heading']="Asset Position";
        $data['nestedView']['cur_page'] = 'asset_position';
        $data['nestedView']['parent_page'] = 'asset_position';

        #page authentication
        $task_access = page_access_check('manage_user');

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Open Orders';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Asset Position','class'=>'active','url'=>'');

        # Search Functionality
        $psearch= ($this->input->post('search_asset', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'asset_number'      => validate_string($this->input->post('asset_number', TRUE)),
            'from_date'         => validate_string($this->input->post('from_date', TRUE)),
            'to_date'           => validate_string($this->input->post('to_date', TRUE)),
            'fe_id'             => validate_string($this->input->post('fe_id', TRUE)),
            'status_id'         => validate_number($this->input->post('status_id', TRUE)),
            'a_country_id'      => validate_number($this->input->post('a_country_id', TRUE)),
            'a_warehouse_id'    => validate_number($this->input->post('a_warehouse_id', TRUE)));
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'asset_number'      => $this->session->userdata('asset_number'),
                'from_date'         => $this->session->userdata('from_date'),
                'to_date'           => $this->session->userdata('to_date'),
                'fe_id'             => $this->session->userdata('fe_id'),
                'status_id'         => $this->session->userdata('status_id'),
                'a_country_id'      => $this->session->userdata('a_country_id'),
                'a_warehouse_id'    => $this->session->userdata('a_warehouse_id')
                );
            }
            else 
            {
                $searchParams=array(
                'asset_number'      => '',
                'from_date'         => '',
                'to_date'           => '',
                'fe_id'             => '',
                'status_id'         => '',
                'a_country_id'      => '',
                'a_warehouse_id'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'asset_position_data/';

        # Total Records
        $config['total_rows'] = $this->Asset_position_m->asset_position_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['asset_results'] = $this->Asset_position_m->asset_position_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        # Additional data
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['asset_status'] = $this->Common_model->get_data('asset_status',array()); 


        $this->load->view('asset_position/asset_position_view',$data);

    }


}  