<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_controller {

	public function __construct() 
	{
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
	}

	public function cron_daily()
	{
		insert_calibration();
		insert_calibration_egm();
		calibration_mail();
		calibration_mail_egm();
		deleteUploadData();
		get_unacknowledged_tools();
		get_delayed_return_tools();
	}

	public function cron_test()
	{
		//send_email('mallareddy.thukkani@entransys.com','Test','cron test'.date('Y m d H:i:s'));
	}

	// delete when giving final production data
	public function update_records()
	{		
		$this->Common_model->update_data('part',array('part_status'=>1),array('part_id>'=>0));
		$this->Common_model->update_data('asset_status',array('type'=>1),array('asset_status_id'=>12));
	}
	
}