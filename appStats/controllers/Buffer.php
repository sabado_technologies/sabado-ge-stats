<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Buffer extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Buffer_m');
	}

	// Srilekha
	public function buffer_to_inventory()
	{
		# Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Buffer To Inventory";
        $data['nestedView']['cur_page'] = 'buffer';
        $data['nestedView']['parent_page'] = 'buffer_to_inventory';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']); 
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($_SESSION['header_country_id']!='')
            {
                $country_id = $_SESSION['header_country_id'];   
            }
            else if($this->input->post('country_id')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                if(@$this->uri->segment(2)!='')
                {
                    $country_id = $this->session->userdata('bi_country_id');
                }
                else
                {
                    $_SESSION['bi_country_id'] = '';
                    $data['nestedView']['heading']="Buffer To Inventory";
                    $data['nestedView']['cur_page'] = 'buffer_to_inventory';
                    $data['nestedView']['parent_page'] = 'buffer_to_inventory';

                    # include files
                    $data['nestedView']['css_includes'] = array();
                    $data['nestedView']['js_includes'] = array();
                    $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                    # Breadcrumbs
                    $data['nestedView']['breadCrumbTite'] = 'Buffer To Inventory';
                    $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                    $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                    $data['form_action'] = SITE_URL.'buffer_to_inventory';
                    $data['cancel'] = SITE_URL;
                    $this->load->view('user/intermediate_page',$data); 
                }
            }
        }
        if(@$country_id != '')
        {
            $_SESSION['bi_country_id'] = $country_id;
            # include files
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/buffer.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Buffer To Inventory';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            if($task_access == 3 && $_SESSION['header_country_id']=='')
            {
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'','url'=>SITE_URL.'buffer_to_inventory');
            }
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Buffer To Inventory','class'=>'active','url'=>'');
            $brow = $this->Common_model->get_data_row('warehouse',array('country_id'=>$country_id,'status'=>3));
            $buffer_wh_id = $brow['wh_id'];

            // Logic For Cart Functionality 
            if(isset($_POST['add']))
            {
                if(!isset($_SESSION['b_asset_id']))
                {
                    $_SESSION['b_asset_id'] = array();
                }
                $a = count(@$_POST['asset_id']);
                for ($i=0; $i < $a; $i++)
                {   
                    $_SESSION['b_asset_id'][@$_POST['asset_id'][$i]] = @$_POST['asset_id'][$i];
                }
            }
            if(!isset($_POST['reset']) && !isset($_POST['search']) && !isset($_POST['add']) && !isset($_POST['b_asset_id'])  )
            {           
                if(isset($_SESSION['b_first_url_count']))
                {
                    $_SESSION['b_last_url_count'] = base_url(uri_string());
                    if(strcmp($_SESSION['b_first_url_count'],$_SESSION['b_last_url_count']))
                    {                
                    }
                    else
                    {
                        if(isset($_SESSION['b_asset_id'])) unset($_SESSION['b_asset_id']);
                    }
                }
                else
                {
                    $_SESSION['b_first_url_count'] = base_url(uri_string());
                    if(isset($_SESSION['b_asset_id'])) unset($_SESSION['b_asset_id']);
                }
            }

            #search functionality
            $psearch=validate_string($this->input->post('search', TRUE));
            if($psearch!='') 
            {
                $searchParams=array(
                    'part_number'      => validate_string($this->input->post('part_number', TRUE)),
                    'part_description' => validate_string($this->input->post('part_description', TRUE)),
                    'modality_id'      => validate_string($this->input->post('modality_id', TRUE)),
                    'asset_number'     => validate_string($this->input->post('asset_number', TRUE)),
                    'serial_no'        => validate_string($this->input->post('serial_no',TRUE))
                );
                $this->session->set_userdata($searchParams);
            } 
            else 
            {
                if($this->input->post('reset')!='')
                {
                    $searchParams=array(
                        'part_number'      => '',
                        'part_description' => '',
                        'modality_id'      => '',
                        'asset_number'     => '',
                        'serial_no'        => ''
                    );
                    $this->session->set_userdata($searchParams);
                }

                else if($this->uri->segment(2)!='')
                {
                    $searchParams=array(
                        'part_number'      => $this->session->userdata('part_number'),
                        'part_description' => $this->session->userdata('part_description'),
                        'modality_id'      => $this->session->userdata('modality_id'),
                        'asset_number'     => $this->session->userdata('asset_number'),
                        'serial_no'        => $this->session->userdata('serial_no')
                    );
                }
                else 
                {
                    $searchParams=array(
                        'part_number'      => '',
                        'part_description' => '',
                        'modality_id'      => '',
                        'asset_number'     => '',
                        'serial_no'        => ''
                    );
                    $this->session->set_userdata($searchParams);
                }
            }
            $data['searchParams'] = $searchParams;

            # Default Records Per Page - always 10
            /* pagination start */
            $config = get_paginationConfig();
            $config['base_url'] = SITE_URL . 'buffer_to_inventory/';

            # Total Records
            $config['total_rows'] = $this->Buffer_m->buffer_total_num_rows($searchParams,$country_id,$buffer_wh_id);
            $config['per_page'] = getDefaultPerPageRecords();
            $data['total_rows'] = $config['total_rows'];
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            
            if ($data['pagination_links'] != '') {
                $data['last'] = $this->pagination->cur_page * $config['per_page'];
                if ($data['last'] > $data['total_rows']) {
                    $data['last'] = $data['total_rows'];
                }
                $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
            }
            $data['sn'] = $current_offset + 1;
            /* pagination end */
            # Loading the data array to send to View
            $data['toolResults'] = $this->Buffer_m->buffer_results($current_offset, $config['per_page'], $searchParams,$country_id,$buffer_wh_id);

            # Additional data
            $data['current_offset'] = $current_offset;
            $data['displayResults'] = 1;
            $data['country_id'] = $country_id;
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
            $this->load->view('buffer/buffer_to_inventory_view',$data);
        }
	}

    public function issue_buffer_to_inventory()
    {
        $data['nestedView']['heading']="Move Buffer To Inventory";
        $data['nestedView']['cur_page'] = 'check_buffer_to_inventory';
        $data['nestedView']['parent_page'] = 'buffer_to_inventory';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']); 

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/buffer.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Move Buffer to Inventory';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        if($task_access == 3 && $_SESSION['header_country_id']=='')
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'','url'=>SITE_URL.'buffer_to_inventory');
        }
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Buffer To Inventory','class'=>'','url'=>SITE_URL.'buffer_to_inventory');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Move Buffer To Inventory','class'=>'active','url'=>'');
        if(isset($_SESSION['b_asset_id']))
        {
            foreach ($_SESSION['b_asset_id'] as $key => $value)
            {
                $result_data[] = $this->Buffer_m->get_asset_based_on_id($value);
                $asset_id=$value;
            }            
            $data['asset_list'] = $result_data;
        }
        $country_id = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'country_id');
        $data['wh_data'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
        $data['country_id'] = $country_id;
        $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
        $data['form_action'] = SITE_URL.'submit_buffer_to_inventory';
        $this->load->view('buffer/issue_buffer_to_inventory',$data);
    }

	// Srilekha
	public function submit_buffer_to_inventory()
	{
		$wh_id=validate_number($this->input->post('wh_id',TRUE));
        $ses_asset_id = $this->session->userdata('b_asset_id');
        if($wh_id =='' || $ses_asset_id=='')
        {
            redirect(SITE_URL.'buffer_to_inventory'); exit();
        }

        $wh = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
        $display = $wh['wh_code'].' -('.$wh['name'].')';

        $this->db->trans_begin();
        foreach ($ses_asset_id as $key => $value) 
        {
            #audit data
            $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$value));
            $old_data = array(
                'wh_id'             => $asset_arr['wh_id'],
                'tool_availability' => get_asset_position($value)
            );

            $update_a = array(
                'wh_id'         => $wh_id,
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $update_a_where = array('asset_id' => $value);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            $update_ap = array('to_date' => date('Y-m-d'));
            $update_ap_where = array('asset_id' => $value);
            $this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);

            $insert_ap = array('asset_id' => $value,
                               'wh_id'    => $wh_id,
                               'transit'  => 0,
                               'from_date'=> date('Y-m-d'),
                               'status'   => 1);
            $this->Common_model->insert_data('asset_position',$insert_ap);

            #audit data
            $new_data = array(
                'wh_id'             => $wh_id,
                'tool_availability' => get_asset_position($value)
            );
            $remarks = "Asset moved from Buffer to Inventory:".$display;
            audit_data('asset_master',$value,'asset',2,'',$new_data,array('asset_id'=>$value),$old_data,$remarks,'',array(),'','',$asset_arr['country_id']);

            $update_ah = array('to_date' => date('Y-m-d'));
            $update_ah_where = array('asset_id' => $value);
            $this->Common_model->update_data('asset_history',$update_ah,$update_ah_where);

            $insert_ah = array('asset_id' => $value,
                               'wh_id'    => $wh_id,
                               'from_date'=> date('Y-m-d'),
                               'sub_inventory' => NULL);
            $this->Common_model->insert_data('asset_history',$insert_ah);

        }
		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'buffer_to_inventory'); exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Assets has been moved successfully to Warehouse : <strong>'.$display.'</strong> !
            </div>');
            redirect(SITE_URL.'buffer_to_inventory'); exit();
        }
    }

    public function ajax_get_warehouse_by_country_id(){
        $country_id = $this->input->post('country_id');
        $res=$this->Buffer_m->get_warehouse_by_country($country_id);
        echo $res;
    }

    public function inventory_to_buffer()
    {
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Inventory To Buffer";
        $data['nestedView']['cur_page'] = 'inventory_to_buffer';
        $data['nestedView']['parent_page'] = 'inventory_to_buffer';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        if($task_access == 1)
        {
            $warehouse_id = $this->session->userdata('s_wh_id');
        }
        else if($task_access == 2 || $task_access == 3)
        {
            $country_id = $this->session->userdata('s_country_id');
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }

            if(@$this->input->post('warehouse_id',TRUE)!='')
            {
                $warehouse_id = $this->input->post('warehouse_id',TRUE);
            }
            else
            {
                if(@$this->uri->segment(2)!='')
                {
                    $warehouse_id = $this->session->userdata('ib_wh_id');
                }
                else
                {
                    $_SESSION['ib_wh_id'] = '';
                    $data['nestedView']['heading']="Inventory To Buffer";
                    $data['nestedView']['cur_page'] = 'inventory_to_buffer';
                    $data['nestedView']['parent_page'] = 'inventory_to_buffer';

                    # include files
                    $data['nestedView']['css_includes'] = array();
                    $data['nestedView']['js_includes'] = array();
                    $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
                    
                    # Breadcrumbs
                    $data['nestedView']['breadCrumbTite'] = 'Inventory To Buffer';
                    $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Warehouse','class'=>'active','url'=>'');

                    $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                    $warehouse_list = array();
                    $data['country_id'] = 0;
                    if($task_access == 2)
                    {
                        $data['country_id'] = $country_id;
                        $data['warehouse_list'] = $this->Common_model->get_data('warehouse',array('country_id'=>$country_id,'status'=>1));
                    }
                    else if($this->session->userdata('header_country_id')!='')
                    {
                        $data['country_id'] = $country_id;
                        $data['warehouse_list']=$this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
                    }
                    $data['form_action'] = SITE_URL.'inventory_to_buffer';
                    $data['cancel'] = SITE_URL;
                    $data['flag']=1;
                    $data['task_access']=$task_access;
                    $this->load->view('transfer/warehouse_list',$data); 
                }
            }
        }

        if(@$warehouse_id != '')
        {
            $_SESSION['ib_wh_id'] = $warehouse_id;
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/buffer.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Inventory To Buffer';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            if($task_access == 3)
            {
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Warehouse','class'=>'','url'=>SITE_URL.'inventory_to_buffer');
            }
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Inventory To Buffer','class'=>'active','url'=>'');

            // Logic For Cart Functionality 
            if(isset($_POST['add']))
            {
                if(!isset($_SESSION['ib_asset_id']))
                {
                    $_SESSION['ib_asset_id'] = array();
                }
                $a = count(@$_POST['asset_id']);
                for ($i=0; $i < $a; $i++)
                {   
                    $_SESSION['ib_asset_id'][@$_POST['asset_id'][$i]] = @$_POST['asset_id'][$i];
                }
            }
            if(!isset($_POST['reset']) && !isset($_POST['search']) && !isset($_POST['add']) && !isset($_POST['ib_asset_id'])  )
            {            
                if(isset($_SESSION['ib_first_url_count']))
                {
                    $_SESSION['ib_last_url_count'] = base_url(uri_string());
                    if(!strcmp($_SESSION['ib_first_url_count'],$_SESSION['ib_last_url_count']))
                    {                
                        if(isset($_SESSION['ib_asset_id'])) unset($_SESSION['ib_asset_id']);
                    }
                }
                else
                {  
                    $_SESSION['ib_first_url_count'] = base_url(uri_string());
                    if(isset($_SESSION['ib_asset_id'])) unset($_SESSION['ib_asset_id']);
                }
            }

            $psearch=validate_string($this->input->post('search', TRUE));
            if($psearch!='') 
            {
                $searchParams=array(
                    'part_number'      => validate_string($this->input->post('part_number', TRUE)),
                    'part_description' => validate_string($this->input->post('part_description', TRUE)),
                    'modality_id'      => validate_string($this->input->post('modality_id', TRUE)),
                    'asset_number'     => validate_string($this->input->post('asset_number', TRUE)),
                    'serial_no'        => validate_string($this->input->post('serial_no',TRUE))
                );
                $this->session->set_userdata($searchParams);
            } 
            else 
            {

                if($this->input->post('reset')!='')
                {
                    $searchParams=array(
                        'part_number'       => '',
                        'part_description'  => '',  
                        'modality_id'       => '',
                        'asset_number'      => '',
                        'serial_no'         => ''
                    );
                    $this->session->set_userdata($searchParams);
                }

                else if($this->uri->segment(2)!='')
                {
                    $searchParams=array(
                        'part_number'      => $this->session->userdata('part_number'),
                        'part_description' => $this->session->userdata('part_description'),
                        'modality_id'      => $this->session->userdata('modality_id'),
                        'asset_number'     => $this->session->userdata('asset_number'),
                        'serial_no'        => $this->session->userdata('serial_no')
                    );
                }
                else 
                {
                    $searchParams=array(
                        'part_number'       => '',
                        'part_description'  => '',  
                        'modality_id'       => '',
                        'asset_number'      => '',
                        'serial_no'         => ''
                    );
                    $this->session->set_userdata($searchParams);
                }
                
            }
            $data['searchParams'] = $searchParams;

            # Default Records Per Page - always 10
            /* pagination start */
            $config = get_paginationConfig();
            $config['base_url'] = SITE_URL.'inventory_to_buffer/';

            # Total Records
            $config['total_rows'] = $this->Buffer_m->inventory_buffer_total_num_rows($searchParams,$warehouse_id);
            $config['per_page'] = getDefaultPerPageRecords();
            $data['total_rows'] = $config['total_rows'];
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            
            if ($data['pagination_links'] != '') {
                $data['last'] = $this->pagination->cur_page * $config['per_page'];
                if ($data['last'] > $data['total_rows']) {
                    $data['last'] = $data['total_rows'];
                }
                $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
            }
            $data['sn'] = $current_offset + 1;
            /* pagination end */
            # Loading the data array to send to View
            $data['toolResults'] = $this->Buffer_m->inventory_buffer_results($current_offset, $config['per_page'], $searchParams,$warehouse_id);
            
            
            # Additional data
            $data['displayResults'] = 1;
            $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
            $wrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$warehouse_id));
            $data['wh_name'] = $wrow['wh_code'].' -('.$wrow['name'].')';
            $data['warehouse_id'] = $wrow['wh_id'];
            $data['current_offset'] = $current_offset;

            $this->load->view('buffer/inventory_to_buffer_view',$data);
        }
    }

    public function issue_inventory_to_buffer()
    {
        $data['nestedView']['heading']="Confirm Assets List";
        $data['nestedView']['cur_page'] = 'check_buffer_to_inventory';
        $data['nestedView']['parent_page'] = 'inventory_to_buffer'; 

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/buffer.js"></script>';
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Confirm Assets List';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        if($task_access == 3)
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Warehouse','class'=>'','url'=>SITE_URL.'inventory_to_buffer');
        }
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Inventory To Buffer','class'=>'','url'=>SITE_URL.'inventory_to_buffer');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Confirm Assets List','class'=>'active','url'=>'');

        if(isset($_SESSION['ib_asset_id']))
        {
            $result_data = array();
            foreach ($_SESSION['ib_asset_id'] as $key => $value)
            {
                $result_data[] = $this->Buffer_m->get_asset_based_on_id_ib($value);
                $asset_id = $value;
            }            
            $data['asset_list'] = $result_data;
        }
        $warehouse_id = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'wh_id');
        $wrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$warehouse_id));
        $data['wh_name'] = $wrow['wh_code'].' -('.$wrow['name'].')';
        $data['warehouse_id'] = $wrow['wh_id'];
        $data['form_action'] = SITE_URL.'submit_inventory_to_buffer';

        $this->load->view('buffer/issue_inventory_to_buffer',$data);
    }
    public function submit_inventory_to_buffer()
    {
        $ses_asset_id = $this->session->userdata('ib_asset_id');
        if($ses_asset_id=='')
        {
            redirect(SITE_URL.'inventory_to_buffer'); exit();
        }
        $warehouse_id = $this->input->post('warehouse_id',TRUE);
        $country_id = $this->Common_model->get_value('warehouse',array('wh_id'=>$warehouse_id),'country_id');
        $buffer_wh_id = $this->Common_model->get_value('warehouse',array('country_id'=>$country_id,'status'=>3),'wh_id');

        $this->db->trans_begin();
        foreach ($ses_asset_id as $key => $value) 
        {
            #audit data
            $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$value));
            $old_data = array(
                'wh_id'             => $asset_arr['wh_id'],
                'tool_availability' => get_asset_position($value)
            );

            $update_a = array('wh_id'         => $buffer_wh_id,
                              'modified_by'   => $this->session->userdata('sso_id'),
                              'modified_time' => date('Y-m-d H:i:s'));
            $update_a_where = array('asset_id' => $value);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            $update_ap = array('to_date' => date('Y-m-d'));
            $update_ap_where = array('asset_id' => $value);
            $this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);

            $insert_ap = array('asset_id' => $value,
                               'wh_id'    => $buffer_wh_id,
                               'transit'  => 0,
                               'from_date'=> date('Y-m-d'),
                               'status'   => 1);
            $this->Common_model->insert_data('asset_position',$insert_ap);

            #audit data
            $new_data = array(
                'wh_id'             => $buffer_wh_id,
                'tool_availability' => get_asset_position($value)
            );
            $remarks = "Asset moved from Inventory To Buffer";
            audit_data('asset_master',$value,'asset',2,'',$new_data,array('asset_id'=>$value),$old_data,$remarks,'',array(),'','',$asset_arr['country_id']);

            $update_ah = array('to_date' => date('Y-m-d'));
            $update_ah_where = array('asset_id' => $value);
            $this->Common_model->update_data('asset_history',$update_ah,$update_ah_where);

            $insert_ah = array('asset_id' => $value,
                               'wh_id'    => $buffer_wh_id,
                               'from_date'=> date('Y-m-d'),
                               'sub_inventory' => NULL);
            $this->Common_model->insert_data('asset_history',$insert_ah);

        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'inventory_to_buffer'); exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $wh = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$warehouse_id));
            $display = $wh['wh_code'].' -('.$wh['name'].')';
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Warehouse : <strong>'.$display.'</strong>  Assets has been moved successfully to <strong>Buffer Stock </strong> !
            </div>');
            redirect(SITE_URL.'inventory_to_buffer'); exit();
        }
    }

    public function remove_buffer_asset_from_cart()
    {
        $asset_id = validate_number($this->input->post('asset_id',TRUE));
        if(isset($_SESSION['b_asset_id'][$asset_id]))
        {
            unset($_SESSION['b_asset_id'][$asset_id]);
            echo 1;
        }

        if(isset($_SESSION['ib_asset_id'][$asset_id]))
        {
            unset($_SESSION['ib_asset_id'][$asset_id]);
            echo 1;
        }
    }
}