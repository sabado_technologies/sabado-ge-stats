<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
 * created by Roopa on 17th june 2017 11:00AM
*/
class Branch extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Branch_m');
	}
    //Branch Ware House View..
	public function branch()
    {
        
        $data['nestedView']['heading']="Manage Warehouse";
		$data['nestedView']['cur_page'] = 'manage_warehouse';
		$data['nestedView']['parent_page'] = 'manage_warehouse';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Manage Warehouse';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Warehouse','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchbranch', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'name'       => validate_string($this->input->post('branch_name', TRUE)),
            'wh_code'    => validate_string($this->input->post('wh_code', TRUE)),
            'country_id' => validate_number($this->input->post('country_id',TRUE)));
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
            'name'       => $this->session->userdata('name'),
            'wh_code'    => $this->session->userdata('wh_code'),
            'country_id' => $this->session->userdata('country_id'));
            }
            else {
                $searchParams=array(
                'name'       => '',
                'wh_code'    => '',
                'country_id' => '');
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'branch/';
        # Total Records
        $config['total_rows'] = $this->Branch_m->branch_total_num_rows($searchParams,$task_access); 
        
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['branch_results'] = $this->Branch_m->branch_results($current_offset, $config['per_page'], $searchParams,$task_access);
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('branch/branch_view',$data);

    }

    //add Branch..
    public function add_branch()
    {
        
        $data['nestedView']['heading']="Add Warehouse";
        $data['nestedView']['cur_page'] = 'check_warehouse_master';
        $data['nestedView']['parent_page'] = 'manage_warehouse';
        
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Select Country";
                $data['nestedView']['cur_page'] = 'check_warehouse_user';
                $data['nestedView']['parent_page'] = 'manage_warehouse';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/branch.js"></script>';
                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Warehouse','class'=>'','url'=>SITE_URL.'branch');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_branch';
                $data['cancel']=SITE_URL.'branch';
                $this->load->view('user/intermediate_page',$data);
            }
        }
        if(@$country_id != '')
        {
    		# include files
    		$data['nestedView']['js_includes'] = array();
    		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/branch.js"></script>';
    		$data['nestedView']['css_includes'] = array();

    		# Breadcrumbs
    		$data['nestedView']['breadCrumbTite'] = 'Add Warehouse';
    		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
    		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Warehouse','class'=>'','url'=>SITE_URL.'branch');
    		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add Warehouse','class'=>'active','url'=>'');

            # Additional data        
            $data['area']= $this->Branch_m->get_area_location($country_id);
            $data['country_id']=$country_id;
           
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['flg'] = 1;
            $data['form_action'] = SITE_URL.'insert_branch';
            $data['displayResults'] = 0;
            $this->load->view('branch/branch_view',$data);
        }
    }
    //insert branch..
    public function insert_branch()    
    {
        $task_access = page_access_check('manage_warehouse');

        $name = validate_string($this->input->post('name', TRUE)); 
        $branch_code= validate_string($this->input->post('wh_code',TRUE));
        $country_id = validate_number($this->input->post('c_id',TRUE)); 
        $branch_id=0; 
        $name_available = $this->Branch_m->is_branchnameExist($name,$branch_id,$country_id);
        $code_available = $this->Branch_m->is_branchcodeExist($branch_code,$branch_id,$country_id);
        if(checkAsean($country_id,1))
        {
           $whsData  = $this->Common_model->get_data('warehouse',array('country_id'=>$country_id,'status'=>1));                
           if(count($whsData)!=0){
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> For ASEAN Country, We can not add more than one warehouse.</div>'); 
                redirect(SITE_URL.'branch'); exit();   
           }
        }
        if($name_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Warehouse: '.$name.' already exist! please check.</div>'); 
            redirect(SITE_URL.'branch'); exit();
        }
        if($code_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Warehouse Code: '.$branch_code.' already exist! please check.
            </div>'); 
            redirect(SITE_URL.'branch'); exit();
        }
        $info_name_result=array(); 
        $info_name=$this->input->post('info_name',TRUE);
        $info_contact=$this->input->post('info_number',TRUE);
        $info_mail=$this->input->post('info_mail',TRUE);
       
        foreach($info_name as $key=>$value)
        {
            $info_name_result[]=$value.','.$info_contact[$key].','.$info_mail[$key];
        }
        $data = array(
                'name'          => $name,
                'country_id'    => $country_id,
                'location_id'   => validate_string($this->input->post('area',TRUE)),
                'phone'         => validate_string($this->input->post('contact_no',TRUE)),
                'pin_code'      => validate_string($this->input->post('pin_no',TRUE)),
                'tin_number'    => validate_string($this->input->post('tin_number',TRUE)),                    
                'cst_number'    => validate_string($this->input->post('cst_number',TRUE)),
                'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
                'pan_number'    => validate_string($this->input->post('pan_no',TRUE)),
                'contact_info1' => $info_name_result[0],
                'contact_info2' => $info_name_result[1],
                'contact_info3' => $info_name_result[2],
                'wh_code'       => $branch_code,
                'managed_by'    => validate_string($this->input->post('managed_by',TRUE)),
                'email'         => validate_string($this->input->post('contact_name',TRUE)),  
                'address1'      => validate_string($this->input->post('address_1',TRUE)),
                'address2'      => validate_string($this->input->post('address_2',TRUE)),
                'address3'      => validate_string($this->input->post('address_3',TRUE)),
                'address4'      => validate_string($this->input->post('address_4',TRUE)),                  
                'created_by'    => $this->session->userdata('sso_id'),
                'created_time'  => date('Y-m-d H:i:s')
                      );
        $this->db->trans_begin();
        $this->Common_model->insert_data('warehouse',$data);
        if($this->db->trans_status()===FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong>Something Went Wrong! Please check.
             </div>');
        }
        else
        {
            $this->db->trans_commit(); 
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Warehouse : <strong>'.$branch_code.'</strong> has been Added successfully!
             </div>');   
        }
        redirect(SITE_URL.'branch');  
    }
    //edit branch..
    public function edit_branch()
    {
        $branch_id=@storm_decode($this->uri->segment(2));
        if($branch_id=='')
        {
            redirect(SITE_URL.'branch');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Warehouse";
        $data['nestedView']['cur_page'] = 'check_warehouse_master';
        $data['nestedView']['parent_page'] = 'manage_warehouse';
        $task_access = page_access_check($data['nestedView']['parent_page']);

        $country_id = $this->Common_model->get_value('warehouse',array('wh_id'=>$branch_id),'country_id');
        if(@$country_id != '')
        {
        # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/branch.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit Warehouse';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Warehouse','class'=>'','url'=>SITE_URL.'branch');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Warehouse','class'=>'active','url'=>'');

            # Additional data
            $data['flg'] = 2;
            $data['form_action'] = SITE_URL.'update_branch';
            $data['display_results'] = 0;

            # Data
            $row = $this->Common_model->get_data('warehouse',array('wh_id'=>$branch_id));
            foreach($row as $value)
            {
               $data['contact_info1']=explode(',', $value['contact_info1']);
               $data['contact_info2']=explode(',', $value['contact_info2']);
               $data['contact_info3']=explode(',', $value['contact_info3']);
            }
            
            $data['branch_row'] = $row[0];
            $data['country_id'] = $country_id;
            
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['area'] = $this->Branch_m->get_area_location($country_id);
            $this->load->view('branch/branch_view',$data);
        }
    }

    //update branch...
    public function update_branch()
    {
        $task_access = page_access_check('manage_warehouse');

        $branch_id=validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($branch_id==''){
            redirect(SITE_URL.'branch');
            exit;
        }
        $name = validate_string($this->input->post('name', TRUE)); 
        $branch_code= validate_string($this->input->post('wh_code',TRUE));  
        $country_id = validate_number($this->input->post('c_id',TRUE));
        $name_available = $this->Branch_m->is_branchnameExist($name,$branch_id,$country_id);
        $code_available = $this->Branch_m->is_branchcodeExist($branch_code,$branch_id,$country_id);
        if($name_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Warehouse: '.$name.' already exist! please check.
            </div>'); 
            redirect(SITE_URL.'branch'); exit();
        }
        if($code_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Warehouse Code: '.$branch_code.' already exist! please check.
            </div>'); 
            redirect(SITE_URL.'branch'); exit();
        }
        $info_name_result=array(); 
        $info_name=$this->input->post('info_name',TRUE);
        $info_contact=$this->input->post('info_number',TRUE);
        $info_mail=$this->input->post('info_mail',TRUE);
       
        foreach($info_name as $key=>$value)
        {
            $info_name_result[]=$value.','.$info_contact[$key].','.$info_mail[$key];
        }
        $data = array(
                'name'          => $name,   
                'country_id'    => $country_id,                     
                'location_id'   => validate_string($this->input->post('area',TRUE)),                
                'phone'         => validate_string($this->input->post('contact_no',TRUE)),
                'pin_code'      => validate_string($this->input->post('pin_no',TRUE)),
                'tin_number'    => validate_string($this->input->post('tin_number',TRUE)),                    
                'cst_number'    => validate_string($this->input->post('cst_number',TRUE)),
                'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
                'pan_number'    => validate_string($this->input->post('pan_no',TRUE)),
                'contact_info1' => $info_name_result[0],
                'contact_info2' => $info_name_result[1],
                'contact_info3' => $info_name_result[2],
                'wh_code'       => validate_string($this->input->post('wh_code',TRUE)),
                'managed_by'    => validate_string($this->input->post('managed_by',TRUE)),
                'email'         => validate_string($this->input->post('contact_name',TRUE)), 
                'address1'      => validate_string($this->input->post('address_1',TRUE)),
                'address2'      => validate_string($this->input->post('address_2',TRUE)),
                'address3'      => validate_string($this->input->post('address_3',TRUE)),
                'address4'      => validate_string($this->input->post('address_4',TRUE)),                   
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
                      );

        $where = array('wh_id'=>$branch_id);
        $this->db->trans_begin();
        $this->Common_model->update_data('warehouse',$data,$where);
        if($this->db->trans_status()===FALSE)
        {
            $this->db->trans_rollback(); 
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong>Something Went Wrong! Please check.
             </div>');            
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Warehouse : <strong>'.$branch_code.'</strong> has been updated successfully!
         </div>');
        }
        redirect(SITE_URL.'branch');
    }
    //deactivate warehouse
    public function deactivate_branch($encoded_id)
    {
        $task_access = page_access_check('manage_warehouse');
        $wh_id=@storm_decode($encoded_id);
        if($wh_id==''){
            redirect(SITE_URL.'branch');
            exit;
        }
        $where = array('wh_id' => $wh_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('warehouse',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Warehouse has been deactivated successfully!
         </div>');
        redirect(SITE_URL.'branch');

    }
    //activate warehouse
    public function activate_branch($encoded_id)
    {
        $task_access = page_access_check('manage_warehouse');
        $wh_id=@storm_decode($encoded_id);
        if($wh_id==''){
            redirect(SITE_URL.'branch');
            exit;
        }
        $where = array('wh_id' => $wh_id);
        //activating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('warehouse',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Warehouse has been Activated successfully!
         </div>');
        redirect(SITE_URL.'branch');
    }
    public function is_branchnameExist()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $branch_id = validate_number($this->input->post('branch_id',TRUE));
        $country_id = $this->input->post('country_id');
        $result = $this->Branch_m->is_branchnameExist($name,$branch_id,$country_id);
        echo $result;
    }
    public function is_branchcodeExist()
    {
        $branch_code = validate_string($this->input->post('branch_code',TRUE));
        $branch_id = validate_number($this->input->post('branch_id',TRUE));
        $country_id = $this->input->post('country_id');
        $result = $this->Branch_m->is_branchcodeExist($branch_code,$branch_id,$country_id);
        echo $result;
    }
}