<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By Maruthi on 20th july 17 8:30PM

class Stock_transfer extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Stock_transfer_m');
        $this->load->model('Order_m');
        $this->load->model('Fe2fe_m');;
        
    }

    public function raiseSTforOrder()
    {
        unset($_SESSION['insert_assigned_tools']);
        $task_access = page_access_check('raiseSTforOrder');
        $data['nestedView']['heading']="Raise ST For Order";
        $data['nestedView']['cur_page'] = 'raiseSTforOrder';
        $data['nestedView']['parent_page'] = 'raiseSTforOrder';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/raiseSTforOrder.js"></script>';
        $data['nestedView']['css_includes'] = array();
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Raise ST For Order';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise Stock Transfer','class'=>'active','url'=>'');

        # Search Functionality       
        $psearch=$this->input->post('raiseSTforOrder', TRUE);
        if($psearch!='') 
        {
            $searchParams=array(
                'st_order_number' => validate_string($this->input->post('order_number', TRUE)),
                'st_sso_id'       => validate_string($this->input->post('sso_id', TRUE)),
                'st_request_date' => validate_string($this->input->post('request_date', TRUE)),
                'st_return_date'  => validate_string($this->input->post('return_date', TRUE)),
                'st_country_id'   => validate_string($this->input->post('st_country_id', TRUE)),
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'st_order_number' => $this->session->userdata('st_order_number'),
                    'st_sso_id'       => $this->session->userdata('st_sso_id'),
                    'st_request_date' => $this->session->userdata('st_request_date'),
                    'st_return_date'  => $this->session->userdata('st_return_date'),
                    'st_country_id'   => $this->session->userdata('st_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                    'st_order_number' => '',
                    'st_sso_id'       => '',
                    'st_request_date' => '',
                    'st_return_date'  => '',
                    'st_country_id'   => '',
                );
                $this->session->set_userdata($searchParams);
            }            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'raiseSTforOrder/';

        # Total Records
        $config['total_rows'] = $this->Stock_transfer_m->STforOrder_tool_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Stock_transfer_m->STforOrder_tool_results($current_offset, $config['per_page'], $searchParams,$task_access);       
        
        # Additional data
        $data['displayResults'] = 1;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $this->load->view('stock_transfer/stock_transfer',$data);
    }

   public function assignSTtoolsForOrder()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'raiseSTforOrder');
            exit;
        }
        $data['tool_order_id'] = $tool_order_id;
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Assign ST Tools For Order ";
        $data['nestedView']['cur_page'] = 'raiseSTforOrderSecond';
        $data['nestedView']['parent_page'] = 'raiseSTforOrder';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/st.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'ST For Order';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise ST For Order','class'=>'','url'=>SITE_URL.'raiseSTforOrder');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Assign ST Tools For Order','class'=>'active','url'=>'');

        # Additional data
        $ordered_tools = $this->Stock_transfer_m->get_ordered_tool_info($tool_order_id,2);  
        //echo "<pre>";print_r($ordered_tools);exit;
        $orderInfo = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $ordered_from_wh = $orderInfo['wh_id'];
        $ordered_sso_id = $orderInfo['sso_id'];

        $whs = getWhArrNotInID($ordered_from_wh,$ordered_sso_id);
        $tools_info = array();
        $new_order_tools = array();
        foreach ($ordered_tools as $key1 => $value1)
        {
            $checkPSFromDedicatedWh = checkPSFromDedicatedWh($tool_order_id);
            // echo $checkPSFromDedicatedWh;exit;
            if($checkPSFromDedicatedWh == 0)
            {
                $ack_qty = quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$value1['tool_id']);            
                //echo $ack_qty;exit;
            }
            else
            {
                $ack_qty = quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$value1['tool_id'],1);
            }
            $in_process_qty = quantityInProcessByOtherStockTransfer($tool_order_id,$value1['tool_id']);
            // echo $in_process_qty;exit;
            $final_qty = $ack_qty + $in_process_qty;
            if($value1['quantity'] > $final_qty) // there are some of the tools which needs the st
            {   
                foreach ($whs as $key => $value2) // looping the warehouse and counting the availability
                {
                    $condition_array= array('tool_id'=>$value1['tool_id'],'wh_id'=>$value2['wh_id']);
                    $avail_tools = available_assets($condition_array);
                    if(count($avail_tools)>0)
                    {
                        $tools_info[$value1['tool_id']][$value2['wh_id']]['wh_name'] = $value2['name'];
                        $tools_info[$value1['tool_id']][$value2['wh_id']]['wh_id'] = $value2['wh_id'];
                        $tools_info[$value1['tool_id']][$value2['wh_id']]['wh_code'] = $value2['wh_code'];
                        $tools_info[$value1['tool_id']][$value2['wh_id']]['wh_qty'] = count($avail_tools);
                    }                    
                }
                if($checkPSFromDedicatedWh == 0)
                {
                    $new_order_tools[$key1] = $value1; 
                    $new_order_tools[$key1]['final_qty'] = ($final_qty - $ack_qty);
                    // meansrequired final qty is 0
                    //quantity-available_quantity-final_qty'

                }
                else  
                {                    
                    $new_order_tools[$key1] = $value1;
                    $new_order_tools[$key1]["available_quantity"] = 0;
                    $new_order_tools[$key1]['final_qty'] = $final_qty;
                    // making the available qty = 0 and because finall qty incrementing means some of the assets in progress and ack
                }
               
            }              
        }
        $data['tools_info'] = $tools_info;
        $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);
        $data['wh_data'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>getWHByUser($data['order_info']['sso_id'])));
        $data['ordered_tools'] = $new_order_tools;
        // enable or disable ship to to old ship to 
        $oldStData = $this->Common_model->get_data('st_tool_order',array('tool_order_id'=>$tool_order_id,'status'=>1));
        
        # once st has done then all the sts should pass the same sequence
        if(count($oldStData)>0)
        {
            if($data['order_info']['order_delivery_type_id'] == 2)
            {
                $data['fe_check'] = 0;
            }
            else
            {
                // get old ship to type and same should pass the new one also
                $fe_check_data = $this->Stock_transfer_m->getOldFeCheck($tool_order_id);
                $fe_check = $fe_check_data[0]['fe_check'];
                $data['fe_check'] = $fe_check;
            }
        }
        $this->load->view('stock_transfer/assignSTtools',$data);
    }
    
    public function insertAssignedSTtools()
    {
        $_SESSION['insert_assigned_tools']  =1;
        $post_qty = $this->input->post('post_qty',TRUE);
        $post_od_tool = $this->input->post('post_od_tool',TRUE);
        $fe_check = $this->input->post('fe_check',TRUE);
        $od_req_qty = $this->input->post('od_req_qty',TRUE);
        $tool_order_id = validate_string($this->input->post('tool_order_id',TRUE));
        $order_data = $this->Order_m->get_order_info($tool_order_id);
        $sts_arr = array();
        $this->db->trans_begin();

        # TODO
        // checking the conditioon when zonal or national person raises the order and wh_id and to_wh_id not equal 
        // if above is true then unlock the assets which are locked in wh_id and raise ST from wh_id to to_wh_id
        # End of TODO
        // unlocking the asset for order and locking for ST
        if($fe_check == 1)
        {
            $ordered_tools = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status<'=>3));
            if(count($ordered_tools))
            {   
                foreach ($ordered_tools as $key => $values)
                {
                    // if already stock transfer raises then no need to unlock the assets because when first time doing stock transfer only unlocking
                    $ack_qty = quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$values['tool_id']);                       
                    $in_process_qty = quantityInProcessByOtherStockTransfer($tool_order_id,$values['tool_id']);
                    $final_qty = $ack_qty + $in_process_qty;
                    if($final_qty != 0) { break; }  // this condition helps to skip the assets to unlock every time except for first time
                    else
                    {
                        if($values['available_quantity'] == 0) continue;
                        else $unlockAndLockForST = 1;
                    }

                    $ordered_tool_id = $values['ordered_tool_id'];
                    $available_quantity=@$values['available_quantity'];
                    $quantity=@$values['quantity'];
                    $current_stage_id=$this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'current_stage_id');

                    if(@$unlockAndLockForST == 1) // if any assets need to unlock then only update the order with ordered tools and assets
                    {
                        if(!isset($main_ad))
                        {
                            # Audit Start for Tool Order
                            $remarksString = "As Some of the Quantity not available in Assigned Warehouse, System Unlocking the assets to Tool Order and locking to Stock Transfer to Ship as ST for FE";
                            $blockArr =array('blockName'=> "Tool Order") ;
                            $finalArr = array('transaction_remarks'=>$remarksString);
                            $oldData = array();                            
                            $main_ad = tool_order_audit_data("tool_order",$tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$oldData,$remarksString,'',array(),"tool_order",$tool_order_id,$order_data['country_id']);    
                        }
                    }
                    
                    $unlockAndLockForST = 0;  // below linked order assets and main needed locaked_assets defines do we need to create st or not
                    if($available_quantity > 0)
                    {                       
                        // as it was transaction quanity 
                        $main_needed_locked_assets = $available_quantity;
                        if(@$main_needed_locked_assets > 0)
                        { 
                            $locked_assets = $this->Order_m->get_locked_assets($ordered_tool_id,$available_quantity);
                            if(($locked_assets))
                            {
                                 # Audit Start 
                                $auditOldData = array(
                                    'quantity'           => $values['quantity'],
                                    'available_quantity' => $values['available_quantity']                            
                                );
                                $auditNewArr = array(
                                    'quantity'           => $values['quantity'],
                                    'available_quantity' => NULL,
                                    'tool_status'        => "Unlocked"
                                );

                                $auditBlock = array("tool_id"=> $values['tool_id']);
                                $auditOrderedToolId = tool_order_audit_data("ordered_tools",$ordered_tool_id,'ordered_tool',2,$main_ad,$auditNewArr,$auditBlock,$auditOldData,'','',array(),"tool_order",$tool_order_id,$order_data['country_id']); 
                                $unlockAndLockForST  = 1; // means one of the asset is unlocked so that st can create
                                foreach ($locked_assets as $key => $value)
                                {    
                                    $asset_main_status = $this->Common_model->get_value('asset',array('asset_id'=>$value['asset_id']),'status');
                                    $asset_actual_status = ($asset_main_status==2)?1:$asset_main_status;
                                    $auditAssetRemarks = "Unlocked for ".$order_data['order_number'];
                                    assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"locked_assets",2,$auditOrderedToolId,'','',"tool_order",$tool_order_id,$order_data['country_id']);
                                    assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$main_ad,"tool_order",$tool_order_id,$order_data['country_id']);
                                    
                                    # Updating Asset
                                    $assetUpdateData = array(
                                        'status'        => $asset_actual_status,
                                        'modified_by'   => $this->session->userdata('sso_id'),
                                        'modified_time' => date('Y-m-d H:i:s')
                                    );
                                    $assetUpdateWhere = array('asset_id'=>$value['asset_id']);
                                    $this->Common_model->update_data('asset',$assetUpdateData,$assetUpdateWhere);

                                    # Update insert asset status history
                                    $assetStatusHistoryArray = array(
                                        'asset_id'          => $value['asset_id'],
                                        'created_by'        => $this->session->userdata('sso_id'),
                                        'status'            => $asset_actual_status,
                                        'ordered_tool_id'   => $ordered_tool_id, // gives new tool_order id
                                        'current_stage_id'  => 4,
                                        'tool_order_id'     => $tool_order_id
                                    );
                                    updateInsertAssetStatusHistory($assetStatusHistoryArray);

                                    $main_needed_locked_assets--;                       
                                    if($main_needed_locked_assets == 0)
                                        break;
                                }
                                # Audit Update
                                $auditUpdateData = array(
                                        'quantity'               => $quantity,
                                        'available_quantity'     => 0
                                );
                                updatingAuditData($auditOrderedToolId,$auditUpdateData);
                                // important note:
                                # as we are doing audit here to lock the same list of assets we are are not updating the ordered tool available qty here 
                                # at the end of locking to new order only we are making the tool order ordered tool to zero
                            }
                        }
                    }
                }

                if(@$unlockAndLockForST == 1)
                {
                    // preparing data to insert order table 
                    $ordered_sso = $order_data['sso_id'];
                    $transCountryId = $order_data['country_id'];
                    $f_order_number = getSTOrderNumber($transCountryId);
                    $new_order_delivery_type_id = $order_data['order_delivery_type_id'];

                    $stn_number = "ST".$f_order_number;
                    $sts_arr[] = $stn_number;
                    $st_data = array(
                        'sso_id'                     => $ordered_sso,
                        'current_stage_id'           => 1,
                        'stn_number'                 => $stn_number,
                        'wh_id'                      => $order_data['wh_id'],
                        'to_wh_id'                   => $order_data['to_wh_id'],
                        'country_id'                 => $transCountryId,
                        'fe_check'                   => $fe_check,
                        'order_delivery_type_id'     => $new_order_delivery_type_id,
                        'order_type'                 => 1,
                        'request_date'               => $order_data['request_date'],
                        'return_date'                => $order_data['return_date'],   
                        'deploy_date'                => $order_data['deploy_date'],
                        'created_by'                 => $this->session->userdata('sso_id'),
                        'created_time'               => date('Y-m-d H:i:s')
                    );
                    $st_order_id = $this->Common_model->insert_data('tool_order',$st_data);

                    $order_address_data = array(
                        'address1'      => $order_data['address1'],
                        'address2'      => $order_data['address2'],
                        'address3'      => $order_data['address3'],
                        'address4'      => $order_data['address4'],
                        'pin_code'      => $order_data['pin_code'],
                        'site_id'       => @$order_data['site_id'],
                        'gst_number'    => @$order_data['gst_number'],
                        'pan_number'    => @$order_data['pan_number'],
                        'system_id'     => $order_data['system_id'],
                        'tool_order_id' => $st_order_id,
                        'location_id'   => @$order_data['location_id'],
                        'created_by'    => $this->session->userdata('sso_id'),
                        'created_time'  => date('Y-m-d H:i:s')
                    );
                    $order_address_id = $this->Common_model->insert_data('order_address',$order_address_data);

                    if($new_order_delivery_type_id !=1)
                    {
                        unset($order_address_data['site_id'],$order_address_data['system_id']);
                    }
                    unset($order_address_data['created_by'],$order_address_data['created_time'],$order_address_data['tool_order_id']);

                    $st_data = unsetStockTransferData($st_data);
                    $finalArr = array_merge($st_data,$order_address_data);
                    $blockArr  = array('blockName'=> "Stock Transfer");
                    $remarksString = "Stock Transfer Created"; 
                    $ad_id = tool_order_audit_data('stock_transfer',$st_order_id,'tool_order',1,'',$st_data,$blockArr,array(),$remarksString,'',array(),"tool_order",$tool_order_id,$transCountryId);

                    $order_status_data = array(
                        'tool_order_id'         => $st_order_id,
                        'current_stage_id'      => 1,
                        'created_by'            => $this->session->userdata('sso_id'),
                        'created_time'          => date('Y-m-d H:i:s')
                        );
                    $st_order_status_id = $this->Common_model->insert_data('order_status_history',$order_status_data);
                    // insert into st tool order
                    $this->Common_model->insert_data('st_tool_order',array('stock_transfer_id'=>$st_order_id,'tool_order_id'=>$tool_order_id));    
                    $ordered_tools = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status<'=>3));
                    foreach ($ordered_tools as $key => $value) 
                    {             
                        $ordered_tool_id = $value['ordered_tool_id'];
                        $tool_id = $value['tool_id'];
                        $main_available_assets = $value['available_quantity'];
                        // stock transfer ordered tool
                        if($main_available_assets > 0)  // asking the wh guy to lock the only the tools which linked for order
                        {
                            $order_tool_data = array(
                                'tool_order_id'      => $st_order_id,
                                'tool_id'            => $tool_id,
                                'available_quantity' => 0,
                                'quantity'           => $main_available_assets,
                                'status'             => 1,
                                'created_by'         => $this->session->userdata('sso_id'),
                                'created_time'       => date('Y-m-d H:i:s')
                            ); 
                            $new_ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);
                            $order_tool_data = unsetOrderedToolsExceptionColumns($order_tool_data);
                            $order_tool_data['tool_status'] = "Added";
                            $auditOrderedToolBlock = array('tool_id'=>$ordered_tool_id);
                            $auditOrderedToolId = tool_order_audit_data('st_ordered_tools',$new_ordered_tool_id,'ordered_tool',1,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$st_order_id,$transCountryId);

                            $searchParams = array(
                                    'wh_id'     => $order_data['wh_id'],
                                    'tool_id'   => $tool_id
                                );
                            $available_assets = available_assets($searchParams);
                            $available_assets_count = count($available_assets);               

                            // looping all the assets and assigning to lock In period on basis of First Come First Locked In till Ordered Quantity
                            $available_quantity = 0; 
                            if(count($available_assets_count) > 0)
                            {
                                foreach ($available_assets as $key => $value)
                                {
                                    $available_quantity++; 
                                    // updating asset table 
                                    $asset_main_status = $value['asset_main_status'];
                                    $asset_actual_status  = ($value['asset_main_status']==1)?2:$asset_main_status;
                                    $where = array('asset_id'=>$value['asset_id']);
                                    $u_data  = array(
                                        'status'        => $asset_actual_status,
                                        'modified_by'   => $this->session->userdata('sso_id'),
                                        'modified_time' => date('Y-m-d H:i:s')
                                    );
                                    $this->Common_model->update_data('asset',$u_data,$where);

                                    # Audit Start
                                    $auditAssetRemarks = "Locked for ".$stn_number;
                                    assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"st_locked_assets",1,$auditOrderedToolId,'','',"tool_order",$st_order_id,$transCountryId);
                                    assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$transCountryId);
                                    # Audit End

                                     # Update insert asset status history
                                    $assetStatusHistoryArray = array(
                                        'asset_id'          => $value['asset_id'],
                                        'created_by'        => $this->session->userdata('sso_id'),
                                        'status'            => $asset_actual_status,
                                        'ordered_tool_id'   => $ordered_tool_id, // gives new tool_order id
                                        'current_stage_id'  => 1,
                                        'tool_order_id'     => $st_order_id
                                    );
                                    updateInsertAssetStatusHistory($assetStatusHistoryArray);
                                    if($available_quantity == $main_available_assets) break;
                                }  
                                $status = ($available_quantity == $quantity)?1:2;                                                                      
                            }
                            // updating the available assets in order when those are unlocked and locked for st
                            if($main_available_assets != $available_quantity)
                            {
                                $tool_order_again_approval = 1;
                                $status = 2;
                                $this->Common_model->update_data('st_tool_order',array('status'=>2),array('stock_transfer_id'=>$st_order_id));
                            }  

                            // updating the available quantity for ST
                            $this->Common_model->update_data('ordered_tool',array('status'=>$status,'available_quantity'=>$available_quantity),array('ordered_tool_id'=>$new_ordered_tool_id));
                            # Audit Start
                            updatingAuditData($auditOrderedToolId,array('available_quantity'=>$available_quantity));
                            # Audit End
                            # important note
                            // updating the available quantity and status for main tool order  and no need to update the Audit becuase it is doing when we are unlocking the assets
                            $this->Common_model->update_data('ordered_tool',array('available_quantity'=>0,'status'=>2),array('ordered_tool_id'=>$ordered_tool_id));
                        }
                    }

                    // send mails 
                    sendSTMails($st_order_id,1,1);
                    sendSTMails($st_order_id,2,1);
                    sendSTMails($st_order_id,3,1);
                }
            }
        }

        $new_ordered_tool_id_arr = array(); // preparing to update the available quantity when tool available in next warehoouse
        foreach ($post_qty as $wh_id => $value)
        {
            $w_data = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
            $f_order_number = getSTOrderNumber($w_data['country_id']);
            if($fe_check == 0) $new_order_delivery_type_id = 2;
            if($fe_check == 1) $new_order_delivery_type_id = $order_data['order_delivery_type_id'];
            $stn_number = "ST".$f_order_number;
            $transCountryId = $w_data['country_id'];
            $sts_arr[] = $stn_number;
            $st_data = array(
                'sso_id'                     => $order_data['sso_id'], // changed on 2nd jan'19 from session to tool order sso
                'current_stage_id'           => 1,
                'stn_number'                 => $stn_number,
                'wh_id'                      => $wh_id,
                'country_id'                 => $transCountryId,
                'to_wh_id'                   => $order_data['wh_id'],
                'fe_check'                   => $fe_check,
                'order_delivery_type_id'     => $new_order_delivery_type_id,
                'order_type'                 => 1,
                'request_date'               => $order_data['request_date'],
                'return_date'                => $order_data['return_date'],    
                'deploy_date'                => $order_data['deploy_date'],
                'created_by'                 => $this->session->userdata('sso_id'),
                'created_time'               => date('Y-m-d H:i:s')
            );
            $st_order_id = $this->Common_model->insert_data('tool_order',$st_data);
            if($fe_check == 1)
            {
                $order_address_data = array(
                    'address1'      => $order_data['address1'],
                    'address2'      => $order_data['address2'],
                    'address3'      => $order_data['address3'],
                    'address4'      => $order_data['address4'],
                    'pin_code'      => $order_data['pin_code'],
                    'remarks'       => @$order_data['address_remarks'],
                    'site_id'       => $order_data['site_id'],
                    'gst_number'    => @$order_data['gst_number'],
                    'pan_number'    => @$order_data['pan_number'],
                    'system_id'     => $order_data['system_id'],
                    'tool_order_id' => $st_order_id,
                    'location_id'   => @$order_data['location_id'],
                    'created_by'    => $this->session->userdata('sso_id'),
                    'created_time'  => date('Y-m-d H:i:s')
                );
                $order_address_id = $this->Common_model->insert_data('order_address',$order_address_data);
                if($new_order_delivery_type_id !=1)
                {
                    unset($order_address_data['site_id'],$order_address_data['system_id']);
                }
            }
            else
            {
                $to_wh_id_data = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$order_data['wh_id']));
                $order_address_data = array(
                    'tool_order_id' => $st_order_id,
                    'location_id'   => $to_wh_id_data['location_id'],
                    'address1'      => $to_wh_id_data['address1'],
                    'address2'      => $to_wh_id_data['address2'],
                    'address3'      => $to_wh_id_data['address3'],
                    'address4'      => $to_wh_id_data['address4'],
                    'pin_code'      => $to_wh_id_data['pin_code'],
                    'created_by'    => $this->session->userdata('sso_id'),
                    'created_time'  => date('Y-m-d H:i:s')
                    );
                $this->Common_model->insert_data('order_address',$order_address_data);    
            }

            unset($order_address_data['created_by'],$order_address_data['created_time'],$order_address_data['tool_order_id']);

            $st_data = unsetStockTransferData($st_data);
            $finalArr = array_merge($st_data,$order_address_data);
            $blockArr  = array('blockName'=> "Stock Transfer");
            $remarksString = "Stock Transfer Created"; 
            // echo "<pre>"; print_r($finalArr);exit;
            $ad_id = tool_order_audit_data('stock_transfer',$st_order_id,'tool_order',1,'',$st_data,$blockArr,array(),$remarksString,'',array(),"tool_order",$tool_order_id,$transCountryId);

            $order_status_data = array(
                'tool_order_id'         => $st_order_id,
                'current_stage_id'      => 1,
                'created_by'            => $this->session->userdata('sso_id'),
                'created_time'          => date('Y-m-d H:i:s')
                );
            $st_order_status_id = $this->Common_model->insert_data('order_status_history',$order_status_data);
            // insert into st tool order
            $this->Common_model->insert_data('st_tool_order',array('stock_transfer_id'=>$st_order_id,'tool_order_id'=>$tool_order_id));    
            $ordered_tool_arr = array(); // preparing to skip inserting the into ordered tool table
            foreach ($value as $ordered_tool_id => $quantity) 
            {             
                // stock transfer ordered tool
                if(!in_array($ordered_tool_id,$ordered_tool_arr))
                {
                    $order_tool_data = array(
                        'tool_order_id'      => $st_order_id,
                        'tool_id'            => $post_od_tool[$ordered_tool_id],
                        'available_quantity' => 0,
                        'quantity'           => $quantity,
                        'status'             => 1,
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s')
                    ); 

                    $new_ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);
                    $new_ordered_tool_id_arr[$ordered_tool_id] = $new_ordered_tool_id;                    
                    // pusing ordered tool id to array to skip next time inserting the 
                    //same tool quantiy and updating the quantity which is available in other warehosue   
                    $ordered_tool_arr []= $ordered_tool_id; 

                    # Audit start
                    $order_tool_data = unsetOrderedToolsExceptionColumns($order_tool_data);
                    $order_tool_data['tool_status'] = "Added";
                    $auditOrderedToolBlock = array('tool_id'=>$post_od_tool[$ordered_tool_id]);
                    // echo $transCountryId;exit;
                    $auditOrderedToolId = tool_order_audit_data('st_ordered_tools',$new_ordered_tool_id,'ordered_tool',1,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$tool_order_id,$transCountryId);
                    # pusing the values of audit ordered tool
                    $audit_new_ordered_tool_id_arr[$ordered_tool_id] = $auditOrderedToolId; 
                    # Audit end
                }

                $searchParams = array(
                        'wh_id'     => $wh_id,
                        'tool_id'   => $post_od_tool[$ordered_tool_id]
                );
                $available_assets = available_assets($searchParams);
                $available_assets_count = count($available_assets);               

                // looping all the assets and assigning to lock In period on basis of First Come First Locked In till Ordered Quantity
                if(count($available_assets_count) > 0)
                {
                    $available_quantity = 0;                    
                    foreach ($available_assets as $key => $value)
                    {
                        $available_quantity++; 
                        // updating asset table 
                        $asset_main_status = $value['asset_main_status'];
                        $asset_actual_status  = ($value['asset_main_status']==1)?2:$asset_main_status;
                        $where = array('asset_id'=>$value['asset_id']);
                        $u_data  = array(
                            'status'        => $asset_actual_status,
                            'modified_by'   => $this->session->userdata('sso_id'),
                            'modified_time' => date('Y-m-d H:i:s')
                            );
                        $this->Common_model->update_data('asset',$u_data,$where);


                        # Audit Start
                        $auditAssetRemarks = "Locked for ".$stn_number;
                            assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"st_locked_assets",1,$auditOrderedToolId,'','',"tool_order",$st_order_id,$transCountryId);
                            assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$transCountryId);
                        # Audit End

                        # Update insert asset status history
                        $assetStatusHistoryArray = array(
                            'asset_id'          => $value['asset_id'],
                            'created_by'        => $this->session->userdata('sso_id'),
                            'status'            => $asset_actual_status,
                            'ordered_tool_id'   => $new_ordered_tool_id_arr[$ordered_tool_id], // gives new tool_order id
                            'current_stage_id'  => 4,
                            'tool_order_id'     => $st_order_id
                        );
                        updateInsertAssetStatusHistory($assetStatusHistoryArray);
                        if($available_quantity == $quantity) break;
                    } 
                    $status = ($available_quantity == $quantity)?1:2;                                                                   
                }
                // worst scenario like when viewing the screen available but after submitting some other transaction available so no tools available
                else
                {
                    $tool_order_again_approval = 1;
                    $status = 2;
                    $this->Common_model->update_data('st_tool_order',array('status'=>2),array('stock_transfer_id'=>$st_order_id));
                } 

                $this->Common_model->update_data('ordered_tool',array('status'=>$status,'available_quantity'=>$available_quantity),array('ordered_tool_id'=>$new_ordered_tool_id_arr[$ordered_tool_id]));
                # Audit Start
                updatingAuditData($audit_new_ordered_tool_id_arr[$ordered_tool_id],array('available_quantity'=>$available_quantity));
                # Audit End    
            }
            sendSTMails($st_order_id,1,1);
            sendSTMails($st_order_id,2,1);
            sendSTMails($st_order_id,3,1);

        }

        if(@$unlockAndLockForST ==1)
        {
            $remarksString = "Stock Transfers ". $auditStsString." are Created for Order Number".$order_data['order_number'];
            $updateAuditData = array('remarks'=>$remarksString);
            updatingAuditData($main_ad,$updateAuditData);
        }
        else
        {
           # Audit Start for Tool Order
            $blockArr =array('blockName'=> "Tool Order") ;
            $finalArr = array();
            $oldData = array();
            $auditStsString = implode(',', $sts_arr);
            $remarksString = "Stock Transfers ". $auditStsString." are Created for Order Number".$order_data['order_number'];
            $main_ad = tool_order_audit_data("tool_order",$tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$oldData,$remarksString,'',array(),"tool_order",$tool_order_id,$order_data['country_id']);    
        }

        if($this->db->trans_status === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'openSTforOrder'); exit();
        }
        else
        {
            $sts_string = implode(',', $sts_arr);
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Stock Transfer '.$sts_string.' Has Raised Successfully For Order Number :'.$order_data['order_number'].' </div>');
            redirect(SITE_URL.'openSTforOrder'); exit();
        }
    }

    public function initiatePartialShipment()
    {       
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'raiseSTforOrder');
            exit;
        }
        $data['tool_order_id'] = $tool_order_id;
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Generate Partial Shipment For Order ";
        $data['nestedView']['cur_page'] = 'raiseSTforOrder';
        $data['nestedView']['parent_page'] = 'raiseSTforOrder';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/st.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Partial Shipment For Order';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Partial Shipment ','class'=>'','url'=>SITE_URL.'raiseSTforOrder');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Partial Shipment for Order','class'=>'active','url'=>'');

        # Additional data
        $ordered_tools = $this->Stock_transfer_m->get_ordered_tool_info($tool_order_id,0,1); // 0 for status order tool status in 1,2 - 1 for available qty greater than 1
        $ordered_from_wh = $ordered_tools[0]['wh_id'];
        $ordered_sso_id = $ordered_tools[0]['sso_id'];
        $odt_id = $ordered_tools[0]['order_delivery_type_id'];
        $whs = $this->Common_model->get_data('warehouse',array('wh_id'=>$ordered_from_wh,'status'=>1));
        $tools_info = array();
        $new_order_tools = array();
        $checkPSFromDedicatedWh = checkPSFromDedicatedWh($tool_order_id);        
        foreach ($ordered_tools as $key1 => $value1)
        {   

            if($checkPSFromDedicatedWh == 0)
            {
                $ack_qty = quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$value1['tool_id'],1);             
            }
            else
            {
                $ack_qty = quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$value1['tool_id'],1);            
            }
            $in_process_qty = quantityInProcessByOtherStockTransfer($tool_order_id,$value1['tool_id']);
            $final_qty = $ack_qty + $in_process_qty;
            if($value1['quantity'] > $final_qty)
            {   
                foreach ($whs as $key => $value2)
                {
                    $condition_array= array('tool_id'=>$value1['tool_id'],'wh_id'=>$value2['wh_id']);                    
                    $assets_array = $this->Order_m->get_locked_assets($value1['ordered_tool_id'],$value1['available_quantity']);                    
                    // get the locakets assets for sts from wh to wh for fe order 
                    if($assets_array)
                    {
                        $assets_array1 = ($assets_array)?convertIntoIndexedArray($assets_array,'asset_id'):array();
                        $assets_array = $assets_array1;
                    }
                    else
                    {
                        $assets_array = array();
                    }
                    $avail_tools = available_assets($condition_array,$assets_array);
                    // get the quantity with the assets available in dd wh,
                    // locked for tool order while st doing first time and 
                    //the assets which are received by wh which are shipped from other warehouse
                    // and finally summing of them
                    if(count($avail_tools)>0)
                    {
                        $tools_info[$value1['tool_id']][$value2['wh_id']]['wh_name'] = $value2['name'];
                        $tools_info[$value1['tool_id']][$value2['wh_id']]['wh_id'] = $value2['wh_id'];
                        $tools_info[$value1['tool_id']][$value2['wh_id']]['wh_code'] = $value2['wh_code'];
                        $tools_info[$value1['tool_id']][$value2['wh_id']]['wh_qty'] = count($avail_tools);
                    }                    
                }

                $new_order_tools[$key1] = $value1;
                $new_order_tools[$key1]["available_quantity"] = 0; // as final qty incrementing available quantiry always should be zero                                
                $new_order_tools[$key1]['final_qty'] = $final_qty;
            }              
        }        
        $data['tools_info'] = $tools_info;
        $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);
        $data['wh_data'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>getWHByUser($data['order_info']['sso_id'])));
        $data['ordered_tools'] = $new_order_tools;
        // enable or disable ship to to old ship to 
        $oldStData = $this->Common_model->get_data('st_tool_order',array('tool_order_id'=>$tool_order_id,'status'=>1));
        if(count($oldStData)>0)
        {
            if($data['order_info']['order_delivery_type_id'] == 2)
            {
                $data['fe_check'] = 0;
            }
            else
            {
                // get old ship to type
                $fe_check_data = $this->Stock_transfer_m->getOldFeCheck($tool_order_id);
                $fe_check = $fe_check_data[0]['fe_check'];
                $data['fe_check'] = $fe_check;
            }
        }
        $this->load->view('stock_transfer/assignPartialSTtools',$data);
    }

    public function insertPartiallyAssignedSTtools()
    {
        $_SESSION['insert_assigned_tools']  =1;
        $post_qty = $this->input->post('post_qty',TRUE);
        $post_od_tool = $this->input->post('post_od_tool',TRUE);
        $fe_check = $this->input->post('fe_check',TRUE);
        $od_req_qty = $this->input->post('od_req_qty',TRUE);
        $tool_order_id = validate_string($this->input->post('tool_order_id',TRUE));
        $order_data = $this->Order_m->get_order_info($tool_order_id);
        
        $this->db->trans_begin();        
        // unlocking the asset for order and locking for ST    
        $ordered_tools = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status<'=>3));
        $order_from_wh_id = $order_data['wh_id'];

        // preparing the array to unlock perticular ordered tools
        $orderedToolIxdexed = array();
        $orderedToolAndQty = array();
        foreach ($post_qty[$order_from_wh_id] as $orderedToolId => $odtQty) {
            $orderedToolIxdexed[] = $orderedToolId;
            $orderedToolAndQty[$orderedToolId] = $odtQty;
        }
        // checking that any child sts
        $checkPSFromDedicatedWh = checkPSFromDedicatedWh($tool_order_id);
        if(count($ordered_tools))
        {
            foreach ($ordered_tools as $key => $values)
            {                
                $ack_qty = quantityAlreadyFullfilledByOtherStockTransfer($tool_order_id,$values['tool_id'],1);   
                $in_process_qty = quantityInProcessByOtherStockTransfer($tool_order_id,$values['tool_id'],1);
                $final_qty = $ack_qty + $in_process_qty;
                // partial shipments are not yet processed/initiated from dd or other whs, this condition only execute when doing the ST for the first time from dd warehouse
                // because if any sts are raised the then ack qty or in progress qty won't be zero
                if($final_qty == 0)  
                { 
                    if(in_array($values['ordered_tool_id'], $orderedToolIxdexed))
                    {
                        $ordered_tool_id = $values['ordered_tool_id'];
                        $available_quantity = @$values['available_quantity'];
                        $quantity = @$values['quantity'];
                        $current_stage_id = $order_data['current_stage_id'];
                        $qt = 1;
                        $unlockAndLockForST = 0;  // below linked order assets and main needed locaked_assets defines do we need to create st or not
                        if($available_quantity > 0)
                        {
                            if(!isset($main_ad))
                            {
                                # Audit Start for Tool Order
                                $remarksString = "As Some of the Quantity not available in Assigned Warehouse,Unlocking the tools to Tool Order and Locking to ST to ship as Partial Stock Transfer";
                                $blockArr = array('tool_order_id' => $tool_order_id);
                                $finalArr = array('transaction_remarks'=>$remarksString);
                                $oldData = array();                            
                                $main_ad = tool_order_audit_data("tool_order",$tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$oldData,$remarksString,'',array(),"tool_order",$tool_order_id,$order_data['country_id']);    
                            }
                            // as it was transaction quanity 
                            $main_needed_locked_assets = $available_quantity;
                            if(@$main_needed_locked_assets > 0)
                            {   
                                // this condition helps at as there are sts happen from other whs and available qty incremented
                                // and st generated from dd warehoue and if locked assets not found in dd whs then when fe acks then
                                // ordered qty will again increase which in bug
                                $locked_assets = $this->Order_m->get_locked_assets($ordered_tool_id,$available_quantity);

                                $qry  = 'UPDATE ordered_tool set available_quantity = 0 , status = 2
                                            WHERE ordered_tool_id  ="'.$ordered_tool_id.'" ';
                                $this->db->query($qry);

                                if(($locked_assets))
                                {
                                    # Audit Start 
                                    $auditOldData = array(
                                        'quantity'           => $values['quantity'],
                                        'available_quantity' => $values['available_quantity']                            
                                    );
                                    $auditNewArr = array(
                                        'quantity'           => $values['quantity'],
                                        'available_quantity' => NULL,
                                        'tool_status'        => "Unlocked"
                                    );
                                    $auditBlock = array("tool_id"=> $values['tool_id']);
                                    $auditOrderedToolId = tool_order_audit_data("ordered_tools",$ordered_tool_id,'ordered_tool',2,$main_ad,$auditNewArr,$auditBlock,$auditOldData,'','',array(),"tool_order",$tool_order_id,$order_data['country_id']); 
                                    
                                    $unlockAndLockForST  = 1; // means one of the asset is unlocked so that st can createdd
                                    foreach ($locked_assets as $key => $value)
                                    {                                        
                                        $asset_main_status = $value['asset_main_status'];
                                        $asset_actual_status = ($asset_main_status==2)?1:$asset_main_status;

                                        $auditAssetRemarks = "Unlocked for ".$order_data['order_number'];
                                        assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"locked_assets",2,$auditOrderedToolId,'','',"tool_order",$tool_order_id,$order_data['country_id']);
                                        assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$main_ad,"tool_order",$tool_order_id,$order_data['country_id']);
                                        
                                        # Updating Asset
                                        $assetUpdateData = array(
                                            'status'        => $asset_actual_status,
                                            'modified_by'   => $this->session->userdata('sso_id'),
                                            'modified_time' => date('Y-m-d H:i:s')
                                        );
                                        $assetUpdateWhere = array('asset_id'=>$value['asset_id']);
                                        $this->Common_model->update_data('asset',$assetUpdateData,$assetUpdateWhere);

                                        # Update insert asset status history
                                        $assetStatusHistoryArray = array(
                                            'asset_id'          => $value['asset_id'],
                                            'created_by'        => $this->session->userdata('sso_id'),
                                            'status'            => $asset_actual_status,
                                            'ordered_tool_id'   => $ordered_tool_id, // gives new tool_order id
                                            'current_stage_id'  => 4,
                                            'tool_order_id'     => $tool_order_id
                                        );
                                        updateInsertAssetStatusHistory($assetStatusHistoryArray);

                                        $main_needed_locked_assets--;  
                                        if($main_needed_locked_assets == 0 )
                                            break;
                                    }

                                    # Audit Update
                                    $auditUpdateData = array(
                                            'quantity'               => $quantity,
                                            'available_quantity'     => 0
                                    );
                                    updatingAuditData($auditOrderedToolId,$auditUpdateData);
                                    // important note:
                                    # in raise ST we are not updating the qty of available qty but
                                    # here we need to update qty here because those assets will comes under at wh assets and can be locked for st
                                    
                                }
                            }
                        }
                    }
                }
                else
                {                        
                    continue;
                }
            }
        }
        
        $new_ordered_tool_id_arr = array(); // preparing to update the available quantity when tool available in next warehoouse
        foreach ($post_qty as $wh_id => $value)
        {
            $w_data = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
            // preparing data to insert order table 
            $f_order_number = getSTOrderNumber($w_data['country_id']);
            if($fe_check == 0) $new_order_delivery_type_id = 2;
            if($fe_check == 1) $new_order_delivery_type_id = $order_data['order_delivery_type_id'];   
            $transCountryId = $w_data['country_id'];
            $stn_number = "ST".$f_order_number;
            $st_data = array(
                'sso_id'                     => $order_data['sso_id'],
                'current_stage_id'           => 1,
                'stn_number'                 => $stn_number,
                'wh_id'                      => $wh_id,
                'country_id'                 => $w_data['country_id'],
                'to_wh_id'                   => $order_data['wh_id'],
                'fe_check'                   => 1,  // hard coded becasue all the stock transfers need to acknowledge by FE
                'order_delivery_type_id'     => $new_order_delivery_type_id,
                'order_type'                 => 1,
                'request_date'               => $order_data['request_date'],
                'return_date'                => $order_data['return_date'],    
                'deploy_date'                => $order_data['deploy_date'],
                'created_by'                 => $this->session->userdata('sso_id'),
                'created_time'               => date('Y-m-d H:i:s')
                );
            $st_order_id = $this->Common_model->insert_data('tool_order',$st_data);
            if($fe_check == 1)
            {
                $order_address_data = array(
                        'address1'      => $order_data['address1'],
                        'address2'      => $order_data['address2'],
                        'address3'      => $order_data['address3'],
                        'address4'      => $order_data['address4'],
                        'pin_code'      => $order_data['pin_code'],
                        'remarks'       => @$order_data['address_remarks'],
                        'site_id'       => $order_data['site_id'],
                        'gst_number'    => @$order_data['gst_number'],
                        'pan_number'    => @$order_data['pan_number'],
                        'system_id'     => $order_data['system_id'],
                        'tool_order_id' => $st_order_id,
                        'location_id'   => @$order_data['location_id'],
                        'created_by'    => $this->session->userdata('sso_id'),
                        'created_time'  => date('Y-m-d H:i:s'));
                $order_address_id = $this->Common_model->insert_data('order_address',$order_address_data);
                if($new_order_delivery_type_id !=1)
                {
                    unset($order_address_data['site_id'],$order_address_data['system_id']);
                }
            }
            else
            {
                $to_wh_id_data = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$order_data['wh_id']));
                // order address array
                $order_address_data = array(
                    'tool_order_id' => $st_order_id,
                    'location_id'   => $to_wh_id_data['location_id'],
                    'address1'      => $to_wh_id_data['address1'],
                    'address2'      => $to_wh_id_data['address2'],
                    'address3'      => $to_wh_id_data['address3'],
                    'address4'      => $to_wh_id_data['address4'],
                    'pin_code'      => $to_wh_id_data['pin_code'],
                    'created_by'    => $this->session->userdata('sso_id'),
                    'created_time'  => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('order_address',$order_address_data);    
            }
            unset($order_address_data['created_by'],$order_address_data['created_time'],$order_address_data['tool_order_id']);
            $st_data = unsetStockTransferData($st_data);
            $finalArr = array_merge($st_data,$order_address_data);
            $blockArr  = array('blockName'=> "Stock Transfer");
            $remarksString = "Stock Transfer Created"; 
            $ad_id = tool_order_audit_data('stock_transfer',$st_order_id,'tool_order',1,'',$st_data,$blockArr,array(),$remarksString,'',array(),"tool_order",$tool_order_id,$transCountryId);

            $order_status_data = array(
                'tool_order_id'         => $st_order_id,
                'current_stage_id'      => 1,
                'created_by'            => $this->session->userdata('sso_id'),
                'created_time'          => date('Y-m-d H:i:s')
            );
            $st_order_status_id = $this->Common_model->insert_data('order_status_history',$order_status_data);
            // insert into st tool order
            $this->Common_model->insert_data('st_tool_order',array('stock_transfer_id'=>$st_order_id,'tool_order_id'=>$tool_order_id));    
            $ordered_tool_arr = array(); // preparing to skip inserting the into ordered tool table
            foreach ($value as $ordered_tool_id => $quantity) 
            { 
                // stock transfer ordered tool
                if(!in_array($ordered_tool_id,$ordered_tool_arr))
                {
                    $order_tool_data = array(
                        'tool_order_id'      => $st_order_id,
                        'tool_id'            => $post_od_tool[$ordered_tool_id],
                        'available_quantity' => 0,
                        'quantity'           => $quantity,
                        'status'             => 1,
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s')
                        ); 
                    $new_ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);
                    $new_ordered_tool_id_arr[$ordered_tool_id] = $new_ordered_tool_id;                    
                    // pusing ordered tool id to array to skip next time inserting the 
                    //same tool quantiy and updating the quantity which is available in other warehosue   
                    $ordered_tool_arr []= $ordered_tool_id; 

                    # Audit start
                    $order_tool_data = unsetOrderedToolsExceptionColumns($order_tool_data);
                    $order_tool_data['tool_status'] = "Added";
                    $auditOrderedToolBlock = array('tool_id'=>$post_od_tool[$ordered_tool_id]);
                    // echo $transCountryId;exit;
                    $auditOrderedToolId = tool_order_audit_data('st_ordered_tools',$new_ordered_tool_id,'ordered_tool',1,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$st_order_id,$transCountryId);
                    # pusing the values of audit ordered tool
                    $audit_new_ordered_tool_id_arr[$ordered_tool_id] = $auditOrderedToolId; 
                }
                $searchParams = array(
                    'wh_id'     => $wh_id,
                    'tool_id'   => $post_od_tool[$ordered_tool_id]
                );

                $assets_array = array();
                $below_availaable_qty = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'available_quantity');
                $assets_array = $this->Order_m->get_locked_assets($ordered_tool_id,$below_availaable_qty);                    
                
                if($assets_array)
                {
                    $assets_array1 = ($assets_array)?convertIntoIndexedArray($assets_array,'asset_id'):array();
                    $assets_array = $assets_array1;
                }
                else
                {
                    $assets_array = array();
                }
                $available_assets = available_assets($searchParams,$assets_array);
                $available_assets_count = count($available_assets);               
                // looping all the assets and assigning to lock In period on basis of First Come First Locked In till Ordered Quantity
                $available_quantity = 0;  
                if(count($available_assets_count) > 0)
                {                
                    foreach ($available_assets as $key => $value)
                    {
                        $available_quantity++; 
                        // updating asset table
                        $asset_main_status = $value['asset_main_status'];
                        $asset_actual_status  = ($value['asset_main_status']==1)?2:$asset_main_status;
                        $where = array('asset_id'=>$value['asset_id']);
                        $u_data  = array(
                            'status'        => $asset_actual_status,
                            'modified_by'   => $this->session->userdata('sso_id'),
                            'modified_time' => date('Y-m-d H:i:s')
                            );
                        $this->Common_model->update_data('asset',$u_data,$where);


                        # Audit Start
                        $auditAssetRemarks = "Locked for ".$stn_number;
                            assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"st_locked_assets",1,$auditOrderedToolId,'','',"tool_order",$st_order_id,$transCountryId);
                            assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$transCountryId);
                        # Audit End

                        # Update insert asset status history
                        $assetStatusHistoryArray = array(
                            'asset_id'          => $value['asset_id'],
                            'created_by'        => $this->session->userdata('sso_id'),
                            'status'            => $asset_actual_status,
                            'ordered_tool_id'   => $new_ordered_tool_id_arr[$ordered_tool_id], // gives new tool_order id
                            'current_stage_id'  => 4,
                            'tool_order_id'     => $st_order_id
                        );
                        updateInsertAssetStatusHistory($assetStatusHistoryArray);                        
                        if($available_quantity == $quantity) break;
                    }                       
                }
                // worst scenario like when viewing the screen available but after submitting some other transaction available so no tools available
                else
                {
                    $status = 2;
                    $this->Common_model->update_data('st_tool_order',array('status'=>2),array('stock_transfer_id'=>$st_order_id));
                } 

                $this->Common_model->update_data('ordered_tool',array('available_quantity'=>$available_quantity),array('ordered_tool_id'=>$new_ordered_tool_id_arr[$ordered_tool_id]));              
                updatingAuditData($audit_new_ordered_tool_id_arr[$ordered_tool_id],array('available_quantity'=>$available_quantity));
            }
            sendSTMails($st_order_id,1,1);
            sendSTMails($st_order_id,2,1);
            sendSTMails($st_order_id,3,1);
        }

        if(!isset($main_ad))
        {
            # Audit Start for Tool Order            
            $blockArr =array('blockName'=> "Tool Order") ;
            $finalArr = array();
            $oldData = array();     
            $remarksString = "Stock Transfer ".$stn_number." Created for Order Number".$order_data['order_number'] ;
            $main_ad = tool_order_audit_data("tool_order",$tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$oldData,$remarksString,'',array(),"tool_order",$tool_order_id,$order_data['country_id']);    
        }
        else
        {   
            $remarksString = "Stock Transfer ".$stn_number." Created for Order Number".$order_data['order_number'] ;
            $updatingAuditData = array('remarks'=>$remarksString);
            updatingAuditData($main_ad,$updatingAuditData,1);
        }

        if($this->db->trans_status === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'openSTforOrder'); exit();
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Stock Transfer '.$stn_number.' Has Raised Successfully For Order Number :'.$order_data['order_number'].' </div>');
            redirect(SITE_URL.'openSTforOrder'); exit();
        }
    }

    public function openSTforOrder()
    {
        $data['nestedView']['heading']="List Of ST For Order";
        $data['nestedView']['cur_page'] = 'openSTforOrder';
        $data['nestedView']['parent_page'] = 'openSTforOrder';
        $task_access = page_access_check('raiseSTforOrder');

        /*koushik 10-10-2018*/
        $this->session->unset_userdata('st_asset_id');
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'List Of Stock Transfer';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'List Of Stock Transfer','class'=>'active','url'=>'');

            # Search Functionality
       
        $psearch=$this->input->post('openSTforOrder',TRUE);
        if($psearch!='') 
        {
            $searchParams=array(
            'order_number' => validate_string($this->input->post('order_number', TRUE)),
            'to_wh_id'     => validate_string($this->input->post('to_wh_id', TRUE)),
            'st_type'      => validate_string($this->input->post('st_type', TRUE)),
            'st_sso_id'    => validate_string($this->input->post('st_sso_id', TRUE)),
            'st_country_id'    => validate_string($this->input->post('st_country_id', TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order_number' => $this->session->userdata('order_number', TRUE),
                'to_wh_id'     => $this->session->userdata('to_wh_id', TRUE),
                'st_type'      => $this->session->userdata('st_type', TRUE),
                'st_sso_id'    => $this->session->userdata('st_sso_id', TRUE),
                'st_country_id'    => $this->session->userdata('st_country_id', TRUE),
                );
            }
            else 
            {
                $searchParams=array(
                'order_number' => '',
                'to_wh_id'     => '',
                'st_type'      => '',
                'st_sso_id'    => '',
                'st_country_id'=> ''
                );
                $this->session->set_userdata($searchParams);
            }
            $searchParams['st_type'] = ($searchParams['st_type'] =='')?2:$searchParams['st_type'];
            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'openSTforOrder/';

        # Total Records
        $config['total_rows'] = $this->Stock_transfer_m->raisedSTforOrderTotalRows($searchParams,$task_access);
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Stock_transfer_m->raisedSTforOrderResults($current_offset, $config['per_page'], $searchParams,$task_access);
        if($searchParams['st_type'] ==2)
        {
            $st_data = array();
            if(count($data['orderResults'])>0)
            {
                foreach ($data['orderResults'] as $key => $value)
                {
                   $st_data[$value['tool_order_id']] = $this->Stock_transfer_m->stnForOrder($value['tool_order_id']);
                }
                $data['st_data'] = $st_data;
            }
        }
        # Additional data
        $data['displayResults'] = 1;
        $data['whs']=$this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $this->load->view('stock_transfer/open_st',$data);

    }

    public function closedSTforOrder()
    {
        $task_access = page_access_check('raiseSTforOrder');
        $data['nestedView']['heading']="Closed ST For Order";
        $data['nestedView']['cur_page'] = 'closedSTforOrder';
        $data['nestedView']['parent_page'] = 'closedSTforOrder';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'List Of Closed Stock Transfer';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'List Of Closed Stock Transfer','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch = validate_string($this->input->post('closedSTforOrder',TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'order_number'   => validate_string($this->input->post('order_number', TRUE)),
            'to_wh_id'       => validate_string($this->input->post('to_wh_id', TRUE)),
            'st_type'        => validate_string($this->input->post('st_type', TRUE)),
            'cst_sso_id'     => validate_string($this->input->post('cst_sso_id', TRUE)),
            'cst_country_id' => validate_string($this->input->post('cst_country_id', TRUE)),
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order_number'   => $this->session->userdata('order_number'),
                'to_wh_id'       => $this->session->userdata('to_wh_id'),
                'st_type'        => $this->session->userdata('st_type'),
                'cst_sso_id'     => $this->session->userdata('cst_sso_id'),
                'cst_country_id' => $this->session->userdata('cst_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'order_number'   => '',
                'to_wh_id'       => '',
                'st_type'        => '',
                'cst_country_id' => '',
                'cst_sso_id'     =>''
                );
                $this->session->set_userdata($searchParams);
            }
            $searchParams['st_type'] = ($searchParams['st_type'] =='')?2:$searchParams['st_type'];
            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closedSTforOrder/';

        # Total Records
        $config['total_rows'] = $this->Stock_transfer_m->closedSTforOrderTotalRows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Stock_transfer_m->closedSTforOrderResults($current_offset, $config['per_page'], $searchParams,$task_access);
        if($searchParams['st_type'] ==2)
        {
            $st_data = array();
            if(count($data['orderResults'])>0)
            {
                foreach ($data['orderResults'] as $key => $value)
                {
                   $st_data[$value['tool_order_id']] = $this->Stock_transfer_m->stnForOrder($value['tool_order_id']);
                }
                $data['st_data'] = $st_data;
            }
        }
        # Additional data
        $data['displayResults'] = 1;
        $data['whs']=$this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $this->load->view('stock_transfer/closed_st',$data);
    }
    
    // WH Receive
    public function wh2_wh_receive()
    {
        $data['nestedView']['heading']="Acknowledge Stock Transfer";
        $data['nestedView']['cur_page'] = 'wh2_wh_receive';
        $data['nestedView']['parent_page'] = 'wh2_wh_receive';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Acknowledge Stock Transfer';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Stock Transfer','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch = validate_string($this->input->post('wh2_wh_receive', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'r_stn_number' => validate_string($this->input->post('stn_number', TRUE)),
            'r_wh_id'      => validate_string($this->input->post('r_wh_id', TRUE)),
            'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),  
            'to_wh_id'     => validate_number(@$this->input->post('to_wh_id',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'r_stn_number' => $this->session->userdata('r_stn_number'),
                'r_wh_id'      => $this->session->userdata('r_wh_id'),
                'country_id'   => $this->session->userdata('country_id'),
                'to_wh_id'     => $this->session->userdata('to_wh_id'));
            }
            else
            {
                $searchParams=array(
                'r_stn_number' => '',
                'r_wh_id'      => '',
                'country_id'   => '',
                'to_wh_id'     => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'wh2_wh_receive/';

        # Total Records
        $config['total_rows'] = $this->Stock_transfer_m->wh2_wh_receive_order_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Stock_transfer_m->wh2_wh_receive_order_results($current_offset, $config['per_page'], $searchParams,$task_access);

        $data['from_wh_list'] = $this->Stock_transfer_m->get_warehouse_list($task_access,$searchParams);
        $data['to_wh_list'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        
        # Additional data
        $data['displayResults'] = 1;
        $data['current_offset'] = $current_offset;
        $this->load->view('stock_transfer/wh2_wh_receive',$data);
    }

    public function download_open_st_returns()
    {
        #page authentication
        $task_access = page_access_check('wh2_wh_receive');

        if(validate_number($this->input->post('download_open_st_returns',TRUE))==1)
        {
            $searchParams=array(
                'r_stn_number' => validate_string($this->input->post('stn_number', TRUE)),
                'r_wh_id'      => validate_string($this->input->post('r_wh_id', TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),  
                'to_wh_id'     => validate_number(@$this->input->post('to_wh_id',TRUE))
            );

            $result_data = $this->Stock_transfer_m->download_open_st_returns($searchParams,$task_access);

            $header = '';
            $data ='';
            $titles = array('S.No.','ST Number','From Warehouse','To Warehouse','Expected Arrival Date','For Order Number','For SSO','Tool Number','Tool Description','Asset Number','Tool Availability','Asset Status','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            
            $data.='<tbody>';
            if(count($result_data)>0)
            {
                $sno = 1;
                foreach($result_data as $row)
                {
                    $actual_order_info = $this->Stock_transfer_m->get_actual_order_info($row['tool_order_id']);
                    if(@$actual_order_info['order_number']!='') { $order_no = @$actual_order_info['order_number']; } else { $order_no = "--";};
                    if(@$actual_order_info['sso']!='') { $sso = @$actual_order_info['sso']; } else { $sso = "--"; }

                    $assets_arr = $this->Stock_transfer_m->get_assets_in_open_st_returns($row['tool_order_id']);
                    $asset_count = count($assets_arr);
                    $exp_date = full_indian_format($row['expected_delivery_date']);
                    $data.='<tr>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$sno++.'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['stn_number'].'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['from_wh_name'].'</td>';  
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['to_wh_name'].'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$exp_date.'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$order_no.'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$sso.'</td>';                    
                    
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['part_number'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['part_description'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['asset_number'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.get_asset_position($assets_arr[0]['asset_id']).'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['asset_status'].'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.get_country_location($row['country_id']).'</td>';
                    $data.='</tr>';

                    if($asset_count>1)
                    {
                        for($i=1;$i<$asset_count;$i++)
                        {
                            $data.='<tr>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['part_number'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['part_description'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['asset_number'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.get_asset_position($assets_arr[$i]['asset_id']).'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['asset_status'].'</td>';
                            $data.='</tr>';
                        }
                    }
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            //echo $data;exit;
            //echo $data;exit;
            $time = date("Y-m-d H:i:s");
            $xlFile='Open_st_returns'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function wh2_wh_receive_details()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'wh2_wh_receive'); exit();
        }
        $data['tool_order_id'] = $tool_order_id;

        $data['nestedView']['heading']="Acknowledge Stock Transfer Details";
        $data['nestedView']['cur_page'] = "wh2_wh_receive";
        $data['nestedView']['parent_page'] = 'wh2_wh_receive';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/wh2_wh_receive.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Acknowledge Stock Transfer Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Stock Transfer   ','class'=>'','url'=>SITE_URL.'wh2_wh_receive');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Stock Transfer Details ','class'=>'','url'=>'');
    
        $return_assets = $this->Stock_transfer_m->wh_return_initialted_assets($tool_order_id);
        $return_parts = array();
        foreach ($return_assets as $key => $value) 
        {
            $return_parts[$value['oah_id']]['part_number']       = $value['part_number'];
            $return_parts[$value['oah_id']]['asset_number']      = $value['asset_number'];
            $return_parts[$value['oah_id']]['serial_number']     = $value['serial_number'];
            $return_parts[$value['oah_id']]['asset_id']          = $value['asset_id'];
            $return_parts[$value['oah_id']]['asset_status']      = $value['asset_status'];
            $return_parts[$value['oah_id']]['ordered_asset_id']  = $value['ordered_asset_id'];
            $return_parts[$value['oah_id']]['ordered_tool_id']   = $value['ordered_tool_id'];
            $return_parts[$value['oah_id']]['part_description']  = $value['part_description'];
            $return_parts[$value['oah_id']]['health_data'] = $this->Order_m->get_transit_asset_data($value['oah_id']);
        }
        $data['return_assets'] = $return_assets;
        $data['return_parts'] = $return_parts;
        $data['return_info'] = $this->Stock_transfer_m->get_return_info($tool_order_id);
        $this->load->view('stock_transfer/wh2_wh_receive_details',$data);
    }

    public function insert_wh2_wh_receive()
    { 
        $_SESSION['insert_wh2_wh_receive'] = 1;
        $order_status_id = validate_string($this->input->post('order_status_id',TRUE));
        $sub_inventory = $this->input->post('sub_inventory',TRUE);
        $oah_oa_health_id_part_id = $this->input->post('oah_oa_health_id_part_id',TRUE);
        $oah_id_arr = $this->input->post('oah_id',TRUE);
        $oa_health_id_arr = $this->input->post('oa_health_id',TRUE);
        $remarks_arr = $this->input->post('remarks',TRUE);        
        $history_status_id = $this->input->post('history_status_id',TRUE);
        $tool_order_id = validate_string($this->input->post('tool_order_id',TRUE));        
        $oah_condition_id = $this->input->post('oah_condition_id',TRUE);
        $assetAndOrderedAsset = $this->input->post('assetAndOrderedAsset',TRUE); 

        $this->db->trans_begin();
        # getting tool order and st data for transaction data
        $stData = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $country_id = $stData['country_id'];
        $stn_number = $stData['stn_number'];
        $to_wh_id = $stData['to_wh_id'];
        $actual_tool_order_id = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        $orderData = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$actual_tool_order_id));  

        $mainOrderedTool = array();
        $auditOrderedToolIdArr = array();
        // ST For Order
        if($actual_tool_order_id!='')
        {
            # Audit Start
            $oldArr = array(
                'current_stage'  => "Intransit WH to WH"
            );
            $newArr = array(
                'current_stage'  => "Closed"
            );            
            $blockArr = array( "blockName" => "Stock Transfer Ack at WH for FE ");
            $ad_id = tool_order_audit_data("st_ack",$stData['tool_order_id'],"tool_order",2,'',$newArr,$blockArr,$oldArr,'','',array(),"tool_order",$actual_tool_order_id,$country_id);
            $actual_ordered_tool_data = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$actual_tool_order_id,'status!='=>3));
            $k=0; $j=0; $unsuccessful = 0; 
            foreach ($oah_id_arr as $oah_id => $ordered_asset_id)
            {
                $asset_id = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$ordered_asset_id),'asset_id');
                $assetData = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                $asset_number = $assetData['asset_number'];
                $stOrderedToolId = $assetAndOrderedTool[$asset_id];
                //updating end time of previous stage
                if($k ==0 )
                {  
                    $end_time = array('end_time'=>date('Y-m-d H:i:s'));
                    $this->Common_model->update_data('order_status_history',$end_time,array('order_status_id'=>$order_status_id)); 
                } 
                // updating the previous asset status 
                // please careful here from post it was 2 but to tell asset status in that transaction we are
                // inserting that status to 3 to say that is missed
                $replaced_status = ($oah_condition_id[$ordered_asset_id] == 2)?3:$oah_condition_id[$ordered_asset_id];
                $u_data = array('status'=>$replaced_status);
                $u_where = array('oah_id'=>$oah_id);
                $this->Common_model->update_data('order_asset_history',$u_data,$u_where);

                // get stock transfer tool id
                $st_tool_id = $this->Stock_transfer_m->get_tool_id_by($ordered_asset_id);
                // updating the actual ordered tool data i.e quantity                
                // inserting even the asset is missed in acknowledge status 
                if($k == 0)
                {
                    // inserting new order status
                    $new_order_status_history_data = array(
                        'current_stage_id'   => 3,
                        'tool_order_id'      => $tool_order_id,  
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 1
                    );
                    $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);  
                } 

                if($oah_condition_id[$ordered_asset_id] == 1)
                {
                    // updating the available quantity
                    $q= 1;
                    $checkPSFromDedicatedWh = checkPSFromDedicatedWh($actual_tool_order_id);
                    if($checkPSFromDedicatedWh == 0)
                    {
                        if(!isset($main_ad))
                        {
                            # Audit Start for Tool Order
                            $remarksString = "Stock Transfer".$stData['stn_number']." Acknowledged by Warehouse";
                            $blockArr =array('blockName'=> "Tool Order") ;
                            // $finalArr = array('transaction_remarks'=>$remarksString);
                            $oldData = array('current_stage'=> "In Stock Transfer");     
                            $finalArr = array('current_stage'=>"In Stock Transfer");
                            $main_ad = tool_order_audit_data("tool_order",$actual_tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$oldData,$remarksString,'',array(),"tool_order",$actual_tool_order_id,$country_id);    
                        }
                        $actualOdtQtyAvailqty = $this->Common_model->get_data_row('ordered_tool',array('tool_order_id'=>$actual_tool_order_id,'tool_id'=>$st_tool_id,'status'=>2));

                        $qry = 'UPDATE ordered_tool SET available_quantity = available_quantity + "'.$q.'" 
                                 WHERE ordered_tool_id = "'.$actualOdtQtyAvailqty['ordered_tool_id'].'"  ';
                        $this->db->query($qry);
                        // updating the status to 2 by comparing available quantity and quantity
                        if(!in_array($actualOdtQtyAvailqty['ordered_tool_id'], $mainOrderedTool))
                        {
                            $mainOrderedTool[] = $actualOdtQtyAvailqty['ordered_tool_id'];
                            # Audit Start
                            $oldOrderToolData = array(
                                'quantity'              => $actualOdtQtyAvailqty['quantity'],
                                'available_quantity'    => $actualOdtQtyAvailqty['available_quantity'],
                            );
                            $newOrderToolData = array(
                                'quantity'              => $actualOdtQtyAvailqty['quantity'],
                                'available_quantity'    => ($actualOdtQtyAvailqty['available_quantity']+1),
                                "tool_status"           => "Updated"
                            );
                            $auditOrderedToolBlock = array('tool_id'=>$actualOdtQtyAvailqty['tool_id']);
                            $auditOrderedToolId = tool_order_audit_data('ordered_tools',$actualOdtQtyAvailqty['ordered_tool_id'],'ordered_tool',2,$main_ad,$newOrderToolData,$auditOrderedToolBlock,$oldOrderToolData,'','',array(),"tool_order",$actual_tool_order_id,$country_id); 
                            $auditOrderedToolIdArr[$actualOdtQtyAvailqty['ordered_tool_id']] = $auditOrderedToolId;
                        }
                        else 
                        {
                            $updateAuditData = array('available_quantity'=>($actualOdtQtyAvailqty['available_quantity']+1));
                            updatingAuditData($auditOrderedToolIdArr[$actualOdtQtyAvailqty['ordered_tool_id']],$updateAuditData);
                        }

                        $quantity = $actualOdtQtyAvailqty['quantity'];
                        $available_quantity = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$actualOdtQtyAvailqty['ordered_tool_id']),'available_quantity');
                        if($quantity == $available_quantity)
                            $this->Common_model->update_data('ordered_tool',array('status'=>1),array('ordered_tool_id'=>$actualOdtQtyAvailqty['ordered_tool_id']));
                    }

                    // inserting fe owned
                    // inserting the new record
                    $new_asset_history = array(
                        'ordered_asset_id'   => $ordered_asset_id,
                        'order_status_id'    => $new_order_status_id,
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => $oah_condition_id[$ordered_asset_id]
                    );
                    $new_oah_id = $this->Common_model->insert_data('order_asset_history',$new_asset_history);

                    # Audit Start for transaction asset                
                    $old_data_array  = array('transaction_status' => 'Shipped');
                    $acd_array = array('transaction_status'  => 'Received');                
                    $blockArra = array('asset_id' => $asset_id);

                    $auditOahId = tool_order_audit_data("st_ack_assets",$new_oah_id,"order_asset_history",1,$ad_id,$acd_array,$blockArra,$old_data_array,$stData['stn_number'],'',array(),"tool_order",$orderData['tool_order_id'],$country_id);

                    $defective_status =1;
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    {                    // updating the transist health
                        $part_id = $value[0];
                        $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                        $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];
                        $health_data = array(
                            'asset_condition_id'  => $asset_condition_id,
                            'remarks'             => $remarks
                            );
                        $health_where = array('oa_health_id'=>$oa_health_id);
                        $this->Common_model->update_data('order_asset_health',$health_data,$health_where);
                        // inserting FE Owned

                        $order_asset_health = array(
                                'asset_condition_id' => $asset_condition_id,
                                'part_id'            => $part_id,
                                'oah_id'             => $new_oah_id,
                                'remarks'            => $remarks
                            );
                        $new_oahl_id = $this->Common_model->insert_data('order_asset_health',$order_asset_health);
                        // to tell overall asset status 
                        if($asset_condition_id != 1)
                        {
                            $defective_status = 2; $unsuccessful =  1;
                        }
                    }

                    # Old Audit Asset and its position
                    $oldAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    $auditOldAssetData = auditAssetData($oldAssetData);

                    $approval = $assetData['approval_status'];
                    // if defective alert to admin
                    if(@$defective_status == 2)
                    { 
                        // 0 for not waititng for approval
                        if($approval == 0)
                        {
                            // inserting into defective asset 
                            $defective_asset = array(
                                'asset_id'          => $asset_id,
                                'current_stage_id'  => 2,
                                'wh_id'             => $to_wh_id,
                                'trans_id'          => $tool_order_id,
                                'created_by'        => $this->session->userdata('sso_id'),
                                'created_time'      => date('Y-m-d H:i:s'),
                                'type'              => 1,
                                'status'            => 2
                            );
                            $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                            foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                            { 
                                $part_id = $value[0];
                                $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                                $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];
                                $order_asset_health_data = array(
                                    'defective_asset_id'    => $defective_asset_id,
                                    'part_id'               => $part_id,
                                    'asset_condition_id'    => $asset_condition_id,
                                    'remarks'               => $remarks
                                    );
                                $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                            } 
                            // send Notification To Admin
                            sendDefectiveOrMissedAssetMail($defective_asset_id,1,$country_id,2,$tool_order_id);  
                        }
                        // uipdate owned asset status
                        $this->Common_model->update_data('order_asset_history',array('status'=>$defective_status),array('oah_id'=>$new_oah_id));
                    }

                    #if the asset is in our of calibration then don't chnge the asset status to FD and keep it as Out of calibration
                    $newAssetStatus = ($assetData['status']==8)?2:$assetData['status'];
                    # transaction Updating the Asset
                    $updateData = array(
                        'status'              => $newAssetStatus,
                        'wh_id'               => $to_wh_id,
                        'sub_inventory'       => @$sub_inventory[$asset_id],
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s') 
                        );
                    if($approval == 0 && @$defective_status ==2)
                        $updateData['approval_status'] = 1;
                    $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                    # update insert asset position
                    $transUpdateInsertPosition = array(
                        'asset_id' => $asset_id,
                        'transit'  => 0,
                        'wh_id'    => $to_wh_id,
                        'status'   => $newAssetStatus
                    );
                    updateInsertAssetPosition($transUpdateInsertPosition);

                    # Update insert asset status history
                    $assetStatusHistoryArray = array(
                        'asset_id'          => $asset_id,
                        'created_by'        => $this->session->userdata('sso_id'),
                        'status'            => $newAssetStatus,
                        'current_stage_id'  => 3,
                        'tool_order_id'     => $tool_order_id,
                        'ordered_tool_id'   => $stOrderedToolId
                    );
                    updateInsertAssetStatusHistory($assetStatusHistoryArray);

                    // newly added this will insert one more record in asset status history to link this asset to mail tool order so that it can be helpful in reports and others
                   
                    $assetStatusHistoryArray['tool_order_id'] = $actual_tool_order_id;
                    $assetStatusHistoryArray['current_stage_id'] = 4;
                    if(isset($main_ad))
                    {
                        $assetStatusHistoryArray['ordered_tool_id'] = $actualOdtQtyAvailqty['ordered_tool_id'];
                    }
                    else
                    {
                        $newToolId = $this->Common_model->get_value('part',array('asset_id'=>$asset_id,'status'=>1),'tool_id');
                        if(@$newToolId!='')
                        {
                            $newOrderedToolId = $this->Common_model->get_value('ordered_tool',array('tool_order_id'=>$actual_tool_order_id,'tool_id'=>$newToolId,'status<'=>3),'ordered_tool_id');
                            $assetStatusHistoryArray['ordered_tool_id'] = @$newOrderedToolId;
                        }
                    }
                    updateInsertAssetStatusHistory($assetStatusHistoryArray);
                                            
                    # Audit Asset start
                    $newAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $newAssetStatus
                    );
                    if($defective_status == 2 || $approval == 1) // old approval required or new approval required
                    {
                        $newAssetData['type'] = $defective_status;
                    }   
                    $auditNewAssetData = auditAssetData($newAssetData,2);
                    $auditAssetRemarks = "Received for ".$stData['order_number'] ." And Locked for ".$orderData['order_number'];
                    assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$orderData['tool_order_id'],$country_id);
                    $j++;
                }
                // inserting missed asset
                else
                { 
                    $defective_status = 3;
                    # Audit Start for transaction asset                
                    $auditAssetOldData  = array(
                        'transaction_status'    => "Shipped"
                    );
                    $auditAssetNewData = array(
                        'transaction_status'    =>  "Not Received"
                    );
                    $blockArr = array("asset_id" => $asset_id);
                    $auditOahId = tool_order_audit_data("st_ack_assets",$oah_id,"order_asset_history",1,$ad_id,$auditAssetNewData,$blockArr,$auditAssetOldData,$orderData['stn_number'],'',array(),"tool_order",$orderData['tool_order_id'],$country_id);

                    # Audit Old Asset and its position
                    $oldAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    $auditOldAssetData = auditAssetData($oldAssetData);

                    $newAssetStatus = ($assetData['status']==8)?8:$assetData['status'];

                    // inserting into acknowlwdge status eventhough asset status is missed
                    // inserting the new record
                    $new_asset_history = array(
                        'ordered_asset_id'   => $ordered_asset_id,
                        'order_status_id'    => $new_order_status_id,
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 3
                    );
                    $new_oah_id = $this->Common_model->insert_data('order_asset_history',$new_asset_history);
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    {                    
                        $part_id = $value[0];                        
                        // inserting Health
                        $order_asset_health = array(
                            'asset_condition_id' => 3,
                            'part_id'            => $part_id,
                            'oah_id'             => $new_oah_id,
                            'remarks'            => 'Missed'
                        );
                        $this->Common_model->insert_data('order_asset_health',$order_asset_health);
                    }
                    $unsuccessful = 1; // reinitiating the order for stock transfer
                    // 0 for not waititng for approval
                    if($approval == 0)
                    {
                        $defective_asset = array(
                                'asset_id'          => $asset_id,
                                'current_stage_id'  => 2,
                                'wh_id'             => $to_wh_id,
                                'trans_id'          => $tool_order_id,
                                'created_by'        => $this->session->userdata('sso_id'),
                                'created_time'      => date('Y-m-d H:i:s'),
                                'type'              => 2,
                                'status'            => 3
                                );
                        $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                        foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                        { 
                            $part_id = $value[0];
                            $order_asset_health_data = array(
                                'defective_asset_id'    => $defective_asset_id,
                                'part_id'               => $part_id,
                                'asset_condition_id'    => 3,
                                'remarks'               => 'Missed'
                                );
                            $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                        }
                        // send Notification To Admin
                            sendDefectiveOrMissedAssetMail($defective_asset_id,2,$country_id,2,$tool_order_id);  
                    }

                    # transaction Updating the Asset
                    $updateData = array(
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s') 
                    );
                    if($approval == 0)
                        $updateData['approval_status'] = 1;
                    $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                    # Audit Asset and its position
                    $newAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    if($defective_status == 3 || $approval == 1) // old approval required or new approval required
                    {
                        $newAssetData['type'] = $defective_status;
                    }   
                    $auditNewAssetData = auditAssetData($newAssetData,2);
                    $auditAssetRemarks = "Missed while receiving ".$stData['stn_number'];
                    assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$country_id);            
                    # Audit Asset End
                    $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$order_status_id)); 
                }
                $k++;
                
            }

            // updating the actual order status
            $fullfilled_data = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$actual_tool_order_id,'status'=>2));
            if(count($fullfilled_data) == 0)
            {
                $check_address = $this->Common_model->get_value('order_address',array('tool_order_id'=>$actual_tool_order_id),'check_address');
                // this case not happen because once the check address is then only we are allowing to stock transefer proceed
                if($check_address == 0)
                {
                    # if checkPartialShipmentfromOtherWh is 0 then only it will come to this condition 
                    # Audit Start
                        $updatingAuditData = array('current_stage_id'=> 5);
                        updatingAuditData($main_ad,$updatingAuditData);
                    # Audit End
                    $updateArr = array(
                        'current_stage_id' => 5,
                        'modified_by'      => $this->session->userdata('sso_id'),
                        'modified_time'    => date('Y-m-d H:i:s')
                    );
                    $this->Common_model->update_data('tool_order',$updateArr,array('tool_order_id'=>$actual_tool_order_id));

                    $end_time = array('end_time'=>date('Y-m-d H:i:s'));
                    $this->Common_model->update_data('order_status_history',$end_time,array('tool_order_id'=>$$actual_tool_order_id,'current_stage_id'=>4)); 
                    $new_actual_order_status_history_data = array(
                        'current_stage_id'   => 5,
                        'tool_order_id'      => $actual_tool_order_id,  
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 1
                        );
                    $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_actual_order_status_history_data);  
                }
            }
            // updating FE owned stage        
            if(@$unsuccessful == 1)
            {
                if($j > 0)
                    $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$new_order_status_id));

                // closing the st
                $this->Common_model->update_data('tool_order',array('status'=>2,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));               
                // re routing/initiating the order to admin for stock transfer
                $this->Common_model->update_data('st_tool_order',array('status'=>2),array('tool_order_id'=>$actual_tool_order_id,'stock_transfer_id'=>$tool_order_id));               
            }
            else
            {
                if(@$defective_status == 2)
                    $this->Common_model->update_data('tool_order',array('status'=>2,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));               
                else
                    $this->Common_model->update_data('tool_order',array('status'=>10,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));
            }
        } 
        else            
        {   // updating tool order 
            $this->Common_model->update_data('tool_order',array('status'=>10),array('tool_order_id'=>$tool_order_id));  
            # Audit Start
            $oldArr = array(
                'current_stage'  => "Intransit WH to WH"
            );
            $newArr = array(
                'current_stage'  => "Closed"
            );            
            $blockArr = array( "blockName" => "Stock Transfer Ack at WH for FE ");
            $ad_id = tool_order_audit_data("st_ack",$stData['tool_order_id'],"tool_order",2,'',$newArr,$blockArr,$oldArr,'','',array(),"tool_order",$actual_tool_order_id,$country_id);
            
            $k=0; $j=0; $unsuccessful = 0;
            foreach ($oah_id_arr as $oah_id => $ordered_asset_id)
            {
                $orderedAssetData = $this->Common_model->get_data_row('ordered_asset',array('ordered_asset_id'=>$ordered_asset_id));
                $asset_id = $orderedAssetData['asset_id'];
                $stOrderedToolId = $orderedAssetData['ordered_tool_id'];
                $assetData = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                $asset_number = $assetData['asset_number'];
                //updating end time of previous stage
                if($k ==0 )
                {   
                    $end_time = array('end_time'=>date('Y-m-d H:i:s'));
                    $this->Common_model->update_data('order_status_history',$end_time,array('order_status_id'=>$order_status_id)); 
                } 
                // please careful here from post it was 2 but to tell asset status in that transaction we are
                // inserting that status to 3 to say that is missed
                $replaced_status = ($oah_condition_id[$ordered_asset_id] == 2)?3:$oah_condition_id[$ordered_asset_id];
                $u_data = array('status'=>$replaced_status);
                $u_where = array('oah_id'=>$oah_id);
                $this->Common_model->update_data('order_asset_history',$u_data,$u_where);
                // get stock transfer tool id
                $st_tool_id = $this->Stock_transfer_m->get_tool_id_by($ordered_asset_id);
                // updating the actual ordered tool data i.e quantity
                if($k == 0)
                { 
                    // inserting new order status
                    $new_order_status_history_data = array(
                        'current_stage_id'   => 3,
                        'tool_order_id'      => $tool_order_id,  
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 1
                    );
                    $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);  
                }
                if($oah_condition_id[$ordered_asset_id] == 1)
                {
                    // inserting fe owned
                    // inserting the new record
                    $new_asset_history = array(
                        'ordered_asset_id'   => $ordered_asset_id,
                        'order_status_id'    => $new_order_status_id,
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => $oah_condition_id[$ordered_asset_id]
                    );
                    $new_oah_id = $this->Common_model->insert_data('order_asset_history',$new_asset_history);

                    $old_data_array  = array('transaction_status' => 'Shipped');
                    $acd_array = array('transaction_status'  => 'Received');                
                    $blockArra = array('asset_id' => $asset_id);
                    $auditOahId = tool_order_audit_data("st_ack_assets",$new_oah_id,"order_asset_history",1,$ad_id,$acd_array,$blockArra,$old_data_array,$stData['stn_number'],'',array(),"tool_order",$orderData['tool_order_id'],$country_id);
                    
                    $defective_status =0;
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    {  
                        $part_id = $value[0];
                        $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                        $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];
                        $health_data = array(
                            'asset_condition_id'  => $asset_condition_id,
                            'remarks'             => $remarks
                        );
                        $health_where = array('oa_health_id'=>$oa_health_id);
                        $this->Common_model->update_data('order_asset_health',$health_data,$health_where);
                        // inserting FE Owned

                        $order_asset_health = array(
                            'asset_condition_id' =>$asset_condition_id,
                            'part_id'            =>$part_id,
                            'oah_id'             =>$new_oah_id,
                            'remarks'            =>$remarks
                        );
                        $new_oahl_id = $this->Common_model->insert_data('order_asset_health',$order_asset_health);
                         // to tell overall asset status                          
                        if($asset_condition_id != 1)
                        {
                            $defective_status = 2; $unsuccessful =  1;
                        }
                    }

                    # Old Audit Asset and its position
                    $oldAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    $auditOldAssetData = auditAssetData($oldAssetData);

                    $approval = $assetData['approval_status'];
                    // if defective alert to admin
                    if(@$defective_status == 2)
                    {                         
                        if($approval == 0)
                        {
                            // inserting into defective asset 
                            $defective_asset = array(
                                'asset_id'          => $asset_id,
                                'current_stage_id'  => 2,
                                'wh_id'             => $to_wh_id,
                                'trans_id'          => $tool_order_id,
                                'created_by'        => $this->session->userdata('sso_id'),
                                'created_time'      => date('Y-m-d H:i:s'),
                                'type'              => 1,
                                'status'            => 2
                            );
                            $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                            foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                            { 
                                $part_id = $value[0];
                                $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                                $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];
                                $order_asset_health_data = array(
                                    'defective_asset_id'    => $defective_asset_id,
                                    'part_id'               => $part_id,
                                    'asset_condition_id'    => $asset_condition_id,
                                    'remarks'               => $remarks
                                    );
                                $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                            } 
                            // send Notification To Admin
                            sendDefectiveOrMissedAssetMail($defective_asset_id,1,$country_id,2,$tool_order_id);  
                        }
                        // uipdate owned asset status
                        $this->Common_model->update_data('order_asset_history',array('status'=>$defective_status),array('oah_id'=>$new_oah_id));
                    }

                    #if the asset is in our of calibration then don't chnge the asset status to FD and keep it as Out of calibration
                    $newAssetStatus = ($assetData['status']==8)?1:$assetData['status'];
                    # transaction Updating the Asset
                    $updateData = array(
                        'status'              => $newAssetStatus,
                        'wh_id'               => $to_wh_id,
                        'sub_inventory'       => @$sub_inventory[$asset_id],
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s') 
                        );
                    if($approval == 0 && @$defective_status == 2 )
                        $updateData['approval_status'] = 1;
                    $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                    # update insert asset position
                    $transUpdateInsertPosition = array(
                        'asset_id' => $asset_id,
                        'transit'  => 0,
                        'wh_id'    => $to_wh_id,
                        'status'   => $newAssetStatus
                    );
                    updateInsertAssetPosition($transUpdateInsertPosition);

                    # Update insert asset status history
                    $assetStatusHistoryArray = array(
                        'asset_id'          => $asset_id,
                        'created_by'        => $this->session->userdata('sso_id'),
                        'status'            => $newAssetStatus,
                        'current_stage_id'  => 3,
                        'tool_order_id'     => $tool_order_id,
                        'ordered_tool_id'   => $stOrderedToolId
                    );
                    updateInsertAssetStatusHistory($assetStatusHistoryArray);
                    # Audit Asset start
                    $newAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $newAssetStatus
                    );
                    if($defective_status == 2 || $approval == 1) // old approval required or new approval required
                    {
                        $newAssetData['type'] = $defective_status;
                    }   
                    $auditNewAssetData = auditAssetData($newAssetData,2);
                    $auditAssetRemarks = "Requested for ".$stData['stn_number'];
                    assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$orderData['tool_order_id'],$country_id);                    
                    $j++;
                }
                // inserting missed asset
                else
                {

                    $defective_status = 3;
                    $approval = $assetData['approval'];
                    # Audit Start for transaction asset                
                    $auditAssetOldData  = array(
                        'transaction_status'    => "Shipped"
                    );
                    $auditAssetNewData = array(
                        'transaction_status'    =>  "Not Received"
                    );
                    $blockArr = array("asset_id" => $asset_id);
                    $auditOahId = tool_order_audit_data("st_ack_assets",$oah_id,"order_asset_history",1,$ad_id,$auditAssetNewData,$blockArr,$auditAssetOldData,$orderData['stn_number'],'',array(),"tool_order",$orderData['tool_order_id'],$country_id);

                    # Audit Old Asset and its position
                    $oldAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    $auditOldAssetData = auditAssetData($oldAssetData);

                    $newAssetStatus = ($assetData['status']==8)?1:$assetData['status'];

                    // inserting into acknowlwdge status eventhough asset status is missed
                    // inserting the new record
                    $new_asset_history = array(
                            'ordered_asset_id'   => $ordered_asset_id,
                            'order_status_id'    => $new_order_status_id,
                            'created_by'         => $this->session->userdata('sso_id'),
                            'created_time'       => date('Y-m-d H:i:s'),
                            'status'             => 3
                        );
                    $new_oah_id = $this->Common_model->insert_data('order_asset_history',$new_asset_history);
                    $defective_status =1;
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    { 
                        $part_id = $value[0];
                        $order_asset_health = array(
                            'asset_condition_id' =>3,
                            'part_id'            =>$part_id,
                            'oah_id'             =>$new_oah_id,
                            'remarks'            =>'Missed'
                        );
                        $this->Common_model->insert_data('order_asset_health',$order_asset_health);
                    }
                    $unsuccessful = 1;
                    
                    // 0 for not waititng for approval
                    if($approval == 0)
                    {
                        $defective_asset = array(
                            'asset_id'          => $asset_id,
                            'current_stage_id'  => 2,
                            'wh_id'             => $to_wh_id,
                            'trans_id'          => $tool_order_id,
                            'created_by'        => $this->session->userdata('sso_id'),
                            'created_time'      => date('Y-m-d H:i:s'),
                            'type'              => 2,
                            'status'            => 3
                        );
                        $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                        foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                        { 
                            $part_id = $value[0];
                            $order_asset_health_data = array(
                                'defective_asset_id'    => $defective_asset_id,
                                'part_id'               => $part_id,
                                'asset_condition_id'    => 3,
                                'remarks'               => 'Missed'
                                );
                            $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                        }
                        // send Notification To Admin
                        sendDefectiveOrMissedAssetMail($defective_asset_id,1,$country_id,2,$tool_order_id);  
                    }
                    # transaction Updating the Asset
                    $updateData = array(
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s') 
                    );
                    if($approval == 0)
                        $updateData['approval_status'] = 1;
                    $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                    # Audit Asset and its position
                    $newAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    if($defective_status == 3 || $approval == 1) // old approval required or new approval required
                    {
                        $newAssetData['type'] = $defective_status;
                    }   
                    $auditNewAssetData = auditAssetData($newAssetData,2);
                    $auditAssetRemarks = "Received for ".$stData['stn_number'];
                    assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$country_id);            
                    # Audit Asset End
                    $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$order_status_id)); 
                }
                $k++;                
            }
            // updating final status
            if(@$unsuccessful == 1)
            {
                $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$new_order_status_id));
                $this->Common_model->update_data('tool_order',array('status'=>2,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));                
            }
            else
            {
                $this->Common_model->update_data('tool_order',array('status'=>10,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));
            }
        }
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'wh2_wh_receive');  
        }
        else
        { 
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Stock Transfer with ST Number '.$stn_number.' has been Acknowledged!
            </div>');
            redirect(SITE_URL.'wh2_wh_closed');  
        }
    }

    public function wh2_wh_closed()
    {
        $data['nestedView']['heading']="Closed Stock Transfer Returns";
        $data['nestedView']['cur_page'] = 'wh2_wh_closed';
        $data['nestedView']['parent_page'] = 'wh2_wh_closed';

         #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Stock Transfer Returns';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Returns','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=$this->input->post('wh2_wh_closed', TRUE);
        if($psearch!='') 
        {
            $searchParams=array(
            'stn_number' => validate_string($this->input->post('stn_number', TRUE)),
            'wh_id'      => validate_string($this->input->post('wh_id', TRUE)),
            'country_id' => validate_number(@$this->input->post('country_id',TRUE)),  
            'to_wh_id'   => validate_number(@$this->input->post('to_wh_id',TRUE))                               
               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'stn_number' => $this->session->userdata('stn_number'),
                'wh_id'      => $this->session->userdata('wh_id'),
                'country_id' => $this->session->userdata('country_id'),
                'to_wh_id'   => $this->session->userdata('to_wh_id'));
            }
            else
            {
                $searchParams=array(
                'stn_number' => '',
                'wh_id'      => '',
                'country_id' => '',
                'to_wh_id'   => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'wh2_wh_closed/';

        # Total Records
        $config['total_rows'] = $this->Stock_transfer_m->wh2_wh_closed_order_total_num_rows($searchParams,$task_access);
        //echo "<pre>"; print_r($data['toolResults']);exit;
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Stock_transfer_m->wh2_wh_closed_order_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        $data['from_wh_list'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['to_wh_list'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        
        # Additional data
        $data['displayResults'] = 1;
        $data['current_offset'] = $current_offset;

        $this->load->view('stock_transfer/wh2_wh_closed',$data);

    }

    public function cancelSTForOrder()
    {   
        $actual_tool_order_id = @storm_decode($this->uri->segment(2));
        if(@$actual_tool_order_id=='')
        {
            redirect(SITE_URL.'openSTforOrder');
            exit;
        }     
        $listOfSts = getListOfSTsRaisedForFEOrder($actual_tool_order_id,2);
        $transCountryId = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$actual_tool_order_id),'country_id');
        $st_data = array('current_stage'=> NULL);
        $blockArr = array('blockArr'=>"Stock Transfer");
        $oldStData = array('current_stage'=>"At WH");

        foreach ($listOfSts as $key => $value) {            
            $sts_arr[] = $value['stn_number'];
            $remarksString = "Cancelled the Stock Transfer".$value['stn_number'];            
            $ad_id = tool_order_audit_data('stock_transfer',$value['tool_order_id'],'tool_order',2,'',$st_data,$blockArr,$oldStData,$remarksString,'',array(),"tool_order",$value['tool_order_id'],$transCountryId);
            unlockingByToolOrderId($value['tool_order_id'],1,1,0,$ad_id); //  cancel , stock transfer
        }
        $order_number = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$actual_tool_order_id),'order_number');


        # Audit Start for Tool Order
        $sts_string = implode(',', $sts_arr);
        $actual_tool_order_data = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$actual_tool_order_id));
        $blockArr = array('blockArr'=> "Tool Order");
        $remarksString = "Stock transfers ".$sts_string." Cancelled for Order Number ".$actual_tool_order_data['order_number'];
        tool_order_audit_data('tool_order',$actual_tool_order_id,'tool_order',2,'',array(),$blockArr,array(),$remarksString,'',array(),"tool_order",$actual_tool_order_id,$transCountryId);
        # Audit End For Tool Order

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'openSTforOrder');  
        }
        else
        { 
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <div class="icon"><i class="fa fa-check"></i></div>
                                <strong>Success!</strong> Stock Transfer Requests For Order Number '.$order_number.' has Cancelled Successfully!
                             </div>');
                redirect(SITE_URL.'openSTforOrder');  
            
        }
    }

    public function cancelChildSTForOrder()
    {
        $tool_order_id = @storm_decode($this->uri->segment(2));
        if(@$tool_order_id=='')
        {
            redirect(SITE_URL.'openSTforOrder');
            exit;
        }     
        # Audit Start for ST
        $stData = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $transCountryId = $stData['country_id'];
        $stn_number = $stData['stn_number'];
        $st_data = array('current_stage_id'=> NULL);
        $blockArr = array('blockArr'=>"Stock Transfer");
        $oldStData = array('current_stage_id'=>1);
        $remarksString = "Cancelled the Stock Transfer".$stn_number;            
        $ad_id = tool_order_audit_data('stock_transfer',$tool_order_id,'tool_order',2,'',$st_data,$blockArr,$oldStData,$remarksString,'',array(),"tool_order",$tool_order_id,$transCountryId);
        # Audit End for ST

        # Transaction Start
        unlockingByToolOrderId($tool_order_id,1,1,0,$ad_id); //  cancel , stock transfer
        # Transaction End



        # Audit Start for Tool Order
        $actual_tool_order_id = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        $mainOrderNumber = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$actual_tool_order_id),'order_number');
        $blockArr = array('blockArr'=> "Tool Order");
        $remarksString = "Stock Transfer ".$stn_number." Cancelled the For Order Number".$mainOrderNumber;  
        tool_order_audit_data('tool_order',$actual_tool_order_id,'tool_order',2,'',array(),$blockArr,array(),$remarksString,'',array(),"tool_order",$actual_tool_order_id,$transCountryId);
        # Audit End For Tool Order
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'openSTforOrder');  
        }
        else
        { 
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <div class="icon"><i class="fa fa-check"></i></div>
                                <strong>Success!</strong> Stock Transfer with ST Number '.$stn_number.' has Cancelled Successfully!
                             </div>');
                redirect(SITE_URL.'openSTforOrder'); 
        }
        
    }


    public function download_open_st()
    {
        $task_access = page_access_check('raiseSTforOrder');
        if(validate_string($this->input->post('download_st_order',TRUE))==1)
        {
            $searchParams=array(
            'order_number' => validate_string($this->input->post('order_number', TRUE)),
            'to_wh_id'     => validate_string($this->input->post('to_wh_id', TRUE)),
            'st_type'      => validate_string($this->input->post('st_type', TRUE)),
            'st_sso_id'    => validate_string($this->input->post('st_sso_id', TRUE)),
            'st_country_id'    => validate_string($this->input->post('st_country_id', TRUE))
                               );

            $stResults = $this->Stock_transfer_m->downloadSTforOrderResults($searchParams,$task_access);
            if($searchParams['st_type']==1)
            {
                $header = '';
                $data ='';
                $titles = array('ST Number','Requested By','Order Type','Request Date','Need Date',
                    'Order Status','Country','Ship To Address','Tool Number',
                    'Tool Code','Tool Description','Ordered Qty','Asset Number');
                $data = '<table border="1">';
                $data.='<thead>';
                $data.='<tr>';
                foreach ( $titles as $title)
                {
                    $data.= '<th align="center">'.$title.'</th>';
                }
                $data.='</tr>';
                $data.='</thead>';            
                $data.='<tbody>';
                if(count($stResults)>0)
                {
                    foreach($stResults as $row)
                    {
                        $orderedTools = $this->Stock_transfer_m->get_ordered_tools($row['tool_order_id']);

                        $address['address1'] = $row['address1'];
                        $address['address2'] = $row['address2'];
                        $address['address3'] = $row['address3'];
                        $address['address4'] = $row['address4'];
                        $address['pin_code'] = $row['pin_code'];
                        $address_string = implode(",",$address);
                        $data.='<tr>';
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['stn_number'].'</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['sso'].'</td>';  
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">ST for WH to WH</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['deploy_date']).'</td>';
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['request_date']).'</td>';
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['cs_name'].'</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['countryName'].'</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" align="center">'.$address_string.'</td>';

                        foreach ($orderedTools as $key => $value) {
                                //if($i!=1)  $data.='<tr>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.'`'.$value['tool_code'].'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['quantity'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['assets'].'</td>';                    
                                $data.='</tr>';          
                                //$i++;
                        } 

                    }
                }
                else
                {
                    $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
                }
                $data.='</tbody>';
                $data.='</table>';
            }
            // For Tool order ST
            else
            {   
                $header = '';
                $data ='';
                $titles = array('Tool Order Number','Requested By','Order Type','Request Date',
                    'Need Date','Order Status','Country','Ship To Address','ST Number',
                    'Shipping From','Request Date','Need Date','Order Status','ST Ship To Address',
                    'Tool Number','Tool Code','Tool Description','Ordered Qty','Available Qty');
                $data = '<table border="1">';
                $data.='<thead>';
                $data.='<tr>';
                foreach ( $titles as $title)
                {
                    $data.= '<th align="center">'.$title.'</th>';
                }
                $data.='</tr>';
                $data.='</thead>';            
                $data.='<tbody>';
                if(count($stResults)>0)
                {
                    foreach($stResults as $row)
                    {
                        $orderedTools = $this->Stock_transfer_m->get_ordered_tools($row['tool_order_id']);

                        $st_data = $this->Stock_transfer_m->stnForOrder($row['tool_order_id']);
                        $colspan = 0;
                        $st_arr = array();
                        foreach ($st_data as $key1 => $value1) 
                        {
                            $result = $this->Stock_transfer_m->get_ordered_tools_for_tool_order($value1['tool_order_id']);
                            $st_arr[$value1['tool_order_id']] = $result;
                            
                            $colspan+=count($result);
                        }
                        if($row['sto_fe_check'] == 1)
                        {
                            if($row['order_delivery_type_id']==1 )
                            {
                                $ib_data = get_install_basedata($row['system_id']);
                                $address_string = $ib_data['c_name'].','.$row['system_id'].','.$row['address1'].','.$row['address2'].','.$row['address3'].','.$row['address4'].','.$row['pin_code'];
                            }
                            else
                            {
                               if($row['order_delivery_type_id']==3)
                                {
                                    $address_string = $row['address1'].','.$row['address2'].','.$row['address3'].','.$row['address4'].','.$row['pin_code'];
                                }
                                else
                                {
                                    $wh_add = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$row['wh_id']));
                                    $address_string = $wh_add['address1'].','.$wh_add['address2'].','.$wh_add['address3'].','.$wh_add['address4'].','.$wh_add['pin_code'];
                                }

                            }
                        }
                        else
                        {
                            $wh_add = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$row['wh_id']));
                            $address_string = $wh_add['address1'].','.$wh_add['address2'].','.$wh_add['address3'].','.$wh_add['address4'].','.$wh_add['pin_code'];
                        }

                        $data.='<tr>';
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.$row['order_number'].'</td>'; 
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.$row['sso'].'</td>';  
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">ST For Tool Order</td>'; 
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['deploy_date']).'</td>';
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['request_date']).'</td>';
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.$row['cs_name'].'</td>';
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.$row['countryName'].'</td>';
                        $data.='<td rowspan="'.$colspan.'" align="center">'.$address_string.'</td>';

                         
                        foreach ($st_data as $key1 => $value1) 
                        {
                            $result = $st_arr[$value1['tool_order_id']];

                            $address['address1'] = $value1['address1'];
                            $address['address2'] = $value1['address2'];
                            $address['address3'] = $value1['address3'];
                            $address['address4'] = $value1['address4'];
                            $address['pin_code'] = $value1['pin_code'];
                            $address_string2 = implode(",",$address);

                            if($key1!=0)
                            {
                                $data.='<tr>';
                            }
                            
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.$value1['stn_number'].'</td>'; 
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.$value1['wh'].'</td>';
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($value1['deploy_date']).'</td>';
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($value1['request_date']).'</td>';
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.stockTransferStatus($value1,1).'</td>'; 
                            $data.='<td rowspan="'.count($result).'" align="center">'.$address_string2.'</td>';

                            foreach ($result as $key => $value) 
                            {
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.'`'.$value['tool_code'].'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['quantity'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['available_quantity'].'</td>';                    
                                $data.='</tr>';
                            } 
                        }
                    }
                }
                else
                {
                    $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
                }
                $data.='</tbody>';
                $data.='</table>';
            }

            $time = date("Y-m-d H:i:s");
            $xlFile='open_st_downloads_'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }


    public function download_closed_st()
    {
        $task_access = page_access_check('raiseSTforOrder');
        if(validate_string($this->input->post('download_closed_st',TRUE))==1)
        {
            $searchParams=array(
            'order_number'   => validate_string($this->input->post('order_number', TRUE)),
            'to_wh_id'       => validate_string($this->input->post('to_wh_id', TRUE)),
            'st_type'        => validate_string($this->input->post('st_type', TRUE)),
            'cst_sso_id'     => validate_string($this->input->post('cst_sso_id', TRUE)),
            'cst_country_id' => validate_string($this->input->post('cst_country_id', TRUE)),
            );

            $stResults = $this->Stock_transfer_m->downloadclosedSTResults($searchParams,$task_access);
            if($searchParams['st_type']==1)
            {
                $header = '';
                $data ='';
                $titles = array('ST Number','Requested By','Order Type','Request Date','Need Date',
                    'Order Status','Country','Type','Ship To Address','Tool Number',
                    'Tool Code','Tool Description','Ordered Qty','Asset Number');
                $data = '<table border="1">';
                $data.='<thead>';
                $data.='<tr>';
                foreach ( $titles as $title)
                {
                    $data.= '<th align="center">'.$title.'</th>';
                }
                $data.='</tr>';
                $data.='</thead>';            
                $data.='<tbody>';
                if(count($stResults)>0)
                {
                    foreach($stResults as $row)
                    {
                        $orderedTools = $this->Stock_transfer_m->get_closed_st_assets($row['tool_order_id']);

                        $address['address1'] = $row['address1'];
                        $address['address2'] = $row['address2'];
                        $address['address3'] = $row['address3'];
                        $address['address4'] = $row['address4'];
                        $address['pin_code'] = $row['pin_code'];
                        $address_string = implode(",",$address);
                        $data.='<tr>';
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['stn_number'].'</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['sso'].'</td>';  
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['order_type'].'</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['deploy_date']).'</td>';
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['request_date']).'</td>';
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['cs_name'].'</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['countryName'].'</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">ST for WH to WH</td>'; 
                        $data.='<td rowspan="'.count($orderedTools).'" align="center">'.$address_string.'</td>';

                        foreach ($orderedTools as $key => $value) {
                                //if($i!=1)  $data.='<tr>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.'`'.$value['tool_code'].'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['request_quantity'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['assets'].'</td>';                    
                                $data.='</tr>';          
                                //$i++;
                        } 

                    }
                }
                else
                {
                    $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
                }
                $data.='</tbody>';
                $data.='</table>';
                
            }
            // For Tool order ST
            else
            {
                $header = '';
                $data ='';
                $titles = array('Tool Order Number','Requested By','Order Type','Request Date',
                    'Need Date','Order Status','Country','Ship To Address','ST Number',
                    'Shipping From','Request Date','Need Date','Order Status','ST Ship To Address',
                    'Tool Number','Tool Code','Tool Description','Ordered Qty','Asset Number');
                $data = '<table border="1">';
                $data.='<thead>';
                $data.='<tr>';
                foreach ( $titles as $title)
                {
                    $data.= '<th align="center">'.$title.'</th>';
                }
                $data.='</tr>';
                $data.='</thead>';            
                $data.='<tbody>';
                if(count($stResults)>0)
                {
                    foreach($stResults as $row)
                    {
                        $orderedTools = $this->Stock_transfer_m->get_ordered_tools($row['tool_order_id']);

                        $st_data = $this->Stock_transfer_m->stnForOrder($row['tool_order_id']);
                        $colspan = 0;
                        $st_arr = array();
                        foreach ($st_data as $key1 => $value1) 
                        {
                            $result = $this->Stock_transfer_m->get_ordered_tools($value1['tool_order_id']);
                            $st_arr[$value1['tool_order_id']] = $result;
                            if(count($result)>0)
                            {
                                $colspan+=count($result);
                            }
                            else
                            {
                                $colspan+=1;
                            }
                            
                        }

                        $fe_check = 0;
                        // if fe check is 1 then order address if not then to the warehouse
                        $fe_check_data = $this->Stock_transfer_m->getOldFeCheck($row['tool_order_id']);
                        if(count($fe_check_data)>0)
                        {
                            $fe_check = $fe_check_data[0]['fe_check'];
                        }
                        if($fe_check == 1)
                        {
                            if($row['order_delivery_type_id']==1 )
                            {
                                $ib_data = get_install_basedata($row['system_id']);
                                $address_string = $ib_data['c_name'].','.$row['system_id'].','.$ib_data['address1'].','.$ib_data['address2'].','.$ib_data['address3'].','.$ib_data['address4'].','.$ib_data['zip_code'];
                            }
                            else
                            {
                               if($row['order_delivery_type_id']==3)
                                {
                                    $address_string = $row['address1'].','.$row['address2'].','.$row['address3'].','.$row['address4'].','.$row['pin_code'];
                                }
                                else
                                {
                                    $wh_add = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$row['wh_id']));
                                    $address_string = $wh_add['address1'].','.$wh_add['address2'].','.$wh_add['address3'].','.$wh_add['address4'].','.$wh_add['pin_code'];
                                }
                            }
                        }
                        else
                        {
                            $wh_add = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$row['wh_id']));
                            $address_string = $wh_add['address1'].','.$wh_add['address2'].','.$wh_add['address3'].','.$wh_add['address4'].','.$wh_add['pin_code'];
                        }

                        $data.='<tr>';
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.$row['order_number'].'</td>'; 
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.$row['sso'].'</td>';  
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">ST For Tool Order</td>'; 
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['deploy_date']).'</td>';
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['request_date']).'</td>';
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.$row['cs_name'].'</td>';
                        $data.='<td rowspan="'.$colspan.'" style="vertical-align: middle; text-align: center;">'.$row['countryName'].'</td>';
                        $data.='<td rowspan="'.$colspan.'" align="center">'.$address_string.'</td>';

                         
                        foreach ($st_data as $key1 => $value1) 
                        {
                            $result = $st_arr[$value1['tool_order_id']];

                            $address['address1'] = $value1['address1'];
                            $address['address2'] = $value1['address2'];
                            $address['address3'] = $value1['address3'];
                            $address['address4'] = $value1['address4'];
                            $address['pin_code'] = $value1['pin_code'];
                            $address_string2 = implode(",",$address);

                            if($key1!=0)
                            {
                                $data.='<tr>';
                            }
                            
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.$value1['stn_number'].'</td>'; 
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.$value1['wh'].'</td>';
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($value1['deploy_date']).'</td>';
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($value1['request_date']).'</td>';
                            $data.='<td rowspan="'.count($result).'" style="vertical-align: middle; text-align: center;">'.stockTransferStatus($value1,1).'</td>'; 
                            $data.='<td rowspan="'.count($result).'" align="center">'.$address_string2.'</td>';

                            foreach ($result as $key => $value) 
                            {
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.'`'.$value['tool_code'].'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['quantity'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value['assets'].'</td>';                    
                                $data.='</tr>';
                            } 
                        }
                    }
                }
                else
                {
                    $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
                }
                $data.='</tbody>';
                $data.='</table>';
            }

            $time = date("Y-m-d H:i:s");
            $xlFile='closed_st_downloads_'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }
}