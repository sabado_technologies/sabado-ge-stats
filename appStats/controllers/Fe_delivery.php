<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
created_by : Koushik
Date : 20-07-2017
*/
class Fe_delivery extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Fe_delivery_m');
	}

    public function fe_delivery_list()
    {
        unset($_SESSION['scanned_assets_list']);
        $data['nestedView']['heading']="FE Shipment Requests";
        $data['nestedView']['cur_page'] = "fe_delivery";
        $data['nestedView']['parent_page'] = 'fe_delivery';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'FE Shipment Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Shipment Requests','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchfedelivery',TRUE));
        $courier_date = validate_string($this->input->post('courier_date',TRUE));
        if($courier_date != '')
        {
            $date = strtotime("+2 days", strtotime($courier_date));
            $courier_date =  date("Y-m-d", $date);
        }   
        if($psearch!='') 
        {
            $searchParams=array(
            'tool_order_number' => validate_string($this->input->post('tool_order_number', TRUE)),
            'courier_date'      => $courier_date,
            'ssoid'             => validate_string($this->input->post('ssoid',TRUE)),
            'whc_id'            => validate_number(@$this->input->post('wh_id',TRUE)),
            'country_id'        => validate_number(@$this->input->post('country_id',TRUE)),
            'service_type_id'   => validate_number(@$this->input->post('service_type_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'tool_order_number' => $this->session->userdata('tool_order_number'),
                'courier_date'      => $this->session->userdata('courier_date'),
                'ssoid'             => $this->session->userdata('ssoid'),
                'whc_id'            => $this->session->userdata('whc_id'),
                'country_id'        => $this->session->userdata('country_id'),
                'service_type_id'   => $this->session->userdata('service_type_id')
                );
            }
            else 
            {
                $searchParams=array(
                'tool_order_number' => '',
                'courier_date'      => '',
                'ssoid'             => '',
                'page_redirect'     => '',
                'whc_id'            => '',
                'country_id'        => '',
                'service_type_id'   => ''
                );
                $this->session->set_userdata($searchParams);
            }  
        }
        $data['search_data'] = $searchParams;
        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'fe_delivery_list/';
        # Total Records
        $config['total_rows'] = $this->Fe_delivery_m->fe_delivery_list_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $fe_deliveryResults = $this->Fe_delivery_m->fe_delivery_list_results($current_offset, $config['per_page'], $searchParams,$task_access);

        foreach ($fe_deliveryResults as $key => $value) 
        {
            $check = $this->Fe_delivery_m->check_for_scanned_assets($value['tool_order_id']);
            $fe_deliveryResults[$key]['check_scanned_asset'] = $check;
        }
        $data['fe_deliveryResults'] = $fe_deliveryResults;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['service_type']=$this->Common_model->get_data('service_type',array('status'=>1));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;
        # Additional data
        $this->load->view('fe_delivery/fe_delivery_list',$data);
    }

    public function unlink_scanned_assets_individually()
    {
        $tool_order_id = @storm_decode($this->uri->segment(2));
        $ordered_tool_id = storm_decode($this->uri->segment(3));
        if($ordered_tool_id == '' || $tool_order_id=='')
        {
            redirect(SITE_URL.'generate_fe_delivery/'.storm_encode(@$tool_order_id)); exit();
        }
        unset($_SESSION['scanned_assets_list'][$ordered_tool_id]);
        $tool_id = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'tool_id');
        $tool_desc = $this->Common_model->get_value('tool',array('tool_id'=>$tool_id),'part_description');
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Assets has been unlinked for Tool : <strong>'.$tool_desc.'</strong> !
        </div>');  
        redirect(SITE_URL.'generate_fe_delivery/'.storm_encode($tool_order_id)); exit();
    }

	public function generate_fe_delivery()
	{
        //echo "<pre>"; print_r($_SESSION['scanned_assets_list']); exit();
        $tool_order_id = @storm_decode($this->uri->segment(2));
        $page_redirect = $this->session->userdata('page_redirect');
        if($tool_order_id == '' || $page_redirect == 1)
        {
            redirect(SITE_URL.'fe_delivery_list');
            exit;
        }  

        $data['nestedView']['heading']="Generate FE Shipment";
        $data['nestedView']['cur_page'] = 'check_fe_delivery';
        $data['nestedView']['parent_page'] = 'fe_delivery';
        $data['enableFormWizard'] = 1;

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

       	# include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux-responsive.min.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/fuelux/loader.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/fe_delivery.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Generate FE Shipment';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Shipment Requests','class'=>'','url'=>SITE_URL.'fe_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate FE Shipment','class'=>'active','url'=>'');

        #additional data
        $data['tool_order_id'] = $tool_order_id;
        $data['trow'] = $this->Fe_delivery_m->get_tool_order_details($tool_order_id);
        $order_tool = $this->Fe_delivery_m->get_ordered_tool($tool_order_id);
        $swh_id = $data['trow']['wh_id'];
        $present_count = 0;
        $actual_count = count($order_tool);
        foreach ($order_tool as $key => $value) 
        {
            $avail_qty = $this->Fe_delivery_m->get_avail_tool($value['tool_id'],$swh_id);
            $result = get_scanned_assets_details($value['ordered_tool_id']);
            $asset_count = $result['asset_count'];
            $assets_list = $result['assets_list'];

            $order_tool[$key]['avail_qty'] = check_for_minus($avail_qty['avail_qty']-$asset_count);
            $order_tool[$key]['asset_count'] = $asset_count;
        	$order_tool[$key]['assets'] = $assets_list;
        	
        	if($value['requested_qty'] == @$asset_count)
            {
                $present_count++;
            }
        }
        $check_no = 1;
        if($actual_count == $present_count)
        {
            $check_no = 0;
        }
        $data['check_no'] = $check_no;
        $data['order_tool'] = $order_tool;
        $order_delivery_type_id = $data['trow']['order_delivery_type_id'];
        $country_id = $data['trow']['country_id'];
        $data['country_id'] = $country_id;
        if($order_delivery_type_id == 2)
        {
            $data['wh_gp'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$swh_id));
        }
        else
        {
            $data['wh_gp'] = '';
        }
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$swh_id));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
        $data['order_address'] = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>2));
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_fe_delivery';
       	$this->load->view('fe_delivery/wizard_fe_delivery',$data);
	}

	public function fe_delivery_asset()
	{
        $ordered_tool_id = @storm_decode($this->uri->segment(2));
		if($ordered_tool_id == '')
        {
            redirect(SITE_URL.'fe_delivery_list');
            exit;
        }

        $trow = $this->Fe_delivery_m->get_ordered_tool_detail($ordered_tool_id);
        $data['nestedView']['heading']="QR Code";
        $data['nestedView']['cur_page'] = 'check_fe_delivery';
        $data['nestedView']['parent_page'] = 'fe_delivery';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'QR Code';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Shipment Requests','class'=>'','url'=>SITE_URL.'fe_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate FE Shipment','class'=>'','url'=> SITE_URL.'generate_fe_delivery/'.storm_encode($trow['tool_order_id']));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'QR Code','class'=>'active','url'=>'');

        
        #additional data
        $data['trow'] = $trow;
        $tool_id = $trow['tool_id'];
        $tool_order_wh_id = $trow['wh_id'];
        $result = get_scanned_assets_details($ordered_tool_id);
        $asset_id_arr = $result['asset_id_arr'];
        $data['fe_asset_sub_inventory'] = $this->Fe_delivery_m->get_sub_inventory_asset($tool_id,$tool_order_wh_id,$asset_id_arr);
        $this->load->view('fe_delivery/fe_delivery_asset',$data);
	}

	public function fe_asset_details()
	{
        #page authentication
        $task_access = page_access_check('fe_delivery');

        $tool_id = validate_number(storm_decode($this->input->post('tool_id',TRUE)));
		$ordered_tool_id = validate_number(storm_decode($this->input->post('ordered_tool_id',TRUE)));
		$asset_number = validate_string($this->input->post('asset_number',TRUE));
		$asset_number = trim(@$asset_number);
        $tool_order_id = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'tool_order_id');
        $tool_order_wh_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');
        $result = get_scanned_assets_details($ordered_tool_id);
        $asset_id_arr = $result['asset_id_arr'];
		$check_asset = $this->Fe_delivery_m->check_asset($tool_id,$asset_number,$tool_order_wh_id,$asset_id_arr);
		if($check_asset['asset_id']=='')
		{
			$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Invalid Asset Number:<strong> '.$asset_number.' </strong>!</div>'); 
			redirect(SITE_URL.'fe_delivery_asset/'.storm_encode($ordered_tool_id)); exit();
		}

        /*check asset is involved in ST or not */
        $st_entry = check_for_st_entry($check_asset['asset_id']);
        if(count($st_entry)>0)
        {
            $stn_number = $st_entry['stn_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
            redirect(SITE_URL.'fe_delivery_asset/'.storm_encode($ordered_tool_id)); exit();
        }
		else
		{
			$trow = $this->Fe_delivery_m->get_ordered_tool_detail($ordered_tool_id);
			$data['nestedView']['heading']="Asset Details";
	        $data['nestedView']['cur_page'] = 'check_fe_delivery';
	        $data['nestedView']['parent_page'] = 'fe_delivery';

	        # include files
	        $data['nestedView']['css_includes'] = array();
	        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

	        $data['nestedView']['js_includes'] = array();
	        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
	        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
	        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/health.js"></script>';

	        # Breadcrumbs
	        $data['nestedView']['breadCrumbTite'] = "Asset Details : ".$asset_number."";
	        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
	        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Shipment Requests','class'=>'','url'=>SITE_URL.'fe_delivery_list');
	        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate FE Shipment','class'=>'','url'=> SITE_URL.'generate_fe_delivery/'.storm_encode($trow['tool_order_id']));
	        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'QR Code','class'=>'','url'=> SITE_URL.'fe_delivery_asset/'.storm_encode($ordered_tool_id));
	        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Asset Details','class'=>'active','url'=>'');

	        #additional data
	        $asset_details = $this->Fe_delivery_m->get_asset_part_details($check_asset['asset_id']);
			$data['ordered_tool_id'] = $ordered_tool_id;
			$data['asset_details'] = $asset_details;
	        $data['trow'] = $trow;
	        #display partial tolerance detials
            $data['display_cr_remarks'] = get_cr_remarks($check_asset['asset_id']);
            
	        $this->load->view('fe_delivery/fe_delivery_asset',$data);
		}
	}

	public function insert_fe_delivery_asset()
	{
        #page authentication
        $task_access = page_access_check('fe_delivery');
		$ordered_tool_id = validate_number(storm_decode($this->input->post('ordered_tool_id',TRUE)));
		$asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
		$asset_number = $asset_arr['asset_number'];
		$asset_condition_id = $this->input->post('asset_condition_id',TRUE);
		$remarks = $this->input->post('remarks',TRUE);
		$asset_details = $this->Fe_delivery_m->get_asset_part_details($asset_id);
		$tool_order_id = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'tool_order_id');

        $to_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $tool_order_wh_id = $to_arr['wh_id'];
		$order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>5,'end_time'=>NULL),'order_status_id'); 

        /*koushik 11-10-2018 check asset is involved in ST or not */
        $st_entry = check_for_st_entry($asset_id);
        if(count($st_entry)>0)
        {
            $stn_number = $st_entry['stn_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
            redirect(SITE_URL.'fe_delivery_asset/'.storm_encode($ordered_tool_id)); exit();
        }

		#check asset_condition_status
		$check_astatus = 0;
		foreach ($asset_details as $key => $value) {
			if($asset_condition_id[$value['part_id']]!=1)
			{
				$check_astatus++;
			}
		}
		$this->db->trans_begin();
		if($check_astatus == 0)
		{
            $part_list = array();
            foreach ($asset_details as $key => $value) 
            {
                $remarks1 = $remarks[$value['part_id']];
                if($remarks1 == '') $remarks1 = NULL; 
                $insert_oah = array(
                    'part_id'            => $value['part_id'],
                    'asset_condition_id' => $asset_condition_id[$value['part_id']],
                    'remarks'            => $remarks1
                );
                $part_list[] = $insert_oah;
            }

            $_SESSION['scanned_assets_list'][$ordered_tool_id][] = 
            array('asset_id'     => $asset_id,
                  'asset_number' => $asset_number,
                  'part_list_arr'=> $part_list);

            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset : <strong>'.$asset_number.'</strong> has been Linked successfully!</div>');
            redirect(SITE_URL.'generate_fe_delivery/'.storm_encode($tool_order_id)); exit();
		}
		else //insert in defective asset
		{
			$insert_defective = array(
                'asset_id'	       => $asset_id,
                'current_stage_id' => 5,//at wh-tools order
                'type'			   => 1,
                'wh_id'			   => $tool_order_wh_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'	   => date('Y-m-d H:i:s'),
                'trans_id'         => $tool_order_id,
                'status'		   => 1
            );
			$defective_asset_id = $this->Common_model->insert_data('defective_asset',$insert_defective);

			foreach ($asset_details as $key => $value) 
			{
				$remarks1 = $remarks[$value['part_id']];

				if($remarks1 == '') $remarks1 = NULL; 
				$insert_defect_health =array(
                    'asset_condition_id' => $asset_condition_id[$value['part_id']],
                    'part_id'			 => $value['part_id'],
                    'remarks'            => $remarks1,
                    'defective_asset_id' => $defective_asset_id
                );
				$this->Common_model->insert_data('defective_asset_health',$insert_defect_health);
			}
            // send Notification To Admin
            $country_id = $to_arr['country_id'];
            sendDefectiveOrMissedAssetMail($defective_asset_id,1,$country_id,2,$tool_order_id);
            
            #audit data
            $old_data = array(get_da_key() => get_da_status($asset_id,$asset_arr['approval_status']));
			$update_data5 = array(
                'approval_status' => 1,
                'modified_time'   => date('Y-m-d H:i:s'),
                'modified_by'     => $this->session->userdata('sso_id')
            );
			$update_where5 = array('asset_id' => $asset_id);
			$this->Common_model->update_data('asset',$update_data5,$update_where5);

            #audit data
            $new_data = array(get_da_key() => get_da_status($asset_id,1));
            $remarks = "Identified as defective in Wh";
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'defective_asset',$defective_asset_id,$asset_arr['country_id']);

			if ($this->db->trans_status() === FALSE)
	        {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'fe_delivery_asset/'.storm_encode($ordered_tool_id)); exit(); 
	        }
	        else
	        {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Note!</strong> Asset : <strong>'.$asset_number.'</strong> has been Blocked and Intimated to Admin!</div>'); 
                redirect(SITE_URL.'generate_fe_delivery/'.storm_encode($tool_order_id)); 
	        }
		}
	}

	public function insert_fe_delivery()
	{
        #page authentication
        $task_access = page_access_check('fe_delivery');

		$tool_order_id = validate_number(storm_decode($this->input->post('tool_order_id',TRUE)));
		if($tool_order_id == '')
		{
			redirect(SITE_URL.'fe_delivery_list'); exit();
		}
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));

        #check tool order is at wh = 5
        if($trow['current_stage_id']!=5)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Something went wrong. please Try Again !.
            </div>');
            redirect(SITE_URL.'fe_delivery_list'); exit();
        }

        #check for out of calibration tool
        $order_tool = $this->Fe_delivery_m->get_ordered_tool($tool_order_id);
        foreach ($order_tool as $key => $value) 
        {
            $ordered_tool_id = $value['ordered_tool_id'];
            $session = $_SESSION['scanned_assets_list'][$ordered_tool_id];
            if(count($session)>0)
            {
                foreach ($session as $key1 => $value1) 
                {
                    $asset_status = $this->Common_model->get_value('asset',array('asset_id'=>$value1['asset_id']),'status');
                    if($asset_status>2) // 1 at wh, 2 lock in period
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> Invalid Assets are linked for Order Number:<strong>'.@$trow['order_number'].'</strong>. please Try Again !.
                        </div>');
                        redirect(SITE_URL.'fe_delivery_list'); exit();
                    }
                }
            }
            else
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Something went wrong. please Try Again !.
                </div>');
                redirect(SITE_URL.'fe_delivery_list'); exit();
                //something went wrong
            }
        }

		$courier_type = validate_number($this->input->post('courier_type',TRUE));
		$courier_name = validate_string($this->input->post('courier_name',TRUE));
		$courier_number = validate_string($this->input->post('courier_number',TRUE));
		$contact_person = validate_string($this->input->post('contact_person',TRUE));
		$phone_number = validate_string($this->input->post('phone_number',TRUE));
		$expected_delivery_date = validate_string(format_date($this->input->post('expected_delivery_date',TRUE)));
		$doc_type = $this->input->post('document_type',TRUE);
		$vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
		$remarks        = validate_string($this->input->post('remarks',TRUE));
        if($remarks == ""){ $remarks = NULL; }

		if($courier_type == 1)
		{
			$phone_number   = NULL;
			$contact_person = NULL;
			$vehicle_number = NULL;
		}
		else if($courier_type == 2)
		{
			$courier_name   = NULL;
			$courier_number = NULL;
			$vehicle_number = NULL;
		}
		else if($courier_type == 3)
		{
			$courier_name   = NULL;
			$courier_number = NULL;
		}

        $print_type = validate_number($this->input->post('print_type',TRUE));
        $tool_order_wh_id = $trow['wh_id'];
        $this->db->trans_begin();
        $print_id = get_current_print_id($print_type,$tool_order_wh_id);

		$insert_del_details = array(
            'tool_order_id' => $tool_order_id,
            'print_id'      => $print_id,
            'courier_type'  => $courier_type,
            'contact_person'=> $contact_person,
            'phone_number'  => $phone_number,
            'courier_name'	=> $courier_name,
            'courier_number'=> $courier_number,
            'vehicle_number'=> $vehicle_number,
            'remarks'       => $remarks,
            'billed_to'     => validate_number($this->input->post('billed_to',TRUE)),
            'created_by'	=> $this->session->userdata('sso_id'),
            'created_time'	=> date('Y-m-d H:i:s'),
            'status'		=> 1,
            'expected_delivery_date'=> $expected_delivery_date
        );
		
		$order_delivery_id = $this->Common_model->insert_data('order_delivery',$insert_del_details);

        #audit data
        $remarks = "FE Tool Order Shipment details:".$trow['order_number'];
        $ad_id = audit_data('tool_order_delivery',$order_delivery_id,'order_delivery',1,'',$insert_del_details,array('tool_order_id' => $tool_order_id),array(),$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);

        #Update Order Address
        $old_oa = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id,'status'=>1));

        $update_oa = array(
            'address1'    => validate_string($this->input->post('address1',TRUE)),
            'address2'    => validate_string($this->input->post('address2',TRUE)),
            'address3'    => validate_string($this->input->post('address3',TRUE)),
            'address4'    => validate_string($this->input->post('address4',TRUE)),
            'pin_code'    => validate_string($this->input->post('pin_code',TRUE)),
            'gst_number'  => validate_string($this->input->post('gst_number',TRUE)),
            'pan_number'  => validate_string($this->input->post('pan_number',TRUE)),
            'modified_by' => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $update_oa_where = array('tool_order_id'=>$tool_order_id,'status'=>1);
        $order_address_id = $this->Common_model->update_data('order_address',$update_oa,$update_oa_where);

        #audit data
        unset($update_oa['modified_by'],$update_oa['modified_time']);
        $remarks = "Ship To Address";
        $final_arr = array_diff_assoc($update_oa, $old_oa);
        if(count($final_arr)>0)
        {
            audit_data('tool_order_address',@$order_address_id,'order_address',2,$ad_id,$final_arr,array('tool_order_id'=>$tool_order_id),$old_oa,'','',array(),'tool_order',$tool_order_id,$trow['country_id']);
        }
    
        #print_history
        $insert_print_history = array(
            'print_id'          => $print_id,
            'remarks'           => 'Print Creation',
            'order_delivery_id' => $order_delivery_id,
            'created_by'        => $this->session->userdata('sso_id'),
            'created_time'      => date('Y-m-d H:i:s'),
            'status'            => 1
        );
        $this->Common_model->insert_data('print_history',$insert_print_history);

		if(count($doc_type)>0)
		{
			foreach ($doc_type as $key => $value) 
	        {
	            if($_FILES['support_document_'.$key]['name']!='')
	            {
	                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
	                $config['upload_path'] = asset_document_path();
	                $config['allowed_types'] = 'gif|jpg|png|pdf';
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                $this->upload->do_upload('support_document_'.$key);
	                $fileData = $this->upload->data();
	                $support_document = $config['file_name'];
	            }
	            else
	            {
	                $support_document = '';
	            }
	            $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'		   => $_FILES['support_document_'.$key]['name'],
                    'order_delivery_id'=> $order_delivery_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1);

	            if($value!='' && $support_document != '')
	            {
	                $this->Common_model->insert_data('order_delivery_doc',$insert_doc);
	            }
	        }
		}

        #captured assets as stage 5
        $order_status_id5 = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>5,'end_time'=>NULL),'order_status_id');

        #entering details while moving to In Transit
        $insert_order_status = array(
            'current_stage_id' => 6,//at transit from wh to FE
            'tool_order_id'    => $tool_order_id,
            'created_by'       => $this->session->userdata('sso_id'),
            'created_time'     => date('Y-m-d H:i:s'),
            'status'           => 1
        );
        $order_status_id6 = $this->Common_model->insert_data('order_status_history',$insert_order_status);

        #audit data for in transit
        $old_data = array('current_stage_id' => 5);
        $new_data = array('current_stage_id' => 6);
        $remarks = "In Transit to FE Tool Order:".$trow['order_number'];
        $ad_id2 = audit_data('tool_in_transit',$order_status_id6,'order_status_history',2,'',$new_data,array('tool_order_id'=>$tool_order_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);

        foreach ($order_tool as $key => $value)
        {
            $ordered_tool_id = $value['ordered_tool_id'];
            $session = $_SESSION['scanned_assets_list'][$ordered_tool_id];
            foreach ($session as $key1 => $value1) 
            {
                $asset_id = $value1['asset_id'];
                $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                $asset_details = $value1['part_list_arr'];

                #insert ordered asset
                $insert_oa = array(
                    'ordered_tool_id' => $ordered_tool_id,
                    'asset_id'        => $asset_id,
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s'),
                    'status'          => 1
                );
                $ordered_asset_id = $this->Common_model->insert_data('ordered_asset',$insert_oa);

                #insert into order asset history for 5
                $insert_oah = array(
                    'ordered_asset_id' => $ordered_asset_id,
                    'order_status_id'  => $order_status_id5,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
                $oah_id5 = $this->Common_model->insert_data('order_asset_history',$insert_oah);
                #audit_data
                $new_data = array('transaction_status'=>'Shipped To FE');
                $remarks = "Asset is shipped to FE Tool Order:".$trow['order_number'];
                audit_data('shipped_assets',$oah_id5,'order_asset_history',1,$ad_id,$new_data,array('asset_id'=>$asset_id),array(),$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);

                #insert into order asset history for 6
                $insert_oah = array(
                    'ordered_asset_id' => $ordered_asset_id,
                    'order_status_id'  => $order_status_id6,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
                $oah_id6 = $this->Common_model->insert_data('order_asset_history',$insert_oah);
                #audit_data
                $new_data = array('transaction_status'=>'In Transit To FE');
                $remarks = "In Transit to FE Tool Order:".$trow['order_number'];
                audit_data('in_transit_asset',$oah_id6,'order_asset_history',1,$ad_id2,$new_data,array('asset_id'=>$asset_id),array(),$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);

                foreach ($asset_details as $key2 => $value2) 
                {
                    #insert into order asset health for 5
                    $insert_data3_1 = array(
                        'oah_id'             => $oah_id5,
                        'part_id'            => $value2['part_id'],
                        'asset_condition_id' => $value2['asset_condition_id'],
                        'remarks'            => $value2['remarks']);
                    $this->Common_model->insert_data('order_asset_health',$insert_data3_1);

                    #insert into order asset health for 6
                    $insert_data3_2 = array(
                        'oah_id'             => $oah_id6,
                        'part_id'            => $value2['part_id'],
                        'asset_condition_id' => $value2['asset_condition_id'],
                        'remarks'            => $value2['remarks']);
                    $this->Common_model->insert_data('order_asset_health',$insert_data3_2);
                }

                $asset_status = $asset_arr['status'];

                $ot_assets = $this->Common_model->get_data('asset_status_history',array('ordered_tool_id'=>$ordered_tool_id,'end_time'=>NULL,'status'=>2));

                $oa_assets = $this->Common_model->get_data('ordered_asset',array('ordered_tool_id'=>$ordered_tool_id,'status <',3));
                $asset_identity = 0;
                if(count($ot_assets)>0 && count($oa_assets))
                {
                    $ot_assets_arr = array();
                    foreach ($ot_assets as $key3 => $value3) 
                    {
                        $ot_assets_arr[] = $value3['asset_id'];
                    }
                    $oa_assets_arr = array();
                    foreach ($oa_assets as $key4 => $value4) 
                    {
                        $oa_assets_arr[] = $value4['asset_id'];
                    }
                    $final_assets = array_diff($oa_assets_arr,$ot_assets_arr);
                    foreach ($final_assets as $key5 => $value5) 
                    {
                        $asset_identity = $value5;
                        $asset_status_is = $this->Common_model->get_value('asset',array('asset_id'=>$asset_identity),'status');
                        if($asset_status_is == 2)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    foreach ($ot_assets as $key5 => $value5) 
                    {
                        $asset_identity = $value5['asset_id'];
                        $asset_status_is = $this->Common_model->get_value('asset',array('asset_id'=>$asset_identity),'status');
                        if($asset_status_is == 2)
                        {
                            break;
                        }
                    }
                }
                #conditions if asset status is 1
                if($asset_status == 1)
                {
                    #unlink the linked asset while tool order is placed.
                    if($asset_identity>0)
                    {
                        $first_asset = $asset_identity;
                        #audit 
                        $asset_arr1 = $this->Common_model->get_data_row('asset',array('asset_id'=>$first_asset));
                        $old_data = array('asset_status_id'=>$asset_arr1['status']);
                        #update asset
                        $update_a = array(
                            'status'        => 1,
                            'modified_by'   => $this->session->userdata('sso_id'),
                            'modified_time' => date('Y-m-d H:i:s')
                        );
                        $update_a_where = array('asset_id'=>$first_asset);
                        $this->Common_model->update_data('asset',$update_a,$update_a_where);

                        #audit data
                        $new_data = array('asset_status_id'=>1);
                        $remarks = "Unlinked From Tool Order while shipping:".$trow['order_number'];
                        audit_data('asset_master',$first_asset,'asset',2,'',$new_data,array('asset_id'=>$first_asset),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$asset_arr1['country_id']);

                        #update ash
                        $update_ash = array(
                            'end_time'    => date('Y-m-d H:i:s'),
                            'modified_by' => $this->session->userdata('sso_id')
                        );
                        $update_ash_where = array(
                            'asset_id' => $first_asset,
                            'end_time' => NULL
                        );
                        $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                        #insert in ash
                        $insert_ash = array(
                            'asset_id'        => $first_asset,
                            'created_by'      => $this->session->userdata('sso_id'),
                            'created_time'    => date('Y-m-d H:i:s'),
                            'status'          => 1
                        );
                        $this->Common_model->insert_data('asset_status_history',$insert_ash);
                    }
                }
                else if($asset_status == 2)
                {
                    $ot_assets_arr = array();
                    foreach ($ot_assets as $key3 => $value3) 
                    {
                        $ot_assets_arr[] = $value3['asset_id'];
                    }
                    if(count($ot_assets_arr)>0)
                    {
                        if(!in_array($asset_id, $ot_assets_arr))
                        {
                            if($asset_identity>0)
                            {
                                $old_ash = $this->Common_model->get_data_row('asset_status_history',array('end_time'=>NULL,'status'=>2,'asset_id'=>$asset_identity));

                                $new_ash = $this->Common_model->get_data_row('asset_status_history',array('end_time'=>NULL,'status'=>2,'asset_id'=>$asset_id));

                                if(count($old_ash)>0 && count($new_ash)>0)
                                {
                                    if($new_ash['tool_order_id']!='' && $new_ash['ordered_tool_id']!='' && $old_ash['tool_order_id']!='' && $old_ash['ordered_tool_id']!='')
                                    {
                                        #update old ASH
                                        $update_ash = array(
                                            'end_time'    => date('Y-m-d H:i:s'),
                                            'modified_by' => $this->session->userdata('sso_id')
                                        );
                                        $update_ash_where = array(
                                            'asset_id' => $asset_id,
                                            'end_time' => NULL
                                        );
                                        $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                                        #insert for old ash
                                        $insert_ash = array(
                                            'asset_id'        => $asset_identity,
                                            'tool_order_id'   => $new_ash['tool_order_id'],
                                            'ordered_tool_id' => $new_ash['ordered_tool_id'],
                                            'created_by'      => $this->session->userdata('sso_id'),
                                            'created_time'    => date('Y-m-d H:i:s'),
                                            'status'          => 2
                                        );
                                        $this->Common_model->insert_data('asset_status_history',$insert_ash);
                                        #audit data
                                        $ad_id3 = audit_data('tool_order',$new_ash['tool_order_id'],'tool_order',2,'',array(),array('tool_order_id'=>$new_ash['tool_order_id']),array(),'','',array(),'tool_order',$new_ash['tool_order_id'],$trow['country_id']);

                                        $old_data = array(
                                            'asset_status_id' => 2,
                                            'asset_id'        => $asset_id,
                                            'tool_order_id'   => $old_ash['tool_order_id']
                                        );

                                        $new_data = array(
                                            'asset_status_id' => 2,
                                            'asset_id'        => $asset_identity,
                                            'tool_order_id'   => $new_ash['tool_order_id']
                                        );
                                        audit_data('locked_assets',$asset_identity,'asset',2,$ad_id3,$new_data,array('asset_id'=>$asset_identity),$old_data,"Assets Swapped",'',array(),'tool_order',$new_ash['tool_order_id'],$trow['country_id']);

                                        #asset master update
                                        $old_data = array('asset_status_id'=>2);
                                        $new_data = array('asset_status_id'=>2);
                                        $tool_order_number = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$new_ash['tool_order_id']),'order_number');
                                        $remarks = "Locked For ".$tool_order_number;
                                        audit_data('asset_master',$asset_identity,'asset',2,'',$new_data,array('asset_id'=>$asset_identity),$old_data,$remarks,'',array(),'tool_order_id',$new_ash['tool_order_id'],$trow['country_id']);

                                        #update FE ASH
                                        $update_ash = array(
                                            'end_time'    => date('Y-m-d H:i:s'),
                                            'modified_by' => $this->session->userdata('sso_id')
                                        );
                                        $update_ash_where = array(
                                            'asset_id' => $asset_identity,
                                            'end_time' => NULL
                                        );
                                        $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                                        #insert in new FE ash
                                        $insert_ash = array(
                                            'asset_id'        => $asset_id,
                                            'tool_order_id'   => $old_ash['tool_order_id'],
                                            'ordered_tool_id' => $old_ash['ordered_tool_id'],
                                            'created_by'      => $this->session->userdata('sso_id'),
                                            'created_time'    => date('Y-m-d H:i:s'),
                                            'status'          => 2
                                        );
                                        $this->Common_model->insert_data('asset_status_history',$insert_ash);
                                        #audit data
                                        $ad_id4 = audit_data('tool_order',$old_ash['tool_order_id'],'tool_order',2,'',array(),array('tool_order_id'=>$old_ash['tool_order_id']),array(),'','',array(),'tool_order',$old_ash['tool_order_id'],$trow['country_id']);
                                        $old_data = array(
                                            'asset_status_id' => 2,
                                            'asset_id'        => $asset_identity,
                                            'tool_order_id'   => $new_ash['tool_order_id']
                                        );

                                        $new_data = array(
                                            'asset_status_id' => 2,
                                            'asset_id'        => $asset_id,
                                            'tool_order_id'   => $old_ash['tool_order_id']

                                        );
                                        audit_data('locked_assets',$asset_id,'asset',2,$ad_id4,$new_data,array('asset_id'=>$asset_id),$old_data,"Assets Swapped",'',array(),'tool_order',$old_ash['tool_order_id'],$trow['country_id']);
                                    }
                                }
                            }
                        }
                    }
                }

                #update ash
                $update_ash = array(
                    'end_time'    => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('sso_id')
                )   ;
                $update_ash_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                #insert ash 
                $insert_ash = array(
                    'asset_id'        => $asset_id,
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s'),
                    'tool_order_id'   => $tool_order_id,
                    'current_stage_id'=> 6,
                    'status'          => 8
                );
                $this->Common_model->insert_data('asset_status_history',$insert_ash);

                #audit data
                $old_data = array(
                    'asset_status_id' => $asset_arr['status'],
                    'tool_availability' => get_asset_position($asset_id)
                );

                #update asset with in transit status 8
                $update_a = array(
                    'status'        => 8,
                    'b_inventory'   => 0,
                    'modified_by'   => $this->session->userdata('sso_id'),
                    'modified_time' => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id'=>$asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                #update asset position
                $update_ap = array('to_date' => date('Y-m-d H:i:s'));
                $update_ap_w = array('asset_id'=>$asset_id, 'to_date'=>NULL);
                $this->Common_model->update_data('asset_position',$update_ap,$update_ap_w);

                #insert asset position
                $insert_asset_position = array(
                    'asset_id'  => $asset_id,
                    'transit'   => 1,
                    'sso_id'    => $trow['sso_id'],
                    'from_date' => date('Y-m-d H:i:s'),
                    'status'       => 1
                );
                $this->Common_model->insert_data('asset_position',$insert_asset_position);

                #audit data
                $new_data = array(
                    'asset_status_id'   => 8,
                    'tool_availability' => get_asset_position($asset_id)
                );
                $remarks = "Shipped for Tool Order:".$trow['order_number'];
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);
            }
        }

        #close all osh with status 5
        $update_order_status_history = array('end_time' => date('Y-m-d H:i:s'));
        $update_where1 = array(
            'tool_order_id'   => $tool_order_id,
            'current_stage_id'=> 5,
            'end_time'        => NULL
        );
        $this->Common_model->update_data('order_status_history',$update_order_status_history,$update_where1);

        #change tool order status to 6 = intransit to FE
        $update_tool_order = array(
            'current_stage_id' => 6,//at transit from wh to FE
            'modified_by'      => $this->session->userdata('sso_id'),
            'modified_time'    => date('Y-m-d H:i:s'),
            'status'           => 1
        );
        $update_where = array('tool_order_id' => $tool_order_id);
        $this->Common_model->update_data('tool_order',$update_tool_order,$update_where);

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'fe_delivery_list'); exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Details Has been submitted successfully!
            </div>'); 
            redirect(SITE_URL.'fe_invoice_print/'.storm_encode($tool_order_id)); 
        }
	}

    //
	public function deactivate_order_delivery_doc()
	{
        #page authentication
        $task_access = page_access_check('fe_delivery');

		$order_delivery_doc_id = validate_number($this->input->post('doc_id',TRUE));
        $update_data = array('status'=>2);
        $update_where = array('order_delivery_doc_id'=>$order_delivery_doc_id);
        $res = $this->Common_model->update_data('order_delivery_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
	}

	public function activate_order_delivery_doc()
	{
        #page authentication
        $task_access = page_access_check('fe_delivery');

		$order_delivery_doc_id = validate_number($this->input->post('doc_id',TRUE));
        $update_data = array('status'=>1);
        $update_where = array('order_delivery_doc_id'=>$order_delivery_doc_id);
        $res = $this->Common_model->update_data('order_delivery_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
	}

    public function closed_fe_delivery_list()
    {
        $data['nestedView']['heading']="Closed FE Shipment List";
        $data['nestedView']['cur_page'] = "closed_fe_delivery";
        $data['nestedView']['parent_page'] = 'closed_fe_delivery';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed FE Shipment List';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Shipment List','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchfedelivery',TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'tool_order_number' => validate_string($this->input->post('tool_order_number', TRUE)),
            'ssoid'             => validate_string($this->input->post('ssoid',TRUE)),
            'whc_id'            => validate_number(@$this->input->post('wh_id',TRUE)),
            'country_id'        => validate_number(@$this->input->post('country_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'tool_order_number' => $this->session->userdata('tool_order_number'),
                'ssoid'             => $this->session->userdata('ssoid'),
                'whc_id'            => $this->session->userdata('whc_id'),
                'country_id'        => $this->session->userdata('country_id')
                                  );
            }
            else 
            {
                $searchParams=array(
                'tool_order_number' => '',
                'ssoid'             => '',
                'page_redirect'     => '',
                'whc_id'            => '',
                'country_id'        => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;
        
        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_fe_delivery_list/';
        # Total Records
        $config['total_rows'] = $this->Fe_delivery_m->closed_fe_delivery_list_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['closed_fe_deliveryResults'] = $this->Fe_delivery_m->closed_fe_delivery_list_results($current_offset, $config['per_page'], $searchParams,$task_access);

        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        # Additional data
        $data['displayResults'] = 1;
        $data['current_offset'] = $current_offset;
        $this->load->view('fe_delivery/closed_fe_delivery_list',$data);
    }

	public function view_fe_delivery_details()
	{
		$tool_order_id = storm_decode($this->uri->segment(2));
		if($tool_order_id == '')
		{
			redirect(SITE_URL.'closed_fe_delivery_list'); exit();
		}

		$data['nestedView']['heading']="Closed FE Shipment Details";
		$data['nestedView']['cur_page'] = "closed_fe_delivery";
		$data['nestedView']['parent_page'] = 'closed_fe_delivery';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Closed FE Shipment Details';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Shipment List','class'=>'','url'=>SITE_URL.'closed_fe_delivery_list');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Shipment Details','class'=>'active','url'=>'');

		#additional details
        $data['tool_order_id'] = $tool_order_id;
        $data['trow'] = $this->Fe_delivery_m->get_tool_order_details($tool_order_id);
        $order_tool = $this->Fe_delivery_m->get_ordered_tool($tool_order_id);
        $tool_order_wh_id = $data['trow']['wh_id'];
        $check_no = 1;
        foreach ($order_tool as $key => $value) 
        {
            $asset = $this->Fe_delivery_m->get_oa_by_ot($value['ordered_tool_id']);
            $order_tool[$key]['assets'] = $asset['assets'];
            $order_tool[$key]['quantity'] = $value['requested_qty'];
            $order_tool[$key]['serial_nos'] = $asset['serial_nos'];
        }
        # Additional data
        $data['flg'] = 1;
        $data['order_tool'] = $order_tool;
        $data['drow'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('order_delivery_doc',array('order_delivery_id'=>$data['drow']['order_delivery_id']));

		$this->load->view('fe_delivery/closed_fe_delivery_list',$data);
	}

    public function fe_invoice_print()
    {
        #page authentication
        $task_access = page_access_check('fe_delivery');

        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'fe_delivery'); exit();
        }
        $_SESSION['page_redirect'] = 1;
        $delivery = $this->Fe_delivery_m->get_delivery_details($tool_order_id);
        $data['tool_order'] = $this->Fe_delivery_m->get_fe_address($tool_order_id);
        $address_info = $data['tool_order'];
        
        $tools_list = $this->Fe_delivery_m->get_invoiced_tools($tool_order_id);
        $country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
        $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id,'status'=>1),'name');
        $total_amount = 0;
        foreach ($tools_list as $key => $value) 
        {
            $cost = $value['cost_in_inr'];
            $gst_percent = $value['gst_percent'];
            $gst = explode("%", $gst_percent);
            $cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
            if($delivery['print_type'] == 3)
            {
                $cgst_percent = round($gst[0]/2,2);
                $cgst_amt = ($cost*$cgst_percent)/100;
                $sgst_percent = $cgst_percent;
                $sgst_amt = $cgst_amt; 
            }
            else if($delivery['print_type'] == 4)
            {
                $igst_percent = $gst[0];
                $igst_amt = ($cost*$igst_percent)/100;   
            }
            $tax_amount = ($cost * $gst[0])/100;
            $amount = $cost+$tax_amount;
            $total_amount+= ($cost+$tax_amount);
            
            $tools_list[$key]['cgst_percent'] = $cgst_percent;
            $tools_list[$key]['cgst_amt'] = $cgst_amt;
            $tools_list[$key]['sgst_percent'] = $sgst_percent;
            $tools_list[$key]['sgst_amt'] = $sgst_amt;
            $tools_list[$key]['igst_percent'] = $igst_percent;
            $tools_list[$key]['igst_amt'] = $igst_amt;
            $tools_list[$key]['taxable_value'] = $cost;
            $tools_list[$key]['tax_amount'] = $tax_amount;
            $tools_list[$key]['amount'] = $amount;
        }
        $data['total_amount'] = $total_amount;
        $data['delivery'] = $delivery;
        $data['tools_list'] = $tools_list;
        $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$delivery['billed_to']));

        //Trigger mail to FE
        $assets_list = $this->Fe_delivery_m->get_invoiced_assets($tool_order_id);
        if(count($address_info)>0)
            sendFeDeliveryMail($address_info,$assets_list);

        $this->load->view('fe_delivery/dc_print',$data);
        
    }

    public function cancel_fe_print()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_fe_delivery_list'); exit();
        }
        $data['nestedView']['heading']="Cancel Generated Print";
        $data['nestedView']['cur_page'] = "check_fe_delivery";
        $data['nestedView']['parent_page'] = 'closed_fe_delivery';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Cancel Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Shipment List','class'=>'','url'=>SITE_URL.'closed_fe_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Cancel Generated Print','class'=>'active','url'=>'');

        #additional Data
        $data['cancel_type_list'] = $this->Common_model->get_data('cancel_type',array('status'=>1));
        $data['tool_order_id'] = $tool_order_id;
        $data['order_number'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'order_number');
        $order_delivery_id = $this->Common_model->get_value('order_delivery',array('tool_order_id'=>$tool_order_id),'order_delivery_id');
        $print_id = $this->Common_model->get_value('order_delivery',array('order_delivery_id'=>$order_delivery_id),'print_id');
        $data['print_type'] = $this->Common_model->get_value('print_format',array('print_id'=>$print_id),'print_type');
         $data['format_number'] = $this->Common_model->get_value('print_format',array('print_id'=>$print_id),'format_number');

        $this->load->view('fe_delivery/cancel_fe_print',$data);
    }

    public function insert_cancel_fe_print()
    {
        #page authentication
        $task_access = page_access_check('closed_fe_delivery');

        $tool_order_id = validate_number(storm_decode($this->input->post('tool_order_id',TRUE)));

        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_fe_delivery_list'); exit;
        }
        $od_arr = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $order_delivery_id = $od_arr['order_delivery_id'];
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));

        $update_print_history = array(
            'remarks'=>validate_string($this->input->post('reason',TRUE)),
            'modified_by' => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $update_print_where = array('order_delivery_id'=>$order_delivery_id,'modified_time'=>NULL);
        
        $this->Common_model->update_data('print_history',$update_print_history,$update_print_where);

        $print_type = $this->input->post('print_type',TRUE);
        $tool_order_wh_id = $trow['wh_id'];
        $print_id = get_current_print_id($print_type,$tool_order_wh_id);

        #audit data
        $old_data = array('print_id' => $od_arr['print_id']);

        $update_order_delivery = array(
            'print_id'      => $print_id,
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $update_od_where = array('order_delivery_id'=>$order_delivery_id);
        $this->Common_model->update_data('order_delivery',$update_order_delivery,$update_od_where);

        #audit data
        $new_data = array('print_id' => $print_id);
        $remarks = "FE Shipment print has updated";
        audit_data('tool_order_delivery',$order_delivery_id,'order_delivery',2,'',$new_data,array('tool_order_id'=>$tool_order_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);

        $insert_print_history = array(
            'print_id'          => $print_id,
            'remarks'           => 'Print Got updated',
            'order_delivery_id' => $order_delivery_id,
            'created_by'        => $this->session->userdata('sso_id'),
            'created_time'      => date('Y-m-d H:i:s')
        );
        $this->Common_model->insert_data('print_history',$insert_print_history);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'closed_fe_delivery_list'); exit();
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Print Has been Updated successfully!
            </div>'); 
            redirect(SITE_URL.'fe_invoice_print/'.storm_encode($tool_order_id)); 
        } 
    }

    public function update_fe_print()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_fe_delivery_list'); exit();
        }
        $data['nestedView']['heading']="Update Generated Print";
        $data['nestedView']['cur_page'] = "check_fe_delivery";
        $data['nestedView']['parent_page'] = 'closed_fe_delivery';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/update_fe_delivery.js"></script>';

        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Shipment List','class'=>'','url'=>SITE_URL.'closed_fe_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Generated Print','class'=>'active','url'=>'');

        $data['tool_order_id'] = $tool_order_id;
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $data['order_number'] = $trow['order_number'];
        $swh_id = $trow['wh_id'];
        $country_id = $trow['country_id'];

        $data['od'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $order_delivery_id = $data['od']['order_delivery_id'];

        $print_id = $this->Common_model->get_value('order_delivery',array('order_delivery_id'=>$order_delivery_id),'print_id');
        $prow = $this->Common_model->get_data_row('print_format',array('print_id'=>$print_id));
        $data['print_type'] = $prow['print_type'];
        $data['print_date'] = $prow['created_time'];
        $data['format_number'] = $prow['format_number'];
        $data['order_address'] = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$swh_id));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
        $this->load->view('fe_delivery/update_fe_print',$data);
    }

    public function update_fe_print_details()
    {
        #page authentication
        $task_access = page_access_check('closed_fe_delivery');

        $tool_order_id = storm_decode($this->input->post('tool_order_id',TRUE));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_fe_delivery_list'); exit();
        }
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $od_arr = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $order_delivery_id = $od_arr['order_delivery_id'];

        $courier_type = validate_number($this->input->post('courier_type',TRUE));
        $courier_name = validate_string($this->input->post('courier_name',TRUE));
        $courier_number = validate_string($this->input->post('courier_number',TRUE));
        $contact_person = validate_string($this->input->post('contact_person',TRUE));
        $phone_number = validate_string($this->input->post('phone_number',TRUE));
        $expected_delivery_date = validate_string(format_date($this->input->post('expected_delivery_date',TRUE)));
        $vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
        $remarks        = validate_string($this->input->post('remarks',TRUE));
        $date = validate_string($this->input->post('print_date',TRUE));
        $print_date = date('Y-m-d',strtotime($date));
        if($remarks == ""){ $remarks = NULL; }

        if($courier_type == 1)
        {
            $phone_number   = NULL;
            $contact_person = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 2)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 3)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
        }
        $billed_to = validate_number($this->input->post('billed_to',TRUE));
        $update_details = array(
            'tool_order_id' => $tool_order_id,
            'courier_type'  => $courier_type,
            'contact_person'=> $contact_person,
            'phone_number'  => $phone_number,
            'courier_name'  => $courier_name,
            'courier_number'=> $courier_number,
            'vehicle_number'=> $vehicle_number,
            'expected_delivery_date'=> $expected_delivery_date,
            'remarks'       => $remarks,
            'billed_to'     => $billed_to,
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s'),
            'status'        => 1
        );
        $update_details_where = array('order_delivery_id'=>$order_delivery_id);
        $this->db->trans_begin();
        $this->Common_model->update_data('order_delivery',$update_details,$update_details_where);

        #audit data
        unset($update_details['modified_by'],$update_details['modified_time']);
        $final_arr = array_diff_assoc($update_details, $od_arr);
        if(count($final_arr)>0)
        {
            $remarks = "Updated FE Shipment Details:".$trow['order_number'];
            $ad_id = audit_data('tool_order_delivery',$order_delivery_id,'order_delivery',2,'',$final_arr,array('tool_order_id'=>$tool_order_id),$od_arr,$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);
        }

        #Update Order Address
        $oa_arr = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id,'status'=>1));
        $update_oa = array(
            'address1'      => validate_string($this->input->post('address1',TRUE)),
            'address2'      => validate_string($this->input->post('address2',TRUE)),
            'address3'      => validate_string($this->input->post('address3',TRUE)),
            'address4'      => validate_string($this->input->post('address4',TRUE)),
            'pin_code'      => validate_string($this->input->post('pin_code',TRUE)),
            'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
            'pan_number'    => validate_string($this->input->post('pan_number',TRUE)),
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $update_oa_where = array('tool_order_id'=>$tool_order_id,'status'=>1);
        $this->Common_model->update_data('order_address',$update_oa,$update_oa_where);

        #audit data
        unset($update_oa['modified_by'],$update_oa['modified_time']);
        $final_arr = array_diff_assoc($update_oa, $oa_arr);
        if(count($final_arr)>0)
        {
            if(!isset($ad_id))
            {
                $ad_id = audit_data('tool_order_delivery',$order_delivery_id,'order_delivery',2,'',array(),array('tool_order_id'=>$tool_order_id),array(),'','',array(),'tool_order',$tool_order_id,$trow['country_id']);
            }
            $remarks = "Updated Order Address for FE Shipment Details:".$trow['order_number'];
            $ad_id = audit_data('tool_order_address',$oa_arr['order_address_id'],'order_address',2,$ad_id,$final_arr,array('tool_order_id'=>$tool_order_id),$oa_arr,$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);
        }

        $print_id = $this->Common_model->get_value('order_delivery',array('tool_order_id'=>$tool_order_id),'print_id');

        #update print date
        update_print_format_date($print_date,$print_id);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'closed_fe_delivery_list'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Details Has been Updated successfully!
            </div>'); 
            redirect(SITE_URL.'fe_invoice_print/'.storm_encode($tool_order_id)); 
        } 
    }

    /*gowri*/
    public function fe_delivery_admin()
    {
        $data['nestedView']['heading']="FE Shipment Requests";
        $data['nestedView']['cur_page'] = "fe_delivery";
        $data['nestedView']['parent_page'] = 'fe_delivery';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'FE Shipment Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Shipment Requests','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchfedelivery1',TRUE));
        $courier_date = validate_string($this->input->post('courier_date',TRUE));
        if($courier_date != '')
        {
            $date = strtotime("+2 days", strtotime($courier_date));
            $courier_date =  date("Y-m-d", $date);
        }   
        if($psearch!='') 
        {
            $searchParams=array(
            'tool_order_number' => validate_string($this->input->post('tool_order_number', TRUE)),
            'courier_date'      => $courier_date,
            'ssoid'             => validate_string($this->input->post('ssoid',TRUE)),
            'whc_id'            => validate_number(@$this->input->post('wh_id',TRUE)),
            'country_id'        => validate_number(@$this->input->post('country_id',TRUE)),
            'service_type_id'   => validate_number(@$this->input->post('service_type_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'tool_order_number' => $this->session->userdata('tool_order_number'),
                'courier_date'      => $this->session->userdata('courier_date'),
                'ssoid'             => $this->session->userdata('ssoid'),
                'whc_id'            => $this->session->userdata('whc_id'),
                'country_id'        => $this->session->userdata('country_id'),
                'service_type_id'   => $this->session->userdata('service_type_id')
                );
            }
            else 
            {
                $searchParams=array(
                'tool_order_number' => '',
                'courier_date'      => '',
                'ssoid'             => '',
                'page_redirect'     => '',
                'whc_id'            => '',
                'country_id'        => '',
                'service_type_id'   => ''
                );
                $this->session->set_userdata($searchParams);
            }  
        }
        $data['search_data'] = $searchParams;
        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'fe_delivery_admin/';
        # Total Records
        $config['total_rows'] = $this->Fe_delivery_m->fe_delivery_list_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $fe_deliveryResults = $this->Fe_delivery_m->fe_delivery_list_results($current_offset, $config['per_page'], $searchParams,$task_access);

        foreach ($fe_deliveryResults as $key => $value) 
        {
            $ordered_tool_list = $this->Fe_delivery_m->get_ordered_tool($value['tool_order_id']);
            foreach ($ordered_tool_list as $key1 => $row) 
            {
                $avail_qty = $this->Fe_delivery_m->get_avail_tool($row['tool_id'],$row['wh_id']);
                $ordered_tool_list[$key1]['avail_qty'] = $avail_qty['avail_qty'];
            }
            $fe_deliveryResults[$key]['ordered_tool'] = $ordered_tool_list;
        }
        # Additional data
        $data['fe_deliveryResults'] = $fe_deliveryResults;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['service_type']=$this->Common_model->get_data('service_type',array('status'=>1));
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;   
        $this->load->view('fe_delivery/fe_delivery_admin',$data);
    }
} 
?>