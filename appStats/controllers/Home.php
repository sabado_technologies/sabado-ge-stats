<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Home extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
        $this->load->model('User_model');
	}
	
	public function index()
	{
    	$data['nestedView']['heading']="STATS Home";
    	$data['nestedView']['cur_page'] = 'home';
    	$data['nestedView']['parent_page'] = 'home';

        # include files
    	$data['nestedView']['js_includes'] = array();
    	$data['nestedView']['css_includes'] = array();
    		
    	# Breadcrumbs
    	$data['nestedView']['breadCrumbTite'] = 'Home Page';
    	$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));

    	$role = $this->session->userdata('main_role_id');
    	if($role==2 || $role==5 || $role==6)
    	{
    		$l_modal = $this->session->userdata('l_modal');
    		if($l_modal!=2)
    		{
    			$data['l_modal'] = 1;
    			$this->session->set_userdata($data);

                $country_id = $this->session->userdata('s_country_id');
                $data['location_list']= $this->User_model->get_location_city($country_id);
    		}
    	}   
    	
        $_SESSION['onbehalf_fe'] = '';
    	$this->load->view('home',$data);
	}
}