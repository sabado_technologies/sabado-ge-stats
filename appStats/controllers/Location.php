<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
 * created by Roopa on 13th june 2017 11:00AM
*/
class Location extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Location_m');
	}

    public function assign_country_region()
    {
        $location_id = validate_number(storm_decode($this->uri->segment(2)));
        $region_id = validate_number($this->uri->segment(3));
        if($location_id=='' || $region_id=='')
        {
            redirect(SITE_URL.'home'); exit();
        }
        $update_data = array('region_id'=>$region_id);
        $update_data_where = array('location_id'=>$location_id);
        $this->Common_model->update_data('location',$update_data,$update_data_where);

        $country_name = $this->Common_model->get_value('location',array('location_id'=>$location_id),'name');
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <div class="icon"><i class="fa fa-check"></i></div>
      <strong>Success!</strong> Region : <strong>'.$region_id.'</strong> has been updated successfully for country :<strong> '.$country_name.'</strong>! </div>'); 
        redirect(SITE_URL.'country');

    }

    //Location add..
    public function location_add()
    {
        if(validate_string($this->input->post('submit_location',TRUE)) != "")
        {
            $parent_page = validate_string($this->input->post('parent_page',TRUE));
            $task_access = page_access_check($parent_page);

            $location_id = validate_number($this->input->post('location_id',TRUE));
            $name        = validate_string($this->input->post('name',TRUE));
            $parent_id   = validate_string($this->input->post('parent_id',TRUE));
            $short_name  = validate_string(@$this->input->post('short_name',TRUE));
            $level_id    = validate_number($this->input->post('level_id',TRUE));
            $currency_name = validate_string($this->input->post('currency_name',TRUE));
            /*$cal_trigger_days  = validate_string(@$this->input->post('cal_trigger_days',TRUE));*/

            if($location_id == "")
            {  
                $territory_level_name = $this->Common_model->get_value('territory_level', array('level_id'=>$level_id), 'name');
                $this->db->trans_begin();
                #insert location
                $insert_data = array('name'         => $name,
                                     'parent_id'    => $parent_id,
                                     'level_id'     => $level_id,
                                     'created_by'   => $this->session->userdata('sso_id'),
                                     'created_time' => date('Y-m-d H:i:s'),
                                     'status'       => 1);
                if($short_name !=''){ $insert_data['short_name'] = $short_name; }
                /*if($cal_trigger_days !=''){ $insert_data['cal_trigger_days'] = $cal_trigger_days; }*/
                $location_id = $this->Common_model->insert_data('location',$insert_data);

                #insert currency
                if($currency_name!='' && $location_id!='')
                {
                    $insert_currency = array('name'        => $currency_name,
                                             'location_id' => $location_id,
                                             'status'      => 1);
                    $this->Common_model->insert_data('currency',$insert_currency);
                }
                
                if(strtolower($territory_level_name) == 'country')
                {
                    $insert_buffer_wh = array(
                    'name'          => 'Buffer Warehouse',
                    'wh_code'       => 'TI00',
                    'location_id'   => $location_id,
                    'managed_by'    => 'NA',
                    'phone'         => 'NA',
                    'email'         => 'NA',
                    'contact_info1' => 'NA',
                    'contact_info2' => 'NA',
                    'contact_info3' => 'NA',
                    'address1'      => 'NA',
                    'address2'      => 'NA',
                    'address3'      => 'NA',
                    'address4'      => 'NA',
                    'pin_code'      => 'NA',
                    'tin_number'    => 'NA',
                    'cst_number'    => 'NA',
                    'pan_number'    => 'NA',
                    'gst_number'    => 'NA',
                    'created_by'    => $this->session->userdata('sso_id'),
                    'created_time'  => date('Y-m-d H:i:s'),
                    'status'        => 3,
                    'country_id'    => $location_id);
                    $this->Common_model->insert_data('warehouse',$insert_buffer_wh);

                }
                if($this->db->trans_status()===FALSE)
                {
                    $this->db->trans_rollback();                     
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong>Something Went Wrong! Please check.
                    </div>');             
                }
                else
                {
                    $this->db->trans_commit();
                    $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> Location : <strong> '.$name.' </strong> has been added successfully! </div>');     
                }             
            }
            else
            {
                $territory_level_name = $this->Common_model->get_value('territory_level', array('level_id'=>$level_id), 'name');
                $this->db->trans_begin();

                #update location 
                $update_data = array('name'          => $name,
                                     'parent_id'     => $parent_id,
                                     'level_id'      => $level_id,
                                     'modified_by'   => $this->session->userdata('sso_id'),
                                     'modified_time' => date('Y-m-d H:i:s'),
                                     'status'        => 1);
                if($short_name !=''){ $update_data['short_name'] = $short_name; }
                /*if($cal_trigger_days !=''){ $update_data['cal_trigger_days'] = $cal_trigger_days; }*/
                $update_where = array('location_id' => $location_id);
                $this->Common_model->update_data('location',$update_data, $update_where);

                #update Currency
                if($currency_name!='' && $location_id!='')
                {
                    $update_currency = array('name'        => $currency_name,
                                             'location_id' => $location_id,
                                             'status'      => 1);
                    $this->Common_model->update_data('currency',$update_currency, $update_where);
                }
                if($this->db->trans_status()===FALSE)
                {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong>Something Went Wrong! Please check.
                                             </div>');              
                }
                else
                {
                    $this->db->trans_commit();
                    $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <div class="icon"><i class="fa fa-check"></i></div>
                      <strong>Success!</strong> Location : <strong>'.$name.'</strong> has been updated successfully! </div>');  
                }        
            }
            redirect(SITE_URL.strtolower($territory_level_name));
        }
    }

    //Country view..
	public function country()
    {
        $data['nestedView']['heading']="Manage Country";
		$data['nestedView']['cur_page'] = 'country_master';
		$data['nestedView']['parent_page'] = 'country_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['task_access'] = $task_access;
        $data['parent_page'] = $data['nestedView']['parent_page'];

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Manage Country';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Country','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcountry', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'country_name' => validate_string($this->input->post('country_name', TRUE)),
            'currency_name' => validate_string($this->input->post('currency_name', TRUE)),
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                              'country_name' => $this->session->userdata('country_name'),
                              'currency_name' => $this->session->userdata('currency_name')
                              );
            }
            else {
                $searchParams=array(
                                    'country_name' => '',
                                    'currency_name' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'country/';
        # Total Records
        $config['total_rows'] = $this->Location_m->country_total_num_rows($searchParams);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['country_results'] = $this->Location_m->country_results($current_offset, $config['per_page'], $searchParams);
        
        # Additional data
        $data['displayResults'] = 1;
        $this->load->view('locations/country_view',$data);
    }
    //Country add..
    public function add_country()
    {
        $data['nestedView']['heading']="Add New Country";
		$data['nestedView']['cur_page'] = 'check_location';
		$data['nestedView']['parent_page'] = 'country_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['task_access'] = $task_access;
        $data['parent_page'] = $data['nestedView']['parent_page'];

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Add New Country';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Country','class'=>'','url'=>SITE_URL.'country');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Country','class'=>'active','url'=>'');
        # Additional data
        $data['flg'] = 1;            
        $data['displayResults'] = 0;
        $data['parent_id'] = 1;
        $data['level_id'] = 2;

        $this->load->view('locations/country_view',$data);
    }

    //Country Edit..
    public function edit_country()
    {
        $country_id=@storm_decode($this->uri->segment(2));
        if($country_id=='')
        {
            redirect(SITE_URL.'country');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Country";
		$data['nestedView']['cur_page'] = 'check_location';
		$data['nestedView']['parent_page'] = 'country_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['task_access'] = $task_access;
        $data['parent_page'] = $data['nestedView']['parent_page'];

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Edit Country';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Country','class'=>'','url'=>SITE_URL.'country');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Country','class'=>'active','url'=>'');

         # Additional data
        $data['val'] = 1;
        $data['flg'] = 2;
        $data['displayResults'] = 0;
        
        $where = array('location_id' => $country_id);
        $data['country_edit'] = $this->Common_model->get_data_row('location', $where); 
        $data['level_id'] = $data['country_edit']['level_id'];
        $data['parent_id'] = $data['country_edit']['parent_id'];  
        $data['currency_name'] = $this->Common_model->get_value('currency',$where,'name');
        $this->load->view('locations/country_view',$data);
    }
    //Deactivating Country..
    public function deactivate_country($encoded_id)
    {
        $task_access = page_access_check('country_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'country');
            exit;
        }
        $where = array('location_id' => $location_id);
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('location',$data_arr, $where);
        
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> country has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'country');

    }
    //acitivating Country..
    public function activate_country($encoded_id)
    {
        $task_access = page_access_check('country_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'country');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Success!</strong> Country has been Activated successfully!
                                     </div>');
        redirect(SITE_URL.'country');
    }
    //Check country Name Uniqueness..
    public  function check_country()
    {    
        $name = validate_string($this->input->post('name',TRUE));
        $location_id = validate_number($this->input->post('location_id',TRUE));
        $data = array('name'=>$name,'location_id'=>$location_id);
        $result = $this->Location_m->check_country($data);
        echo $result;
    }

    //Zone view..
    public function zone()
    {
        $data['nestedView']['heading']="Manage Zone";
        $data['nestedView']['cur_page'] = 'zone_master';
        $data['nestedView']['parent_page'] = 'zone_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Manage Zone';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Zone','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('search_zone', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'zone_name'  => validate_string($this->input->post('zone_name', TRUE)),
                               'country_id' => validate_number($this->input->post('country_id', TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                              'zone_name'  => $this->session->userdata('zone_name'),
                              'country_id' =>  $this->session->userdata('country_id'));
            }
            else {
                $searchParams=array(
                                    'zone_name'  => '',
                                    'country_id' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        //print_r($searchParams); exit();
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'zone/';
        # Total Records
        $config['total_rows'] = $this->Location_m->zone_total_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['zone_results'] = $this->Location_m->zone_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['displayResults'] = 1;
        $this->load->view('locations/zone_view',$data);
    }
    //Add Zone..
    public function add_zone()
    {
        $data['nestedView']['heading']="Add New Zone";
        $data['nestedView']['cur_page'] = 'check_location';
        $data['nestedView']['parent_page'] = 'zone_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['parent_page'] = $data['nestedView']['parent_page'];
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Select Country";
                $data['nestedView']['cur_page'] = 'check_location';
                $data['nestedView']['parent_page'] = 'zone_master';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Zone','class'=>'','url'=>SITE_URL.'zone');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_zone';
                $data['cancel'] = SITE_URL.'zone';
                $this->load->view('user/intermediate_page',$data); 
            }
        }

        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';        
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New Zone';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Zone','class'=>'','url'=>SITE_URL.'zone');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Zone','class'=>'active','url'=>'');
            //getting zone details.
            $data['level_id'] = 3;
            $data['flg'] = 1;      
            $data['country_id'] = $country_id;      
            $data['displayResults'] = 0;

            # Load page with all shop details
            $this->load->view('locations/zone_view', $data);
        }
    }
    //Edit Zone..
    public function edit_zone()
    {
        $location_id=@storm_decode($this->uri->segment(2));
        if($location_id=='')
        {
            redirect(SITE_URL.'zone');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Zone";
        $data['nestedView']['cur_page'] = 'check_location';
        $data['nestedView']['parent_page'] = 'zone_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['parent_page'] = $data['nestedView']['parent_page'];
        $country_id = $this->Common_model->get_value('location',array('location_id'=>$location_id),'parent_id');
        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit Zone';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Zone','class'=>'','url'=>SITE_URL.'zone');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Zone','class'=>'active','url'=>'');

            #country results
            $where = array('location_id' => $location_id);
            $data['zone_edit'] = $this->Common_model->get_data('location', $where);
            $data['level_id'] = $data['zone_edit'][0]['level_id'];
            $data['country_id'] = $country_id;
            $data['flg'] = 2;
            $data['val'] = 1;

            # Load page with all shop details
            $this->load->view('locations/zone_view', $data);
        }
    }

    //deactivate zone
    public function deactivate_zone($encoded_id)
    {
        $task_access = page_access_check('zone_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'zone');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Zone has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'zone');
    }

    //activate zone
    public function activate_zone($encoded_id)
    {
        $task_access = page_access_check('zone_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'zone');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Zone has been Activated successfully!
        </div>');
        redirect(SITE_URL.'zone');
    }

    //check Zone name
    public  function check_zone()
    {

        $name = validate_string($this->input->post('name',TRUE));
        $parent_id = validate_number($this->input->post('parent_id',TRUE));
        $location_id = validate_number($this->input->post('location_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('name'=>$name,'parent_id'=>$parent_id,'location_id'=>$location_id,'country_id'=>$country_id);
        $result = $this->Location_m->check_zone($data);
        echo $result;
    }

    //state view
    public function state()
    {
        $data['nestedView']['heading']="Manage State";
        $data['nestedView']['cur_page'] = 'state_master';
        $data['nestedView']['parent_page'] = 'state_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['task_access'] = $task_access;

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Manage State';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage State','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('search_state', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'state_name' => validate_string($this->input->post('state_name', TRUE)),
                               'zone_id'    => validate_number($this->input->post('zone_id', TRUE)),
                               'country_id' => validate_number($this->input->post('country_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                              'state_name' => $this->session->userdata('state_name'),
                              'zone_id'    => $this->session->userdata('zone_id'),
                              'country_id' => $this->session->userdata('country_id'));
            }
            else {
                $searchParams=array(
                                    'state_name' => '',
                                    'zone_id'    => '',
                                    'country_id' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        //print_r($searchParams); exit();
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'state/';
        # Total Records
        $config['total_rows'] = $this->Location_m->state_total_num_rows($searchParams,$task_access);
        //echo $config['total_rows']; exit();

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['state_results'] = $this->Location_m->state_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        # Additional data
        $data['zoneList'] = $this->Location_m->get_zone_by_country($task_access);
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['displayResults'] = 1;

        $this->load->view('locations/state_view',$data);
    }

    //add state
    public function add_state()
    {
        $data['nestedView']['heading']="Add New State";
        $data['nestedView']['cur_page'] = 'check_location';
        $data['nestedView']['parent_page'] = 'state_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['parent_page'] = $data['nestedView']['parent_page'];
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Select Country";
                $data['nestedView']['cur_page'] = 'check_location';
                $data['nestedView']['parent_page'] = 'state_master';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage State','class'=>'','url'=>SITE_URL.'state');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_state';
                $data['cancel'] = SITE_URL.'state';
                $this->load->view('user/intermediate_page',$data); 
            }
        }

        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New State';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage State','class'=>'','url'=>SITE_URL.'state');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New State','class'=>'active','url'=>'');
            $data['zoneList'] = $this->Location_m->get_zone_by_country(0,$country_id);

            $data['level_id'] = 4;
            $data['flg'] = 1;
            $data['val'] = 0;
            $data['country_id'] = $country_id;
            # Load page with all shop details
            $this->load->view('locations/state_view', $data);
        }
    }

    //state edit
    public function edit_state()
    {
        $value=@storm_decode($this->uri->segment(2));
        if($value=='')
        {
            redirect(SITE_URL.'state');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit State";
        $data['nestedView']['cur_page'] = 'check_location';
        $data['nestedView']['parent_page'] = 'state_master';
        $data['parent_page'] = $data['nestedView']['parent_page'];
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $zone_id = $this->Common_model->get_value('location',array('location_id'=>$value),'parent_id');
        $country_id = $this->Common_model->get_value('location',array('location_id'=>$zone_id),'parent_id');
        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit State';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage State','class'=>'','url'=>SITE_URL.'state');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit State','class'=>'active','url'=>'');
            $where = array('location_id' => $value);
            $data['zoneList'] = $this->Location_m->get_zone_by_country(0,$country_id);
            $data['state_edit'] = $this->Common_model->get_data('location', $where);
            $data['country_id'] = $country_id;
            $data['level_id'] = $data['state_edit'][0]['level_id'];
            $data['flg'] = 2;
            $data['val'] = 1;

            # Load page with all shop details
            $this->load->view('locations/state_view', $data);
        }
    }

    //deactivate state
    public function deactivate_state($encoded_id)
    {
        $task_access = page_access_check('state_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'state');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> State has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'state');

    }
    //activate state
    public function activate_state($encoded_id)
    {
        $task_access = page_access_check('state_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'state');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> State has been Activated successfully!
        </div>');
        redirect(SITE_URL.'state');
    } 
    //check state name   
    public  function check_state()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $parent_id = validate_number($this->input->post('parent_id',TRUE));
        $location_id = validate_number($this->input->post('location_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('name'=>$name,'parent_id'=>$parent_id,'location_id'=>$location_id,'country_id'=>$country_id);
        $result = $this->Location_m->check_state($data);
        echo $result;
    }

    // checking short name for state name 
    public function is_shortNameExist()
    {
        $short_name = validate_string($this->input->post('short_name',TRUE));
        $parent_id = validate_number($this->input->post('parent_id',TRUE));
        $location_id = validate_number($this->input->post('location_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('short_name'=>$short_name,'parent_id'=>$parent_id,'location_id'=>$location_id,'country_id'=>$country_id);
        $result = $this->Location_m->is_shortNameExist($data);
        echo $result;
    }

    //City View
    public function city()
    {
        $data['nestedView']['heading']="Manage City";
        $data['nestedView']['cur_page'] = 'city_master';
        $data['nestedView']['parent_page'] = 'city_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['task_access'] = $task_access;

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Manage City';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage City','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('search_city', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'city_name'  => validate_string(@$this->input->post('city_name', TRUE)),
            'state_id'   => validate_number(@$this->input->post('state_id', TRUE)),
            'zone_id'    => validate_number(@$this->input->post('zone_id', TRUE)),
            'country_id' => validate_number(@$this->input->post('country_id', TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
            'city_name' => $this->session->userdata('city_name'),
            'state_id'  => $this->session->userdata('state_id'),
            'zone_id'   => $this->session->userdata('zone_id'),
            'country_id'=> $this->session->userdata('country_id'));
            }
            else {
                $searchParams=array(
                'city_name' => '',
                'state_id'  => '',
                'zone_id'   => '',
                'country_id'=> ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        //print_r($searchParams); exit();
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'city/';
        # Total Records
        $config['total_rows'] = $this->Location_m->city_total_num_rows($searchParams,$task_access);
        //echo $config['total_rows']; exit();

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;

        # Loading the data array to send to View
        $data['city_results'] = $this->Location_m->city_results($current_offset, $config['per_page'], $searchParams,$task_access);
         /* pagination end */
        
        # Additional data
        $data['displayResults'] = 1;
        $data['countryList'] = $this->Common_model->get_data('location',array('level_id'=>2,'status'=>1,'task_access'=>$task_access));
        $data['zoneList'] = $this->Location_m->get_zone_by_country($task_access);
        $data['stateList'] = $this->Location_m->get_state_by_country($task_access);

        $this->load->view('locations/city_view',$data);
    }
    //add city
    public function add_city()
    {
        $data['nestedView']['heading']="Manage Add New";
        $data['nestedView']['cur_page'] = 'check_location';
        $data['nestedView']['parent_page'] = 'city_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['parent_page'] = $data['nestedView']['parent_page'];
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Select Country";
                $data['nestedView']['cur_page'] = 'check_location';
                $data['nestedView']['parent_page'] = 'city_master';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage City','class'=>'','url'=>SITE_URL.'city');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_city';
                $data['cancel'] = SITE_URL.'city';
                $this->load->view('user/intermediate_page',$data); 
            }
        }

        if(@$country_id != '')
        {

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New City';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage City','class'=>'','url'=>SITE_URL.'city');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New City','class'=>'active','url'=>'');
            $data['stateList'] = $this->Location_m->get_state_by_country(0,$country_id);
            $data['level_id'] = 5;
            $data['flg'] = 1;
            $data['val'] = 0;
            $data['country_id'] = $country_id;
            # Load page with all shop details
            $this->load->view('locations/city_view', $data);
        }
    }
    //edit city
    public function edit_city()
    {
        $value=@storm_decode($this->uri->segment(2));
        if($value=='')
        {
            redirect(SITE_URL.'city');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit City";
        $data['nestedView']['cur_page'] = 'check_location';
        $data['nestedView']['parent_page'] = 'city_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['parent_page'] = $data['nestedView']['parent_page'];
        $state_id = $this->Common_model->get_value('location',array('location_id'=>$value),'parent_id');
        $zone_id = $this->Common_model->get_value('location',array('location_id'=>$state_id),'parent_id');
        $country_id = $this->Common_model->get_value('location',array('location_id'=>$zone_id),'parent_id');
        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit City';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage City','class'=>'','url'=>SITE_URL.'city');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit City','class'=>'active','url'=>'');
            $where = array('location_id' => $value);
            $data['stateList'] = $this->Location_m->get_state_by_country(0,$country_id);
            $data['city_edit'] = $this->Common_model->get_data('location', $where);
            $data['level_id'] = $data['city_edit'][0]['level_id'];
            $data['flg'] = 2;
            $data['val'] = 1;
            $data['country_id'] = $country_id;

            $this->load->view('locations/city_view', $data);
        }
    }
    //deactivate city
    public function deactivate_city($encoded_id)
    {
        $task_access = page_access_check('city_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'city');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> City has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'city');

    }
    //activate city
    public function activate_city($encoded_id)
    {
        $task_access = page_access_check('city_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'city');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Success!</strong> City has been Activated successfully!
                                     </div>');
        redirect(SITE_URL.'city');
    }   
    //check city name 
    public  function check_city()
    {

        $name = validate_string($this->input->post('name',TRUE));
        $parent_id = validate_number($this->input->post('parent_id',TRUE));
        $location_id = validate_number($this->input->post('location_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('name'=>$name,'parent_id'=>$parent_id,'location_id'=>$location_id,'country_id'=>$country_id);
        $result = $this->Location_m->check_city($data);
        echo $result;
    }

    //area view
    public function area()
    {
        $data['nestedView']['heading']="Manage Area";
        $data['nestedView']['cur_page'] = 'area_master';
        $data['nestedView']['parent_page'] = 'area_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['task_access'] = $task_access;

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Manage Area';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Area','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('search_area', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'area_name'  => validate_string($this->input->post('area_name', TRUE)),
            'city_id'    => validate_number($this->input->post('city_id', TRUE)),
            'state_id'   => validate_number(@$this->input->post('state_id', TRUE)),
            'zone_id'    => validate_number(@$this->input->post('zone_id', TRUE)),
            'country_id' => validate_number(@$this->input->post('country_id', TRUE)
                               ));
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'area_name' => $this->session->userdata('area_name'),
                'city_id'   => $this->session->userdata('city_id'),
                'state_id'  => $this->session->userdata('state_id'),
                'zone_id'   => $this->session->userdata('zone_id'),
                'country_id'=> $this->session->userdata('country_id'));
            }
            else 
            {
                $searchParams=array(
                'area_name' => '',
                'city_id'   => '',
                'state_id'  => '',
                'zone_id'   => '',
                'country_id'=> '');
                $this->session->set_userdata($searchParams);
            }
            
        }
        //print_r($searchParams); exit();
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'area/';
        # Total Records
        $config['total_rows'] = $this->Location_m->area_total_num_rows($searchParams,$task_access);
        //echo $config['total_rows']; exit();

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        
        # Loading the data array to send to View
        $data['area_results'] = $this->Location_m->area_results($current_offset, $config['per_page'], $searchParams,$task_access);
        /* pagination end */

        # Additional data
        $data['displayResults'] = 1;
        $data['countryList'] = $this->Common_model->get_data('location',array('level_id'=>2,'status'=>1,'task_access'=>$task_access));
        $data['zoneList'] = $this->Location_m->get_zone_by_country($task_access);
        $data['stateList'] = $this->Location_m->get_state_by_country($task_access);
        $data['cityList'] = $this->Location_m->get_city_by_country($task_access);

        $this->load->view('locations/area_view',$data);
    }
    //add area
    public function add_area()
    {
        $data['nestedView']['heading']="Add New Area";
        $data['nestedView']['cur_page'] = 'check_location';
        $data['nestedView']['parent_page'] = 'area_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['parent_page'] = $data['nestedView']['parent_page'];
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Select Country";
                $data['nestedView']['cur_page'] = 'check_location';
                $data['nestedView']['parent_page'] = 'area_master';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Area','class'=>'','url'=>SITE_URL.'area');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_area';
                $data['cancel'] = SITE_URL.'area';
                $this->load->view('user/intermediate_page',$data); 
            }
        }

        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New Area';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Area','class'=>'','url'=>SITE_URL.'area');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Area','class'=>'active','url'=>'');
            $data['cityList'] = $this->Location_m->get_city_by_country(0,$country_id);
            $data['country_id'] = $country_id;
            $data['level_id'] = 6;
            $data['flg'] = 1;
            $data['val'] = 0;

            # Load page with all shop details
            $this->load->view('locations/area_view', $data);
        }
    }
    //edit area
    public function edit_area()
    {
        $value=@storm_decode($this->uri->segment(2));
        if($value=='')
        {
            redirect(SITE_URL.'area');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Area";
        $data['nestedView']['cur_page'] = 'check_location';
        $data['nestedView']['parent_page'] = 'area_master';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $data['parent_page'] = $data['nestedView']['parent_page'];
        $city_id = $this->Common_model->get_value('location',array('location_id'=>$value),'parent_id');
        $state_id = $this->Common_model->get_value('location',array('location_id'=>$city_id),'parent_id');
        $zone_id = $this->Common_model->get_value('location',array('location_id'=>$state_id),'parent_id');
        $country_id = $this->Common_model->get_value('location',array('location_id'=>$zone_id),'parent_id');
        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/location.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit Area';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Area','class'=>'','url'=>SITE_URL.'area');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Area','class'=>'active','url'=>'');
            $where = array('location_id' => $value);
            $data['area_edit'] = $this->Common_model->get_data('location', $where);
            $data['parentInfo'] = getParentLocation($value);
            $data['country_id'] = $country_id;
            $data['cityList'] = $this->Location_m->get_city_by_country(0,$country_id);
            $data['level_id'] = $data['area_edit'][0]['level_id'];
            $data['flg'] = 2;
            $data['val'] = 1;

            # Load page with all shop details
            $this->load->view('locations/area_view', $data);
        }
    }
    //deactivate area
    public function deactivate_area($encoded_id)
    {
        $task_access = page_access_check('area_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'area');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Area has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'area');

    }
    //activate area
    public function activate_area($encoded_id)
    {
        $task_access = page_access_check('area_master');
        $location_id=@storm_decode($encoded_id);
        if($location_id==''){
            redirect(SITE_URL.'area');
            exit;
        }
        $where = array('location_id' => $location_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('location',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Area has been Activated successfully!
        </div>');
        redirect(SITE_URL.'area');
    }
    //check area name-uniqueness.
    public  function check_area()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $parent_id = validate_number($this->input->post('parent_id',TRUE));
        $location_id = validate_number($this->input->post('location_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('name'=>$name,'parent_id'=>$parent_id,'location_id'=>$location_id,'country_id'=>$country_id);
        $result = $this->Location_m->check_area($data);
        echo $result;
    }
}