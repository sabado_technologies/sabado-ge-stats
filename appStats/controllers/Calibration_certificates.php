<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
created_by : Srilekha
Date : 20-09-2017
*/
class Calibration_certificates extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Calibration_certificates_m');
	}

	public function calibration_certificates()
    {
        $data['nestedView']['heading']="Calibration Certificates";
		$data['nestedView']['cur_page'] = "calibration_certificates";
		$data['nestedView']['parent_page'] = 'calibration_certificates';

        #page Authencation
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Calibration Certificates';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Certificates','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchlist',TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'tool_no'      => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'    => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'whid'         => validate_number($this->input->post('wh_id',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'tool_no'      => $this->session->userdata('tool_no'),
                    'tool_desc'    => $this->session->userdata('tool_desc'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'whid'         => $this->session->userdata('whid'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no') 
                );
            }
            else 
            {
                $searchParams=array(
                    'tool_no'      => '',
                    'tool_desc'    => '',
                    'asset_number' => '',
                    'whid'         => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }

        $data['search_data'] = $searchParams;
		# Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'calibration_certificates/';
        # Total Records
        $config['total_rows'] = $this->Calibration_certificates_m->calibration_certificates_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['cal_list_Results'] = $this->Calibration_certificates_m->calibration_certificates_list_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['whList']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['flg']=1;
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;

		$this->load->view('calibration_certificates/view_calibration_certificates',$data);
	}
    
    public function view_calibration_certificates_list()
    {
        $asset_id= storm_decode(@$this->uri->segment(2));
        $page_enable = storm_decode(@$this->uri->segment(3));
        if($asset_id == '' || $page_enable == '')
        {
            redirect(SITE_URL.'calibration_certificates'); exit;
        }

        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View Calibration Certificates";
        if($page_enable == 1)
        {
            $data['nestedView']['cur_page'] = 'calibration_certificates';
            $data['nestedView']['parent_page'] = 'calibration_certificates';
        }
        else if($page_enable == 2)
        {
            $data['nestedView']['cur_page'] = 'fe_owned_tools';
            $data['nestedView']['parent_page'] = 'fe_owned_tools';
        }

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View Calibration Certificates';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        if($page_enable == 1)
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Certificates','class'=>'','url'=>SITE_URL.'calibration_certificates');
        }
        else if($page_enable == 2)
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'My Inventory','class'=>'','url'=>SITE_URL.'fe_owned_tools');
        }
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View Calibration Certificates','class'=>'active','url'=>'');

        $data['asset_row']=$this->Calibration_certificates_m->get_asset_data($asset_id);
        $asset_doc=$this->Calibration_certificates_m->get_asset_documents($asset_id);
        $asset_rc_doc=$this->Calibration_certificates_m->get_asset_rc_documents($asset_id);
        $asset_rca_doc=$this->Calibration_certificates_m->get_asset_rca_documents($asset_id);
        $asset_rc_asset_doc=array_merge($asset_doc,$asset_rc_doc);
        $asset_rc_rca_asset_doc=array_merge($asset_rc_asset_doc,$asset_rca_doc);
        $data['document'] = $asset_rc_rca_asset_doc; 
        $data['flg']=2;

        $data['page_enable'] = $page_enable;
        $this->load->view('calibration_certificates/view_calibration_certificates',$data);
    }
} 
?>