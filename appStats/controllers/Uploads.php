<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Upload extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
    }

    public function bulkAssetStatus()
    {

      $data['nestedView']['heading']="Asset Status Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'Asset Status Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Asset Status Bulk Upload','class'=>'active','url'=>'');
      
      #additional Data        
      $data['form_action'] = SITE_URL.'insertBulkAssetStatus';  
      $this->load->view('bulk/asset_status_upload_view',$data);
    } 

    public function insertBulkAssetStatus()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          
          if($level_id == '')
          {
            redirect(SITE_URL.'bulk_city'); exit();
          }
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              $insert_location_arr= array();
              #loop the records fetched from csv file
              while (($assetData = fgetcsv($file, 10000, ",")) !== FALSE)
              {
                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 

                  #assign data to variable as feteched individually
                  $asset_number = trim($assetData[0]);
                  $status  = trim($assetData[1]);
                  //echo '<pre>';print_r($assetData);exit;
                  if($asset_number!='' && $status !='' )
                  {
                    $asset_id = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number),'asset_id');
                    $updateData = array('status'    => $status);
                    $updateWhere = array('asset_id'=> $asset_id);
                    $this->Common_model->update_data('asset',$updateData,$updateWhere);
                  }
                  $j++;
              }
              //echo '<pre>';print_r($insert_location_arr);exit;
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'bulk_city');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong>Asset Status successfully Updated!
                    </div>');
                    redirect(SITE_URL.'bulk_city'); exit();
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'bulk_city'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulk_city'); exit();
      }
    }
}
