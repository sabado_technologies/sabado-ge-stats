<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Special_Cases extends Base_Controller
{
    public  function __construct() 
    {
        parent::__construct();      

    }

    public function unlock_transit_asset()
    {
        $asset_id = storm_decode($this->uri->segment(2));
        if(is_numeric($asset_id) && $asset_id!='' && isset($asset_id))
        {
            $this->db->trans_begin();

            $wh_id = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'wh_id');

            $update_ass_status_his = array('end_time' => date('Y-m-d H:i:s'));
            $update_asset_where = array('asset_id' => $asset_id,'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$update_ass_status_his,$update_asset_where);

            $insert_ash = array('asset_id' => $asset_id,
                                'status'   => 1,
                                'created_time' => date('Y-m-d H:i:s'),
                                'created_by'   => $this->session->userdata('sso_id'));
            $this->Common_model->insert_data('asset_status_history',$insert_ash);

            $update_ap = array('to_date' => date('Y-m-d H:i:s'));
            $update_ap_where = array('asset_id'=>$asset_id,'to_date'=>NULL);
            $this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);

            $insert_ap = array(
                           'wh_id'    => $wh_id,
                           'asset_id' => $asset_id,
                           'transit'  => 0,
                           'from_date'=> date('Y-m-d H:i:s'));
            $this->Common_model->insert_data('asset_position',$insert_ap);

            $this->Common_model->update_data('asset',array('status'=>1,'approval_status'=>0),array('asset_id'=>$asset_id));
            
            $this->db->trans_commit(); 
            echo 'transaction success'       ;exit;
        }
        else
        {
            echo 'invalid decoded value of asset id'.$this->uri->segment(2);exit;
        }
    }
    
    public function update_asset_position()
    {
        $asset_position_id = $this->uri->segment(2);  
        $update_type = $this->uri->segment(3);   // 1 for warehouser // 2 for SSO ID
        $update_value =  $this->uri->segment(4);   // 1 for 0(at) // 2 for 1(transit)
        $sso_id = $this->uri->segment(5);   // SSO ID Value
        
        if($asset_position_id=='' || $update_type =='' || $update_value =='')
        {
            redirect(SITE_URL);
        }
        if(is_numeric($asset_position_id) && isset($asset_position_id) && is_numeric($update_type) && isset($update_type) && is_numeric($update_value) && isset($update_value) )
        {
            $this->db->trans_begin();
            // modified by maurthi on 18thSep'18
            $asset_id = $this->Common_model->get_value('asset_position',array('asset_position_id'=>$asset_position_id),'asset_id');
            $wh_id = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'wh_id');
            if($update_type == 1)
            {
                if($wh_id!='' && isset($wh_id))
                {
                    $update_ass_status_his = array('end_time' => date('Y-m-d H:i:s'));
                    $update_asset_where = array('asset_id' => $asset_id,'end_time'=>NULL);
                    $this->Common_model->update_data('asset_status_history',$update_ass_status_his,$update_asset_where);

                    $insert_ash = array('asset_id' => $asset_id,
                                        'status'   => 1,
                                        'created_time' => date('Y-m-d H:i:s'),
                                        'created_by'   => $this->session->userdata('sso_id'));
                    $this->Common_model->insert_data('asset_status_history',$insert_ash);

                    $update_ap = array('to_date' => date('Y-m-d H:i:s'));
                    $update_ap_where = array('asset_id'=>$asset_id,'to_date'=>NULL);
                    $this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);

                    $insert_ap = array(
                                   'wh_id'    => $wh_id,
                                   'asset_id' => $asset_id,
                                   'transit'  => 0,
                                   'from_date'=> date('Y-m-d H:i:s'));
                    $this->Common_model->insert_data('asset_position',$insert_ap);

                    $this->Common_model->update_data('asset',array('status'=>1,'approval_status'=>0),array('asset_id'=>$asset_id));
                }
                else
                {
                    echo 'Wh id not available';exit;
                }
            }
            else
            {    
                $db_sso_id = $this->Common_model->get_value('asset_position',array('asset_position_id'=>$asset_position_id),'sso_id')   ;
                $transit = ($update_value == 1)?0:1; 
                if($sso_id=='')
                {
                    $final_sso_id =$db_sso_id;
                }
                else
                {
                     $final_sso_id =$sso_id;                             
                }
                //$wh_id = ($update_value ==1)?NULL:$wh_id;
                $update_ap = array('transit' => $transit,'wh_id'=>NULL,'sso_id'=>$final_sso_id);
                $update_ap_where = array('asset_position_id'=>$asset_position_id);
                $this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);             
            }  
            
            $this->db->trans_commit();       
            echo $this->db->last_query();
        }
    }

    public function get_data()
    {
        $table_name   = $this->uri->segment(2);  // table name       
        $columns_arr = ($this->uri->segment(3)=='')?'':explode('~',$this->uri->segment(3)); // columns
        $values_arr = ($this->uri->segment(4)=='')?'':explode('~',$this->uri->segment(4)); // values        
        if( is_array($columns_arr) && is_array($values_arr) )
        {            
            $where_arr = array();
            foreach ($columns_arr as $key => $value) {
                if($value!='' && $values_arr[$key]!='')
                    $where_arr["$value"] = $values_arr[$key];
            }
            if(count($columns_arr) ==count($values_arr))
            {
                $results = $this->Common_model->get_data("$table_name",$where_arr);  
                echo $this->db->last_query();          
            }
            else
            {
                echo 'Key values are not paired.Please check<br>';
                echo $table_name.'<br>';
                echo '<pre>';print_r($columns_arr);
                echo '<pre>';print_r($values_arr);exit;
            }
        }
        else
        {
            $results = $this->Common_model->get_data("$table_name");
        }        
        echo '<pre>';print_r($results);exit;
    }

    public function do_action()
    {
        $commit_value   = $this->uri->segment(2); // 1-commit 2- check 
        $table_name   = $this->uri->segment(3); // table name       
        $action_type   = $this->uri->segment(4);//1-insert 2-update 3-insert& update 4 -update & insert 5-delete
        $columns_arr = ($this->uri->segment(5)=='')?'':explode('~',$this->uri->segment(5)); // columns
        $values_arr = ($this->uri->segment(6)=='')?'':explode('~',$this->uri->segment(6)); // values        
        $columns_arr2 = ($this->uri->segment(7)=='')?'':explode('~',$this->uri->segment(7)); // columns
        $values_arr2 = ($this->uri->segment(8)=='')?'':explode('~',$this->uri->segment(8)); // values 
        $this->db->trans_begin();
        if(isset($table_name) && $table_name!='' && $action_type!='' && isset($action_type) && is_array($columns_arr) && is_array($values_arr) )
        {            
            $data = array();
            foreach ($columns_arr as $key => $value) {
                if($value!='' && $values_arr[$key]!='')
                    $data["$value"] = ($value =='modified_time')?date('Y-m-d H:i:s'):$values_arr[$key];
            }

            if(count($data)>0)
            {
                if($action_type==1)
                {
                    $last_inserted_id = $this->Common_model->insert_data("$table_name",$data);
                    echo $last_inserted_id.'<br>';
                    echo $this->db->last_query();
                    if($commit_value == 1)
                        $this->db->trans_commit();
                    else
                        $this->db->trans_rollback();
                }
                if($action_type == 2)
                {
                    $data_where = array();
                    foreach ($columns_arr2 as $key => $value) {
                        if($value!='' && $values_arr2[$key]!='')
                            $data_where["$value"] = $values_arr2[$key];
                    }
                    // print_r($data).'<br>';
                    // print_r($data_where);exit;
                    if(count($data_where)>0)
                    {
                        $this->Common_model->update_data("$table_name",$data,$data_where);
                        echo $this->db->last_query().'<br>';
                        $res = $this->Common_model->get_data("$table_name",$data_where);
                        echo '<pre>';print_r($res);
                        echo $this->db->last_query();
                        if($commit_value == 1)
                            $this->db->trans_commit();
                        else
                            $this->db->trans_rollback();

                    }
                    else
                    {
                        echo 'Key values are not paired At Update Data.Please check<br>';
                        echo $table_name.'<br>';
                        echo '<pre>';print_r($columns_arr);
                        echo '<pre>';print_r($values_arr);
                        echo '<pre>';print_r($columns_arr2);
                        echo '<pre>';print_r($values_arr2);exit;                          
                    }
                }
            }
            else
            {
                echo 'Key values are not paired.Please check<br>';
                echo $table_name.'--'.$action_type.'--'.$columns_arr.'--'.$values_arr;exit;
            }
        }   
        else
        {
            echo 'Some Values are missing in the parameters.Please check<br>';
            echo $table_name.'--'.$action_type.'--'.$columns_arr.'--'.$values_arr;exit;
        }  
    }

    public function get_encoded_value()
    {
        echo $this->uri->segment(2).'<br>';
        echo storm_encode($this->uri->segment(2));exit;
    }

    public function get_decoded_value()
    {
        echo $this->uri->segment(2).'<br>';
        echo storm_decode($this->uri->segment(2)); exit;
    }

    /*koushik*/
    public function run_scheduler()
    {
        $task = validate_number($this->uri->segment(2));
        $countryId = validate_number($this->uri->segment(3));
        if(@$countryId =='')
            $countryId = 0;
        if(!isset($task))
        {
            $this->session->set_flashdata('response','<div class=" col-md-offset-1 col-md-10 alert alert-success alert-white rounded" style="margin-top:10px;">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Wrong segment! 
            </div>');
            redirect(SITE_URL);
        }
        if($task == 1)
        {
            insert_calibration($countryId);
        }
        else if($task == 2)
        {
            calibration_mail($countryId);
        }
        else if($task == 3)
        {
            fe_crossed_expected_delivery_date();
        }
        else if($task == 4)
        {
            deleteUploadData();
        }
        else if($task == 5)
        {
            get_unacknowledged_tools($countryId);   
        }
        else if($task == 6)
        {
            get_delayed_return_tools($countryId);   
        }
        
        redirect(SITE_URL.'home');
    }

    public function rollback_asset_status()
    {
        $asset_list = $this->Common_model->get_data('asset',array('status'=>1));
        $list = array();
        $this->db->trans_begin();
        foreach ($asset_list as $key => $value) 
        {
            $update_status = ''; 
            $asset_id = $value['asset_id'];
            $current_stage_id = check_for_rc_status_entry($asset_id);//calibration asset
            if($current_stage_id >0)
            {
                if($current_stage_id == 11) // 11
                {
                    if($value['cal_due_date']<date('Y-m-d'))
                    {
                        $update_status = 12;//out of cal
                    }
                    else
                    {
                        $oa_asset_id = get_for_scanned_assets($asset_id);
                        if($oa_asset_id >0)
                        {
                            $update_status = 8;
                        }
                        else
                        {
                            $arr = $this->Common_model->get_data_row('asset_position',array('to_date'=>NULL,'asset_id'=>$asset_id));
                            $wh_id=$arr['wh_id'];
                            $transit=$arr['transit'];
                            $sso_id=$arr['sso_id'];
                            if($transit == 0)
                            {
                                if($wh_id !='')
                                {
                                    $ash = get_ash($asset_id);
                                    if($ash>0)
                                    {
                                        $update_status = 2;//lock in period
                                    }
                                    else
                                    {
                                        $update_status = 1;//at wh
                                    }
                                }
                                else if($sso_id !='')
                                {
                                    $update_status = 3;//field deployed
                                }
                            }
                            else if ($transit == 1)
                            {
                                if($wh_id!='')
                                {
                                    $update_status = 8;//in transit
                                }
                                else if($sso_id !='')
                                {
                                    $update_status = 8;//in transit
                                }
                                else if($wh_id=='' && $sso_id=='')
                                {
                                    $update_status = 7;//lost
                                }
                            }
                        }
                    }
                }   
                else
                {
                    if($value['cal_due_date']>date('Y-m-d'))
                    {
                        $update_status = 4;//in calibration
                    }
                    else
                    {
                        $update_status = 12;//out of cal
                    }
                }
                
            }
            
            if($update_status == '' || $update_status == 12)//check for repair
            {
                $rr_check = check_for_rr_status_entry($asset_id);
                if($rr_check >0)
                {
                    $update_status = 10;
                }
            }

            if($update_status == '')//field_deployed
            {
                $oa_asset_id = get_for_scanned_assets($asset_id);
                if($oa_asset_id >0)
                {
                    $update_status = 8;
                }
                else
                {
                    $arr = $this->Common_model->get_data_row('asset_position',array('to_date'=>NULL,'asset_id'=>$asset_id));
                    $wh_id=$arr['wh_id'];
                    $transit=$arr['transit'];
                    $sso_id=$arr['sso_id'];

                    if($transit == 0)
                    {
                        if($wh_id !='')
                        {

                            $ash = get_ash($asset_id);
                            if($ash>0)
                            {
                                $update_status = 2;
                            }
                            else
                            {
                                $update_status = 1;
                            }
                        }
                        else if($sso_id !='')
                        {
                            $update_status = 3;
                        }
                    }
                    else if ($transit == 1)
                    {
                        if($wh_id!='')
                        {
                            $update_status = 8;
                        }
                        else if($sso_id !='')
                        {
                            $update_status = 8;
                        }
                        else if($wh_id=='' && $sso_id=='')
                        {
                            $update_status = 7;
                        }
                    }
                }

            }
            if($update_status =='')//cannot decide
            {
                $list[$asset_id] = '952';
                $update_a = 
                array('remarks2'      => '952',
                      'modified_by'   => $this->session->userdata('sso_id'),
                      'modified_time' => date('Y-m-d H:i:s'));
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);
            }
            else if($update_status!=1)
            {
                $list[$asset_id] = $update_status;
                $update_a = array(
                    'status'        => $update_status,
                    'modified_by'   => $this->session->userdata('sso_id'),
                    'modified_time' => date('Y-m-d H:i:s'));
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);
            }

            if($update_status == 1)
            {
                $list[$asset_id] = 'Status is already 1';
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit(); 
        } 
        echo "<pre>"; print_r($list); exit();
        /*end of for loop*/
    }

    public function rollback_asset_status_with_segment()
    {
        $from = validate_number($this->uri->segment(3));
        if(!isset($from))
        {
            $this->session->set_flashdata('response','<div class=" col-md-offset-1 col-md-10 alert alert-success alert-white rounded" style="margin-top:10px;">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>error!</strong> enter current_offset! 
            </div>');
            redirect(SITE_URL);
        }
        $asset_list = get_asset_listwith_1($from);
        $list = array();
        $this->db->trans_begin();
        foreach ($asset_list as $key => $value) 
        {
            $update_status = ''; 
            $asset_id = $value['asset_id'];
            $current_stage_id = check_for_rc_status_entry($asset_id);//calibration asset
            if($current_stage_id >0)
            {
                if($current_stage_id == 11) // 11
                {
                    if($value['cal_due_date']<date('Y-m-d'))
                    {
                        $update_status = 12;//out of cal
                    }
                    else
                    {
                        $oa_asset_id = get_for_scanned_assets($asset_id);
                        if($oa_asset_id >0)
                        {
                            $update_status = 8;
                        }
                        else
                        {
                            $arr = $this->Common_model->get_data_row('asset_position',array('to_date'=>NULL,'asset_id'=>$asset_id));
                            $wh_id=$arr['wh_id'];
                            $transit=$arr['transit'];
                            $sso_id=$arr['sso_id'];
                            if($transit == 0)
                            {
                                if($wh_id !='')
                                {
                                    $ash = get_ash($asset_id);
                                    if($ash>0)
                                    {
                                        $update_status = 2;//lock in period
                                    }
                                    else
                                    {
                                        $update_status = 1;//at wh
                                    }
                                }
                                else if($sso_id !='')
                                {
                                    $update_status = 3;//field deployed
                                }
                            }
                            else if ($transit == 1)
                            {
                                if($wh_id!='')
                                {
                                    $update_status = 8;//in transit
                                }
                                else if($sso_id !='')
                                {
                                    $update_status = 8;//in transit
                                }
                                else if($wh_id=='' && $sso_id=='')
                                {
                                    $update_status = 7;//lost
                                }
                            }
                        }
                    }
                }   
                else
                {
                    if($value['cal_due_date']>date('Y-m-d'))
                    {
                        $update_status = 4;//in calibration
                    }
                    else
                    {
                        $update_status = 12;//out of cal
                    }
                }
                
            }
            
            if($update_status == '' || $update_status == 12)//check for repair
            {
                $rr_check = check_for_rr_status_entry($asset_id);
                if($rr_check >0)
                {
                    $update_status = 10;
                }
            }

            if($update_status == '')//field_deployed
            {
                $oa_asset_id = get_for_scanned_assets($asset_id);
                if($oa_asset_id >0)
                {
                    $update_status = 8;
                }
                else
                {
                    $arr = $this->Common_model->get_data_row('asset_position',array('to_date'=>NULL,'asset_id'=>$asset_id));
                    $wh_id=$arr['wh_id'];
                    $transit=$arr['transit'];
                    $sso_id=$arr['sso_id'];

                    if($transit == 0)
                    {
                        if($wh_id !='')
                        {

                            $ash = get_ash($asset_id);
                            if($ash>0)
                            {
                                $update_status = 2;
                            }
                            else
                            {
                                $update_status = 1;
                            }
                        }
                        else if($sso_id !='')
                        {
                            $update_status = 3;
                        }
                    }
                    else if ($transit == 1)
                    {
                        if($wh_id!='')
                        {
                            $update_status = 8;
                        }
                        else if($sso_id !='')
                        {
                            $update_status = 8;
                        }
                        else if($wh_id=='' && $sso_id=='')
                        {
                            $update_status = 7;
                        }
                    }
                }

            }

            if($update_status =='')//cannot decide
            {
                $list[$asset_id] = '952';
                $update_a = 
                array('remarks2'      => '952',
                      'modified_by'   => $this->session->userdata('sso_id'),
                      'modified_time' => date('Y-m-d H:i:s'));
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);
            }
            else if($update_status!=1)
            {
                $list[$asset_id] = $update_status;
                $update_a = array(
                    'status'        => $update_status,
                    'modified_by'   => $this->session->userdata('sso_id'),
                    'modified_time' => date('Y-m-d H:i:s'));
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);
            }

            if($update_status == 1)
            {
                $list[$asset_id] = 'Status is already 1';
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit(); 
        }  
        echo "previous offset:".$from.'<br>';
        echo "<pre>"; print_r($list); exit();
        /*end of for loop*/
    }
    /*end of function*/

    public function update_avail_status()
    {
        $asset_arr = $this->Common_model->get_data('asset',array());
        $list = array();
        foreach ($asset_arr as $key => $value) 
        {
            $asset_id = $value['asset_id'];
            $old_avail_status = $value['availability_status'];
            $avail_status = $this->Common_model->get_value('asset_status',array('asset_status_id'=>$value['status']),'type');
            if($avail_status!=$old_avail_status)
            {
                $list[$asset_id] = $avail_status;
                $update_a = array(
                    'availability_status' => $avail_status,
                    'modified_by'   => $this->session->userdata('sso_id'),
                    'modified_time' => date('Y-m-d H:i:s'));
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);
            }
            else
            {
                $list[$asset_id] = $old_avail_status;
            }
        }
        echo "<pre>"; print_r($list); exit();
        /*end of for loop*/
    }
    /*end of function*/

    public function update_actual_asset_position()
    {
        $asset_arr = get_assets_with1_2();
        $list = array();
        if(count($asset_arr)>0)
        {
            foreach ($asset_arr as $key => $value) 
            {
                $wh_id    = $value['wh_id'];
                $ap_wh_id = $value['ap_wh_id'];
                $asset_id = $value['asset_id'];
                if($wh_id != $ap_wh_id)
                {
                    $list[$asset_id] = array('asset_id'=>$asset_id,'wh_id'=>$wh_id,'ap_wh_id'=>$ap_wh_id);
                    $update_ap = array('to_date' => date('Y-m-d H:i:s'));
                    $update_w = array('asset_id'=>$asset_id, 'to_date'=>NULL);
                    $this->Common_model->update_data('asset_position',$update_ap,$update_w);

                    $insert_ap = array(
                        'asset_id'  => $asset_id,
                        'transit'   => 0,
                        'wh_id'     => $wh_id,
                        'from_date' => date('Y-m-d H:i:s'),
                        'status'    => 1
                    );
                    $this->Common_model->insert_data('asset_position',$insert_ap);
                }
            }
        }
        echo '<pre>'; print_r($list); echo "Loop Ended"; exit();
    }
    /*end of function*/

    public function unlink_scanned_assets_in_wh()
    {
        $list = array();
        $this->db->trans_begin();

        #for calibration shipment
        $this->load->model('Calibration_m');
        $rc_asset_health_arr = $this->Calibration_m->get_scanned_asset_list();
        foreach ($rc_asset_health_arr as $key => $value) 
        {
            #delete rc asset health
            $this->Common_model->delete_data('rc_asset_health',array('rcah_id'=>$value['rcah_id']));
            #delete rc asset history
            $this->Common_model->delete_data('rc_asset_history',array('rcah_id'=>$value['rcah_id']));
            $list['calibration_shipment'][] = array('CRB No:'=>$value['rcb_number']);
        }

        #for repair shipment
        $this->load->model('Repair_m');
        $rc_asset_health_arr = $this->Repair_m->get_scanned_asset_list();
        foreach ($rc_asset_health_arr as $key => $value) 
        {
            #delete rc asset health
            $this->Common_model->delete_data('rc_asset_health',array('rcah_id'=>$value['rcah_id']));
            #delete rc asset history
            $this->Common_model->delete_data('rc_asset_history',array('rcah_id'=>$value['rcah_id']));
            $list['repair_shipment'][] = array('RRB No:'=>$value['rcb_number']);
        }

        #for FE Shipment
        $this->load->model('Fe_delivery_m');
        $oa_arr = $this->Fe_delivery_m->get_for_scanned_assets();
        foreach ($oa_arr as $key => $value) 
        {
            $tool_order_id = $value['tool_order_id'];

            $oah_id = $this->Common_model->get_value('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']),'oah_id');

            #delete from order_asset_health
            $this->Common_model->delete_data('order_asset_health',array('oah_id'=>$oah_id));

            #delete from order_asset_history
            $this->Common_model->delete_data('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']));

            #delete from ordered_asset
            $this->Common_model->delete_data('ordered_asset',array('ordered_asset_id'=>$value['ordered_asset_id']));

            #delete from asset status history
            $this->Common_model->delete_data('asset_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>5,'asset_id'=>$value['asset_id']));

            #update asset status history
            $update_data3 = array(
                'end_time'    => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('sso_id')
            );
            $update_where3 = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$update_data3,$update_where3);

            #update asset
            $update_a = array(
                'status'        => 1,
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $update_a_where = array('asset_id'=>$value['asset_id']);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            $insert_data5 = array(
                'asset_id'        => $value['asset_id'],
                'created_by'      => $this->session->userdata('sso_id'),
                'created_time'    => date('Y-m-d H:i:s'),
                'status'          => 1
            );
            $this->Common_model->insert_data('asset_status_history',$insert_data5);
            $list['fe_shipment'][] = array('Tool Order No:'=>$value['order_number']);
        }

        #ST Shipment
        $this->load->model('Wh_stock_transfer_m');
        $oa_arr = $this->Wh_stock_transfer_m->get_for_scanned_assets();
        foreach ($oa_arr as $key => $value) 
        {
            $tool_order_id = $value['tool_order_id'];
            $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
            if($check_st=='') //stock transfer
            {
                $oah_id = $this->Common_model->get_value('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']),'oah_id');

                #delete from order_asset_health
                $this->Common_model->delete_data('order_asset_health',array('oah_id'=>$oah_id));

                #delete from order_asset_history
                $this->Common_model->delete_data('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']));

                $current_status = $this->Common_model->get_value('asset',array('asset_id'=>$value['asset_id']),'status');
                if($current_status == 8)
                {
                    #delete from asset status history
                    $this->Common_model->delete_data('asset_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>1,'asset_id'=>$value['asset_id']));

                    #update asset status history
                    $update_data3 = array(
                        'end_time'    => date('Y-m-d H:i:s'),
                        'modified_by' => $this->session->userdata('sso_id')
                    );
                    $update_where3 = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
                    $this->Common_model->update_data('asset_status_history',$update_data3,$update_where3);

                    #update asset
                    $update_a = array(
                        'status'        => 1,
                        'modified_by'   => $this->session->userdata('sso_id'),
                        'modified_time' => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$value['asset_id']);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    $insert_data5 = array(
                        'asset_id'     => $value['asset_id'],
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'status'       => 1
                    );
                    $this->Common_model->insert_data('asset_status_history',$insert_data5);
                }
            } 
            else//tool order
            {
                $oah_id = $this->Common_model->get_value('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']),'oah_id');

                #delete from order_asset_health
                $this->Common_model->delete_data('order_asset_health',array('oah_id'=>$oah_id));

                #delete from order_asset_history
                $this->Common_model->delete_data('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']));

                #delete from ordered_asset
                $this->Common_model->delete_data('ordered_asset',array('ordered_asset_id'=>$value['ordered_asset_id']));

                #delete from asset status history
                $this->Common_model->delete_data('asset_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>1,'asset_id'=>$value['asset_id']));

                #update asset status history
                $update_data3 = array(
                    'end_time'    => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('sso_id')
                );
                $update_where3 = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$update_data3,$update_where3);

                #update asset
                $update_a = array(
                    'status'        => 1,
                    'modified_by'   => $this->session->userdata('sso_id'),
                    'modified_time' => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id'=>$value['asset_id']);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                $insert_data5 = array(
                    'asset_id'        => $value['asset_id'],
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s'),
                    'status'          => 1
                );
                $this->Common_model->insert_data('asset_status_history',$insert_data5);
            }
            $list['st_shipment'][] = array('ST Order No:'=>$value['stn_number']);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback(); 
        }
        else
        {
            $this->db->trans_commit();
        }
        echo '<pre>'; print_r($list); echo "Loop Ended"; exit();
    }
    /*end of function*/

    public function delete_ib_data()
    {
        $country_id = $this->uri->segment(3);
        if($country_id=='')
        {
            echo "country_id is required"; exit();
        }
        $str = "delete ib, csh, cs, c 
                FROM install_base ib
                INNER JOIN customer_site cs ON cs.customer_site_id = ib.customer_site_id
                INNER JOIN cs_history csh ON cs.customer_site_id = csh.customer_site_id 
                INNER JOIN customer c ON c.customer_id = cs.customer_id  
                WHERE cs.country_id = ".$country_id."
                AND ib.country_id = ".$country_id."
                AND c.country_id = ".$country_id."";
        $this->db->query($str);

        $country_name = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> For Country : <strong>'.$country_name.'</strong> IB Data is deleted successfully!</div>');
        redirect(SITE_URL.'home'); exit();
    }

    // created by maruthi on 24thMay'18
    public function addressChangeResults()
    {
        $task_access = validate_number($this->uri->segment(3));
        if($task_access ==1 || $task_access == 2)
        {
            $this->db->select('to.*,oa.*,count(to.tool_order_id)as count');
        }
        if($task_access==3)
        {
            $this->db->select('to.*,oa.*');
        }
        $this->db->from('tool_order to');               
        $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
        $this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
        $this->db->join('user u','u.sso_id = to.sso_id');
        $this->db->join('location l','l.location_id=to.country_id');        
        $this->db->where('to.current_stage_id',4);  
        $this->db->where('oa.check_address',1); 
        $this->db->where('oa.status',1);
        $this->db->where('to.order_delivery_type_id',1);
        if($task_access == 1)
        {
            $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        else if($task_access == 2)
        {
            $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('to.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
                $this->db->where_in('to.country_id',$this->session->userdata('countriesIndexedArray'));
            }
        } 
        if($task_access == 1)
        {
            $this->db->group_by('to.sso_id');
        }
        // else if($task_access ==2 || $task_access ==3)
        // {
        //     $this->db->group_by('to.country_id');
        // }
        $res1 = $this->db->get();
        $result1 = $res1->result_array();


        if($task_access ==1 || $task_access == 2)
        {
            $this->db->select('ro.*,count(ro.return_order_id) as count');
        }
        if($task_access==3)
        {
            $this->db->select('ro.*');
        }
        $this->db->from('return_order ro');             
        $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');        
        $this->db->join('user u','u.sso_id = ro.created_by');
        $this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
        $this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');        
        $this->db->where('ro.address_check',1);     
        $this->db->where('ro.order_delivery_type_id',1);
        $this->db->join('location l','l.location_id=ro.country_id');    
        if($task_access == 1)
        {
            $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
        }
        else if($task_access == 2)
        {
            $this->db->where('ro.country_id',$this->session->userdata('s_country_id'));
        }
        else if($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $this->db->where('ro.country_id',$this->session->userdata('header_country_id'));    
            }
            else
            {
               $this->db->where_in('ro.country_id',$this->session->userdata('countriesIndexedArray')); 
            }
        } 
        if($task_access == 1)
        {
            $this->db->group_by('to.sso_id');
        }   
        // if($task_access ==2 || $task_access ==3)
        // {
        //     $this->db->group_by('ro.country_id');
        // }
        $res2 = $this->db->get();
        $result2 = $res2->result_array();
        echo "<pre>";print_r($result1).'<br>';
        echo "<pre>";print_r($result2).'<br>';

        $final = array_merge($result1,$result2);
        echo "<pre>";print_r($final).'<br>';exit;
        if($task_access == 1 || $task_access == 2)
        {
            $final_arr = array();
            if(count($final)>0)
            {
                $count1 = (isset($result1[0]['count']))?$result1[0]['count']:0;
                $count2 = (isset($result2[0]['count']))?$result2[0]['count']:0;
                $count = $count1+$count2;
                if($count>0)
                {
                    $final_arr[] = array(
                        'count' => $count
                    );
                }
                echo "<pre>";print_r($final_arr);exit;
                return $final_arr;
            }
            else
            {
                return FALSE;
            }
            
        }
        else
        {
            if(count($final)>0)
            {
                $ids_arr = array();
                $location_id_arr = array();
                $location_id_arr_count = array();
                $return_arr = array();
                foreach ($final as $key => $value) {    

                    if(in_array($value['location_id'], $ids_arr))
                    {
                        $new_count = ($value['count'] + $location_id_arr_count[$value['location_id']]);
                        $location_id_arr[$value['location_id']] = $value['display_name'];
                        $location_id_arr_count[$value['location_id']] = $new_count;
                    }
                    else
                    {
                        $location_id_arr[$value['location_id']] = $value['display_name'];
                        $location_id_arr_count[$value['location_id']] = $value['count'];
                        $ids_arr[] = $value['location_id'];
                    }               
                }
                if(count($location_id_arr_count)>0)
                {
                    foreach ($location_id_arr_count as $key => $value) {
                        $return_arr[] = array(
                            'count'         => $value,
                            'display_name'  => $location_id_arr[$key]
                            );
                    }
                    echo "<pre>";print_r($final_arr);exit;
                    return $return_arr;
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                return FALSE;
            }
        }
    }

    public function addressChangeResultsCont()
    {
        $type =  validate_number($this->uri->segment(3));
        $task_access = 3;
        if($type == 1)
        {
            $this->db->select('to.*,to.order_number as final_order_number,odt.name as order_type,concat(u.sso_id, "-(" ,u.name,")") as sso,l.name as country');
            $this->db->from('tool_order to');               
            $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = to.order_delivery_type_id');
            $this->db->join('order_address oa','oa.tool_order_id = to.tool_order_id');
            $this->db->join('user u','u.sso_id = to.sso_id');
            $this->db->join('location l','l.location_id=to.country_id');
            if(@$searchParams['order_number']!='')
                $this->db->like('to.order_number',$searchParams['order_number']);
            if(@$searchParams['deploy_date']!='')
                $this->db->where('to.deploy_date>=',format_date($searchParams['deploy_date']));
            if(@$searchParams['return_date']!='')
                $this->db->where('to.return_date<=',format_date($searchParams['return_date']));
            if($task_access == 1 )
            {
                $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
            }
            else if($task_access == 2)
            {
                if(@$searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

                $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            }
            else if($task_access == 3)
            {
                
                if($_SESSION['header_country_id']!='')
                {
                    $this->db->where('to.country_id',$_SESSION['header_country_id']);   
                }
                else
                {
                    if(@$searchParams['country_id']!='')
                    {
                        $this->db->where('to.country_id',$searchParams['country_id']);
                    }
                    else
                    {
                        $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                    }
                    
                }
                if(@$searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

            }
            $this->db->where('to.current_stage_id',4);  
            $this->db->where('oa.check_address',1); 
            $this->db->where('oa.status',1);
            $this->db->where('to.order_delivery_type_id',1);
            $res1 = $this->db->get();
            $result = $res1->result_array();
            echo "<pre>";print_r($result);exit;
        }
        else
        {
            $this->db->select('ro.*,to.order_number as final_order_number,concat(u.sso_id, "-(" ,u.name,")") as ro_sso,odt.name as order_type,to.deploy_date,to.return_date,l.name as country');
            $this->db->from('return_order ro');             
            $this->db->join('order_delivery_type odt','odt.order_delivery_type_id = ro.order_delivery_type_id');
            $this->db->join('user u','u.sso_id = ro.created_by');
            $this->db->join('return_tool_order rto','rto.return_order_id = ro.return_order_id');
            $this->db->join('tool_order to','to.tool_order_id = rto.tool_order_id');
            $this->db->join('location l','l.location_id = to.country_id');
            if(@$searchParams['return_number']!='')
                $this->db->like('ro.return_number',$searchParams['return_number']);
            if(@$searchParams['order_number']!='')
                $this->db->like('to.order_number',$searchParams['order_number']);
            if($task_access == 1 )
            {
                $this->db->where('to.sso_id',$this->session->userdata('sso_id'));
            }
            else if($task_access == 2)
            {
                if(@$searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

                $this->db->where('to.country_id',$this->session->userdata('s_country_id'));
            }
            else if($task_access == 3)
            {
                
                if($_SESSION['header_country_id']!='')
                {
                    $this->db->where('to.country_id',$_SESSION['header_country_id']);   
                }
                else
                {
                    if(@$searchParams['country_id']!='')
                    {
                        $this->db->where('to.country_id',$searchParams['country_id']);
                    }
                    else
                    {
                        $this->db->where_in('to.country_id',$_SESSION['countriesIndexedArray']);    
                    }
                    
                }
                if(@$searchParams['a_sso_id']!='')
                {
                    $this->db->where('to.sso_id',$searchParams['a_sso_id']);
                }

            }
            
            $this->db->where('ro.address_check',1);     
            $this->db->where('ro.order_delivery_type_id',1);
            $this->db->where('ro.status',1);
            $res2 = $this->db->get();
            $result = $res2->result_array();            
            echo "<pre>";print_r($result);exit;
        }
        return $result;
    }

    public function update_asset_to_4()
    {
        $asset_list = get_asset_cal_12_16();
        foreach ($asset_list as $key => $value) 
        {
            $asset_id = $value['asset_id'];

            #update asset
            $update_a = array(
                'status'        => 4,
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $update_a_where = array('asset_id'=>$asset_id);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            #update old ash
            $update_ass_status_his = array('end_time' => date('Y-m-d H:i:s'));
            $update_asset_where = array('asset_id' => $asset_id,'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$update_ass_status_his,$update_asset_where);

            #insert ash
            $insert_ash = array(
                'asset_id'     => $asset_id,
                'status'       => 4,
                'created_time' => date('Y-m-d H:i:s'),
                'current_stage_id' => $value['current_stage_id'],
                'created_by'   => $this->session->userdata('sso_id')
            );
            $this->Common_model->insert_data('asset_status_history',$insert_ash);
        }
        echo "<pre>"; print_r($asset_list); exit();
    }

    public function get_information()
    {
        $data['nestedView']['heading']="Get Information";
        $data['nestedView']['cur_page'] = 'get_info';
        $data['nestedView']['parent_page'] = 'get_info';

        # include files
        $data['nestedView']['css_includes'] = array();

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Get Information';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Get Information','class'=>'active','url'=>'');

        # Additional data
        $this->load->view('user/get_information',$data);
    }

    public function get_information_data()
    {
        $query = $this->input->post('query',TRUE);
        $query_type = $this->input->post('query_type',TRUE);
        if($query != '' && $query_type != 0)
        {
            $qry = explode(';', $query);
            $res = $this->db->query($qry[0]);

            echo "Query : ".$query;
            echo "<br><br>";
            switch ($query_type) {
                case 1:
                    $results = $res->result_array();
                    $num_row = $res->num_rows();
                    echo 'Num of Rows Fetched : '.$num_row;
                    echo "<br><br>";
                    echo "Result Set : <pre>"; print_r($results);
                break;

                case 2:
                    echo "Primary Key: ".$this->db->insert_id();
                break;

                case 3:
                    echo "Affected Rows: ".$this->db->affected_rows();
                break;

                case 4:
                    echo "Records are deleted successfully";
                break;
            }
        }
        exit();
    }

    #update with actual status for out of calibration and CR is not available
    public function update_actual_ofc()
    {
        $asset_arr = $this->Common_model->get_data('asset',array('status'=>12));
        $list = array(); $not_list = array();
        foreach ($asset_arr as $key => $value) 
        {
            $asset_id = $value['asset_id'];
            $rc_check=check_for_rc_entry($asset_id);

            if($rc_check == 0)
            {
                $flag = get_position_for_calibration($asset_id);
                if($flag == 1)
                {
                    $new_status = 1;
                }
                else if($flag == 2)
                {
                    $new_status = 3;
                }
                else if($flag == 4)
                {
                    $new_status = 8;
                }
                else
                {
                    $new_status = '';
                }
                if($new_status !='')
                {
                    $list[] = $asset_id;
                    $update_a = array(
                        'status'              => $new_status,
                        'approval_status'     => 0,
                        'availability_status' => 1,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);
                }
                else
                {
                    $not_list[] = $asset_id;
                }
            }
        }
        echo "resolved:";
        echo "<pre>"; print_r($list);
        echo "<br><br>Not able to resolve:";
        echo "<pre>"; print_r($not_list);
        exit();
    }
    /*end of function*/

    public function updateLockinAtWhToInitial()
    {
        $this->db->trans_begin();
        $atWhLockinAssets = getAshLastRecord();
        $updatedAssets = array();
        foreach ($atWhLockinAssets as $key => $value) {
            $asset_id = $value['asset_id'];
            $wh_id = $value['wh_id'];
            # Update Asset Status history
            $update_ass_status_his = array('end_time' => date('Y-m-d H:i:s'));
            $update_asset_where = array('asset_id' => $asset_id,'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$update_ass_status_his,$update_asset_where);
            # Insert Asset Status History
            $insert_ash = array('asset_id' => $asset_id,
                                'status'   => 1,
                                'created_time' => date('Y-m-d H:i:s'),
                                'created_by'   => $this->session->userdata('sso_id'));
            $this->Common_model->insert_data('asset_status_history',$insert_ash);
            # Updated asset Position
            $update_ap = array('to_date' => date('Y-m-d H:i:s'));
            $update_ap_where = array('asset_id'=>$asset_id,'to_date'=>NULL);
            $this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);
            # Insert Asset Position
            $insert_ap = array(
                           'wh_id'    => $wh_id,
                           'asset_id' => $asset_id,
                           'transit'  => 0,
                           'from_date'=> date('Y-m-d H:i:s'));
            $this->Common_model->insert_data('asset_position',$insert_ap);
            #Update Asset
            $assetUpdateArray = array(
                'status'         => 1,
                'approval_status'=> 0,
                'modified_by'    => $this->session->userdata('sso_id'),
                'modified_time'  => date('Y-m-d H:i:s')
            );
            $this->Common_model->update_data('asset',$assetUpdateArray,array('asset_id'=>$asset_id));
                $updatedAssets[] = $asset_id;
            
        }
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.</div>');
        }
        else
        {
            $this->db->trans_commit();
            echo "<pre>";print_r($updatedAssets);exit;
        } 
    }

    public function updateLockinAtWhToInitialEndTime()
    {
        $this->db->trans_begin();
        $atWhLockinAssets = getAshLastRecordEndTime();
        $updatedAssets = array();
        foreach ($atWhLockinAssets as $key => $value) {
            if($value['end_time']!=''){
                $update_ass_status_his = array('end_time' => NULL);
                $update_asset_where = array('ash_id' => $value['ash_id']);
                $this->Common_model->update_data('asset_status_history',$update_ass_status_his,$update_asset_where);
                $updatedAssets[] = $value['asset_id'];            
            }
        }
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.</div>');
        }
        else
        {
            $this->db->trans_commit();
            echo "<pre>";print_r($updatedAssets);exit;
        } 
    }

    public function getAssetsWithIssues()
    {
        $country_id = validate_number(@$this->uri->segment(2));
        $postedStatus = validate_number(@$this->uri->segment(3)); 
        $this->load->model('Special_cases_m');
        if($postedStatus !='')
            $assetStatus = $this->Common_model->get_data('asset_status',array('asset_status_id'=>$postedStatus));
        else
            $assetStatus = $this->Common_model->get_data('asset_status');

        foreach ($assetStatus as $key => $value) {
            $status = $value['asset_status_id'];
            $allAssets = $this->Special_cases_m->getAssets($status,$country_id);
            $assetPositionData = $this->Special_cases_m->getAssetPosition($country_id,$status);
            echo "<b>" .$value['name']. "</b><br>";
            switch ($status) {
                case 1: case 2:
                    commonConditions($status,$allAssets,$country_id);             

                    $apIssues = assetPositionCheck($status,$assetPositionData);
                    if(count($apIssues)>0){
                        echo "<br>AP Issues<pre>";print_r($apIssues);    
                    }
                    break;
                case 3:
                    $InReturnIssueData = checkingInToolReturn($allAssets,$country_id);                    
                    if(count($InReturnIssueData) > 0){
                        echo "<br> In issues <br>";
                        // checking with defective asset data also
                        if(count($InReturnIssueData[0])>0){
                           $InReturnIssueData[0]  = checkingIssuesWithApprovals($country_id,$InReturnIssueData[0]);
                        }
                        echo "<pre>";print_r($InReturnIssueData);
                    }  

                    commonConditions($status,$allAssets,$country_id,array(2,7,8,9));                                             

                    $apIssues = assetPositionCheck($status,$assetPositionData);
                    if(count($apIssues)>0){
                        echo "<br>AP Issues<pre>";print_r($apIssues);    
                    }
                    break;
                case 4:                        
                        $calibrationIssue = checkingCalibraionAssetsNotInCalibrationProcess($allAssets,$country_id);                         
                        if(count($calibrationIssue[0])>0|| count($calibrationIssue[1]) >0 ){
                            echo "<br> Admin - Open Calibration <br>";
                            echo "<pre>";print_r($calibrationIssue[0]);
                            echo "Trans<br><pre>"; print_r($calibrationIssue[1]);
                        }  
                        commonConditions($status,$allAssets,$country_id,array(1,2,3,4,5,6,7,9));

                        $apIssues = assetPositionCheck($status,$assetPositionData);
                        if(count($apIssues)>0){
                            echo "<br>AP Issues<pre>";print_r($apIssues);    
                        }
                break;
                case 5: 
                    if(count($allAssets)>0)
                    {
                        echo "Defective Status All Assets List<br>";
                        commonConditions($status,$allAssets,$country_id);    
                    }
                break;
                case 6:case 7:case 9:case 11:
                    commonConditions($status,$allAssets,$country_id);             
                    if($status ==9 || $status == 11){
                        $apIssues = assetPositionCheck($status,$assetPositionData);
                        if(count($apIssues)>0){
                            echo "<br>AP Issues<pre>";print_r($apIssues);    
                        }
                    }
                break;
                case 8:
                    $inTransitIssues = checkingInTransitShouldBeButNotAvailable($allAssets,$country_id);
                    if(count($inTransitIssues[0])>0){
                           $inTransitIssues[0]  = checkingIssuesWithApprovals($country_id,$inTransitIssues[0]);
                    }
                    echo "<br><pre> Should be but not available";print_r($inTransitIssues); 
                    commonConditions($status,$allAssets,$country_id,array(1,4,5,8,9));    
                    $apIssues = assetPositionCheck($status,$assetPositionData);
                    if(count($apIssues)>0){
                        echo "<br>AP Issues<pre>";print_r($apIssues);    
                    }
                break;
                case 10:
                    $repairIssue = checkingRepairAssetsNotInRepairProcess($allAssets,$country_id);                         
                    if(count($repairIssue[0])>0|| count($repairIssue[1]) >0 ){
                        echo "<br> Admin - Open Repair <br>";
                        echo "<pre>";print_r($repairIssue[0]);
                        echo "Trans<br><pre>"; print_r($repairIssue[1]);
                    }  
                    commonConditions($status,$allAssets,$country_id,array(1,2,3,4,5,6,7,8));
                    $apIssues = assetPositionCheck($status,$assetPositionData);
                    if(count($apIssues)>0){
                        echo "<br>AP Issues<pre>";print_r($apIssues);    
                    }
                break;
                case 12:
                    $outOfCalibrationIssue = checkingOutOfCalibrationAssetsNotInOutOfCalibrationProcess($allAssets,$country_id);                         
                    if(count($outOfCalibrationIssue[0])>0|| count($outOfCalibrationIssue[1]) >0 ){
                        echo "<br> Admin - Out of Calibration <br>";
                        echo "<pre>";print_r($outOfCalibrationIssue[0]);
                        echo "Trans<br><pre>"; print_r($outOfCalibrationIssue[1]);
                    }
                default:
                    # code...
                    break;
            }
        }

        $approvalAssets = $this->Special_cases_m->getAssets('',$country_id,1);
        if(count($approvalAssets)>0){
            echo "<b>Approvals not in Defective/Missed Asset Request</b>";
            $defData = checkingDefAssetDataNotInDefeAsset($approvalAssets,$country_id);
            echo "<br><pre>";print_r($defData);
        }
    }

    public function getFEtoFEIssues()
    {
        $this->load->model('Special_cases_m');
        $country_id = validate_number(@$this->uri->segment(2));
        $orderedToolOa = $this->Special_cases_m->getOrderedToolsOa($country_id);
        $oahOa = $this->Special_cases_m->getOahOa($country_id);
        $firstOa = array();
        $secondOa = array();
        if(count($orderedToolOa)>0){
            foreach ($orderedToolOa as $key => $value) {
                $firstOa[$value['order_number']][] = $value['ordered_asset_id'];
            }
        }

        if(count($oahOa)>0){
            foreach ($oahOa as $key2 => $value2) {
                $secondOa[$value2['order_number']][] = $value2['ordered_asset_id'];
            }
        }
        echo "<pre>";print_r($orderedToolOa);
        echo "<pre>";print_r($oahOa);
        echo "<pre>";print_r($firstOa);
        echo "<pre>";print_r($secondOa);

        $issuesOrders = array();
        if(count($firstOa)>0){
            foreach ($firstOa as $main_key => $main_value) {
                if(isset($secondOa[$main_key])){
                    $secondOaIndexed = $secondOa[$main_key];
                    foreach ($main_value as $key => $value) {
                        if(!in_array($value, $secondOaIndexed)){
                            if(!in_array($main_key, $issuesOrders)){
                                $issuesOrders[] = $main_key;
                            }
                        }
                    }
                }
            }
        }
        echo "<pre>";print_r($issuesOrders);
        $openOrderesWithIssues = array();
        if(count($issuesOrders)>0){
            foreach ($issuesOrders as $key4 => $value4) {
                $exist = $this->Common_model->get_value('tool_order',array('order_number'=>$value4,'country_id'=>$country_id,'current_stage_id<'=>10),'tool_order_id');
                if(@$exist!='')
                    $openOrderesWithIssues[] = $value4;
            }
        }            
        echo "Final Issues:<br><pre>";print_r($openOrderesWithIssues);exit;
    }

    public function add_preference()
    {
        $pref_arr = $this->Common_model->get_data('preference',array('status'=>1));
        $countries_list = $this->Common_model->get_data('location',array('level_id'=>2,'status'=>1));
        foreach ($countries_list as $key => $value)
        {
            $country_id = $value['location_id'];
            $region_id = $value['region_id'];
            

            foreach ($pref_arr as $key1 => $value1) 
            {
                $pref_id = $value1['pref_id'];
                
                $status = ($region_id==2)?1:2;
              
                $insert_location_pref = array(
                    'pref_id'     => $pref_id,
                    'location_id' => $country_id,
                    'status'      => $status
                );
                $this->Common_model->insert_data('location_pref',$insert_location_pref);
            }
        }
        redirect(SITE_URL.'home');
    }

    #change current stage to tools coordinator in cal process
    public function change_to_tc()
    {
        $rca_arr = get_rca_exclude();
        $loop_arr = array();

        foreach ($rca_arr as $key => $value) 
        {
            $current_stage_id = $value['current_stage_id'];
            $rc_asset_id = $value['rc_asset_id'];
            $asset_id = $value['asset_id'];

            if($current_stage_id == 14)
            {
                $update_rcsh = array('current_stage_id' => 16);
                $update_rcsh_where = array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>$current_stage_id);
                $this->Common_model->update_data('rc_status_history',$update_rcsh,$update_rcsh_where);

                $update_rc = array('current_stage_id' => 16);
                $update_rc_where = array('rc_asset_id' => $rc_asset_id);
                $this->Common_model->update_data('rc_asset',$update_rc,$update_rc_where);
                $loop_arr[] = $value;
            }
            else if($current_stage_id == 15)
            {
                $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>15),'rcsh_id');
                $remarks = 'approved';
                // update Calibration Data
                $this->Common_model->update_data('rc_asset',array('current_stage_id'=>16,'remarks'=>$remarks),array('rc_asset_id'=>$rc_asset_id));

                // update cal_asset_history data
                $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));

                $rcsh_data=array(
                    'rc_asset_id'      => $rc_asset_id,   
                    'current_stage_id' => 16,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
                $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                // update asset_status_history data
                $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('sso_id')),array('ash_id'=>$ash_id));
                $ash_arr=array(
                    'status'           => 4,
                    'asset_id'         => $asset_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'rc_asset_id'      => $rc_asset_id,
                    'current_stage_id' => 16
                );
                $this->Common_model->insert_data('asset_status_history',$ash_arr);
                $loop_arr[] = $value;
            }
        }
        echo count($loop_arr).'<br>'; 
        echo "<pre>"; print_r($loop_arr); exit();
    }
}