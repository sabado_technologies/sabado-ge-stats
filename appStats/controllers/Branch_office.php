<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
 * created by Roopa on 19th june 2017 11:00AM
*/
class Branch_office extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Branch_office_m');
	}
    //Branch Office View..
	public function branch_office()
    {
        
        $data['nestedView']['heading']="Manage Branch Office";
		$data['nestedView']['cur_page'] = 'branch_office_master';
		$data['nestedView']['parent_page'] = 'manage_branch';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Manage Branch Office';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Branch Office','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchbranch_office', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'name'       => validate_string($this->input->post('branch_name', TRUE)),
                               'country_id' => validate_number(@$this->input->post('country_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                              'name'       => $this->session->userdata('name'),
                              'country_id' => $this->session->userdata('country_id')
                              );
            }
            else {
                $searchParams=array(
                                'name'       => '',
                                'country_id' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'branch_office/';
        # Total Records
        $config['total_rows'] = $this->Branch_office_m->branch_office_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['branch_office_results'] = $this->Branch_office_m->branch_office_results($current_offset, $config['per_page'], $searchParams,$task_access);
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('branch/branch_office_view',$data);

    }
    //add Branch..
    public function add_branch_office()
    {
        
        $data['nestedView']['heading']="Add Branch Office";
        $data['nestedView']['cur_page'] = 'check_branch_office_master';
        $data['nestedView']['parent_page'] = 'manage_branch';

        $task_access = page_access_check($data['nestedView']['parent_page']);
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Select Country";
                $data['nestedView']['cur_page'] = 'check_warehouse_user';
                $data['nestedView']['parent_page'] = 'manage_branch';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/branch_office.js"></script>';
                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Branch Office','class'=>'','url'=>SITE_URL.'branch_office');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_branch_office';
                $data['cancel']=SITE_URL.'branch_office';
                $this->load->view('user/intermediate_page',$data);
            }
        }
        if(@$country_id != '')
        {
		# include files
    		$data['nestedView']['js_includes'] = array();
    		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/branch_office.js"></script>';
    		$data['nestedView']['css_includes'] = array();

    		# Breadcrumbs
    		$data['nestedView']['breadCrumbTite'] = 'Add Branch Office';
    		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
    		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Branch Office','class'=>'','url'=>SITE_URL.'branch_office');
    		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add Branch Office','class'=>'active','url'=>'');

            # Additional data
            $data['area']= $this->Branch_office_m->get_area_location($country_id);
            $data['country_id']=$country_id;
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['flg'] = 1;
            $data['form_action'] = SITE_URL.'insert_branch_office';
            $data['displayResults'] = 0;
            $this->load->view('branch/branch_office_view',$data);
        }
    }
    //insert branch..
    public function insert_branch_office()    
    {
        $task_access = page_access_check('manage_branch');

        $country_id=validate_number($this->input->post('c_id',TRUE));
        $name = validate_string($this->input->post('name', TRUE));   
        $branch_id=0;
        $available = $this->Branch_office_m->is_officenameExist($name,$branch_id,$country_id);
        if($available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Office Name already exist! please check.
            </div>'); 
            redirect(SITE_URL.'branch_office'); exit();
        } 

        $data = array(
                'name'          => $name,
                'country_id'    => $country_id,
                'location_id'   => validate_number($this->input->post('area',TRUE)),                
                'phone_number'  => validate_string($this->input->post('contact_no',TRUE)),
                'pin_code'      => validate_string($this->input->post('pin_no',TRUE)),
                'tin_number'    => validate_string($this->input->post('tin_no',TRUE)), 
                'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
                'pan_number'    => validate_string($this->input->post('pan_no',TRUE)), 
                'cst_number'    => validate_string($this->input->post('cst_no',TRUE)),
                'Branch_manager'=> validate_string($this->input->post('contact_name',TRUE)),
                'email'         => validate_string($this->input->post('email',TRUE)), 
                'address1'      => validate_string($this->input->post('address_1',TRUE)),
                'address2'      => validate_string($this->input->post('address_2',TRUE)),
                'address3'      => validate_string($this->input->post('address_3',TRUE)),
                'address4'      => validate_string($this->input->post('address_4',TRUE)),                  
                'created_by'    => $this->session->userdata('sso_id'),
                'created_time'  =>date('Y-m-d H:i:s')
                      );
        $this->db->trans_begin();
        $this->Common_model->insert_data('branch',$data);
        if($this->db->trans_status()===FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.
            </div>');
        }
        else
        {
            $this->db->trans_commit(); 
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Branch Office :<strong>'.$name.'</strong> has been Added successfully!
            </div>');   
        }
        redirect(SITE_URL.'branch_office');  
    }
    //edit branch..
     public function edit_branch_office()
    {
        $branch_id=@storm_decode($this->uri->segment(2));
        if($branch_id=='')
        {
            redirect(SITE_URL.'branch_office');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Branch Office";
        $data['nestedView']['cur_page'] = 'check_branch_office_master';
        $data['nestedView']['parent_page'] = 'manage_branch';

        $task_access = page_access_check($data['nestedView']['parent_page']);

        $country_id = $this->Common_model->get_value('branch',array('branch_id'=>$branch_id),'country_id');
        if(@$country_id != '')
        {
        # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/branch_office.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit Branch Office';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Branch Office','class'=>'','url'=>SITE_URL.'branch_office');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Branch Office','class'=>'active','url'=>'');

            # Additional data
            $data['flg'] = 2;
            $data['form_action'] = SITE_URL.'update_branch_office';
            $data['display_results'] = 0;

            # Data
            $row = $this->Common_model->get_data('branch',array('branch_id'=>$branch_id));
            $data['area']= $this->Branch_office_m->get_area_location($country_id);
            $data['branch_row'] = $row[0];
            $data['country_id'] = $country_id;
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');        
            $this->load->view('branch/branch_office_view',$data);
        }
    }
    //update branch...
    public function update_branch_office()
    {
        $task_access = page_access_check('manage_branch');

        $branch_id=validate_number(@storm_decode($this->input->post('encoded_id',TRUE)));
        if($branch_id==''){
            redirect(SITE_URL.'branch_office');
            exit;
        }
        $country_id = validate_number($this->input->post('c_id',TRUE));
        $name = validate_string($this->input->post('name',TRUE));
        $available = $this->Branch_office_m->is_officenameExist($name,$branch_id,$country_id);
        if($available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>'.$name.' already exist! please check.
            </div>'); 
            redirect(SITE_URL.'branch_office'); exit();
        }  

        $data = array(
                'name'          => $name,     
                'country_id'    => $country_id,               
                'location_id'   => validate_number($this->input->post('area',TRUE)),
                'phone_number'  => validate_string($this->input->post('contact_no',TRUE)),
                'pin_code'      => validate_string($this->input->post('pin_no',TRUE)),
                'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
                'pan_number'    => validate_string($this->input->post('pan_no',TRUE)),
                'tin_number'    => validate_string($this->input->post('tin_no',TRUE)),  
                'cst_number'    => validate_string($this->input->post('cst_no',TRUE)),
                'Branch_manager'=> validate_string($this->input->post('contact_name',TRUE)),
                'email'         => validate_string($this->input->post('email',TRUE)),   
                'address1'      => validate_string($this->input->post('address_1',TRUE)),
                'address2'      => validate_string($this->input->post('address_2',TRUE)),
                'address3'      => validate_string($this->input->post('address_3',TRUE)),
                'address4'      => validate_string($this->input->post('address_4',TRUE)),                 
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
                      );  
            
        $where = array('branch_id'=>$branch_id);
        $this->db->trans_begin();
        $this->Common_model->update_data('branch',$data,$where);
        if($this->db->trans_status()===FALSE)
        {
            $this->db->trans_rollback(); 
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.
            </div>');            
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong>Branch Office has been updated successfully!
             </div>');
        }
        redirect(SITE_URL.'branch_office');
    }
    //deactivate branch
    public function deactivate_branch_office($encoded_id)
    {
        $task_access = page_access_check('manage_branch');
        $branch_id=@storm_decode($encoded_id);
        if($branch_id==''){
            redirect(SITE_URL.'branch');
            exit;
        }
        $where = array('branch_id' => $branch_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('branch',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Branch Office has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'branch_office');

    }
    //activate Branch
    public function activate_branch_office($encoded_id)
    {
        $task_access = page_access_check('manage_branch');

        $branch_id=@storm_decode($encoded_id);
        if($branch_id==''){
            redirect(SITE_URL.'branch');
            exit;
        }
        $where = array('branch_id' => $branch_id);
        //activating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('branch',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Branch Office has been Activated successfully!
        </div>');
        redirect(SITE_URL.'branch_office');
    }

    //office name uniqueness...
    public function is_officenameExist()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $branch_id = validate_number($this->input->post('branch_id',TRUE));
        $country_id = $this->input->post('country_id');
        $result = $this->Branch_office_m->is_officenameExist($name,$branch_id,$country_id);
        echo $result;
    }
}