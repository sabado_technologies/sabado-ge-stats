<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
 * created by Roopa on 17th june 2017 11:00AM Modified By Pradad on 29th June'17
*/
class Supplier extends Base_Controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Supplier_m');
    }
    
    public function supplier()
    {
        
        $data['nestedView']['heading']="Manage Supplier";
        $data['nestedView']['cur_page'] = 'supplier_master';
        $data['nestedView']['parent_page'] = 'manage_supplier';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        //print_r($task_access);exit();
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Manage Supplier';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Supplier','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchsupplier', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'name'       => validate_string($this->input->post('supplier_name', TRUE)),
                               'modality_id'=> validate_number($this->input->post('modality_id', TRUE)),
                               'calibration'=> validate_number($this->input->post('calibration', TRUE)),
                               'sup_type_id'=> validate_number($this->input->post('sup_type_id', TRUE)),
                               'country_id' => validate_number(@$this->input->post('country_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                              'name'        => $this->session->userdata('name'),
                              'modality_id' => $this->session->userdata('modality_id'),
                              'calibration' => $this->session->userdata('calibration'),
                              'sup_type_id' => $this->session->userdata('sup_type_id'),
                              'country_id'  => $this->session->userdata('country_id')
                              );
            }
            else {
                $searchParams=array(
                                    'name'       => '',
                                    'modality_id'=> '',
                                    'calibration'=> '',
                                    'sup_type_id'=> '',
                                    'country_id' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'supplier/';
        # Total Records
        $config['total_rows'] = $this->Supplier_m->supplier_total_num_rows($searchParams,$task_access); 
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
         $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        # Loading the data array to send to View
        $data['supplier_results'] = $this->Supplier_m->supplier_results($current_offset, $config['per_page'], $searchParams,$task_access);
        $data['task_access'] = $task_access;
        # Additional data
        $data['modality'] = array(''=>'- Select Modality -') + $this->Common_model->get_dropdown('modality', 'modality_id', 'name', array('status'=>1));
        $data['displayResults'] = 1;

        $this->load->view('supplier/supplier_view',$data);
    }

    public function add_supplier()
    {   
        $data['nestedView']['heading']="Add New supplier";
        $data['nestedView']['cur_page'] = 'check_supplier_master';
        $data['nestedView']['parent_page'] = 'manage_supplier';

        $task_access = page_access_check($data['nestedView']['parent_page']);
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Select Country";
                $data['nestedView']['cur_page'] = 'check_supplier_master';
                $data['nestedView']['parent_page'] = 'manage_supplier';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/supplier.js"></script>';
                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Supplier','class'=>'','url'=>SITE_URL.'supplier');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_supplier';
                $data['cancel']=SITE_URL.'supplier';
                $this->load->view('user/intermediate_page',$data);
            }
        }
        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/supplier.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New Supplier';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Supplier','class'=>'','url'=>SITE_URL.'supplier');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Supplier','class'=>'active','url'=>'');

            $data['modality'] = array(''=>'Select Modality') + $this->Common_model->get_dropdown('modality', 'modality_id', 'name', array('status'=>1));
            $data['modality_selected'] = array();
            $data['country_id']=$country_id;
            
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            # Additional data
            $data['flg'] = 1;
            $data['form_action'] = SITE_URL.'insert_supplier';
            $data['displayResults'] = 0;

            //Retreving supplier types from prasad helper
            $data['supplier_type'] = get_supplier_type();
            //Retreving calibration types from prasad helper
            $data['calibration'] = calibration_type();
            //echo '<pre>'; print_r($data['calibration']);

            /*$number = $this->Supplier_m->get_latest_record($country_id);
            if($number == '')
            {
                $supplier_code = "TS01";
            }
            else
            {
                $num = $number['supplier_code'];
                $result = explode("TS", $num);
                $running_no = (int)$result[1];
                $r_no = $running_no+1;
                $count = strlen($r_no);
                $zero_count = 2-$count;
                $sno = '';
                while($zero_count > 0)
                {
                    $sno.='0';
                    $zero_count--;
                }
                $sno.=$r_no;
                $supplier_code = "TS".$sno;
            }*/

            $data['supplier_code'] = '';
            $data['trans_type'] = $this->Common_model->get_data('cal_sup_type');
            $this->load->view('supplier/supplier_view',$data);
        }
    }
    public function insert_supplier()
    {
        $task_access = page_access_check('manage_supplier');

        $name = validate_string($this->input->post('supplier_name', TRUE));
        $modality = $this->input->post('modality', TRUE);
        $calibration=validate_string($this->input->post('calibration',TRUE));
        $cal_sup_type_id = validate_number($this->input->post('transport_type',TRUE));
        $sup_type_id = validate_number($this->input->post('supplier_type',TRUE));
        $country_id = validate_number($this->input->post('c_id',TRUE));
        $supplier_code = validate_string($this->input->post('supplier_code',TRUE));
        /*$number = $this->Supplier_m->get_latest_record($country_id);
        if($number == '')
        {
            $supplier_code = "TS01";
        }
        else
        {
            $num = $number['supplier_code'];
            $result = explode("TS", $num);
            $running_no = (int)$result[1];
            $r_no = $running_no+1;
            $count = strlen($r_no);
            $zero_count = 2-$count;
            $sno = '';
            while($zero_count > 0)
            {
                $sno.='0';
                $zero_count--;
            }
            $sno.=$r_no;
            $supplier_code = "TS".$sno;
        }
        //echo $supplier_code; exit;*/
        $data = array(
                  'name'              => $name,
                  'country_id'        => $this->input->post('c_id'),
                  'supplier_code'     => $supplier_code,
                  'contact_person'    => validate_string($this->input->post('contact_person',TRUE)),
                  'email'             => validate_string($this->input->post('email',TRUE)),
                  'contact_number'    => validate_string($this->input->post('phone_number',TRUE)),
                  'calibration'       => $calibration,
                  'sup_type_id'       => $sup_type_id,
                  'cal_sup_type_id'   => validate_number($this->input->post('transport_type',TRUE)),
                  'address1'          => validate_string($this->input->post('address_1',TRUE)),
                  'address2'          => validate_string($this->input->post('address_2',TRUE)),
                  'address3'          => validate_string($this->input->post('address_3',TRUE)),
                  'address4'          => validate_string($this->input->post('address_4',TRUE)),
                  'pin_code'          => validate_string($this->input->post('pin_code',TRUE)),
                  'gst_number'        => validate_string($this->input->post('gst_number',TRUE)),
                  'pan_number'        => validate_string($this->input->post('pan_number',TRUE)),
                  'created_by'        => $this->session->userdata('sso_id'),
                  'created_time'      => date('Y-m-d H:i:s')
                     ); 

        //echo '<pre>';print_r($data);exit;
        $this->db->trans_begin();
        $supplier_id = $this->Common_model->insert_data('supplier',$data); 
        if($sup_type_id==1) // If supplier
        {
            if(isset($modality))
            {
                foreach ($modality as $key => $modality_id)
                {
                    $data1=array('supplier_id' =>$supplier_id,'modality_id' =>$modality_id);
                    $this->Common_model->insert_data('modality_supplier',$data1);
                } 
            }       
        }  
        if($this->db->trans_status()===FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.</div>');
        }
        else
        {
            $this->db->trans_commit();            
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Supplier : <strong>'.$supplier_code.'</strong> has been Added successfully!</div>');
        }
        redirect(SITE_URL.'supplier'); exit();
    }

    public function edit_supplier()
    {
        $supplier_id=@storm_decode($this->uri->segment(2));
        if($supplier_id=='')
        {
            redirect(SITE_URL.'supplier');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Supplier";
        $data['nestedView']['cur_page'] = 'check_supplier_master';
        $data['nestedView']['parent_page'] = 'manage_supplier';

        $task_access = page_access_check($data['nestedView']['parent_page']);
        $row = $this->Common_model->get_data_row('supplier',array('supplier_id'=>$supplier_id));
        $country_id = $row['country_id'];
        if(@$country_id != '')
        {
        # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';        
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/supplier.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit Supplier';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Supplier','class'=>'','url'=>SITE_URL.'supplier');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Supplier','class'=>'active','url'=>'');

            # Additional data
            $data['flg'] = 2;
            $data['lrow'] = $row;
            $data['country_id'] = $country_id;
            $data['form_action'] = SITE_URL.'update_supplier';
            $data['displayResults'] = 0;
            $data['trans_type'] = $this->Common_model->get_data('cal_sup_type');

            # Data    
            $data['modality_selected'] = array_column($this->Common_model->get_data('modality_supplier', array('supplier_id'=>$supplier_id,'status'=>1), array('modality_id'),"",'1'),'modality_id'); 
            $data['modality'] = $this->Common_model->get_dropdown('modality', 'modality_id', 'name', array('status'=>1));
            
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            
            //Retreving supplier types from prasad helper
            $data['supplier_type'] = get_supplier_type();

            //Retreving calibration types from prasad helper
            $data['calibration'] = calibration_type();
            //echo '<pre>';print_r($data['calibration']);exit;

            $this->load->view('supplier/supplier_view',$data);
        }
    }

    public function update_supplier()
    {
        $task_access = page_access_check('manage_supplier');

        $supplier_id=validate_number(@storm_decode($this->input->post('encoded_id',TRUE)));
        if($supplier_id==''){
            redirect(SITE_URL.'supplier');
            exit;
        }
        $name = validate_string($this->input->post('supplier_name', TRUE));
        $email = validate_string($this->input->post('email',TRUE));
        $contact_number = validate_string($this->input->post('phone_number',TRUE));
        $calibration=validate_number($this->input->post('calibration',TRUE));
        $sup_type_id = validate_number($this->input->post('supplier_type',TRUE));
        $modality = $this->input->post('modality', TRUE);
        $supplier_code = validate_string($this->input->post('supplier_code',TRUE));
        $data = array(
        'name'              => $name,
        'country_id'        => $this->input->post('c_id'),
        'contact_person'    => validate_string($this->input->post('contact_person',TRUE)),
        'email'             => $email,
        'contact_number'    => $contact_number,
        'calibration'       => $calibration,
        'sup_type_id'       => $sup_type_id,
        'supplier_code'     => $supplier_code,
        'cal_sup_type_id'   => validate_number($this->input->post('transport_type',TRUE)),
        'address1'          => validate_string($this->input->post('address_1',TRUE)),
        'address2'          => validate_string($this->input->post('address_2',TRUE)),
        'address3'          => validate_string($this->input->post('address_3',TRUE)),
        'address4'          => validate_string($this->input->post('address_4',TRUE)),
        'pin_code'          => validate_string($this->input->post('pin_code',TRUE)),
        'gst_number'        => validate_string($this->input->post('gst_number',TRUE)),
        'pan_number'        => validate_string($this->input->post('pan_number',TRUE)),
        'modified_by'       => $this->session->userdata('sso_id'),
        'modified_time'     => date('Y-m-d H:i:s'),
        'status'            => 1);

        $where = array('supplier_id' =>$supplier_id);
        $this->db->trans_begin();
        $res = $this->Common_model->update_data('supplier',$data,$where);
        if($res)
        {
            $where = array('supplier_id' =>$supplier_id);
            $data = array('status'=>2);
            $this->Common_model->update_data('modality_supplier',$data,$where); 

            #intially make status 2 for supplier_id
            $update_data1 =array('status'=> 2);
            $update_where1 = array('supplier_id'=> $supplier_id);
            $this->Common_model->update_data('modality_supplier',$update_data1,$update_where1);

            #make changes from post
            if($sup_type_id == 1)
            {
                 if(@count($modality)>0)
                {
                    foreach ($modality as $modality_id) 
                    {
                        $this->Supplier_m->modality_supplier_insert_update($supplier_id,$modality_id);
                    }
                }
            }
           
        }
             
        if($this->db->trans_status()===FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check. </div>');
        }
        else
        {
            $this->db->trans_commit();            
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Supplier has been updated successfully!
            </div>');
        }
        redirect(SITE_URL.'supplier');
    }

    public function deactivate_supplier($encoded_id)
    {
        $task_access = page_access_check('manage_supplier');
        $supplier_id=@storm_decode($encoded_id);
        if($supplier_id==''){
            redirect(SITE_URL.'supplier');exit;
        }
        $where = array('supplier_id' => $supplier_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('supplier',$data_arr, $where);
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Supplier has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'supplier');

    }

    public function activate_supplier($encoded_id)
    {
        $task_access = page_access_check('manage_supplier');
        $supplier_id=@storm_decode($encoded_id);
        if($supplier_id==''){
            redirect(SITE_URL.'supplier');exit;
        }
        $where = array('supplier_id' => $supplier_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('supplier',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Supplier has been Activated successfully!
        </div>');
        redirect(SITE_URL.'supplier');
    }
    public function supplier_name_availability()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $supplier_id = validate_number($this->input->post('supplier_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('name'=>$name,'supplier_id'=>$supplier_id,'country_id'=>$country_id);
        $result = $this->Supplier_m->supplier_name_availability($data);
        echo $result;
    }
}