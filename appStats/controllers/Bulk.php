<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Bulk extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
    }

    public function bulk_modality()
    {
      $data['nestedView']['heading']="modality Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'modality Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'modality Bulk Upload','class'=>'active','url'=>'');

      #additional Data        
      $data['form_action'] = SITE_URL.'insert_bulkupload_modality';  
      $this->load->view('bulk/city_upload_view',$data);
    }

    public function insert_bulkupload_modality()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              $insert_location_arr= array();
              #loop the records fetched from csv file
              while (($modData = fgetcsv($file, 10000, ",")) !== FALSE)
              {
                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 

                  #assign data to variable as feteched individually
                  $modality_code = trim($modData[0]);
                  $modality_name  = trim($modData[1]);


                  $check_mod = $this->Common_model->get_value('modality',array('name'=>$modality_name),'modality_id');

                  if($check_mod=='')
                  {

                    $insert_mod = array(
                      'name'         => $modality_name,
                      'modality_code'=> $modality_code,
                      'created_by'   => 0,
                      'created_time' => date('Y-m-d H:i:s'),
                      'status'       => 1);
                    $this->Common_model->insert_data('modality',$insert_mod);
                  }
                  $j++;
              }
              //echo '<pre>';print_r($insert_location_arr);exit;
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'bulk_modality');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong>modality successfully inserted!
                    </div>');
                    redirect(SITE_URL.'bulk_modality'); exit();
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'bulk_modality'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulk_modality'); exit();
      }
    } 

    public function bulk_city()
    {
      $data['nestedView']['heading']="City Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'City Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'City Bulk Upload','class'=>'active','url'=>'');

      #additional Data        
      $data['form_action'] = SITE_URL.'insert_bulkupload_city';  
      $this->load->view('bulk/location_upload_view',$data);
  	} 

  	public function insert_bulkupload_city()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $level_id = validate_number($this->input->post('level_id',TRUE));
          if($level_id == '')
          {
            redirect(SITE_URL.'bulk_city'); exit();
          }
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              $insert_location_arr= array();
              #loop the records fetched from csv file
              while (($cityData = fgetcsv($file, 10000, ",")) !== FALSE)
              {
                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 

                  #assign data to variable as feteched individually
                  $first = trim($cityData[0]);
                  $second  = trim($cityData[1]);
                  $short_name = trim(@$cityData[2]);
                  //echo '<pre>';print_r($cityData);exit;

                  $inc_level_id = $level_id+1;
                  $first_id = $this->Common_model->get_value('location',array('name'=>$first,'level_id'=>$level_id),'location_id');

                  $second_id = $this->Common_model->get_value('location',array('name'=>$second,'level_id'=>$inc_level_id),'location_id');

                  if($first_id!='' && $second !='' && $second_id=='')
                  {
                  	$insert_location = array('name'        => $second,
                                            'level_id'     => $inc_level_id,
                                            'parent_id'    => $first_id,
                                            'created_by'   => 0,
                                            'created_time' => date('Y-m-d H:i:s'),
                                            'status'       => 1);
                    if($short_name!='')
                    {
                      $insert_location['short_name'] = $short_name;
                    }
                  	$this->Common_model->insert_data('location',$insert_location);
                  }
                  $j++;
              }
              //echo '<pre>';print_r($insert_location_arr);exit;
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'bulk_city');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong>Locations successfully inserted!
                    </div>');
                    redirect(SITE_URL.'bulk_city'); exit();
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'bulk_city'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulk_city'); exit();
      }
    }

    public function bulk_supplier()
    {
      $data['nestedView']['heading']="Supplier Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'Supplier Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Supplier Bulk Upload','class'=>'active','url'=>'');

      #additional Data        
      $data['form_action'] = SITE_URL.'insert_bulkupload_supplier';  
      $this->load->view('bulk/city_upload_view',$data);
  	} 

  	public function insert_bulkupload_supplier()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;

              #loop the records fetched from csv file
              while (($supplierData = fgetcsv($file, 10000, ",")) !== FALSE)
              { 

                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 

                  #assign data to variable as feteched individually
                  $modality = validate_string(trim($supplierData[0]));
                  $supplier_code  = validate_string(trim($supplierData[1]));
                  $name = validate_string(trim($supplierData[2]));
                  $contact_person  = validate_string(trim($supplierData[3]));
                  $contact_number = validate_string(trim($supplierData[4]));
                  $email  = validate_string(trim($supplierData[5]));
                  $calibration = validate_string(trim($supplierData[6]));
                  $cal_sup_type_id  = validate_string(trim($supplierData[7]));
                  $sup_type_id = validate_string(trim($supplierData[8]));
                  $address1  = validate_string(trim($supplierData[9]));
                  $address2 = validate_string(trim($supplierData[10]));
                  $address3  = validate_string(trim($supplierData[11]));
                  $address4  = validate_string(trim($supplierData[12]));
                  $pin_code  = validate_string(trim($supplierData[13]));
                  $gst_number  = validate_string(trim($supplierData[14]));
                  $pan_number  = validate_string(trim($supplierData[15]));
                  $country_name  = validate_string(trim($supplierData[16]));

                  $country_id = $this->Common_model->get_value('location',array('name'=>$country_name,'level_id'=>2),'location_id');
                  $check_supp_code = '';
                  if($country_id!='')
                  {
                    $check_supp_code = $this->Common_model->get_value('supplier',array('supplier_code'=>$supplier_code,'country_id'=>$country_id),'supplier_id');
                  }
                  
                  if($check_supp_code=='' && $country_id!='')
                  {
                    $supplier_data = array(
                    'name'              => $name,
                    'supplier_code'     => $supplier_code,
                    'contact_person'    => $contact_person,
                    'email'             => $email,
                    'contact_number'    => $contact_number,
                    'calibration'       => $calibration,
                    'sup_type_id'       => $sup_type_id,
                    'cal_sup_type_id'   => $cal_sup_type_id,
                    'address1'          => $address1,
                    'address2'          => $address2,
                    'address3'          => $address3,
                    'address4'          => $address4,
                    'pin_code'          => $pin_code,
                    'gst_number'        => $gst_number,
                    'pan_number'        => $pan_number,
                    'country_id'        => $country_id,
                    'created_by'        => 0,
                    'created_time'      => date('Y-m-d H:i:s')
                       ); 
                    $supplier_id = $this->Common_model->insert_data('supplier',$supplier_data);

                    if($sup_type_id == 1)
                    {
                      $modality_id = $this->Common_model->get_value('modality',array('name'=>$modality),'modality_id');
                      if($modality_id=='')
                      {
                        $modality_id = $this->Common_model->get_value('modality',array('modality_code'=>$modality),'modality_id');
                      }
                      if($modality_id!='')
                      {
                        $insert_modality = array('modality_id' => $modality_id,
                                   'supplier_id' => $supplier_id,
                                   'status' => 1);
                        $this->Common_model->insert_data('modality_supplier',$insert_modality);
                      }
                      
                    }
                  }
                  
                  $j++;

              }
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'bulk_supplier');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> Suppliers successfully inserted!
                    </div>');
                    redirect(SITE_URL.'bulk_supplier'); exit();
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'bulk_supplier'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulk_supplier'); exit();
      }
    }

    public function bulk_warehouse()
    {
      $data['nestedView']['heading']="Warehouse Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'Warehouse Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Warehouse Bulk Upload','class'=>'active','url'=>'');

      #additional Data        
      $data['form_action'] = SITE_URL.'insert_bulkupload_warehouse';  
      $this->load->view('bulk/city_upload_view',$data);
  	} 

  	public function insert_bulkupload_warehouse()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              //$wh_upload_arr = array();
              #loop the records fetched from csv file
              while (($whData = fgetcsv($file, 10000, ",")) !== FALSE)
              { 

                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 
                  #assign data to variable as feteched individually
                  $name = validate_string(trim($whData[0]));
                  $wh_code  = validate_string(trim($whData[1]));
                  $tin_number = validate_string(trim($whData[2]));
                  $cst_number  = validate_string(trim($whData[3]));
                  $contact_person = validate_string(trim($whData[4]));
                  $phone_number  = validate_string(trim($whData[5]));
                  $email = validate_string(trim($whData[6]));
                  $address1  = validate_string(trim($whData[7]));
                  $address2 = validate_string(trim($whData[8]));
                  $address3  = validate_string(trim($whData[9]));
                  $address4 = validate_string(trim($whData[10]));
                  $pin_code = validate_string(trim($whData[11]));
                  $gst_number  = validate_string(trim($whData[12]));
                  $pan_number = validate_string(trim($whData[13]));
                  $area_name  = validate_string(trim($whData[14]));
                  $city_name  = validate_string(trim($whData[15]));
                  $contactinfo1name  = validate_string(trim($whData[16]));
                  $contactinfo1number  = validate_string(trim($whData[17]));
                  $contactinfo1email  = validate_string(trim($whData[18]));
                  $country_name = validate_string($whData[25]);

                  $country_id = $this->Common_model->get_value('location',array('name'=>$country_name,'level_id'=>2),'location_id');
                  if($country_id!='')
                  {
                      if($area_name!='')
                      {
                        $location_id = $this->Common_model->get_value('location',array('name'=>$area_name,'level_id'=>6),'location_id');
                        if($location_id=='')
                        {
                          $location_id = NULL;
                        }
                      }
                      else
                      {
                        $location_id = NULL;
                      }
                      $contact_info1 = @$contactinfo1name.','.@$contactinfo1number.','.@$contactinfo1email;
                      
                      $wh_id = $this->Common_model->get_value('warehouse',array('wh_code'=>$wh_code,'country_id'=>$country_id),'wh_id');

                      if($wh_id=='' && $country_id!='' && $location_id!='')
                      {
                        $insert_wh = array(
                          'name'         => $name,
                          'wh_code'      => $wh_code,
                          'tin_number'   => $tin_number,
                          'cst_number'   => $cst_number,
                          'managed_by'   => $contact_person,
                          'phone'        => $phone_number,
                          'email'        => $email,
                          'address1'     => $address1,
                          'address2'     => $address2,
                          'address3'     => $address3,
                          'address4'     => $address4,
                          'pin_code'     => $pin_code,
                          'gst_number'   => $gst_number,
                          'pan_number'   => $pan_number,
                          'location_id'  => $location_id,
                          'contact_info1'=> $contact_info1,
                          'created_by'   => 0,
                          'created_time' => date('Y-m-d H:i:s'),
                          'country_id'   => $country_id,
                          'status'       => 1);
                        $this->Common_model->insert_data('warehouse',$insert_wh);
                      }
                  }
                  
              //$wh_upload_arr[] = $insert_wh;
                  $j++;
              }
              //echo '<pre>';print_r($wh_upload_arr);exit;
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'bulk_warehouse');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> Warehouse successfully inserted!
                    </div>');
                    redirect(SITE_URL.'bulk_warehouse'); exit();
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'bulk_warehouse'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulk_warehouse'); exit();
      }
    }

     public function bulk_branch()
    {
      
      $data['nestedView']['heading']="Branch Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'Branch Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Branch Bulk Upload','class'=>'active','url'=>'');

      #additional Data        
      $data['form_action'] = SITE_URL.'insert_bulkupload_branch';  
      $this->load->view('bulk/city_upload_view',$data);
  	} 

  	public function insert_bulkupload_branch()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              $b_arr= array();
              #loop the records fetched from csv file
              while (($branchData = fgetcsv($file, 10000, ",")) !== FALSE)
              { 

                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 
                  #assign data to variable as feteched individually
                  $name = validate_string(trim($branchData[0]));
                  $tin_number  = validate_string(trim($branchData[1]));
                  $cst_number = validate_string(trim($branchData[2]));
                  $managed_by  = validate_string(trim($branchData[3]));
                  $phone_number = validate_string(trim($branchData[4]));
                  $email  = validate_string(trim($branchData[5]));
                  $address1 = validate_string(trim($branchData[6]));
                  $address2  = validate_string(trim($branchData[7]));
                  $address3 = validate_string(trim($branchData[8]));
                  $address4  = validate_string(trim($branchData[9]));
                  $pin_code = validate_string(trim($branchData[10]));
                  $gst_number  = validate_string(trim($branchData[11]));
                  $pan_number = validate_string(trim($branchData[12]));
                  $area_name  = validate_string(trim($branchData[13]));
                  $country_name  = validate_string(trim($branchData[15]));

                  if($area_name!='')
                  {
                    $location_id = $this->Common_model->get_value('location',array('name'=>$area_name,'level_id'=>6),'location_id');
                    if($location_id=='')
                    {
                      $location_id = NULL;
                    }
                  }
                  else
                  {
                    $location_id = NULL;
                  }
                  $country_id = $this->Common_model->get_value('location',array('name'=>$country_name,'level_id'=>2),'location_id');

                  $b_id = '';
                  if($country_id!='')
                  {
                    $b_id = $this->Common_model->get_value('branch',array('name'=>$name,'country_id'=>$country_id),'branch_id');
                  }

                  if($country_id!='' && $location_id!='' && @$b_id=='')
                  {
                    $insert_branch = array(
                      'name'     => $name,
                      'tin_number'   => $tin_number,
                      'cst_number'   => $cst_number,
                      'branch_manager' => $managed_by,
                      'phone_number'   => $phone_number,
                      'email'        => $email,
                      'address1'     => $address1,
                      'address2'     => $address2,
                      'address3'     => $address3,
                      'address4'     => $address4,
                      'pin_code'     => $pin_code,
                      'gst_number'   => $gst_number,
                      'pan_number'   => $pan_number,
                      'location_id'  => $location_id,
                      'created_by'   => 0,
                      'created_time' => date('Y-m-d H:i:s'),
                      'country_id'   => $country_id,
                      'status'       => 1);
                    $this->Common_model->insert_data('branch',$insert_branch);
                  }
                  
                  $j++;
              }
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'bulk_branch');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> Branch successfully inserted!
                    </div>');
                    redirect(SITE_URL.'bulk_branch'); exit();
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'bulk_branch'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulk_branch'); exit();
      }
    }

    public function bulk_designation()
    {
      $data['nestedView']['heading']="Designation Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'Designation Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Designation Bulk Upload','class'=>'active','url'=>'');

      #additional Data        
      $data['form_action'] = SITE_URL.'insert_bulkupload_designation';  
      $this->load->view('bulk/city_upload_view',$data);
    } 

    public function insert_bulkupload_designation()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              #loop the records fetched from csv file
              while (($cityData = fgetcsv($file, 10000, ",")) !== FALSE)
              { 
                //echo '<pre>';print_r($cityData);
                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 

                  #assign data to variable as feteched individually
                  $role_name = trim(@$cityData[0]);
                  $designation_name  = trim(@$cityData[1]);

                  if($role_name!='' && $designation_name!='')
                  {
                    $role_id = $this->Common_model->get_value('role',array('name'=>$role_name),'role_id');
                    if($role_id!='')
                    {
                      $designation_id = $this->Common_model->get_value('designation',array('name'=>$designation_name),'designation_id');
                      $checkdr = '';
                      if($designation_id!='')
                      {
                        $checkdr = $this->Common_model->get_value('role_designation',array('role_id'=>$role_id,'designation_id'=>$designation_id),'role_id'); 
                      }
                      
                      if($checkdr=='')
                      {
                        $data=array(
                        'name'                  =>     $designation_name,
                        'status'                =>     1,
                        'created_by'            =>     0,
                        'created_time'          =>     date('Y-m-d H:i:s'));
                        $designation = $this->Common_model->insert_data('designation',$data);

                        $data1=array(
                        'role_id' => $role_id,
                        'designation_id' => $designation
                        );
                        $role_designation = $this->Common_model->insert_data('role_designation',$data1);
                      }
                    }
                  }
                  $j++;
              }
              //echo '<pre>';print_r($insert_location_arr);exit;
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'bulk_designation');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong>Designation successfully inserted!
                    </div>');
                    redirect(SITE_URL.'bulk_designation'); exit();
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'bulk_designation'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulk_designation'); exit();
      }
    }


    public function bulkAssetStatus()
    {
      $data['nestedView']['heading']="Asset Status Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'Asset Status Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Asset Status Bulk Upload','class'=>'active','url'=>'');
      
      #additional Data        
      $data['form_action'] = SITE_URL.'Bulk/insertBulkAssetStatus';  
      $this->load->view('bulk/asset_status_upload_view',$data);
    } 

    public function insertBulkAssetStatus()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              $insert_location_arr= array();
              #loop the records fetched from csv file
              while (($assetData = fgetcsv($file, 10000, ",")) !== FALSE)
              {
                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 

                  #assign data to variable as feteched individually
                  $asset_number = trim($assetData[0]);
                  $status  = trim($assetData[1]);
                  //echo '<pre>';print_r($assetData);exit;
                  if($asset_number!='' && $status !='' )
                  {
                    $asset_id = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number),'asset_id');
                    $updateData = array('status'=>$status);
                    $updateWhere = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$updateData,$updateWhere);
                  }
                  $j++;
              }
              //echo '<pre>';print_r($insert_location_arr);exit;
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'Bulk/bulkAssetStatus');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong>Asset Status successfully Updated!
                    </div>');
                    redirect(SITE_URL.'Bulk/bulkAssetStatus');  
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'Bulk/bulkAssetStatus');  
          }  
      }
      else
      {
          redirect(SITE_URL.'Bulk/bulkAssetStatus');  
      }
    }
    
    public function assetUpdateWithNumber()
    {
      $data['nestedView']['heading']="assetUpdateWithNumber Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'assetUpdateWithNumber Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Asset Status Bulk Upload','class'=>'active','url'=>'');
      
      #additional Data        
      $data['form_action'] = SITE_URL.'Bulk/insertassetUpdateWithNumber';  
      $this->load->view('bulk/asset_status_upload_view',$data);
    } 

    public function insertassetUpdateWithNumber()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              $insert_location_arr= array();
              #loop the records fetched from csv file
              while (($assetData = fgetcsv($file, 10000, ",")) !== FALSE)
              {
                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 

                  #assign data to variable as feteched individually
                  $asset_number = trim($assetData[0]);
                  $status  = trim($assetData[1]);
                  //echo '<pre>';print_r($assetData);exit;
                  if($asset_number!='' && $status !='' )
                  {
                    
                    $updateData = array('status'=>$status);
                    $updateWhere = array('asset_number'=>$asset_number);
                    
                    $this->Common_model->update_data('asset',$updateData,$updateWhere);
                  }
                  $j++;
              }
              //echo '<pre>';print_r($insert_location_arr);exit;
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'Bulk/assetUpdateWithNumber');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong>Asset Status successfully Updated!
                    </div>');
                    redirect(SITE_URL.'Bulk/assetUpdateWithNumber');  
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'Bulk/assetUpdateWithNumber');  
          }  
      }
      else
      {
          redirect(SITE_URL.'Bulk/assetUpdateWithNumber');  
      }
    }

    public function bulkAssetCalibrationUpdate()
    {
      $data['nestedView']['heading']="Calibration Bulk Upload";
      $data['nestedView']['cur_page'] = '';
      $data['nestedView']['parent_page'] = '';

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'Calibration Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Bulk Upload','class'=>'active','url'=>'');

      #additional Data        
      $data['form_action'] = SITE_URL.'updateBulkAssetCalibration';  
      $this->load->view('bulk/city_upload_view',$data);
    }

    public function updateBulkAssetCalibration()
    {
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;
              $insert_location_arr= array();
              #loop the records fetched from csv file
              while (($calibration = fgetcsv($file, 10000, ",")) !== FALSE)
              {
                    #exclude the first record as contain heading
                    if($j==0) { $j++; continue; } 

                    #assign data to variable as feteched individually
                    $asset_number = trim($calibration[0]);
                    $calibratedDate  = trim($calibration[1]);
                    $calibrationDueDate  = trim($calibration[2]);
                    $countryId  = trim($calibration[3]);
                    $crNumber  = trim($calibration[4]);

                    $asset_id = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number,'country_id'=>$countryId),'asset_id');
                    $updateData = array();
                    if($asset_id!=''){
                        if($crNumber!='')
                            delete_cr_request($countryId,$crNumber);
                        if(check_for_rc_entry($asset_id) == 0)  {
                            if($calibratedDate!='')
                                $updateData['calibrated_date'] = format_date($calibratedDate);
                            if($calibrationDueDate!='')
                                $updateData['cal_due_date'] = format_date($calibrationDueDate);
                            $updateData['modified_by'] = 0;
                            $updateData['modified_time'] = date('Y-m-d H:i:s');
                            $updateWhere = array('asset_id'=>$asset_id);
                            $this->Common_model->update_data('asset',$updateData,$updateWhere);
                        }
                        $update++;
                    }
                    $j++;
              }
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'bulkAssetCalibrationUpdate');  
              }
              else
              {
                  $this->db->trans_commit();
                  $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> '.$update.' Records are updated!
                    </div>');
                    redirect(SITE_URL.'bulkAssetCalibrationUpdate'); exit();
                  
              }
          } 
          else
          {
              $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
              redirect(SITE_URL.'bulkAssetCalibrationUpdate'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulkAssetCalibrationUpdate'); exit();
      }
    } 
}
