<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Page extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Page_m');
	}

	public function task()
    {
        $data['nestedView']['heading']=" Manage Task";
		$data['nestedView']['cur_page'] = 'task';
		$data['nestedView']['parent_page'] = 'task';

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = ' Manage Task';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Task','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('search_page', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'page_name' => validate_string($this->input->post('page_name',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                               'page_name' => validate_string($this->input->post('page_name',TRUE))
                               );
            }
            else {
                $searchParams=array(
                                    'page_name' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'task/';
        # Total Records
        $config['total_rows'] = $this->Page_m->page_total_num_rows($searchParams);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['pageResults'] = $this->Page_m->page_results($current_offset, $config['per_page'], $searchParams);
        
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('page/page_view',$data);

    }
    public function add_page()
    {
        $data['nestedView']['heading']="Add New Task";
		$data['nestedView']['cur_page'] = 'task';
		$data['nestedView']['parent_page'] = 'task';

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/page.js"></script>';

		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Add New Task';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Task','class'=>'','url'=>SITE_URL.'task');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Task','class'=>'active','url'=>'');
        # Additional data
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_page';
        $data['displayResults'] = 0;
        $this->load->view('page/page_view',$data);
    }

    public function insert_page()
    
    {
        $page_name = validate_string($this->input->post('page_name',TRUE));
        $page_data = array('name' => $page_name,'page_id'=>0);
        $unique = $this->Page_m->check_page_name_availability($page_data);
        if($unique>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> '.$page_name.' already exist! please check.</div>'); 
            redirect(SITE_URL.'task'); exit();
        }
        else
        {
            $data=array(
            'name'         => $page_name,
            'title'        => $this->input->post('title',TRUE),
            'status'       => 1,
            'created_by'   => $this->session->userdata('sso_id'),
            'created_time' => date('Y-m-d H:i:s')
            );

            $this->db->trans_begin();    
            $this->Common_model->insert_data('page',$data); 

            if ($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>'); 
               
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Task has been Added successfully!</div>');
                      
            }
        }
        redirect(SITE_URL.'task');
    }   

    public function edit_page()
    {
        $page_id=@storm_decode($this->uri->segment(2));
        if($page_id=='')
        {
            redirect(SITE_URL.'task');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Task";
		$data['nestedView']['cur_page'] = 'task';
		$data['nestedView']['parent_page'] = 'task';

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/page.js"></script>';

		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Edit Task';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Task','class'=>'','url'=>SITE_URL.'task');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Task','class'=>'active','url'=>'');       
        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_page';
        $data['displayResults'] = 0;
       
        # Data
        $row = $this->Common_model->get_data_row('page',array('page_id'=>$page_id));
        $data['lrow'] = $row;
        $this->load->view('page/page_view',$data);
    }
    public function update_page()
    {
        $page_id=validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($page_id==''){
            redirect(SITE_URL.'task');
            exit;
        }
        $page_name = validate_string($this->input->post('page_name',TRUE));
        $data = array('name'=>$page_name,'page_id'=>$page_id);
        $unique = $this->Page_m->check_page_name_availability($data);
        if($unique>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> '.$page_name.' already exist! please check.</div>'); 
            redirect(SITE_URL.'task'); exit();
        }
        else
        {
            // GETTING INPUT TEXT VALUES
            $data = array( 
            'name'          => $page_name,
            'title'         => $this->input->post('title',TRUE),
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s'),
            'status'        => 1
            );
            $where = array('page_id'=>$page_id);

            $this->db->trans_begin();
            $this->Common_model->update_data('page',$data,$where);

            if($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>');  
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Task has been updated successfully! </div>');
            }
        }
        redirect(SITE_URL.'task');
    }

    public function deactivate_page($encoded_id)
    {
        $page_id=@storm_decode($encoded_id);
        if($page_id==''){
            redirect(SITE_URL.'task');
            exit;
        }
        $where = array('page_id' => $page_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('page',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Task has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'task');

    }

    public function activate_page($encoded_id)
    {
        $page_id=@storm_decode($encoded_id);
        if($page_id==''){
            redirect(SITE_URL.'task');
            exit;
        }
        $where = array('page_id' => $page_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('page',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Task has been Activated successfully!
        </div>');
        redirect(SITE_URL.'task');
	}
    public function is_page_exist()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $page_id = validate_number($this->input->post('page_id',TRUE));
        $data = array('name'=>$name,'page_id'=>$page_id);
        $result = $this->Page_m->check_page_name_availability($data);
        echo $result;
    }
}