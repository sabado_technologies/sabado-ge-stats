<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Calibration extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Calibration_m');
	}
    // Srilekha
    public function calibration_list()
    {
        $data['nestedView']['heading']="Calibration Requests";
        $data['nestedView']['cur_page'] = 'calibration';
        $data['nestedView']['parent_page'] = 'calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Calibration Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Requests','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='')
        {
            $searchParams=array(
                'crn'          => validate_string($this->input->post('crn', TRUE)),
                'tool_no'      => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'    => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'whc_id'       => validate_number($this->input->post('wh_id',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        }
        else
        {
            if($this->input->post('reset')!='')
            {
                $searchParams=array(
                    'crn'          => '',
                    'tool_no'      => '',
                    'tool_desc'    => '',
                    'asset_number' => '',
                    'whc_id'       => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
            else if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'crn'          => $this->session->userdata('crn'),
                    'tool_no'      => $this->session->userdata('tool_no'),
                    'tool_desc'    => $this->session->userdata('tool_desc'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'crn'          => '',
                    'tool_no'      => '',
                    'tool_desc'    => '',
                    'asset_number' => '',
                    'whc_id'       => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        // Logic For Cart Functionality 
        if(isset($_POST['add']))
        {
            if(!isset($_SESSION['rc_asset_id']))
            {
                $_SESSION['rc_asset_id'] = array();
            }
            $a = count(@$_POST['asset_id']);
            for ($i=0; $i < $a; $i++)
            {   
                $_SESSION['rc_asset_id'][@$_POST['asset_id'][$i]] = @$_POST['asset_id'][$i];
                $_SESSION['rc_wh'][] = @$_POST['asset_wh'][$_POST['asset_id'][$i]];
            }
        }
       
        if(!isset($_POST['reset']) && !isset($_POST['searchcalibration']) && !isset($_POST['add']) && !isset($_POST['calibration']))
        {    
            if(isset($_SESSION['rc_first_url_count']))
            {     
                $_SESSION['rc_last_url_count'] = base_url(uri_string());
                if(strcmp($_SESSION['rc_first_url_count'],$_SESSION['rc_last_url_count']))
                {                
                }
                else
                {
                    if(isset($_SESSION['rc_asset_id'])){ unset($_SESSION['rc_asset_id']); }
                    if(isset($_SESSION['rc_wh'])){ unset($_SESSION['rc_wh']); }
                }
            }
            else
            {
                $_SESSION['rc_first_url_count'] = base_url(uri_string());
                if(isset($_SESSION['rc_asset_id'])){ unset($_SESSION['rc_asset_id']);}
                if(isset($_SESSION['rc_wh'])){ unset($_SESSION['rc_wh']); }
            }
        }

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'calibration/';
        # Total Records
        $config['total_rows'] = $this->Calibration_m->calibration_total_num_rows($searchParams,$task_access); 
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['calibration_results']=$calibration_results;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['displayResults'] = 1;
        $data['current_offset'] = $current_offset;
 
        $this->load->view('calibration/calibration_view',$data);
    }

    public function calibration_asset_details()
    {
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View Calibration Details";
        $data['nestedView']['cur_page'] = 'check_calibration';
        $data['nestedView']['parent_page'] = 'calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View Calibration Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Requests','class'=>'','url'=>SITE_URL.'calibration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View Calibration Details','class'=>'active','url'=>'');

        if(isset($_SESSION['rc_asset_id']))
        {
            $asset_data=array();
            foreach ($_SESSION['rc_asset_id'] as $rc_asset_id => $qty)
            {
                $result_data = $this->Calibration_m->get_rc_details($rc_asset_id);
                $rc_asset_id = $rc_asset_id;
                $asset_data[] = $result_data; 
            } 
            $data['assets']=$asset_data;
        }

        $country_id = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'country_id');

        #additional Data
        $data['calibration_supplier']=$this->Common_model->get_data('supplier',array('calibration'=>1,'status'=>1,'country_id'=>$country_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $data['form_action'] = SITE_URL.'insert_calibration_request';
       
        $this->load->view('calibration/calibration_asset_details',$data);
    }

    // Srilekha
    public function insert_calibration_request()
    {
        #page authentication
        $task_access = page_access_check('calibration');

        $rc_asset_id_arr=$this->input->post('rc_asset_id',TRUE);
        $rc_asset_id = $rc_asset_id_arr[0];
        $country_id = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'country_id');

        // To Check Whether All Assets Are from same Warehouse
        $warehouse_id=$this->input->post('wh_id',TRUE);
        $wh_id=$warehouse_id[0];
        $count=0;
        foreach ($warehouse_id as $row) 
        {
            if($wh_id !== $row)
            {
                $count++;
            }
        }
        if($count>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Selected Assets must be in same Inventory !</div>'); 
            redirect(SITE_URL.'calibration'); exit; 
        }
        // Add Supplier to the Calibration
        else
        {
            // To Generate Dynamic Number
            $number=get_rcb_number($country_id);
            if(count($number) == 0)
            {
                $f_order_number = "1-00001";
            }
            else
            {
               
                $num = ltrim($number['rcb_number'],"CRB");
                
                $result = explode("-", $num);
                $running_no = (int)$result[1];

                if($running_no == 99999)
                {
                    $first_num = $result[0]+1;
                    $second_num = get_running_sno_five_digit(1);  
                }
                else
                {
                    $first_num = $result[0];
                    $val = $running_no+1;
                    $second_num = get_running_sno_five_digit($val);
                }
                $f_order_number = $first_num.'-'.$second_num;
            }
            $order_number = 'CRB'.$f_order_number;
            $expected_delivery_date=validate_string(date('Y-m-d',strtotime($this->input->post('delivery_date',TRUE))));
            $expected_return_date=validate_string(date('Y-m-d',strtotime($this->input->post('return_date',TRUE))));
            $data=array(
            'supplier_id'               =>  validate_number($this->input->post('supplier_id',TRUE)),
            'rcb_number'                =>  $order_number,
            'contact_person'            =>  validate_string($this->input->post('contact_person',TRUE)),
            'wh_id'                     =>  $wh_id,
            'phone_number'              =>  validate_string($this->input->post('phone_number',TRUE)),
            'gst_number'                =>  validate_string($this->input->post('gst_number',TRUE)),
            'pan_number'                =>  validate_string($this->input->post('pan_number',TRUE)),
            'pin_code'                  =>  validate_string($this->input->post('zip_code',TRUE)),
            'expected_delivery_date'    =>  $expected_delivery_date,
            'expected_return_date'      =>  $expected_return_date,
            'country_id'                =>  $country_id,
            'rc_type'                   =>  2,
            'address1'                  =>  validate_string($this->input->post('address_1',TRUE)),
            'address2'                  =>  validate_string($this->input->post('address_2',TRUE)),
            'address3'                  =>  validate_string($this->input->post('address_3',TRUE)),
            'address4'                  =>  validate_string($this->input->post('address_4',TRUE)),
            'created_by'                =>  $this->session->userdata('sso_id'),
            'created_time'              =>  date('Y-m-d H:i:s'),
            'remarks'                   =>  validate_string($this->input->post('remarks',TRUE)),
            'status'                    =>  1
                       );
            $this->db->trans_begin();
            // Raise Order For Calibration
            $rc_order_id=$this->Common_model->insert_data('rc_order',$data);
            // Document Type Retrieving
            $document_type = $this->input->post('document_type',TRUE);
            foreach ($document_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $original_name    = $config['org_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                                    'document_type_id' => $value,
                                    'rc_order_id'      => $rc_order_id,
                                    'name'             => $support_document,
                                    'doc_name'         => $original_name,
                                    'created_by'       => $this->session->userdata('sso_id'),
                                    'created_time'     => date('Y-m-d H:i:s'),
                                    'status'           => 1
                                    );

                if($value!='' && $support_document != '')
                {
                    // Insert data into rc_doc
                    $this->Common_model->insert_data('rc_doc',$insert_doc);
                }
            }

            // Updates Assets in Order
            foreach($rc_asset_id_arr as $rc_asset_id)
            {
                
                $asset_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'asset_id');
                //echo $asset_id; exit;
                
                //update_rc_status_history
                $update_rcsh = array('modified_by'      =>$this->session->userdata('sso_id'),
                                     'modified_time'    =>date('Y-m-d H:i:s'));
                $update_rcsh_where = array('rc_asset_id'=>$rc_asset_id,'modified_time'=>NULL);
                $this->Common_model->update_data('rc_status_history',$update_rcsh,$update_rcsh_where);
                //insert_rc_status_history
                $insert_rcsh = array('rc_asset_id'      =>$rc_asset_id,
                                     'current_stage_id' =>12,
                                     'created_by'       =>$this->session->userdata('sso_id'),
                                     'created_time'     =>date('Y-m-d H:i:s'),
                                     'status'           =>1
                                     );
                $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

                //update_rc_asset
                $this->Common_model->update_data('rc_asset',array('current_stage_id'=>12,'rc_order_id'=>$rc_order_id),array('rc_asset_id'=>$rc_asset_id));
                // checking and closing the defective asset request if defective asset request raised after CR initialised
                // modified by maruthi on 26th April'17
                $defective_asset_data = $this->Common_model->get_data_row('defective_asset',array('asset_id'=>$asset_id,'status<'=>10));
                if(count(@$defective_asset_data) >0)
                {
                    $this->Common_model->update_data('defective_asset',array('request_handled_type'=>5,'status'=>10),array('defective_asset_id'=>$defective_asset_data['defective_asset_id']));
                    $this->Common_model->update_data('asset',array('approval_status'=>0),array('asset_id'=>$asset_id));
                }

                // Update Asset Status to In Calibration
                $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                #audit data
                $old_data = array('asset_status_id' => $asset_arr['status']);

                $update_asset_data=array(
                    'status'        =>  4,
                    'modified_time' =>  date('Y-m-d H:i:s'),
                    'modified_by'   =>  $this->session->userdata('sso_id')
                );

                $this->Common_model->update_data('asset',$update_asset_data,array('asset_id'=>$asset_id));

                // Update Asset_status_history
                $ash_id=$this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));

                #audit data
                $new_data = array('asset_status_id' => 4);
                $remarks = "Asset linked for calibration CRB:".$order_number;
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                //insert asset_status_history
                $ash_data=array(
                    'status'           => 4,
                    'asset_id'         => $asset_id,
                    'rc_asset_id'      => $rc_asset_id,
                    'current_stage_id' => 12,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                );
                // Insert data into asset_status_history
                $this->Common_model->insert_data('asset_status_history',$ash_data);
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'calibration'); exit; 
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> CRB Number :<strong>'.$order_number.'</strong> is intiated Successfully !</div>');
                redirect(SITE_URL.'calibration'); exit;
            }
            
        }
    }

    #delete CR Request
    public function delete_cr_request()
    {
        #page authentication
        $task_access = page_access_check('calibration');

        $rc_asset_id = storm_decode($this->uri->segment(2));
        if($rc_asset_id == '')
        {
            redirect(SITE_URL.'calibration'); exit();
        }
        $this->db->trans_begin();

        $rc_row = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));
        $asset_id = $rc_row['asset_id'];

        #update rc asset //cancelled
        $update_rca = array(
            'status'           => 10,
            'current_stage_id' => 28,
            'modified_by'      => $this->session->userdata('sso_id'),
            'modified_time'    => date('Y-m-d H:i:s')
        );
        $update_rca_where = array('rc_asset_id'=>$rc_asset_id);
        $this->Common_model->update_data('rc_asset',$update_rca,$update_rca_where);

        #update status only when out of calibration
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $flag = get_position_for_calibration($asset_id);
        if($flag == 1)
        {
            $new_status = 1;
        }
        else if($flag == 2)
        {
            $new_status = 3;
        }
        else if($flag == 4)
        {
            $new_status = 8;
        }
        else
        {
            $new_status = '';
        }
        if($asset_arr['status']==12 && $new_status !='')
        {
            #audit data
            $old_data = array('asset_status_id'=>$asset_arr['status']);
            $update_a = array(
                'status'              => $new_status,
                'approval_status'     => 0,
                'availability_status' => 1,
                'modified_by'         => $this->session->userdata('sso_id'),
                'modified_time'       => date('Y-m-d H:i:s')
            );
            $update_a_where = array('asset_id'=>$asset_id);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            #audit data
            $new_data = array('asset_status_id' => $new_status);
            $remarks = "Calibration:".$rc_row['rc_number']." req is cancelled";
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'calibration'); exit; 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> CR Number :<strong>'.$rc_row['rc_number'].'</strong> has been Deleted Successfully !</div>');
            redirect(SITE_URL.'calibration'); exit;
        }
    }

    #open requests of CR in Admin/superAdmin Page
    public function admin_calibration_list()
    {
        
        $data['nestedView']['heading']="Open Calibration Requests";
        $data['nestedView']['cur_page'] = 'open_calibration';
        $data['nestedView']['parent_page'] = 'open_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Open Calibration Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Calibration Requests','class'=>'active','url'=>'');

        # Search Functionality
        
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'   => validate_string($this->input->post('rcb_number', TRUE)),
                'whc_id'       => validate_number($this->input->post('wh_id',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                'rcb_number'   => '',
                'whc_id'       => '',
                'asset_number' => '',
                'rc_number'    => '',
                'country_id'   => '',
                'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'admin_calibration_list/';
        # Total Records

        $config['total_rows'] = $this->Calibration_m->admin_calibration_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->admin_calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Calibration_m->get_asset_details($value['rc_order_id']);
            $calibration_results[$key]['asset_list'] = $part_list;
        }
        
        # Additional data
        $data['calibration_results'] = $calibration_results;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;
        $data['displayResults'] = 1;
        $data['flg']=1;
        $this->load->view('calibration/admin_calibration_list',$data);
    }

    #view page of open CR requests in Admin/super Admin page
    public function view_admin_calibration()
    {
        $rc_order_id = storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'admin_calibration_list'); exit();
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View open Calibration Details";
        $data['nestedView']['cur_page'] = 'open_calibration';
        $data['nestedView']['parent_page'] = 'open_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View Open Calibration Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Calibration Request','class'=>'','url'=>SITE_URL.'admin_calibration_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View open Calibration Details','class'=>'active','url'=>'');

        #additional Data
        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Calibration_m->get_asset_details($rc_order_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));

        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['attach_document'] = $this->Calibration_m->get_documents_by_stage($rc_order_id);
        $data['rcb_number'] = $cal_row['rcb_number'];
        $data['cal_row']=$cal_row;

        $this->load->view('calibration/view_admin_calibration_list',$data);
    }

    public function edit_admin_calibration()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'admin_calibration_list'); exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Update Calibration Details";
        $data['nestedView']['cur_page'] = 'check_calibration';
        $data['nestedView']['parent_page'] = 'open_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Calibration Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Calibration Requests','class'=>'','url'=>SITE_URL.'admin_calibration_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Calibration Details','class'=>'active','url'=>'');


       
        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $country_id = $cal_row['country_id'];
        $data['cal_row'] = $cal_row;
        $data['calibration_supplier']=$this->Common_model->get_data('supplier',array('status'=>1,'country_id'=>$country_id));
        $data['asset_details']=$this->Calibration_m->get_asset_details($rc_order_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $attach_document = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['rc_order_id']=$rc_order_id;
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document; 
        $data['flg']=2;
        $data['form_action'] = SITE_URL.'update_admin_calibration';
        $this->load->view('calibration/admin_calibration_list',$data);
    }

    public function update_admin_calibration()
    {
        #page authentication
        $task_access = page_access_check('open_calibration');

        $rc_order_id = validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));

        $rcb_number = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $expected_delivery_date=validate_string(date('Y-m-d',strtotime($this->input->post('delivery_date',TRUE))));
        $expected_return_date=validate_string(date('Y-m-d',strtotime($this->input->post('return_date',TRUE))));
        $data=array(
        'supplier_id'            => validate_number($this->input->post('supplier_id',TRUE)),
        'contact_person'         => validate_string($this->input->post('contact_person',TRUE)),
        'phone_number'           => validate_string($this->input->post('phone_number',TRUE)),
        'gst_number'             => validate_string($this->input->post('gst_number',TRUE)),
        'pan_number'             => validate_string($this->input->post('pan_number',TRUE)),
        'pin_code'               => validate_string($this->input->post('zip_code',TRUE)),
        'expected_delivery_date' => $expected_delivery_date,
        'expected_return_date'   => $expected_return_date,
        'rc_type'                => 2,
        'address1'               => validate_string($this->input->post('address_1',TRUE)),
        'address2'               => validate_string($this->input->post('address_2',TRUE)),
        'address3'               => validate_string($this->input->post('address_3',TRUE)),
        'address4'               => validate_string($this->input->post('address_4',TRUE)),
        'modified_by'            => $this->session->userdata('sso_id'),
        'modified_time'          => date('Y-m-d H:i:s'),
        'remarks'                => validate_string($this->input->post('remarks',TRUE)),
        'status'                 => 1
                   );
        $this->db->trans_begin();
        // Update Calibration data
        $this->Common_model->update_data('rc_order',$data,array('rc_order_id'=>$rc_order_id));
        // Document Type Retrieving
        $document_type = $this->input->post('document_type',TRUE);
        foreach ($document_type as $key => $value) 
        {
            if($_FILES['support_document_'.$key]['name']!='')
            {
                $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                $config['upload_path'] = asset_document_path();
                $config['allowed_types'] = 'gif|jpg|png|pdf';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('support_document_'.$key);
                $fileData = $this->upload->data();
                $support_document = $config['file_name'];
                $original_name    = $config['org_name'];
            }
            else
            {
                $support_document = '';
            }
            $insert_doc = array(
                                'document_type_id' => $value,
                                'rc_order_id'      => $rc_order_id,
                                'name'             => $support_document,
                                'doc_name'         => $original_name,
                                'created_by'       => $this->session->userdata('sso_id'),
                                'created_time'     => date('Y-m-d H:i:s'),
                                'status'           => 1
                                );
            if($value!='' && $support_document != '')
            {
                // Insert data into calibration_doc
                $this->Common_model->insert_data('rc_doc',$insert_doc);
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'admin_calibration_list');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> CRB :<strong>'.$rcb_number.'</strong> Details has been updated Successfully !
            </div>');
            redirect(SITE_URL.'admin_calibration_list');
        }
    }

    public function unlink_cal_scanned_asset_individually()
    {
        $rc_order_id = storm_decode($this->uri->segment(2));
        $rc_asset_id = storm_decode($this->uri->segment(3));
        if($rc_order_id == '' || $rc_asset_id=='')
        {
            redirect(SITE_URL.'wh_calibration_wizard/'.storm_encode($rc_order_id)); exit();
        }
        unset($_SESSION['scanned_assets_list'][$rc_asset_id]);
        $rc_asset_number = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_number');
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Assets has been unlinked for CR Number : <strong>'.$rc_asset_number.'</strong> !
        </div>'); 
            
        redirect(SITE_URL.'wh_calibration_wizard/'.storm_encode($rc_order_id)); exit();
    }

    public function remove_asset()
    {
        $rc_asset_id=validate_number($this->input->post('rc_asset_id',TRUE));
        $rc_arr = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));

        $res=$this->Common_model->update_data('rc_asset',array('current_stage_id'=>11,'rc_order_id'=>NULL),array('rc_asset_id'=>$rc_asset_id));

        $this->Common_model->delete_data('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>12));

        $update_rc_status_history = array('modified_time'=>NULL);
        $update_rcsh_where = array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>11);
        $this->Common_model->update_data('rc_status_history',$update_rc_status_history,$update_rcsh_where);

        $asset_id = $rc_arr['asset_id'];
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

        #audit data
        $old_data = array('asset_status_id' => $asset_arr['status']);

        $cal_due_date = $asset_arr['cal_due_date'];
        $current_date = date('Y-m-d');
        if($cal_due_date <= $current_date)
        {
            $status = 12;
        }
        else
        {
            $status = 1;
        }
        $update_asset_data=array(
            'status'        =>  $status,
            'modified_time' =>  date('Y-m-d H:i:s'),
            'modified_by'   =>  $this->session->userdata('sso_id')
        );
        $this->Common_model->update_data('asset',$update_asset_data,array('asset_id'=>$asset_id));

        #audit data
        $new_data = array('asset_status_id' => $status);
        $crb_no = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_arr['rc_order_id']),'rcb_number');
        $remarks = "Removed from CRB No:".$crb_no;
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function remove_admin_calibration()
    {
        #page authentication
        $task_access = page_access_check('open_calibration');

        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'admin_calibration_list'); exit;
        }
        $rc_asset_id=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
        $rcb_number = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $this->db->trans_begin();
        foreach($rc_asset_id as $row)
        {
            $rc_arr = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$row['rc_asset_id']));
            $this->Common_model->update_data('rc_asset',array('current_stage_id'=>11,'rc_order_id'=>NULL),array('rc_asset_id'=>$row['rc_asset_id'],'current_stage_id'=>12));

            $this->Common_model->delete_data('rc_status_history',array('rc_asset_id'=>$row['rc_asset_id'],'current_stage_id'=>12));

            $update_rc_status_history = array('modified_time'=>NULL);
            $update_rcsh_where = array('rc_asset_id'=>$row['rc_asset_id'],'current_stage_id'=>11);
            $this->Common_model->update_data('rc_status_history',$update_rc_status_history,$update_rcsh_where);

            $asset_id = $rc_arr['asset_id'];
            $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

            #audit data
            $old_data = array('asset_status_id' => $asset_arr['status']);

            $cal_due_date = $asset_arr['cal_due_date'];
            $current_date = date('Y-m-d');
            if($cal_due_date <= $current_date)
            {
                $status = 12;
            }
            else
            {
                $status = 1;
            }
            $update_asset_data=array(
                'status'        =>  $status,
                'modified_time' =>  date('Y-m-d H:i:s'),
                'modified_by'   =>  $this->session->userdata('sso_id')
            );
            $this->Common_model->update_data('asset',$update_asset_data,array('asset_id'=>$asset_id));

            #audit data
            $new_data = array('asset_status_id' => $status);
            $crb_no = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_arr['rc_order_id']),'rcb_number');
            $remarks = "Cancelled CRB No:".$crb_no." Request";
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$row['rc_asset_id'],$asset_arr['country_id']);

        }
        $this->Common_model->update_data('rc_order',array('status'=>4),array('rc_order_id'=>$rc_order_id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> CRB Number :<strong>'.$rcb_number.'</strong> has been Cancelled Successfully !
            </div>');
        }
        redirect(SITE_URL.'admin_calibration_list');
    }

    public function edit_open_calibration()
    {
        $rc_asset_id=@storm_decode($this->uri->segment(2));
        if($rc_asset_id == '' )
        {
            redirect(SITE_URL.'admin_calibration_list'); exit;
        }
        $rc_arr = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));
        $rc_order_id = $rc_arr['rc_order_id'];
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Update Calibration Details";
        $data['nestedView']['cur_page'] = 'check_calibration';
        $data['nestedView']['parent_page'] = 'open_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Calibration Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Calibration Requests','class'=>'','url'=>SITE_URL.'admin_calibration_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Calibration Details','class'=>'active','url'=>'');

        $country_id = $rc_arr['country_id'];
        $asset_id = $rc_arr['asset_id'];
        $access_status = $this->Common_model->get_value('location_pref',array('location_id'=>$country_id,'pref_id'=>1),'status');

        if($access_status == 2)//include qa step
        {
            $rcsh_id = $this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>14,'status'=>1),'rcsh_id');
            $rcah_id = $this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id),'rcah_id');
            $asset_health = $this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
            $selected_asset_health = array();
            foreach($asset_health as $row)
            {
                $selected_asset_health[$row['part_id']]['asset_condition_id']=$row['asset_condition_id'];
                $selected_asset_health[$row['part_id']]['remarks']=$row['remarks'];
            }
            $data['selected_asset_health']=$selected_asset_health;
            
            $data['current_stage_id'] = $rc_arr['current_stage_id'];
            if($data['current_stage_id']==16)
            {
                $qas = $this->Calibration_m->get_latest_qa_reason($rc_asset_id);
                $qa_status_id = @$qas['qa_status_id'];
                if($qa_status_id!='')
                {
                    $data['qa_reason']=$this->Common_model->get_value('qa_status',array('qa_status_id'=>$qa_status_id),'name');
                }
                $data['qa_status_id'] = $qa_status_id;
            }
            
        }
        else//exclude qa step
        {
            $rcsh_id = $this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>16,'status'=>1),'rcsh_id');
            $rcah_id = $this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id),'rcah_id');
            $asset_health = $this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
            $selected_asset_health = array();
            foreach($asset_health as $row)
            {
                $selected_asset_health[$row['part_id']]['asset_condition_id']=$row['asset_condition_id'];
                $selected_asset_health[$row['part_id']]['remarks']=$row['remarks'];
            }
            $data['selected_asset_health'] = $selected_asset_health;
            $data['current_stage_id'] = 14;//masking status 16 with 14
        }

        $data['cal_row'] = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $asset_details = $this->Calibration_m->get_asset_details_row($rc_order_id,$asset_id);
        $part_list = $this->Calibration_m->get_asset_part_details($asset_details['asset_id']);
        $asset_details['asset_health_list'] = $part_list;
        $data['asset_details'] = $asset_details;
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['attach_document'] = $this->Common_model->get_data('rca_doc',array('rc_asset_id'=>$rc_asset_id));
        $data['rc_order_id'] = $rc_order_id;
        $data['rc_asset_id'] = $rc_asset_id;
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_calibration_request';
        $this->load->view('calibration/edit_calibration_request',$data);
    }
    // Srilekha
    public function update_calibration_request()
    {
        #page authentication
        $task_access = page_access_check('open_calibration');

        $rc_order_id=validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        $rc_asset_id=validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        if($rc_order_id=='' || $rc_asset_id=='')
        {
            redirect(SITE_URL.'admin_calibration_list'); exit();
        }

        $rc_arr = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));
        $asset_id = $rc_arr['asset_id'];
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $rc_number = $rc_arr['rc_number'];
        $country_id = $rc_arr['country_id'];
        
        $this->db->trans_begin();
        $document_type = $this->input->post('document_type',TRUE);

        if($document_type[1]!='')
        {
            foreach ($document_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $original_name    = $config['org_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'rc_asset_id'      => $rc_asset_id,
                    'doc_name'         => $original_name,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );

                if($value!='' && $support_document!='')
                {
                    // Insert Documents into Calibration_doc
                    $this->Common_model->insert_data('rca_doc',$insert_doc);
                }
            }
        }

        $access_status = $this->Common_model->get_value('location_pref',array('location_id'=>$country_id,'pref_id'=>1),'status');

        if($access_status == 2)//include qa step
        {
            $current_stage_id = validate_number($this->input->post('current_stage_id',TRUE));
            if($current_stage_id == 14)
            {
                $cal_status=validate_string($this->input->post('status',TRUE));

                $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>14,'modified_time'=>NULL),'rcsh_id');

                $rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id),'rcah_id');

                $asset_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'asset_id');

                // If admin Selects Calibrated
                if($cal_status == 1)
                {
                    $calibrated_date=validate_string(date('Y-m-d',strtotime($this->input->post('calibrated_date',TRUE))));
                    $due_date=validate_string(date('Y-m-d',strtotime($this->input->post('due_date',TRUE))));
                    $remarks=validate_string($this->input->post('remarks',TRUE));
                    
                    #audit_data
                    $old_data = array(
                        'cal_due_date'    => date('d-m-Y',strtotime($asset_arr['cal_due_date'])),
                        'calibrated_date' => date('d-m-Y',strtotime($asset_arr['calibrated_date']))
                    );
                    //Update in Asset 
                    $update_a = array(
                        'cal_due_date'    => $due_date,
                        'calibrated_date' => $calibrated_date,
                        'modified_by'     => $this->session->userdata('sso_id'),
                        'modified_time'   => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id' => $asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    #audit data
                    $new_data = array(
                        'cal_due_date'    => date('d-m-Y',strtotime($due_date)),
                        'calibrated_date' => date('d-m-Y',strtotime($calibrated_date))
                    );
                    $remarks = "Route for QA Approval, CR No:".$rc_arr['rc_number'];
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');

                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));

                    // Insert into asset status history
                    $ash_arr=array(
                        'status'        =>  4,
                        'asset_id'      =>  $asset_id,
                        'created_by'    =>  $this->session->userdata('sso_id'),
                        'created_time'  =>  date('Y-m-d H:i:s'),
                        'rc_asset_id'   =>  $rc_asset_id,
                        'current_stage_id' => 15
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);

                    #insert cal due date history
                    $cal_due_date = array(
                        'rc_asset_id'       =>  $rc_asset_id,
                        'calibrated_date'   =>  $calibrated_date,
                        'cal_due_date'      =>  $due_date,
                        'asset_id'          =>  $asset_id,
                        'created_by'        =>  $this->session->userdata('sso_id'),
                        'created_time'      =>  date('Y-m-d H:i:s')
                    );
                    $this->Common_model->insert_data('cal_due_date_history',$cal_due_date); 

                    #update rc_asset
                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>15,'remarks'=>$remarks),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));

                    $rcsh_data=array(
                        'rc_asset_id'       =>  $rc_asset_id,   
                        'current_stage_id'  =>  15,
                        'created_by'        =>  $this->session->userdata('sso_id'),
                        'created_time'      =>  date('Y-m-d H:i:s'),
                        'status'            =>  1
                    );
                    $new_rcsh_id = $this->Common_model->insert_data('rc_status_history',$rcsh_data);
                    
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> Calibration Details  has been Updated successfully For CR Number : <strong>'.$rc_number.'</strong> !
                        </div>');
                        redirect(SITE_URL.'admin_calibration_list'); exit;
                    }
                }
                // If Admin Select Not Calibrated
                if($cal_status == 2)
                {
                    $repair=validate_number($this->input->post('repair',TRUE));
                    // Sent for repair
                    if(@$repair == 1)
                    {
                        $this->Common_model->update_data('rc_status_history',array('status'=>2),array('rcsh_id'=>$rcsh_id));
                        $this->Common_model->update_data('rc_asset',array('status'=>2),array('rc_asset_id'=>$rc_asset_id));

                        // Dynamic CRB Number generation
                        $number=get_repair_rc_number($country_id);
                        if(count($number) == 0)
                        {
                            $f_order_number = "1-00001";
                        }
                        else
                        {
                            $num = ltrim($number['rc_number'],"R");
                            $result = explode("-", $num);
                            $running_no = (int)$result[1];

                            if($running_no == 99999)
                            {
                                $first_num = $result[0]+1;
                                $second_num = get_running_sno_five_digit(1);  
                            }
                            else
                            {
                                $first_num = $result[0];
                                $val = $running_no+1;
                                $second_num = get_running_sno_five_digit($val);
                            }
                            $f_order_number= $first_num.'-'.$second_num;
                        }
                        $order_number = 'R'.$f_order_number;
                        $rc_asset_data=array(
                            'asset_id'         => $asset_id,
                            'current_stage_id' => 20,
                            'rc_number'        => $order_number,
                            'country_id'       => $country_id,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'status'           => 1,
                            'rca_type'         => 1,
                            'cal_rc_asset_id'  => $rc_asset_id
                        );
                        // Send From Calibration to Repair
                        $repair_rc_asset_id=$this->Common_model->insert_data('rc_asset',$rc_asset_data);

                        $rc_status_data=array(
                            'rc_asset_id'      => $repair_rc_asset_id,
                            'current_stage_id' => 20,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'status'           => 1
                        );
                        $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rc_status_data);

                        $rc_asset=array(
                            'rc_asset_id'  => $repair_rc_asset_id,
                            'rcsh_id'      => $new_rcsh_id,
                            'created_by'   => $this->session->userdata('sso_id'),
                            'created_time' => date('Y-m-d H:i:s'),
                            'status'       => 1
                        );
                        $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rc_asset);

                        // Fetch Asset Health
                        $rc_asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
                        foreach($rc_asset_health as $row)
                        {
                            $health_data=array(
                            'part_id'            => $row['part_id'],
                            'asset_condition_id' => $row['asset_condition_id'],
                            'remarks'            => $row['remarks'],
                            'created_by'         => $this->session->userdata('sso_id'),
                            'created_time'       => date('Y-m-d H:i:s'), 
                            'rcah_id'            => $new_rcah_id
                            );
                            $this->Common_model->insert_data('rc_asset_health',$health_data);
                        }

                        #audit data
                        $old_data = array(
                            'asset_status_id'     => $asset_arr['status'],
                            'availability_status' => $asset_arr['availability_status']
                        );
                        #update asset 
                        $update_a = array(
                            'status'              => 10,
                            'availability_status' => 2,
                            'modified_by'         => $this->session->userdata('sso_id'),
                            'modified_time'       => date('Y-m-d H:i:s'));
                        $update_a_where = array('asset_id' => $asset_id);
                        $this->Common_model->update_data('asset',$update_a,$update_a_where);

                        #audit data
                        $new_data = array(
                            'asset_status_id'     => 10,
                            'availability_status' => 2
                        );
                        $remarks = "From Cal CR:".$rc_arr['rc_number']." Requested for Repair RR:".$order_number;
                        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$repair_rc_asset_id,$asset_arr['country_id']);

                        $update_ash = array(
                            'end_time'    => date('Y-m-d H:i:s'),
                            'modified_by' => $this->session->userdata('sso_id')
                        );
                        $update_ash_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                        $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                        $asset_status=array(
                            'asset_id'         => $asset_id,
                            'rc_asset_id'      => $repair_rc_asset_id,
                            'current_stage_id' => 20,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'status'           => 10  
                        );
                        $this->Common_model->insert_data('asset_status_history',$asset_status);
                        
                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-times-circle"></i></div>
                            <strong>Error!</strong> something went wrong! please check.</div>'); 
                            redirect(SITE_URL.'admin_calibration_list');  exit;
                        }
                        else
                        {
                            $this->db->trans_commit();
                            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-check"></i></div>
                            <strong>Success!</strong> Repair Process is Successfully Initiated With  RR Number :<strong>'.$order_number.'</strong> ! </div>');
                            redirect(SITE_URL.'admin_calibration_list'); exit;
                        }
                    }
                    // Sent For Recalibration
                    if(@$repair == 2)
                    {
                        // Dynamic CRB number Generation
                        $number=get_rc_number($country_id);
                        if(count($number) == 0)
                        {
                            $f_order_number = "1-00001";
                        }
                        else
                        {
                            //echo $number['rcb_number'];exit;
                            $num = ltrim($number['rc_number'],"C");
                            //echo $num;exit;
                            $result = explode("-", $num);
                            $running_no = (int)$result[1];

                            if($running_no == 99999)
                            {
                                $first_num = $result[0]+1;
                                $second_num = get_running_sno_five_digit(1);  
                            }
                            else
                            {
                                $first_num = $result[0];
                                $val = $running_no+1;
                                $second_num = get_running_sno_five_digit($val);
                            }
                            $f_order_number = $first_num.'-'.$second_num;
                        }
                        $order_number = 'C'.$f_order_number;

                        $remarks='Sent For Recalibration with CR Number '.$order_number.'!';

                        $this->Common_model->update_data('rc_asset',array('current_stage_id'=>19,'status'=>10,'remarks'=>$remarks,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                        $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));

                        $insert_rcsh = array(
                                'rc_asset_id'      => $rc_asset_id,
                                'current_stage_id' => 19,
                                'created_by'       => $this->session->userdata('sso_id'),
                                'created_time'     => date('Y-m-d H:i:s'),
                                'status'           => 1,
                                'remarks'          => $remarks);
                        $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

                        $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                        $this->Common_model->update_data('asset_status_history',
                            array('end_time'=>date('Y-m-d H:i:s'),
                                  'modified_by'=>$this->session->userdata('sso_id')),
                            array('ash_id'=>$ash_id));

                        #audit data
                        $old_data = array('asset_status_id' => $asset_arr['status']);
                        #update asset
                        $update_a = array(
                            'status'              => 1,
                            'approval_status'     => 0,
                            'availability_status' => 1,
                            'modified_by'         => $this->session->userdata('sso_id'),
                            'modified_time'       => date('Y-m-d H:i:s')
                        );
                        $update_a_where = array('asset_id'=>$asset_id);
                        $this->Common_model->update_data('asset',$update_a,$update_a_where);
                        #audit data
                        $new_data = array('asset_status_id' => 1);
                        $remarks = "From CR:".$rc_arr['rc_number']." Initiated Recalibration with CR:".$order_number;
                        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                        $ash_arr=array(
                            'status'           => 1,
                            'asset_id'         => $asset_id,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'rc_asset_id'      => $rc_asset_id,
                            'current_stage_id' => 19
                        );
                        $this->Common_model->insert_data('asset_status_history',$ash_arr);

                        #re-calibration entry
                        $rc_asset_data=array(
                            'asset_id'         =>  $asset_id,
                            'current_stage_id' =>  11,
                            'rc_number'        =>  $order_number,
                            'country_id'       =>  $country_id,
                            'created_by'       =>  $this->session->userdata('sso_id'),
                            'created_time'     =>  date('Y-m-d H:i:s'),
                            'status'           =>  1,
                            'rca_type'         =>  2,
                            'cal_rc_asset_id'  =>  $rc_asset_id
                                            );
                        // Send For Recalibration With same asset
                        $new_rc_asset_id=$this->Common_model->insert_data('rc_asset',$rc_asset_data);

                        $rcsh_data=array(
                            'rc_asset_id'       =>  $new_rc_asset_id,   
                            'current_stage_id'  =>  11,
                            'created_by'        =>  $this->session->userdata('sso_id'),
                            'created_time'      =>  date('Y-m-d H:i:s'),
                            'status'            =>  1
                                            );
                        $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);
                        
                        $rcah_data=array(
                            'rc_asset_id'       =>  $new_rc_asset_id,
                            'rcsh_id'           =>  $new_rcsh_id,
                            'created_by'        =>  $this->session->userdata('sso_id'),
                            'created_time'      =>  date('Y-m-d H:i:s'),
                            'status'            =>  1
                                       );
                        $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rcah_data);
                        
                        // Fetch Asset Health
                        $rc_asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
                        foreach($rc_asset_health as $row)
                        {
                            $health_data=array(
                                'part_id'           => $row['part_id'],
                                'asset_condition_id'=> $row['asset_condition_id'],
                                'created_by'        => $this->session->userdata('sso_id'),
                                'created_time'      => date('Y-m-d H:i:s'), 
                                'rcah_id'           => $new_rcah_id         
                            );
                            $this->Common_model->insert_data('rc_asset_health',$health_data);
                        }

                        $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                        $this->Common_model->update_data('asset_status_history',
                            array('end_time'=>date('Y-m-d H:i:s'),
                                  'modified_by'=>$this->session->userdata('sso_id')),
                            array('ash_id'=>$ash_id));
                        $ash_arr=array(
                            'status'           => 10,
                            'asset_id'         => $asset_id,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'rc_asset_id'      => $new_rc_asset_id,
                            'current_stage_id' => 20
                        );
                        $this->Common_model->insert_data('asset_status_history',$ash_arr);
                       
                        // Close Rc Order if all assets are calibrated
                        $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                        $check_closed = 0;
                        foreach ($rc_asset_data as $key => $value) 
                        {
                            if($value['status']!=10)
                            {
                                $check_closed++;
                            }
                        }
                        if($check_closed == 0)
                        {
                           $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                        }
                        
                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-times-circle"></i></div>
                            <strong>Error!</strong> something went wrong! please check.</div>'); 
                            redirect(SITE_URL.'admin_calibration_list');  exit;
                        }
                        else
                        {
                            $this->db->trans_commit();
                            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-check"></i></div>
                            <strong>Success!</strong> ' .$rc_number .' has been successfully Sent For Recalibration With CR Number : <strong>'.$order_number.'</strong> !</div>');
                            redirect(SITE_URL.'admin_calibration_list');  exit;
                        }
                    }
                    // Sent for Scrap
                    if($repair == 3)
                    {
                        $this->Common_model->update_data('rc_asset',array('current_stage_id'=>18,'status'=>10,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                        $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d')),array('rcsh_id'=>$rcsh_id));

                        $rcsh_data=array(
                            'rc_asset_id'      => $rc_asset_id,   
                            'current_stage_id' => 18,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'status'           => 1
                        );
                        $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                        // Close Rc Order if all assets are calibrated
                        $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                        $check_closed = 0;
                        foreach ($rc_asset_data as $key => $value) 
                        {
                            if($value['status']!=10)
                            {
                                $check_closed++;
                            }
                        }
                        if($check_closed == 0)
                        {
                           $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                        }

                        #audit data
                        $old_data = array(
                            'asset_status_id'     => $asset_arr['status'],
                            'availability_status' => $asset_arr['availability_status'],
                            'tool_availability'   => get_asset_position($asset_id)
                        );

                        // Update Asset as Scrap
                        $update_a = array(
                            'status'              => 11,
                            'approval_status'     => 0,
                            'availability_status' => 2,
                            'modified_by'         => $this->session->userdata('sso_id'),
                            'modified_time'       => date('Y-m-d H:i:s')
                        );
                        $update_a_where = array('asset_id'=>$asset_id);
                        $this->Common_model->update_data('asset',$update_a,$update_a_where);
                        #audit data
                        $new_data = array(
                            'asset_status_id'     => 11,
                            'availability_status' => 2,
                            'tool_availability'   => get_asset_position($asset_id)
                        );
                        $remarks = "From Cal CR:".$rc_arr['rc_number']." Changed to Scraped";
                        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                        $ash_id=$this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                        
                        $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));

                        $ash_arr=array(
                            'status'           => 11,
                            'asset_id'         => $asset_id,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'rc_asset_id'      => $rc_asset_id,
                            'current_stage_id' => 18
                        );
                        $this->Common_model->insert_data('asset_status_history',$ash_arr);
                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-times-circle"></i></div>
                            <strong>Error!</strong> something went wrong! please check.</div>'); 
                            redirect(SITE_URL.'admin_calibration_list');  exit;
                        }
                        else
                        {
                            $this->db->trans_commit();
                            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-check"></i></div>
                            <strong>Success!</strong> CR Number :<strong>'.$rc_number .'</strong> has been Scraped Successfully !
                            </div>');
                            redirect(SITE_URL.'admin_calibration_list'); exit;
                        }
                    }
                    // Out of Tolerance
                    if($repair == 4)
                    {
                        $this->Common_model->update_data('rc_asset',array('current_stage_id'=>27,'status'=>10,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                        $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));

                        $rcsh_data=array(
                            'rc_asset_id'      => $rc_asset_id,   
                            'current_stage_id' => 27,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'status'           => 1
                        );
                        $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                        #Update Asset as Out of Tolerence
                        #audit data
                        $old_data = array(
                            'asset_status_id'     => $asset_arr['status'],
                            'availability_status' => $asset_arr['availability_status'],
                            'tool_availability'   => get_asset_position($asset_id)
                        );

                        $update_a = array(
                            'status'              => 9,
                            'approval_status'     => 0,
                            'availability_status' => 2,
                            'modified_by'         => $this->session->userdata('sso_id'),
                            'modified_time'       => date('Y-m-d H:i:s')
                        );
                        $update_a_where = array('asset_id'=>$asset_id);
                        $this->Common_model->update_data('asset',$update_a,$update_a_where);
                        #audit data
                        $new_data = array(
                            'asset_status_id'     => 9,
                            'availability_status' => 2,
                            'tool_availability'   => get_asset_position($asset_id)
                        );
                        $remarks = "From Cal CR:".$rc_arr['rc_number']." Changed to Out of Tolerance";
                        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                        $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                        
                        $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));
                        $ash_arr=array(
                            'status'       => 9,
                            'asset_id'     => $asset_id,
                            'created_by'   => $this->session->userdata('sso_id'),
                            'created_time' => date('Y-m-d H:i:s'),
                            'rc_asset_id'  => $rc_asset_id
                        );
                        $this->Common_model->insert_data('asset_status_history',$ash_arr);
                        
                        // Close Rc Order if all assets are calibrated
                        $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                        $check_closed = 0;
                        foreach ($rc_asset_data as $key => $value) 
                        {
                            if($value['status']!=10)
                            {
                                $check_closed++;
                            }
                        }
                        if($check_closed == 0)
                        {
                           $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                        }

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-times-circle"></i></div>
                            <strong>Error!</strong> something went wrong! please check.</div>'); 
                            redirect(SITE_URL.'admin_calibration_list');  exit;
                        }
                        else
                        {
                            $this->db->trans_commit();
                            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-check"></i></div>
                            <strong>Success!</strong> CR Number : <strong>'.$rc_number.'</strong> has been Closed successfully and Asset Status is changed to <strong> Out of Tolerance</strong> !</div>');
                            redirect(SITE_URL.'admin_calibration_list'); exit;
                        }
                    }
                }
            }
            else if($current_stage_id == 16) 
            {
                $admin_status=validate_number($this->input->post('cal_status',TRUE));
                $remarks=validate_string($this->input->post('remarks',TRUE));

                $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>16,'modified_time'=>NULL),'rcsh_id');

                $qas = $this->Calibration_m->get_latest_qa_reason($rc_asset_id);
                $qa_status_id = $qas['qa_status_id'];

                // If Successfully Calibrated
                if($qa_status_id == '')
                {
                    #audit data
                    $old_data = array('asset_status_id' => $asset_arr['status']);

                    #update asset
                    $update_a = array(
                        'status'              => 1,
                        'approval_status'     => 0,
                        'availability_status' => 1,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    #audit data
                    $new_data = array('asset_status_id' => 1);
                    $remarks = "Asset successfully Calibrated, For CR:".$rc_arr['rc_number'];
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));
                    $ash_arr=array(
                        'status'       => 1,
                        'asset_id'     => $asset_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s')
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);

                    // Fetch Asset Health Records 
                    $health_rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>14),'rcsh_id');
                    $health_rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$health_rcsh_id),'rcah_id');
            
                    $asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$health_rcah_id));

                    // Update Part status
                    foreach($asset_health as $row)
                    {
                        $this->Common_model->update_data('part',array('status'=>1),array('part_id'=>$row['part_id']));
                    }

                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>17,'status'=>10,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));
                    
                    $rcsh_data=array(
                        'rc_asset_id'      => $rc_asset_id,   
                        'current_stage_id' => 17,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);
                    
                    // Close Rc Order if all assets are calibrated
                    $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                    $check_closed = 0;
                    foreach ($rc_asset_data as $key => $value) 
                    {
                        if($value['status']!=10)
                        {
                            $check_closed++;
                        }
                    }
                    if($check_closed == 0)
                    {
                       $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                    }
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> CR Number : <strong>'.$rc_number.'</strong> has Closed Successfully with Calibration Status : <strong>Calibrated</strong> !</div>');
                        redirect(SITE_URL.'admin_calibration_list');
                    }
                }
                // Reroute to QA
                else if($admin_status == 1)
                {
                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>15),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d'),'remarks'=>'reroute to QA'),array('rcsh_id'=>$rcsh_id));

                    $rcsh_data=array(
                        'rc_asset_id'      => $rc_asset_id,   
                        'current_stage_id' => 15,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');

                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));
                    $ash_arr=array(
                        'status'       => 4,
                        'asset_id'     => $asset_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'rc_asset_id'  => $rc_asset_id,
                        'current_stage_id' => 15
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> CR Number : <strong>'.$rc_number.'</strong> is Rerouted to QA For Calibration Approval !
                        </div>');
                        redirect(SITE_URL.'admin_calibration_list');
                    }
                }
                // Sent For Repair
                else if($admin_status == 2)
                {
                    // Fetch Asset Health Records 
                    $health_rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>14,'status'=>1),'rcsh_id');
                    $health_rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$health_rcsh_id),'rcah_id');
                    $rc_asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$health_rcah_id));

                    $this->Common_model->update_data('rc_asset',array('status'=>2),array('rc_asset_id'=>$rc_asset_id));
                    
                    // Generate Dynamic RR number
                    $number=get_repair_rc_number($country_id);
                    if(count($number) == 0)
                    {
                        $f_order_number = "1-00001";
                    }
                    else
                    {
                        //echo $number['rcb_number'];exit;
                        $num = ltrim($number['rc_number'],"R");
                        //echo $num;exit;
                        $result = explode("-", $num);
                        $running_no = (int)$result[1];

                        if($running_no == 99999)
                        {
                            $first_num = $result[0]+1;
                            $second_num = get_running_sno_five_digit(1);  
                        }
                        else
                        {
                            $first_num = $result[0];
                            $val = $running_no+1;
                            $second_num = get_running_sno_five_digit($val);
                        }
                        $f_order_number= $first_num.'-'.$second_num;
                    }
                    $order_number = 'R'.$f_order_number;
                    // Send Asset to Repair
                    $rc_asset_data=array(
                        'asset_id'         => $asset_id,
                        'current_stage_id' => 20,
                        'rc_number'        => $order_number,
                        'country_id'       => $country_id,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1,
                        'rca_type'         => 1,
                        'cal_rc_asset_id'  => $rc_asset_id
                    );
                    $repair_rc_asset_id=$this->Common_model->insert_data('rc_asset',$rc_asset_data);
                    

                    $rc_status_data=array(
                        'rc_asset_id'      => $repair_rc_asset_id,
                        'current_stage_id' => 20,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rc_status_data);
                    $rc_asset=array(
                        'rc_asset_id'  => $repair_rc_asset_id,
                        'rcsh_id'      => $new_rcsh_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'status'       => 1
                    );
                    $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rc_asset);
                    // Insert Calibration Previous health data when New Rc asset generates
                    foreach($rc_asset_health as $row)
                    {
                        $health_data=array(
                            'part_id'            => $row['part_id'],
                            'asset_condition_id' => $row['asset_condition_id'],
                            'created_by'         => $this->session->userdata('sso_id'),
                            'created_time'       => date('Y-m-d H:i:s'), 
                            'rcah_id'            => $new_rcah_id,
                            'remarks'            => $row['remarks']         
                        );
                        $this->Common_model->insert_data('rc_asset_health',$health_data);
                    }

                    #update asset status
                    #audit data
                    $old_data = array(
                        'asset_status_id'     => $asset_arr['status'],
                        'availability_status' => $asset_arr['availability_status']
                    );

                    #update asset
                    $update_a = array(
                        'status'              => 10,
                        'approval_status'     => 0,
                        'availability_status' => 2,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    #audit data
                    $new_data = array(
                        'asset_status_id'     => 10,
                        'availability_status' => 2
                    );
                    $remarks = "From Cal CR:".$rc_arr['rc_number']." Sent to Repair RR:".$order_number;
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$repair_rc_asset_id,$asset_arr['country_id']);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('sso_id')),array('ash_id'=>$ash_id));

                    $asset_status=array(
                        'asset_id'         => $asset_id,
                        'rc_asset_id'      => $repair_rc_asset_id,
                        'current_stage_id' => 20,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 10 
                    );
                    $this->Common_model->insert_data('asset_status_history',$asset_status);
                   
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> Repair Process is Successfully Initiated With  RR Number :<strong>'.$order_number.'</strong> !
                        </div>');
                        redirect(SITE_URL.'admin_calibration_list'); exit;
                    }
                }
                // Sent For Recalibration
                else if($admin_status == 3)
                {
                    // Dynamic CR number generation
                    $number=get_rc_number($country_id);
                    if(count($number) == 0)
                    {
                        $f_order_number = "1-00001";
                    }
                    else
                    {
                        $num = ltrim($number['rc_number'],"C");
                        $result = explode("-", $num);
                        $running_no = (int)$result[1];

                        if($running_no == 99999)
                        {
                            $first_num = $result[0]+1;
                            $second_num = get_running_sno_five_digit(1);  
                        }
                        else
                        {
                            $first_num = $result[0];
                            $val = $running_no+1;
                            $second_num = get_running_sno_five_digit($val);
                        }
                        $f_order_number = $first_num.'-'.$second_num;
                    }
                    $order_number = 'C'.$f_order_number;
                    $remarks='Sent For Recalibration with CR Number '.$order_number;
                    // Send to Recalibration for same Asset
                    $rc_asset_data=array(
                        'asset_id'         => $asset_id,
                        'current_stage_id' => 11,
                        'rc_number'        => $order_number,
                        'country_id'       => $country_id,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1,
                        'rca_type'         => 2,
                        'cal_rc_asset_id'  => $rc_asset_id
                    );
                    $new_rc_asset_id=$this->Common_model->insert_data('rc_asset',$rc_asset_data);
                    $rcsh_data=array(
                        'rc_asset_id'      => $new_rc_asset_id,   
                        'current_stage_id' => 11,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);
                    $rcah_data=array(
                        'rc_asset_id'  => $new_rc_asset_id,
                        'rcsh_id'      => $new_rcsh_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'status'       => 1,
                    );
                    $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rcah_data);
                    
                    // Fetch Asset Health Records 
                    $health_rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>14),'rcsh_id');
                    $health_rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$health_rcsh_id),'rcah_id');
                    // Fetch Asset Health
                    $rc_asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$health_rcah_id));
                    foreach($rc_asset_health as $row)
                    {
                        $health_data=array(
                            'part_id'            => $row['part_id'],
                            'asset_condition_id' => $row['asset_condition_id'],
                            'created_by'         => $this->session->userdata('sso_id'),
                            'created_time'       => date('Y-m-d H:i:s'), 
                            'rcah_id'            => $new_rcah_id,
                            'remarks'            => $row['remarks']        
                        );
                        $this->Common_model->insert_data('rc_asset_health',$health_data);
                    }

                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>19,'status'=>10,'remarks'=>$remarks,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d'),'remarks'=>$remarks),array('rcsh_id'=>$rcsh_id));

                    $insert_rcsh = array(
                        'rc_asset_id'      => $rc_asset_id,
                        'current_stage_id' => 19,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));

                    #update asset to 1 since sent for recalibration
                    #audit data
                    $old_data = array(
                        'asset_status_id'     => $asset_arr['status'],
                        'availability_status' => $asset_arr['availability_status']
                    );

                    #update asset
                    $update_a = array(
                        'status'              => 1,
                        'approval_status'     => 0,
                        'availability_status' => 1,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    #audit data
                    $new_data = array(
                        'asset_status_id'     => 1,
                        'availability_status' => 1
                    );
                    $remarks = "From Cal CR:".$rc_arr['rc_number']." Sent for Recalibration CR:".$order_number;
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$new_rc_asset_id,$asset_arr['country_id']);

                    $ash_arr=array(
                        'status'       => 1,
                        'asset_id'     => $asset_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'rc_asset_id'  => $new_rc_asset_id,
                        'current_stage_id' => 11
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);
                   
                    // Close Rc Order if all assets are calibrated
                    $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                    $check_closed = 0;
                    foreach ($rc_asset_data as $key => $value) 
                    {
                        if($value['status']!=10)
                        {
                            $check_closed++;
                        }
                    }
                    if($check_closed == 0)
                    {
                       $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                    }
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong>' .$rc_number .' has been successfully Sent For Recalibration With CR Number :<strong>'.$order_number.'</strong> !</div>');
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                }
                // Sent For Scrap
                else if($admin_status == 4)
                {
                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>18,'status'=>10,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d')),array('rcsh_id'=>$rcsh_id));
                    $rcsh_data=array(
                        'rc_asset_id'      => $rc_asset_id,   
                        'current_stage_id' => 18,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                    #audit data
                    $old_data = array(
                        'asset_status_id'     => $asset_arr['status'],
                        'availability_status' => $asset_arr['availability_status']
                    );

                    #update asset
                    $update_a = array(
                        'status'              => 11,
                        'approval_status'     => 0,
                        'availability_status' => 2,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    #audit data
                    $new_data = array(
                        'asset_status_id'     => 11,
                        'availability_status' => 2
                    );
                    $remarks = "From Cal CR:".$rc_arr['rc_number']." Changed to Scraped";
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);
                    
                    $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    
                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('sso_id')),array('ash_id'=>$ash_id));
                    $ash_arr=array(
                        'status'       => 11,
                        'asset_id'     => $asset_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'rc_asset_id'  => $rc_asset_id,
                        'current_stage_id' => 18
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);

                    // Close Rc Order if all assets are calibrated
                    $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                    $check_closed = 0;
                    foreach ($rc_asset_data as $key => $value) 
                    {
                        if($value['status']!=10)
                        {
                            $check_closed++;
                        }
                    }
                    if($check_closed == 0)
                    {
                       $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                    }
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> CR Number : <strong>' .$rc_number .'</strong> has been Scraped Successfully </strong> !
                        </div>');
                        redirect(SITE_URL.'admin_calibration_list'); exit;
                    }
                }
                // Sent For Out of Tolerance
                else if($admin_status == 5)
                {
                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>27,'status'=>10,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));
                    $rcsh_data=array(
                        'rc_asset_id'      => $rc_asset_id,   
                        'current_stage_id' => 27,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                    #audit data
                    $old_data = array(
                        'asset_status_id'     => $asset_arr['status'],
                        'availability_status' => $asset_arr['availability_status']
                    );

                    #update asset
                    $update_a = array(
                        'status'              => 9,
                        'approval_status'     => 0,
                        'availability_status' => 2,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    #audit data
                    $new_data = array(
                        'asset_status_id'     => 9,
                        'availability_status' => 2
                    );
                    $remarks = "From Cal CR:".$rc_arr['rc_number']." Changed to Out Of Tolerance";
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    
                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));
                    $ash_arr=array(
                        'status'       => 9,
                        'asset_id'     => $asset_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'rc_asset_id'  => $rc_asset_id
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);

                    // Close Rc Order if all assets are calibrated
                    $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                    $check_closed = 0;
                    foreach ($rc_asset_data as $key => $value) 
                    {
                        if($value['status']!=10)
                        {
                            $check_closed++;
                        }
                    }
                    if($check_closed == 0)
                    {
                       $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                    }
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> CR Number : <strong>'.$rc_number.'</strong> has been Closed successfully and Asset Status is changed to <strong> Out of Tolerance</strong> !
                        </div>');
                        redirect(SITE_URL.'admin_calibration_list'); exit;
                    }
                }
            }
        }
        else//exclude qa step
        {
            $current_stage_id = 16;
            $cal_status=validate_string($this->input->post('status',TRUE));

            $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>16,'modified_time'=>NULL),'rcsh_id');

            $rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id),'rcah_id');
            // If admin Selects Calibrated
            if($cal_status == 1)
            {
                $calibrated_date=validate_string(date('Y-m-d',strtotime($this->input->post('calibrated_date',TRUE))));
                $due_date=validate_string(date('Y-m-d',strtotime($this->input->post('due_date',TRUE))));
                $remarks=validate_string($this->input->post('remarks',TRUE));
                
                #audit_data
                $old_data = array(
                    'cal_due_date'    => date('d-m-Y',strtotime($asset_arr['cal_due_date'])),
                    'calibrated_date' => date('d-m-Y',strtotime($asset_arr['calibrated_date'])),
                    'asset_status_id' => $asset_arr['status']
                );
                //Update in Asset 
                $update_a = array(
                    'cal_due_date'        => $due_date,
                    'calibrated_date'     => $calibrated_date,
                    'status'              => 1,
                    'approval_status'     => 0,
                    'availability_status' => 1,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                #audit data
                $new_data = array(
                    'cal_due_date'    => date('d-m-Y',strtotime($due_date)),
                    'calibrated_date' => date('d-m-Y',strtotime($calibrated_date)),
                    'asset_status_id' => 1
                );
                $remarks = "Asset successfully Calibrated, For CR:".$rc_arr['rc_number'];
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                $ash_id = $this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');

                $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));

                // Insert into asset status history
                $ash_arr=array(
                    'status'        =>  1,
                    'asset_id'      =>  $asset_id,
                    'created_by'    =>  $this->session->userdata('sso_id'),
                    'created_time'  =>  date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('asset_status_history',$ash_arr);

                #insert cal due date history
                $cal_due_date = array(
                    'rc_asset_id'       =>  $rc_asset_id,
                    'calibrated_date'   =>  $calibrated_date,
                    'cal_due_date'      =>  $due_date,
                    'asset_id'          =>  $asset_id,
                    'created_by'        =>  $this->session->userdata('sso_id'),
                    'created_time'      =>  date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('cal_due_date_history',$cal_due_date); 

                // Fetch Asset Health Records 
                $health_rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>16),'rcsh_id');
                $health_rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$health_rcsh_id),'rcah_id');

                $asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$health_rcah_id));

                // Update Part status
                foreach($asset_health as $row)
                {
                    $this->Common_model->update_data('part',array('status'=>1),array('part_id'=>$row['part_id']));
                }

                $this->Common_model->update_data('rc_asset',array('current_stage_id'=>17,'status'=>10,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));

                $rcsh_data=array(
                    'rc_asset_id'      => $rc_asset_id,   
                    'current_stage_id' => 17,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
                $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                // Close Rc Order if all assets are calibrated
                $rc_asset_data = $this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                $check_closed = 0;
                foreach ($rc_asset_data as $key => $value) 
                {
                    if($value['status']!=10)
                    {
                        $check_closed++;
                    }
                }
                if($check_closed == 0)
                {
                   $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                }
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.</div>'); 
                    redirect(SITE_URL.'admin_calibration_list'); exit();
                }
                else
                {
                    $this->db->trans_commit();
                    $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> CR Number : <strong>'.$rc_number.'</strong> has Closed Successfully with Calibration Status : <strong>Calibrated</strong> !</div>');
                    redirect(SITE_URL.'admin_calibration_list'); exit();
                }
            }
            // If Admin Select Not Calibrated
            if($cal_status == 2)
            {
                $repair = validate_number($this->input->post('repair',TRUE));
                // Sent for repair
                if(@$repair == 1)
                {
                    $this->Common_model->update_data('rc_status_history',array('status'=>2),array('rcsh_id'=>$rcsh_id));
                    $this->Common_model->update_data('rc_asset',array('status'=>2),array('rc_asset_id'=>$rc_asset_id));

                    // Dynamic CRB Number generation
                    $number=get_repair_rc_number($country_id);
                    if(count($number) == 0)
                    {
                        $f_order_number = "1-00001";
                    }
                    else
                    {
                        $num = ltrim($number['rc_number'],"R");
                        $result = explode("-", $num);
                        $running_no = (int)$result[1];

                        if($running_no == 99999)
                        {
                            $first_num = $result[0]+1;
                            $second_num = get_running_sno_five_digit(1);  
                        }
                        else
                        {
                            $first_num = $result[0];
                            $val = $running_no+1;
                            $second_num = get_running_sno_five_digit($val);
                        }
                        $f_order_number= $first_num.'-'.$second_num;
                    }
                    $order_number = 'R'.$f_order_number;
                    $rc_asset_data=array(
                        'asset_id'         => $asset_id,
                        'current_stage_id' => 20,
                        'rc_number'        => $order_number,
                        'country_id'       => $country_id,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1,
                        'rca_type'         => 1,
                        'cal_rc_asset_id'  => $rc_asset_id
                    );
                    // Send From Calibration to Repair
                    $repair_rc_asset_id=$this->Common_model->insert_data('rc_asset',$rc_asset_data);

                    $rc_status_data=array(
                        'rc_asset_id'      => $repair_rc_asset_id,
                        'current_stage_id' => 20,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rc_status_data);

                    $rc_asset=array(
                        'rc_asset_id'  => $repair_rc_asset_id,
                        'rcsh_id'      => $new_rcsh_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'status'       => 1
                    );
                    $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rc_asset);

                    // Fetch Asset Health
                    $rc_asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
                    foreach($rc_asset_health as $row)
                    {
                        $health_data=array(
                        'part_id'            => $row['part_id'],
                        'asset_condition_id' => $row['asset_condition_id'],
                        'remarks'            => $row['remarks'],
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'), 
                        'rcah_id'            => $new_rcah_id
                        );
                        $this->Common_model->insert_data('rc_asset_health',$health_data);
                    }

                    #audit data
                    $old_data = array(
                        'asset_status_id'     => $asset_arr['status'],
                        'availability_status' => $asset_arr['availability_status']
                    );
                    #update asset 
                    $update_a = array(
                        'status'              => 10,
                        'availability_status' => 2,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s'));
                    $update_a_where = array('asset_id' => $asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    #audit data
                    $new_data = array(
                        'asset_status_id'     => 10,
                        'availability_status' => 2
                    );
                    $remarks = "From Cal CR:".$rc_arr['rc_number']." Requested for Repair RR:".$order_number;
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$repair_rc_asset_id,$asset_arr['country_id']);

                    $update_ash = array(
                        'end_time'    => date('Y-m-d H:i:s'),
                        'modified_by' => $this->session->userdata('sso_id')
                    );
                    $update_ash_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                    $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                    $asset_status=array(
                        'asset_id'         => $asset_id,
                        'rc_asset_id'      => $repair_rc_asset_id,
                        'current_stage_id' => 20,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 10  
                    );
                    $this->Common_model->insert_data('asset_status_history',$asset_status);
                    
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> Repair Process is Successfully Initiated With  RR Number :<strong>'.$order_number.'</strong> ! </div>');
                        redirect(SITE_URL.'admin_calibration_list'); exit;
                    }
                }
                // Sent For Recalibration
                if(@$repair == 2)
                {
                    // Dynamic CRB number Generation
                    $number=get_rc_number($country_id);
                    if(count($number) == 0)
                    {
                        $f_order_number = "1-00001";
                    }
                    else
                    {
                        //echo $number['rcb_number'];exit;
                        $num = ltrim($number['rc_number'],"C");
                        //echo $num;exit;
                        $result = explode("-", $num);
                        $running_no = (int)$result[1];

                        if($running_no == 99999)
                        {
                            $first_num = $result[0]+1;
                            $second_num = get_running_sno_five_digit(1);  
                        }
                        else
                        {
                            $first_num = $result[0];
                            $val = $running_no+1;
                            $second_num = get_running_sno_five_digit($val);
                        }
                        $f_order_number = $first_num.'-'.$second_num;
                    }
                    $order_number = 'C'.$f_order_number;

                    $remarks='Sent For Recalibration with CR Number '.$order_number.'!';

                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>19,'status'=>10,'remarks'=>$remarks,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));

                    $insert_rcsh = array(
                            'rc_asset_id'      => $rc_asset_id,
                            'current_stage_id' => 19,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'status'           => 1,
                            'remarks'          => $remarks);
                    $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    $this->Common_model->update_data('asset_status_history',
                        array('end_time'=>date('Y-m-d H:i:s'),
                              'modified_by'=>$this->session->userdata('sso_id')),
                        array('ash_id'=>$ash_id));

                    #audit data
                    $old_data = array('asset_status_id' => $asset_arr['status']);
                    #update asset
                    $update_a = array(
                        'status'              => 1,
                        'approval_status'     => 0,
                        'availability_status' => 1,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);
                    #audit data
                    $new_data = array('asset_status_id' => 1);
                    $remarks = "From CR:".$rc_arr['rc_number']." Initiated Recalibration with CR:".$order_number;
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                    $ash_arr=array(
                        'status'           => 1,
                        'asset_id'         => $asset_id,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'rc_asset_id'      => $rc_asset_id,
                        'current_stage_id' => 19
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);

                    #re-calibration entry
                    $rc_asset_data=array(
                        'asset_id'         =>  $asset_id,
                        'current_stage_id' =>  11,
                        'rc_number'        =>  $order_number,
                        'country_id'       =>  $country_id,
                        'created_by'       =>  $this->session->userdata('sso_id'),
                        'created_time'     =>  date('Y-m-d H:i:s'),
                        'status'           =>  1,
                        'rca_type'         =>  2,
                        'cal_rc_asset_id'  =>  $rc_asset_id
                                        );
                    // Send For Recalibration With same asset
                    $new_rc_asset_id=$this->Common_model->insert_data('rc_asset',$rc_asset_data);

                    $rcsh_data=array(
                        'rc_asset_id'       =>  $new_rc_asset_id,   
                        'current_stage_id'  =>  11,
                        'created_by'        =>  $this->session->userdata('sso_id'),
                        'created_time'      =>  date('Y-m-d H:i:s'),
                        'status'            =>  1
                                        );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);
                    
                    $rcah_data=array(
                        'rc_asset_id'       =>  $new_rc_asset_id,
                        'rcsh_id'           =>  $new_rcsh_id,
                        'created_by'        =>  $this->session->userdata('sso_id'),
                        'created_time'      =>  date('Y-m-d H:i:s'),
                        'status'            =>  1
                                   );
                    $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rcah_data);
                    
                    // Fetch Asset Health
                    $rc_asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
                    foreach($rc_asset_health as $row)
                    {
                        $health_data=array(
                            'part_id'           => $row['part_id'],
                            'asset_condition_id'=> $row['asset_condition_id'],
                            'created_by'        => $this->session->userdata('sso_id'),
                            'created_time'      => date('Y-m-d H:i:s'), 
                            'rcah_id'           => $new_rcah_id         
                        );
                        $this->Common_model->insert_data('rc_asset_health',$health_data);
                    }

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    $this->Common_model->update_data('asset_status_history',
                        array('end_time'=>date('Y-m-d H:i:s'),
                              'modified_by'=>$this->session->userdata('sso_id')),
                        array('ash_id'=>$ash_id));
                    $ash_arr=array(
                        'status'           => 10,
                        'asset_id'         => $asset_id,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'rc_asset_id'      => $new_rc_asset_id,
                        'current_stage_id' => 20
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);
                   
                    // Close Rc Order if all assets are calibrated
                    $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                    $check_closed = 0;
                    foreach ($rc_asset_data as $key => $value) 
                    {
                        if($value['status']!=10)
                        {
                            $check_closed++;
                        }
                    }
                    if($check_closed == 0)
                    {
                       $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                    }
                    
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> ' .$rc_number .' has been successfully Sent For Recalibration With CR Number : <strong>'.$order_number.'</strong> !</div>');
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                }
                // Sent for Scrap
                if($repair == 3)
                {
                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>18,'status'=>10,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d')),array('rcsh_id'=>$rcsh_id));

                    $rcsh_data=array(
                        'rc_asset_id'      => $rc_asset_id,   
                        'current_stage_id' => 18,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                    // Close Rc Order if all assets are calibrated
                    $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                    $check_closed = 0;
                    foreach ($rc_asset_data as $key => $value) 
                    {
                        if($value['status']!=10)
                        {
                            $check_closed++;
                        }
                    }
                    if($check_closed == 0)
                    {
                       $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                    }

                    #audit data
                    $old_data = array(
                        'asset_status_id'     => $asset_arr['status'],
                        'availability_status' => $asset_arr['availability_status'],
                        'tool_availability'   => get_asset_position($asset_id)
                    );

                    // Update Asset as Scrap
                    $update_a = array(
                        'status'              => 11,
                        'approval_status'     => 0,
                        'availability_status' => 2,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);
                    #audit data
                    $new_data = array(
                        'asset_status_id'     => 11,
                        'availability_status' => 2,
                        'tool_availability'   => get_asset_position($asset_id)
                    );
                    $remarks = "From Cal CR:".$rc_arr['rc_number']." Changed to Scraped";
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    
                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));

                    $ash_arr=array(
                        'status'           => 11,
                        'asset_id'         => $asset_id,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'rc_asset_id'      => $rc_asset_id,
                        'current_stage_id' => 18
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);
                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> CR Number :<strong>'.$rc_number .'</strong> has been Scraped Successfully !
                        </div>');
                        redirect(SITE_URL.'admin_calibration_list'); exit;
                    }
                }
                // Out of Tolerance
                if($repair == 4)
                {
                    $this->Common_model->update_data('rc_asset',array('current_stage_id'=>27,'status'=>10,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rc_asset_id'=>$rc_asset_id));

                    $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));

                    $rcsh_data=array(
                        'rc_asset_id'      => $rc_asset_id,   
                        'current_stage_id' => 27,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

                    #Update Asset as Out of Tolerence
                    #audit data
                    $old_data = array(
                        'asset_status_id'     => $asset_arr['status'],
                        'availability_status' => $asset_arr['availability_status'],
                        'tool_availability'   => get_asset_position($asset_id)
                    );

                    $update_a = array(
                        'status'              => 9,
                        'approval_status'     => 0,
                        'availability_status' => 2,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);
                    #audit data
                    $new_data = array(
                        'asset_status_id'     => 9,
                        'availability_status' => 2,
                        'tool_availability'   => get_asset_position($asset_id)
                    );
                    $remarks = "From Cal CR:".$rc_arr['rc_number']." Changed to Out of Tolerance";
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                    $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                    
                    $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));
                    $ash_arr=array(
                        'status'       => 9,
                        'asset_id'     => $asset_id,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s'),
                        'rc_asset_id'  => $rc_asset_id
                    );
                    $this->Common_model->insert_data('asset_status_history',$ash_arr);
                    
                    // Close Rc Order if all assets are calibrated
                    $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
                    $check_closed = 0;
                    foreach ($rc_asset_data as $key => $value) 
                    {
                        if($value['status']!=10)
                        {
                            $check_closed++;
                        }
                    }
                    if($check_closed == 0)
                    {
                       $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id)); 
                    }

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> something went wrong! please check.</div>'); 
                        redirect(SITE_URL.'admin_calibration_list');  exit;
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> CR Number : <strong>'.$rc_number.'</strong> has been Closed successfully and Asset Status is changed to <strong> Out of Tolerance</strong> !</div>');
                        redirect(SITE_URL.'admin_calibration_list'); exit;
                    }
                }
            }
        }
    }

    public function closed_calibration()
    {
        
        $data['nestedView']['heading']="Closed Calibration Requests";
        $data['nestedView']['cur_page'] = 'closed_calibration';
        $data['nestedView']['parent_page'] = 'closed_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Calibration Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Calibration Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'   => validate_string($this->input->post('rcb_number', TRUE)),
                'whc_id'       => validate_number($this->input->post('wh_id',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'asset_number' => $this->session->userdata('asset_number'), 
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'rcb_number'   => '',
                    'whc_id'       => '',
                    'rc_number'    => '',
                    'asset_number' => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            } 
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_calibration/';
        # Total Records

        $config['total_rows'] = $this->Calibration_m->closed_calibration_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->closed_calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);
        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Calibration_m->get_closed_asset_details($value['rc_order_id']);
            $calibration_results[$key]['asset_list']=$part_list;
        }
        # Additional data
        $data['calibration_results']=$calibration_results;
        
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['displayResults'] = 1;
        $data['flg']=1;
        $this->load->view('calibration/closed_calibration_list',$data);
    }

    // Srilekha
    public function closed_calibration_view()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id =='')
        {
            redirect(SITE_URL.'closed_calibration'); exit;
        }
 
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View Closed Calibration Details";
        $data['nestedView']['cur_page'] = 'closed_calibration';
        $data['nestedView']['parent_page'] = 'closed_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/asset.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View Closed Calibration Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Calibration Requests','class'=>'','url'=>SITE_URL.'closed_calibration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View Closed Calibration Details','class'=>'active','url'=>'');

        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Calibration_m->get_closed_asset_details($rc_order_id);

        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['rcb_number'] = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $data['attach_document'] = $this->Calibration_m->get_documents_by_stage($rc_order_id);
        $data['cal_row']=$cal_row;
        $this->load->view('calibration/closed_calibration_details',$data);
    }

    //Srilekha //acknowledge cal tool at logistics
    public function vendor_calibration()
    {
        $data['nestedView']['heading']="Calibration Returned Tools";
        $data['nestedView']['cur_page'] = 'acknowledge_cal_tools';
        $data['nestedView']['parent_page'] = 'acknowledge_cal_tools';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Acknowledge Calibration Returned Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Returned Tools','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'   => validate_string($this->input->post('rcb_number', TRUE)),
                'whc_id'       => validate_number($this->input->post('wh_id',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else {
                $searchParams=array(
                    'rcb_number'   => '',
                    'whc_id'       => '',
                    'asset_number' => '',
                    'rc_number'    => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }   
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'vendor_calibration/';
        # Total Records

        $config['total_rows'] = $this->Calibration_m->vendor_calibration_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->vendor_calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Calibration_m->get_vendor_assets($value['rc_order_id']);

            $calibration_results[$key]['asset_list']=$part_list;
        }
        
        # Additional data
        $data['calibration_results']=$calibration_results;
        
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['displayResults'] = 1;
        $data['flg']=1;
        
        $this->load->view('calibration/vendor_calibration_request_list',$data);  
    }
    // Srilekha
    public function edit_vendor_calibration_list()
    {
        $rc_asset_id=@storm_decode($this->uri->segment(2));
        if($rc_asset_id == '')
        {
            redirect(SITE_URL.'vendor_calibration'); exit();
        }

        #page authentication
        $task_access = page_access_check('acknowledge_cal_tools');
        if($task_access == 1)
        {
            $warehouse_id = $this->session->userdata('swh_id');
        }
        elseif($task_access == 2 || $task_access == 3)
        {
            if($this->input->post('SubmitWarehouse')!='')
            {
                $warehouse_id = $this->input->post('warehouse_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']= "Select Warehouse";
                $data['nestedView']['cur_page'] = 'check_calibration';
                $data['nestedView']['parent_page'] = 'acknowledge_cal_tools';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Warehouse';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Returned Tools','class'=>'','url'=>SITE_URL.'vendor_calibration');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Warehouse','class'=>'active','url'=>'');

                $country_id = $this->Calibration_m->get_country_from_rc_asset($rc_asset_id);
                $data['warehouse_list']=$this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));

                $data['form_action'] = SITE_URL.'edit_vendor_calibration_list/'.storm_encode($rc_asset_id);
                $data['task_access'] = $task_access;
                $data['cancel'] = SITE_URL.'vendor_calibration';
                $this->load->view('calibration/intermediate_page',$data); 
            }
        }

        if(@$warehouse_id!='')
        {
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Scan QR Code";
            $data['nestedView']['cur_page'] = 'check_calibration';
            $data['nestedView']['parent_page'] = 'acknowledge_cal_tools';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Scan QR Code';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Returned Tools','class'=>'','url'=>SITE_URL.'vendor_calibration');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scan QR Code','class'=>'active','url'=>'');

            
            $asset_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'asset_id');
            $rc_order_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_order_id');
            $order_details=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
            $data['rc_number']=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_number');
            $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$order_details['supplier_id']));
            $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$order_details['wh_id']));
            $data['order_details']=$order_details;
            $data['rc_order_id']=$rc_order_id;
            $data['rc_asset_id']=$rc_asset_id;
            $data['add_wh_id'] = $warehouse_id;
            $add_wh_list = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$warehouse_id));
            $data['wh_name'] = $add_wh_list['wh_code'].' -('.$add_wh_list['name'].')';
            $data['asset_list']=$this->Calibration_m->get_vendor_assets_row($rc_order_id,$asset_id);
            $data['flg']=2;

            $this->load->view('calibration/vendor_calibration_request_list',$data);
        }
    }
    
    public function vendor_asset_details()
    {
        #page authentication
        $task_access = page_access_check('acknowledge_cal_tools');

        $rc_order_id=validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        $rc_asset_id=validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        $add_wh_id = validate_number(storm_decode($this->input->post('add_wh_id',TRUE)));
        if($rc_order_id =='' || $rc_asset_id == '' || $add_wh_id == '')
        {
            redirect(SITE_URL.'vendor_calibration'); exit();
        }

        $asset_number = validate_string($this->input->post('asset_number',TRUE));
        $asset_number = trim(@$asset_number);
        $check_asset=$this->Calibration_m->check_asset_number($rc_asset_id,$asset_number);
        if($check_asset['asset_id']=='')
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Invalid Asset Number:<strong> '.$asset_number.' </strong>!</div>'); 
            redirect(SITE_URL.'edit_vendor_calibration_list/'.storm_encode($rc_asset_id)); 
            exit();
        }
        else
        {
            $order_details=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
            $data['nestedView']['heading']="Acknowledge Calibrated Tool";
            $data['nestedView']['cur_page'] = 'check_calibration';
            $data['nestedView']['parent_page'] = 'acknowledge_cal_tools';

            # include files
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/health_calibration.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = "Acknowledge Calibrated Tool : ".$asset_number."";
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Cal Returned Tools','class'=>'','url'=>SITE_URL.'vendor_calibration');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Calibrated Tool','class'=>'active','url'=>'');

            #additional data
            $asset_details = $this->Calibration_m->get_asset_part_details($check_asset['asset_id']);

            $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$order_details['supplier_id']));
            $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$order_details['wh_id']));
            $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>13),'rcsh_id');
            $rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id,'end_time'=>NULL),'rcah_id');
            //echo $rcah_id; exit;
            $asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
            //print_r($asset_health); exit;
            $selected_asset_health=array();
            foreach($asset_health as $row)
            {
                $selected_asset_health[$row['part_id']]['asset_condition_id']=$row['asset_condition_id'];
                $selected_asset_health[$row['part_id']]['remarks']=$row['remarks'];
            }

            //print_r($selected_asset_health); exit;
            $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
            $data['attach_document']=$this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
            $data['rc_number']=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_number');
            $data['selected_asset_health']=$selected_asset_health;
            $data['asset_details'] = $asset_details;
            $data['order_details'] = $order_details;
            $data['rc_order_id']=$rc_order_id;
            $data['rc_asset_id']=$rc_asset_id;
            $data['add_wh_id'] = $add_wh_id;
            $add_wh_list = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$add_wh_id));
            $data['wh_name'] = $add_wh_list['wh_code'].' -('.$add_wh_list['name'].')';
            $data['form_action'] = SITE_URL.'update_vendor_calibration_request';

            $this->load->view('calibration/acknowledge_tools',$data);
        }
    }

    public function update_vendor_calibration_request()
    {
        #page authentication
        $task_access = page_access_check('acknowledge_cal_tools');

        $rc_order_id=validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        $rc_asset_id=validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        $add_wh_id = validate_number(storm_decode($this->input->post('add_wh_id',TRUE)));

        if($rc_asset_id=='' || $rc_order_id=='' || $add_wh_id == '')
        {
            redirect(SITE_URL.'vendor_calibration'); exit();
        }
        $doc_type = $this->input->post('document_type',TRUE);
        $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>13),'rcsh_id');
        $rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id,'end_time'=>NULL),'rcah_id');

        $rc_arr = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));
        $asset_id = $rc_arr['asset_id'];
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $asset_number = $asset_arr['asset_number'];

        $this->db->trans_begin();
        if($doc_type[1]!='')
        {
            foreach ($doc_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                   $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $original_name=$config['org_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'rc_asset_id'      => $rc_asset_id,
                    'doc_name'         => $original_name,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1);

                if($value!='' && $support_document != '')
                {
                    $this->Common_model->insert_data('rca_doc',$insert_doc);
                }
            }
        }

        $this->Common_model->update_data('rc_status_history',array('modified_time'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('sso_id')),array('rcsh_id'=>$rcsh_id));
        $country_id = $rc_arr['country_id'];
        $access_status = $this->Common_model->get_value('location_pref',array('location_id'=>$country_id,'pref_id'=>1),'status');
        $cur_stg_id = ($access_status == 1)?16:14;

        $rcsh_data=array(
            'rc_asset_id'       =>  $rc_asset_id,
            'current_stage_id'  =>  $cur_stg_id,
            'created_by'        =>  $this->session->userdata('sso_id'),
            'created_time'      =>  date('Y-m-d H:i:s'),
            'status'            =>  1
            );
        $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);
        $rcah_data=array(
            'rc_asset_id'       =>  $rc_asset_id,
            'rcsh_id'           =>  $new_rcsh_id,
            'created_by'        =>  $this->session->userdata('sso_id'),
            'created_time'      =>  date('Y-m-d H:i:s'),
            'status'            =>  1,
            );
        $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rcah_data);
        $asset_condition_arr = $this->input->post('asset_condition_id',TRUE);
        $remarks_arr =  $this->input->post('remarks',TRUE);
        $asset_details=$this->Calibration_m->get_asset_part_details($asset_id);
        foreach ($asset_details as $key => $value)
        {
            $remarks = $remarks_arr[$value['part_id']];
            if($remarks == '') $remarks = NULL;
            $insert_data3 = array(
                'rcah_id'            => $new_rcah_id,
                'part_id'            => $value['part_id'],
                'asset_condition_id' => $asset_condition_arr[$value['part_id']],
                'created_by'         => $this->session->userdata('sso_id'),
                'created_time'       => date('Y-m-d H:i:s'),
                'remarks'            => $remarks);
            $this->Common_model->insert_data('rc_asset_health',$insert_data3);
           // print_r($insert_data3);
        }

        $this->Common_model->update_data('rc_asset',array('current_stage_id'=>$cur_stg_id),array('rc_asset_id'=>$rc_asset_id));

        $ash_id=$this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
        $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));
        $ash_arr=array(
            'status'        =>  4,
            'asset_id'      =>  $asset_id,
            'created_by'    =>  $this->session->userdata('sso_id'),
            'created_time'  =>  date('Y-m-d H:i:s'),
            'rc_asset_id'   =>  $rc_asset_id);
        $this->Common_model->insert_data('asset_status_history',$ash_arr);

        #audit data
        $old_data = array(
            'tool_availability' => get_asset_position($asset_id),
            'wh_id'             => $asset_arr['wh_id'],
            'sub_inventory'     => $asset_arr['sub_inventory']
        );

        $asset_position_id=$this->Common_model->get_value('asset_position',array('asset_id'=>$asset_id,'to_date'=>NULL),'asset_position_id');
        $this->Common_model->update_data('asset_position',array('to_date'=>date('Y-m-d')),array('asset_position_id'=>$asset_position_id));

        $asset_position_data=array(
            'asset_id'  => $asset_id,
            'wh_id'     => $add_wh_id,
            'from_date' => date('Y-m-d'),
            'transit'   => 0
        );
        $this->Common_model->insert_data('asset_position',$asset_position_data);

        #update asset
        $sub_inventory_location = validate_string($this->input->post('sub_inventory',TRUE));
        if($sub_inventory_location == '') { $sub_inventory_location =NULL; }

        $update_asset = array(
            'wh_id'         => $add_wh_id,
            'sub_inventory' => $sub_inventory_location,
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $update_where_asset = array('asset_id'=> $asset_id);
        $this->Common_model->update_data('asset',$update_asset,$update_where_asset);

        #audit data
        $new_data = array(
            'tool_availability' => get_asset_position($asset_id),
            'wh_id'             => $add_wh_id,
            'sub_inventory'     => $sub_inventory_location
        );
        $remarks = "Received Cal tool at Inventory, CR No:".$rc_arr['rc_number'];
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset <strong>' .$asset_number.'</strong> has been Acknowledged successfully!</div>');
        }
        redirect(SITE_URL.'vendor_calibration');  exit(); 
    }

    // Srilekha
    public function vendor_calibration_delivery_view()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id =='')
        {
            redirect(SITE_URL.'vendor_calibration'); exit;
        }
 
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View open Calibration Details";
        $data['nestedView']['cur_page'] = 'calibration';
        $data['nestedView']['parent_page'] = 'calibration';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/asset.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View Open Calibration Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Calibration Request','class'=>'','url'=>'open_calibration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View open Calibration Details','class'=>'active','url'=>'');

        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Calibration_m->get_vendor_assets($rc_order_id);
        $data['main_doc']=$this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        //echo $this->db->last_query(); exit();
        $data['rc_order_id']=$rc_order_id;
        $data['cal_row']=$cal_row;

        //print_r($attach_document); exit;

        $this->load->view('calibration/view_vendor_wh_calibration_details',$data);
    }

    //Srilekha //logistics
    public function closed_cal_delivery_list()
    {
        $data['nestedView']['heading']="Closed Cal Shipment List";
        $data['nestedView']['cur_page'] = 'closed_cal_delivery_list';
        $data['nestedView']['parent_page'] = 'closed_cal_delivery_list';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Calibration Shipment List';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Calibration Shipment List','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'      => validate_string($this->input->post('rcb_number', TRUE)),
                'whc_id'          => validate_number(@$this->input->post('wh_id',TRUE)),
                'asset_number'    => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'       => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'      => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'rcb_number'       => '',
                    'whc_id'           => '',
                    'page_redirect_cal'=> '',
                    'rc_number'        => '',
                    'asset_number'     => '',
                    'country_id'       => '',
                    'serial_no'        => ''
                );
                $this->session->set_userdata($searchParams);
            } 
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_cal_delivery_list/';
        # Total Records

        $config['total_rows'] = $this->Calibration_m->closed_cal_delivery_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->closed_cal_delivery_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Calibration_m->get_vendor_closed_assets($value['rc_order_id']);
            $calibration_results[$key]['asset_list']=$part_list;
        }
        
        # Additional data
        $data['calibration_results']=$calibration_results;
        
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access)); 
       
        $data['displayResults'] = 1;
        $data['flg']=1;
        
        $this->load->view('calibration/closed_cal_delivery_list',$data);
    }

     // Srilekha //logistics
    public function view_vendor_calibration_list()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id =='')
        {
            redirect(SITE_URL.'closed_cal_delivery_list'); exit;
        }
 
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View Calibration Delivery Details";
        $data['nestedView']['cur_page'] = 'closed_cal_delivery_list';
        $data['nestedView']['parent_page'] = 'closed_cal_delivery_list';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View Calibration Delivery Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Cal delivery list','class'=>'','url'=>SITE_URL.'closed_cal_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View Calibration Delivery Details','class'=>'active','url'=>'');


        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Calibration_m->get_vendor_closed_assets($rc_order_id);
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));

        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $data['rc_order_id']=$rc_order_id;
        $data['cal_row']=$cal_row;
        
        $this->load->view('calibration/view_vendor_calibration_details',$data);
    }

    public function qc_calibration()
    {
        $data['nestedView']['heading']="QA Calibration Requests";
        $data['nestedView']['cur_page'] = 'qc_calibration';
        $data['nestedView']['parent_page'] = 'qc_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'QA Calibration Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'QA Calibration Requests','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'      => validate_string($this->input->post('rcb_number', TRUE)),
                'whc_id'          => validate_number($this->input->post('wh_id',TRUE)),
                'asset_number'    => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'       => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'      => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else {
                $searchParams=array(
                    'rcb_number'   => '',
                    'whc_id'       => '',
                    'asset_number' => '',
                    'rc_number'    => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'admin_calibration_list/';
        # Total Records

        $config['total_rows'] = $this->Calibration_m->qc_calibration_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->qc_calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);
       
        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Calibration_m->get_asset_details_for_qc($value['rc_order_id']);
            $calibration_results[$key]['asset_list']=$part_list;
        }
       
        # Additional data
        $data['calibration_results']=$calibration_results;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['displayResults'] = 1;
        $data['flg']=1;
        $this->load->view('calibration/qc_calibration_request',$data);
    }
    
    public function open_qc_calibration_list()
    {
        $rc_asset_id=@storm_decode($this->uri->segment(2));
        if($rc_asset_id == '' )
        {
            redirect(SITE_URL.'qc_calibration'); exit;
        }
        $rc_order_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_order_id');
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Calibration Asset Details";
        $data['nestedView']['cur_page'] = 'check_calibration';
        $data['nestedView']['parent_page'] = 'qc_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Calibration Asset Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'QA Calibration Requests','class'=>'','url'=>SITE_URL.'qc_calibration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Asset Details','class'=>'active','url'=>'');

        $data['cal_row']=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>14,'status'=>1),'rcsh_id');
        $rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id),'rcah_id');
        $asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
        $asset_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'asset_id');
        $selected_asset_health=array();
        // Fetch Asset Health
        foreach($asset_health as $row)
        {
            $selected_asset_health[$row['part_id']]['asset_condition_id']=$row['asset_condition_id'];
            $selected_asset_health[$row['part_id']]['remarks']=$row['remarks'];
        }
        $asset_details=$this->Calibration_m->get_asset_details_for_qc_row($rc_order_id,$asset_id);
        $part_list=$this->Calibration_m->get_asset_part_details($asset_id);
        $asset_details['asset_health_list']=$part_list;
        
        $data['selected_asset_health']=$selected_asset_health;
        $data['asset_details']=$asset_details;
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['attach_document'] = $this->Common_model->get_data('rca_doc',array('rc_asset_id'=>$rc_asset_id,'status'=>1));
        $data['qa_status']=$this->Common_model->get_data('qa_status',array('status'=>1));
        
        $data['rc_order_id']=$rc_order_id;
        $data['rc_asset_id']=$rc_asset_id; 
        $data['flg']=2;

        
        $data['form_action'] = SITE_URL.'update_qc_calibration_list';


        $this->load->view('calibration/qc_calibration_request',$data);
    }
    public function update_qc_calibration_list()
    {
        #page authentication
        $task_access = page_access_check('qc_calibration');

        $rc_order_id=validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        $rc_asset_id=validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        if($rc_order_id =='' || $rc_asset_id =='')
        {
            redirect(SITE_URL.'qc_calibration'); exit();
        }

        $status=validate_number($this->input->post('qa_status',TRUE));
        $qa_reason=validate_string($this->input->post('qa_reason',TRUE));

        $rc_number=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_number');

        $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>15),'rcsh_id');

        $asset_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'asset_id');
        $remarks=validate_string($this->input->post('remarks',TRUE));
        $this->db->trans_begin();

        $document_type = $this->input->post('document_type',TRUE);
        foreach ($document_type as $key => $value) 
        {
            if($_FILES['support_document_'.$key]['name']!='')
            {
                $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                $config['upload_path'] = asset_document_path();
                $config['allowed_types'] = 'gif|jpg|png|pdf';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('support_document_'.$key);
                $fileData = $this->upload->data();
                $support_document = $config['file_name'];
                $original_name    = $config['org_name'];
            }
            else
            {
                $support_document = '';
            }
            $insert_doc = array(
                                'document_type_id' => $value,
                                'name'             => $support_document,
                                'rc_asset_id'      => $rc_asset_id,
                                'doc_name'         => $original_name,
                                'created_by'       => $this->session->userdata('sso_id'),
                                'created_time'     => date('Y-m-d H:i:s'),
                                'status'           => 1
                                );

            if($value!='' && $support_document!='')
            {
                // Insert Documents into Calibration_doc
                $this->Common_model->insert_data('rca_doc',$insert_doc);
            }
        }
        // If QC Approves
        if($status == 1)
        {
            // update Calibration Data
            $this->Common_model->update_data('rc_asset',array('current_stage_id'=>16,'remarks'=>$remarks),array('rc_asset_id'=>$rc_asset_id));

            // update cal_asset_history data
            $this->Common_model->update_data('rc_status_history',array('modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('rcsh_id'=>$rcsh_id));

            $rcsh_data=array(
                'rc_asset_id'      => $rc_asset_id,   
                'current_stage_id' => 16,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1
            );
            $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);

            // update asset_status_history data
            $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
            $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('sso_id')),array('ash_id'=>$ash_id));
            $ash_arr=array(
                'status'           => 4,
                'asset_id'         => $asset_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'rc_asset_id'      => $rc_asset_id,
                'current_stage_id' => 16
            );
            $this->Common_model->insert_data('asset_status_history',$ash_arr);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>');
                redirect(SITE_URL.'qc_calibration');  
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> QA Details has been Updated successfully For CR Number : <strong>'.$rc_number.'</strong> !
                </div>');
                redirect(SITE_URL.'qc_calibration');
            }
        }
        // If QC Rejects
        if($status == 2)
        {
            // update Calibration Data
            $this->Common_model->update_data('rc_asset',array('current_stage_id'=>16,'remarks'=>$remarks),array('rc_asset_id'=>$rc_asset_id));

            // update asset_status_history data
            $ash_id=$this->Common_model->get_value('asset_status_history',array('asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
            $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('sso_id')),array('ash_id'=>$ash_id));
            $ash_arr=array(
                'status'           => 4,
                'asset_id'         => $asset_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'rc_asset_id'      => $rc_asset_id,
                'current_stage_id' => 16
            );
            $this->Common_model->insert_data('asset_status_history',$ash_arr);

            $this->Common_model->update_data('rc_status_history',array('qa_status_id'=>$qa_reason,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s'),'status'=>2,'remarks'=>$remarks),array('rcsh_id'=>$rcsh_id));

            $rcsh_data=array(
                'rc_asset_id'      => $rc_asset_id,   
                'current_stage_id' => 16,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1
            );
            $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'qc_calibration');  
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> CR Number :<strong>'.$rc_number.'</strong> has been Rejected By QA Team !</div>');
                redirect(SITE_URL.'qc_calibration');
            }
        }
    }

    public function view_qc_calibration_list()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id =='')
        {
            redirect(SITE_URL.'qc_calibration'); exit;
        }
 
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="QA Calibration Details";
        $data['nestedView']['cur_page'] = 'qc_calibration';
        $data['nestedView']['parent_page'] = 'qc_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'QA Calibration Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'QA Calibration Requests','class'=>'','url'=>SITE_URL.'qc_calibration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'QA Calibration Details','class'=>'active','url'=>'');

        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Calibration_m->get_asset_details_for_qc($rc_order_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['cal_row']=$cal_row;
        
        $this->load->view('calibration/view_qc_calibration_details',$data);
    }

    public function closed_qc_calibration_list()
    {
        $data['nestedView']['heading']="Closed QA Calibration Requests";
        $data['nestedView']['cur_page'] = 'closed_qc_calibration';  
        $data['nestedView']['parent_page'] = 'closed_qc_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed QA Calibration Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed QA Calibration Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'      => validate_string($this->input->post('rcb_number', TRUE)),
                'whc_id'          => validate_number($this->input->post('wh_id',TRUE)),
                'asset_number'    => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'       => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'      => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'       => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'rcb_number'   => '',
                    'whc_id'       => '',
                    'asset_number' => '',
                    'rc_number'    => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_qc_calibration_list/';
        # Total Records

        $config['total_rows'] = $this->Calibration_m->closed_qc_calibration_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->closed_qc_calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Calibration_m->get_qc_closed_assets($value['rc_order_id']);
            $calibration_results[$key]['asset_list']=$part_list;
        }
       
        # Additional data
        $data['calibration_results']=$calibration_results;
        //print_r($calibration_results); exit;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['displayResults'] = 1;
        $data['flg']=1;
        $this->load->view('calibration/closed_qc_calibration_list',$data);
    }
    
    public function view_closed_qc_calibration_list()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id =='')
        {
            redirect(SITE_URL.'closed_qc_calibration_list'); exit;
        }
 
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Closed QA Calibration Details";
        $data['nestedView']['cur_page'] = 'closed_qc_calibration';
        $data['nestedView']['parent_page'] = 'closed_qc_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed QA Calibration Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed QA Calibration Requests','class'=>'','url'=>SITE_URL.'closed_qc_calibration_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed QA Calibration Details','class'=>'active','url'=>'');

        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Calibration_m->get_qc_closed_assets($rc_order_id);

        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['attach_document'] = $this->Calibration_m->get_documents_by_stage($rc_order_id);
        $data['cal_row']=$cal_row;
        

        $this->load->view('calibration/view_closed_qc_calibration_details',$data);
    }

    public function get_supplier_details_by_id()
    {
        $supplier_id = validate_number($this->input->post('supplier_id',TRUE));
        $result = $this->Common_model->get_data_row('supplier',array('supplier_id'=>$supplier_id));
        echo json_encode($result);
    }

    

    public function crossed_expected_delivery_date()
    {
        $data['nestedView']['heading']="Crossed Expected Delivery Date";
        $data['nestedView']['cur_page'] = 'crossed_expected_delivery_date';
        $data['nestedView']['parent_page'] = 'crossed_expected_delivery_date';

        # include files
        $data['nestedView']['css_includes'] = array();

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Crossed Expected Delivery Date';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Crossed Expected Delivery Date','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'order'          => validate_string($this->input->post('order_no', TRUE)),
            'order_type'     => validate_string($this->input->post('order_type', TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
            'order'          => $this->session->userdata('order'),
            'order_type'     => $this->session->userdata('order_type')
                              );
            }
            else {
                $searchParams=array(
                'order'         => '',
                'order_type'    => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'crossed_expected_delivery_date/';
        # Total Records
        $config['total_rows'] = $this->Calibration_m->crossed_expected_delivery_total_num_rows($searchParams); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->crossed_expected_delivery_results($current_offset, $config['per_page'], $searchParams);
        //echo $this->db->last_query(); exit();
        # Additional data
        //$cal_required_tools=array();
        foreach($calibration_results as $key=>$value)
        {
            $calibration_results[$key]['expected_delivery_date']=$this->Calibration_m->fe_not_acknowledged_tools($value['tool_order_id']);
        }

        //print_r($calibration_results); exit;
        $data['calibration_results']=$calibration_results;
        $data['order_delivery_type']=$this->Common_model->get_data('order_delivery_type',array('status'=>1)); 
        $data['displayResults'] = 1;

        $this->load->view('calibration/crossed_expected_delivery_date_list',$data);
    }
    
    public function cal_invoice_print()
    {
        #page authentication
        $task_access = page_access_check('wh_calibration');

        $rc_order_id = storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'wh_calibration'); exit();
        }
        $_SESSION['page_redirect_cal'] = 1;
        $from_address = $this->Calibration_m->get_from_address($rc_order_id); 
        $rc_order_arr = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id)); 
        $data['to_address'] = $rc_order_arr;
        $tools_list = $this->Calibration_m->get_calibration_tool($rc_order_id);
        $country_id = $rc_order_arr['country_id'];
        $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id,'status'=>1),'name');
        $total_amount = 0;
        foreach ($tools_list as $key => $value)
        {
            $cost = $value['cost_in_inr'];
            $gst_percent = $value['gst_percent'];
            $gst = explode("%", $gst_percent);
            $cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
            if($from_address['print_type'] == 3)
            {
                $cgst_percent = round($gst[0]/2,2);
                $cgst_amt = ($cost*$cgst_percent)/100;
                $sgst_percent = $cgst_percent;
                $sgst_amt = $cgst_amt; 
            }
            else if($from_address['print_type'] == 4)
            {
                $igst_percent = $gst[0];
                $igst_amt = ($cost*$igst_percent)/100;   
            }
            $tax_amount = ($cost * $gst[0])/100;
            $amount = $cost+$tax_amount;
            $total_amount+= ($cost+$tax_amount);
            $tools_list[$key]['cgst_percent'] = $cgst_percent;
            $tools_list[$key]['cgst_amt'] = $cgst_amt;
            $tools_list[$key]['sgst_percent'] = $sgst_percent;
            $tools_list[$key]['sgst_amt'] = $sgst_amt;
            $tools_list[$key]['igst_percent'] = $igst_percent;
            $tools_list[$key]['igst_amt'] = $igst_amt;
            $tools_list[$key]['taxable_value'] = $cost;
            $tools_list[$key]['tax_amount'] = $tax_amount;
            $tools_list[$key]['amount'] = $amount;
        }
        $data['total_amount'] = $total_amount;
        $data['from_address'] = $from_address;
        $data['tools_list'] = $tools_list;
        $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['billed_to']));

        $this->load->view('calibration/dc_print',$data);
    }
    public function removeToolFromCart()
    {
        $rc_asset_id=validate_number($this->input->post('rc_asset_id',TRUE));
        if(isset($_SESSION['rc_asset_id'][$rc_asset_id]))
        {
            unset($_SESSION['rc_asset_id'][$rc_asset_id]);
            echo 1;
        }
    }


    public function wh_calibration()
    {
        unset($_SESSION['scanned_assets_list']);
        $data['nestedView']['heading']="Calibration Shipment Requests";
        $data['nestedView']['cur_page'] = 'wh_calibration';
        $data['nestedView']['parent_page'] = 'wh_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Calibration Shipment Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Shipment Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $ship_by_date = validate_string($this->input->post('ship_by_date',TRUE));
            if($ship_by_date != '')
            {
                $date = strtotime("+2 days", strtotime($ship_by_date));
                $ship_by_date =  date("Y-m-d", $date);
            }
            $searchParams=array(
                'crb_number'   => validate_string($this->input->post('crb_number', TRUE)),
                'supplier_id'  => validate_number($this->input->post('supplier_id', TRUE)),
                'ship_by_date' => $ship_by_date,
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'whc_id'       => validate_number(@$this->input->post('wh_id',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'crb_number'   => $this->session->userdata('crb_number'),
                    'supplier_id'  => $this->session->userdata('supplier_id'),
                    'ship_by_date' => $this->session->userdata('ship_by_date'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'crb_number'        => '',
                    'supplier_id'       => '',
                    'ship_by_date'      => '',
                    'page_redirect_cal' => '',
                    'asset_number'      => '',
                    'rc_number'         => '',
                    'country_id'        => '',
                    'whc_id'            => '',
                    'country_id'        => '',
                    'serial_no'         => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'wh_calibration/';
        # Total Records
        $config['total_rows'] = $this->Calibration_m->wh_calibration_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_m->wh_calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);

        if(count($calibration_results)>0)
        {
            foreach ($calibration_results as $key => $value) 
            {
                $cal_asset = $this->Calibration_m->get_cal_asset_list($value['rc_order_id']);
                $check = $this->Calibration_m->check_scanned_asset_list($value['rc_order_id']);
                $calibration_results[$key]['check_scanned_asset'] = $check;
                $calibration_results[$key]['cal_asset'] = $cal_asset;
            }
        }
        
        # Additional data
        $data['calibration_results'] = $calibration_results;
        $data['supplier_list'] = $this->Common_model->get_data('supplier',array('status'=>1,'task_access'=>$task_access,'calibration' => 1));
        $data['warehouse'] = $this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['current_offset'] = $current_offset;
        $data['task_access'] = $task_access;

        $this->load->view('calibration/wh_calibration_request_list',$data);
    }

    public function wh_calibration_wizard()
    {
        $rc_order_id = @storm_decode($this->uri->segment(2));
        $page_redirect_cal = $this->session->userdata('page_redirect_cal');
        if($rc_order_id == '' || $page_redirect_cal == 1)
        {
            redirect(SITE_URL.'wh_calibration');
            exit;
        }  

        $data['nestedView']['heading']="Generate Calibration Shipment";
        $data['nestedView']['cur_page'] = 'check_calibration';
        $data['nestedView']['parent_page'] = 'wh_calibration';
        $data['enableFormWizard'] = 1;

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux-responsive.min.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/fuelux/loader.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/wh_calibration_wizard.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Generate Calibration Shipment';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Shipment Requests','class'=>'','url'=>SITE_URL.'wh_calibration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Calibration Shipment','class'=>'active','url'=>'');

        #additional data
        $crow = $this->Calibration_m->get_cal_asset_list($rc_order_id);
        $check_no = 0;
        foreach ($crow as $key => $value) 
        {
            $result = get_rc_scanned_assets_details($value['rc_asset_id']);
            $check = $result['asset_count'];
            if($check == 0)
            {
                $check_no++;
                $crow[$key]['check_scan'] = 0;
            }
            else
            {
                $crow[$key]['check_scan'] = 1;
            }
        }

        if($check_no>0)
        {
            $check_no = 1;
        }
        $data['check_no'] = $check_no;
        $rc_order_arr = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $country_id = $rc_order_arr['country_id'];
        $data['country_id'] = $country_id;
        $swh_id = $rc_order_arr['wh_id'];
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$swh_id));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
        $data['rc_order']= $this->Calibration_m->get_cal_sup_detials($rc_order_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>4));
        $data['docs'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['flg'] = 1;
        $data['crow'] = $crow;
        $data['form_action'] = SITE_URL.'insert_wh_cal_wizard';
        $this->load->view('calibration/wh_calibration_wizard',$data);
    }

    public function scan_cal_asset()
    {
        $rc_asset_id = @storm_decode($this->uri->segment(2));

        if($rc_asset_id == '')
        {
            redirect(SITE_URL.'wh_calibration');
            exit;
        } 
       
        $crow = $this->Calibration_m->get_cal_asset_detail($rc_asset_id);
        $data['nestedView']['heading']="Scan QR Code";
        $data['nestedView']['cur_page'] = 'check_calibration';
        $data['nestedView']['parent_page'] = 'wh_calibration';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Scan QR Code';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Shipment Requests','class'=>'','url'=>SITE_URL.'wh_calibration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Calibration Shipment','class'=>'','url'=> SITE_URL.'wh_calibration_wizard/'.storm_encode($crow['rc_order_id']));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scan QR Code','class'=>'active','url'=>'');

        #additional data
        $data['crow'] = $crow;
        $this->load->view('calibration/wh_scan_cal_asset',$data);
    }

    public function scanned_cal_asset_detials()
    {
        
        #page authentication
        $task_access = page_access_check('wh_calibration');

        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        $rc_asset_id = validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        $country_id = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'country_id');
        $asset_number = validate_string($this->input->post('asset_number',TRUE));
        $asset_number = trim(@$asset_number);

        $asset_id = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number,'country_id'=>$country_id),'asset_id');

        $check_asset = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id),'rc_asset_id');
        if($check_asset == '')
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Invalid Asset Number:<strong> '.$asset_number.' </strong>!</div>'); 
            redirect(SITE_URL.'scan_cal_asset/'.storm_encode($rc_asset_id)); exit();
        }

        /*koushik 11-10-2018 check asset is involved in ST or not */
        $st_entry = check_for_st_entry($asset_id);
        if(count($st_entry)>0)
        {
            $stn_number = $st_entry['stn_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
            redirect(SITE_URL.'scan_cal_asset/'.storm_encode($rc_asset_id)); exit();
        }
        else
        {

            $crow = $this->Calibration_m->get_cal_asset_detail($rc_asset_id);
            $data['nestedView']['heading']="Scanned Asset Details";
            $data['nestedView']['cur_page'] = 'check_calibration';
            $data['nestedView']['parent_page'] = 'wh_calibration';

            #page authentication
            $task_access = page_access_check($data['nestedView']['parent_page']);

            # include files
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/health.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = "Asset Details : ".$asset_number."";
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Shipment Requests','class'=>'','url'=>SITE_URL.'wh_calibration');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Calibration Shipment','class'=>'','url'=> SITE_URL.'wh_calibration_wizard/'.storm_encode($crow['rc_order_id']));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scan QR Code','class'=>'','url'=> SITE_URL.'scan_cal_asset/'.storm_encode($rc_asset_id));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scanned Asset Details','class'=>'active','url'=>'');

            #additional data
            $current_stage_id = 11;
            $rc = $this->Calibration_m->get_rcsh_by_rc_asset($rc_asset_id,$current_stage_id);
            $rcah_id = $this->Common_model->get_value('rc_asset_history',array('rcsh_id'=>$rc['rcsh_id']),'rcah_id');
            $asset_details = $this->Calibration_m->get_asset_part_details($asset_id);
            foreach ($asset_details as $key => $value) 
            {
                $asset_details[$key]['status'] = $this->Common_model->get_value('rc_asset_health',array('part_id'=>$value['part_id'],'rcah_id'=>$rcah_id),'asset_condition_id');
            }
            $data['asset_details'] = $asset_details;
            $data['crow'] = $crow;
            
            $this->load->view('calibration/wh_scan_cal_asset',$data);
        }
    }

    public function insert_scanned_cal_asset()
    {
        #page authentication
        $task_access = page_access_check('wh_calibration');

        $rc_asset_id = validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        $asset_number = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'asset_number');
        $asset_conditon = $this->input->post('asset_condition_id',TRUE);
        $remarks = $this->input->post('remarks',TRUE);
        if($rc_asset_id == '' || $asset_id == '')
        {
            redirect(SITE_URL.'wh_calibration'); exit();
        }

        /*koushik 11-10-2018 check asset is involved in ST or not */
        $st_entry = check_for_st_entry($asset_id);
        if(count($st_entry)>0)
        {
            $stn_number = $st_entry['stn_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
            redirect(SITE_URL.'scan_cal_asset/'.storm_encode($rc_asset_id)); exit();
        }

        $part_list = array();
        $asset_details = $this->Calibration_m->get_asset_part_details($asset_id);
        foreach ($asset_details as $key => $value) 
        {
            $insert_rcahealth = array(
                'part_id'            => $value['part_id'],
                'asset_condition_id' => $asset_conditon[$value['part_id']],
                'remarks'            => $remarks[$value['part_id']]
            );
            $part_list[] = $insert_rcahealth;
        }

        $_SESSION['scanned_assets_list'][$rc_asset_id] = 
            array('asset_id'     => $asset_id,
                  'asset_number' => $asset_number,
                  'part_list_arr'=> $part_list);

        $rc_order_id = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_order_id');
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Asset: <strong>'.$asset_number.'</strong> Is Scanned Successfully!
        </div>');
        redirect(SITE_URL.'wh_calibration_wizard/'.storm_encode($rc_order_id)); exit();
    }

    public function insert_wh_cal_wizard()
    {
        #page authentication
        $task_access = page_access_check('wh_calibration');

        $rc_order_id = validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'wh_calibration'); exit();
        }
        $courier_type = validate_number($this->input->post('courier_type',TRUE));
        $courier_name = validate_string($this->input->post('courier_name',TRUE));
        $courier_number = validate_string($this->input->post('courier_number',TRUE));
        $contact_person = validate_string($this->input->post('contact_person',TRUE));
        $phone_number = validate_string($this->input->post('phone_number',TRUE));
        $shipment_date = validate_string(format_date($this->input->post('shipment_date',TRUE)));
        $doc_type = $this->input->post('document_type',TRUE);
        $vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
        $remarks        = validate_string($this->input->post('remarks',TRUE));
        if($remarks == ""){ $remarks = NULL; }

        if($courier_type == 1)
        {
            $phone_number   = NULL;
            $contact_person = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 2)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 3)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
        }

        $print_type = validate_number($this->input->post('print_type',TRUE));
        $this->db->trans_begin();
        $rco_wh_id=$this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'wh_id');
        $print_id = get_current_print_id($print_type,$rco_wh_id);

        $insert_rc_ship = array(
            'rc_order_id'   => $rc_order_id,
            'print_id'      => $print_id,
            'courier_name'  => $courier_name,
            'courier_type'  => $courier_type,
            'vehicle_number'=> $vehicle_number,
            'phone_number'  => $phone_number,
            'contact_person'=> $contact_person,
            'courier_number'=> $courier_number,
            'shipment_date' => $shipment_date,
            'remarks'       => $remarks,
            'billed_to'     => validate_number($this->input->post('billed_to',TRUE)),
            'created_by'    => $this->session->userdata('sso_id'),
            'created_time'  => date('Y-m-d H:i:s'),
            'status'        => 1
        );
        $rc_shipment_id = $this->Common_model->insert_data('rc_shipment',$insert_rc_ship);

        #Update Order Address
        $update_rco = array(
            'address1'      => validate_string($this->input->post('address1',TRUE)),
            'address2'      => validate_string($this->input->post('address2',TRUE)),
            'address3'      => validate_string($this->input->post('address3',TRUE)),
            'address4'      => validate_string($this->input->post('address4',TRUE)),
            'pin_code'      => validate_string($this->input->post('pin_code',TRUE)),
            'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
            'pan_number'    => validate_string($this->input->post('pan_number',TRUE)),
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $update_rco_where = array('rc_order_id'=>$rc_order_id,'status'=>1);
        $this->Common_model->update_data('rc_order',$update_rco,$update_rco_where);

        #print_history
        $insert_print_history = array(
            'print_id'       => $print_id,
            'remarks'        => 'Print Creation',
            'rc_shipment_id' => $rc_shipment_id,
            'created_by'     => $this->session->userdata('sso_id'),
            'created_time'   => date('Y-m-d H:i:s'),
            'status'         => 1
        );
        $this->Common_model->insert_data('print_history',$insert_print_history);

        if(@$doc_type[1]!='')
        {
            foreach ($doc_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'         => $_FILES['support_document_'.$key]['name'],
                    'rc_order_id'      => $rc_order_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
                if($value!='' && $support_document != '')
                {
                    $this->Common_model->insert_data('rc_doc',$insert_doc);
                }
            }
        }

        $rc_asset_list = $this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));

        foreach ($rc_asset_list as $rca) 
        {
            $rc_asset_id = $rca['rc_asset_id'];
            $result = get_rc_scanned_assets_details($rc_asset_id);
            $asset_id = $result['asset_id'];
            $asset_details = $result['part_list_arr'];

            $current_stage_id = 12;
            $rc_12 = $this->Calibration_m->get_rcsh_by_rc_asset($rc_asset_id,$current_stage_id);

            #insert_rc_asset_history
            $insert_rcah = array(
                'rc_asset_id' => $rc_asset_id,
                'rcsh_id'     => $rc_12['rcsh_id'],
                'created_by'  => $this->session->userdata('sso_id'),
                'created_time'=> date('Y-m-d H:i:s'),
                'status'      => 1,
                'remarks'     => 'Scanned At WH'
            );
            $rcah_id = $this->Common_model->insert_data('rc_asset_history',$insert_rcah);

            foreach ($asset_details as $key => $value) 
            {
                $insert_rcahealth = array(
                    'part_id'            => $value['part_id'],
                    'rcah_id'            => $rcah_id,
                    'asset_condition_id' => $value['asset_condition_id'],
                    'remarks'            => $value['remarks'],
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('rc_asset_health',$insert_rcahealth);
            }

            #update rcsh from 12 to 13
            $update_rcsh = array(
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $update_rcsh_where = array(
                'rcsh_id'       => $rc_12['rcsh_id'],
                'modified_time' => NULL
            );
            $this->Common_model->update_data('rc_status_history',$update_rcsh,$update_rcsh_where);

            #insert rcsh with 13 - at cal vendor
            $insert_rcsh = array(
                'rc_asset_id'      => $rc_asset_id,
                'current_stage_id' => 13,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1
            );
            $rcsh_id_13 = $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

            #update rc_asset with 13
            $update_rca = array(
                'current_stage_id' => 13,
                'modified_by'      => $this->session->userdata('sso_id'),
                'modified_time'    => date('Y-m-d H:i:s')
            );
            $update_rca_where = array(
                'rc_asset_id'=>$rca['rc_asset_id'],
                'rc_order_id'=>$rc_order_id
            );
            $this->Common_model->update_data('rc_asset',$update_rca,$update_rca_where);

            //insert rca history - with 13
            $insert_rcahis = array(
                'rc_asset_id' => $rca['rc_asset_id'],
                'rcsh_id'     => $rcsh_id_13,
                'created_by'  => $this->session->userdata('sso_id'),
                'created_time'=> date('Y-m-d H:i:s'),
                'status'      => 1
            );
            $rcah_id_13 = $this->Common_model->insert_data('rc_asset_history',$insert_rcahis);

            $rcah_id_12 = $this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rca['rc_asset_id'],'rcsh_id'=>$rc_12['rcsh_id']),'rcah_id');

            $rcahealth_12 = $this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id_12));

            foreach ($rcahealth_12 as $key => $value) 
            {
                //insert rca_health for 13
                $insert_rcahealth = array(
                    'rcah_id'            => $rcah_id_13,
                    'part_id'            => $value['part_id'],
                    'asset_condition_id' => $value['asset_condition_id'],
                    'remarks'            => $value['remarks'],
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('rc_asset_health',$insert_rcahealth);
            }
            
            //update asset_status_history 12
            $update_ash = array(
                'end_time'    => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('sso_id')
            );
            $update_ash_where = array(
                'asset_id'    => $rca['asset_id'],
                'end_time'    => NULL,
                'rc_asset_id' => $rca['rc_asset_id']
            );
            $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

            //insert asset_status_history with 13
            $insert_ash = array(
                'asset_id'         => $rca['asset_id'],
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1,
                'rc_asset_id'      => $rca['rc_asset_id'],
                'current_stage_id' => 13
            );
            $this->Common_model->insert_data('asset_status_history',$insert_ash);

            #audit data
            $old_data = array('tool_availability' => get_asset_position($rca['asset_id']));

            //update asset_position to in transit
            $update_ap = array('to_date' => date('Y-m-d H:i:s'));
            $update_ap_where = array('asset_id'=>$rca['asset_id'],'to_date'=>NULL);
            $this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);

            $supplier_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rca['rc_order_id']),'supplier_id');
            //insert asset_position from intransit to supplier
            $insert_ap = array(
                'asset_id'    => $rca['asset_id'], 
                'transit'     => 0,
                'supplier_id' => $supplier_id,
                'from_date'   => date('Y-m-d H:i:s'),
                'status'      => 1
            );
            $this->Common_model->insert_data('asset_position',$insert_ap);

            #audit data
            $new_data = array('tool_availability' => get_asset_position($rca['asset_id']));
            $remarks = "Went to Cal Vendor, CR No:".$rca['rc_number'];
            audit_data('asset_master',$rca['asset_id'],'asset',2,'',$new_data,array('asset_id'=>$rca['asset_id']),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$rca['country_id']);
        }
        //update rc_order from 1 to 2
        $update_rco = array('status'=>2);
        $update_rco_where = array('rc_order_id' => $rc_order_id);
        $this->Common_model->update_data('rc_order',$update_rco,$update_rco_where);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'wh_calibration'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Details Has been Submitted successfully!</div>'); 
            redirect(SITE_URL.'cal_invoice_print/'.storm_encode($rc_order_id)); 
        }
    }

    public function update_cal_print()
    {
        
        $rc_order_id = storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'closed_cal_delivery_list'); exit();
        }
        $data['nestedView']['heading']="Update Generated Print";
        $data['nestedView']['cur_page'] = "check_calibration";
        $data['nestedView']['parent_page'] = 'closed_cal_delivery_list';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/update_wh_calibration.js"></script>';

        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Calibration Shipment List','class'=>'','url'=>SITE_URL.'closed_cal_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Generated Print','class'=>'active','url'=>'');

        
        $crow = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));

        $wh_ship = $this->Common_model->get_data_row('rc_shipment',array('rc_order_id'=>$rc_order_id));

        #additional data
        $data['rc_order_id'] = $rc_order_id;
        $country_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'country_id');
        $data['crow'] = $crow;
        $data['wh_ship'] = $wh_ship;
        $data['print_list'] = $this->Common_model->get_data_row('print_format',array('print_id'=>$wh_ship['print_id']));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$crow['wh_id']));
        $this->load->view('calibration/update_cal_print',$data);
    }

    public function update_cal_print_details()
    {
        #page authentication
        $task_access = page_access_check('closed_cal_delivery_list');

        $rc_order_id = validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'closed_cal_delivery_list'); exit();
        }
        $rcs = $this->Common_model->get_data_row('rc_shipment',array('rc_order_id'=>$rc_order_id));
        $courier_type = validate_number($this->input->post('courier_type',TRUE));
        $courier_name = validate_string($this->input->post('courier_name',TRUE));
        $courier_number = validate_string($this->input->post('courier_number',TRUE));
        $contact_person = validate_string($this->input->post('contact_person',TRUE));
        $phone_number = validate_string($this->input->post('phone_number',TRUE));
        $shipment_date = validate_string(format_date($this->input->post('shipment_date',TRUE)));
        $vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
        $remarks        = validate_string($this->input->post('remarks',TRUE));
        $date = validate_string($this->input->post('print_date',TRUE));
        $print_date = date('Y-m-d',strtotime($date));
        if($remarks == ""){ $remarks = NULL; }

        if($courier_type == 1)
        {
            $phone_number   = NULL;
            $contact_person = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 2)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 3)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
        }
        $update_rcs = array(
            'rc_order_id'   => $rc_order_id,
            'courier_name'  => $courier_name,
            'courier_type'  => $courier_type,
            'vehicle_number'=> $vehicle_number,
            'phone_number'  => $phone_number,
            'contact_person'=> $contact_person,
            'courier_number'=> $courier_number,
            'shipment_date' => $shipment_date,
            'remarks'       => $remarks,
            'billed_to'     => validate_number($this->input->post('billed_to',TRUE)),
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s'),
            'status'        => 1);
        $update_rcs_where = array('rc_shipment_id'=>$rcs['rc_shipment_id']);
        $this->db->trans_begin();
        $this->Common_model->update_data('rc_shipment',$update_rcs,$update_rcs_where);

         #Update rc order
        $update_rco = array(
            'address1'      => validate_string($this->input->post('address1',TRUE)),
            'address2'      => validate_string($this->input->post('address2',TRUE)),
            'address3'      => validate_string($this->input->post('address3',TRUE)),
            'address4'      => validate_string($this->input->post('address4',TRUE)),
            'pin_code'      => validate_string($this->input->post('pin_code',TRUE)),
            'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
            'pan_number'    => validate_string($this->input->post('pan_number',TRUE)),
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s'));
        $update_rco_where = array('rc_order_id'=>$rc_order_id,'status'=>2);
        $this->Common_model->update_data('rc_order',$update_rco,$update_rco_where);

        #update print date
        update_print_format_date($print_date,$rcs['print_id']);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'closed_cal_delivery_list'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Details Has been Updated successfully!
            </div>'); 
            redirect(SITE_URL.'cal_invoice_print/'.storm_encode($rc_order_id)); 
        } 
    }

    public function cancel_cal_print()
    {
        $rc_order_id = storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'closed_cal_delivery_list'); exit();
        }
        $data['nestedView']['heading']="Cancel Generated Print";
        $data['nestedView']['cur_page'] = "check_calibration";
        $data['nestedView']['parent_page'] = 'closed_cal_delivery_list';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Cancel Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Calibration Shipment List','class'=>'','url'=>SITE_URL.'closed_cal_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Cancel Generated Print','class'=>'active','url'=>'');
        
        $print_id = $this->Common_model->get_value('rc_shipment',array('rc_order_id'=>$rc_order_id),'print_id');

        #additional data
        $data['rc_order_id'] = $rc_order_id;
        $data['crb_number'] = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $data['print_list'] = $this->Common_model->get_data_row('print_format',array('print_id'=>$print_id));
        $data['cancel_type_list'] = $this->Common_model->get_data('cancel_type',array('status'=>1));
        $this->load->view('calibration/cancel_cal_print',$data);
    }

    public function insert_cancel_cal_print()
    {
        #page authentication
        $task_access = page_access_check('closed_cal_delivery_list');

        $rc_order_id = validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'closed_cal_delivery_list'); exit;
        }
        $rcs = $this->Common_model->get_data_row('rc_shipment',array('rc_order_id'=>$rc_order_id));

        $update_print_history = array('remarks'=>validate_string($this->input->post('reason',TRUE)),
                                      'modified_by' => $this->session->userdata('sso_id'),
                                      'modified_time' => date('Y-m-d H:i:s'));
        $update_print_where = array('rc_shipment_id'=>$rcs['rc_shipment_id'],'modified_time'=>NULL);
        $this->db->trans_begin();
        $this->Common_model->update_data('print_history',$update_print_history,$update_print_where);

        $print_type = validate_number($this->input->post('print_type',TRUE));
        $rco_wh_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'wh_id');
        $print_id = get_current_print_id($print_type,$rco_wh_id);

        $update_rcs = array('print_id'      => $print_id,
                            'modified_by'   => $this->session->userdata('sso_id'),
                            'modified_time' => date('Y-m-d H:i:s'));
        $update_rcs_where = array('rc_shipment_id'=>$rcs['rc_shipment_id']);
        $this->Common_model->update_data('rc_shipment',$update_rcs,$update_rcs_where);

        $insert_print_history = array('print_id' => $print_id,
                                      'remarks'  => 'Print Got updated',
                                      'rc_shipment_id' => $rcs['rc_shipment_id'],
                                      'created_by' => $this->session->userdata('sso_id'),
                                      'created_time' => date('Y-m-d H:i:s'));
        $this->Common_model->insert_data('print_history',$insert_print_history);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
        redirect(SITE_URL.'closed_cal_delivery_list'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Print Has been Updated successfully!
         </div>'); 
            redirect(SITE_URL.'cal_invoice_print/'.storm_encode($rc_order_id)); 
        } 
    }

    public function deactivate_calibration_delivery_doc()
    {

        $calibration_doc_id = validate_number($this->input->post('doc_id',TRUE));
        $update_data = array('status'=>2);
        $update_where = array('rcd_id'=>$calibration_doc_id);
        $res = $this->Common_model->update_data('rc_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }
    public function activate_calibration_delivery_doc()
    {
        $calibration_doc_id = validate_number($this->input->post('doc_id',TRUE));
        $update_data = array('status'=>1);
        $update_where = array('rcd_id'=>$calibration_doc_id);
        $res = $this->Common_model->update_data('rc_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    // modified by maruthi on 15th august'17 5:30PM
     public function calibration_required_tools()
    {
        $task_access = page_access_check('calibration_required_tools');
        $data['nestedView']['heading']="Calibration required Tools";
        $data['nestedView']['cur_page'] = 'calibration_required_tools';
        $data['nestedView']['parent_page'] = 'calibration_required_tools';

        # include files
        $data['nestedView']['css_includes'] = array();

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Calibration required Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration required Tools','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'order'          => validate_string($this->input->post('order_no', TRUE)),
                'order_type'     => validate_string($this->input->post('order_type', TRUE)),
                'crt_sso_id'     => validate_string($this->input->post('crt_sso_id', TRUE)),
                'crt_country_id' => validate_string($this->input->post('crt_country_id', TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order'      => $this->session->userdata('order'),
                'order_type' => $this->session->userdata('order_type'),
                'crt_sso_id' => $this->session->userdata('crt_sso_id'),
                'crt_country_id' => $this->session->userdata('crt_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'order'      => '',
                'order_type' => '',
                'crt_sso_id' => '',
                'crt_country_id' => ''
                );
                $this->session->set_userdata($searchParams);
            }  
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'calibration_required_tools/';
        # Total Records
        $config['total_rows'] = $this->Calibration_m->calibration_required_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['calibration_results'] = $this->Calibration_m->calibration_required_results($current_offset, $config['per_page'], $searchParams,$task_access);

        $cal_orders = array();
        if(count($data['calibration_results'])>0)
        {
            foreach($data['calibration_results'] as $key =>$value)
            {
                $cal_required_tools=$this->Calibration_m->cal_required_tools($value['tool_order_id']);
                $data['calibration_results'][$key]['tool_details'] = $cal_required_tools;
            }
        }
        $data['task_access']=  $task_access;
        //echo $task_access;exit;
        $data['users']=get_fe_users($task_access);
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['order_delivery_type']=$this->Common_model->get_data('order_delivery_type',array('status'=>1)); 

        $this->load->view('calibration/calibration_required_tool_list',$data);
    }
}