<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Defective_asset extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Defective_asset_m');
	}
	// Srilekha
	public function defective_asset()
	{
        $data['nestedView']['heading']="Defective Asset Requests";
		$data['nestedView']['cur_page'] = 'defective_asset';
		$data['nestedView']['parent_page'] = 'defective_asset';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/defective_asset.js"></script>';
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Defective Asset Requests';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Defective Asset Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'tool_no'      => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'    => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'whid'         => validate_number($this->input->post('wh_id',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'tool_no'      => $this->session->userdata('tool_no'),
                    'tool_desc'    => $this->session->userdata('tool_desc'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'whid'         => $this->session->userdata('whid'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else
            {
                $searchParams=array(
                    'tool_no'      => '',
                    'tool_desc'    => '',
                    'asset_number' => '',
                    'whid'         => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'defective_asset/';
        # Total Records
        $config['total_rows'] = $this->Defective_asset_m->defective_asset_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $repair_results = $this->Defective_asset_m->defective_asset_results($current_offset, $config['per_page'], $searchParams,$task_access);
        foreach($repair_results as $key=>$value)
        {
            $part_list=$this->Defective_asset_m->get_asset_part_list($value['defective_asset_id']);
            $repair_results[$key]['part_list']=$part_list;

            #check for asset participating in missed asset list
            $check_da = $this->Common_model->get_value('defective_asset',array('asset_id'=>$value['asset_id'],'status <'=>10,'type'=>2),'defective_asset_id');
            if($check_da!='')
            {
                $repair_results[$key]['check_missed'] = 1;
            }
            else
            {
                $repair_results[$key]['check_missed'] = 0;
            }
        }
        # Additional data
        $data['repair_results'] = $repair_results;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['whList']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['displayResults'] = 1;
        $data['flg']=1;

        $this->load->view('defective_asset/defective_asset_list',$data);
    }

    // To generate repair process
    public function generate_repair_process()
    {
        #page authentication
        $task_access = page_access_check('defective_asset');
        
        $defective_asset_id = storm_decode($this->uri->segment(2));
        $asset_id = $this->Common_model->get_value('defective_asset',array('defective_asset_id'=>$defective_asset_id),'asset_id');
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

        #check for EGM Calibration Entry if available cancel the request
        cancel_egm_cr_entry($asset_id);

        #audit data
        $old_data = array(
            'availability_status' => $asset_arr['availability_status'],
            'asset_status_id'     => $asset_arr['status'],
            get_da_key()          => get_da_status($asset_id,$asset_arr['approval_status'])
        );

        $country_id = $asset_arr['country_id'];
        $asset_number = $asset_arr['asset_number'];
        $part_list = $this->Common_model->get_data('defective_asset_health',array('defective_asset_id'=>$defective_asset_id));
        
        $this->db->trans_begin();
        // Dynamic CRB Number generation
        $number=get_repair_rc_number($country_id);
        if(count($number) == 0)
        {
            $f_order_number = "1-00001";
        }
        else
        {
            //echo $number['rcb_number'];exit;
            $num = ltrim($number['rc_number'],"R");
            //echo $num;exit;
            $result = explode("-", $num);
            $running_no = (int)$result[1];

            if($running_no == 99999)
            {
                $first_num = $result[0]+1;
                $second_num = get_running_sno_five_digit(1);  
            }
            else
            {
                $first_num = $result[0];
                $val = $running_no+1;
                $second_num = get_running_sno_five_digit($val);
            }
            $f_order_number= $first_num.'-'.$second_num;
       }
        $order_number = 'R'.$f_order_number;
        $rc_asset_data = array(
            'asset_id'          =>  $asset_id,
            'current_stage_id'  =>  20,
            'rc_number'         =>  $order_number,
            'country_id'        =>  $country_id,
            'defective_asset_id'=>  $defective_asset_id,
            'created_by'        =>  $this->session->userdata('sso_id'),
            'created_time'      =>  date('Y-m-d H:i:s'),
            'status'            =>  1,
            'rca_type'          =>  1
        );
        $repair_rc_asset_id = $this->Common_model->insert_data('rc_asset',$rc_asset_data);
        
        $rc_status_data=array(
            'rc_asset_id'      =>  $repair_rc_asset_id,
            'current_stage_id' =>  20,
            'created_by'       =>  $this->session->userdata('sso_id'),
            'created_time'     =>  date('Y-m-d H:i:s'),
            'status'           =>  1
        );
        $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rc_status_data);

        $rc_asset=array(
            'rc_asset_id'       =>  $repair_rc_asset_id,
            'rcsh_id'           =>  $new_rcsh_id,
            'created_by'        =>  $this->session->userdata('sso_id'),
            'created_time'      =>  date('Y-m-d H:i:s'),
            'status'            =>  1
        );
        $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rc_asset);

        foreach($part_list as $row)
        {
            $health_arr=array(
                'part_id'              =>  $row['part_id'],
                'asset_condition_id'   =>  $row['asset_condition_id'],
                'created_by'           =>  $this->session->userdata('sso_id'),
                'created_time'         =>  date('Y-m-d H:i:s'),
                'rcah_id'              =>  $new_rcah_id,
                'remarks'              =>  $row['remarks']
            );
            $this->Common_model->insert_data('rc_asset_health',$health_arr);
        }

        #audit data
        $update_a = array('approval_status' => 0,
                          'status'          => 10,
                          'availability_status' => 2,
                          'modified_by'     => $this->session->userdata('sso_id'),
                          'modified_time'   => date('Y-m-d H:i:s'));
        $update_a_where = array('asset_id'=>$asset_id);
        $this->Common_model->update_data('asset',$update_a,$update_a_where);

        #audit data
        $new_data = array(
            'availability_status' => 2,
            'asset_status_id'     => 10,
            get_da_key()          => get_da_status($asset_id,0)
        );
        $remarks = "Changed from Defective to Repair Request :".$order_number;
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$repair_rc_asset_id,$asset_arr['country_id']);

        $update_ash = array('end_time' => date('Y-m-d H:i:s'),
                            'modified_by' => $this->session->userdata('sso_id'));
        $update_ash_where = array('asset_id' => $asset_id);
        $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

        $insert_ash =array(
            'asset_id'         =>  $asset_id,
            'rc_asset_id'      =>  $repair_rc_asset_id,
            'current_stage_id' =>  20,
            'created_by'       =>  $this->session->userdata('sso_id'),
            'created_time'     =>  date('Y-m-d H:i:s'),
            'status'           =>  10
        );
        $this->Common_model->insert_data('asset_status_history',$insert_ash);

        $this->Common_model->update_data('defective_asset',array('request_handled_type'=>2),array('defective_asset_id'=>$defective_asset_id));
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'defective_asset');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Repair Process has been intiated successfully For Asset Number : <strong>'.$asset_number.'</strong> with RR Number : <strong>'.$order_number.'</strong> !
            </div>');            
            redirect(SITE_URL.'defective_asset');              
        }
    }

    // To generate calibration process
    public function generate_calibration_process()
    {
        #page authentication
        $task_access = page_access_check('defective_asset');

        $defective_asset_id=storm_decode($this->uri->segment(2));
        $asset_id=$this->Common_model->get_value('defective_asset',array('defective_asset_id'=>$defective_asset_id),'asset_id');

        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $country_id = $asset_arr['country_id'];
        $asset_number = $asset_arr['asset_number'];
        #audit data
        $old_data = array(
            get_da_key()          => get_da_status($asset_id,$asset_arr['approval_status'])
        );

        // Check Whether RC Number Has Raised Or Not
        $rc_check=check_for_rc_entry($row['asset_id']);
        if($rc_check == 0)
        {
            $part_list=$this->Common_model->get_data('defective_asset_health',array('defective_asset_id'=>$defective_asset_id));
            $this->db->trans_begin();
            // Dynamic CRB Number generation
            $number=get_rc_number($country_id);
            if(count($number) == 0)
            {
                $f_order_number = "1-00001";
            }
            else
            {
                //echo $number['rcb_number'];exit;
                $num = ltrim($number['rc_number'],"C");
                //echo $num;exit;
                $result = explode("-", $num);
                $running_no = (int)$result[1];

                if($running_no == 99999)
                {
                    $first_num = $result[0]+1;
                    $second_num = get_running_sno_five_digit(1);  
                }
                else
                {
                    $first_num = $result[0];
                    $val = $running_no+1;
                    $second_num = get_running_sno_five_digit($val);
                }
                $f_order_number= $first_num.'-'.$second_num;
            }
            $order_number = 'C'.$f_order_number;
            $rc_asset_data=array(
                'asset_id'          =>  $asset_id,
                'current_stage_id'  =>  11,
                'rc_number'         =>  $order_number,
                'country_id'        =>  $country_id,
                'created_by'        =>  $this->session->userdata('sso_id'),
                'created_time'      =>  date('Y-m-d H:i:s'),
                'status'            =>  1,
                'rca_type'          =>  2
            );
            $cal_rc_asset_id=$this->Common_model->insert_data('rc_asset',$rc_asset_data);
            
            $rc_status_data=array(
                'rc_asset_id'      =>  $cal_rc_asset_id,
                'current_stage_id' =>  11,
                'created_by'       =>  $this->session->userdata('sso_id'),
                'created_time'     =>  date('Y-m-d H:i:s'),
                'status'           =>  1
            );
            $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rc_status_data);
            $rc_asset=array(
                'rc_asset_id'       =>  $cal_rc_asset_id,
                'rcsh_id'           =>  $new_rcsh_id,
                'created_by'        =>  $this->session->userdata('sso_id'),
                'created_time'      =>  date('Y-m-d H:i:s'),
                'status'            =>  1
            );
            $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rc_asset);
            
            $this->Common_model->update_data('defective_asset',array('request_handled_type'=>3),array('defective_asset_id'=>$defective_asset_id));
            foreach($part_list as $row)
            {
                $health_arr=array(
                    'part_id'              =>  $row['part_id'],
                    'asset_condition_id'   =>  $row['asset_condition_id'],
                    'created_by'           =>  $this->session->userdata('sso_id'),
                    'created_time'         =>  date('Y-m-d H:i:s'),
                    'rcah_id'              =>  $new_rcah_id
                );
                $this->Common_model->insert_data('rc_asset_health',$health_arr);
            }
            
            $update_a = array('approval_status' => 0,
                              'modified_by'     => $this->session->userdata('sso_id'),
                              'modified_time'   => date('Y-m-d H:i:s'));
            $update_a_where = array('asset_id'=>$asset_id);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            #audit data
            $new_data = array(get_da_key() => get_da_status($asset_id,0));
            $remarks = "Changed from Defective to Cal Request :".$order_number;
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$repair_rc_asset_id,$asset_arr['country_id']);

            $update_ash = array('end_time' => date('Y-m-d H:i:s'),
                                'modified_by' => $this->session->userdata('sso_id'));
            $update_ash_where = array('asset_id' => $asset_id);
            $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

            $asset_status=array(
                'asset_id'          =>  $asset_id,
                'rc_asset_id'       =>  $cal_rc_asset_id,
                'current_stage_id'  =>  11,
                'created_by'        =>  $this->session->userdata('sso_id'),
                'created_time'      =>  date('Y-m-d H:i:s'),
                'status'            =>  1  
            );
            $this->Common_model->insert_data('asset_status_history',$asset_status);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Calibration Process has been intiated successfully for Asset Number : <strong>'.$asset_number.'</strong> with CR Number : <strong>'.$order_number.'</strong>!
                </div>');             
            }
        }
        else
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset Already have CR Request, Please Check ! </div>'); 
        }
        redirect(SITE_URL.'defective_asset'); 
    }

    // To Cancel Defective Asset
    public function cancel_defective_asset()
    {
        #page authentication
        $task_access = page_access_check('defective_asset');

        $defective_asset_id = storm_decode($this->uri->segment(2));
        $asset_id = $this->Common_model->get_value('defective_asset',array('defective_asset_id'=>$defective_asset_id),'asset_id');
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $asset_number = $asset_arr['asset_number'];

        #audit data
        $old_data = array(get_da_key() => get_da_status($asset_id,$asset_arr['approval_status']));

        $this->db->trans_begin();
        $update_data=array(
            'status'               =>  10,
            'request_handled_type' =>  10,  // modified by maruthi
            'modified_time'        =>  date('Y-m-d H:i:s'),
            'modified_by'          =>  $this->session->userdata('sso_id')
        );
        $this->Common_model->update_data('defective_asset',$update_data,array('defective_asset_id'=>$defective_asset_id));

        #check for asset participating in missed asset list
        $check_da = $this->Common_model->get_value('defective_asset',array('asset_id'=>$asset_id,'status <'=>10,'type'=>2),'defective_asset_id');
        if($check_da!='')
        {
            $approval_status = 1;
        }
        else
        {
            $approval_status = 0;
        }

        $asset_update_data=array(
            'approval_status' =>  $approval_status,
            'modified_time'   =>  date('Y-m-d H:i:s'),
            'modified_by'     =>  $this->session->userdata('sso_id')
        );
        $this->Common_model->update_data('asset',$asset_update_data,array('asset_id'=>$asset_id));

        #audit data
        $new_data = array(get_da_key() => get_da_status($asset_id,$approval_status));
        $remarks = "Defective Request is Cancelled";
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'defective_asset',$defective_asset_id,$asset_arr['country_id']);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'defective_asset');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong>  Defective Asset Request has been Cancelled successfully For Asset Number <strong>'.$asset_number.'</strong> !
            </div>');            
            redirect(SITE_URL.'defective_asset');              
        }
    }

    // created by maruthi on 13th august'17 
    public function missed_asset()
    {
        $data['nestedView']['heading']="Missed Asset Requests";
        $data['nestedView']['cur_page'] = 'missed_asset';
        $data['nestedView']['parent_page'] = 'missed_asset';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Missed Asset Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Missed Asset Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'ma_wh_id'        => validate_number($this->input->post('ma_wh_id', TRUE)),
                'ma_tool_no'      => validate_string($this->input->post('tool_no', TRUE)),
                'ma_tool_desc'    => validate_string($this->input->post('tool_desc',TRUE)),
                'ma_asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'country_id'      => validate_string($this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'ma_wh_id'        => $this->session->userdata('ma_wh_id'),
                    'ma_tool_no'      => $this->session->userdata('ma_tool_no'),
                    'ma_tool_desc'    => $this->session->userdata('ma_tool_desc'),
                    'ma_asset_number' => $this->session->userdata('ma_asset_number'),
                    'country_id'      => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'ma_wh_id'        => '',
                    'ma_tool_no'      => '',
                    'ma_tool_desc'    => '',
                    'ma_asset_number' => '',
                    'country_id'      => '',
                    'serial_no'    => ''
                ); 
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'missed_asset/';
        # Total Records
        $config['total_rows'] = $this->Defective_asset_m->missed_asset_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $repair_results = $this->Defective_asset_m->missed_asset_results($current_offset, $config['per_page'], $searchParams,$task_access);
        foreach($repair_results as $key=>$value)
        {
            $part_list=$this->Defective_asset_m->get_asset_part_list($value['defective_asset_id']);
            $repair_results[$key]['part_list']=$part_list;
        }
        # Additional data
        $data['repair_results']=$repair_results;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['displayResults'] = 1;
        $data['flg']=1;

        $this->load->view('defective_asset/missed_asset_list',$data);
    }

    public function submit_missed_asset()
    {
        #page Authentication
        $task_access = page_access_check('missed_asset');
        $asset_id = $this->input->post('asset_id',TRUE);
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

        #audit data
        $old_data = array(
            'asset_status_id'     => $asset_arr['status'],
            'availability_status' => $asset_arr['availability_status'],
            'remarks'             => $asset_arr['remarks'],
            get_da_key()          => get_da_status($asset_id,$asset_arr['approval_status']),
            'tool_availability'   => get_asset_position($asset_id)
        );

        $defective_asset_id = $this->input->post('defective_asset_id',TRUE);
        $remarks = $this->input->post('remarks1',TRUE);
        $this->db->trans_begin();

        // update asset status to LOST
        $this->Common_model->update_data('asset',array('status'=>7,'availability_status'=>2,'remarks'=>$remarks,'approval_status'=>0),array('asset_id'=>$asset_id));

        $update_asset_position = array('to_date' => date('Y-m-d H:i:s'));
        $update_w = array('asset_id'=>$asset_id,'to_date'=>NULL);
        $this->Common_model->update_data('asset_position',$update_asset_position,$update_w);

        $wh_id = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'wh_id');
        $insert_asset_position = array(
            'asset_id'  => $asset_id,
            'transit'   => 1,
            'from_date' => date('Y-m-d H:i:s'),
            'status'    => 7
        );
        $this->Common_model->insert_data('asset_position',$insert_asset_position);

        #audit data
        $new_data = array(
            'asset_status_id'     => 7,
            'availability_status' => 2,
            'remarks'             => $remarks,
            get_da_key()          => get_da_status($asset_id,0),
            'tool_availability'   => get_asset_position($asset_id)
        );
        $remarks = "From Missed Asset Request Changed Asset status to Lost";
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'defective_asset',$defective_asset_id,$asset_arr['country_id']);

        // update defective asset
        $this->Common_model->update_data('defective_asset',array('status'=>10,'remarks'=>$remarks),array('defective_asset_id'=>$defective_asset_id));

        // update in part table
        $this->Common_model->update_data('part',array('status'=>3,'modified_by'=>$this->session->userdata('sso_id'),'modified_time'=>date('Y-m-d H:i:s')),array('asset_id'=>$asset_id));

        $ash_id = $this->Common_model->get_value('asset_status_history',array('end_time'=>NULL),'ash_id');
        $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));
        $asset_status_history_data = array(
            'status'       => 7,
            'asset_id'     => $asset_id,
            'created_by'   => $this->session->userdata('sso_id'),
            'created_time' => date('Y-m-d H:i:s')
        );
        $new_ash_id = $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>');
        }
        else
        {
            $this->db->trans_commit();
            $asset_number = $asset_arr['asset_number'];
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset : <strong>'.$asset_number.'</strong> has been Updated successfully with Asset Status : <strong>Lost</strong> !
            </div>');
        }
        redirect(SITE_URL.'missed_asset'); exit();
    }
}