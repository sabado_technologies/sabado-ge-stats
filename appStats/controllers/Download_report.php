<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Download_report extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Download_report_m');
	}
	public function download_report()
	{
		$_SESSION['check_print_val'] = 0;
		$data['nestedView']['heading']="Download Report";
		$data['nestedView']['cur_page'] = 'download_report';
		$data['nestedView']['parent_page'] = 'download_report';

		#page Authentication
		$task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';
        
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/download_report.js"></script>';

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Download Report';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Download Report','class'=>'active','url'=>'');

		#additionl data
		$country_id = '';
		if($task_access!=3)
		{
			$country_id = $this->session->userdata('s_country_id');
		}
		else
		{
			if($_SESSION['header_country_id']!='')
			{
				$country_id = $this->session->userdata('header_country_id');
			}
		}
		$data['state_list'] = $this->Download_report_m->get_state_by_country($task_access,$country_id);
		$data['warehouse_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access),'','country_id ASC');
		$data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
		$data['country_id'] = $country_id;
		$data['task_access'] = $task_access;
		$this->load->view('download_report/download_report',$data);
	}

	public function download_report_format()
	{
		if(validate_number($this->input->post('submit'))!='')
		{
			$download     = validate_number($this->input->post('download',TRUE));
			$type         = validate_number($this->input->post('type',TRUE));
			$register     = validate_number($this->input->post('register',TRUE));
			$cancel_type  = validate_number($this->input->post('cancel_type',TRUE));
			$state_id     = validate_number($this->input->post('state_id',TRUE));
			$wh_id        = validate_number($this->input->post('wh_id',TRUE));
			$fd           = validate_string($this->input->post('from_date',TRUE));
			$td           = validate_string($this->input->post('to_date',TRUE));
			$print_number = validate_string($this->input->post('print_number',TRUE));
			$country_id   = validate_number($this->input->post('country_id',TRUE));

			if($fd != '') { $from_date = format_date($fd); }
			else { $from_date = '';}
			
			if($td != '') { $to_date = format_date($td); }
			else { $to_date = '';}
			
			$searchParams = array(
				'download'     => $download,
				'type'         => $type,
				'register'     => $register,
				'cancel_type'  => $cancel_type,
				'state_id'     => $state_id,
				'wh_id'	       => $wh_id,
				'from_date'    => $from_date,
				'to_date'      => $to_date,
				'print_number' => $print_number,
				'country_id'   => $country_id);
			if($register == 1)
			{
				$result = array();
				$order_delivery_list = $this->Download_report_m->get_od($searchParams);
				if(count($order_delivery_list)>0)
				{
					foreach ($order_delivery_list as $key => $val) 
					{
						$tools_list = $this->Download_report_m->get_od_tools($val['tool_order_id']);

				        $total_amount = 0;
				        foreach ($tools_list as $key1 => $value) 
				        {
				            $cost = $value['cost_in_inr'];

				            $gst_percent = $value['gst_percent'];
				            $gst = explode("%", $gst_percent);
				            $cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
				            if($val['print_type'] == 3)
				            {
				                $cgst_percent = round($gst[0]/2,2);
				                $cgst_amt = ($cost*$cgst_percent)/100;
				                $sgst_percent = $cgst_percent;
				                $sgst_amt = $cgst_amt; 
				            }
				            else if($val['print_type'] == 4)
				            {
				                $igst_percent = $gst[0];
				                $igst_amt = ($cost*$igst_percent)/100;   
				            }

				            if($val['print_type']==1 || $val['print_type']==3 || $val['print_type']==4)
				            {
				                $tax_amount = ($cost * $gst[0])/100;
				            }
				            else if($val['print_type']==2)
				            {
				                $tax_amount = 0;
				            }
				            $amount = $cost+$tax_amount;
				            $total_amount+=($cost+$tax_amount);
				            $tools_list[$key1]['cgst_percent'] = $cgst_percent;
				            $tools_list[$key1]['cgst_amt'] = $cgst_amt;
				            $tools_list[$key1]['sgst_percent'] = $sgst_percent;
				            $tools_list[$key1]['sgst_amt'] = $sgst_amt;
				            $tools_list[$key1]['igst_percent'] = $igst_percent;
				            $tools_list[$key1]['igst_amt'] = $igst_amt;
				            $tools_list[$key1]['taxable_value'] = $cost;
				            $tools_list[$key1]['tax_amount'] = $tax_amount;
				            $tools_list[$key1]['amount'] = $amount;
				        }
				        $result[$key] = $val;
				        $result[$key]['total_amount'] = $total_amount;
				        $result[$key]['tools_list'] = $tools_list;
				    }
				}
		
				$count_result = count($result);
				$rc_list = $this->Download_report_m->get_rc($searchParams);
				if(count($rc_list)>0)
				{
					foreach ($rc_list as $key => $val) 
					{
						$tools_list = $this->Download_report_m->get_rc_tools($val['rc_order_id']);

				        $total_amount = 0;
				        foreach ($tools_list as $key1 => $value) 
				        {
				            $cost = $value['cost_in_inr'];
				            $gst_percent = $value['gst_percent'];
				            $gst = explode("%", $gst_percent);
				            $cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
				            if($val['print_type'] == 3)
				            {
				                $cgst_percent = round($gst[0]/2,2);
				                $cgst_amt = ($cost*$cgst_percent)/100;
				                $sgst_percent = $cgst_percent;
				                $sgst_amt = $cgst_amt; 
				            }
				            else if($val['print_type'] == 4)
				            {
				                $igst_percent = $gst[0];
				                $igst_amt = ($cost*$igst_percent)/100;   
				            }

				            if($val['print_type']==1 || $val['print_type']==3 || $val['print_type']==4)
				            {
				                $tax_amount = ($cost * $gst[0])/100;
				            }
				            else if($val['print_type']==2)
				            {
				                $tax_amount = 0;
				            }
				            $amount = $cost+$tax_amount;
				            $total_amount+=($cost+$tax_amount);
				            $tools_list[$key1]['cgst_percent'] = $cgst_percent;
				            $tools_list[$key1]['cgst_amt'] = $cgst_amt;
				            $tools_list[$key1]['sgst_percent'] = $sgst_percent;
				            $tools_list[$key1]['sgst_amt'] = $sgst_amt;
				            $tools_list[$key1]['igst_percent'] = $igst_percent;
				            $tools_list[$key1]['igst_amt'] = $igst_amt;
				            $tools_list[$key1]['taxable_value'] = $cost;
				            $tools_list[$key1]['tax_amount'] = $tax_amount;
				            $tools_list[$key1]['amount'] = $amount;
				        }
				        $result[$count_result] = $val;
				        $result[$count_result]['total_amount'] = $total_amount;
				        $result[$count_result]['tools_list'] = $tools_list;
				        $count_result++;
					}
				}
			}
			else if($register == 2)
			{
				$result = array();
				$return_order_list = $this->Download_report_m->get_return_order_list($searchParams);
				if(count($return_order_list)>0)
				{
					foreach ($return_order_list as $key => $val) 
					{
						$return_type_id = $val['return_type_id'];
						$result[$key] = $val;
						if($return_type_id == 1 || $return_type_id == 2)
						{

							$dat = $this->Download_report_m->get_to_address1($val['ro_to_wh_id']);
							$result[$key]['to_address1'] = $dat['to_address1'];
							$result[$key]['to_address2'] = $dat['to_address2'];
							$result[$key]['to_address3'] = $dat['to_address3'];
							$result[$key]['to_address4'] = $dat['to_address4'];
							$result[$key]['to_gst_number'] = $dat['to_gst_number'];
							$result[$key]['to_pan_number'] = $dat['to_pan_number'];
							$result[$key]['to_pin_code'] = $dat['to_pin_code'];
						}
						else if($return_type_id == 4)
						{
							$dat = $this->Download_report_m->get_to_address2($val['return_order_id']);
							$result[$key]['to_address1'] = $dat['to_address1'];
							$result[$key]['to_address2'] = $dat['to_address2'];
							$result[$key]['to_address3'] = $dat['to_address3'];
							$result[$key]['to_address4'] = $dat['to_address4'];
							$result[$key]['to_gst_number'] = $dat['to_gst_number'];
							$result[$key]['to_pan_number'] = $dat['to_pan_number'];
							$result[$key]['to_pin_code'] = $dat['to_pin_code'];
						}
						$rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$val['return_order_id']),'rto_id');
						$order_status_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id,'current_stage_id'=>8),'order_status_id');

						$tools_list = $this->Download_report_m->get_requested_tools($order_status_id);

				        $total_amount = 0;
				        foreach ($tools_list as $key1 => $value) 
				        {
				            $cost = $value['cost_in_inr'];
				            $gst_percent = $value['gst_percent'];
				            $gst = explode("%", $gst_percent);
				            $cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
				            if($val['print_type'] == 3)
				            {
				                $cgst_percent = round($gst[0]/2,2);
				                $cgst_amt = ($cost*$cgst_percent)/100;
				                $sgst_percent = $cgst_percent;
				                $sgst_amt = $cgst_amt; 
				            }
				            else if($val['print_type'] == 4)
				            {
				                $igst_percent = $gst[0];
				                $igst_amt = ($cost*$igst_percent)/100;   
				            }

				            if($val['print_type']==1 || $val['print_type']==3 || $val['print_type']==4)
				            {
				                $tax_amount = ($cost * $gst[0])/100;
				            }
				            else if($val['print_type']==2)
				            {
				                $tax_amount = 0;
				            }
				            $amount = $cost+$tax_amount;
				            $total_amount+=($cost+$tax_amount);
				            $tools_list[$key1]['cgst_percent'] = $cgst_percent;
				            $tools_list[$key1]['cgst_amt'] = $cgst_amt;
				            $tools_list[$key1]['sgst_percent'] = $sgst_percent;
				            $tools_list[$key1]['sgst_amt'] = $sgst_amt;
				            $tools_list[$key1]['igst_percent'] = $igst_percent;
				            $tools_list[$key1]['igst_amt'] = $igst_amt;
				            $tools_list[$key1]['taxable_value'] = $cost;
				            $tools_list[$key1]['tax_amount'] = $tax_amount;
				            $tools_list[$key1]['amount'] = $amount;
				        }
				        $result[$key]['total_amount'] = $total_amount;
				        $result[$key]['tools_list'] = $tools_list;
				    }
				}
				$count_result = count($result);
				$order_delivery_list = $this->Download_report_m->get_inward_od($searchParams);
				if(count($order_delivery_list)>0)
				{
					foreach ($order_delivery_list as $key => $val) 
					{
						$tools_list = $this->Download_report_m->get_od_tools($val['tool_order_id']);
				        $total_amount = 0;
				        foreach ($tools_list as $key1 => $value) 
				        {
				            $cost = $value['cost_in_inr'];
				            $gst_percent = $value['gst_percent'];
				            $gst = explode("%", $gst_percent);
				            $cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
				            if($val['print_type'] == 3)
				            {
				                $cgst_percent = round($gst[0]/2,2);
				                $cgst_amt = ($cost*$cgst_percent)/100;
				                $sgst_percent = $cgst_percent;
				                $sgst_amt = $cgst_amt; 
				            }
				            else if($val['print_type'] == 4)
				            {
				                $igst_percent = $gst[0];
				                $igst_amt = ($cost*$igst_percent)/100;   
				            }

				            if($val['print_type']==1 || $val['print_type']==3 || $val['print_type']==4)
				            {
				                $tax_amount = ($cost * $gst[0])/100;
				            }
				            else if($val['print_type']==2)
				            {
				                $tax_amount = 0;
				            }
				            $amount = $cost+$tax_amount;
				            $total_amount+=($cost+$tax_amount);
				            $tools_list[$key1]['cgst_percent'] = $cgst_percent;
				            $tools_list[$key1]['cgst_amt'] = $cgst_amt;
				            $tools_list[$key1]['sgst_percent'] = $sgst_percent;
				            $tools_list[$key1]['sgst_amt'] = $sgst_amt;
				            $tools_list[$key1]['igst_percent'] = $igst_percent;
				            $tools_list[$key1]['igst_amt'] = $igst_amt;
				            $tools_list[$key1]['taxable_value'] = $cost;
				            $tools_list[$key1]['tax_amount'] = $tax_amount;
				            $tools_list[$key1]['amount'] = $amount;
				        }
				        $result[$count_result] = $val;
				        $result[$count_result]['total_amount'] = $total_amount;
				        $result[$count_result]['tools_list'] = $tools_list;
				        $count_result++;
				    }
				}
			}
			if(count($result)==0)
			{
				$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> No Records Found with this combination.!</div>'); 
                redirect(SITE_URL.'download_report'); exit();
			}
			if($download == 1)//excel
			{
				if($register == 1)//Outward Register
				{
					$header = '';
		            $data ='';
		            $titles = array('Branch From','Customer Code','Customer Name','Billed To Customer Location','Ship To Customer Location','Customer GSTIN (Bill To.) ','Customer GSTIN (Ship To)','Bill From Location','Ship From Location','Bill From GSTIN','Ship From GSTIN','Request Date','Ship Date','Invoice Number','Invoice Date','Nature Of Supply (Goods/Services)','GE Item Code','HSN For Goods/Accounting Code For Services','Description Of Item','Quantity (As Received)','Unit Of Measurement (UoM)','Assessable/Taxable Value','SGST Rate','SGST Amount','CGST Rate','CGST Amount','IGST Rate','IGST Amount','Invoice Value In INR','Place Of Supply (Branch To)','Oracle Reference Number','Tool Order Number','Type','Remarks','Country Name');
		       	 	$data = '<table border="1">';
		            $data.='<thead>';

			            $data.='<tr>';
			            $data.= '<th align="center"></th>';
			            $data.= '<th align="center" colspan="6" style="background-color:#ffd699;">Stock Transfer Received Branch Details</th>';
			            $data.= '<th align="center" colspan="4" style="background-color:#5cd65c;">Stock Transfer Sending Branch Details</th>';
			            $data.='</tr>';

			            $data.='<tr>';
			            $inc = 1;
			            foreach ( $titles as $title)
			            {
			            	if($inc == 1)
			            	{
			            		$color = "#ffff33";
			            	}
			            	else if($inc>=2 && $inc<=7)
			            	{
			            		$color = "#ffd699";
			            	}
			            	else if($inc>=8 && $inc<=11)
			            	{
			            		$color = "#5cd65c";
			            	}
			            	else
			            	{
			            		$color = "#99ccff";
			            	}
			                $data.= '<th align="center" style="background-color:'.$color.';">'.$title.'</th>';
			                $inc++;
			            }
			            $data.='</tr>';

		            $data.='</thead>';
		            $data.='<tbody>';
		            if(count($result)>0)
		            {
		                foreach($result as $row)
		                {
		             		if(@$row['order_number']!='')
			                {
			                  $cust_code = @$row['fe_name'];
			                  $cust_name = @$row['fe_name'];
			                }
			                else if(@$row['stn_number']!='')
			                {

			                  if(@$row['fe_name1']!='' && @$row['fe_check']==1)
			                  {
			                    $cust_code = @$row['fe_name1'];
			                    $cust_name = @$row['fe_name1'];
			                  }
			                  else if(@$row['to_wh_name']!='' && @$row['fe_check']==0)
			                  {
			                    $cust_code = @$row['to_wh_name'];
			                    $cust_name = @$row['to_wh_name'];
			                  }
			                }
			                else if(@$row['supplier_name']!='')
			                {
			                  $cust_code = @$row['supplier_name'];
			                  $cust_name = @$row['supplier_name'];
			                }
			                else 
			                {
			                  $cust_code = @$row['fe_name'];
			                  $cust_name = @$row['fe_name'];
			                }

		             		$billed_to_address = 'WIPRO GE HEALTHCARE PRIVATE LIMITED'.'<br/>'.'C/O '.$row['bill_address1'].',<br/>'.$row['bill_address2'].',<br/>'.$row['bill_address3'].',<br/>'.$row['bill_address4'].',<br/>'.$row['bill_pin_code'];

		             		$to_address = 'WIPRO GE HEALTHCARE PRIVATE LIMITED'.'<br/>'.'C/O '.$row['to_address1'].',<br/>'.$row['to_address2'].',<br/>'.$row['to_address3'].',<br/>'.$row['to_address4'].',<br/>'.$row['to_pin_code'];

		             		$from_address = 'WIPRO GE HEALTHCARE PRIVATE LIMITED'.'<br/>'.'C/O '.$row['from_address1'].',<br/>'.$row['from_address2'].',<br/>'.$row['from_address3'].',<br/>'.$row['from_address4'].',<br/>'.$row['from_pin_code'];

		             		$inv_date = $row['created_time'];
		             		$invoice_date = full_indian_format($inv_date);
		             		$request_date_1 = full_indian_format($row['request_date_1']);
		             		$ship_date_1 = full_indian_format($row['ship_date_1']);

							if(@$row['order_number']!="")
							{
							$order_number = $row['order_number'];
							}
							else if(@$row['stn_number']!='')
							{
							$order_number = @$row['stn_number'];
							}
							else if(@$row['crn_number']!='')
							{
							$order_number = @$row['crn_number'];
							}
							else if(@$row['return_number'])
							{
							$order_number = @$row['return_number'];
							}
							else
							{
								$order_number = '-NA-';
							}

							if($type == 1)
							{
								$type = 'Invoice';
							}
							else if($type == 2)
							{
								$type = 'Delivery Challan';
							}

							if($row['modified_time'] !='')
							{
								$remarks = 'Cancelled '.$type;
							}
							else
							{
								$remarks = '';
							}

		                    $only = 1;
		                    $rowspan = count($row['tools_list']);
		                   	$total_amount = indian_format_price($row['total_amount']);
		                    foreach ($row['tools_list'] as $key1 => $value1) 
		                    {
		                    	$data.='<tr>';
		                    	$cgst_percent = indian_format_price(@$value1['cgst_percent']);
		                    	$sgst_percent = indian_format_price(@$value1['sgst_percent']);
		                    	$igst_percent = indian_format_price(@$value1['igst_percent']);
		                    	$cgst_amount    = indian_format_price(@$value1['cgst_amt']);
		                    	$sgst_amount    = indian_format_price(@$value1['sgst_amt']);
		                    	$igst_amount    = indian_format_price(@$value1['igst_amt']);
	                    		$taxable_value = indian_format_price(@$value1['taxable_value']);
	                    		
		                    	if($only == 1)
		                    	{
		                    		$data.='<td align="center" rowspan="'.$rowspan.'">'.$row['from_wh_name'].'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$cust_code.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$cust_name.'</td>';
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$billed_to_address.'</td>';
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$to_address.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['bill_gst_number'].'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['to_gst_number'].'</td>';
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$from_address.'</td>'; 
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$from_address.'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['from_gst_number'].'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['from_gst_number'].'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$request_date_1.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$ship_date_1.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['format_number'].'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$invoice_date.'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">Services</td>';

		                    	}
		                    	$data.='<td align="center">'.@$value1['part_number'].'</td>';
		                    	$data.='<td align="center">'.@$value1['hsn_code'].'</td>';
		                    	$data.='<td align="center">'.@$value1['part_description'].'</td>';
		                    	$data.='<td align="center">'.@$value1['quantity'].'</td>';
		                    	$data.='<td align="center">-NA-</td>';
		                    	$data.='<td align="center">'.$taxable_value.'</td>';
		                    	$data.='<td align="center">'.$sgst_percent.'</td>';
		                    	$data.='<td align="center">'.$sgst_amount.'</td>';
		                    	$data.='<td align="center">'.$cgst_percent.'</td>';
		                    	$data.='<td align="center">'.$cgst_amount.'</td>';
		                    	$data.='<td align="center">'.$igst_percent.'</td>';
		                    	$data.='<td align="center">'.$igst_amount.'</td>';
		                    	if($only == 1)
		                    	{
		                    		$data.='<td align="center" rowspan="'.$rowspan.'">'.$total_amount.'</td>';
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$to_address.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">-NA-</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$order_number.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$type.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$remarks.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['country_name'].'</td>';
		                    	}
		                    	$data.='</tr>';
		                    	$only++;
							}
		                }
		            }
		            else
		            {
		                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Results Found</td></tr>';
		            }
		            $data.='</tbody>';
		            $data.='</table>';

		            $time = date("Y-M-d H:i:s");
		            $xlFile='Outward_Register_Report ('.$type.')'.$time.'.xls'; 
		            header("Content-type: application/x-msdownload"); 
		            # replace excelfile.xls with whatever you want the filename to default to
		            header("Content-Disposition: attachment; filename=".$xlFile."");
		            header("Pragma: no-cache");
		            header("Expires: 0");
		            echo $data;
				}
				else if($register == 2)//Inward
				{
					$header = '';
		            $data ='';
		            $titles = array('Branch (Stock Received)','Vendor Code','Vendor Name','Billed From Vendor Location','Ship From Vendor Location','Vendor GSTIN (Bill From) ','Vendor GSTIN (Ship From)','Bill To Location','Ship To Location','GE GSTIN (Bill To )','GE GSTIN (Ship To)','Request Date','Ship Date','Invoice Number','Invoice Date','Nature Of Supply (Goods/Services)','GE Item Code','HSN For Goods/Accounting Code For Services','Description Of Item','Quantity (As Received)','Unit Of Measurement (UoM)','Assessable/Taxable Value','SGST Rate','SGST Amount','CGST Rate','CGST Amount','IGST Rate','IGST Amount','Invoice Value In INR','Place Of Supply (Branch To)','Oracle Reference Number','Tool Order Number','Type','Remarks','Country Name');
		       	 	$data = '<table border="1">';
		            $data.='<thead>';

			            $data.='<tr>';
			            $data.= '<th align="center"></th>';
			            $data.= '<th align="center" colspan="6" style="background-color:#5cd65c;">Stock Transfer Sending Branch Details</th>';
			            $data.= '<th align="center" colspan="4" style="background-color:#ffd699;">Stock Transfer Received Branch Details</th>';
			            $data.='</tr>';

			            $data.='<tr>';
			            $inc = 1;
			            foreach ( $titles as $title)
			            {
			            	if($inc == 1)
			            	{
			            		$color = "#ffff33";
			            	}
			            	else if($inc>=2 && $inc<=7)
			            	{
			            		$color = "#5cd65c";
			            	}
			            	else if($inc>=8 && $inc<=11)
			            	{
			            		$color = "#ffd699";
			            	}
			            	else
			            	{
			            		$color = "#99ccff";
			            	}
			                $data.= '<th align="center" style="background-color:'.$color.';">'.$title.'</th>';
			                $inc++;
			            }
			            $data.='</tr>';

		            $data.='</thead>';
		            $data.='<tbody>';
		            if(count($result)>0)
		            {
		                foreach($result as $row)
		                {
		             		if(isset($row['cust_code']))
		             		{ 
		             			$cust_code = @$row['cust_code']; 
		             		}
		             		else
		             		{ 
		             			$cust_code = @$row['sso_id']; 
		             		}

		             		if(isset($row['cust_name']))
		             		{ 
		             			$cust_name = @$row['cust_name']; 
		             		}
		             		else
		             		{ 
		             			$cust_name = @$row['sso_name']; 
		             		}

		             		$billed_to_address = 'WIPRO GE HEALTHCARE PRIVATE LIMITED'.'<br/>'.'C/O '.$row['bill_address1'].',<br/>'.$row['bill_address2'].',<br/>'.$row['bill_address3'].',<br/>'.$row['bill_address4'].',<br/>'.$row['bill_pin_code'];

		             		$to_address = 'WIPRO GE HEALTHCARE PRIVATE LIMITED'.'<br/>'.'C/O '.$row['to_address1'].',<br/>'.$row['to_address2'].',<br/>'.$row['to_address3'].',<br/>'.$row['to_address4'].',<br/>'.$row['to_pin_code'];

		             		$from_address = 'WIPRO GE HEALTHCARE PRIVATE LIMITED'.'<br/>'.'C/O '.$row['from_address1'].',<br/>'.$row['from_address2'].',<br/>'.$row['from_address3'].',<br/>'.$row['from_address4'].',<br/>'.$row['from_pin_code'];

		             		$inv_date = $row['created_time'];
		             		$invoice_date = full_indian_format($inv_date);
		             		$request_date_1 = full_indian_format($row['request_date_1']);
		             		$ship_date_1 = full_indian_format($row['ship_date_1']);

							if(@$row['order_number']!="")
							{
							$order_number = $row['order_number'];
							}
							else if(@$row['stn_number']!='')
							{
							$order_number = @$row['stn_number'];
							}
							else if(@$row['crn_number']!='')
							{
							$order_number = @$row['crn_number'];
							}
							else if(@$row['return_number'])
							{
							$order_number = @$row['return_number'];
							}
							else
							{
								$order_number = '-NA-';
							}

							if($type == 1)
							{
								$type = 'Invoice';
							}
							else if($type == 2)
							{
								$type = 'Delivery Challan';
							}

							if($row['modified_time'] !='')
							{
								$remarks = 'Cancelled '.$type;
							}
							else
							{
								$remarks = '';
							}

		                    
		                    $only = 1;
		                    $rowspan = count($row['tools_list']);
		                    $total_amount = indian_format_price($row['total_amount']);
		                    foreach ($row['tools_list'] as $key1 => $value1) 
		                    {
	                    		$taxable_value = indian_format_price($value1['taxable_value']);
	                    		$cgst_percent = indian_format_price(@$value1['cgst_percent']);
		                    	$sgst_percent = indian_format_price(@$value1['sgst_percent']);
		                    	$igst_percent = indian_format_price(@$value1['igst_percent']);
		                    	$cgst_amount    = indian_format_price(@$value1['cgst_amt']);
		                    	$sgst_amount    = indian_format_price(@$value1['sgst_amt']);
		                    	$igst_amount    = indian_format_price(@$value1['igst_amt']);
	                    		$taxable_value = indian_format_price(@$value1['taxable_value']);
	                    		$data.='<tr>';
		                    	if($only == 1)
		                    	{
		                    		$data.='<td align="center" rowspan="'.$rowspan.'">'.$row['wh_name'].'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$cust_code.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$cust_name.'</td>';
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$from_address.'</td>';
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$from_address.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['from_gst_number'].'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['from_gst_number'].'</td>';
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$billed_to_address.'</td>'; 
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$to_address.'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['bill_gst_number'].'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['to_gst_number'].'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$request_date_1.'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$ship_date_1.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['format_number'].'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$invoice_date.'</td>'; 
				                    $data.='<td align="center" rowspan="'.$rowspan.'">Services</td>'; 
		                    	}
		                    	$data.='<td align="center">'.$value1['part_number'].'</td>';
		                    	$data.='<td align="center">'.$value1['hsn_code'].'</td>';
		                    	$data.='<td align="center">'.$value1['part_description'].'</td>';
		                    	$data.='<td align="center">'.$value1['quantity'].'</td>';
		                    	$data.='<td align="center">-NA-</td>';
		                    	$data.='<td align="center">'.$taxable_value.'</td>';
		                    	$data.='<td align="center">'.$taxable_value.'</td>';
		                    	$data.='<td align="center">'.$sgst_percent.'</td>';
		                    	$data.='<td align="center">'.$sgst_amount.'</td>';
		                    	$data.='<td align="center">'.$cgst_percent.'</td>';
		                    	$data.='<td align="center">'.$cgst_amount.'</td>';
		                    	$data.='<td align="center">'.$igst_percent.'</td>';
		                    	$data.='<td align="center">'.$igst_amount.'</td>';
		                    	if($only == 1)
		                    	{
		                    		$data.='<td align="center" rowspan="'.$rowspan.'">'.$total_amount.'</td>';
				                    $data.='<td align="left" rowspan="'.$rowspan.'">'.$to_address.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">-NA-</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$order_number.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$type.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$remarks.'</td>';
				                    $data.='<td align="center" rowspan="'.$rowspan.'">'.$row['country_name'].'</td>';
		                    	}
		                    	$data.='</tr>';
		                    	$only++;
							}
		                    
		                }
		            }
		            else
		            {
		                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Results Found</td></tr>';
		            }
		            $data.='</tbody>';
		            $data.='</table>';

		            $time = date("Y-M-d H:i:s");
		            $xlFile='Inward_Register_Report ('.$type.')'.$time.'.xls'; 
		            header("Content-type: application/x-msdownload"); 
		            # replace excelfile.xls with whatever you want the filename to default to
		            header("Content-Disposition: attachment; filename=".$xlFile."");
		            header("Pragma: no-cache");
		            header("Expires: 0");
		            echo $data;
				}
			}
			else if($download == 2)//pdf
			{
				$_SESSION['check_print_val'] = 1;
				$data['result'] = $result;
				if($type == 1)//invoice
				{
					$this->load->view('download_report/download_invoice',$data);
				}
				else if($type == 2 || $type == 3 || $type == 4)//DC
				{
					$this->load->view('download_report/download_dc',$data);
				}
			}
		}
		else
		{
			redirect(SITE_URL.'download_report'); exit();
		}
	}

	public function get_state_by_country_dropdown()
	{
		$country_id = sanitize_output(validate_number($this->input->post('country_id',TRUE)));
		if($country_id!='')
		{
			$result1 = $this->Download_report_m->get_state_by_country_dropdown($country_id);
			$result2 = $this->Download_report_m->get_wh_by_state($country_id);
			$data = array('result1'=>$result1,'result2'=>$result2);
			echo json_encode($data);
		}
	}

	public function wh_by_state_dropdown()
	{
		$state_id = sanitize_output(validate_number($this->input->post('state_id',TRUE)));
		if($state_id!='')
		{
			$country_id = '';
			$result = $this->Download_report_m->get_wh_by_state($country_id,$state_id);
			$data = array('result1'=>$result);
			echo json_encode($data);
		}
	}
}