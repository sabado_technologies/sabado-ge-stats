<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Login extends CI_Controller {

	public  function __construct() 
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Login_model');
	}

	public function login()
	{
		# Data Array to carry the require fields to View and Model
		$data['heading']="Login";
		$data['pageTitle'] = 'STOrM - Login';
		$data['cur_page'] = 'login';
		$data['parent_page'] = 'login';

        # Load JS and CSS Files
        $data['js_includes'] = array();
        $data['css_includes'] = array();
        
        if($this->input->post('sso_id') != '')
        {
        	$sso_id = $this->input->post('sso_id', TRUE);
        	$password = $this->input->post('password', TRUE);
        	if($sso_id != '' && $password != '')
        	{
        		// Check if employee exists with the credentials given. Returns SSO ID if user exists and 0, if no SSO ID exists
        		$ssoid = $this->Login_model->getUserDetails($sso_id);
        		if(count($ssoid)>0)
        		{
					setcookie('SM_USER', $sso_id, time() + 3600, "/");
					redirect(SITE_URL.'home');
        		}
        		else
        		{
					$this->session->set_flashdata('response', '<div class="alert alert-danger alert-white rounded">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<i class="fa fa-times-circle"></i>
					<strong>Error!</strong> Username / Password do not match.
					</div>');
					redirect(SITE_URL.'login');
        		}
        	}
        	else
        	{
					$this->session->set_flashdata('response', '<div class="alert alert-danger alert-white rounded">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<i class="fa fa-times-circle"></i>
					<strong>Error!</strong> Username / Password Cannot be Empty.
					</div>');
					redirect(SITE_URL.'login');
        	}
        }
        else
        {
			$this->load->view('user/login',$data);
        }
	}
	public function logout()
	{
		if($this->session->userdata('main_sso_id')!='')
		{
			$log_qry = 'UPDATE m_user_log SET logout_time = "'.date('Y-m-d H:i:s').'" WHERE m_sso_id = '.$this->session->userdata('main_sso_id').' ORDER BY m_user_log_id DESC LIMIT 1';
			$this->db->query($log_qry);

			$log_qry = 'UPDATE user_log SET logout_time = "'.date('Y-m-d H:i:s').'" WHERE sso_id = '.$this->session->userdata('main_sso_id').' ORDER BY user_log_id DESC LIMIT 1';
			$this->db->query($log_qry);
		}
		$sess_items = array('sso_id','branch_id','branch_name','role_id','role_name','userFullName','desig_name','s_wh_name','s_wh_id','swh_id','s_location_id','l_modal','s_role_id','m_sso_id','sl_modal','tool_id','quantity','ChildRolesList','header_country_name','header_country_id','countryList','countriesIndexedArray','SM_USER','main_role_id','main_sso_id','display_sso_name','whsIndededArray');
 		$this->session->unset_userdata($sess_items);

		#redirecting to sso logoff to unset SSO details
		setcookie($this->config->item('unset_sso'),'', time()-(365*24*3600), "/");
		redirect(sso_logout_url()); exit();
	}

  	//mahesh 8th Dec 2016 12:30 pm , updated on:
  	public function unauthorized_request()
  	{
		$data['nestedView']['heading']="Unauthorized Request";
		$data['nestedView']['cur_page'] = 'unauthorized_request';
		$data['nestedView']['parent_page'] = 'unauthorized_request';
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['css_includes'] = array();
		
		$data['nestedView']['pageTitle'] = 'STOrM - Unauthorized Request';

		# Breadcrumbs
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
		$data['nestedView']['breadCrumbOptions'][] = array('label' => 'Unauthorized Request', 'class' => 'active', 'url' =>'');

		# Additional data
        $data['portlet_title'] = 'Unauthorized Request';

		$this->load->view('user/unauthorized_request',$data);
  	}

  	public function access_denied()
  	{
		$this->load->view('user/access_denied');
  	}

  	public function loggedOff()
  	{ 
		$this->load->view('user/loggedOff');
  	}
  	
  	public function roles()
	{
		$child_role_id = storm_decode($this->uri->segment(2));
		if($child_role_id == '')
		{
			header('Location: '.SITE_URL.'home');exit;
		}
		if(!isset($_SESSION['sso_id']))
		{
			$this->Common_model->sso_login_check();
		}
		else
		{
			$main_role_id = $this->session->userdata('main_role_id');
			$main_sso_id = $this->session->userdata('main_sso_id');
			$userDetails = $this->Login_model->getUserDetails($main_sso_id);
			$assign_whs_list = $this->Login_model->get_user_whs($userDetails['sso_id']);

			if($child_role_id == 'special')
			{
				$sessionData = array(
					'sl_modal'       => 2,
					'sso_id'         => $userDetails['sso_id'],
					'branch_id'      => $userDetails['branch_id'],
					'branch_name'    => $userDetails['branch_name'],
					'role_id'        => $_SESSION['role_id'],
					's_role_id'      => $userDetails['role_id'],
					'role_name'      => $_SESSION['role_name'],
					'userFullName'   => $userDetails['name'],
					'desig_name'     => $userDetails['designation_name'],
					's_wh_name'      => $userDetails['wh_name'],
					's_wh_id'        => $userDetails['wh_id'],
					'swh_id'         => $userDetails['wh_id'],
					's_location_id'  => $userDetails['location_id'],
					's_country_id'   => $userDetails['country_id'],
					'display_sso_name' => 'SSO ID'
				);
				#multiple warehouse assigning
				if(count($assign_whs_list)>1)
				{
					$sessionData['whsIndededArray'] = convertIntoIndexedArray($assign_whs_list,'wh_id');
				}
				else
				{
					$this->session->unset_userdata('whsIndededArray');
				}
		        $this->session->set_userdata($sessionData);
			}
			else if($child_role_id == $main_role_id)
			{
				$role_details = $this->Login_model->get_child_roles($userDetails['role_id']);
                $header_country_name = $this->Common_model->get_value('location',array('location_id'=>$userDetails['country_id']),'name');

                #multiple country assigning
                $countryList = array(); $countriesIndexedlist = ''; $header_country_id = '';
                if($userDetails['region_id']==3 && $userDetails['role_id']==4)
                {
                    $assign_countries_list = $this->Common_model->get_data('location',array('level_id'=>2,'region_id'=>$userDetails['region_id'],'status'=>1));
                }
                else
                {
                    $assign_countries_list = $this->Login_model->get_user_countries($userDetails['sso_id']);
                }
                if(count(@$assign_countries_list)>1)
                {
                    $countryList = $assign_countries_list;
                    $countriesIndexedlist = convertIntoIndexedArray($assign_countries_list,'location_id');
                }
                else
                {
                    $countriesIndexedlist = array($userDetails['country_id']);
                    $header_country_id = $userDetails['country_id'];
                }
                
                $sessionData = array(
                    'sso_id'         => $userDetails['sso_id'],
                    'main_sso_id'    => $userDetails['sso_id'],
                    'branch_id'      => $userDetails['branch_id'],
                    'branch_name'    => $userDetails['branch_name'],
                    'role_id'        => $userDetails['role_id'],
                    's_role_id'      => $userDetails['role_id'],
                    'main_role_id'   => $userDetails['role_id'],
                    'role_name'      => $userDetails['role_name'],
                    'userFullName'   => $userDetails['name'],
                    'desig_name'     => $userDetails['designation_name'],
                    's_wh_name'      => $userDetails['wh_name'],
                    's_wh_id'        => $userDetails['wh_id'],
                    'swh_id'         => $userDetails['wh_id'],
                    's_location_id'  => $userDetails['location_id'],
                    's_country_id'   => $userDetails['country_id'],
                    'ChildRolesList' => $role_details,
                    'countryList'    => $countryList,
                    'countriesIndexedArray' => $countriesIndexedlist,
                    'header_country_name' => $header_country_name,
                    'header_country_id' => $header_country_id,
                    'display_sso_name'  => 'SSO ID'
                );

                #multiple warehouse assigning
                if(count($assign_whs_list)>1)
                {
                    $sessionData['whsIndededArray'] = convertIntoIndexedArray($assign_whs_list,'wh_id');
                }
                else
                {
                    $this->session->unset_userdata('whsIndededArray');
                }
                $this->session->set_userdata($sessionData);
			}
			else if(count(@$_SESSION['ChildRolesList'])>1)
			{
				$role_name = $this->Common_model->get_value('role',array('role_id'=>$child_role_id),'name');
				#multiple country assigning
                $countryList = array(); $countriesIndexedlist = ''; $header_country_id = '';
                if($userDetails['region_id']==3 && $userDetails['role_id']==4)
                {
                    $assign_countries_list = $this->Common_model->get_data('location',array('level_id'=>2,'region_id'=>$userDetails['region_id'],'status'=>1));
                }
                else
                {
                    $assign_countries_list = $this->Login_model->get_user_countries($userDetails['sso_id']);
                }
                if(count(@$assign_countries_list)>1)
                {
                    $countryList = $assign_countries_list;
                    $countriesIndexedlist = convertIntoIndexedArray($assign_countries_list,'location_id');
                }
                else
                {
                    $countriesIndexedlist = array($userDetails['country_id']);
                    $header_country_id = $userDetails['country_id'];
                }

				$sessionData = array(
					'sl_modal'		 => 1,
                    'sso_id'         => $userDetails['sso_id'],
                    'role_id'        => $child_role_id,
                    'role_name'      => $role_name,
                    'userFullName'   => $userDetails['name'],
                    'countryList'    => $countryList,
                    'countriesIndexedArray' => $countriesIndexedlist,
                    'header_country_name' => $userDetails['country_name'],
                    'header_country_id' => $header_country_id,
                    'display_sso_name'  => 'SSO ID'
                );
            	$this->session->set_userdata($sessionData);
			}
			
			unset($_SESSION['tool_id']);
        	unset($_SESSION['quantity']);
			header('Location: '.SITE_URL.'home');exit;
		}		
	}

	public function sso_update()
    {
    	if(!isset($_SESSION['sso_id']))
		{
			$this->Common_model->sso_login_check();
		}

        $main_sso_id = $this->session->userdata('main_sso_id');
		$sso_id = validate_string($this->input->post('sso_id',TRUE));
		$userDetails = $this->Login_model->getUserDetails($sso_id);

		#multiple country assigning
        $countryList = array(); $countriesIndexedlist = ''; $header_country_id = '';
        if($userDetails['region_id']==3 && $userDetails['role_id']==4)
        {
            $assign_countries_list = $this->Common_model->get_data('location',array('level_id'=>2,'region_id'=>$userDetails['region_id'],'status'=>1));
        }
        else
        {
            $assign_countries_list = $this->Login_model->get_user_countries($userDetails['sso_id']);
        }
        if(count(@$assign_countries_list)>1)
        {
            $countryList = $assign_countries_list;
            $countriesIndexedlist = convertIntoIndexedArray($assign_countries_list,'location_id');
        }
        else
        {
            $countriesIndexedlist = array($userDetails['country_id']);
            $header_country_id = $userDetails['country_id'];
        }

		$sessionData = array(
            'sso_id'         => $userDetails['sso_id'],
            'branch_id'      => $userDetails['branch_id'],
            'branch_name'    => $userDetails['branch_name'],
            'role_id'        => $userDetails['role_id'],
            's_role_id'      => $userDetails['role_id'],
            'role_name'      => $userDetails['role_name'],
            'userFullName'   => $userDetails['name'],
            'desig_name'     => $userDetails['designation_name'],
            's_wh_name'      => $userDetails['wh_name'],
            's_wh_id'        => $userDetails['wh_id'],
            'swh_id'         => $userDetails['wh_id'],
            's_location_id'  => $userDetails['location_id'],
            's_country_id'   => $userDetails['country_id'],
            'countryList'    => $countryList,
            'countriesIndexedArray' => $countriesIndexedlist,
            'header_country_name' => $userDetails['country_name'],
            'header_country_id' => $header_country_id,
            'display_sso_name'  => 'On Behalf Of'
        );
		#multiple warehouse assigning
        $assign_whs_list = $this->Login_model->get_user_whs($userDetails['sso_id']);
        if(count($assign_whs_list)>1)
        {
            $sessionData['whsIndededArray'] = convertIntoIndexedArray($assign_whs_list,'wh_id');
        }
        else
        {
            $this->session->unset_userdata('whsIndededArray');
        }
        $this->session->set_userdata($sessionData);
        unset($_SESSION['tool_id']);
        unset($_SESSION['quantity']);

        $_SESSION['sl_modal'] = 2;
        $user_log_data = array(
            'sso_id'            => $sso_id,
            'm_sso_id'		    => $main_sso_id,
            'login_time'        => date('Y-m-d H:i:s'),
            'ip_address'        => get_client_ip(),
            'user_agent'        => $_SERVER['HTTP_USER_AGENT'],
            'last_activity'     => date('Y-m-d H:i:s')
	    );
	    $this->db->insert('m_user_log',$user_log_data);
        header('Location: '.SITE_URL.'home');exit;
    }

	public function countries()
	{
		$country_id = storm_decode($this->uri->segment(2));
		if($country_id == '')
		{
			header('Location: '.SITE_URL.'home');exit;
		}

		if(!isset($_SESSION['sso_id']))
		{
			$this->Common_model->sso_login_check();
		}

		$main_sso_id = $this->session->userdata('main_sso_id');
		$new_sso_id = $this->session->userdata('sso_id');
		$sso_id = ($main_sso_id == $new_sso_id)?$main_sso_id:$new_sso_id;
		$userDetails = $this->Login_model->getUserDetails($sso_id);

		if($country_id == '' || $country_id == 'NA')
		{
			#multiple country assigning
			$countryList = array(); $countriesIndexedlist = ''; $header_country_id = '';
			if($userDetails['region_id']==3 && $userDetails['role_id']==4)
            {
                $assign_countries_list = $this->Common_model->get_data('location',array('level_id'=>2,'region_id'=>$userDetails['region_id'],'status'=>1));
            }
            else
            {
                $assign_countries_list = $this->Login_model->get_user_countries($userDetails['sso_id']);
            }
			if(count(@$assign_countries_list)>1)
            {
                $countryList = $assign_countries_list;
                $countriesIndexedlist = convertIntoIndexedArray($assign_countries_list,'location_id');
            }
            else
            {
                $countriesIndexedlist = array($userDetails['country_id']);
                $header_country_id = $userDetails['country_id'];
            }
            
			$sessionData = array(
				'sl_modal'       => 2,
				'sso_id'         => $userDetails['sso_id'],
				'branch_id'      => $userDetails['branch_id'],
				'branch_name'    => $userDetails['branch_name'],
				'role_id'        => $_SESSION['role_id'],
				's_role_id'      => $userDetails['role_id'],
				'role_name'      => $_SESSION['role_name'],
				'userFullName'   => $userDetails['name'],
				'desig_name'     => $userDetails['designation_name'],
				's_wh_name'      => $userDetails['wh_name'],
				's_wh_id'        => $userDetails['wh_id'],
				'swh_id'         => $userDetails['wh_id'],
				's_location_id'  => $userDetails['location_id'],
				's_country_id'   => $userDetails['country_id'],
				'countryList'    => $countryList,
				'countriesIndexedArray' => $countriesIndexedlist,
				'header_country_name' => $userDetails['country_name'],
				'header_country_id' => $header_country_id,
				'display_sso_name' => ($main_sso_id == $new_sso_id)?'SSO ID':'On Behalf Of'
			);

			#multiple warehouse assigning
            $assign_whs_list = $this->Login_model->get_user_whs($userDetails['sso_id']);
            if(count($assign_whs_list)>1)
            {
                $sessionData['whsIndededArray'] = convertIntoIndexedArray($assign_whs_list,'wh_id');
            }
            else
            {
                $this->session->unset_userdata('whsIndededArray');
            }
			$this->session->set_userdata($sessionData);
		}
		else
		{
			$main_role_id = $this->session->userdata('main_role_id');
			$child_role_id = $this->session->userdata('role_id');
			if($userDetails['region_id']==3 && $userDetails['role_id']==4)
            {
                $assign_countries_list = $this->Common_model->get_data('location',array('level_id'=>2,'region_id'=>$userDetails['region_id'],'status'=>1));
            }
            else
            {
                $assign_countries_list = $this->Login_model->get_user_countries($userDetails['sso_id']);
            }
			if($main_role_id==1 || $main_role_id==4)
			{
				if(($child_role_id!=$main_role_id) && count($assign_countries_list)==0)
				{
					$_SESSION['sl_modal'] = 1;
				}
			}
			$_SESSION['header_country_id'] = $country_id;
			$_SESSION['header_country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
		}
		header('Location: '.SITE_URL.'home');exit;
	}
}
?>