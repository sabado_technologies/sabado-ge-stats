<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
created_by : Srilekha
Date : 19-09-2017
*/
class Faq extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
	}

	public function faq()
    {
        $data['nestedView']['heading']="FAQ";
        $data['nestedView']['cur_page'] = 'faq';
        $data['nestedView']['parent_page'] = 'faq';

        # include files
        $data['nestedView']['css_includes'] = array();

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = "FAQ";
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FAQ','class'=>'active','url'=>'');

        #additional data
        $this->load->view('faq/faq_list',$data);
	}

    public function raise_faq()
    {
        if(validate_number($this->input->post('submitUser'))!='')
        {
            $faq = validate_string($this->input->post('faq',TRUE));
            $sso_id = $this->session->userdata('sso_id');
            if($faq!='' && $sso_id!='')
            {
                send_faq_mail($sso_id,$faq);
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Success!</strong> Question has been Raised successfully to Admins!</div>');
            }
        }
        redirect(SITE_URL.'faq');
    }
} 
?>