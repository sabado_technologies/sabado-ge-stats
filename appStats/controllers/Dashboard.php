<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
class Dashboard extends Base_controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->model('Report_m');
	}

	public function get_ib_tools()
	{
		#page authentication
        $task_access = page_access_check('ib_dashboard');

		$part_name = validate_string($_REQUEST['string']);
        $arr = validate_number($_REQUEST['part_type']);
		$country_id = @$arr[0]['country_id'];
		$modality_id = @$arr[0]['modality'];

		if($task_access == 3 && $_SESSION['header_country_id']!='')
		{
			$country = $_SESSION['header_country_id'];
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$country = $_SESSION['s_country_id'];
		}
		else
		{
			$country = $country_id;
		}
		$search_list = array('part_name'=>$part_name,'country_id'=>$country_id,'modality_id'=>$modality_id);
		$data = get_ib_tool_list($search_list);
        echo json_encode($data);
	}

	public function get_ajax_tools()
	{
		#page authentication
        $task_access = page_access_check('assets_calibration_report');

		$part_name = validate_string($_REQUEST['string']);
        $country_id = validate_number($_REQUEST['part_type']);
		if($task_access == 3 && $_SESSION['header_country_id']!='')
		{
			$country = $_SESSION['header_country_id'];
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$country = $_SESSION['s_country_id'];
		}
		else
		{
			$country = $country_id;
		}
		$data = get_tools_result($part_name,$country);
        echo json_encode($data);
	}
	
	public function calibrationDueReport()
	{
		# Data Array to carry the require fields to View and Model
		$data['nestedView']['heading'] = "Calibration Queue";
		$data['nestedView']['cur_page'] = 'calibrationDueReport';
		$data['nestedView']['parent_page'] = 'calibrationDueReport';
		
		#page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# Load JS and CSS Files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/exporting.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/funnel.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts-more.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

		$data['nestedView']['css_includes'] = array();
		$data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';
		
		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Calibration Queue';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Queue','class'=>'active','url'=>'');
		$data['modality'] = $this->Common_model->get_data('modality',array('status'=>1));
		$data['tool_type_list'] = $this->Common_model->get_data('tool_type',array('status'=>1));
		$data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['warehouses'] = $this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));

        $searchParams = array(
			'zone'         => 1,
			'modality_id'  => '',
			'wh_id'        => '',
			'tool_type_id' => '',
			'task_access'  => $task_access,
			'category'     => '',
			'series_name'  => ''
		);
        if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['header_country_id']));
        	$searchParams['country_id'] = $_SESSION['header_country_id'];
		    $data['chart1Data'] = getCalibrationDueData($searchParams);
		}
		elseif($task_access == 1 || $task_access == 2)
		{
			$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['s_country_id']));
			$searchParams['country_id'] = $_SESSION['s_country_id'];
		    $data['chart1Data'] = getCalibrationDueData($searchParams);
		}
		else
		{
			$data['zones'] = '';
			$searchParams['country_id'] = '';
			$data['chart1Data'] = getCalibrationDueDataForCountries($searchParams);
		}
		$data['searchParams'] = $searchParams;
		$this->load->view('dashboard/calibrationDueReport',$data);
	}

	public function getCalDueChart()
	{   
		#page authentication
        $task_access = page_access_check('calibrationDueReport');

		$searchParams = array(
			'zone'         => validate_number($this->input->post('zone',TRUE)),
			'wh_id'        => validate_number($this->input->post('warehouse',TRUE)),
			'modality_id'  => validate_number($this->input->post('modality',TRUE)),
			'tool_type_id' => validate_string($this->input->post('tool_type',TRUE)),
			'task_access'  => $task_access,
			'series_name'  => validate_string($this->input->post('series_name')),
			'category'     => validate_string($this->input->post('category'))
		);
		if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		    $chartData = getCalibrationDueData($searchParams);
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$searchParams['country_id']=$_SESSION['s_country_id'];
		    $chartData = getCalibrationDueData($searchParams);
		}
		elseif($task_access == 3 && $searchParams['series_name']!='' && $searchParams['category']!='') {
			$searchParams['country_id']= validate_number($this->input->post('country_id'));
			$searchParams['zone']=1;
			$chartData = getCalibrationDueData($searchParams);
		}
		else
		{
			$data['zones']='';
			$data['zone']=1;
			$searchParams['country_id']= validate_number($this->input->post('country_id'));
			$chartData = getCalibrationDueDataForCountries($searchParams);
		}
		echo $chartData;
	}

	public function getCalChart2()
	{  
		#page authentication
        $task_access = page_access_check('calibrationDueReport');
		$zone = $this->input->post('zone',TRUE);
		$warehouse = $this->input->post('warehouse',TRUE);
		$modality = $this->input->post('modality',TRUE);
		$tool_type = $this->input->post('tool_type',TRUE);
		$x_category = $this->input->post('x_category',TRUE);
		$first_x_cat = $this->input->post('first_x_cat');
		$country_id  = $this->input->post('country_id');
		if($task_access == 3 && $first_x_cat!='')
		{
			$zone =1;
		}
		$chartData = getCalStatusData($zone,$warehouse,$modality,$tool_type,$x_category,$first_x_cat,$task_access,$country_id);
		echo $chartData;
	}

	#prasAD 30-05-2018
	public function getWarehousesByZone()
	{
		$zone = $this->input->post('zone',TRUE);
		if($zone!=1)
		{
			$wh_list = get_wh_by_zone($zone);
		}
		else
		{
			if($_SESSION['header_country_id']!='')
			{
				$country_id = $_SESSION['header_country_id'];
			}
			else
			{
				$country_id = $_SESSION['s_country_id'];
			}
		}
		$this->db->select('wh_id,wh_code,name');
		$this->db->from('warehouse');
		if($zone!=1)
		{
			$this->db->where_in('wh_id',$wh_list);
		}
		else
		{
			$this->db->where('country_id',$country_id);
		}
		$this->db->where('status',1);
		$res = $this->db->get();
		$result = $res->result_array();
		$str = '';
		if($res->num_rows()>0)
		{
			$str .= '<option value="">Select Warehouse</option>';
			foreach ($result as $row) 
			{
				$str .= '<option value="'.$row['wh_id'].'">'.$row['wh_code'].' -('.$row['name'].')</option>';
			}
		}
		else
		{
			$str .= '<option value="">Select Warehouse</option>';
		}
		//echo $zone;
		echo $str;
	}

	#prasad 30-05-2018
	public function getWarehousesToolsByCountry()
	{   
		$country_id=(int)$this->input->post('country_id');
		$this->db->select('wh_id,wh_code,name');
		$this->db->from('warehouse');
		if($country_id!='')
		{
			$this->db->where('country_id',$country_id);
	    }
	    else
	    {

			$this->db->where_in('country_id',$_SESSION['countriesIndexedArray']);	
	    }
		$this->db->where('status',1);
		$res = $this->db->get();
		$result = $res->result_array();
		$str = '';
		if($res->num_rows()>0)
		{
			$str .= '<option value="">Select Warehouse</option>';
			foreach ($result as $row) 
			{
				$str .= '<option value="'.$row['wh_id'].'">'.$row['wh_code'].' -('.$row['name'].')</option>';
			}
		}
		else
		{
			$str .= '<option value="">Select Warehouse</option>';
		}
		$searchParams = array(
			'country_id'  => $this->input->post('country_id'),
			'modality_id' => $this->input->post('modality_id')
		);
		$tools_str = '';
		$tool_results = get_tool_asset_list_ib_records($searchParams);
		if(count($tool_results)>0)
		{
			$tools_str .= '<option value="">Select Tools</option>';
			foreach ($tool_results as $row1) 
			{
				$tools_str .= '<option value="'.$row1['tool_id'].'">'.$row1['part_number'].' -('.$row1['part_description'].')</option>';
			}
		}
		else
		{
			$tools_str .= '<option value="">Select Tools</option>';
		}

		echo json_encode(array($str,$tools_str));
	}

	public function getWarehousesByCountry()
	{   
		$country_id=(int)$this->input->post('country_id');
		$this->db->select('wh_id,wh_code,name');
		$this->db->from('warehouse');
		if($country_id!='')
		{
			$this->db->where('country_id',$country_id);
	    }
	    else
	    {

			$this->db->where_in('country_id',$_SESSION['countriesIndexedArray']);	
	    }
		$this->db->where('status',1);
		$res = $this->db->get();
		$result = $res->result_array();
		$str = '';
		if($res->num_rows()>0)
		{
			$str .= '<option value="">Select Warehouse</option>';
			foreach ($result as $row) 
			{
				$str .= '<option value="'.$row['wh_id'].'">'.$row['wh_code'].' -('.$row['name'].')</option>';
			}
		}
		else
		{
			$str .= '<option value="">Select Warehouse</option>';
		}
		echo $str;
	}
    	
    #prasad 30-05-2018
	public function ib_dashboard()
	{
		$data['nestedView']['heading']="Tools Vs IB Dashboard";
		$data['nestedView']['cur_page'] = "ib_dashboard";
		$data['nestedView']['parent_page'] = 'ib_dashboard';

		#page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['css_includes'] = array();
		$data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/exporting.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
       
		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Tools Vs IB Dashboard';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tools Vs IB Dashboard','class'=>'active','url'=>'');
		
		$data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
         $data['warehouses'] = $this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
		 $data['modality'] = $this->Common_model->get_data('modality',array('status'=>1));
		 $searchParams = array(
				'zone'               =>    1,
				'modality_id'        =>   '',
				'wh_id'              =>   '',
				'tool_id'       =>   '',
				'task_access'        =>   $task_access
			);
        if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['header_country_id']));
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['s_country_id']));
			$searchParams['country_id']=$_SESSION['s_country_id'];
		}
		else
		{
			$data['zones']='';
			$searchParams['country_id']= '';
		}
		$data['searchParams']=$searchParams;
		$data['tool_list'] = get_tool_asset_list_ib_records($searchParams);
		$data['chartsData'] = getIBdataIbReport($searchParams);
		$this->load->view('dashboard/ib_dashboard',$data);
	}

	#prasad 30-05-2018
	public function get_tool_ib_data()
	{   
		#page authentication
        $task_access = page_access_check('ib_dashboard');
		$searchParams = array(
					'zone'         => $this->input->post('zone',TRUE),
					'wh_id'        => $this->input->post('warehouse',TRUE),
					'modality_id'  => $this->input->post('modality_id',TRUE),
					'tool_id'      => $this->input->post('tool_id',TRUE),
					'task_access'  => $task_access
		   );
		if($task_access==1 || $task_access==2)
		{
			$searchParams['country_id'] = $_SESSION['s_country_id'];
		}
		elseif($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		}
		else
		{
			$searchParams['zone']=1;
			$searchParams['country_id']= validate_number($this->input->post('country_id'));
		}
		$chartData = getIBdataIbReport($searchParams);
		echo $chartData;
	}

	#prasad 30-05-2018
	public function get_tool_modality_ib_data()
	{	#page authentication
        $task_access = page_access_check('ib_dashboard');
        $searchParams = array(
					'zone'         => $this->input->post('zone',TRUE),
					'wh_id'        => $this->input->post('warehouse',TRUE),
					'modality_id'  => $this->input->post('modality',TRUE),
					'tool_id'      => $this->input->post('tool_id',TRUE),
					'task_access'  => $task_access
		   );
		if($task_access==1 || $task_access==2)
		{
			$searchParams['country_id'] = $_SESSION['s_country_id'];
		}
		elseif($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		}
		else
		{
			$searchParams['zone']=1;
			$searchParams['country_id']= validate_number($this->input->post('country_id'));
		}
		$chartData = gettoolIBdataIBReport($searchParams);
		echo $chartData;
	}

	public function gettool_by_modality()
	{
		$modality_id = $this->input->post('modality',TRUE);
		$status = array(1,2,3,4,8,10,12);
	    $this->db->select('t.tool_id,t.part_number,t.part_description');
	    $this->db->from('tool t');
	    $this->db->join('part p','p.tool_id = t.tool_id');
	    $this->db->join('asset a','a.asset_id = p.asset_id');
	    $this->db->where('p.part_level_id',1);
	    $this->db->where_in('a.status',$status);
	    if($modality_id != '')
		$this->db->where('t.modality_id',$modality_id);
	    $this->db->group_by('t.tool_id');
		$res = $this->db->get();
		$result = $res->result_array();
		$str = '';
		if($res->num_rows()>0)
		{
			$str .= '<option value="">Select Tool</option>';
			foreach ($result as $row) 
			{
				$str .= '<option value="'.$row['tool_id'].'">'.$row['part_number'].' - '.$row['part_description'].'</option>';
			}
		}
		else
		{
			$str .= '<option value="">Select Tool</option>';
		}
		echo $str;
	}

	public function toolsReport()
	{
		# Data Array to carry the require fields to View and Model
		$data['nestedView']['heading'] = "Tools Report";
		$data['nestedView']['cur_page'] = 'toolsReport';
		$data['nestedView']['parent_page'] = 'toolsReport';
		
		#page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# Load JS and CSS Files
		$data['nestedView']['js_includes'] = array();

		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/exporting.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/funnel.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts-more.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

		$data['nestedView']['css_includes'] = array();
		$data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';
		
		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Tools Report';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tools Report','class'=>'active','url'=>'');

		$data['modality'] = $this->Common_model->get_data('modality',array('status'=>1));
		$data['tool_type_list'] = $this->Common_model->get_data('tool_type',array('status'=>1));

		$data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['warehouses'] = $this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));

        $searchParams = array(
				'zone'               =>    1,
				'modality_id'        =>   '',
				'wh_id'              =>   '',
				'tool_type_id'       =>   '',
				'task_access'        =>   $task_access,
				'category'           =>   '',
				'series_name'        =>   ''
		);
		if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['header_country_id']));
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		    $data['firstPieData'] = getToolStatusFirstPieData($searchParams);
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['s_country_id']));
			$searchParams['country_id']=$_SESSION['s_country_id'];
		   $data['firstPieData'] = getToolStatusFirstPieData($searchParams);
		}
		else
		{
			$data['zones']='';
			$searchParams['country_id']= '';
			$data['firstPieData'] = getToolStatusFirstDataCountries($searchParams);
		}
		$data['searchParams']=$searchParams;
		//print_r($data['warehouses']); exit;
		
		//print_r($data['firstPieData']); exit;

		$this->load->view('dashboard/toolsReport', $data);
	}
	public function getToolStatusChart()
	{
		#page authentication
        $task_access = page_access_check('toolsReport');


		$searchParams = array(
					'zone'           => validate_number($this->input->post('zone',TRUE)),
					'wh_id'          => validate_number($this->input->post('warehouse',TRUE)),
					'modality_id'    => validate_number($this->input->post('modality',TRUE)),
					'tool_type_id'   => validate_string($this->input->post('tool_type',TRUE)),
					'task_access'    => $task_access,
					'series_name'    => validate_string($this->input->post('series_name')),
					'category'    =>     validate_string($this->input->post('category'))
	                    );
		if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		    $chartData = getToolStatusFirstPieData($searchParams);
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$searchParams['country_id']=$_SESSION['s_country_id'];
		    $chartData = getToolStatusFirstPieData($searchParams);
		}
		elseif($task_access == 3 && $searchParams['series_name']!='' && $searchParams['category']!='') {
			$searchParams['country_id']= validate_number($this->input->post('country_id'));
			$searchParams['zone']=1;
			$chartData = getToolStatusFirstPieData($searchParams);
			//print_r($chartData);exit;
		}
		else
		{
			$data['zones']='';
			$data['zone']=1;
			$searchParams['country_id']= validate_number($this->input->post('country_id'));
			$chartData = getToolStatusFirstDataCountries($searchParams);
		}
		echo $chartData;
	}

	public function getToolStatusChart2Data()
	{
		//echo '<pre>';print_r($_POST);exit;
		#page authentication
        $task_access = page_access_check('toolsReport');
		$zone = $this->input->post('zone',TRUE);
		$warehouse = $this->input->post('warehouse',TRUE);
		$modality = $this->input->post('modality',TRUE);
		$tool_type = $this->input->post('tool_type',TRUE);
		$tool_status_name = $this->input->post('tool_status',TRUE);
		$first_x_cat = $this->input->post('first_x_cat',TRUE);
		$country_id = $this->input->post('country_id',TRUE);
		$tool_status = ($tool_status_name =='Active')?1:2;
		if($task_access == 3 && $first_x_cat!='')
		{
			$zone =1;
		}
		//echo $tool_status;exit;
		$chartData = getToolStatusData2($zone,$warehouse,$modality,$tool_status,$tool_type,$first_x_cat,$country_id,$task_access,$tool_status_name);
		echo $chartData;
	}

	public function getToolStatusChart3Data()
	{
		$task_access = page_access_check('toolsReport');
		$zone = $this->input->post('zone',TRUE);
		$warehouse = $this->input->post('warehouse',TRUE);
		$modality = $this->input->post('modality',TRUE);
		$tool_status = $this->input->post('tool_status',TRUE);
		$zone_or_wh = $this->input->post('zone_or_wh',TRUE);
		$tool_type = @$this->input->post('tool_type',TRUE);
		$first_x_cat = $this->input->post('first_x_cat',TRUE);
		$country_id = $this->input->post('country_id',TRUE);
		if($task_access == 3 && $first_x_cat!='')
		{
			$zone =1;
		}
		$chartData = getToolStatusData3($zone,$warehouse,$modality,$tool_status,$zone_or_wh,$tool_type,$first_x_cat,$country_id,$task_access);
		echo $chartData;
	}

	public function feOwnedToolsReport()
	{
		# Data Array to carry the require fields to View and Model
		$data['nestedView']['heading'] = "FE Owned Tools Report";
		$data['nestedView']['cur_page'] = 'feOwnedToolsReport';
		$data['nestedView']['parent_page'] = 'feOwnedToolsReport';
		
		#page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		
		# Load JS and CSS Files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/exporting.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/funnel.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts-more.js"></script>';
				$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

		$data['nestedView']['css_includes'] = array();
		$data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';
		
		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'FE Owned Tools Report';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Owned Tools Report','class'=>'active','url'=>'');

		$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1));
		// Get Field engineers, Zonal, National users
		$fe_user_roles = array(2,5,6);
		$searchParams = array(
			        'user_id'     =>        '',
			        'zone'           =>         1,
			        'task_access'    =>         $task_access
		                     );
		if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['header_country_id']));
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['s_country_id']));
			$searchParams['country_id']=$_SESSION['s_country_id'];
		}
		else
		{
			$data['zones']='';
			$searchParams['country_id']= '';
		}
		$data['searchParams']=$searchParams;
		$data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
		$data['fe_users'] = get_fe_users($task_access,'','',$searchParams['country_id']);
		$data['firstPieData'] = getFeToolsFirstPieData($searchParams);
		//print_r($data['firstPieData']); exit;

		$this->load->view('dashboard/feOwnedToolsReport', $data);
	}

	public function getFeUsersByZone()
	{   
		$ci = & get_instance();
		$zone = $this->input->post('zone',TRUE);
		//echo $zone;exit;
		$where = array(); $str = '<option value="">Select SSO</option>';
		if($zone!=1)
		{
			 // Write Query to get FE users by zone here. I am writing a dummy result
			$whs = getWarehouseByLocationID($zone);
            if(@$whs)
            {   
                $wh_arr = array();
                foreach ($whs as $key => $value)
                {
                    $wh_arr[] = $value['wh_id'];
                }
                $fe_ar= getTaskRoles('raise_order');
                $this->db->select('*');
                $this->db->from('user u');
                $this->db->where_in('u.wh_id',$wh_arr);
                $this->db->where_in('u.role_id',$fe_ar);
                $res = $this->db->get();
                if($res->num_rows()>0)
                    $results = $res->result_array();
                else
                    $results = FALSE;
                
            }
            else
            {
                $results = FALSE;
            }
            if($results)
            {
				foreach ($results as $row) {
					$str .= '<option value="'.$row['sso_id'].'">'.$row['sso_id'].' ('.$row['name'].')</option>';
				}
			}
			else
	        {
	            $str.="<option value=''>-No Data Found-</option>";
	        }
		}
		else
		{
			if($_SESSION['header_country_id']!='')
			{
				$country_id = $_SESSION['header_country_id'];
			}
			else
			{
				$country_id = $_SESSION['s_country_id'];
			}
			// Get Field engineers, Zonal, National users
			$fe_user_roles = getTaskRoles('raise_order');
			$ci->db->from('user');
			$ci->db->where_in('role_id',$fe_user_roles);
			$ci->db->where('country_id',$country_id);
			$res = $ci->db->get();
			$data= $res->result_array();
			foreach ($data as $row) {
				$str .= '<option value="'.$row['sso_id'].'">'.$row['sso_id'].' ('.$row['name'].')</option>';
			}
		}
		echo $str;
	}

	public function getFeToolsChart()
	{   
	    #page authentication
        $task_access = page_access_check('feOwnedToolsReport');
        $searchParams = array(
		'zone'        => $this->input->post('zone',TRUE),
		'user_id'     => $this->input->post('user_id',TRUE),
		'task_access' => $task_access,
					);
        if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$searchParams['country_id']=$_SESSION['s_country_id'];
		}
		else
		{
			$searchParams['zone'] = 1;
			$searchParams['country_id'] = $this->input->post('country_id');
		}
		$chartData = getFeToolsFirstPieData($searchParams);
		echo $chartData;
	}

	public function getFeToolsChart2Data()
	{
		#page authentication
        $task_access = page_access_check('feOwnedToolsReport');
        
        $searchParams = array(
		'zone'        => $this->input->post('zone',TRUE),
		'user_id'     => $this->input->post('user_id',TRUE),
		'task_access' => $task_access,
		'modality'    => $this->input->post('modality',TRUE),
					);
        if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$searchParams['country_id']=$_SESSION['s_country_id'];
		}
		else
		{
			$searchParams['zone'] = 1;
			$searchParams['country_id'] = $this->input->post('country_id');
		}
		$chartData = getFeToolsData2($searchParams);
		echo $chartData;
	}

	public function get_fe_users_by_country()
	{   
		$country_id   = (int)$this->input->post('country_id');
		$task_access  = $this->input->post('task_access');
		$fe_users     = get_fe_users($task_access,'','',$country_id);
		$str = '';
		if(count($fe_users)>0)
		{
			$str .= '<option value="">Select FE</option>';
			foreach ($fe_users as $row) 
			{
				$str .= '<option value="'.$row['sso_id'].'">'.$row['name'].'</option>';
			}
		}
		else
		{
			$str .= '<option value="">Select FE</option>';
		}
		echo $str;
	}

	#srilekha 30-05-2018
	public function assets_calibration_report()
	{
		# Data Array to carry the require fields to View and Model
		$data['nestedView']['heading'] = "Assets Calibration Report";
		$data['nestedView']['cur_page'] = 'assets_calibration_report';
		$data['nestedView']['parent_page'] = 'assets_calibration_report';

		#page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		
		# Load JS and CSS Files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/exporting.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/funnel.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts-more.js"></script>';
				$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

		$data['nestedView']['css_includes'] = array();
		$data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';
		//$data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';
		
		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Assets Calibration Report';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Owned Tools Report','class'=>'active','url'=>'');

		
		$data['modality'] = $this->Common_model->get_data('modality',array('status'=>1));
		$modality_id = '';
		$tool_id = '';
		

		//print_r($data['warehouses']); exit;
		$searchParams = array('modality_id'=>'','tool_id'=>'');
		if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$data['tools'] = $this->Common_model->get_data('tool',array('status'=>1));
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		    $data['firstPieData'] = get_in_calibration_assets_FirstPieData($searchParams,$task_access);
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			
			$data['tools'] = $this->Common_model->get_data('tool',array('status'=>1,'country_id'=>$_SESSION['s_country_id']));
			$searchParams['country_id']=$_SESSION['s_country_id'];
		    $data['firstPieData'] = get_in_calibration_assets_FirstPieData($searchParams,$task_access);
		}
		else
		{
			$data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
			$data['tools'] = $this->Common_model->get_data('tool',array('status'=>1));
			$searchParams['country_id']= '';
			$data['firstPieData'] = get_in_calibration_assets_FirstPieData_countries($searchParams,$task_access);
		}
		$data['task_access'] = $task_access;

		$this->load->view('dashboard/assets_calibration_report', $data);
	}

	#srilekha 30-05-2018
	public function get_calibration_country_based_tools()
	{
		$country_id = validate_number($this->input->post('country_id',TRUE));
		if($country_id!='')
		{
			$tools = $this->Common_model->get_data('tool',array('status'=>1,'country_id'=>$country_id));
		}
        else
        {
        	$tools = $this->Common_model->get_data('tool',array('status'=>1));
        }
        //echo $country_id; echo "<pre>";
        //print_r($tools); exit;
        $tool_str = '';
        $tool_str.= "<select name='tool_id'  class='tool select3'>";
        $tool_str.="<option value=''>- Select Tool Number/Description -</option>";
        foreach ($tools as $key => $value) 
        {
            $tool_str.='<option value="'.$value['tool_id'].'">'.$value['part_number'].' / '.$value['part_description'].'</option>';  
        }
        $tool_str.="</select>";
        echo $tool_str;
	}

	#srilekha 30-05-2018
	public function get_in_calibration_assets_Chart()
	{
		#page authentication
        $task_access = page_access_check('assets_calibration_report');


		$modality = $this->input->post('modality',TRUE);
		$tool = $this->input->post('tool',TRUE);
		$country = $this->input->post('country',TRUE);
		$searchParams=array('modality_id'=>$modality,'tool_id'=>$tool,'country_id'=>$country);
		if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$chartData = get_in_calibration_assets_FirstPieData($searchParams,$task_access);
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			
			$chartData = get_in_calibration_assets_FirstPieData($searchParams,$task_access);
		}
		else
		{
			$chartData = get_in_calibration_assets_FirstPieData_countries($searchParams,$task_access);
		}
		
		echo $chartData;
	}

	#srilekha 30-05-2018
	public function get_in_calibration_Chart2Data()
	{
		#page authentication
        $task_access = page_access_check('assets_calibration_report');


		$modality_id = $this->input->post('modality',TRUE);
		$tool_id = $this->input->post('tool',TRUE);
		$name=$this->input->post('name',TRUE);
		$country_name = $this->input->post('country_name',TRUE);
		$country_id = $this->Common_model->get_value('location',array('name'=>$country_name),'location_id');
		$searchParams=array('modality_id'=>$modality_id,'tool_id'=>$tool_id,'name'=>$name,'country_id'=>$country_id);
		$chartData = get_in_calibration_Chart2Data($searchParams);
		echo $chartData;
	}

	#srilekha 30-05-2018
	public function download_field_deployed_excel()
	{
		#page authentication
        $task_access = page_access_check('assets_calibration_report');

		$modality_id = $this->input->post('modality',TRUE);
		$tool_id = $this->input->post('tool',TRUE);
		$name=$this->input->post('name',TRUE);
		$country_name = $this->input->post('country',TRUE);
		$country_id = $this->Common_model->get_value('location',array('name'=>$country_name),'location_id');
		$searchParams=array('modality_id'=>$modality_id,'tool_id'=>$tool_id,'name'=>$name,'country_id'=>$country_id);
		if($name=='Field Deployed')
		{
			$result=$this->Report_m->download_field_deployed_excel($searchParams);
			$data ='';
            $titles = array('S.No','SSO ID','Tool Number','Tool Description','Asset Number','Tool Type','Calibration Due Date','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            $i=1;
            if(count($result)>0)
            {
            	foreach($result as $row)
            	{
            		$cal_due_date = $row['cal_due_date'];
                    if($cal_due_date!='') { $cal_due_date = indian_format($cal_due_date);}
                    else { $cal_due_date = '';}
            		$data.='<tr>';
            		$data.='<td align="center">'.$i++.'</td>';
            		$data.='<td align="center">'.$row['user_name'].'- ( '.$row['sso_id'].' )'.'</td>';
            		$data.='<td align="center">'.$row['part_number'].'</td>';
            		$data.='<td align="center">'.$row['part_description'].'</td>';
            		$data.='<td align="center">'.$row['asset_number'].'</td>';
            		$data.='<td align="center">'.$row['tool_type'].'</td>';
            		$data.='<td align="center">'.$cal_due_date.'</td>'; 
            		$data.='<td align="center">'.$row['country'].'</td>';
            		$data.='</tr>';
            	}
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Results Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Field_Deployed_report'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
		}
	}

	public function egm_calibrationDueReport()
	{
		# Data Array to carry the require fields to View and Model
		$data['nestedView']['heading'] = "EGM Calibration Queue";
		$data['nestedView']['cur_page'] = 'egm_calibrationDueReport';
		$data['nestedView']['parent_page'] = 'egm_calibrationDueReport';
		
		#page authentication
        $task_access = page_access_check('calibrationDueReport');

		# Load JS and CSS Files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/exporting.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/funnel.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/highcharts-more.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

		$data['nestedView']['css_includes'] = array();
		$data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';
		
		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'EGM Calibration Queue';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'EGM Calibration Queue','class'=>'active','url'=>'');

		$data['modality'] = $this->Common_model->get_data('modality',array('status'=>1));
		$data['tool_type_list'] = $this->Common_model->get_data('tool_type',array('status'=>1));
		$data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['warehouses'] = $this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));

        $searchParams = array(
			'zone'         => 1,
			'modality_id'  => '',
			'wh_id'        => '',
			'tool_type_id' => '',
			'task_access'  => $task_access,
			'category'     => '',
			'series_name'  => ''
		);
        if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['header_country_id']));
        	$searchParams['country_id'] = $_SESSION['header_country_id'];
		    $data['chart1Data'] = egm_getCalibrationDueData($searchParams);
		}
		elseif($task_access == 1 || $task_access == 2)
		{
			$data['zones'] = $this->Common_model->get_data('location',array('level_id'=>3,'status'=>1,'parent_id'=>$_SESSION['s_country_id']));
			$searchParams['country_id'] = $_SESSION['s_country_id'];
		    $data['chart1Data'] = egm_getCalibrationDueData($searchParams);
		}
		else
		{
			$data['zones'] = '';
			$searchParams['country_id'] = '';
			$data['chart1Data'] = egm_getCalibrationDueDataForCountries($searchParams);
		}
		$data['searchParams'] = $searchParams;
		$this->load->view('dashboard/egm_calibrationDueReport',$data);
	}

	public function egm_getCalDueChart()
	{   
		#page authentication
        $task_access = page_access_check('calibrationDueReport');

		$searchParams = array(
			'zone'         => validate_number($this->input->post('zone',TRUE)),
			'wh_id'        => validate_number($this->input->post('warehouse',TRUE)),
			'modality_id'  => validate_number($this->input->post('modality',TRUE)),
			'tool_type_id' => validate_string($this->input->post('tool_type',TRUE)),
			'task_access'  => $task_access,
			'series_name'  => validate_string($this->input->post('series_name')),
			'category'     => validate_string($this->input->post('category'))
		);
		if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
        	$searchParams['country_id']=$_SESSION['header_country_id'];
		    $chartData = egm_getCalibrationDueData($searchParams);
		}
		elseif($task_access == 1 || $task_access ==2)
		{
			$searchParams['country_id']=$_SESSION['s_country_id'];
		    $chartData = egm_getCalibrationDueData($searchParams);
		}
		elseif($task_access == 3 && $searchParams['series_name']!='' && $searchParams['category']!='') {
			$searchParams['country_id']= validate_number($this->input->post('country_id'));
			$searchParams['zone']=1;
			$chartData = egm_getCalibrationDueData($searchParams);
		}
		else
		{
			$data['zones']='';
			$data['zone']=1;
			$searchParams['country_id']= validate_number($this->input->post('country_id'));
			$chartData = egm_getCalibrationDueDataForCountries($searchParams);
		}
		echo $chartData;
	}

	public function egm_getCalChart2()
	{  
		#page authentication
        $task_access = page_access_check('calibrationDueReport');
		$zone = $this->input->post('zone',TRUE);
		$warehouse = $this->input->post('warehouse',TRUE);
		$modality = $this->input->post('modality',TRUE);
		$tool_type = $this->input->post('tool_type',TRUE);
		$x_category = $this->input->post('x_category',TRUE);
		$first_x_cat = $this->input->post('first_x_cat');
		$country_id  = $this->input->post('country_id');
		if($task_access == 3 && $first_x_cat!='')
		{
			$zone =1;
		}
		$chartData = egm_getCalStatusData($zone,$warehouse,$modality,$tool_type,$x_category,$first_x_cat,$task_access,$country_id);
		echo $chartData;
	}
}