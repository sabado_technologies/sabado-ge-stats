<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Tools_inventory extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Tools_inventory_m');
	}

	public function tools_inventory()
    {
        $data['nestedView']['heading']="Tools Inventory";
		$data['nestedView']['cur_page'] = 'tools_inventory';
		$data['nestedView']['parent_page'] = 'tools_inventory';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/tools_inventory.js"></script>';
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Tools Inventory';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tools Inventory','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'warehouse'    => validate_number($this->input->post('warehouse',TRUE)),
                'tool_no'      => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'    => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'modality_id'  => validate_number($this->input->post('modality_id',TRUE)),
                'asset_status' => validate_number($this->input->post('asset_status',TRUE)),
                'asset_type'   => validate_number($this->input->post('asset_type',TRUE)),
                'zone_id'	   => validate_number($this->input->post('zone_id',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'user_id'      => validate_number(@$this->input->post('user_id',TRUE)),
                'created_date' => validate_string($this->input->post('created_date',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'warehouse'    => $this->session->userdata('warehouse'),
                    'tool_no'      => $this->session->userdata('tool_no'),
                    'tool_desc'    => $this->session->userdata('tool_desc'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'modality_id'  => $this->session->userdata('modality_id'),
                    'asset_status' => $this->session->userdata('asset_status'),
                    'asset_type'   => $this->session->userdata('asset_type'),
                    'zone_id'      => $this->session->userdata('zone_id'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'user_id'      => $this->session->userdata('user_id'),
                    'created_date' => $this->session->userdata('created_date'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'tool_no'      => '',
                    'tool_desc'    => '',
                    'asset_number' => '',
                    'modality_id'  => '',
                    'asset_status' => '',
                    'asset_type'   => '',
                    'zone_id'	   => '',
                    'country_id'   => '',
                    'warehouse'    => '',
                    'user_id'      => '',
                    'created_date' => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;
		if($searchParams['asset_type'] != '' || $searchParams['asset_status']!='')
		{
			$data['astatusList'] = $this->Common_model->get_data('asset_status',array('type'=>$searchParams['asset_type']));
		}
		if($searchParams['zone_id'] != '')
		{
			$data['zwhList'] = get_wh_by_zone_tools_inventory($searchParams['zone_id'],$task_access);
		}
		# Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'tools_inventory/';
        # Total Records
        $config['total_rows'] = $this->Tools_inventory_m->t_inventory_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $t_inventory_results = $this->Tools_inventory_m->t_inventory_results($current_offset, $config['per_page'], $searchParams,$task_access);

        foreach ($t_inventory_results as $key => $value) 
        {
            $t_inventory_results[$key]['asset_list'] = $this->Tools_inventory_m->get_asset_items($value['asset_id']);
        }
        # Additional data
        $data['t_inventory_results'] = $t_inventory_results;
        $data['modalityList'] = $this->Common_model->get_data('modality',array('status'=>1));
        $data['whList'] = $this->Tools_inventory_m->get_wh_list($task_access);
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['zoneList'] = $this->Tools_inventory_m->get_zone_by_country($task_access);
        $data['asset_status'] = $this->Common_model->get_data('asset_status',array());
        $data['task_access'] = $task_access;
        
        $this->load->view('tool/tools_inventory_view',$data);
	}

	public function get_asset_status_by_type()
	{
		$asset_type = validate_number($this->input->post('asset_type',TRUE));
		if($asset_type !='')
		{
			$asset_status = $this->Common_model->get_data('asset_status',array('type'=>$asset_type));
		}
		else
		{
			$asset_status = $this->Common_model->get_data('asset_status',array());
		}
		

		$asset_str = '';
		$asset_str.="<option value=''>- Asset Status -</option>";
		foreach ($asset_status as $key => $value) 
		{
			$asset_str.='<option value="'.$value['asset_status_id'].'">'.$value['name'].'</option>';  
		}
        $data = array('result1'=>$asset_str);
		echo json_encode($data);
	}

	public function get_wh_by_zone()
	{
        #page Authentication
        $task_access = page_access_check('tools_inventory');

		$zone_id = validate_number($this->input->post('zone_id',TRUE));
		if($zone_id != '')
		{
            $wh_arr = get_wh_by_zone_tools_inventory($zone_id,$task_access);
		}
		else
		{
            $wh_arr = $this->Tools_inventory_m->get_wh_list($task_access);
		}
	
		$wh_str = '';
        if(count($wh_arr)>0)
        {
            $wh_str.="<option value=''>- Inventory -</option>";
            foreach ($wh_arr as $key => $value) 
            {
                $wh_str.='<option value="'.$value['wh_id'].'">'.$value['wh_name'].'</option>';  
            }
        }
        else
        {
            $wh_str.="<option value=''>- No Records Found -</option>";
        }
        $data = array('result1'=>$wh_str);
		echo json_encode($data);
	}

    // created by maruthi on 23rd sep 
    public function toolUtilization()
    {
        $data['nestedView']['heading']="Tool Utilization";
        $data['nestedView']['cur_page'] = 'toolUtilization';
        $data['nestedView']['parent_page'] = 'toolUtilization';

        #Page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/tool_utilization.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Tool Utilization';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Utilization','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'tool_number'      => validate_string($this->input->post('tool_number', TRUE)),
                'tool_description' => validate_string($this->input->post('tool_description', TRUE)),
                'asset_number'     => validate_string($this->input->post('asset_number',TRUE)),
                'tool_type_id'     => validate_number($this->input->post('tool_type_id',TRUE)),
                'fe_sso_id'        => validate_string($this->input->post('fe_sso_id',TRUE)),
                'from_date'        => validate_string($this->input->post('from_date',TRUE)),
                'to_date'          => validate_string($this->input->post('to_date',TRUE)),
                'modality_id'      => validate_number($this->input->post('modality_id',TRUE)),
                'country_id'       => validate_number($this->input->post('country_id',TRUE)),
                'serial_no'        => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'tool_number'      => $this->session->userdata('tool_number'),
                    'tool_description' => $this->session->userdata('tool_description'),
                    'asset_number'     => $this->session->userdata('asset_number'),
                    'tool_type_id'     => $this->session->userdata('tool_type_id'),
                    'fe_sso_id'        => $this->session->userdata('fe_sso_id'),
                    'from_date'        => $this->session->userdata('from_date'),
                    'to_date'          => $this->session->userdata('to_date'),
                    'modality_id'      => $this->session->userdata('modality_id'),
                    'country_id'       => $this->session->userdata('country_id'),
                    'serial_no'        => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'tool_number'      => '',
                    'tool_description' => '',
                    'asset_number'     => '',
                    'modality_id'      => '',
                    'tool_type_id'     => '',
                    'fe_sso_id'        => '',
                    'from_date'        => '',
                    'to_date'          => '',
                    'country_id'       => '',
                    'serial_no'        => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;
        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'toolUtilization/';
        # Total Records
        $config['total_rows'] = $this->Tools_inventory_m->tool_utilization_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $t_inventory_results = $this->Tools_inventory_m->tool_utilization_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['modalityList'] = $this->Common_model->get_data('modality',array('status'=>1));
        $data['toolType'] = $this->Common_model->get_data('tool_type',array('status'=>1));
        $data['t_inventory_results'] = $t_inventory_results;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        
        $this->load->view('tool/tools_utilization_view',$data);
    }

    public function download_tools_utilization()
    {
        $task_access = page_access_check('toolUtilization');
        $asset_type = validate_number($this->input->post('asset_type',TRUE));
        if(validate_string($this->input->post('download_tools_utilization',TRUE))==1)
        {
            $searchParams=array(
                'tool_number'      => validate_string($this->input->post('tool_number', TRUE)),
                'tool_description' => validate_string($this->input->post('tool_description', TRUE)),
                'asset_number'     => validate_string($this->input->post('asset_number',TRUE)),
                'tool_type_id'     => validate_number($this->input->post('tool_type_id',TRUE)),
                'fe_sso_id'        => validate_string($this->input->post('fe_sso_id',TRUE)),
                'from_date'        => validate_string($this->input->post('from_date',TRUE)),
                'to_date'          => validate_string($this->input->post('to_date',TRUE)),
                'modality_id'      => validate_number($this->input->post('modality_id',TRUE)),
                'country_id'       => validate_number($this->input->post('country_id',TRUE)),
                'serial_no'        => validate_string($this->input->post('serial_no',TRUE))
            );

            $t_utilization = $this->Tools_inventory_m->download_t_utilization($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','SSO','Tool Number','Tool Description','Asset Number','Serial Number','Modality','Tool Type','Received Date',
                'Returned Date','Country','No. of Days Used');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_utilization)>0)
            { 
                $sno = 1;              
                foreach($t_utilization as $row)
                {
                    $num_days=((@$row['to_date']!='')?dateDiffInDays($row['from_date'],$row['to_date']):dateDiffInDays($row['from_date'],date('Y-m-d'))).' Days';
                    if($row['to_date']!='')
                        { 
                           $num_days1= full_indian_format($row['to_date']);
                        }
                        else{ $num_days1="--";} 
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['sso'].'</td>';
                    $data.='<td align="center">'.$row['part_number'].'</td>'; 
                    $data.='<td align="center">'.$row['part_description'].'</td>';
                    $data.='<td align="center">'.$row['asset_number'].'</td>';
                    $data.='<td align="center">'.$row['serial_number'].'</td>';
                    $data.='<td align="center">'.$row['modality'].'</td>';
                    $data.='<td align="center">'.$row['tool_type'].'</td>';
                    $data.='<td align="center">'.full_indian_format($row['from_date']).'</td>';
                    $data.='<td align="center">'.$num_days1.'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='<td align="center">'.$num_days.'</td>';
                    $data.='</tr>';

                   
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Tools_Utilization_'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }


    public function download_tools_inventory()
    {
        $task_access = page_access_check('tools_inventory');
        $asset_type = validate_number($this->input->post('asset_type',TRUE));
        if(validate_string($this->input->post('download_tools_inventory',TRUE))==1)
        {
            $searchParams=array(
                'warehouse'    => validate_number($this->input->post('warehouse', TRUE)),
                'tool_no'      => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'    => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'modality_id'  => validate_number($this->input->post('modality_id',TRUE)),
                'asset_status' => validate_number($this->input->post('asset_status',TRUE)),
                'asset_type'   => validate_number($this->input->post('asset_type',TRUE)),
                'zone_id'      => validate_number($this->input->post('zone_id',TRUE)),
                'country_id'   => validate_number($this->input->post('country_id',TRUE)),
                'user_id'      => validate_number($this->input->post('user_id',TRUE)),
                'created_date' => validate_string($this->input->post('created_date',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $t_inventory = $this->Tools_inventory_m->download_t_inventory($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Asset Number','Tool Number','Tool Description','Serial Number','Part Level','Quantity','Status','HSN Code','GST %','Tool Code','Tool Type','Modality','Model','Supplier','Calibration','Calibration Supplier','Length','Breadth','Height','Weight','Asset Type','Kit','Manufacturer','Inventory','Pallet Location','Actual Cost','Currency','Cost in INR','EARPE Number','PO Number','Date Of Use','Calibrated Date','Calibration Due Date','Asset Main Status','Asset Status','Tool Availability','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_inventory)>0)
            { 
                $sno = 1;              
                foreach($t_inventory as $row)
                {
                    if($row['asset_type_id']==1) $asset_type = "Assembly";
                    else $asset_type = "Component";

                    if($row['kit']==1)$kit = "Yes";
                    else $kit = "No";

                    if($row['status']==1) $status = "Good";
                    else if($row['status']==2) $status = "Defective";
                    else $status = "Missing";

                    if($row['availability_status']==1)$asset_main_status = "Active";
                    else $asset_main_status = "Inactive";

                    if($row['date_of_use']=='' || $row['date_of_use']=="1970-01-01" || $row['date_of_use']=='0000-00-00')$date_of_use = "";
                    else $date_of_use = full_indian_format($row['date_of_use']);

                    if($row['calibrated_date']=='' || $row['calibrated_date']=="1970-01-01"  || $row['calibrated_date']=='0000-00-00')$calibrated_date = "";
                    else $calibrated_date = full_indian_format($row['calibrated_date']);

                    if($row['cal_due_date']=='' || $row['cal_due_date']=="1970-01-01"  || $row['cal_due_date']=='0000-00-00')$cal_due_date = "";
                    else $cal_due_date = full_indian_format($row['cal_due_date']);

                    if($row['approval_status']==1)$asset_status = "Waiting For Approval";
                    else $asset_status = $row['asset_status'];

                    $inventory = $row['wh_code'].' -('.$row['wh_name'].')';
                    $tool_availability = get_asset_position($row['asset_id']);
                    $tool_code = get_tool_code($row['tool_code']);
                    //$country_name = $row['country_name'];

                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['asset_number'].'</td>'; 
                    $data.='<td align="center">'.$row['part_number'].'</td>';  
                    $data.='<td align="center">'.$row['part_description'].'</td>'; 
                    $data.='<td align="center">'.$row['serial_number'].'</td>';
                    $data.='<td align="center">L0</td>';
                    $data.='<td align="center">'.$row['quantity'].'</td>';
                    $data.='<td align="center">'.$status.'</td>';
                    $data.='<td align="center">'.$row['hsn_code'].'</td>';
                    $data.='<td align="center">'.$row['gst_percent'].'</td>';
                    $data.='<td align="center">'.'`'.$tool_code.'</td>';
                    $data.='<td align="center">'.$row['tool_type_name'].'</td>';
                    $data.='<td align="center">'.$row['modality_name'].'</td>'; 
                    $data.='<td align="center">'.$row['model'].'</td>'; 
                    $data.='<td align="center">'.$row['supplier_name'].'</td>';
                    $data.='<td align="center">'.$row['cal_type_name'].'</td>';
                    $data.='<td align="center">'.$row['calibration_supplier'].'</td>';
                    $data.='<td align="center">'.$row['length'].'</td>';
                    $data.='<td align="center">'.$row['breadth'].'</td>';
                    $data.='<td align="center">'.$row['height'].'</td>';
                    $data.='<td align="center">'.$row['weight'].'</td>';
                    $data.='<td align="right">'.$asset_type.'</td>';
                    $data.='<td align="right">'.$kit.'</td>';
                    $data.='<td align="center">'.$row['manufacturer'].'</td>';
                    $data.='<td align="right">'.$inventory.'</td>';
                    $data.='<td align="right">'.$row['sub_inventory'].'</td>';
                    $data.='<td align="right">'.$row['cost'].'</td>';
                    $data.='<td align="right">'.$row['currency_name'].'</td>';
                    $data.='<td align="right">'.$row['cost_in_inr'].'</td>';
                    $data.='<td align="right">'.$row['earpe_number'].'</td>';
                    $data.='<td align="right">'.$row['po_number'].'</td>';
                    $data.='<td align="right">'.$date_of_use.'</td>';
                    $data.='<td align="right">'.$calibrated_date.'</td>';
                    $data.='<td align="right">'.$cal_due_date.'</td>';
                    $data.='<td align="right">'.$asset_main_status.'</td>';
                    $data.='<td align="right">'.$asset_status.'</td>';
                    $data.='<td align="right">'.$tool_availability.'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';

                    $sub_assets = $this->Tools_inventory_m->get_sub_asset_detials($row['asset_id']);
                    if(count($sub_assets)>0)
                    {
                        foreach ($sub_assets as $key => $value) 
                        {
                            if($value['status']==1) $status = "Good";
                            else if($value['status']==2) $status = "Defective";
                            else $status = "Missing";

                            $data.='<tr>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>'; 
                            $data.='<td align="center">'.$value['part_number'].'</td>';  
                            $data.='<td align="center">'.$value['part_description'].'</td>'; 
                            $data.='<td align="center">'.$value['serial_number'].'</td>';
                            $data.='<td align="center">L1</td>';
                            $data.='<td align="center">'.$value['quantity'].'</td>';
                            $data.='<td align="center">'.$status.'</td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>'; 
                            $data.='<td align="center"></td>'; 
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="center"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='<td align="right"></td>';
                            $data.='</tr>';
                        }
                    }
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Tools_Inventory_'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }


}