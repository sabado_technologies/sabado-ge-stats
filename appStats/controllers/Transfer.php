<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Transfer extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Transfer_m');
        $this->load->model('Fe2fe_m');
        $this->load->model('Order_m');
        $this->load->model('Fe_delivery_m');
        $this->load->model('Pickup_point_m');
    }

    public function raise_transfer()
    {
        $data['nestedView']['heading']="Raise Stock Transfer";
        $data['nestedView']['cur_page'] = 'raise_transfer';
        $data['nestedView']['parent_page'] = 'raise_transfer';  

        $task_access = page_access_check($data['nestedView']['parent_page']); 
        
        if($task_access == 2 || $task_access == 3 || isset($_SESSION['whsIndededArray']))
        {
            $country_id = $this->session->userdata('s_country_id');
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }

            if(@$this->input->post('warehouse_id',TRUE)!='')
            {
                $warehouse_id = $this->input->post('warehouse_id',TRUE);
            }
            else
            {
                if(@$this->uri->segment(2)!='')
                {
                    $warehouse_id = $this->session->userdata('st_wh_id');
                }
                else
                {
                    $_SESSION['st_wh_id'] = '';
                    $data['nestedView']['heading']="Raise Stock Transfer";
                    $data['nestedView']['cur_page'] = 'raise_transfer';
                    $data['nestedView']['parent_page'] = 'raise_transfer';

                    # include files
                    $data['nestedView']['css_includes'] = array();
                    $data['nestedView']['js_includes'] = array();
                    $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
                    
                    # Breadcrumbs
                    $data['nestedView']['breadCrumbTite'] = 'Raise Stock Transfer';
                    $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Warehouse','class'=>'active','url'=>'');

                    $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                    $warehouse_list = array();
                    $data['country_id'] = 0;
                    if($task_access == 2)
                    {
                        $data['country_id'] = $country_id;
                        $data['warehouse_list'] = $this->Common_model->get_data('warehouse',array('country_id'=>$country_id,'status'=>1));
                    }
                    else if(isset($_SESSION['whsIndededArray']))
                    {
                        $data['country_id'] = $country_id;
                        $data['warehouse_list']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
                    }
                    else if($this->session->userdata('header_country_id')!='')
                    {
                        $data['country_id'] = $country_id;
                        $data['warehouse_list']=$this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));

                        
                    }
                    $data['form_action'] = SITE_URL.'raise_transfer';
                    $data['cancel'] = SITE_URL;
                    $data['flag']=1;
                    $data['task_access']=$task_access;
                    $this->load->view('transfer/warehouse_list',$data); 
                }
            }
        }
        else if($task_access == 1)
        {
            $warehouse_id = $this->session->userdata('s_wh_id');
        }

        if(@$warehouse_id != '')
        {
            $_SESSION['st_wh_id'] = $warehouse_id;
            $wh = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$warehouse_id));
            $data['wh_name'] = $wh['wh_code'].' - ('.$wh['name'].')';
             
            $data['nestedView']['heading']="Raise Stock Transfer";
            $data['nestedView']['cur_page'] = 'raise_transfer';
            $data['nestedView']['parent_page'] = 'raise_transfer';
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/transfer.js"></script>';
             $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

            $data['nestedView']['css_includes'] = array();
            
            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Raise Stock Transfer';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            if($task_access == 2 || $task_access == 3 || isset($_SESSION['whsIndededArray']))
            {
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Warehouse','class'=>'','url'=>SITE_URL.'raise_transfer');
            }
            
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise Stock Transfer','class'=>'active','url'=>'');

            # Search Functionality
           $psearch=$this->input->post('search', TRUE);
            if($psearch!='') 
            {
                $asset_number = validate_string($this->input->post('asset_number', TRUE));
                #check asset number is available in other ST Or not
                if($asset_number!='')
                {
                    $asset_id = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number),'asset_id');
                    if($asset_id!='')
                    {
                        /*koushik 11-10-2018 check asset is involved in ST or not */
                        $st_entry = check_for_st_entry($asset_id);
                        if(count($st_entry)>0)
                        {
                            $stn_number = $st_entry['stn_number'];
                            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-times-circle"></i></div>
                            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Remove asset from ST to proceed !.</div>'); 
                            $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
                            $asset_number = '';
                            redirect(SITE_URL.'raise_transfer/'.$current_offset); exit();
                        }
                    }
                }
                $searchParams=array(
                    'part_number'        => validate_string($this->input->post('part_number', TRUE)),
                    'asset_number'       => $asset_number,
                    'part_description'   => validate_string($this->input->post('part_description', TRUE)),
                    'modality_id'        => validate_number($this->input->post('modality_id', TRUE)),
                    'asset_status_id'    => $this->input->post('asset_status_id', TRUE),
                    'serial_no'          => validate_string($this->input->post('serial_no',TRUE))
                );
                $this->session->set_userdata($searchParams);
            } 
            else 
            {
                if($this->input->post('reset')!='')
                {
                    $searchParams=array(
                        'part_number'      => '',
                        'asset_number'     => '',
                        'part_description' => '',
                        'modality_id'      => '',
                        'asset_status_id'  => '',
                        'serial_no'        => ''
                    );
                    $this->session->set_userdata($searchParams);
                }

                else if($this->uri->segment(2)!='')
                {
                    $searchParams=array(
                        'part_number'      => $this->session->userdata('part_number'),
                        'asset_number'     => $this->session->userdata('asset_number'),
                        'part_description' => $this->session->userdata('part_description'),
                        'modality_id'      => $this->session->userdata('modality_id'),
                        'asset_status_id'  => $this->session->userdata('asset_status_id'),
                        'serial_no'        => $this->session->userdata('serial_no')
                    );
                }
                else 
                {
                    $searchParams=array(
                        'part_number'      => '',
                        'asset_number'     => '',
                        'part_description' => '',
                        'modality_id'      => '',
                        'asset_status_id'  => '',
                        'serial_no'        => ''
                    );
                    $this->session->set_userdata($searchParams);
                }
            }
            $data['searchParams'] = $searchParams;
            
            // Logic For Cart Functionality 
            if(isset($_POST['add']))
            {
                if(!isset($_SESSION['st_asset_id']))
                {   
                    $_SESSION['st_asset_id'] = array();
                }
                $a = count(@$_POST['asset_id']);
                //$_SESSION['st_asset_id'] = array();
                for ($i=0; $i < $a; $i++)
                {   
                    $_SESSION['st_asset_id'][$_POST['asset_id'][$i]] = @$_POST['asset_id'][$i];
                }
                $_SESSION['dummy'] = 1;
            }

            if(!isset($_POST['reset']) && !isset($_POST['search']) && !isset($_POST['add']) && !isset($_POST['raise_transfer']))
            {  
                if(isset($_SESSION['st_first_url_count']))
                { 
                    $_SESSION['st_last_url_count'] = base_url(uri_string());
                    if(strcmp($_SESSION['st_first_url_count'],$_SESSION['st_last_url_count']))
                    {
                    }
                    else
                    {   
                        if(isset($_SESSION['st_asset_id'])) unset($_SESSION['st_asset_id']);
                        $_SESSION['dummy'] = 0;
                    }
                }
                else
                {  
                    $_SESSION['st_first_url_count'] = base_url(uri_string());
                    if(isset($_SESSION['st_asset_id'])) unset($_SESSION['st_asset_id']);
                    $_SESSION['dummy'] = 0;
                }
            }
            
            # Default Records Per Page - always 10
            /* pagination start */
            $config = get_paginationConfig();
            $config['base_url'] = SITE_URL . 'raise_transfer/';
            //print_r($searchParams['asset_status_id']); exit;
            # Total Records
            $config['total_rows'] = $this->Transfer_m->tool_total_num_rows($searchParams,$warehouse_id);
            //echo $this->db->last_query(); exit;
            $config['per_page'] = getDefaultPerPageRecords();
            $data['total_rows'] = $config['total_rows'];
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
            if ($data['pagination_links'] != '') {
                $data['last'] = $this->pagination->cur_page * $config['per_page'];
                if ($data['last'] > $data['total_rows']) {
                    $data['last'] = $data['total_rows'];
                }
                $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
            }
            $data['sn'] = $current_offset + 1;
            /* pagination end */
            # Loading the data array to send to View
            $data['toolResults'] = $this->Transfer_m->tool_results($current_offset, $config['per_page'], $searchParams,$warehouse_id);
            
            $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
            
            # Additional data
            $data['displayResults'] = 1;
            $data['warehouse_id'] = $warehouse_id;
            $data['current_offset'] = $current_offset;
            $data['task_access']=$task_access;
            $this->load->view('transfer/raise_transfer',$data);
        }
    }

    public function issue_tool_transfer()
    {
        $wh_id = $this->input->post('warehouse_id',TRUE);
        if($wh_id == '')
        {
            redirect(SITE_URL.'raise_transfer'); exit();
        }

        $data['nestedView']['heading']="Stock  Transfer Tools";
        $data['nestedView']['cur_page'] = 'raise_transfer_second';
        $data['nestedView']['parent_page'] = 'raise_transfer';

        #page Authentication
        $task_access = page_access_check('raise_transfer');

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/transfer.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Stock Transfer Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        if($task_access == 3)
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Warehouse','class'=>'','url'=>SITE_URL.'raise_transfer');
        }
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise Stock Transfer','class'=>'','url'=>SITE_URL.'raise_transfer');
        
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer Tools','class'=>'active','url'=>'');

        #session data
        if(isset($_SESSION['st_asset_id']))
        {
            
            $result_data = array();
            foreach ($_SESSION['st_asset_id'] as $key => $asset_id)
            {
                $result_data[] = asset_info($asset_id);
            }            
            $data['assets'] = $result_data;
        }

        #additional Data
        $wh = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
        $data['wh_name'] = $wh['wh_code'].' - ('.$wh['name'].')';
        $data['service_type'] = $this->Common_model->get_data('service_type',array('status'=>1));

        $data['document_type'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>3));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>3));     
        $data['wh_data'] = getWhArrNotInID($wh_id);
        $data['flg'] = 1;                
        $data['form_action'] = SITE_URL.'insert_transfer'; 
        $data['task_access']=$task_access;
        $data['warehouse_id'] = $wh_id;
        $this->load->view('transfer/issue_tool_transfer',$data);
    }

    public function removeToolFromIssueCartTransfer()
    {
        $asset_id = validate_number($this->input->post('asset_id',TRUE));
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $ordered_tool_id = validate_number($this->input->post('ordered_tool_id',TRUE));
        $ordered_asset_id = validate_number($this->input->post('ordered_asset_id',TRUE));
        $asset_status_id = validate_number($this->input->post('asset_status_id',TRUE));
        if($tool_order_id!='')
        {  
            $current_stage_id = validate_number($this->input->post('current_stage_id',TRUE));
            
            $this->Common_model->update_data('ordered_asset',array('status'=>3),array('ordered_asset_id'=>$ordered_asset_id));

            #audit data
            $old_data = array('asset_status_id' => $asset_arr['status']);

            $status = ($asset_status_id == 2)?1:$asset_status_id;
            #update asset
            $update_a = array(
                'status'              => $status,
                'approval_status'     => 0,
                'modified_by'         => $this->session->userdata('sso_id'),
                'modified_time'       => date('Y-m-d H:i:s')
            );
            $update_a_where = array('asset_id'=>$asset_id);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            #audit data
            $new_data = array('asset_status_id' => $status);
            $remarks = "Removed from Stock Transfer:".$trow['stn_number'];
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$asset_arr['country_id']);

            #inserting into asset history
            $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
            $u_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);                        
            $asset_status_history_data = array(
                'status'           => $status,
                'asset_id'         => $asset_id,
                'tool_order_id'    => $tool_order_id,
                'ordered_tool_id'  => $ordered_tool_id,
                'current_stage_id' => $current_stage_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s')
            );
            $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);

            #updating ordered tool quantity
            $dec = 1; 
            $ot_arr = $this->Common_model->get_data_row('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id));
            $new_qty = $ot_arr['quantity']-$dec;
            $new_avail_qty = $ot_arr['available_quantity']-$dec;

            $update_ot = array('quantity'=>$new_qty,'available_quantity'=>$new_avail_qty);
            $update_ot_where = array('ordered_tool_id'=>$ordered_tool_id);
            $this->Common_model->update_data('ordered_tool',$update_ot,$update_ot_where);

            #update the final tool status
            $quantity = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'quantity');
            if($quantity == 0)
            {
                $this->Common_model->update_data('ordered_tool',array('status'=>3),array('ordered_tool_id'=>$ordered_tool_id));
            }

            if(isset($_SESSION['st_asset_id'][$asset_id]))
            {
                unset($_SESSION['st_asset_id'][$asset_id]);
            }
        }
        else
        {
            if(isset($_SESSION['st_asset_id'][$asset_id]))
            {
                unset($_SESSION['st_asset_id'][$asset_id]);
            }
        }
        echo 1;
    }

    public function insert_transfer()
    {
        if($this->input->post('submitorder',TRUE) != '')
        {
            if(count(@$_SESSION['st_asset_id']) == 0 )
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please add more than 1 tool to cart and proceed </div>'); 
                redirect(SITE_URL.'issue_tool'); exit(); 
            }
            
            $ordered_assets = $this->input->post('asset_id',TRUE);
            $deploy_date = format_date(@$this->input->post('deploy_date',TRUE),'Y-m-d');
            
            $wh_id = $this->input->post('wh_id',TRUE);
            $wh_data = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));

            $from_wh_id  = $this->input->post('warehouse_id',TRUE);
            $country_id = $this->Common_model->get_value('warehouse',array('wh_id'=>$from_wh_id),'country_id');

            //Modified by prasad
            $task_access = page_access_check('raise_transfer');
            $current_stage_id  = ($task_access==2 || $task_access==3)?1:4;
           
            #Return Number Generation
            $number = $this->Order_m->get_st_latest_record($country_id);
            if(count($number) == 0)
            {
                $f_order_number = "1-00001";
            }
            else
            {
                $num = ltrim($number['stn_number'],"ST");                    
                $result = explode("-", $num);
                $running_no = (int)$result[1];
                if($running_no == 99999)
                {
                    $first_num = $result[0]+1;
                    $second_num = get_running_sno_five_digit(1);  
                }
                else
                {
                    $first_num = $result[0];
                    $val = $running_no+1;
                    $second_num = get_running_sno_five_digit($val);
                }
                $f_order_number = $first_num.'-'.$second_num;
            }
            $stn_number = "ST".$f_order_number;
            $this->db->trans_begin();

            $order_data = array(
                'sso_id'           => $_SESSION['sso_id'],
                'stn_number'       => $stn_number,                    
                'current_stage_id' => $current_stage_id,
                'wh_id'            => $from_wh_id,
                'to_wh_id'         => $wh_id,
                'request_date'     => $deploy_date,  
                'deploy_date'      => date('Y-m-d'),
                'order_type'       => 1,
                'country_id'       => $country_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s')
            );
            $tool_order_id = $this->Common_model->insert_data('tool_order',$order_data);

            // inserting documents
            $document_type = $this->input->post('document_type',TRUE);
            foreach ($document_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = order_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $doc_name = $_FILES['support_document_'.$key]['name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'         => @$doc_name,
                    'tool_order_id'    => @$tool_order_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s')
                );
                if($value!='')
                {
                    $this->Common_model->insert_data('order_doc',$insert_doc);
                }
            }

            $order_address_data = array(
                'address1'      => $wh_data['address1'],
                'address2'      => $wh_data['address2'],
                'address3'      => $wh_data['address3'],
                'address4'      => $wh_data['address4'],
                'pin_code'      => $wh_data['pin_code'],
                'gst_number'    => $wh_data['gst_number'],
                'pan_number'    => $wh_data['pan_number'],
                'tool_order_id' => $tool_order_id,
                'location_id'   => @$wh_data['location_id'],
                'created_by'    => $this->session->userdata('sso_id'),
                'created_time'  => date('Y-m-d H:i:s')
            );
            $order_address_id = $this->Common_model->insert_data('order_address',$order_address_data);

            // looping tools and get asset which are available
            $tool_id_arr = array();
            foreach ($ordered_assets as $key => $asset_id)
            {
                $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

                $asset_data = asset_info($asset_id);
                $tool_id =$asset_data['tool_id'];

                if(in_array($tool_id, $tool_id_arr))
                { 
                    // updating the ordered tool
                    $ordered_tool_id = $this->Common_model->get_value('ordered_tool',array('tool_order_id'=>$tool_order_id,'tool_id'=>$tool_id),'ordered_tool_id');
                    $inc = 1; 
                    $qry ='UPDATE ordered_tool SET quantity = quantity +'.$inc.' ,
                            available_quantity = available_quantity + '.$inc.'
                         WHERE ordered_tool_id = '.$ordered_tool_id.' ';
                    $this->db->query($qry);
                }
                else
                {
                    $order_tool_data = array(
                        'tool_order_id'      => $tool_order_id,
                        'tool_id'            => $tool_id,
                        'available_quantity' => 1, // initially 1
                        'quantity'           => 1, // initilalty 1
                        'status'             => 1,
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'));
                    $ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);
                }   
                $tool_id_arr[] = $tool_id;
                // updating asset table 
                $asset_status_id = $asset_arr['status'];

                // inserting ordered asset
                $od_asset = array(
                    'asset_id'        => $asset_id,
                    'ordered_tool_id' => $ordered_tool_id,
                    'status'          => 1,
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('ordered_asset',$od_asset);

                #audit data
                $old_data = array('asset_status_id' => $asset_arr['status']);

                $final_asset_status_id = ($asset_status_id == 1)?2:$asset_status_id;
                
                #update asset
                $update_a = array(
                    'status'              => $final_asset_status_id,
                    'approval_status'     => 0,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id'=>$asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                #audit data
                $new_data = array('asset_status_id' => $final_asset_status_id);
                $remarks = "Asset Linked for Stock Transfer:".$stn_number;
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$asset_arr['country_id']);

                $u_data  = array('status'=>$final_asset_status_id);
                $where = array('asset_id'=>$asset_id);
                $this->Common_model->update_data('asset',$u_data,$where);

                // inserting into asset history
                $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
                $u_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);

                $asset_status_history_data = array(
                    'status'           => $final_asset_status_id,
                    'asset_id'         => $asset_id,
                    'tool_order_id'    => $tool_order_id,
                    'ordered_tool_id'  => $ordered_tool_id,
                    'current_stage_id' => $current_stage_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);
            }

            $order_status_history_data = array(
                'current_stage_id' => $current_stage_id,
                'tool_order_id'    => $tool_order_id,
                'remarks'          => 'Raised ST Request',
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1, 
            );
            $this->Common_model->insert_data('order_status_history',$order_status_history_data);

            if($this->db->trans_status === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'raise_transfer'); exit();
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Transfer Request has been Raised successfully With ST Number :'.$stn_number.' </div>');
            }
        } 
        redirect(SITE_URL.'raise_transfer'); exit();  
    }

    public function update_transfer_list()
    {
        $data['nestedView']['heading']="Update Stock Transfer";
        $data['nestedView']['cur_page'] = 'raise_transfer';
        $data['nestedView']['parent_page'] = 'raise_transfer';  

        $task_access = page_access_check($data['nestedView']['parent_page']); 
        
        $warehouse_id = $this->input->post('warehouse_id',TRUE);
        if($warehouse_id=='')
        {
            $warehouse_id = $this->session->userdata('st_wh_id');
        }

        $page_type_check = $this->input->post('page_type_check',TRUE);
        if($page_type_check=='')
        {
            $page_type_check = $this->session->userdata('page_type_check');
        }

        $tool_order_id = storm_decode($this->uri->segment(2));
        if($warehouse_id == '' || $tool_order_id == '' || $page_type_check=='')
        {
            redirect(SITE_URL); exit();
        }

        if(@$warehouse_id != '')
        {
            $_SESSION['st_wh_id'] = $warehouse_id;
            $_SESSION['page_type_check'] = $page_type_check;
            $wh = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$warehouse_id));
            $data['wh_name'] = $wh['wh_code'].' - ('.$wh['name'].')';
             
            $data['nestedView']['heading']="Update Stock Transfer";
            $data['nestedView']['cur_page'] = 'raise_transfer';
            $data['nestedView']['parent_page'] = 'raise_transfer';
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/transfer.js"></script>';
             $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

            $data['nestedView']['css_includes'] = array();
            
            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Update Stock Transfer';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            if($page_type_check == 1)//for admin page
            {
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'List Of Stock Transfer','class'=>'','url'=>SITE_URL.'openSTforOrder');
            }
            else if($page_type_check == 2)//for logistic page
            {
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer Requests','class'=>'','url'=>SITE_URL.'wh_stock_transfer_list');
            }
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Stock Transfer','class'=>'active','url'=>'');

            # Search Functionality
           $psearch=$this->input->post('search', TRUE);
            if($psearch!='') 
            {
                $asset_number = validate_string($this->input->post('asset_number', TRUE));
                #check asset number is available in other ST Or not
                if($asset_number!='')
                {
                    $asset_id = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number),'asset_id');
                    if($asset_id!='')
                    {
                        /*koushik 11-10-2018 check asset is involved in ST or not */
                        $st_entry = check_for_st_entry($asset_id,$tool_order_id);
                        if(count($st_entry)>0)
                        {
                            $stn_number = $st_entry['stn_number'];
                            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-times-circle"></i></div>
                            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Remove asset from ST to proceed !.</div>'); 
                            $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
                            $asset_number = '';
                            redirect(SITE_URL.'update_transfer_list/'.storm_encode($tool_order_id).'/'.$current_offset); exit();
                        }
                    }
                }
                $searchParams=array(
                    'part_number'      => validate_string($this->input->post('part_number', TRUE)),
                    'asset_number'     => $asset_number,
                    'part_description' =>validate_string($this->input->post('part_description', TRUE)),
                    'modality_id'      => validate_number($this->input->post('modality_id', TRUE)),
                    'asset_status_id'  => $this->input->post('asset_status_id', TRUE),
                    'serial_no'        => validate_string($this->input->post('serial_no',TRUE))
                );
                $this->session->set_userdata($searchParams);
            } 
            else 
            {
                if($this->input->post('reset')!='')
                {
                    $searchParams=array(
                        'part_number'      => '',
                        'asset_number'     => '',
                        'part_description' => '',
                        'modality_id'      => '',
                        'asset_status_id'  => '',
                        'serial_no'        => ''
                    );
                    $this->session->set_userdata($searchParams);
                }

                else if($this->uri->segment(3)!='')
                {
                    $searchParams=array(
                        'part_number'      => $this->session->userdata('part_number'),
                        'asset_number'     => $this->session->userdata('asset_number'),
                        'part_description' => $this->session->userdata('part_description'),
                        'modality_id'      => $this->session->userdata('modality_id'),
                        'asset_status_id'  => $this->session->userdata('asset_status_id'),
                        'serial_no'        => $this->session->userdata('serial_no')
                    );
                }
                else 
                {
                    $searchParams=array(
                        'part_number'      => '',
                        'asset_number'     => '',
                        'part_description' => '',
                        'modality_id'      => '',
                        'asset_status_id'  => '',
                        'serial_no'        => ''
                    );
                    $this->session->set_userdata($searchParams);
                }
                
            }
            $data['searchParams'] = $searchParams;

            if(!isset($_POST['reset']) && !isset($_POST['search']) && !isset($_POST['add']) && !isset($_POST['raise_transfer']))
            { 
                if($this->uri->segment(3)=='')
                {
                    if(isset($_SESSION['st_asset_id'])) unset($_SESSION['st_asset_id']);
                }
            }

            $assets_list = $this->Transfer_m->get_existing_asset_list($tool_order_id);
            // Logic For Cart Functionality 
            if(isset($_POST['add']))
            {
                if(!isset($_SESSION['st_asset_id']))
                {   
                    $_SESSION['st_asset_id'] = array();
                }
                $a = count(@$_POST['asset_id']);
                //$_SESSION['st_asset_id'] = array();
                for ($i=0; $i < $a; $i++)
                {   
                    $_SESSION['st_asset_id'][$_POST['asset_id'][$i]] = @$_POST['asset_id'][$i];
                }
                $_SESSION['dummy'] = 1;
            }
            else
            {
                #append existing asset list in tool order id
                if(!isset($_SESSION['st_asset_id']))
                {   
                    $_SESSION['st_asset_id'] = array();
                }
                $a = count(@$assets_list);
                //$_SESSION['st_asset_id'] = array();
                for ($i=0; $i < $a; $i++)
                {   
                    $_SESSION['st_asset_id'][$assets_list[$i]] = @$assets_list[$i];
                }
            }
            
            # Default Records Per Page - always 10
            /* pagination start */
            $config = get_paginationConfig();
            $config['base_url'] = SITE_URL . 'update_transfer_list/'.storm_encode($tool_order_id).'/';
            //print_r($searchParams['asset_status_id']); exit;
            # Total Records
            $config['total_rows'] = $this->Transfer_m->tool_total_num_rows($searchParams,$warehouse_id,$assets_list);
            //echo $this->db->last_query(); exit;
            $config['per_page'] = getDefaultPerPageRecords();
            $data['total_rows'] = $config['total_rows'];
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            if ($data['pagination_links'] != '') {
                $data['last'] = $this->pagination->cur_page * $config['per_page'];
                if ($data['last'] > $data['total_rows']) {
                    $data['last'] = $data['total_rows'];
                }
                $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
            }
            $data['sn'] = $current_offset + 1;
            /* pagination end */
            # Loading the data array to send to View
            $data['toolResults'] = $this->Transfer_m->tool_results($current_offset, $config['per_page'], $searchParams,$warehouse_id,$assets_list);
            
            $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
            
            # Additional data
            $data['displayResults'] = 1;
            $data['warehouse_id'] = $warehouse_id;
            $data['current_offset'] = $current_offset;
            $data['task_access']=$task_access;
            $data['tool_order_id'] = $tool_order_id;
            $data['page_type_check'] = $page_type_check;
            $this->load->view('transfer/update_transfer_list',$data);
        }
    }

    public function edit_transfer()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        $page_type_check = storm_decode($this->uri->segment(3));
        if($tool_order_id== '' || $page_type_check == '')
        {
            if($page_type_check == '')// 1 for admin, 2 for logistic
            {
                $this->session->set_flashdata('response','<div class=" col-md-offset-1 col-md-10 alert alert-danger alert-white rounded" style="margin-top:10px;">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Pass 3 segment Value ! 1- for admin, 2- logistic </div>'); 
            }       
            redirect(SITE_URL.'home'); exit(); 
        }  
    
        #unlink scanned assets in warehouse
        $this->unlink_st_scanned_assets($tool_order_id);

        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Update Transfer Details";
        if($page_type_check == 1)//for admin page
        {
            $data['nestedView']['cur_page'] = 'openSTforOrder';
            $data['nestedView']['parent_page'] = 'openSTforOrder';
            $data['cancel_button'] = 'openSTforOrder';
        }
        else if($page_type_check == 2)//for logistic page
        {
            $data['nestedView']['cur_page'] = 'wh_stock_transfer_list';
            $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';
            $data['cancel_button'] = 'wh_stock_transfer_list';
        }

        #page Authentication 
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/transfer.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Stock Transfer Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        if($page_type_check == 1)//for admin page
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'List Of Stock Transfer','class'=>'','url'=>SITE_URL.'openSTforOrder');
        }
        else if($page_type_check == 2)//for logistic page
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer Requests','class'=>'','url'=>SITE_URL.'wh_stock_transfer_list');
        }
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Stock Transfer Details','class'=>'active','url'=>'');

        # Additional data
        #session data
        if(isset($_SESSION['st_asset_id']))
        {
            $result_data = array();
            foreach ($_SESSION['st_asset_id'] as $key => $asset_id)
            {
                $asset_res = $this->Transfer_m->get_ordered_assets($tool_order_id,$asset_id);
                if(count($asset_res)==0)
                {
                    $asset_res = asset_info($asset_id);
                }

                $result_data[] = $asset_res;
            }
            $data['assets'] = $result_data;
        }
        else
        {
            $this->session->unset_userdata('st_asset_id');
            $data['assets'] =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        }
        
        $data['form_action'] = SITE_URL.'update_transfer/'.storm_encode($page_type_check);
        $data['order_info'] = $this->Transfer_m->get_order_info($tool_order_id);
        
        $data['sso_id']= $data['order_info']['sso_id'];
        $wh_id = $data['order_info']['wh_id'];
        $data['from_wh_id'] = $wh_id;
        $data['warehouse_id'] = $wh_id;
        $wh = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
        $data['wh_name'] = $wh['wh_code'].' - ('.$wh['name'].')';
        $data['document_type'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>3));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>3));

        $data['wh_data'] = getWhArrNotInID($data['from_wh_id']);
        $data['page_type_check'] = $page_type_check;
        $attach_document =$this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document;
        $data['flg'] = 2; 
        $this->load->view('transfer/update_issue_tool_st',$data);
    }

    public function unlink_st_scanned_assets($tool_order_id)
    {
        $tool_order_id = $tool_order_id;
        if($tool_order_id=='')
        {
            redirect(SITE_URL); exit();
        }
        $this->load->model('Wh_stock_transfer_m');
        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        if($check_st=='') //stock transfer
        {  
            $check_st_type = 0; //ST
        } 
        else if($check_st !='') //tool order
        { 
            $check_st_type = 1; //order
        } 
        $oa_arr = $this->Wh_stock_transfer_m->get_for_scanned_assets($tool_order_id);
        if(count($oa_arr)>0)
        {
            $this->db->trans_begin();
            if($check_st_type == 1)//for order
            {
                foreach ($oa_arr as $key => $value) 
                {
                    $oah_id = $this->Common_model->get_value('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']),'oah_id');

                    #delete from order_asset_health
                    $this->Common_model->delete_data('order_asset_health',array('oah_id'=>$oah_id));

                    #delete from order_asset_history
                    $this->Common_model->delete_data('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']));

                    #delete from ordered_asset
                    $this->Common_model->delete_data('ordered_asset',array('ordered_asset_id'=>$value['ordered_asset_id']));

                    #delete from asset status history
                    $this->Common_model->delete_data('asset_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>1,'asset_id'=>$value['asset_id']));

                    #update asset status history
                    $update_data3 = array('end_time'    => date('Y-m-d H:i:s'),
                                          'modified_by' =>$this->session->userdata('sso_id'));
                    $update_where3 = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
                    $this->Common_model->update_data('asset_status_history',$update_data3,$update_where3);

                    $update_data4 = array('status'=>2);
                    $update_where4 = array('asset_id'=>$value['asset_id']);
                    $this->Common_model->update_data('asset',$update_data4,$update_where4);

                    $insert_data5 = array('asset_id'        => $value['asset_id'],
                                          'created_by'      => $this->session->userdata('sso_id'),
                                          'created_time'    => date('Y-m-d H:i:s'),
                                          'status'          => 2);
                    $this->Common_model->insert_data('asset_status_history',$insert_data5);
                }
            }
            else if($check_st_type == 0)//for ST
            {   
                foreach ($oa_arr as $key => $value) 
                {
                    $oah_id = $this->Common_model->get_value('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']),'oah_id');

                    #delete from order_asset_health
                    $this->Common_model->delete_data('order_asset_health',array('oah_id'=>$oah_id));

                    #delete from order_asset_history
                    $this->Common_model->delete_data('order_asset_history',array('ordered_asset_id'=>$value['ordered_asset_id']));

                    $current_status = $this->Common_model->get_value('asset',array('asset_id'=>$value['asset_id']),'status');
                    if($current_status == 8)
                    {
                        #delete from asset status history
                        $this->Common_model->delete_data('asset_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>1,'asset_id'=>$value['asset_id']));

                        #update asset status history
                        $update_data3 = array('end_time'    => date('Y-m-d H:i:s'),
                                              'modified_by' =>$this->session->userdata('sso_id'));
                        $update_where3 = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
                        $this->Common_model->update_data('asset_status_history',$update_data3,$update_where3);

                        $update_data4 = array('status'=>2);
                        $update_where4 = array('asset_id'=>$value['asset_id']);
                        $this->Common_model->update_data('asset',$update_data4,$update_where4);

                        $insert_data5 = array('asset_id'        => $value['asset_id'],
                                              'created_by'      => $this->session->userdata('sso_id'),
                                              'created_time'    => date('Y-m-d H:i:s'),
                                              'status'          => 2);
                        $this->Common_model->insert_data('asset_status_history',$insert_data5);
                    }
                }
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }
        }
        
        return 0;
    }

    public function update_transfer()
    {
        $page_type_check = storm_decode(@$this->uri->segment(2));
        if($page_type_check == '')
        {
            redirect(SITE_URL.'home'); exit();
        }

        if($this->input->post('submitorder',TRUE) != '')
        {
            $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
            $to_wh_id = validate_number($this->input->post('wh_id',TRUE));
            $stn_number = $this->input->post('stn_number',TRUE);
            $request_date = format_date($this->input->post('deploy_date',TRUE));
            $wh_data = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$to_wh_id));

            $this->db->trans_begin();

            $order_data = array(
                'to_wh_id'      => $to_wh_id,                    
                'request_date'  => $request_date,                    
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $this->Common_model->update_data('tool_order',$order_data,array('tool_order_id'=>$tool_order_id));

            // inseting documents
            $document_type = $this->input->post('document_type',TRUE);
            foreach ($document_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {     
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = order_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $doc_name = $_FILES['support_document_'.$key]['name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'         => @$doc_name,
                    'tool_order_id'    => @$tool_order_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s')
                );

                if($value!='')
                {
                    $this->Common_model->insert_data('order_doc',$insert_doc);
                }
            }

            $order_address_data = array(
                'address1'      => $wh_data['address1'],
                'address2'      => $wh_data['address2'],
                'address3'      => $wh_data['address3'],
                'address4'      => $wh_data['address4'],
                'gst_number'    => $wh_data['gst_number'],
                'pan_number'    => $wh_data['pan_number'],
                'pin_code'      => $wh_data['pin_code'],
                'location_id'   => $wh_data['location_id'],                    
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $order_address_id = $this->Common_model->update_data('order_address',$order_address_data,array('tool_order_id'=>$tool_order_id));


            #update ordered tool and ordered asset info
            if(isset($_SESSION['st_asset_id']))
            {
                $ordered_assets = $this->input->post('asset_id',TRUE);
                // looping tools and get asset which are available
                $tool_id_arr = array();
                foreach ($ordered_assets as $key => $asset_id)
                {
                    $asset_res = $this->Transfer_m->get_ordered_assets($tool_order_id,$asset_id);
                    if(count($asset_res)==0)
                    {
                        $asset_data = asset_info($asset_id);
                        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

                        $tool_id = $asset_data['tool_id'];

                        if(in_array($tool_id, $tool_id_arr))
                        { 
                            // updating the ordered tool
                            $ordered_tool_id = $this->Common_model->get_value('ordered_tool',array('tool_order_id'=>$tool_order_id,'tool_id'=>$tool_id),'ordered_tool_id');
                            
                            $inc = 1; 
                            $qry ='UPDATE ordered_tool SET quantity = quantity +'.$inc.',
                                    available_quantity = available_quantity + '.$inc.'
                                 WHERE ordered_tool_id = '.$ordered_tool_id.' ';
                            $this->db->query($qry);
                        }
                        else
                        {
                            $order_tool_data = array(
                                'tool_order_id'      => $tool_order_id,
                                'tool_id'            => $tool_id,
                                'available_quantity' => 1, // initially 1
                                'quantity'           => 1, // initilalty 1
                                'status'             => 1,
                                'created_by'         => $this->session->userdata('sso_id'),
                                'created_time'       => date('Y-m-d H:i:s')
                            );
                            $ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);
                        }   
                        $tool_id_arr[] = $tool_id;
                        // updating asset table 
                        $asset_status_id = $asset_data['asset_status_id'];

                        // inserting ordered asset
                        $od_asset = array(
                            'asset_id'        => $asset_id,
                            'ordered_tool_id' => $ordered_tool_id,
                            'status'          => 1,
                            'created_by'      => $this->session->userdata('sso_id'),
                            'created_time'    => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->insert_data('ordered_asset',$od_asset);
                        $final_asset_status_id = ($asset_status_id == 1)?2:$asset_status_id;
                        
                        #audit data
                        $old_data = array('asset_status_id' => $asset_arr['status']);

                        #update asset
                        $update_a = array(
                            'status'          => $final_asset_status_id,
                            'approval_status' => 0,
                            'modified_by'     => $this->session->userdata('sso_id'),
                            'modified_time'   => date('Y-m-d H:i:s')
                        );
                        $update_a_where = array('asset_id'=>$asset_id);
                        $this->Common_model->update_data('asset',$update_a,$update_a_where);

                        #audit data
                        $new_data = array('asset_status_id' => $final_asset_status_id);
                        $remarks = "Asset has been Linked for Stock transfer no:".$stn_number;
                        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$asset_arr['country_id']);

                        // inserting into asset history
                        $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
                        $u_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                        $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);

                        $asset_status_history_data = array(
                            'status'            => $final_asset_status_id,
                            'asset_id'          => $asset_id,
                            'tool_order_id'     => $tool_order_id,
                            'ordered_tool_id'   => $ordered_tool_id,
                            'current_stage_id'  => $current_stage_id,
                            'created_by'        => $this->session->userdata('sso_id'),
                            'created_time'      => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);
                    }
                    else
                    {
                        $tool_id_arr[] = $asset_res['tool_id'];
                    }
                }
            }
            $this->session->unset_userdata('st_asset_id');

            if($this->db->trans_status === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Stock Transfer Details has been Updated successfully, For STN Number: <strong>'.$stn_number.'</strong> ! </div>');
            }
        } 
        if($page_type_check == 1)//at admin page
        {
            redirect(SITE_URL.'openSTforOrder'); exit();
        }
        else if($page_type_check == 2)//at logistic page
        {
            redirect(SITE_URL.'wh_stock_transfer_list'); exit();
        }    
    }

    public function cancel_transfer()
    {
        $task_access = page_access_check('raise_transfer');
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'wh_stock_transfer_list');
            exit;
        } 
        
        $ordered_assets =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        $stn_number = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'stn_number');

        foreach ($ordered_assets as $key => $values)
        {
            $asset_status_id  = $values['asset_status_id'];
            $asset_id         = $values['asset_id'];
            $ordered_tool_id  = $values['ordered_tool_id'];
            $current_stage_id = $values['current_stage_id'];
            $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

            #audit data
            $old_data = array('asset_status_id' => $asset_arr['status']);

            #update asset
            $status = ($asset_status_id == 2)?1:$asset_status_id;
            $update_a = array(
                'status'          => $status,
                'approval_status' => 0,
                'modified_by'     => $this->session->userdata('sso_id'),
                'modified_time'   => date('Y-m-d H:i:s')
            );
            $update_a_where = array('asset_id'=>$asset_id);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            #audit data
            $new_data = array('asset_status_id' => $status);
            $remarks = "Stock Transfer is cancelled, ST No:".$stn_number;
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$asset_arr['country_id']);

            // inserting into asset history
            $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
            $u_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);                        

            $asset_status_history_data = array(
                'status'           => $status,
                'asset_id'         => $asset_id,
                'tool_order_id'    => $tool_order_id,
                'ordered_tool_id'  => $ordered_tool_id,
                'current_stage_id' => $current_stage_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s')
            );
            $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);
        }
        
        $wh_data = array(
            'status'           => 4,
            'current_stage_id' => 3,
            'modified_by'      => $this->session->userdata('sso_id'),
            'modified_time'    => date('Y-m-d H:i:s'),
            'remarks'          => 'Cancelled Stock transfer'.$stn_number
        );
        $this->Common_model->update_data('tool_order',$wh_data,array('tool_order_id'=>$tool_order_id));

        
        $this->Common_model->insert_data('order_status_history',array('status'=>1,'current_stage_id'=>3,'remarks'=>'Cancelled The Order','tool_order_id'=>$tool_order_id));

        if($this->db->trans_status === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Stock Transfer has been Cancelled successfully For ST Number:'.$stn_number.'</div>');
        }
        if($task_access ==2 || $task_access == 3)
        {
            redirect(SITE_URL.'closedSTforOrder'); exit();
        }
        else
        {
            redirect(SITE_URL.'closed_wh_delivery_list'); exit();
        }
    }

    public function approveOrRejectST()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        $cancel_form_action =@storm_decode($this->uri->segment(3));
        if(@$cancel_form_action == 1 ) // 1 For Only View and goto open STs
        {
            $data['form_action'] = SITE_URL.'raiseSTforOrder';
            $data['view'] = 1;   
            $data['nestedView']['heading']="Stock Transfer  Details";
            $data['nestedView']['cur_page'] = 'raiseSTforOrder';
            $data['nestedView']['parent_page'] = 'raiseSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer','class'=>'','url'=>SITE_URL.'raiseSTforOrder');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer Details','class'=>'active','url'=>''); 
            $data['assets'] =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        }
        if(@$cancel_form_action == 2)
        {
            $data['form_action'] = SITE_URL.'openSTforOrder';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Transfer  Details";
            $data['nestedView']['cur_page'] = 'openSTforOrder';
            $data['nestedView']['parent_page'] = 'openSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer','class'=>'','url'=>SITE_URL.'openSTforOrder');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer Details','class'=>'active','url'=>'');
            $data['tools'] =  $this->Transfer_m->get_ordered_tools($tool_order_id);
        }
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'raiseSTforOrder');
            exit;
        }    
        
        $data['order_info'] = $this->Transfer_m->get_order_info($tool_order_id);
        $data['wh_data'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$data['order_info']['wh_id']));
        $attach_document = $this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));
        
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document; 
        $this->load->view('transfer/transfer_action',$data);
    }

    public function transfer_order_info()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        $cancel_form_action =@storm_decode($this->uri->segment(3));
        if(@$cancel_form_action == 1)  // Admin Open Order For Order ST
        {
            $data['form_action'] = SITE_URL.'openSTforOrder';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Transfer  Details";
            $data['nestedView']['cur_page'] = 'openSTforOrder';
            $data['nestedView']['parent_page'] = 'openSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer','class'=>'','url'=>SITE_URL.'openSTforOrder');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer Details','class'=>'active','url'=>'');
            
        }
        if(@$cancel_form_action == 2)  // WH Shipment Deliveries View
        {
            $data['form_action'] = SITE_URL.'wh_stock_transfer_list';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="WH Stock Transfer List";
            $data['nestedView']['cur_page'] = 'wh_stock_transfer_list';
            $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'WH Stock Transfer List';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'WH Stock Transfer List','class'=>'','url'=>SITE_URL.'wh_stock_transfer_list');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'WH Stock Transfer Details','class'=>'active','url'=>'');
            
        }
        if(@$cancel_form_action == 3)  // FE Acknowledge 
        {
            $data['form_action'] = SITE_URL.'receive_order';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="WH Stock Transfer List";
            $data['nestedView']['cur_page'] = 'wh_stock_transfer_list';
            $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'WH Stock Transfer List';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'WH Stock Transfer List','class'=>'','url'=>SITE_URL.'receive_order');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'WH Stock Transfer Details','class'=>'active','url'=>'');
            
        }
        
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'raiseSTforOrder');
            exit;
        }    
        $data['tools'] =  $this->Transfer_m->get_ordered_tools($tool_order_id);
        $data['order_info'] = $this->Transfer_m->get_order_info($tool_order_id);
        $data['wh_data'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$data['order_info']['wh_id']));
        $attach_document = $this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));
        
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document;
        $this->load->view('transfer/transfer_order_info',$data);
    }

    public function wh_transfer_info()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        $cancel_form_action =@storm_decode($this->uri->segment(3));
        if(@$cancel_form_action == 1 )  // 1 For Only View and goto Stock Transfer Shipment Requests
        {
            $data['form_action'] = SITE_URL.'wh_stock_transfer_list';
            $data['nestedView']['heading']="Stock Transfer Details";
            $data['nestedView']['cur_page'] = 'wh_stock_transfer_list';
            $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';

            #page Authentication
            $task_access = page_access_check($data['nestedView']['parent_page']);

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Stock Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer Requests','class'=>'','url'=>SITE_URL.'wh_stock_transfer_list');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer Details','class'=>'active','url'=>'');

            $data['assets'] =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        }  
        if(@$cancel_form_action == 2)   // for closed st deliveriees  at Warehosue 
        {
            $data['form_action'] = SITE_URL.'closed_wh_delivery_list'; 

            $data['nestedView']['heading']="Closed Stock Transfer Details";
            $data['nestedView']['cur_page'] = 'closed_wh_delivery';
            $data['nestedView']['parent_page'] = 'closed_wh_delivery';

            #page Authentication
            $task_access = page_access_check($data['nestedView']['parent_page']);

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();          

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Closed Stock Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Wh Delivery List','class'=>'','url'=>SITE_URL.'closed_wh_delivery_list');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Details','class'=>'active','url'=>'');
            $data['assets'] =  $this->Transfer_m->get_closed_ordered_assets($tool_order_id,2); // 2 for shipment done
        }
        if(@$cancel_form_action == 3 )   // Raised Stock Transfer At Admin  
        {
            $data['form_action'] = SITE_URL.'raiseSTforOrder';
             
            $data['nestedView']['heading']="Transfer  Details";
            $data['nestedView']['cur_page'] = 'raiseSTforOrder';
            $data['nestedView']['parent_page'] = 'raiseSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer','class'=>'','url'=>SITE_URL.'raiseSTforOrder');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer Details','class'=>'active','url'=>'');   
            $data['assets'] =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        }
        if(@$cancel_form_action == 4)   // 4 For Open Order at Admin To see 
        {
            $data['form_action'] = SITE_URL.'openSTforOrder';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Transfer Details";
            $data['nestedView']['cur_page'] = 'openSTforOrder';
            $data['nestedView']['parent_page'] = 'openSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock transfer','class'=>'','url'=>SITE_URL.'openSTforOrder');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer Details','class'=>'active','url'=>'');

            $data['assets'] =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        }
        if(@$cancel_form_action == 5)   // 5 For Closed WH acknowledgements 
        {
            $data['form_action'] = SITE_URL.'wh2_wh_closed';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Closed Stock Transfer Acknowledgements";
            $data['nestedView']['cur_page'] = 'wh2_wh_closed';
            $data['nestedView']['parent_page'] = 'wh2_wh_closed';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Closed Stock Transfer Acknowledgements';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Acknowledgements','class'=>'','url'=>SITE_URL.'wh2_wh_closed');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Acknowledgements Details','class'=>'active','url'=>'');

            $order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>3),'order_status_id');
            $data['assets'] =  $this->Order_m->getAssetDataByOrderStatusID($order_status_id);
        }
        if(@$cancel_form_action == 6)   // 6 For Closed Stock Transfer Details 
        {
            $data['form_action'] = SITE_URL.'closedSTforOrder';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Closed Stock Transfer Details";
            $data['nestedView']['cur_page'] = 'closedSTforOrder';
            $data['nestedView']['parent_page'] = 'closedSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Closed Stock Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Details','class'=>'','url'=>SITE_URL.'closedSTforOrder');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Details','class'=>'active','url'=>'');

            $data['assets'] =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        }
        if(@$cancel_form_action == 7)   // 7 For  Stock Transfer Acknowlege at  Details 
        {
            $data['form_action'] = SITE_URL.'wh2_wh_receive';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']=" Stock Transfer Details";
            $data['nestedView']['cur_page'] = 'wh2_wh_receive';
            $data['nestedView']['parent_page'] = 'wh2_wh_receive';

            #page Authentication
            $task_access = page_access_check($data['nestedView']['parent_page']);

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = ' Stock Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>' Stock Transfer Details','class'=>'','url'=>SITE_URL.'wh2_wh_receive');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>' Stock Transfer Details','class'=>'active','url'=>'');
            
            $data['assets'] =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        }

        $data['drow'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $data['order_info'] = $this->Transfer_m->get_order_info($tool_order_id);
        $data['wh_data'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$data['order_info']['wh_id']));
        $attach_document = $this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));        
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document; 
        $data['flg'] = 1;
        $this->load->view('transfer/transfer_view',$data);
    }

    public function closed_wh_transfer_info()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        $cancel_form_action =@storm_decode($this->uri->segment(3));
        if(@$cancel_form_action == 5)   // 5 For Closed WH acknowledgements 
        {
            $data['form_action'] = SITE_URL.'wh2_wh_closed';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Closed Stock Transfer Acknowledgements";
            $data['nestedView']['cur_page'] = 'wh2_wh_closed';
            $data['nestedView']['parent_page'] = 'wh2_wh_closed';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Closed Stock Transfer Acknowledgements';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Acknowledgements','class'=>'','url'=>SITE_URL.'wh2_wh_closed');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Acknowledgements Details','class'=>'active','url'=>'');

           $order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>3),'order_status_id');
            $data['assets'] =  $this->Transfer_m->getAssetDataByOrderStatusID($order_status_id);
        }
        if(@$cancel_form_action == 6)   // 6 For Closed Stock Transfer Details 
        {
            $data['form_action'] = SITE_URL.'closedSTforOrder';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Closed Stock Transfer Details";
            $data['nestedView']['cur_page'] = 'closedSTforOrder';
            $data['nestedView']['parent_page'] = 'closedSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Closed Stock Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Details','class'=>'','url'=>SITE_URL.'closedSTforOrder');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Stock Transfer Details','class'=>'active','url'=>'');

            $order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>3),'order_status_id');
            $data['assets'] =  $this->Transfer_m->getAssetDataByOrderStatusID($order_status_id);
        }
        if(@$cancel_form_action == 7)   // 7 For  Stock Transfer Acknowlege at  Details 
        {
            $data['form_action'] = SITE_URL.'wh2_wh_receive';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']=" Stock Transfer Details";
            $data['nestedView']['cur_page'] = 'wh2_wh_receive';
            $data['nestedView']['parent_page'] = 'wh2_wh_receive';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = ' Stock Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>' Stock Transfer Details','class'=>'','url'=>SITE_URL.'wh2_wh_receive');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>' Stock Transfer Details','class'=>'active','url'=>'');

            $data['assets'] =  $this->Transfer_m->get_ordered_assets($tool_order_id);
        }
        if(@$cancel_form_action == 1)  // Admin Open Order For Order ST
        {
            $data['form_action'] = SITE_URL.'openSTforOrder';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Transfer  Details";
            $data['nestedView']['cur_page'] = 'openSTforOrder';
            $data['nestedView']['parent_page'] = 'openSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer','class'=>'','url'=>SITE_URL.'openSTforOrder');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer Details','class'=>'active','url'=>'');
            $order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>3),'order_status_id');
            $data['assets'] =  $this->Transfer_m->getAssetDataByOrderStatusID($order_status_id); 
        }
        if(@$cancel_form_action == 10)  // After FE acknowledged
        {
            $data['form_action'] = SITE_URL.'receive_order';
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Transfer  Details";
            $data['nestedView']['cur_page'] = 'openSTforOrder';
            $data['nestedView']['parent_page'] = 'openSTforOrder';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Transfer Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer','class'=>'','url'=>SITE_URL.'receive_order');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Transfer Details','class'=>'active','url'=>'');
            $order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>3),'order_status_id');
            $data['assets'] =  $this->Transfer_m->getAssetDataByOrderStatusID($order_status_id); 
        }
        $data['drow'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $data['order_info'] = $this->Transfer_m->get_order_info($tool_order_id);
        $data['wh_data'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$data['order_info']['wh_id']));
        $attach_document = $this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));        
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document; 
        $data['flg'] = 1;
        $this->load->view('transfer/closed_transfer_view',$data);
    }

    public function submitApproveOrRejectST()
    {
        $submit_action = $this->input->post('submit_action',TRUE);
        $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
        $stn_number  = validate_string($this->input->post('stn_number',TRUE));
        $admin_remarks  = validate_number($this->input->post('admin_remarks',TRUE));
        //  1 is for approve
        $where = array('tool_order_id'=>$tool_order_id);
        if($submit_action == 1)
        {
            $to_data = array(
                'current_stage_id' => 1,
                'remarks'          => $admin_remarks,
                'modified_by'      => $this->session->userdata('sso_id'),
                'modified_time'    => date('Y-m-d H:i:s')
            );
            $this->Common_model->update_data('tool_order',$to_data,$where);

            // updating the old order status history
            $this->Common_model->update_data('order_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('tool_order_id'=>$tool_order_id,'current_stage_id'=>4));
            $osh_data = array(
                'tool_order_id'    => $tool_order_id,
                'current_stage_id' => 1,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s')
                );
            $this->Common_model->insert_data('order_status_history',$osh_data);
        }
        else
        {
            $to_data = array(
                'current_stage_id' => 3,
                'remarks'          => $admin_remarks,
                'modified_by'      => $this->session->userdata('sso_id'),
                'modified_time'    => date('Y-m-d H:i:s'),
                'status'           => 5
            );
            $this->Common_model->update_data('tool_order',$to_data,$where);
            // updating the old order status history
            $this->Common_model->update_data('order_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('tool_order_id'=>$tool_order_id,'current_stage_id'=>4));
            $osh_data = array(
                'tool_order_id'    => $tool_order_id,
                'current_stage_id' => 3,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s')
            );
            $this->Common_model->insert_data('order_status_history',$osh_data);

            $ordered_assets =  $this->Transfer_m->get_ordered_assets($tool_order_id);
            
            // unlinking the status of assets
            foreach ($ordered_assets as $key => $values)
            {
                $asset_status_id  = $values['asset_status_id'];
                $asset_id         = $values['asset_id'];
                $ordered_tool_id  = $values['ordered_tool_id'];
                $current_stage_id = $values['current_stage_id'];
                $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                
                #audit data
                $old_data = array('asset_status_id' => $asset_arr['status']);

                #update asset
                $status = ($asset_status_id == 2)?1:$asset_status_id;
                $update_a = array(
                    'status'          => $status,
                    'approval_status' => 0,
                    'modified_by'     => $this->session->userdata('sso_id'),
                    'modified_time'   => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id'=>$asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                #audit data
                $new_data = array('asset_status_id' => $status);
                $remarks = "Stock transfer:".$stn_number." is rejected by Admin";
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$asset_arr['country_id']);

                // inserting into asset history
                $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
                $u_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);                        

                $asset_status_history_data = array(
                    'status'           => $status,
                    'asset_id'         => $asset_id,
                    'tool_order_id'    => $tool_order_id,
                    'ordered_tool_id'  => $ordered_tool_id,
                    'current_stage_id' => $current_stage_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);
            }
        }
        if($this->db->trans_status === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>');
        }
        else
        {
            $this->db->trans_commit();
            if($submit_action == 1)
            {
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> STN Number : <strong>'.$stn_number.'</strong> Has Been Approved successfully ! </div>');
            }
            else
            {
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> STN Number : <strong>'.$stn_number.'</strong> Has Been Rejected successfully ! </div>');
            }
        }
        redirect(SITE_URL.'raiseSTforOrder'); exit();
    }

    public function viewFeClosedReturnDetails()
    {
        $return_order_id = storm_decode($this->uri->segment(2));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'closed_fe_returns'); exit();
        }

        $data['nestedView']['heading']="View FE Closed Return Details";
        $data['nestedView']['cur_page'] = "closed_order";
        $data['nestedView']['parent_page'] = 'closed_order';

        # include files
        $data['nestedView']['js_includes'] = array();
        
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View FE Closed Return Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Order','class'=>'','url'=>SITE_URL.'closed_order');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View FE Closed  Return Details','class'=>'active','url'=>'');

        $return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
        {
            $trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['wh_id']));
            $trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
        }
        else 
        {
            $trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
            $sso_id = $this->Common_model->get_value('tool_order_id',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
            $sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
            $trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];


        }
        $trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');

        $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
        $rrow = $this->Pickup_point_m->get_closed_return_details($return_order_id,$rto_id);
        $asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
        foreach ($asset_list as $key => $value) 
        {
            $components = $this->Pickup_point_m->get_asset_components($value['asset_id']);
            $asset_list[$key]['components'] = $components;
        }

        #to select 9th stage value
        $os_id = $rrow['order_status_id'];
        $order_asset = $this->Common_model->get_data('order_asset_history',array('order_status_id'=>$os_id));
        $asset_val = array();
        foreach ($order_asset as $row) 
        {
            $oahealth = $this->Common_model->get_data('order_asset_health',array('oah_id'=>$row['oah_id']));
            $assetid = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$row['ordered_asset_id']),'asset_id');
            foreach ($oahealth as $key => $value) 
            {
                $asset_val[$assetid]['asset_status'] = $row['status'];
                $asset_val[$assetid][$value['part_id']]['asset_condition_id'] = $value['asset_condition_id'];
                $asset_val[$assetid][$value['part_id']]['remarks'] = $value['remarks'];
            }
        }
        
        $data['flg'] = 1;
        $data['rrow'] = $rrow;
        $data['trrow'] = $trrow;
        $data['asset_list'] = $asset_list;
        $data['asset_val'] = $asset_val;
        $data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));

        $this->load->view('order/closed_order',$data);
    }

    public function deactivate_attached_order_rec()
    {
        $order_doc_id = validate_number($this->input->post('asset_doc_id',TRUE));
        $update_data = array('status'=>2);
        $update_where = array('order_doc_id'=>$order_doc_id);
        $res = $this->Common_model->update_data('order_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function activate_attached_order_rec()
    {
        $order_doc_id = validate_number($this->input->post('asset_doc_id',TRUE));
        $update_data = array('status'=>1);
        $update_where = array('order_doc_id'=>$order_doc_id);
        $res = $this->Common_model->update_data('order_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }
}