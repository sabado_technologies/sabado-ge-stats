<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
 * created by Srilekha on 28th July 2017 04:33PM
*/
class Document_type extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Document_type_m');
	}
    //Document Type View..
	public function document_type()
    {
        
        $data['nestedView']['heading']="Manage Document Type";
		$data['nestedView']['cur_page'] = 'document_type';
		$data['nestedView']['parent_page'] = 'document_type';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Manage Document Type';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Document Type','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchdocument', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'name'   => validate_string($this->input->post('doc_name', TRUE)),
                               'workflow_type' => validate_number($this->input->post('workflow_type',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                              'name'   => $this->session->userdata('name'),
                              'workflow_type' => $this->session->userdata('workflow_type')
                              );
            }
            else {
                $searchParams=array(
                                    'name'   => '',
                                    'workflow_type' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'document_type/';
        # Total Records
        $config['total_rows'] = $this->Document_type_m->documenttype_total_num_rows($searchParams); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['document_results'] = $this->Document_type_m->documenttype_results($current_offset, $config['per_page'], $searchParams);
        
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('document_type/document_type_view',$data);

    }
    //add Document Type..
    public function add_document_type()
    {
        
        $data['nestedView']['heading']="Add New Document Type";
		$data['nestedView']['cur_page'] = 'check_document_type';
		$data['nestedView']['parent_page'] = 'document_type';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/document_type.js"></script>';
		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Add New Document Type';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Document Type','class'=>'','url'=>SITE_URL.'document_type');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Document Type','class'=>'active','url'=>'');

        # Additional data   
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_document_type';
        $data['displayResults'] = 0;
        $this->load->view('document_type/document_type_view',$data);
    }
    //insert Document Type..
    public function insert_document_type()    
    {
        #page authentication
        $task_access = page_access_check('document_type');
        $name = validate_string($this->input->post('name', TRUE)); 
        $document_type_id=0;
        $name_available = $this->Document_type_m->is_documenttypeExist($name,$document_type_id);
        if($name_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                                        <strong>Error!</strong> Document Type: '.$name.' already exist! please check.
                                     </div>'); 
            redirect(SITE_URL.'document_type'); exit();
        }

        $data = array(
                'name'          => $name,  
                'workflow_type' => validate_number($this->input->post('workflow_type',TRUE)),                  
                'created_by'    => $this->session->userdata('sso_id'),
                'status'        => 1,
                'created_time'  => date('Y-m-d H:i:s')
                      );
        $this->db->trans_begin();
        $this->Common_model->insert_data('document_type',$data);
        if($this->db->trans_status()===FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                                        <strong>Error!</strong>Something Went Wrong! Please check.
                                     </div>');
        }
        else
        {
            $this->db->trans_commit(); 
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Success!</strong> Document Type has been Added successfully!
                                     </div>');   
        }

        redirect(SITE_URL.'document_type');  
    }
    //edit Document Type..
     public function edit_document_type()
    {
        
        $document_type_id=@storm_decode($this->uri->segment(2));
        if($document_type_id=='')
        {
            redirect(SITE_URL.'document_type');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Document Type";
        $data['nestedView']['cur_page'] = 'check_document_type';
        $data['nestedView']['parent_page'] = 'document_type';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/document_type.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Edit Document Type';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Document Type','class'=>'','url'=>SITE_URL.'document_type');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Document Type','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_document_type';
        $data['display_results'] = 0;

        $data['row']=$this->Common_model->get_data_row('document_type',array('document_type_id'=>$document_type_id));
        
       $this->load->view('document_type/document_type_view',$data);;
    }
    //update Document Type...
    public function update_document_type()
    {
        #page authentication
        $task_access = page_access_check('document_type');
        $document_type_id=validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($document_type_id==''){
            redirect(SITE_URL.'document_type');
            exit;
        }
        $name = validate_string($this->input->post('name', TRUE)); 
        $name_available = $this->Document_type_m->is_documenttypeExist($name,$document_type_id);
        if($name_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                                        <strong>Error!</strong> Document Type: '.$name.' already exist! please check.
                                     </div>'); 
            redirect(SITE_URL.'document_type'); exit();
        }
        $data = array(
                'name'          => $name,   
                'workflow_type' => validate_number($this->input->post('workflow_type',TRUE)),                  
                'modified_by'   =>$this->session->userdata('sso_id'),
                'modified_time' =>date('Y-m-d H:i:s')
                      );
        $where = array('document_type_id'=>$document_type_id);
        $this->db->trans_begin();
        $this->Common_model->update_data('document_type',$data,$where);
        if($this->db->trans_status()===FALSE)
        {
            $this->db->trans_rollback(); 
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                                        <strong>Error!</strong>Something Went Wrong! Please check.
                                     </div>');            
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Success!</strong> Document Type has been updated successfully!
                                     </div>');
        }

        redirect(SITE_URL.'document_type');
    }
    //deactivate Document Type
    public function deactivate_document_type($encoded_id)
    {
       #page authentication
        $task_access = page_access_check('document_type');
        $document_type_id=@storm_decode($encoded_id);
        if($document_type_id==''){
            redirect(SITE_URL.'document_type');
            exit;
        }
        $where = array('document_type_id' => $document_type_id);
        //deactivating Document Type
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('document_type',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Success!</strong> Document type has been deactivated successfully!
                                     </div>');
        redirect(SITE_URL.'document_type');

    }
    //activate Document Type
    public function activate_document_type($encoded_id)
    {
       #page authentication
        $task_access = page_access_check('document_type');
        $document_type_id=@storm_decode($encoded_id);
        if($document_type_id==''){
            redirect(SITE_URL.'document_type');
            exit;
        }
        $where = array('document_type_id' => $document_type_id);
        //activating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('document_type',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Success!</strong> Warehouse has been Activated successfully!
                                     </div>');
        redirect(SITE_URL.'document_type');
    }
    // Document Type Uniqueness
    public function is_documenttypeExist()
    {

        $name = validate_string($this->input->post('name',TRUE));
        $document_type_id = validate_number($this->input->post('document_type_id',TRUE));
        $result = $this->Document_type_m->is_documenttypeExist($name,$document_type_id);
        echo $result;
    }
}