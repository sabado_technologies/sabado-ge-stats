<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Calibration_report extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
	    $this->load->model('Calibration_report_m');
	}

	public function calibration_report()
	{
		$data['nestedView']['heading']="Calibration Report";
		$data['nestedView']['cur_page'] = 'calibration_report';
		$data['nestedView']['parent_page'] = 'calibration_report';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration_report.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Calibration Report';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Report','class'=>'active','url'=>'');

		# Search Functionality
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {  
            $cr_raised_date = validate_string($this->input->post('cr_raised_date',TRUE));
            if($cr_raised_date!='') { $cr_raised_date = format_date($cr_raised_date); }
            else { $cr_raised_date = '';}
        	$searchParams=array(
                'tool_no'        	 => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'      	 => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number'   	 => validate_string($this->input->post('asset_number',TRUE)),
                'calibration_status' => validate_number($this->input->post('calibration_status',TRUE)),
                'cr_status'			 => validate_number($this->input->post('cr_status',TRUE)),
                'zone_id'            => validate_number($this->input->post('zone_id',TRUE)),
                'modality_id'        => validate_number($this->input->post('modality_id',TRUE)),
                'warehouse'          => validate_number($this->input->post('warehouse',TRUE)),
                'cr_raised_date'     => $cr_raised_date,
                'cr_number'          => validate_string($this->input->post('cr_number',TRUE)),
                'country_id'         => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'          => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'tool_no'            => $this->session->userdata('tool_no'),
                    'tool_desc'     	 => $this->session->userdata('tool_desc'),
                    'asset_number'  	 => $this->session->userdata('asset_number'),
                    'calibration_status' => $this->session->userdata('calibration_status'),
                    'cr_status'			 => $this->session->userdata('cr_status'),
                    'zone_id'            => $this->session->userdata('zone_id'),
                    'modality_id'        => $this->session->userdata('modality_id'),
                    'warehouse'          => $this->session->userdata('warehouse'),
                    'cr_raised_date'     => $this->session->userdata('cr_raised_date'),
                    'cr_number'          => $this->session->userdata('cr_number'),
                    'country_id'         => $this->session->userdata('country_id'),
                    'serial_no'          => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'tool_no'        	 => '',
                    'tool_desc'      	 => '',
                    'asset_number'   	 => '',
                    'calibration_status' => '',
                    'cr_status'			 => '',
                    'zone_id'            => '',
                    'modality_id'        => '',
                    'cr_number'          => '',
                    'cr_raised_date'     => '',
                    'country_id'         => '',
                    'warehouse'          => '',
                    'serial_no'          => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;
        if($searchParams['zone_id'] != '')
        {
            $data['zwhList'] = get_wh_by_zone_tools_inventory($searchParams['zone_id'],$task_access);
        }
        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'calibration_report/';
        # Total Records
        $config['total_rows'] = $this->Calibration_report_m->calibration_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Calibration_report_m->calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);
        # Additional data
        $data['calibration_results'] = $calibration_results;
        $data['displayResults'] = 1;
        $data['calibration_status'] = calibration_status();
        $data['current_stage'] = $this->Common_model->get_data('current_stage',array('workflow_type'=>3));
        $data['modalityList'] = $this->Common_model->get_data('modality',array('status'=>1));
        $data['whList'] = $this->Calibration_report_m->get_wh_list($task_access);
        $data['asset_status'] = $this->Common_model->get_data('asset_status',array());
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['zoneList'] = $this->Calibration_report_m->get_zone_by_country($task_access);
        $data['task_access'] = $task_access;

        $this->load->view('calibration_report/calibration_report_view',$data);
    }

    public function calibration_download()
    {
        $task_access = page_access_check('calibration_report');
        if(validate_string($this->input->post('download_calibration',TRUE))!='') 
        {
            $cr_raised_date = validate_string($this->input->post('cr_raised_date',TRUE));
            if($cr_raised_date!='') { $cr_raised_date = format_date($cr_raised_date); }
            else { $cr_raised_date = '';}
            $searchParams=array(
                'tool_no'            => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'          => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number'       => validate_string($this->input->post('asset_number',TRUE)),
                'calibration_status' => validate_number($this->input->post('calibration_status',TRUE)),
                'cr_status'          => validate_number($this->input->post('cr_status',TRUE)),
                'zone_id'            => validate_number($this->input->post('zone_id',TRUE)),
                'modality_id'        => validate_number($this->input->post('modality_id',TRUE)),
                'warehouse'          => validate_number($this->input->post('warehouse',TRUE)),
                'cr_raised_date'     => $cr_raised_date,
                'cr_number'          => validate_string($this->input->post('cr_number',TRUE)),
                'country_id'         => validate_number($this->input->post('country_id',TRUE)),
                'serial_no'          => validate_string($this->input->post('serial_no',TRUE))
            );
            $calibration = $this->Calibration_report_m->calibration_report_downloads($searchParams,$task_access);
            $header = '';
            $data ='';
            $titles = array('S.No','CR Number','Tool Number','Tool Description','Asset Number','Serial Number','Modality','Calibration Status','Calibration Due Date','Remaining Day(s)','CR Status','Tool Availability','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            $j=1;
            if(count($calibration)>0)
            {
                foreach($calibration as $row)
                {
                    $current_position = get_asset_position($row['asset_id']);

                    if($row['asset_main_status']==12)
                    {
                        $calibration_status = "Out Of Calibration";
                    }
                    else
                    {
                        if($row['current_stage_id']<17)
                        {
                            $calibration_status = "In Calibration";
                        }
                        else if($row['current_stage_id'] == 17)
                        {
                            $calibration_status = "Calibrated";
                        }
                        else if($row['current_stage_id'] == 18)
                        {
                            $calibration_status = "Scrapped";
                        }
                        else if($row['current_stage_id'] == 19)
                        {
                            $calibration_status = "Sent For Recalibration";
                        }
                        else if($row['current_stage_id'] == 27)
                        {
                            $calibration_status = "Out Of Tolerance";
                        }
                        else if($row['current_stage_id'] == 28)
                        {
                            $calibration_status = "Cancelled";
                        }
                    }
                    if($row['current_stage_id']<17)
                    {
                        if($row['days']>=0)
                        {
                            $dayy =  $row['days'];
                        }
                        else
                        {
                            $day = explode("-", $row['days']);
                            $dayy = $day[1].' '.'day(s) exceeded';
                        }
                    }
                    else
                    {
                        $dayy = 'Closed';
                    }
                    $cal_due_date = $row['cal_due_date'];
                    if($cal_due_date!='') { $cal_due_date = full_indian_format($cal_due_date);}
                    else { $cal_due_date = '';}
                    $data.='<tr>';
                    $data.='<td align="center">'.$j.'</td>';
                    $data.='<td align="center">'.$row['rc_number'].'</td>';
                    $data.='<td align="center">'.$row['part_number'].'</td>';
                    $data.='<td align="center">'.$row['part_description'].'</td>';
                    $data.='<td align="center">'.$row['asset_number'].'</td>';
                    $data.='<td align="center">'.$row['serial_number'].'</td>';
                    $data.='<td align="center">'.$row['modality_name'].'</td>';
                    $data.='<td align="center">'.$calibration_status.'</td>';
                    $data.='<td align="center">'.$cal_due_date.'</td>'; 
                    $data.='<td align="center">'.$dayy.'</td>'; 
                    $data.='<td align="center">'.$row['current_stage'].'</td>'; 
                    $data.='<td align="center">'.$current_position.'</td>'; 
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';
                    $j++;
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Results Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Calibration_report_'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

	public function get_cr_status()
	{
		$calibration_status = validate_number($this->input->post('calibration_status',TRUE));
        $qry_data = "";
        $qry_data.="<option value=''>- CR Status -</option>";
        if($calibration_status == "")
        {
            $cr_status=$this->Common_model->get_data('current_stage',array('workflow_type'=>3,'status'=>1));
            foreach($cr_status as $row)
            {
                $qry_data.='<option value="'.$row['current_stage_id'].'">'.$row['name'].'</option>';
            }
        }
		else if($calibration_status==1)//in calibration
		{
            $cr_status=$this->Common_model->get_data('current_stage',array('workflow_type'=>3,'status'=>1,'current_stage_id !='=>21));
            foreach($cr_status as $row)
            {
                $qry_data.='<option value="'.$row['current_stage_id'].'">'.$row['name'].'</option>';
            }
			
		}
		else if($calibration_status==2)//calibrated
		{
            $cr_status=$this->Common_model->get_data_row('current_stage',array('workflow_type'=>3,'status'=>1,'current_stage_id'=>17));

            $qry_data.='<option value="'.$cr_status['current_stage_id'].'">'.$cr_status['name'].'</option>';
		}
        else if($calibration_status==3)//Scrapped
        {
            $cr_status=$this->Common_model->get_data_row('current_stage',array('workflow_type'=>3,'status'=>1,'current_stage_id'=>18));

            $qry_data.='<option value="'.$cr_status['current_stage_id'].'">'.$cr_status['name'].'</option>';
        }
        else if($calibration_status==4)//Sent for Re-calibration
        {
            $cr_status=$this->Common_model->get_data_row('current_stage',array('workflow_type'=>3,'status'=>1,'current_stage_id'=>19));

            $qry_data.='<option value="'.$cr_status['current_stage_id'].'">'.$cr_status['name'].'</option>';
        }
        else if($calibration_status==5)//Out of tolerance
        {
            $cr_status=$this->Common_model->get_data_row('current_stage',array('workflow_type'=>3,'status'=>1,'current_stage_id'=>27));

            $qry_data.='<option value="'.$cr_status['current_stage_id'].'">'.$cr_status['name'].'</option>';
        }
        else if($calibration_status==6)//Out of Ca
        {
            $cr_status=$this->Common_model->get_data('current_stage',array('workflow_type'=>3,'status'=>1));
            foreach($cr_status as $row)
            {
                if($row['current_stage_id']<17)
                {
                    $qry_data.='<option value="'.$row['current_stage_id'].'">'.$row['name'].'</option>';
                }
            }
        }
        $data = array('result1'=>$qry_data);
        echo json_encode($data);
	}

    public function egm_calibration_report()
    {
        $data['nestedView']['heading']="EGM Calibration Report";
        $data['nestedView']['cur_page'] = 'egm_calibration_report';
        $data['nestedView']['parent_page'] = 'egm_calibration_report';

        #page authentication
        $task_access = page_access_check('calibration_report');

        # include files
        $data['nestedView']['js_includes'] = array();
        //$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration_report.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'EGM Calibration Report';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'EGM Calibration Report','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {  
            $cr_raised_date = validate_string($this->input->post('cr_raised_date',TRUE));
            if($cr_raised_date!='') { $cr_raised_date = format_date($cr_raised_date); }
            else { $cr_raised_date = '';}
            $searchParams=array(
                'tool_no'        => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'      => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number'   => validate_string($this->input->post('asset_number',TRUE)),
                'cr_status'      => validate_number($this->input->post('cr_status',TRUE)),
                'modality_id'    => validate_number($this->input->post('modality_id',TRUE)),
                'warehouse'      => validate_number($this->input->post('warehouse',TRUE)),
                'cr_raised_date' => $cr_raised_date,
                'cr_number'      => validate_string($this->input->post('cr_number',TRUE)),
                'country_id'     => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'      => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'tool_no'        => $this->session->userdata('tool_no'),
                    'tool_desc'      => $this->session->userdata('tool_desc'),
                    'asset_number'   => $this->session->userdata('asset_number'),
                    'cr_status'      => $this->session->userdata('cr_status'),
                    'modality_id'    => $this->session->userdata('modality_id'),
                    'warehouse'      => $this->session->userdata('warehouse'),
                    'cr_raised_date' => $this->session->userdata('cr_raised_date'),
                    'cr_number'      => $this->session->userdata('cr_number'),
                    'country_id'     => $this->session->userdata('country_id'),
                    'serial_no'      => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'tool_no'        => '',
                    'tool_desc'      => '',
                    'asset_number'   => '',
                    'cr_status'      => '',
                    'modality_id'    => '',
                    'cr_number'      => '',
                    'cr_raised_date' => '',
                    'country_id'     => '',
                    'warehouse'      => '',
                    'serial_no'      => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'egm_calibration_report/';
        # Total Records
        $config['total_rows'] = $this->Calibration_report_m->egm_calibration_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['calibration_results'] = $this->Calibration_report_m->egm_calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['displayResults'] = 1;
        $data['current_stage'] = $this->Common_model->get_data('egm_cal_status',array('status'=>1));
        $data['modalityList'] = $this->Common_model->get_data('modality',array('status'=>1));
        $data['whList'] = $this->Calibration_report_m->get_wh_list($task_access);
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;

        $this->load->view('calibration_report/egm_calibration_report_view',$data);
    }

    public function egm_calibration_download()
    {
        $task_access = page_access_check('calibration_report');
        if(validate_string($this->input->post('download_calibration',TRUE))!='') 
        {
            $cr_raised_date = validate_string($this->input->post('cr_raised_date',TRUE));
            if($cr_raised_date!='') { $cr_raised_date = format_date($cr_raised_date); }
            else { $cr_raised_date = '';}
             $searchParams=array(
                'tool_no'        => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'      => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number'   => validate_string($this->input->post('asset_number',TRUE)),
                'cr_status'      => validate_number($this->input->post('cr_status',TRUE)),
                'modality_id'    => validate_number($this->input->post('modality_id',TRUE)),
                'warehouse'      => validate_number($this->input->post('warehouse',TRUE)),
                'cr_raised_date' => $cr_raised_date,
                'cr_number'      => validate_string($this->input->post('cr_number',TRUE)),
                'country_id'     => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'      => validate_string($this->input->post('serial_no',TRUE))
            );
            $calibration = $this->Calibration_report_m->egm_calibration_report_downloads($searchParams,$task_access);
            $header = '';
            $data ='';
            $titles = array('S.No','CR Number','Tool Number','Tool Description','Asset Number','Serial Number','Modality','Asset Main Status','Calibration Due Date','Remaining Day(s)','CR Status','Tool Availability','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            $j=1;
            if(count($calibration)>0)
            {
                foreach($calibration as $row)
                {
                    $current_position = get_asset_position($row['asset_id']);
                    $calibration_status = $row['asset_main_status'];
                    if($row['cal_status_id']<3)
                    {
                        if($row['days']>=0)
                        {
                            $dayy =  $row['days'];
                        }
                        else
                        {
                            $day = explode("-", $row['days']);
                            $dayy = $day[1].' '.'day(s) exceeded';
                        }
                    }
                    else
                    {
                        $dayy = 'Closed';
                    }
                    $cal_due_date = $row['cal_due_date'];
                    if($cal_due_date!='') { $cal_due_date = full_indian_format($cal_due_date);}
                    else { $cal_due_date = '';}
                    $data.='<tr>';
                    $data.='<td align="center">'.$j.'</td>';
                    $data.='<td align="center">'.$row['cr_number'].'</td>';
                    $data.='<td align="center">'.$row['part_number'].'</td>';
                    $data.='<td align="center">'.$row['part_description'].'</td>';
                    $data.='<td align="center">'.$row['asset_number'].'</td>';
                    $data.='<td align="center">'.$row['serial_number'].'</td>';
                    $data.='<td align="center">'.$row['modality_name'].'</td>';
                    $data.='<td align="center">'.$calibration_status.'</td>';
                    $data.='<td align="center">'.$cal_due_date.'</td>'; 
                    $data.='<td align="center">'.$dayy.'</td>'; 
                    $data.='<td align="center">'.$row['cal_status_name'].'</td>'; 
                    $data.='<td align="center">'.$current_position.'</td>'; 
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';
                    $j++;
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Results Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='EGM_Calibration_report_'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }
}