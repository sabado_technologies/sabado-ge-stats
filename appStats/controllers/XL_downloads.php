<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class XL_downloads extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('XL_downloads_m');
        $this->load->model('Order_m');
        $this->load->model('Pickup_point_m');
        $this->load->model('Stock_transfer_m');
	}
    public function downloadOpenOrders()
    {
        #page Authentication
        $task_access = page_access_check('open_order');
        if(validate_string($this->input->post('downloadOpenOrders',TRUE))==1)
        {
            $searchParams=array(
                'order_number'           => validate_string($this->input->post('order_number', TRUE)),
                'order_delivery_type_id' => validate_number($this->input->post('order_delivery_type_id', TRUE)),
                'request_date'           => validate_string($this->input->post('request_date', TRUE)),
                'return_date'            => validate_string($this->input->post('return_date', TRUE)),
                'search_fe_sso_id'       => validate_string($this->input->post('search_fe_sso_id', TRUE)),
                'current_stage_id'       => validate_number($this->input->post('current_stage_id', TRUE)),
                'ro_country_id'          => validate_number($this->input->post('ro_country_id',TRUE))
            );
            $orderResults = $this->XL_downloads_m->downloadOpenOrders($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('Order Number','Requested By','Order Type','Request Date','Need Date','Return Date',
                'Order Status','Service Type','Siebel SR Number','Country','Type','Ship To Address','Tool Number',
                'Tool Code','Tool Description','Ordered Qty','Avail Qty');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';            
            $data.='<tbody>';
            if(count($orderResults)>0)
            {                
                foreach($orderResults as $row)
                {
                    $orderedTools = $this->Order_m->get_ordered_tools($row['tool_order_id']);
                    $status = getOpenOrderStatus($row);                    
                    if($row['order_delivery_type_id'] == 1)
                    {                
                        $address['system_id'] = $row['system_id'];
                        $address['site_id'] = $row['site_id'];
                        $rest =  $this->Order_m->get_install_basedata($row['system_id']);        
                        $address['c_name'] = $rest['c_name'];
                    }
                    else
                    {
                        if($row['order_delivery_type_id'] == 2)
                        {
                            $address['warehouse'] = getWhCodeAndName($row['wh_id']);                    
                        }
                    }
                    $address['address1'] = $row['address1'];
                    $address['address2'] = $row['address2'];
                    $address['address3'] = $row['address3'];
                    $address['address4'] = $row['address4'];
                    $address['pin_code'] = $row['pin_code'];
                    $address_string = implode(",",$address);
                    $type = ($row['fe_check'] == 0)?'Request For Tool':'Transfer From FE';

                    $data.='<tr>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['order_number'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['sso'].'</td>';  
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['order_type'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['request_date']).'</td>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['need_date']).'</td>';                    
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['return_date']).'</td>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$status.'</td>';                    
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['service_type'].'</td>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['siebel_sr_number'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['country_name'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$type.'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" align="center">'.$address_string.'</td>';
                    $i = 1;
                    foreach ($orderedTools as $key => $value) {
                        $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_number'].'</td>'; 
                        $data.='<td style="vertical-align: middle; text-align: center;">'.'`'.$value['tool_code'].'</td>';  
                        $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_description'].'</td>'; 
                        $data.='<td style="vertical-align: middle; text-align: center;">'.$value['quantity'].'</td>';
                        $data.='<td style="vertical-align: middle; text-align: center;">'.$value['available_quantity'].'</td>';                    
                        $data.='</tr>';          
                    } 
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='OpenOrdersDownloads'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function downloadClosedOrders()
    {
        #page Authentication
        $task_access = page_access_check('closed_order');
        if(validate_string($this->input->post('downloadClosedOrders',TRUE))==1)
        {
            $searchParams=array(
                'order_number'             => validate_string($this->input->post('order_number', TRUE)),
                'order_delivery_type_id'   => validate_number($this->input->post('order_delivery_type_id', TRUE)),
                'deploy_date'              => validate_string($this->input->post('deploy_date', TRUE)),
                'return_date'              => validate_string($this->input->post('return_date', TRUE)),
                'closed_fe_sso_id'         => validate_string($this->input->post('closed_fe_sso_id', TRUE)),
                'status'                   => validate_number($this->input->post('status', TRUE)),
                'co_country_id'            => validate_number($this->input->post('co_country_id', TRUE))
            );
            $orderResults = $this->XL_downloads_m->downloadClosedOrders($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('Order Number','Requested By','Order Type','Request Date','Need Date','Return Date','Shipped Date','Closed Date',
                'Order Status','Service Type','Siebel SR Number','Country','Type','Ship To Address','Tool Number',
                'Tool Code','Tool Description','Ordered Qty','Avail Qty',
                'Return Number','Pickup Point','Received Date','Return Type' ,'From Address','To Address',
                'Courier name','courier type','Contact Person','Cocket/Awb number','Vehicle number',
                'Phone number','Tool Number','Tool Descrption','Tool Code','Asset Number','Status'

            );
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            
            $data.='<tbody>';
            if(count($orderResults)>0)
            {                
                foreach($orderResults as $row)
                {
                    $orderedTools = $this->Order_m->get_ordered_tools($row['tool_order_id']);
                    if($searchParams['status'] == 10) // successfully closed returns
                    {
                        $returns = $this->Order_m->getReturnsForOrder($row['tool_order_id']);
                        $noOfReturns = count($returns);
                        $returnAssetsCount = 0;
                        $returnsArr = array();
                        if($noOfReturns>0)
                        {
                            foreach ($returns as $key => $returnRow)
                            {
                                $return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$returnRow['return_order_id']));
                                $return_order_id = $returnRow['return_order_id'];
                                if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
                                {
                                    $trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['ro_to_wh_id']));
                                    $trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
                                }
                                else 
                                {
                                    $trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
                                    $sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
                                    $sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
                                    $trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];
                                }
                                $trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');
                                $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
                                $rrow = $this->Order_m->get_fe_closed_return_details($return_order_id,$rto_id);
                                $asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
                                foreach ($asset_list as $key => $value) 
                                {
                                    $components = $this->Pickup_point_m->get_asset_components($value['asset_id'],$rrow['order_status_id']);
                                    $asset_list[$key]['components'] = $components;
                                }
                                #to select 9th stage value
                                $os_id = $rrow['order_status_id'];
                                $order_asset = $this->Common_model->get_data('order_asset_history',array('order_status_id'=>$os_id));
                                $asset_val = array();
                                foreach ($order_asset as $oadrow) 
                                {
                                    $oahealth = $this->Common_model->get_data('order_asset_health',array('oah_id'=>$oadrow['oah_id']));
                                    $assetid = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$oadrow['ordered_asset_id']),'asset_id');
                                    foreach ($oahealth as $key => $value) 
                                    {
                                        $asset_val[$assetid]['asset_status'] = $oadrow['status'];
                                        $asset_val[$assetid][$value['part_id']]['asset_condition_id'] = $value['asset_condition_id'];
                                        $asset_val[$assetid][$value['part_id']]['remarks'] = $value['remarks'];
                                    }
                                }
                                // preparing array of returns
                                $returnsArr[$returnRow['return_order_id']]['returnRow'] = $returnRow;
                                $returnsArr[$returnRow['return_order_id']]['rrow'] = $rrow;
                                $returnsArr[$returnRow['return_order_id']]['trrow'] = $trrow;
                                $returnsArr[$returnRow['return_order_id']]['asset_list'] = $asset_list;
                                $returnsArr[$returnRow['return_order_id']]['asset_val'] = $asset_val;                                                                
                                $returnAssetsCount += count($asset_list);
                            } // end of return rows foreach
                            $firstRowSpan = $returnAssetsCount;
                        }
                        else
                        {
                            $firstRowSpan =count($orderedTools);
                        }                            
                    }
                    else // cancelled returns
                    {
                         $firstRowSpan =count($orderedTools);
                    }                    
                    // Infor
                    if($row['current_stage_id'] == 4)
                    {
                        if(checkSTorderExist($row['tool_order_id'])==1)
                        {
                            $status = 'In Stock Transfer';
                        }
                        else
                        {
                            $status = getReasonForOrderAtAdmin($row['tool_order_id']);
                        }
                    }
                    else
                    {
                        $status = $row['name'];
                    }                        
                    if($row['order_delivery_type_id'] == 1)
                    {                
                        $address['system_id'] = $row['system_id'];
                        $address['site_id'] = $row['site_id'];
                        $rest =  $this->Order_m->get_install_basedata($row['system_id']);        
                        $address['c_name'] = $rest['c_name'];
                        
                    }
                    else
                    {
                        if($row['order_delivery_type_id'] == 2)
                        {
                            $address['warehouse'] = getWhCodeAndName($row['wh_id']);                    
                        }
                    }
                    $address['address1'] = $row['address1'];
                    $address['address2'] = $row['address2'];
                    $address['address3'] = $row['address3'];
                    $address['address4'] = $row['address4'];
                    $address['pin_code'] = $row['pin_code'];
                    $address_string = implode(",",$address);
                    $type = ($row['fe_check'] == 0)?'Request For Tool':'Transfer From FE';
                    $shipped_date = getXLFinalShipDate($row);
                    $request_date =($row['request_date']=='')?"NA": full_indian_format($row['request_date']);
                    $need_date = ($row['need_date']=='')?"NA":full_indian_format($row['need_date']);
                    $return_date = ($row['return_date']=='')?"NA":full_indian_format($row['return_date']);
                    $closed_date = ($row['closed_date']=='')?"NA":full_indian_format($row['closed_date']);
                    $data.='<tr>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['order_number'].'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['sso'].'</td>';  
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['order_type'].'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.$request_date.'</td>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.$need_date.'</td>';                    
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.$return_date.'</td>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.$shipped_date.'</td>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.$closed_date.'</td>';                    
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['name'].'</td>';                    
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['service_type'].'</td>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['siebel_sr_number'].'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['country_name'].'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$type.'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$address_string.'</td>';
                        
                    $i = 0;
                    if(@$noOfReturns>0)
                    {
                        foreach ($returnsArr as $rerunArrayKey => $returnArrValue)
                        {
                            $tool_code ='';
                            if(@$orderedTools[$i]['tool_code']!='')
                                $tool_code = '`'.@$orderedTools[$i]['tool_code'];                            
                            $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_number'].'</td>'; 
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code.'</td>';  
                            $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_description'].'</td>'; 
                            $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['quantity'].'</td>';
                            $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['available_quantity'].'</td>';                    
                            $returnRow = $returnArrValue['returnRow'];
                            $rrow = $returnArrValue['rrow'];
                            $trrow = $returnArrValue['trrow'];
                            $asset_list = $returnArrValue['asset_list'];
                            $asset_val = $returnArrValue['asset_val'];
                            if(@$rrow['courier_type']==1)
                            {
                                $courier_type = "By Courier";
                            }
                            else if(@$rrow['courier_type']==2)
                            {
                                $courier_type = "By Hand";
                            }
                            else if(@$rrow['courier_type']==3)
                            {
                                $courier_type = "By Determined";
                            }
                            // Return Info
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.$returnRow['return_number'].'</td>'; 
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.@$returnRow['location_name'].'</td>';  
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($returnRow['received_date']).'</td>'; 
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.$returnRow['return_type_name'].'</td>';
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.$rrow['address1'].','.$rrow['address2'].','.$rrow['address3'].','.$rrow['address4'].','.$rrow['zip_code'].'</td>';                    
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.$trrow['address1'].','.$trrow['address2'].','.$trrow['address3'].','.$trrow['address4'].','.$trrow['pin_code'].'</td>';                    
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.@$returnRow['courier_name'].'</td>';
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.@$courier_type.'</td>';
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.@$rrow['contact_person'].'</td>';
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.@$rrow['courier_number'].'</td>';
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.@$rrow['vehicle_number'].'</td>';
                            $data.='<td rowspan="'.count($asset_list).'" style="vertical-align: middle; text-align: center;">'.@$rrow['phone_number'].'</td>';
                            $j = 1;
                            foreach ($asset_list as $key => $asset_list_row)
                            {
                                if($j!=1)
                                {
                                    $tool_code ='';
                                    if(@$orderedTools[$i]['tool_code']!='')
                                        $tool_code = '`'.@$orderedTools[$i]['tool_code']; 

                                    $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_number'].'</td>'; 
                                    $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code.'</td>';  
                                    $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_description'].'</td>'; 
                                    $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['quantity'].'</td>';
                                    $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['available_quantity'].'</td>';                    
                                }
                                if(@$asset_val[@$asset_list_row['asset_id']]['asset_status'] == 3)    $asset_status= "Not Received"; 
                                else $asset_status="Received";
                                $tool_code='';
                                if($asset_list_row['tool_code']!='')
                                    $tool_code = '`'.$asset_list_row['tool_code'];
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$asset_list_row['part_number'].'</td>';                       
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$asset_list_row['part_description'].'</td>';                       
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$tool_code.'</td>';                       
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$asset_list_row['asset_number'].'</td>';                       
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$asset_status.'</td>';                       
                                $data.='</tr>';
                                $i++; 
                                $j++;
                            }                                                                         
                        }                      
                    }
                    else
                    {
                        $ci = 0;
                        foreach ($orderedTools as $key => $value) {
                            $tool_code='';
                            if($value['tool_code']!='')
                                $tool_code = '`'.$value['tool_code'];
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_number'].'</td>'; 
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code.'</td>';  
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_description'].'</td>'; 
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['quantity'].'</td>';
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['available_quantity'].'</td>';                    
                            if($ci==0)
                                $data.='<td colspan="17" rowspan="'.$firstRowSpan.'" style="vertical-align: middle; text-align: center;">No Returns Found For This Order</td>';                    
                            $data.='</tr>';      
                            $ci++;
                        }  
                    }
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='ClosedOrdersDownloads'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_user()
    {
        $task_access = page_access_check('manage_user');
        if(validate_string($this->input->post('download_user',TRUE))==1)
        {
            $searchParams=array(
                'ssonum'         => validate_string($this->input->post('sso_num',TRUE)),
                'username'       => validate_string($this->input->post('user_name',TRUE)),
                'branchnum'      => validate_string($this->input->post('branch_num',TRUE)),
                'designationnum' => validate_string($this->input->post('designation_num',TRUE)),
                'roleid'         => validate_number($this->input->post('role_id',TRUE)),
                'whid'           => validate_number($this->input->post('wh_id',TRUE)),
                'fe_position'    => validate_string($this->input->post('fe_position',TRUE)),
                'country_id'     => validate_number(@$this->input->post('country_id',TRUE))
           );

            $t_user = $this->XL_downloads_m->download_user($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','SSO ID','Name','Reporting Manager','Managed By','Email',
                'Mobile Number','Role',
                'Designation','Warehouse',
                'Branch','Address1','Address2','FE Position','Location (For Zonal/National Users)','Modality Code','Country Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_user)>0)
            { 
                $sno = 1;            
                foreach($t_user as $row)
                {

                    $managedby ='';  
                    if(@$row['managed_by']!='')
                    {
                        $managedby = $row['managed_by'];
                    }
                    else
                    {
                        $managedby = 'NA';
                    }

                     $modality1 ='';  
                    if(@$row['modality_list']!='')
                    {
                        $modality1 = $row['modality_list'];
                    }
                    else
                    {
                        $modality1 = 'NA';
                    }
                    if($row['wh_arr']!='')
                    {
                        $warehouse = $row['wh_arr'];
                    }
                    else
                    {
                        $warehouse = $row['wh'];
                    }

                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['sso_id'].'</td>';
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='<td align="center">'.$row['rm_id'].'</td>';
                    $data.='<td align="center">'.$managedby.'</td>';
                    $data.='<td align="center">'.$row['email'].'</td>';
                    $data.='<td align="center">'.$row['mobile_no'].'</td>';
                    $data.='<td align="center">'.$row['role'].'</td>';
                    $data.='<td align="center">'.$row['designation_name'].'</td>'; 
                    $data.='<td align="center">'.$warehouse.'</td>'; 
                    $data.='<td align="center">'.$row['branch_name'].'</td>';
                    $data.='<td align="center">'.$row['address1'].'</td>';
                    $data.='<td align="center">'.$row['address2'].'</td>';
                    $data.='<td align="center">'.$row['fe_position'].'</td>';
                    $data.='<td align="center">'.$row['location_name'].'</td>';
                    $data.='<td align="center">'.$modality1.'</td>';
                    $data.='<td align="center">'.$row['country'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='User'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_tool()
    {
        $task_access = page_access_check('manage_tool');
        $tool_id=validate_string($this->input->post('part_number',TRUE)); 
        if(validate_string($this->input->post('download_tool',TRUE))==1)
        {
            $searchParams=array(
            'part_number'       => validate_string($this->input->post('part_number', TRUE)),
            'part_description'  => validate_string($this->input->post('part_description', TRUE)),
            'tool_code'         => validate_string($this->input->post('tool_code', TRUE)),
            'part_level_id'     => validate_number($this->input->post('part_level_id', TRUE)),
            'country_id'        => validate_number(@$this->input->post('country_id',TRUE))
           );
            $new_arr =array();
            $t_tool = $this->XL_downloads_m->download_tool($searchParams,$task_access);
            foreach ($t_tool as $key => $value) 
            {
                $new_arr[] = $value;
                $childs = $this->XL_downloads_m->get_tool_parts($value['tool_id']);
                if(count($childs)>0)
                {
                    foreach ($childs as $key => $value2) {
                        $value3 = $value2;
                        $value3["tool_id"] = $value['tool_id'];
                        $value3['sub_tool_id'] = $value2['tool_id'];
                        $new_arr[] = $value3;
                    }
                }
            } 
            $t_tool = $new_arr;
            $header = '';
            $data ='';
            $titles = array('Asset Level',' Modality Code','Tool Classification','Kit','Asset Type',
            'Tool Part Number',
            'Description','Tool Code','Part Level','Quantity','Length','Breadth','Height',
            'Weight','Calibration Required','Cal Supplier Code',
            'Supplier Code','Manufacturer','Equipment Model','HSN Code','Model','GST%',
            'Country','Comment');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_tool)>0)
            { 
                $p = 0;     
                $first_tool_id = $t_tool[0]['tool_id']; 
                foreach($t_tool as $row)
                {
                    $next_tool_id = $row['tool_id'];
                    $data.='<tr>';
                    if($p== 0) 
                    { 
                        $data.='<td align="center">'.$row['asset_level_name'].'</td>'; 
                    }
                    else
                    {
                        if($first_tool_id == $next_tool_id)
                        {
                            $data.='<td align="center"></td>';                                               
                        }
                        else
                        {
                            $data.='<td align="center">'.$row['asset_level_name'].'</td>';                                               
                        }
                    }
                    $qty ='';  
                    if(@$row['quantity']!='')
                    {
                        $qty = $row['quantity'];
                    }
                    else
                    {
                        $qty = 'NA';
                    }
                    $modality1 ='';  
                    if(@$row['modality']!='')
                    {
                        $modality1 = $row['modality'];
                    }
                    else
                    {
                        $modality1 = 'NA';
                    }
                    $eq ='';  
                    if(@$row['eq_model']!='')
                    {
                        $eq = $row['eq_model'];
                    }
                    else
                    {
                        $eq = 'NA';
                    }
                    $cal_type ='';  
                    if(@$row['cal_type_id']='1')
                    {
                        $cal_type = 'Y';
                    }
                    else
                    {
                        $cal_type = 'N';
                    }

                    $kit1 ='';  
                    if($row['kit']='1')
                    {
                        $kit1 = 'Yes';
                    }
                    else
                    {
                        $kit1 = 'No';
                    }
                    $asset_type1 ='';  
                    if($row['asset_type']='1')
                    {
                        $asset_type1 = 'Assembly';
                    }
                    else
                    {
                        $asset_type1 = 'Component';
                    }
                    $data.='<td align="center">'.$modality1.'</td>';                
                    $data.='<td align="center">'.$row['tool_type_name'].'</td>';                   
                    $data.='<td align="center">'.$kit1.'</td>';                   
                    $data.='<td align="center">'.$asset_type1.'</td>';
                    $data.='<td align="center">'.$row['part_number'].'</td>';                
                    $data.='<td align="center">'.$row['part_description'].'</td>';                   
                    $data.='<td align="center">'.$row['tool_code'].'</td>';                   
                    $data.='<td align="center">'.$row['tool_level_name'].'</td>';
                    $data.='<td align="center">'.$qty.'</td>';               
                    $data.='<td align="center">'.$row['length'].'</td>';                   
                    $data.='<td align="center">'.$row['breadth'].'</td>';                   
                    $data.='<td align="center">'.$row['height'].'</td>';   
                    $data.='<td align="center">'.$row['weight'].'</td>';
                    $data.='<td align="center">'.$cal_type.'</td>';                
                    $data.='<td align="center">'.$row['cal_supplier_id'].'</td>';                   
                    $data.='<td align="center">'.$row['supplier'].'</td>';                   
                    $data.='<td align="center">'.$row['manufacturer'].'</td>';                   
                    $data.='<td align="center">'.$eq.'</td>';                
                    $data.='<td align="center">'.$row['hsn_code'].'</td>';
                    $data.='<td align="center">'.$row['model'].'</td>';
                    $data.='<td align="center">'.$row['gst_percent'].'</td>'; 
                    $data.='<td align="center">'.$row['country'].'</td>';               
                    $data.='<td align="center">'.$row['remarks'].'</td>';
                    $data.='</tr>';
                    $p++;
                   $first_tool_id = $next_tool_id;
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Tool'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

     public function download_install_base()
    {
        $task_access = page_access_check('install_base');
        if(validate_string($this->input->post('download_install_base',TRUE))==1)
        {
            $searchParams=array(
            'customer_name'   => validate_string($this->input->post('customer_name', TRUE)),
            'site_id'         => validate_string($this->input->post('site_id',TRUE)),
            'system_id'       => validate_string($this->input->post('system_id',TRUE)),
            'customer_number' => validate_string($this->input->post('customer_number',TRUE)),
            'asset_number'    => validate_string($this->input->post('asset_number',TRUE)),
            'status'          => validate_string($this->input->post('status',TRUE)),
            'country_id'      => validate_number($this->input->post('country_id'))
           );

            $t_ib = $this->XL_downloads_m->download_install_base($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Asset#','System ID','Modality','Product Description','Model Type',
            'Customer#','Customer Name','Site ID','Address1','Address2','Address3','Address4',
            'Zip Code','City','Service Region','Country Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_ib)>0)
            { 
                $sno = 1;            
                foreach($t_ib as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['asset_number'].'</td>';                   
                    $data.='<td align="center">'.$row['system_id'].'</td>';                   
                    $data.='<td align="center">'.$row['modality_name'].'</td>';                   
                    $data.='<td align="center">'.$row['product_description'].'</td>'; 
                    $data.='<td align="center">'.$row['model_type'].'</td>';                   
                    $data.='<td align="center">'.$row['customer_number'].'</td>';                   
                    $data.='<td align="center">'.$row['name'].'</td>';                   
                    $data.='<td align="center">'.$row['site_id'].'</td>'; 
                    $data.='<td align="center">'.$row['address1'].'</td>';                   
                    $data.='<td align="center">'.$row['address2'].'</td>';                   
                    $data.='<td align="center">'.$row['address3'].'</td>';                   
                    $data.='<td align="center">'.$row['address4'].'</td>'; 
                    $data.='<td align="center">'.$row['zip_code'].'</td>';                   
                    $data.='<td align="center">'.$row['address3'].'</td>';                   
                    $data.='<td align="center">'.$row['service_region'].'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>'; 
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Install Base'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_install_base_xlsx()
    {
        $task_access = page_access_check('install_base');
        if(validate_string($this->input->post('download_install_base',TRUE))==1)
        {
            $searchParams=array(
            'customer_name'   => validate_string($this->input->post('customer_name', TRUE)),
            'site_id'         => validate_string($this->input->post('site_id',TRUE)),
            'system_id'       => validate_string($this->input->post('system_id',TRUE)),
            'customer_number' => validate_string($this->input->post('customer_number',TRUE)),
            'asset_number'    => validate_string($this->input->post('asset_number',TRUE)),
            'status'          => validate_string($this->input->post('status',TRUE)),
            'country_id'      => validate_number($this->input->post('country_id'))
            );

            $ib_data = $this->XL_downloads_m->download_install_base_xlsx($searchParams,$task_access);
            
            include_once("xlsxwriter.class.php");
            ini_set('display_errors', 0);
            ini_set('log_errors', 1);
            error_reporting(E_ALL & ~E_NOTICE);

            $time = date("Y-m-d H:i:s");
            $filename = "Install Base".$time.".xlsx";
            header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');

            $titles = array('S.No.','Asset#','System ID','Modality','Product Description','Model Type','Customer#','Customer Name','Site ID','Address1','Address2','Address3','Address4','Zip Code','City','Service Region','Country Name');

            $header = array(
                'Asset#'         => 'string',
                'System ID'      => 'string',
                'Modality'       => 'string',
                'Product Description' => 'string',
                'Model Type'     => 'string',
                'Customer#'      => 'string',
                'Customer Name'  => 'string',
                'Site ID'        => 'string',
                'Address1'       => 'string',
                'Address2'       => 'string',
                'Address3'       => 'string',
                'Address4'       => 'string',
                'Zip Code'       => 'string',
                'City'           => 'string',
                'Service Region' => 'string',
                'Country Name'   => 'string'
            );
            $writer = new XLSXWriter();
            $writer->writeSheetHeader('Sheet1', $header);
            foreach($ib_data as $row_data)
            {
                $writer->writeSheetRow('Sheet1', $row_data);
            }
            $writer->writeToStdOut();
        }
    }

    public function download_supplier()
    {
        $task_access = page_access_check('manage_supplier');
        if(validate_string($this->input->post('download_supplier',TRUE))==1)
        {
            $searchParams=array(
           'name'       => validate_string($this->input->post('supplier_name', TRUE)),
           'modality_id'=> validate_number($this->input->post('modality_id', TRUE)),
           'calibration'=> validate_number($this->input->post('calibration', TRUE)),
           'sup_type_id'=> validate_number($this->input->post('sup_type_id', TRUE)),
           'country_id' => validate_number(@$this->input->post('country_id',TRUE))
           );

            $t_supplier = $this->XL_downloads_m->download_supplier($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Modality','Supplier Code','Vendor','Contact Person','Contact No ',
                'Email- ID','Calibration Status(Provide - 1, Not Provide - 2)',
                'Transport Type(Local -1,Export -2)','Supplier Type(Supplier -1,Calibration supplier-2)',
                'Address1','Address2','City','State','GST Number','PAN Number','Country Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_supplier)>0)
            { 
                $sno = 1;            
                foreach($t_supplier as $row)
                {

                    $modality ='';  
                    if($row['modality_name']!='')
                    {
                        $modality = $row['modality_name'];
                    }
                    else
                    {
                        $modality = 'NA';
                    }
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$modality.'</td>';
                    $data.='<td align="center">'.$row['supplier_code'].'</td>'; 
                    $data.='<td align="center">'.'NA'.'</td>';
                    $data.='<td align="center">'.$row['contact_person'].'</td>';
                    $data.='<td align="center">'.$row['contact_number'].'</td>';
                    $data.='<td align="center">'.$row['email'].'</td>';
                    $data.='<td align="center">'.$row['calibration'].'</td>';
                    $data.='<td align="center">'.$row['cal_sup_type_id'].'</td>';
                    $data.='<td align="center">'.$row['sup_type_id'].'</td>';
                    $data.='<td align="center">'.$row['address1'].'</td>';
                    $data.='<td align="center">'.$row['address2'].'</td>';
                    $data.='<td align="center">'.$row['address3'].'</td>';
                    $data.='<td align="center">'.$row['address4'].'</td>';
                    $data.='<td align="center">'.$row['gst_number'].'</td>';
                    $data.='<td align="center">'.$row['pan_number'].'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Supplier'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

     public function download_branch()
    {
        $task_access = page_access_check('manage_warehouse');
        if(validate_string($this->input->post('download_branch',TRUE))==1)
        {
            $searchParams=array(
            'name'       => validate_string($this->input->post('branch_name', TRUE)),
            'wh_code'    => validate_string($this->input->post('wh_code', TRUE)),
            'country_id' => validate_number($this->input->post('country_id',TRUE))
           );

            $t_branch = $this->XL_downloads_m->download_branch($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Branch Office Name','Tin Number','CST Number','Managed By','Contact Number',
                'Email','Address1',
                'Address2','Address3',
                'Address4','Pin Code','GST No','PAN No','Area','City','Country Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_branch)>0)
            { 
                $sno = 1;            
                foreach($t_branch as $row)
                {

                    $tin_no ='';  
                    if($row['tin_number']!='')
                    {
                        $tin_no = $row['tin_number'];
                    }
                    else
                    {
                        $tin_no = 'NA';
                    }

                     $cst_no ='';  
                    if($row['cst_number']!='')
                    {
                        $cst_no = $row['cst_number'];
                    }
                    else
                    {
                        $cst_no = 'NA';
                    }

                     $gst_no ='';  
                    if($row['gst_number']!='')
                    {
                        $gst_no = $row['gst_number'];
                    }
                    else
                    {
                        $gst_no = 'NA';
                    }

                    $pan_no ='';  
                    if($row['pan_number']!='')
                    {
                        $pan_no = $row['pan_number'];
                    }
                    else
                    {
                        $pan_no = 'NA';
                    }

                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='<td align="center">'.$tin_no.'</td>';
                    $data.='<td align="center">'.$cst_no.'</td>';
                    $data.='<td align="center">'.$row['managed_by'].'</td>';
                    $data.='<td align="center">'.$row['phone'].'</td>';
                    $data.='<td align="center">'.$row['email'].'</td>';
                    $data.='<td align="center">'.$row['address1'].'</td>';
                    $data.='<td align="center">'.$row['address2'].'</td>';
                    $data.='<td align="center">'.$row['address3'].'</td>';
                    $data.='<td align="center">'.$row['address4'].'</td>'; 
                    $data.='<td align="center">'.$row['pin_code'].'</td>'; 
                    $data.='<td align="center">'.$gst_no.'</td>';
                    $data.='<td align="center">'.$pan_no.'</td>';
                    $data.='<td align="center">'.$row['address3'].'</td>';
                    $data.='<td align="center">'.$row['address4'].'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';                   
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Warehouse'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_branch_office()
    {
        $task_access = page_access_check('manage_branch');
        if(validate_string($this->input->post('download_branch_office',TRUE))==1)
        {
            $searchParams=array(
            'name'       => validate_string($this->input->post('branch_name', TRUE)),
            'country_id' => validate_number(@$this->input->post('country_id',TRUE))
           );

            $t_branch_office = $this->XL_downloads_m->download_branch_office($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Branch Office Name','Tin Number','CST Number','Managed By',
                'Contact Number',
                'Email','Address1',
                'Address2','Address3',
                'Address4','Pin Code','GST No','PAN No','Area','City','Country Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_branch_office)>0)
            { 
                $sno = 1;            
                foreach($t_branch_office as $row)
                {
                    $tin_no ='';  
                    if($row['tin_number']!='')
                    {
                        $tin_no = $row['tin_number'];
                    }
                    else
                    {
                        $tin_no = 'NA';
                    }
                    $cst_no ='';  
                    if($row['cst_number']!='')
                    {
                        $cst_no = $row['cst_number'];
                    }
                    else
                    {
                        $cst_no = 'NA';
                    }

                    $gst_no ='';  
                    if($row['gst_number']!='')
                    {
                        $gst_no = $row['gst_number'];
                    }
                    else
                    {
                        $gst_no = 'NA';
                    }

                    $pan_no ='';  
                    if($row['pan_number']!='')
                    {
                        $pan_no = $row['pan_number'];
                    }
                    else
                    {
                        $pan_no = 'NA';
                    }
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='<td align="center">'.$tin_no.'</td>';
                    $data.='<td align="center">'.$cst_no.'</td>';
                    $data.='<td align="center">'.$row['branch_manager'].'</td>';
                    $data.='<td align="center">'.$row['phone_number'].'</td>';
                    $data.='<td align="center">'.$row['email'].'</td>';
                    $data.='<td align="center">'.$row['address1'].'</td>';
                    $data.='<td align="center">'.$row['address2'].'</td>';
                    $data.='<td align="center">'.$row['address3'].'</td>';
                    $data.='<td align="center">'.$row['address4'].'</td>'; 
                    $data.='<td align="center">'.$row['pin_code'].'</td>'; 
                    $data.='<td align="center">'.$gst_no.'</td>';
                    $data.='<td align="center">'.$pan_no.'</td>';
                    $data.='<td align="center">'.$row['address3'].'</td>';
                    $data.='<td align="center">'.$row['address4'].'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';                   
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Branch Office'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_modality()
    {
        $task_access = page_access_check('modality');
        if(validate_string($this->input->post('download_modality',TRUE))==1)
        {
            $searchParams=array(
            'modality_name' => validate_string($this->input->post('modality_name', TRUE)),
            'modality_code' => validate_string($this->input->post('modality_code', TRUE))
           );

            $t_modality = $this->XL_downloads_m->download_modality($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Modality Code','Modality Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_modality)>0)
            { 
                $sno = 1;            
                foreach($t_modality as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['modality_code'].'</td>';
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Modality'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_eq_model()
    {
        $task_access = page_access_check('manage_equipment_model');
        if(validate_string($this->input->post('download_eq_model',TRUE))==1)
        {
            $searchParams=array(
            'eq_model_name' => validate_string($this->input->post('eq_model_name', TRUE))
           );

            $t_eq_model = $this->XL_downloads_m->download_eq_model($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Equipment Model name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_eq_model)>0)
            { 
                $sno = 1;            
                foreach($t_eq_model as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['eq_model_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Equipment Model'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_country()
    {
        $task_access = page_access_check('country_master');
        if(validate_string($this->input->post('download_country',TRUE))==1)
        {
            $searchParams=array(
             'country_name' => validate_string($this->input->post('country_name', TRUE)),
            'currency_name' => validate_string($this->input->post('currency_name', TRUE))
           );

            $t_country = $this->XL_downloads_m->download_country($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Country Name','Short Name','Currency');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_country)>0)
            { 
                $sno = 1;            
                foreach($t_country as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='<td align="center">'.$row['short_name'].'</td>';
                    $data.='<td align="center">'.$row['currency_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Country'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

     public function download_zone()
    {
        $task_access = page_access_check('zone_master');
        if(validate_string($this->input->post('download_zone',TRUE))==1)
        {
            $searchParams=array(
             'zone_name'  => validate_string($this->input->post('zone_name', TRUE)),
            'country_id' => validate_number($this->input->post('country_id', TRUE))
           );

            $t_zone = $this->XL_downloads_m->download_zone($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Country Name','Zone Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_zone)>0)
            { 
                $sno = 1;            
                foreach($t_zone as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Zone'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

     public function download_state()
    {
        $task_access = page_access_check('state_master');
        if(validate_string($this->input->post('download_state',TRUE))==1)
        {
            $searchParams=array(
            'state_name' => validate_string($this->input->post('state_name', TRUE)),
           'zone_id'    => validate_number($this->input->post('zone_id', TRUE)),
           'country_id' => validate_number($this->input->post('country_id',TRUE))
           );

            $t_state = $this->XL_downloads_m->download_state($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Zone Name','State Name','Short Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_state)>0)
            { 
                $sno = 1;            
                foreach($t_state as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['zone_name'].'</td>';
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='<td align="center">'.$row['short_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='State'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_city()
    {
        $task_access = page_access_check('city_master');
        if(validate_string($this->input->post('download_city',TRUE))==1)
        {
            $searchParams=array(
            'city_name'  => validate_string(@$this->input->post('city_name', TRUE)),
            'state_id'   => validate_number(@$this->input->post('state_id', TRUE)),
            'zone_id'    => validate_number(@$this->input->post('zone_id', TRUE)),
            'country_id' => validate_number(@$this->input->post('country_id', TRUE))
           );

            $t_city = $this->XL_downloads_m->download_city($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','City Name','State Name','Zone Name','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_city)>0)
            { 
                $sno = 1;            
                foreach($t_city as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='<td align="center">'.$row['state_name'].'</td>';
                    $data.='<td align="center">'.$row['zone_name'].'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='City'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_area()
    {
        $task_access = page_access_check('area_master');
        if(validate_string($this->input->post('download_area',TRUE))==1)
        {
            $searchParams=array(
            'area_name'  => validate_string($this->input->post('area_name', TRUE)),
            'city_id'    => validate_number($this->input->post('city_id', TRUE)),
            'state_id'   => validate_number(@$this->input->post('state_id', TRUE)),
            'zone_id'    => validate_number(@$this->input->post('zone_id', TRUE)),
            'country_id' => validate_number(@$this->input->post('country_id', TRUE))
           );

            $t_area = $this->XL_downloads_m->download_area($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Area Name','City Name','State Name','Zone Name','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_area)>0)
            { 
                $sno = 1;            
                foreach($t_area as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='<td align="center">'.$row['city_name'].'</td>';
                    $data.='<td align="center">'.$row['state_name'].'</td>';
                    $data.='<td align="center">'.$row['zone_name'].'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Area'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_document_type()
    {
        $task_access = page_access_check('document_type');
        if(validate_string($this->input->post('download_document_type',TRUE))==1)
        {
            $searchParams=array(
            'name'   => validate_string($this->input->post('doc_name', TRUE)),
            'workflow_type' => validate_number($this->input->post('workflow_type',TRUE))
           );
            $t_document_type = $this->XL_downloads_m->download_document_type($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Workflow Type','Document Type');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_document_type)>0)
            { 
                $sno = 1;            
                foreach($t_document_type as $row)
                {
                    if($row['workflow_type']=='1')
                    {
                        $workflow_type1 = 'Asset Master';
                    }
                    elseif($row['workflow_type']=='2')
                    {
                       $workflow_type1 = 'Tools Order';
                    }
                    elseif($row['workflow_type']=='3')
                    {
                       $workflow_type1 = 'Stock Transfer';
                    }
                    elseif($row['workflow_type']=='4')
                    {
                       $workflow_type1 = 'Calibration';
                    }
                    elseif($row['workflow_type']=='5')
                    {
                       $workflow_type1 = 'Repair';
                    }
                    elseif($row['workflow_type']=='6')
                    {
                       $workflow_type1 = 'Scrap';
                    }
                    else
                    {
                        echo "NA";
                    }

                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$workflow_type1.'</td>';
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='</tr>';                   
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Document Type'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_designation()
    {
        $task_access = page_access_check('designation');
        if(validate_string($this->input->post('download_designation',TRUE))==1)
        {
            $searchParams=array(
            'roleid'               => validate_number($this->input->post('roleid', TRUE)),
            'designame'        => validate_string($this->input->post('designation_name',TRUE))
           );

            $t_designation = $this->XL_downloads_m->download_designation($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Designation','Role');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_designation)>0)
            { 
                $sno = 1;            
                foreach($t_designation as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['designation_name'].'</td>';
                    $data.='<td align="center">'.$row['role_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='Designation'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function download_gps()
    {
        $task_access = page_access_check('gps_tracking');
        if(validate_string($this->input->post('download_gps',TRUE))==1)
        {
            $searchParams=array(
            'gps_name'      => validate_string($this->input->post('gps_name', TRUE)),
            'gps_uid'       => validate_string($this->input->post('gps_uid',TRUE)),
            'gps_country'   => validate_number($this->input->post('country_id',TRUE))
           );

            $t_gps = $this->XL_downloads_m->download_gps($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('S.No.','Tool Tracker Name','UID Number','Country Name');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($t_gps)>0)
            { 
                $sno = 1;            
                foreach($t_gps as $row)
                {
                    $data.='<tr>';
                    $data.='<td align="center">'.$sno++.'</td>'; 
                    $data.='<td align="center">'.$row['name'].'</td>';
                    $data.='<td align="center">'.$row['uid_number'].'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='GPS'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function downloadAckFromWHOrders()
    {
        #page Authentication
        $task_access = page_access_check('receive_order');
        if(validate_string($this->input->post('downloadAckFromWHOrders',TRUE))==1)
        {
            $searchParams=array(
                'order_number'  => validate_string($this->input->post('order_number', TRUE)),
                'deploy_date'   => validate_string($this->input->post('deploy_date', TRUE)),
                'return_date'   => validate_string($this->input->post('return_date', TRUE)),
                'rec_sso_id'    => validate_string($this->input->post('rec_sso_id', TRUE)),
                'rec_country_id' => validate_string($this->input->post('rec_country_id', TRUE)),
                'order_delivery_type_id' => validate_number($this->input->post('order_delivery_type_id', TRUE))
            );

            $orderResults = $this->XL_downloads_m->receive_order_results($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('Order Number','Requested By','Order Type','Request Date','Need Date','Return Date',
                'Order Status','Service Type','Siebel SR Number','Country','Type','Ship To Address','Tool Number',
                'Tool Code','Tool Description','Ordered Qty','Avail Qty',
                'ST Number','Shipment From','Tool Number','Tool Descrption','Tool Code','Asset Number'
            );
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            
            $data.='<tbody>';
            if(count($orderResults)>0)
            {                
                foreach($orderResults as $row)
                {
                    $return_assets = array();
                    $orderedTools = $this->Order_m->get_ordered_tools($row['tool_order_id']);

                    if($row['stock_transfer_id']!='') # Stock Transfers
                    {
                        $ackType = 2;
                        $st_arr = array();
                        $stAssetsCount = 0;
                        $stData = $this->Stock_transfer_m->stnForOrder($row['tool_order_id'],1,1);                
                        if(count($stData)>0){
                            foreach ($stData as $st_key => $st_data) {
                                $st_return_assets = $this->Order_m->wh_return_initialted_assets($st_data['tool_order_id'],1); 
                                $st_arr[$st_data['tool_order_id']]['stRow'] = $st_data;
                                $st_arr[$st_data['tool_order_id']]['st_assets_list'] = $st_return_assets;
                                $stAssetsCount+= count($st_return_assets);
                            }
                        }
                        else
                        {
                            $firstRowSpan = count($orderedTools);  
                        }
                        $firstRowSpan = $stAssetsCount;
                    } # Normal transaction
                    else
                    {
                        $ackType = 1;
                        $return_assets = $this->Order_m->wh_return_initialted_assets($row['tool_order_id']);
                        if(count($return_assets)>0)
                        {
                            $firstRowSpan =count($return_assets);
                        }
                        else
                        {
                            $firstRowSpan = count($orderedTools);
                        }
                    }                   
                    
                    $status = getReasonForOrderAtAdmin($row['tool_order_id']);                                              
                    if($row['order_delivery_type_id'] == 1)
                    {                
                        $address['system_id'] = $row['system_id'];
                        $address['site_id'] = $row['site_id'];
                        $rest =  $this->Order_m->get_install_basedata($row['system_id']);        
                        $address['c_name'] = $rest['c_name'];
                        
                    }
                    else
                    {
                        if($row['order_delivery_type_id'] == 2)
                        {
                            $address['warehouse'] = getWhCodeAndName($row['wh_id']);                    
                        }
                    }
                    $address['address1'] = $row['address1'];
                    $address['address2'] = $row['address2'];
                    $address['address3'] = $row['address3'];
                    $address['address4'] = $row['address4'];
                    $address['pin_code'] = $row['pin_code'];
                    $address_string = implode(",",$address);
                    $type = ($row['fe_check'] == 0)?'Request For Tool':'Transfer From FE';                    
                    $request_date =($row['request_date']=='')?"NA": full_indian_format($row['request_date']);
                    $need_date = ($row['need_date']=='')?"NA":full_indian_format($row['need_date']);
                    $return_date = ($row['return_date']=='')?"NA":full_indian_format($row['return_date']);
                    $data.='<tr>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['order_number'].'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['sso'].'</td>';  
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['order_type'].'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.$request_date.'</td>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.$need_date.'</td>';                    
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.$return_date.'</td>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['name'].'</td>';                    
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['service_type'].'</td>';
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['siebel_sr_number'].'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$row['country_name'].'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$type.'</td>'; 
                    $data.='<td rowspan="'.($firstRowSpan).'" style="vertical-align: middle; text-align: center;">'.@$address_string.'</td>';
                        
                    $i = 0;
                    if($ackType == 2)
                    {
                        if(count($st_arr)>0){
                            foreach ($st_arr as $st_arr_key => $st_arr_val){
                                $ci= 0;
                                $tool_code ='';
                                if(@$orderedTools[$i]['tool_code']!='')
                                    $tool_code = '`'.@$orderedTools[$i]['tool_code'];                            
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code.'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['quantity'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['available_quantity'].'</td>';                    
                                $stRow = $st_arr_val['stRow'];
                                $stAssetsList = $st_arr_val['st_assets_list'];
                                $data.='<td rowspan="'.count($stAssetsList).'" style="vertical-align: middle; text-align: center;">'.$stRow['stn_number'].'</td>'; 
                                $data.='<td rowspan="'.count($stAssetsList).'" style="vertical-align: middle; text-align: center;">'.@$stRow['wh'].'</td>';  
                                $j = 1;
                                if(count($stAssetsList)>0){
                                    foreach ($stAssetsList as $keyyy => $asset_list_row)
                                    {
                                        if($j!=1)
                                        {
                                            $tool_code ='';
                                            if(@$orderedTools[$i]['tool_code']!='')
                                                $tool_code = '`'.@$orderedTools[$i]['tool_code'];
                                            $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_number'].'</td>'; 
                                            $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code.'</td>';  
                                            $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_description'].'</td>'; 
                                            $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['quantity'].'</td>';
                                            $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['available_quantity'].'</td>';                    
                                        }
                                        $tool_code1='';
                                        if($asset_list_row['tool_code']!='')
                                            $tool_code1 = '`'.$asset_list_row['tool_code'];
                                        $data.='<td style="vertical-align: middle; text-align: center;">'.@$asset_list_row['part_number'].'</td>';                       
                                        $data.='<td style="vertical-align: middle; text-align: center;">'.@$asset_list_row['part_description'].'</td>';                       
                                        $data.='<td style="vertical-align: middle; text-align: center;">'.@$tool_code1.'</td>';                       
                                        $data.='<td style="vertical-align: middle; text-align: center;">'.@$asset_list_row['asset_number'].'</td>';                                        
                                        $data.='</tr>';
                                        $i++; 
                                        $j++;
                                    }                      
                                }else{
                                    if($ci==0){
                                        $data.='<td colspan="4" rowspan="'.$firstRowSpan.'" style="vertical-align: middle; text-align: center;">No Assets Found For This Order</td>';                    
                                        $data.='</tr>';      
                                        $ci++;   
                                    } 
                                }                                                  
                            }                    
                        }  
                        else
                        {
                            $ci=0;
                            foreach ($orderedTools as $key => $value)
                            {
                                $tool_code='';
                                if(@$orderedTools[$i]['tool_code']!='')
                                    $tool_code = '`'.$value['tool_code'];
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code.'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['quantity'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['available_quantity'].'</td>';                                                    
                                if($ci==0)
                                    $data.='<td colspan="6" rowspan="'.$firstRowSpan.'" style="vertical-align: middle; text-align: center;">No Assets Found For This Order</td>';                    
                                $data.='</tr>';      
                                $ci++;                                
                            } 
                        }
                    }
                    else
                    {
                        $ci = 0;
                        $i= 0;
                        if(count($return_assets)>0)
                        {                            
                            foreach ($return_assets as $key1 => $value1) 
                            {
                                $tool_code ='';
                                if(@$orderedTools[$i]['tool_code']!='')
                                    $tool_code = '`'.@$orderedTools[$i]['tool_code'];
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code.'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['quantity'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['available_quantity'].'</td>';                    
                                $tool_code1='';
                                if($value1['tool_code']!='')
                                    $tool_code1 = '`'.$value1['tool_code'];
                                $data.='<td style="vertical-align: middle; text-align: center;">NA</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">NA</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value1['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value1['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code1.'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$value1['asset_number'].'</td>';                    
                                $data.='</tr>';   
                                $i++;   
                            }
                        }
                        else
                        {
                            foreach ($orderedTools as $key => $value)
                            {
                                $tool_code='';
                                if(@$orderedTools[$i]['tool_code']!='')
                                    $tool_code = '`'.$value['tool_code'];
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_number'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$tool_code.'</td>';  
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['part_description'].'</td>'; 
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['quantity'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.@$orderedTools[$i]['available_quantity'].'</td>';                                                    
                                if($ci==0)
                                    $data.='<td colspan="6" rowspan="'.$firstRowSpan.'" style="vertical-align: middle; text-align: center;">No Assets Found For This Order</td>';                    
                                $data.='</tr>';      
                                $ci++;                                
                            } 
                        }                         
                    }
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='ackFromWHOrdersDownloads'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function downloadReturnOrders()
    {
        #page Authentication
        $task_access = page_access_check('raise_pickup');
        if(validate_string($this->input->post('downloadReturnOrders',TRUE))==1)
        {
            $searchParams=array(
                'order_number' => validate_string($this->input->post('order_number', TRUE)),
                'order_delivery_type_id'  => validate_number($this->input->post('order_delivery_type_id',TRUE)),
                'deploy_date' => validate_string($this->input->post('deploy_date', TRUE)),
                'return_date' => validate_string($this->input->post('return_date', TRUE)),
                'rt_sso_id' => validate_string($this->input->post('rt_sso_id', TRUE)),
                'rt_country_id' => validate_string($this->input->post('rt_country_id', TRUE))
            );
            $orderResults = $this->XL_downloads_m->owned_order_results($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('Order Number','Requested By','Order Type','Request Date','Need Date','Return Date',
                'Service Type','Siebel SR Number','Country','Type','Ship To Address',
                'Tool Number','Tool Code','Tool Description','Asset Number');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';            
            $data.='<tbody>';
            if(count($orderResults)>0)
            {                
                foreach($orderResults as $row)
                {
                    $orderedTools = $this->Order_m->getOwnedAssetsDetailsByOrderId($row['tool_order_id']);
                    if(count($orderedTools)>0)
                    {
                        $firstRowSpan = count($orderedTools);
                    }
                    else
                    {
                        $firstRowSpan = 1;
                    }
                    if($row['order_delivery_type_id'] == 1)
                    {                
                        $address['system_id'] = $row['system_id'];
                        $address['site_id'] = $row['site_id'];
                        $rest =  $this->Order_m->get_install_basedata($row['system_id']);        
                        $address['c_name'] = $rest['c_name'];
                    }
                    else
                    {
                        if($row['order_delivery_type_id'] == 2)
                        {
                            $address['warehouse'] = getWhCodeAndName($row['wh_id']);                    
                        }
                    }
                    $address['address1'] = $row['address1'];
                    $address['address2'] = $row['address2'];
                    $address['address3'] = $row['address3'];
                    $address['address4'] = $row['address4'];
                    $address['pin_code'] = $row['pin_code'];
                    $address_string = implode(",",$address);
                    $type = ($row['fe_check'] == 0)?'Request For Tool':'Transfer From FE';

                    $data.='<tr>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['order_number'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['sso'].'</td>';  
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['order_type'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['request_date']).'</td>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['need_date']).'</td>';                    
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['return_date']).'</td>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['service_type'].'</td>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['siebel_sr_number'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['country_name'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$type.'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" align="center">'.$address_string.'</td>';
                    $i = 1;
                    if(count($orderedTools)>0){
                        foreach ($orderedTools as $key => $value) {
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_number'].'</td>'; 
                            $data.='<td style="vertical-align: middle; text-align: center;">'.'`'.$value['tool_code'].'</td>';  
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_description'].'</td>'; 
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['asset_number'].'</td>';               
                            $data.='</tr>';          
                        } 
                    }
                    else
                    {
                        $data.='<td colspan="4" align="center">Waiting for Admin Approvals or Extended date request.</td></tr>';  
                    }
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='ReturnOrdersDownloads'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }
    
}