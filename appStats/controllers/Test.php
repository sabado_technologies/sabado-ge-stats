<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Test extends CI_Controller {

	public  function __construct() 
	{
        parent::__construct();
	}

	public function send_mail()
	{
		$this->load->view('user/test_mail.php');
	}


	public function submit_mail()
	{
		if(validate_string($this->input->post('submit',TRUE))!='')
		{
			$body = $this->input->post('body',TRUE);
			if($body=='') $body = 'Hi, This is test mail';
			$body .= '<br>Timestamp: '.date('Y-m-d H:i:s');
			$to = $this->input->post('to',TRUE);
			$subject = $this->input->post('subject',TRUE);
			if($subject=='') $subject = 'Test Mail';
			
			$status  = send_email($to,$subject,$body);
			
			if($status)
			{
				echo 'Mail Sent Successfully<br>';
				echo 'To : '.$to.'<br>';
				echo 'Subject : '.$subject.'<br>';
				echo 'Body : '.$body.'<br>';
			}
			else
			{
				echo $status.'<br>';
				echo 'Something went wrong';
			}
		}
		echo '<br><a href="'.SITE_URL.'send_mail">Back</a>';
	}  	
}
?>