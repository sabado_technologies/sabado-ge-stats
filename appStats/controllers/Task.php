<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Task extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Task_m');
    }

    public function sub_task()
    {
        
        $data['nestedView']['heading']="Manage Sub Task";
        $data['nestedView']['cur_page'] = 'sub_task';
        $data['nestedView']['parent_page'] = 'sub_task';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Manage Sub Task';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Sub Task','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchtask', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'task_name'  => validate_string($this->input->post('task_name', TRUE)),
            'page_id'    => validate_number($this->input->post('page_id', TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                'task_name'   => $this->session->userdata('task_name'),
                'page_id'     => $this->session->userdata('page_id'));
            }
            else {
                $searchParams=array(
                'task_name'  => '',
                'page_id'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'sub_task/';
        # Total Records
        $config['total_rows'] = $this->Task_m->task_total_num_rows($searchParams);

        $config['per_page'] =getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['taskResults'] = $this->Task_m->task_results($current_offset, $config['per_page'], $searchParams);

        # Additional data
        $data['displayResults'] = 1;
        $data['page_name'] = array('' =>'- Select Task -')+$this->Common_model->get_dropdown('page','page_id','name');

        $this->load->view('task/task_view',$data);

    }
    public function add_task()
    {
        $data['nestedView']['heading']="Add New Sub Task";
        $data['nestedView']['cur_page'] = 'sub_task';
        $data['nestedView']['parent_page'] = 'sub_task';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/task.js"></script>';
        
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Add New Sub Task';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Sub Task','class'=>'','url'=>SITE_URL.'task');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Sub Task','class'=>'active','url'=>'');

        $data['pageList'] = $this->Common_model->get_data('page',array('status' => 1));
        # Additional data
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_task';
        $data['displayResults'] = 0;
        $this->load->view('task/task_view',$data);
    }

    public function insert_task()
    {
        $page_id = validate_number(trim($this->input->post('page_id',TRUE)));
        $task_name=validate_string($this->input->post('name',TRUE));
        $title=validate_string($this->input->post('title',TRUE));
        $task_id=0;
        $taskname_available = $this->Task_m->is_tasknameExist($task_name,$task_id,$page_id);
        if($taskname_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Task : '.$task_name.' already exist! please check.
                                     </div>'); 
            redirect(SITE_URL.'sub_task'); exit();
        }
        
        $data = array('page_id' => $page_id,
                      'name'    => $task_name,
                      'title'   => $title,
                      );
        $this->db->trans_begin();
        $this->Common_model->insert_data('task',$data);
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.</div>');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Sub Task has been Added successfully!</div>');
        }
        redirect(SITE_URL.'sub_task');  
    }

    public function edit_task()
    {
        
        $task_id=@storm_decode($this->uri->segment(2));
        if($task_id=='')
        {
            redirect(SITE_URL.'sub_task');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Sub Task ";
        $data['nestedView']['cur_page'] = 'sub_task';
        $data['nestedView']['parent_page'] = 'sub_task';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/task.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Edit Sub Task ';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Sub Task','class'=>'','url'=>SITE_URL.'task');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Sub Task ','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_task';
        $data['displayResults'] = 0;
        $data['pageList'] = $this->Common_model->get_data('page',array('status' => 1));
        # Data
        $row = $this->Common_model->get_data('task',array('task_id'=>$task_id));
        $data['lrow'] = $row[0];
        $this->load->view('task/task_view',$data);
    }

    public function update_task()
    {
        $task_id= validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($task_id=='')
        {
            redirect(SITE_URL.'sub_task');
            exit;
        }
        // GETTING INPUT TEXT VALUES
        $page_id = validate_number(trim($this->input->post('page_id',TRUE)));
        $task_name=validate_string($this->input->post('name',TRUE));

        #check task name excluding $task id record
        $taskname_available = $this->Task_m->is_tasknameExist($task_name,$task_id,$page_id);
        if($taskname_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Task : '.$task_name.' already exist! please check.
                                     </div>'); 
            redirect(SITE_URL.'sub_task'); exit();
        } 
        $this->db->trans_begin();
        $data=array('page_id' => $page_id,
                    'name'    => $task_name,
                    'title'   => $this->input->post('title',TRUE),
                    'status'  => 1,
                    );
        $where = array('task_id'=> $task_id);         
        $res = $this->Common_model->update_data('task',$data,$where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.. </div>');    
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Sub Task has been Updated successfully!</div>');
        }
        redirect(SITE_URL.'sub_task');  
    }

    public function deactivate_task($encoded_id)
    {
        
        $task_id=@storm_decode($encoded_id);
        if($task_id==''){
            redirect(SITE_URL.'sub_task');
            exit;
        }
        $where = array('task_id' => $task_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('task',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Sub Task has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'sub_task');

    }

    public function activate_task($encoded_id)
    {
       
        $task_id=@storm_decode($encoded_id);
        if($task_id==''){
            redirect(SITE_URL.'sub_task');
            exit;
        }
        $where = array('task_id' => $task_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('task',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Sub Task has been Activated successfully!
        </div>');
        redirect(SITE_URL.'sub_task');
    }

    // checking uniqueness for task name 
    public function is_tasknameExist()
    {
        $name    = validate_string($this->input->post('name',TRUE));
        $task_id = validate_number($this->input->post('task_id',TRUE));
        $page_id = validate_number($this->input->post('page_id',TRUE));
        $result  = $this->Task_m->is_tasknameExist($name,$task_id,$page_id);
        echo $result;
    }
}