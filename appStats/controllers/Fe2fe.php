<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By Maruthi on 20th july 17 8:30PM
class Fe2fe extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Order_m');
        $this->load->model('Fe2fe_m');
        $this->load->model('Pickup_point_m');
    }

    // FE Owned Tools
    public function fe_owned_tools()
    {
        $data['nestedView']['heading']= "My Inventory ";
        $data['nestedView']['cur_page'] = 'fe_owned_tools';
        $data['nestedView']['parent_page'] = 'fe_owned_tools';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'My Inventory';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'My Inventory','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('fe_owned_search', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'order_number'     => validate_string($this->input->post('order_number', TRUE)),
                'part_number'      => validate_string($this->input->post('part_number', TRUE)),
                'part_description' => validate_string($this->input->post('part_description', TRUE)),
                'asset_number'     => validate_string($this->input->post('asset_number',TRUE)),
                'myi_country_id'   => validate_number($this->input->post('myi_country_id',TRUE)),
                'users_id'         => validate_number($this->input->post('users_id',TRUE)),
                'serial_no'        => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'order_number'     => $this->session->userdata('order_number'),
                    'part_number'      => $this->session->userdata('part_number'),
                    'part_description' => $this->session->userdata('part_description'),
                    'asset_number'     => $this->session->userdata('asset_number'),
                    'myi_country_id'   => $this->session->userdata('myi_country_id'),
                    'users_id'         => $this->session->userdata('users_id'),
                    'serial_no'        => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'order_number'     => '',
                    'part_number'      => '',
                    'part_description' => '',
                    'asset_number'     => '',
                    'myi_country_id'   => '',
                    'users_id'         => '',
                    'serial_no'        => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'fe_owned_tools/';

        # Total Records
        $config['total_rows'] = $this->Fe2fe_m->fe_owned_tools_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Fe2fe_m->fe_owned_tools_results($current_offset, $config['per_page'], $searchParams,$task_access);

        $assets_data = array();
        if(count($data['orderResults'])>0)
        {
            foreach ($data['orderResults'] as $key => $value)
            {
               $asset_data[$value['asset_id']] = get_asset_data($value['asset_id']);
            }
            $data['asset_data'] = $asset_data;
        }
        # Additional data
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));        
        $this->load->view('fe2fe/fe_owned_tools',$data);
    }

    // FE Owned Tools
    public function toolsAvailabilityWithFE()
    {
        $data['nestedView']['heading']="Tools Availablity With FE";
        $data['nestedView']['cur_page'] = 'toolsAvailabilityWithFE';
        $data['nestedView']['parent_page'] = 'toolsAvailabilityWithFE';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';

        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Tools Availablity With FE';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tools Availablity With FE','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('fe_owned_search', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'order_number'             => validate_string($this->input->post('order_number', TRUE)),
                               'ser_sso_id'               => validate_string($this->input->post('ser_sso_id', TRUE)),
                               'part_number'              => validate_string($this->input->post('part_number', TRUE)),
                               'part_description'         => validate_string($this->input->post('part_description', TRUE)),
                               'asset_number'             => validate_string($this->input->post('asset_number',TRUE))

                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                              'order_number'              => $this->session->userdata('order_number'),
                              'ser_sso_id'              => $this->session->userdata('ser_sso_id'),
                              'part_number'               => $this->session->userdata('part_number'),
                              'part_description'          => $this->session->userdata('part_description'),
                              'asset_number'              =>  $this->session->userdata('asset_number')
                              );
            }
            else {
                $searchParams=array(
                                    'order_number'         => '',
                                    'ser_sso_id'           => '',
                                    'part_number'          => '',
                                    'part_description'     => '',
                                    'asset_number'         => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;

        


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'toolsAvailabilityWithFE/';

        # Total Records
        $config['total_rows'] = $this->Fe2fe_m->toolsAvailabilityWithFEtotal_num_rows($searchParams);
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Fe2fe_m->toolsAvailabilityWithFEresults($current_offset, $config['per_page'], $searchParams);
       // echo '<pre>';print_r($data['orderResults']);exit;
        $assets_data = array();
        if(count($data['orderResults'])>0)
        {
            foreach ($data['orderResults'] as $key => $value)
            {
               $asset_data[$value['asset_id']] = get_asset_data($value['asset_id']);
            }
            $data['asset_data'] = $asset_data;
        } 
        # Additional data
        $data['flg'] = 1;

        $this->load->view('fe2fe/fe_owned_tools',$data);

    }

    // Fe2 Fe Approval
    public function fe2_fe_approval()
    {
        $task_access = page_access_check('fe2_fe_approval');
        $data['nestedView']['heading']="FE to FE Transfer Approval";
        $data['nestedView']['cur_page'] = 'fe2_fe_approval';
        $data['nestedView']['parent_page'] = 'fe2_fe_approval';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'FE to FE Transfer Approval';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE to FE Transfer Approval','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('fe2_fe_approval', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'order_number'           => validate_string($this->input->post('order_number', TRUE)),
            'order_delivery_type_id' => validate_string($this->input->post('order_delivery_type_id', TRUE)),
            'deploy_date'            => validate_string($this->input->post('deploy_date', TRUE)),
            'return_date'            => validate_string($this->input->post('return_date', TRUE)),
            'fe2_fe_sso_id'          => validate_string($this->input->post('fe2_fe_sso_id', TRUE)),
            'fe2_fe_country_id'      => validate_string($this->input->post('fe2_fe_country_id', TRUE))

                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order_number'           => $this->session->userdata('order_number'),
                'order_delivery_type_id' => $this->session->userdata('order_delivery_type_id'),
                'deploy_date'            => $this->session->userdata('deploy_date'),
                'return_date'            => $this->session->userdata('return_date'),
                'fe2_fe_sso_id'          => $this->session->userdata('fe2_fe_sso_id'),
                'fe2_fe_country_id'      => $this->session->userdata('fe2_fe_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'order_number'           => '',
                'order_delivery_type_id' => '',
                'deploy_date'            => '',
                'return_date'            => '',
                'fe2_fe_sso_id'          => '',
                'fe2_fe_country_id'      => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'fe2_fe_approval/';

        # Total Records
        $config['total_rows'] = $this->Fe2fe_m->fe2_fe_approval_order_total_num_rows($searchParams,$task_access);
        //echo "<pre>"; print_r($data['toolResults']);exit;
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Fe2fe_m->fe2_fe_approval_order_results($current_offset, $config['per_page'], $searchParams,$task_access);
        //echo "<pre>"; print_r($data['orderResults']);exit;
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1)); 
        # Additional data
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));

        $this->load->view('fe2fe/fe2_fe_approval',$data);

    }
    public function fe2_fe_approval_details()
    {        
        $rto_id = storm_decode($this->uri->segment(2));
        if($rto_id == '')
        {
            redirect(SITE_URL.'fe2_fe_approval'); exit();
        }
        $data['rto_id'] = $rto_id;

        $data['nestedView']['heading']="FE to FE Transfer Approval Details";
        $data['nestedView']['cur_page'] = "fe2_fe_approval_second";
        $data['nestedView']['parent_page'] = 'fe2_fe_approval';

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/fe2_fe_receive.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'FE to FE Transfer Approval Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE to FE Transfer Approval ','class'=>'','url'=>SITE_URL.'fe2_fe_approval');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE to FE Transfer Approval Details ','class'=>'','url'=>'');
    
        $return_assets = $this->Fe2fe_m->get_return_initialted_assets($rto_id);

        $return_parts = array();
        foreach ($return_assets as $key => $value) 
        {
            $return_parts[$value['oah_id']]['part_number']  = $value['part_number'];
            $return_parts[$value['oah_id']]['asset_number'] = $value['asset_number'];
            $return_parts[$value['oah_id']]['serial_number'] = $value['serial_number'];
            $return_parts[$value['oah_id']]['asset_id']     = $value['asset_id'];
            $return_parts[$value['oah_id']]['asset_status'] = $value['asset_status'];
            $return_parts[$value['oah_id']]['ordered_asset_id'] = $value['ordered_asset_id'];
            $return_parts[$value['oah_id']]['part_description'] = $value['part_description'];
            $return_parts[$value['oah_id']]['health_data'] = $this->Order_m->get_transit_asset_data($value['oah_id']);
        }
        
        $data['return_assets'] = $return_assets;
        $data['return_parts'] = $return_parts;        
        $return_order_id = $this->Common_model->get_value('return_tool_order',array('rto_id'=>$rto_id),'return_order_id');
        $data['return_info'] = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));

        // For Address Info
        $return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
        {
            $trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['wh_id']));
            $trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
        }
        else 
        {
            $trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
            $sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
            $sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
            $trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];
        }
        $trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');
        
        $rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
        $asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
        
        $data['flg'] = 1;
        $data['rrow'] = $rrow;
        $data['trrow'] = $trrow;
        $data['asset_list'] = $asset_list;
        $data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));

        $this->load->view('fe2fe/fe2_fe_approval_details',$data);
    }
    
    public function insert_fe2_fe_approval()
    {
        $_SESSION['fe2_fe_approval'] = 1;
        $this->db->trans_begin();
        $return_type_id = validate_number($this->input->post('return_type_id',TRUE));
        $return_order_id = validate_number($this->input->post('return_order_id',TRUE));
        $admin_remarks = validate_string($this->input->post('admin_remarks',TRUE));
        $fe1_tool_order_id = validate_number($this->input->post('fe1_tool_order_id',TRUE));
        $fe2_tool_order_id = validate_number($this->input->post('fe2_tool_order_id',TRUE));        

        $fe1_data = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe1_tool_order_id));
        $fe2_data = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe2_tool_order_id));            
        $return_data = $this->Common_model->get_data('return_order',array('tool_order_id'=>$fe2_tool_order_id,'return_approval'=>2));
       
        if(isset($_REQUEST['approve']))
        {   
            $approval = validate_string($this->input->post('approval',TRUE));                
            $rto_id = validate_number($this->input->post('rto_id',TRUE));
            $order_status_id = validate_number($this->input->post('order_status_id',TRUE));
            $oah_oa_health_id_part_id = $this->input->post('oah_oa_health_id_part_id',TRUE);
            $oah_id_arr = $this->input->post('oah_id',TRUE);
            $oa_health_id_arr = $this->input->post('oa_health_id',TRUE);
            $remarks_arr = $this->input->post('remarks',TRUE);
            
            $history_status_id = $this->input->post('history_status_id',TRUE);
            $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
            $oah_condition_id = $this->input->post('oah_condition_id',TRUE);
            $assetAndOrderedAsset = $this->input->post('assetAndOrderedAsset',TRUE);
            
            $fe2_country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$fe2_tool_order_id),'country_id');
            $fe1_tool_order_id = validate_number($this->input->post('fe1_tool_order_id',TRUE));

            $returnDataRow = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
            $return_number = $returnDataRow['return_number'];   

            if(count($return_data)>0)
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Sorry!</strong> Tool Order '.$fe2_data['order_number'].' is already In Progress!. </div>'); 
                redirect(SITE_URL.'fe2_fe_approval'); exit();   
            }
            
            if($fe2_data['status'] == 10 || $fe2_data['current_stage_id'] > 4)
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Sorry!</strong> Tool Order '.$fe2_data['order_number'].' is cancelled or Order has already fullfilled!. </div>'); 
                redirect(SITE_URL.'fe2_fe_approval'); exit();
            }

            if($return_type_id == 3)
            {    
                // preparing data for FE2 with asset info
                $k=0; $j=0;
                $fe2_owned_order_assets = array();
                foreach ($oah_id_arr as $oah_id => $ordered_asset_id)
                {
                    $asset_id = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$ordered_asset_id),'asset_id');
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    {   
                        $part_id = $value[0];
                        $part_health_array = array(
                            'part_id'               => $part_id,
                            'asset_condition_id'    => $oa_health_id_arr[$oah_id][$oa_health_id][$part_id],
                            'remarks'               => $remarks_arr[$oah_id][$oa_health_id][$part_id]
                        );
                        $fe2_owned_order_assets[$asset_id][] = $part_health_array;
                    }                        
                }
                // FE2 order

                $fe2OrderOldArr = array('current_stage' => getReasonForOrderAtAdmin($fe2_tool_order_id));
                $md_array = array(
                    'current_stage_id' => 6,
                    'modified_by'      => $this->session->userdata('sso_id'),
                    'modified_time'    => date('Y-m-d H:i:s')
                );
                $this->Common_model->update_data('tool_order',$md_array,array('tool_order_id'=>$fe2_tool_order_id));
                # Audit Start for FE2 Order                
                $newArr = array('current_stage' => getOpenOrderStatus($fe2_data));
                $blockArr = array("blockArr"=> "Tool Order");
                $remarksString = 'FE to FE Receive Process is Approved and is in Progress for '.$fe2_data['order_number'];
                $fe2_to_ad = tool_order_audit_data('tool_order',$fe2_tool_order_id,'tool_order',2,'',$newArr,$blockArr,$fe2OrderOldArr,$remarksString,'',array(),"tool_order",$fe2_tool_order_id,$fe2_data['country_id']);
                # Audit End for FE2 Order

                #audit Start for Fe1 Order
                $oldReturnOrderStatus = returnOrderStatusWithRtoId($rto_id);
                $fe1oldArr = array('current_stage' => getOpenOrderStatus($fe1_data));

                // updating the return approval
                $this->Common_model->update_data('return_order',array('return_approval'=>2,'remarks'=>$admin_remarks,'status'=>10),array('return_order_id'=>$return_order_id));                
                # Return order Audit
                $return_order_data = array(
                    'return_status'   => "Approved",
                    'remarks'         => $admin_remarks
                    );
                $oldArr = array('return_status'   => $oldReturnOrderStatus);
                $blockArr = array('blockName'=> "Tool Order Return");
                $remarksString = "FE to FE By Hand return request ".$return_number." Approved by Admin to transfer from ".$fe1_data['order_number']." To ".$fe2_data['order_number'];
                $fe1ReturnAd = tool_order_audit_data('return_order',$return_order_id,'return_order',2,'',$return_order_data,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$fe1_tool_order_id,$fe1_data['country_id']);

                // get fe2 ordered tool data 
                $k = 0;
                $fe2_ordered_tools  = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$fe2_tool_order_id,'status'=>2));
                foreach ($fe2_ordered_tools as $key => $tools)
                {                
                    $searchParams=array('tool_id'=>$tools['tool_id'],'country_id'=>$fe2_country_id);
                    $fe2_ordered_tool_id = $tools['ordered_tool_id'];
                    $fe2_tool_ordered_qty = $tools['quantity'];
                    $tool_assets_data = tool_assets($searchParams);
                    
                    // assets belongs to pertivular tool
                    $fe2_assets_arr = array();
                    foreach ($tool_assets_data as $key => $value)
                    {
                         $fe2_assets_arr[] = $value['asset_id'];
                    }
                    // assets send by FE1
                    foreach ($assetAndOrderedAsset as $posted_asset_id => $posted_ordered_asset)
                    {
                        //  acknowledge only the tools which are received and skipping the tools not received those data updated in fe1 closed data
                        if($oah_condition_id[$posted_ordered_asset] == 1)
                        {
                            // checking assets send by fe1 available in fe2 tools with assets
                            if(in_array($posted_asset_id,$fe2_assets_arr))
                            {
                                $qt = 1;
                                $qry = 'UPDATE ordered_tool SET available_quantity = available_quantity+"'.$qt.'", modified_by = "'.$this->session->userdata('sso_id').'", 
                                        modified_time = "'.date('Y-m-d H:i:s').'" WHERE ordered_tool_id = '.$tools['ordered_tool_id'];
                                $this->db->query($qry);

                                // inserting FE owned current stage
                                if($k == 0)
                                {
                                    // Updating the End Time of Previous Stage
                                    $this->Common_model->update_data('order_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('tool_order_id'=>$fe2_tool_order_id,'current_stage_id'=>4));

                                    $fe_ownd_ord_status = array(
                                        'current_stage_id'  => 6,
                                        'rto_id'            => $rto_id,
                                        'tool_order_id'     => $fe2_tool_order_id,
                                        'created_by'        => $this->session->userdata('sso_id'),
                                        'created_time'      => date('Y-m-d H:i:s')

                                             );
                                    $fe2_order_status_id = $this->Common_model->insert_data('order_status_history',$fe_ownd_ord_status);
                                    $k++;
                                }
                                
                                // inserting order asset history
                                $fe2_ownd_ord_asset = array(
                                    'ordered_tool_id'   => $tools['ordered_tool_id'],
                                    'asset_id'          => $posted_asset_id,
                                    'created_by'        => $this->session->userdata('sso_id'),
                                    'created_time'      => date('Y-m-d H:i:s'),
                                    'status'            => 2
                                );
                                $fe2_ordered_asset_id = $this->Common_model->insert_data('ordered_asset',$fe2_ownd_ord_asset);

                                #inserting into order asset history
                                $fe2_ownd_ord_asset_hist = array(
                                    'ordered_asset_id' => $fe2_ordered_asset_id,
                                    'order_status_id'  => $fe2_order_status_id,
                                    'created_by'       => $this->session->userdata('sso_id'),
                                    'created_time'     => date('Y-m-d H:i:s')
                                );
                                $fe2_oah_id = $this->Common_model->insert_data('order_asset_history',$fe2_ownd_ord_asset_hist);

                                // inserting asset health 
                                $fe2_asset_history_status =0 ;
                                foreach ($fe2_owned_order_assets[$posted_asset_id] as  $prepared_parts)
                                {
                                    $prepared_order_asset_health = array(
                                        'asset_condition_id' =>$prepared_parts['asset_condition_id'],
                                        'part_id'            =>$prepared_parts['part_id'],
                                        'oah_id'             =>$fe2_oah_id,
                                        'remarks'            =>$prepared_parts['remarks']
                                    );
                                    $this->Common_model->insert_data('order_asset_health',$prepared_order_asset_health);

                                    // to tell overall asset status 
                                    if($prepared_parts['asset_condition_id'] != 1)
                                        $fe2_asset_history_status = 2;   
                                }
                                // updating the asset status based on health condition of all parts
                                if(@$fe2_asset_history_status == 2)
                                {
                                    $this->Common_model->update_data('order_asset_history',array('status'=>2),array('oah_id'=>$fe2_oah_id));
                                }

                                // doing validation to accept upto quantity and comming out of loop 
                                $fe2_avail_qty = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$fe2_ordered_tool_id),'available_quantity');
                                if($fe2_avail_qty == $fe2_tool_ordered_qty)
                                {
                                    break;
                                }
                            }
                        }
                    }

                    // changing the status of ordered tool based on avail quantity
                    $fe2_avail_qty = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$fe2_ordered_tool_id),'available_quantity');
                    if(@$fe2_avail_qty == $fe2_tool_ordered_qty)
                    {   
                        $fe2_ord_tool_u_data = array(
                            'status'        => 1,
                            'modified_by'   => $this->session->userdata('sso_id'),
                            'modified_time' => date('Y-m-d H:i:s'),
                            'remarks'       => 'FE2 FE transfer happened'
                        );
                        $this->Common_model->update_data('ordered_tool',$fe2_ord_tool_u_data,array('ordered_tool_id'=>$fe2_ordered_tool_id));
                    }                       
                }
            }
            else
            {
                // get fe2 ordered tool data 
                $fe2OrderOldArr = array('current_stage' => getReasonForOrderAtAdmin($fe2_tool_order_id));
                # Audit Start for FE2 Order                
                $newArr = array('current_stage' => "FE to FE Receive Process is Approved.");
                $blockArr = array("blockArr"=> "Tool Order");
                $remarksString = 'FE to FE Receive Process is Approved for '.$fe2_data['order_number'];
                $fe2_to_ad = tool_order_audit_data('tool_order',$fe2_tool_order_id,'tool_order',2,'',$newArr,$blockArr,$fe2OrderOldArr,$remarksString,'',array(),"tool_order",$fe2_tool_order_id,$fe2_data['country_id']);
                # Audit End for FE2 Order


                #audit Start for Fe1 Order
                $oldReturnOrderStatus = returnOrderStatusWithRtoId($rto_id);
                $fe1oldArr = array('current_stage' => getOpenOrderStatus($fe1_data));

                // updating the return approval
                $this->Common_model->update_data('return_order',array('return_approval'=>2,'remarks'=>$admin_remarks),array('return_order_id'=>$return_order_id));
                # Return order Audit
                $return_order_data = array('return_status'   => "Approved");
                $oldArr = array('return_status'   => $oldReturnOrderStatus);
                $blockArr = array('blockName'=> "Tool Order Return");
                $remarksString = "FE to FE By Courier return request ".$return_number." Approved by Admin to transfer from ".$fe1_data['order_number']." To ".$fe2_data['order_number'];
                $fe1ReturnAd = tool_order_audit_data('return_order',$return_order_id,'return_order',2,'',$return_order_data,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$fe1_tool_order_id,$fe1_data['country_id']);
            }
            
            approvefe2fetoolrequestbyadmin($fe1_tool_order_id,$fe2_tool_order_id,$return_number);            
            // deciding  8th stage
            decideMainOrder8thstage($fe1_tool_order_id);
            $type = ($return_type_id == 3)?"Hand":"Courier";
            $newArr = array('current_stage' => getOpenOrderStatus($fe1_data));
            $blockArr = array("blockArr"=> "Tool Order");
            $remarksString = "FE to FE By ".$type." return request ".$return_number." Approved by Admin to transfer from ".$fe1_data['order_number']." To ".$fe2_data['order_number'];
            $fe1_to_ad = tool_order_audit_data('tool_order',$fe1_tool_order_id,'tool_order',2,'',$newArr,$blockArr,$fe1oldArr,$remarksString,'',array(),"tool_order",$fe1_tool_order_id,$fe1_data['country_id']);

            if ($this->db->trans_status() === FALSE)
            { 
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'fe2_fe_approval'); exit();  
            }
            else
            {
                $this->db->trans_commit();                
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> FE1 :<strong>'.$fe1_data['order_number'].'</strong> to FE2 :<strong>'.$fe2_data['order_number'].'</strong> request With Return No :<strong>'.$return_number.'</strong> is Approved successfully!</div>');
                redirect(SITE_URL.'fe2_fe_approval'); exit();
            }        
        }
        else
        {
            $returnInfo = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
            $fe1_tool_order_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'tool_order_id');
            $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe1_tool_order_id));
            $return_number = $returnInfo['return_number'];
            // updating owned asset status with the same status when he acknowledged from shipment process
            $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
            $order_status_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id),'order_status_id');
            $fe1_tool_order_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'tool_order_id');

            // update order status history 
            $this->Common_model->update_data('order_status_history',array('end_time'=>date('Y-m-d H:i:s'),'status'=>3),array('order_status_id'=>$order_status_id));
            
            $data['owned_assets'] = $this->Order_m->getOwnedAssetsDetailsByOrderId($fe1_tool_order_id);

            // reinitiating the return process to the rejected assets
            $return_assets = $this->Fe2fe_m->get_return_initialted_assets($rto_id);
            // loping the return initiated assets and getting the status from shipment and updating in fe owned status

            // ship order status id
            $ship_order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$fe1_tool_order_id,'current_stage_id'=>6),'order_status_id');
            $owned_order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$fe1_tool_order_id,'current_stage_id'=>7),'order_status_id');

            $this->Common_model->update_data('tool_order',array('current_stage_id'=>7),array('tool_order_id'=>$fe1_tool_order_id));

            # Audit start  
            $oldReturnOrderStatus = returnOrderStatusWithRtoId($rto_id);
            
            $this->Common_model->update_data('return_order',array('return_approval'=>3,'remarks'=>$admin_remarks,'status'=>10),array('return_order_id'=>$return_order_id));

            # Return order Audit
            $return_order_data = array(
                'return_status'   => "Rejected",
                'remarks'         => $admin_remarks
            );
            $oldArr = array(
                'return_status'   => $oldReturnOrderStatus
            );
            $blockArr = array('blockName'=> "Tool Order Return");
            $remarksString = "FE to FE Transfer request for Return Number ".$return_number." rejected by Admin";
            $ad_id = tool_order_audit_data('return_order',$return_order_id,'return_order',2,'',$return_order_data,$blockArr,$oldArr,'','',array(),"tool_order",$fe1_tool_order_id,$fe1_data['country_id']);

            # End of Audit
            foreach ($return_assets as $key => $value)
            {
                $shp_condition = array('order_status_id'=>$ship_order_status_id,'ordered_asset_id'=>$value['ordered_asset_id']);
                $shipment_asset_condition = $this->Common_model->get_value('order_asset_history',$shp_condition,'status');
                // update owned order asset status
                $owned_condition = array('order_status_id'=>$owned_order_status_id,'ordered_asset_id'=>$value['ordered_asset_id']);
                $this->Common_model->update_data('order_asset_history',array('status'=>$shipment_asset_condition),$owned_condition);

                # Audit Start for transaction asset                
                $old_data_array  = array('transaction_status' => 'Return Initiated');
                $acd_array = array('transaction_status'  => 'Owned');                
                $blockArra = array('asset_id' => $value['asset_id']);
                $auditOahId = tool_order_audit_data("return_assets",$value['oah_id'],"order_asset_history",1,$ad_id,$acd_array,$blockArra,$old_data_array,$tool_order_arr['order_number'],'',array(),"tool_order",$fe1_tool_order_id,$fe1_data['country_id']);
                # End of Audit
            }

            # Tool Order Audit
            $oldArr = array('current_stage_id' => $tool_order_arr['current_stage_id']);
            $newArr = array(
                'current_stage_id' => 7,
                'return_number'    => $return_number
            );
            $remarksString = 'Rejected FE to FE Transfer request for the Return Order'.$return_number;
            $blockArr = array('blockName'=>"Tool Order");
            tool_order_audit_data('tool_order',$fe1_tool_order_id,"tool_order",2,'',$newArr,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$fe1_tool_order_id,$fe1_data['country_id']);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'fe2_fe_approval'); exit();
            }
            else
            {
                $this->db->trans_commit();            
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> FE1 :<strong>'.$fe1_data['order_number'].'</strong> to FE2 :<strong>'.$fe2_data['order_number'].'</strong> request With Return No :<strong>'.$return_number.'</strong> is Rejected successfully!</div>');
                redirect(SITE_URL.'fe2_fe_approval'); exit();
            }
        }  
    }

    // Fe2 Fe Receive
    public function fe2_fe_receive()
    {
        $task_access = page_access_check('fe2_fe_receive');
        $data['nestedView']['heading']="Ack from FE";
        $data['nestedView']['cur_page'] = 'fe2_fe_receive';
        $data['nestedView']['parent_page'] = 'fe2_fe_receive';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Ack from FE ';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack from FE','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=$this->input->post('fe2_fe_receive', TRUE);
        if($psearch!='') 
        {
            $searchParams=array(
                'order_number'             => validate_string($this->input->post('order_number', TRUE)),
                'order_delivery_type_id'   => validate_number($this->input->post('order_delivery_type_id', TRUE)),                               
                'request_date'             => validate_string($this->input->post('request_date', TRUE)),
                'return_date'              => validate_string($this->input->post('return_date', TRUE)),
                'ack_fe_sso_id'            => validate_string($this->input->post('ack_fe_sso_id', TRUE)),
                'ack_fe_country_id'        => validate_string($this->input->post('ack_fe_country_id', TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'order_number'           => $this->session->userdata('order_number'),
                    'order_delivery_type_id' => $this->session->userdata('order_delivery_type_id'),
                    'request_date'           => $this->session->userdata('request_date'),
                    'return_date'            => $this->session->userdata('return_date'),
                    'ack_fe_sso_id'          => $this->session->userdata('ack_fe_sso_id'),
                    'ack_fe_country_id'      => $this->session->userdata('ack_fe_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                    'order_number'           => '',
                    'order_delivery_type_id' => '',
                    'request_date'           => '',
                    'return_date'            => '',
                    'ack_fe_country_id'      => '',
                    'ack_fe_sso_id'          => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;
        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'fe2_fe_receive/';

        # Total Records
        $config['total_rows'] = $this->Fe2fe_m->fe2_fe_receive_order_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Fe2fe_m->fe2_fe_receive_order_results($current_offset, $config['per_page'], $searchParams,$task_access);
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access)); 
        $data['task_access'] = $task_access;

        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1)); 
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('fe2fe/fe2_fe_receive',$data);
    }

    public function download_ack_from_fe()
    {
        #page authentication
        $task_access = page_access_check('fe2_fe_receive');

        if(validate_number($this->input->post('download_ack_from_fe',TRUE))==1)
        {
            $searchParams=array(
            'order_number'             => validate_string($this->input->post('order_number', TRUE)),
            'order_delivery_type_id'   => validate_number($this->input->post('order_delivery_type_id', TRUE)),                               
            'request_date'             => validate_string($this->input->post('request_date', TRUE)),
            'return_date'              => validate_string($this->input->post('return_date', TRUE)),
            'ack_fe_sso_id'            => validate_string($this->input->post('ack_fe_sso_id', TRUE)),
            'ack_fe_country_id'        => validate_string($this->input->post('ack_fe_country_id', TRUE))
            );

            $result_data = $this->Fe2fe_m->download_ack_from_fe($searchParams,$task_access);

            $header = '';
            $data ='';
            $titles = array('S.No.','From Order','To Order','From SSO','To SSO','Return Info','Return Type','Order Type','Request Date','Return Date','Tool Number','Tool Description','Asset Number','Tool Availability','Asset Status','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            
            $data.='<tbody>';
            if(count($result_data)>0)
            {
                $sno = 1;
                foreach($result_data as $row)
                {
                    $assets_arr = $this->Fe2fe_m->get_assets_in_ack_from_fe($row['rto_id']);
                    $asset_count = count($assets_arr);
                    $req_date = full_indian_format($row['request_date']);
                    $ret_date = full_indian_format($row['return_date']);
                    $data.='<tr>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$sno++.'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['from_order'].'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['to_order'].'</td>';  
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['from_sso'].'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['to_sso'].'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['return_number'].'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['return_type'].'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['order_type'].'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$req_date.'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$ret_date.'</td>';                  
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['part_number'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['part_description'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['asset_number'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.get_asset_position($assets_arr[0]['asset_id']).'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['asset_status'].'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$row['country_name'].'</td>';
                    $data.='</tr>';

                    if($asset_count>1)
                    {
                        for($i=1;$i<$asset_count;$i++)
                        {
                            $data.='<tr>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['part_number'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['part_description'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['asset_number'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.get_asset_position($assets_arr[$i]['asset_id']).'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['asset_status'].'</td>';
                            $data.='</tr>';
                        }
                    }
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            //echo $data;exit;
            //echo $data;exit;
            $time = date("Y-m-d H:i:s");
            $xlFile='Ack_from_fe'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
    }

    public function fe2_fe_receive_details()
    {
        $rto_id = storm_decode($this->uri->segment(2));
        if($rto_id == '')
        {
            redirect(SITE_URL.'fe2_fe_receive'); exit();
        }
        $data['rto_id'] = $rto_id;

        $data['nestedView']['heading']="Ack from FE Details";
        $data['nestedView']['cur_page'] = "fe2_fe_receive_second";
        $data['nestedView']['parent_page'] = 'fe2_fe_receive';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/fe2_fe_receive.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'FE to FE Receive';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack from FE ','class'=>'','url'=>SITE_URL.'fe2_fe_receive');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack from FE Details ','class'=>'','url'=>'');
    

        $return_assets = $this->Fe2fe_m->get_return_initialted_assets($rto_id,1); // 1 is to get fe2 6th stage only
        $return_parts = array();
        foreach ($return_assets as $key => $value) 
        {
            $return_parts[$value['oah_id']]['part_number']   =  $value['part_number'];
            $return_parts[$value['oah_id']]['asset_number']  = $value['asset_number'];
            $return_parts[$value['oah_id']]['asset_number']  = $value['asset_number'];
            $return_parts[$value['oah_id']]['asset_id']      =  $value['asset_id'];
            $return_parts[$value['oah_id']]['asset_status']  =     $value['asset_status'];
            $return_parts[$value['oah_id']]['ordered_asset_id'] = $value['ordered_asset_id'];
            $return_parts[$value['oah_id']]['part_description'] = $value['part_description'];
            $return_parts[$value['oah_id']]['health_data'] = $this->Order_m->get_transit_asset_data($value['oah_id']);
        }
        $data['return_assets'] = $return_assets;
        $data['return_parts'] = $return_parts;
        $return_order_id = $this->Common_model->get_value('return_tool_order',array('rto_id'=>$rto_id),'return_order_id');
        $data['return_info'] = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));

        // Address Information

        $return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
        {
            $trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['wh_id']));
            $trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
        }
        else if($return_order['return_type_id'] == 4 || $return_order['return_type_id'] == 3)
        {
            $trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
            $sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
            $sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
            $trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];


        }
        $trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');


        $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
        $rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
        $asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
        
        $data['flg'] = 1;
        $data['rrow'] = $rrow;
        $data['trrow'] = $trrow;
        $data['asset_list'] = $asset_list;
        $data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));
        $this->load->view('fe2fe/fe2_fe_receive_details',$data);
    }

    public function insert_fe2_fe_receive()
    { 
        $_SESSION['fe2_fe_receive']  = 1;
        $rto_id = validate_number($this->input->post('rto_id',TRUE));
        $order_status_id = validate_number($this->input->post('order_status_id',TRUE));
        $oah_oa_health_id_part_id = $this->input->post('oah_oa_health_id_part_id',TRUE);
        $oah_id_arr = $this->input->post('oah_id',TRUE);
        $oa_health_id_arr = $this->input->post('oa_health_id',TRUE);
        $remarks_arr = $this->input->post('remarks',TRUE);
        $oah_condition_id = $this->input->post('oah_condition_id',TRUE);
        $assetAndOrderedAsset = $this->input->post('assetAndOrderedAsset',TRUE);    
        $orderedAssetAndAsset = $this->input->post('orderedAssetAndAsset',TRUE);

        $return_type_id = $this->input->post('return_type_id',TRUE);
        $return_order_id = $this->input->post('return_order_id',TRUE);
        $history_status_id = $this->input->post('history_status_id',TRUE);
        $tool_order_id = validate_number($this->input->post('fe2_tool_order_id',TRUE));

        $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $ackUser = $tool_order_arr['sso_id'];
        $ackUserCountry = $tool_order_arr['country_id'];

        // get FE1 data with current stage id 8
        $fe1_order_status_data = $this->Common_model->get_data_row('order_status_history',array('rto_id'=>$rto_id,'current_stage_id'=>8));
        $fe1_tool_order_id = $fe1_order_status_data['tool_order_id'];
        $fe1_order_status_id = $fe1_order_status_data['order_status_id'];
        $fe1_data = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe1_tool_order_id));
        $returnInfo = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        // 8th stage sent records
        $fe1_order_asset_history = $this->Common_model->get_data('order_asset_history',array('order_status_id'=>$fe1_order_status_id));

        #audit Start for FE1 return order
        $oldReturnOrderStatus = returnOrderStatusWithRtoId($rto_id,1); // to exclude the 10th return stage
        //echo $oldReturnOrderStatus;exit;

        # preparing health for asset id as key and value as received/not received for FE1 closed data
        $asset_condition = array();
        foreach ($oah_condition_id as $od_ast => $oa_condition)
        {
            $asset_condition[$orderedAssetAndAsset[$od_ast]] = $oa_condition;
        } 
        $this->db->trans_begin();           
        #updating the return order table to 10
        $rt_data = array(
            'status'              => 10,
            'received_date'       => date('Y-m-d'),
            'modified_by'         => $this->session->userdata('sso_id'),
            'modified_time'       => date('Y-m-d H:i:s')
        );
        $rt_where = array('return_order_id'=>$return_order_id);
        $this->Common_model->update_data('return_order',$rt_data,$rt_where);

        # FE2 Order Acknowledge Start
            # Audit Start
            $oldArr = array('current_stage_id'  => $tool_order_arr['current_stage_id']);
            $newArr = array('current_stage_id'  => 7);            
            $blockArr = array( "blockName" => "FE to FE Acknowledgement");
            $remarksString = 'Received the Order'.$tool_order_arr['order_number'].' From '.$fe1_data['order_number'];
            $fe2_ad = tool_order_audit_data("fe2_fe_ack",$tool_order_arr['tool_order_id'],"tool_order",2,'',$newArr,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$tool_order_arr['tool_order_id'],$tool_order_arr['country_id']);
            # Audit End

            $k=0; $j=0;  
            $part_condition_asset_part = array(); // for  FE1 closed transaction
            $fe2_unsuccessful = 0; // to decide FE2 current stage is unsuccessful
            $fe1_unsuccessful = 0; // to decide FE1 current stage is unsuccessful
            foreach ($oah_id_arr as $oah_id => $ordered_asset_id)
            {
                $asset_id = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$ordered_asset_id),'asset_id');
                $assetData = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                $asset_number = $assetData['asset_number'];

                //updating end time of previous stage
                if($k ==0 )
                {
                    $this->Common_model->update_data('order_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('tool_order_id'=>$tool_order_id,'current_stage_id'=>6));
                } 
                // please careful here from post it was 2 but to tell asset status in that transaction we are
                // inserting that status to 3 to say that is missed
                $replaced_status = ($oah_condition_id[$ordered_asset_id] == 2)?3:$oah_condition_id[$ordered_asset_id];
                $u_data = array('status'=>$replaced_status);
                
                $u_where = array('oah_id'=>$oah_id);
                $this->Common_model->update_data('order_asset_history',$u_data,$u_where);

                if($k == 0)
                {
                    // inserting new order status
                    $new_order_status_history_data = array(
                        'current_stage_id'   => 7,
                        'rto_id'             => $rto_id,  // to identify this acknowledgment done thr' fe to fe transfer
                        'tool_order_id'      => $tool_order_id,  
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 1
                        );
                    $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);  
                    $toUpdateData = array(
                        'current_stage_id'  => 7,
                        'modified_by'       => $this->session->userdata('sso_id'),
                        'modified_time'     => date('Y-m-d H:i:s')
                    );
                    $this->Common_model->update_data('tool_order',$toUpdateData,array('tool_order_id'=>$tool_order_id));
                }

                if($oah_condition_id[$ordered_asset_id] == 1)
                {
                    // inserting fe owned
                    // inserting the new record
                    $new_asset_history = array(
                            'ordered_asset_id' => $ordered_asset_id,
                            'order_status_id'  => $new_order_status_id,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'status'           => $oah_condition_id[$ordered_asset_id]
                        );
                    $new_oah_id = $this->Common_model->insert_data('order_asset_history',$new_asset_history);

                    # Audit Start for transaction asset     
                    $fe2OldAssetTransStatus = ($return_type_id == 3)?"FE1 Owned":"Shipped";
                    $old_data_array  = array('transaction_status' => $fe2OldAssetTransStatus);
                    $acd_array = array('transaction_status'  => 'Received');                
                    $blockArra = array('asset_id' => $asset_id);
                    $remarksString = "Acknowledgement done for Order ".$tool_order_arr['order_number'];
                    $auditOahId = tool_order_audit_data("fe2_fe_ack_assets",$new_oah_id,"order_asset_history",1,$fe2_ad,$acd_array,$blockArra,$old_data_array,$remarksString,"",array(),"tool_order",$tool_order_arr['tool_order_id'],$tool_order_arr['country_id']);
                    # Audit End

                    $order_asset_health_arr = array();
                    $defective_status = 0;
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    {   
                         // updating the transist health
                        $part_id = @$value[0];
                        $asset_condition_id = @$oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                        $remarks = @$remarks_arr[$oah_id][$oa_health_id][$part_id];
                        $health_data = array(
                            'asset_condition_id'  => $asset_condition_id,
                            'remarks'             => $remarks
                        );
                        $health_where = array('oa_health_id'=>$oa_health_id);
                        $this->Common_model->update_data('order_asset_health',$health_data,$health_where);

                        // inserting FE Owned
                        $order_asset_health = array(
                            'asset_condition_id' => $asset_condition_id,
                            'part_id'            => $part_id,
                            'oah_id'             => $new_oah_id,
                            'remarks'            => $remarks
                        );
                        $new_oahl_id = $this->Common_model->insert_data('order_asset_health',$order_asset_health);

                        // to tell overall asset status 
                        if($asset_condition_id != 1)
                        {
                            $defective_status = 2;
                            $unsuccessful = 1;
                        }
                        // preparing data for FE1
                        $order_asset_health_arr[$part_id] = $order_asset_health;
                    }

                    # Old Audit Asset and its position
                    $oldAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    $auditOldAssetData = auditAssetData($oldAssetData);
                    // echo "<pre>";print_r($auditOldAssetData);exit;

                    $part_condition_asset_part[$asset_id] = $order_asset_health_arr;

                    $approval = $assetData['approval_status'];
                    // if defective alert to admin
                    if(@$defective_status == 2)
                    {
                        // 0 for not waititng for approval
                        if($approval == 0)
                        {
                            // inserting into defective asset 
                            $defective_asset = array(
                                'asset_id'         => $asset_id,
                                'current_stage_id' => 6,
                                'trans_id'         => $tool_order_id,
                                'sso_id'           => $ackUser,
                                'created_by'       => $this->session->userdata('sso_id'),
                                'created_time'     => date('Y-m-d H:i:s'),
                                'type'             => 1,
                                'status'           => 2
                            );
                            $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);

                            foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                            { 
                                $part_id = $value[0];
                                $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                                $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];

                                $order_asset_health_data = array(
                                    'defective_asset_id'    => $defective_asset_id,
                                    'part_id'               => $part_id,
                                    'asset_condition_id'    => $asset_condition_id,
                                    'remarks'               => $remarks
                                );
                                $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                            } 
                            // send Notification To Admin
                            sendDefectiveOrMissedAssetMail($defective_asset_id,1,$ackUserCountry,2,$tool_order_id); 
                        }
                        // update owned asset status
                        $this->Common_model->update_data('order_asset_history',array('status'=>$defective_status),array('oah_id'=>$new_oah_id));
                    }
                    #if the asset is in our of calibration then don't chnge the asset status to FD and keep it as Out of calibration
                    $newAssetStatus = ($assetData['status']==8)?3:$assetData['status'];
                    # transaction Updating the Asset
                    $updateData = array(
                        'status'              => $newAssetStatus,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s') 
                        );
                    if($approval == 0 && @$defective_status ==2 )
                        $updateData['approval_status'] = 1;
                    $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                    # update insert asset position
                    $transUpdateInsertPosition = array(
                        'asset_id' => $asset_id,
                        'transit'  => 0,
                        'sso_id'   => $ackUser,
                        'status'   => $newAssetStatus
                    );
                    updateInsertAssetPosition($transUpdateInsertPosition);

                    # Update insert asset status history
                    $assetStatusHistoryArray = array(
                        'asset_id'          => $asset_id,
                        'created_by'        => $this->session->userdata('sso_id'),
                        'status'            => $newAssetStatus,
                        'current_stage_id'  => 7,
                        'tool_order_id'     => $tool_order_id
                    );
                    updateInsertAssetStatusHistory($assetStatusHistoryArray);

                    # Audit Asset start
                    $newAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $newAssetStatus
                    );
                    if($defective_status == 2 || $approval == 1) // old approval required or new approval required
                    {
                        $newAssetData['type'] = $defective_status;
                    }   
                    $assetRemarks = "Received for Order Number".$tool_order_arr['order_number'];
                    $auditNewAssetData = auditAssetData($newAssetData,2);
                    assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$assetRemarks,$fe2_ad,"tool_order",$tool_order_id,$tool_order_arr['country_id']);            
                    # Audit Asset End
                    $j++;
                }
                
                // inserting missed asset
                else
                {
                    $fe2_unsuccessful = 1;
                    $defective_status = 3;
                    // checking weather that asset is already waiting for admin approval
                    $approval = $assetData['approval_status'];

                    $fe2OldAssetTransStatus = ($return_type_id == 3)?"FE1 Owned":"Shipped";
                    # Audit Start for transaction asset                
                    $auditAssetOldData  = array(
                        'transaction_status'    => $fe2OldAssetTransStatus
                    );
                    $auditAssetNewData = array(
                        'transaction_status'    =>  "Not Received"
                    );
                    $blockArr = array("asset_id" => $asset_id);
                    $auditOahId = tool_order_audit_data("fe2_fe_ack_assets",$oah_id,"order_asset_history",1,$fe2_ad,$auditAssetNewData,$blockArr,$auditAssetOldData,$tool_order_arr['order_number'],'',array(),"tool_order",$tool_order_id,$tool_order_arr['country_id']);

                    # Audit Old Asset and its position
                    $oldAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    $auditOldAssetData = auditAssetData($oldAssetData);

                    $newAssetStatus = ($assetData['status']==8)?3:$assetData['status'];

                    // 0 for not waititng for approval
                    if($approval == 0)
                    {
                        $missed_asset_status = 2;
                        $defective_asset = array(
                            'asset_id'         => $asset_id,
                            'current_stage_id' => 6,
                            'trans_id'         => $tool_order_id,
                            'sso_id'           => $ackUser,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'type'             => 2,
                            'status'           => 3
                        );
                        $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                        foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                        { 
                            $part_id = $value[0];
                            $order_asset_health_data = array(
                                'defective_asset_id' => $defective_asset_id,
                                'part_id'            => $part_id,
                                'asset_condition_id' => 3,
                                'remarks'            => "Missed"
                                );
                            $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                        } 
                        // send Notification To Admin
                            sendDefectiveOrMissedAssetMail($defective_asset_id,2,$ackUserCountry,2,$tool_order_id); 
                    }
                    # transaction Updating the Asset
                    $updateData = array(
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s') 
                        );
                    if($approval == 0)
                        $updateData['approval_status'] = 1;
                    $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                    # Audit Asset and its position
                    $newAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    if($defective_status == 3 || $approval == 1) // old approval required or new approval required
                    {
                        $newAssetData['type'] = $defective_status;
                    }   
                    $assetRemarks = "Missed While Receiving Order Number".$tool_order_arr['order_number'];
                    $auditNewAssetData = auditAssetData($newAssetData,2);
                    assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$assetRemarks,$fe2_ad,"tool_order",$tool_order_arr['tool_order_id'],$tool_order_arr['country_id']);            
                    # Audit Asset End

                    $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$order_status_id)); 
                }
                $k++;
            }
            if(@$fe2_unsuccessful == 1 && $j > 0)
                $this->Common_model->update_data('order_status_history',array('status'=>$defective_status),array('order_status_id'=>$new_order_status_id));
            
            // all the acknowledged assets not received
            if($j == 0)
            {
                // Updating the Tool Order
                $updateDataOrder = array(
                    'current_stage_id'  => 10,
                    'status'            => 10,
                    'modified_by'       => $_SESSION['sso_id'],
                    'modified_time'     => date('Y-m-d H:i:s'),
                    'remarks'           => "As FE not received the any tools, Tool Order closed" // newly added this remarks on 25thDec18
                );
                $this->Common_model->update_data('tool_order',$updateDataOrder,array('tool_order_id'=>$tool_order_id));
                // inserting new order status
                $end_time = array('end_time'=>date('Y-m-d H:i:s'));
                $this->Common_model->update_data('order_status_history',$end_time,array('order_status_id'=>$new_order_status_id)); 

                $new_order_status_history_data = array(
                    'current_stage_id'   => 10,
                    'tool_order_id'      => $tool_order_id,  
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s'),
                    'status'             => 2
                    );
                $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);
                $auditUpdateData = array('current_stage_id'=>10);
                updatingAuditData($fe2_ad,$auditUpdateData);
            }
        # FE2 Order Ack End

        # FE1 Order Start
            // inserting 10th stage inside when first asset is defective
            $new_fe1_order_status_data = array(
                'tool_order_id'    => $fe1_tool_order_id,
                'current_stage_id' => 10, 
                'rto_id'           => $rto_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s')
            );
            $fe1_new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_fe1_order_status_data);

            # Return order Audit of FE1
            $return_order_data = array('return_status'   => returnOrderStatusWithRtoId($rto_id,1));
            $oldArr = array('return_status'   => $oldReturnOrderStatus);
            $blockArr = array('blockName'=> "Tool Order Return");
            $remarksString = "Your FE to FE Return Request ".$returnInfo['return_number']." with Order Number ".$fe1_data['order_number']."  acknowledged by FE2 Order Number ".$tool_order_arr['order_number'];
            $fe1ReturnAd = tool_order_audit_data('return_order_ack',$return_order_id,'return_order',2,'',$return_order_data,$blockArr,$oldArr,$remarksString,'',array(),"tool_order",$fe1_tool_order_id,$fe1_data['country_id']);

            
            foreach($fe1_order_asset_history as $key => $value)
            { 
                $asset_id = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$value['ordered_asset_id']),'asset_id');
                $assetData = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                    
                // 1 is for acknowledge
                $fe1_order_asset_health = $this->Common_model->get_data('order_asset_health',array('oah_id'=>$value['oah_id']));
                
                $fe_owned_oah_id = $this->Fe2fe_m->get_fe_owned_order_asset_id($fe1_tool_order_id,$value['ordered_asset_id']);
                // updating the FE owned tool to Send By FE to FE by Hand
                // As we were already doing at initiating the order then we no need to do it
                if($asset_condition[$asset_id] == 1)
                {
                    // inserting FE1 Closed order Data
                    $new_fe1_order_asset_data = array(
                        'order_status_id'    => $fe1_new_order_status_id,
                        'ordered_asset_id'   => $value['ordered_asset_id'],
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 1,
                        );
                    $new_fe1_oah_id = $this->Common_model->insert_data('order_asset_history',$new_fe1_order_asset_data);

                    $fe1ReturnAssetTransStatus = ($return_type_id ==3)?"FE1 Owned":"Shipped";
                    # Audit Start for transaction asset                
                    $old_data_array  = array('transaction_status' => $fe1ReturnAssetTransStatus);
                    $acd_array = array('transaction_status'  => 'FE2 Received');                
                    $blockArra = array('asset_id' => $asset_id);

                    $auditOahId = tool_order_audit_data("return_order_ack_assets",$new_fe1_oah_id,"order_asset_history",1,$fe1ReturnAd,$acd_array,$blockArra,$old_data_array,$fe1_data['order_number'],'',array(),"tool_order",$fe1_data['tool_order_id'],$fe1_data['country_id']);


                    // changing the FE2 order asset history to 3 because FE to FE by Hand Happend
                    $fe1_defective_part = 0;
                    // inserting closed order asset helath by getting old order helath
                    foreach ($fe1_order_asset_health as $key => $value2)
                    {
                        $asset_condition_id = $part_condition_asset_part[$asset_id][$value2['part_id']]['asset_condition_id'];
                        $remarks = $part_condition_asset_part[$asset_id][$value2['part_id']]['remarks'];
                        $new_fe1_order_asset_health = array(
                            'oah_id'                => $new_fe1_oah_id,
                            'part_id'               => $value2['part_id'],
                            'asset_condition_id'    => $asset_condition_id,
                            'remarks'               => $remarks,
                            );
                        $fe1_new_oahl_id = $this->Common_model->insert_data('order_asset_health',$new_fe1_order_asset_health);
                        if($asset_condition_id!=1)
                            $fe1_defective_part = 2;
                    }

                    if(@$fe1_defective_part == 2)
                    { 
                        // do not insert the defective asset entry for FE1 because the data is already inserted by FE2 so that removed the code                        
                        // update owned asset status
                        $this->Common_model->update_data('order_asset_history',array('status'=>$defective_status),array('oah_id'=>$new_fe1_oah_id));
                    }
                    # AS Asset position and status history already updated at FE2 not required here
                }
                // missing case
                else
                {
                    $fe1ReturnAssetTransStatus = ($return_type_id == 3)?"FE1 Owned":"Shipped";
                    # Audit Start for transaction asset                
                    $auditAssetOldData  = array(
                        'transaction_status'    => $fe1ReturnAssetTransStatus
                    );
                    $auditAssetNewData = array(
                        'transaction_status'    =>  "Not Received"
                    );
                    $blockArr = array("asset_id" => $asset_id);
                    $auditOahId = tool_order_audit_data("return_order_ack_assets",$value['oah_id'],"order_asset_history",2,$fe1ReturnAd,$auditAssetNewData,$blockArr,$auditAssetOldData,$fe1_data['order_number'],'',array(),"tool_order",$tool_order_id,$fe1_data['country_id']);

                    // inserting FE1 closed asset status history
                    $new_fe1_order_asset_data = array(
                        'order_status_id'    => $fe1_new_order_status_id,
                        'ordered_asset_id'   => $value['ordered_asset_id'],
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 3,
                        );
                    $new_fe1_oah_id = $this->Common_model->insert_data('order_asset_history',$new_fe1_order_asset_data);
                    // inserting asset helath by getting old order helath
                    foreach ($fe1_order_asset_health as $key => $value2)
                    {
                        $new_fe1_order_asset_health = array(
                            'oah_id'                => $new_fe1_oah_id,
                            'part_id'               => $value2['part_id'],
                            'asset_condition_id'    => 3,
                            'remarks'               => "Missed",
                            );
                        $this->Common_model->insert_data('order_asset_health',$new_fe1_order_asset_health);
                        // as all assets are missed no need to capture the audit it can ba tracked with old oah is ie. $value['oah_id '] and new once are all missed
                    }
                    // do not insert the defective asset entry for FE1 because the data is already inserted by FE2 so that removed the code
                }                    
            }

            $fe1AuditOldCurrentStage = array('current_stage_id'=> $fe1_data['current_stage_id']);

            $ninthAndtemthStatus = decideMainOrderStatus($fe1_tool_order_id);
            if($ninthAndtemthStatus[1] == 1)        
                $fe1AuditNewCurrentStage = 10;
            else if($ninthAndtemthStatus[2]==1)
                $fe1AuditNewCurrentStage = 9;
            if(isset($fe1AuditNewCurrentStage))
            {
                $finalArr = array('current_stage_id'=> $fe1AuditNewCurrentStage);
                $blockArr = array('blockName'=> "Tool Order");
                $remarksString = "FE to FE transfer request ".$returnInfo['return_number']." with ".$fe1_data['order_number']." Acknowledged by ".$tool_order_arr['order_number'];
                tool_order_audit_data("tool_order",$fe1_data['tool_order_id'],"tool_order",2,'',$finalArr,$blockArr,$fe1AuditOldCurrentStage,$remarksString,'',array(),"tool_order",$fe1_data['tool_order_id'],$fe1_data['country_id']);
            }
        # FE1 Order End
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> FE to FE acknowledge is successful!
            </div>');
        }
        redirect(SITE_URL.'fe2_fe_receive'); exit();
    }
    
    public function check_with_sso()
    {
        $session_tool = $_SESSION['tool_id'];
        $tool_arr = array();
        if(count($session_tool)>0)
        {
            foreach ($session_tool as $key => $value) 
            {
                $tool_arr[] = $key;
            }
        }
        else
        {
            $tool_arr = array(0);
        }
        $fe_users = $this->Fe2fe_m->get_check_with_sso($tool_arr);
        $string = '';
        if(count($fe_users)>0)
        {
            $string.='<option value="">- SSO -</option>';
            foreach ($fe_users as $ot) 
            {
                $string.='<option value="'.$ot['sso_id'].'">'.$ot['sso_id'].'-('.$ot['name'].')</option>';
            }
        }
        else
        {
            $string.="<option value=''>- No Data Found -</option>";
        }
        
        $data = array('result1'=>$string);
        echo json_encode($data);
    }
    public function ajax_toolsAvailabilityWithFE()
    {
        # Search Functionality
        $searchParams=array(
        'order_number'     => validate_string($this->input->post('order_number', TRUE)),
        'ser_sso_id'       => validate_string($this->input->post('ser_sso_id', TRUE)),
        'part_number'      => validate_string($this->input->post('part_number', TRUE)),
        'part_description' => validate_string($this->input->post('part_description', TRUE)),
        'asset_number'     => validate_string($this->input->post('asset_number',TRUE))
        );
        $this->session->set_userdata($searchParams);
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig2();
        $config['base_url'] = '#';

        $session_tool = $_SESSION['tool_id'];
        $tool_arr = array();
        if(count($session_tool)>0)
        {
            foreach ($session_tool as $key => $value) 
            {
                $tool_arr[] = $key;
            }
        }
        else
        {
            $tool_arr = array(0);
        }

        # Total Recordsss
        $config['total_rows'] = $this->Fe2fe_m->toolsAvailabilityWithFEtotal_num_rows($searchParams,$tool_arr);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        //echo $data['pagination_links'];exit;

        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $sn = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $orderResults = $this->Fe2fe_m->toolsAvailabilityWithFEresults($current_offset, $config['per_page'], $searchParams,$tool_arr);
        $asset_data = array();
        if(count($orderResults)>0)
        {
            foreach ($orderResults as $key => $value)
            {
               $asset_data[$value['asset_id']] = get_asset_data($value['asset_id']);
            }
        }
        # Additional data
        $result = '';
        $result.='<div class="col-sm-12 col-md-12">
                    <div class="table-responsive" style="margin-top: 10px;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>Order Number</strong></th>
                                    <th class="text-center"><strong>SSO</strong></th>
                                    <th class="text-center"><strong>Availability</strong></th>                    
                                    <th class="text-center"><strong>Asset Number </strong></th>
                                    <th class="text-center"><strong>Tool Number</strong></th>
                                    <th class="text-center"><strong>Tool Description</strong></th>
                                    
                                </tr>
                            </thead>
                            <tbody>';
        if(count(@$orderResults)>0)
        {   
            foreach(@$orderResults as $row)
            {
                $result.="<tr>";
                $result.='<td class="text-center"><img src="'.assets_url().'images/plus.png" class="toggle-details" title="Expand" ></td>';
                $result.='<td class="text-center"><input type="radio" class="fe1_order" name="fe1_order_chk" value="'.$row['order_number'].'"></td>';
                $result.='<td class="text-center">'.$sn++.'</td>';
                $result.='<td class="text-center">'.$row['order_number'].'</td>';
                $result.='<td class="text-center">'.$row['sso'].'</td>';
                $result.='<td class="text-center">'.$row['countryName'].'</td>';
                $result.='<td class="text-center">'.$row['asset_number'].'</td>';
                $result.='<td class="text-center">'.$row['part_number'].'</td>';
                $result.='<td class="text-center">'.$row['part_description'].'</td>';
                $result.='</tr>';
                if(count(@$asset_data[$row['asset_id']])>0)
                {

                $result.='<tr class="details" style="display:none">';
                $result.='<td></td>';
                $result.='<td  colspan="8">'; 
                $result.='<table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">';
                $result.='<thead>
                                <th class="text-center">Tool Number</th>
                                <th class="text-center">Tool Description</th>
                                <th class="text-center">Serial Number</th>
                                <th class="text-center">Tool Level </th>
                                <th class="text-center">Qty</th>
                            </thead>
                            <tbody>';
                            foreach(@$asset_data[$row['asset_id']] as $value)
                            {
                                $result.='<tr class="asset_row">';
                                $result.='<td class="text-center">'.$value['part_number'].'</td>';
                                $result.='<td class="text-center">'.$value['part_description'].'</td>';
                                $result.='<td class="text-center">'.$value['serial_number'].'</td>';
                                $result.='<td class="text-center">'.$value['part_level_name'].'</td>';
                                $result.='<td class="text-center">'.$value['parts_quantity'].'</td>';
                                    
                                $result.='</tr>';   
                            }
                            $result.='</tbody>
                            </table>
                        </td>
                    </tr>';
                }
                else
                {
                    $result.='<tr><td colspan="8" align="center"><span class="label label-primary">No Records Found</span></td></tr>';
                }
            }
        
        } 
        else 
        {
            $result.='<tr><td colspan="9" align="center"><span class="label label-primary">No Records Found</span></td></tr>';
        }
        $result.='</tbody></table></div></div>';
        $result.='<div class="row">';
        $result.='<div class="col-sm-12">';
        $result.='<div class="pull-left">';
        $result.='<div class="dataTables_info" role="status" aria-live="polite">'.@$data['pagermessage'].'</div>';
        $result.='</div>';
        $result.='<div class="pull-right">';
        $result.='<div class="dataTables_paginate paging_bootstrap_full_number" id="feToolSearchPagination">'.@$data['pagination_links'].'</div>';
        $result.='</div>';
        $result.='</div>';
        $result.='</div>';
                            
        $push_data = array('result'=>$result);
        echo json_encode($push_data);

    }

    public function ajax_toolsNeededByFE()
    {
        $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
        $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $returnUserCountry = $tool_order_arr['country_id'];

        # Search Functionality
        $searchParams=array(
        'order_number' => validate_string($this->input->post('order_number',TRUE)),
        'ser_sso_id'   => validate_string($this->input->post('ser_sso_id',TRUE)),
        'part_number'  => validate_string($this->input->post('part_number',TRUE)),
        'part_description' => validate_string($this->input->post('part_description', TRUE)),
        'country_id' => $returnUserCountry
        );
        $this->session->set_userdata($searchParams);
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig2();
        $config['base_url'] = '#';

        # Total Records
        $config['total_rows'] = $this->Fe2fe_m->toolsNeededByFEtotal_num_rows($searchParams);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $sn = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $orderResults = $this->Fe2fe_m->toolsNeededByFEresults($current_offset, $config['per_page'], $searchParams);
        if(count($orderResults)>0)
        {
            foreach ($orderResults as $key => $value)
            {
                $sub_data = array(); 
                $main_data = get_main_tool_information($value['tool_id']);
                $sub_data[] = $main_data; 
                $tool_sub_parts=get_tool_sub_parts($value['tool_id']);      
                if($tool_sub_parts[0]['tool_sub_id']!='')
                {
                    foreach ($tool_sub_parts as $key1 => $value2) 
                    {
                        $sub_data[] = get_main_tool_information($value2['tool_sub_id']);  
                    }
                }               
                $orderResults[$key]['tool_info'] = $sub_data;
            }
        }
        
        $result = '';
        $result.='<div class="col-sm-12 col-md-12">
                    <div class="table-responsive" style="margin-top: 10px;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>Order Number</strong></th>
                                    <th class="text-center"><strong>SSO</strong></th>   
                                    <th class="text-center"><strong>Tool Number</strong></th>
                                    <th class="text-center"><strong>Tool Description</strong></th>
                                    <th class="text-center"><strong>quantity </strong></th>
                                    <th class="text-center"><strong>Country</strong></th>
                                </tr>
                            </thead>
                            <tbody>';
        if(count(@$orderResults)>0)
        {   
            foreach(@$orderResults as $row)
            {
                $result.="<tr>";
                if(count(@$row['tool_info'])>0)
                {
                    $result.='<td class="text-center"><img src="'.assets_url().'images/plus.png" class="toggle-details" title="Expand" ></td>';
                }
                else
                {
                    $result.="<td></td>";
                }
                $result.='<td class="text-center"><input type="radio" class="fe1_order" name="fe1_order_chk" value="'.$row['order_number'].'"></td>';
                $result.='<td class="text-center">'.$sn++.'</td>';
                $result.='<td class="text-center">'.$row['order_number'].'</td>';
                $result.='<td class="text-center">'.$row['sso'].'</td>';
                $result.='<td class="text-center">'.$row['part_number'].'</td>';
                $result.='<td class="text-center">'.$row['part_description'].'</td>';
                $result.='<td class="text-center">'.$row['quantity'].'</td>';
                $result.='<td class="text-center">'.$row['countryName'].'</td>';
                $result.='</tr>';
                $i=0;
                if(count(@$row['tool_info'])>0)
                {

                    $result.='<tr class="details" style="display:none">';
                    $result.='<td></td>';
                    $result.='<td  colspan="8">'; 
                    $result.='<table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">';
                    $result.='<thead>
                                <th class="text-center">Tool Number</th>
                                <th class="text-center">Tool Description</th>
                                <th class="text-center">Tool Level </th>
                            </thead>
                            <tbody>';
                            foreach(@$row['tool_info'] as $value)
                            {
                                $result.='<tr class="asset_row">';
                                $result.='<td class="text-center">'.$value['part_number'].'</td>';
                                $result.='<td class="text-center">'.$value['part_description'].'</td>';
                                $result.='<td class="text-center">'.$value['part_level_name'].'</td>';
                                /*$pq = ($i==0)?1:$value['parts_quantity'];
                                $result.='<td class="text-center">'.$pq.'</td>';*/
                                    
                                $result.='</tr>'; 
                                $i++;  
                            }
                            $result.='</tbody>
                            </table>
                        </td>
                    </tr>';
                }
            }
        } 
        else 
        {
            $result.='<tr><td colspan="8" align="center"><span class="label label-primary">No Records Found</span></td></tr>';
        }
        $result.='</tbody></table></div></div>';
        $result.='<div class="row">';
        $result.='<div class="col-sm-12">';
        $result.='<div class="pull-left">';
        $result.='<div class="dataTables_info" role="status" aria-live="polite">'.@$data['pagermessage'].'</div>';
        $result.='</div>';
        $result.='<div class="pull-right">';
        $result.='<div class="dataTables_paginate paging_bootstrap_full_number" id="feToolSearchPagination">'.@$data['pagination_links'].'</div>';
        $result.='</div>';
        $result.='</div>';
        $result.='</div>';
                            
        $push_data = array('result'=>$result);
        echo json_encode($push_data);
    }

    // FE Owned Tools
    public function fe_transfer_request()
    {
        $data['nestedView']['heading']="FE Transfer Requests";
        $data['nestedView']['cur_page'] = 'fe_transfer_request';
        $data['nestedView']['parent_page'] = 'fe_transfer_request';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';

        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'FE Transfer Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Transfer Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('fe_transfer_request', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'from_sso'         => validate_string($this->input->post('from_sso', TRUE)),
            'to_sso'           => validate_string($this->input->post('to_sso', TRUE)),
            'from_return_date' => validate_string($this->input->post('from_return_date', TRUE)),
            'to_request_date'  => validate_string($this->input->post('to_request_date',TRUE)),
            'country_id'       => validate_number(@$this->input->post('country_id',TRUE))

                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'from_sso'         => $this->session->userdata('from_sso'),
                'to_sso'           => $this->session->userdata('to_sso'),
                'from_return_date' => $this->session->userdata('from_return_date'),
                'to_request_date'  => $this->session->userdata('to_request_date'),
                'country_id'       => $this->session->userdata('country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'from_sso'         => '',
                'to_sso'           => '',
                'from_return_date' => '',
                'to_request_date'  => '',
                'country_id'       => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'fe_transfer_request/';

        # Total Records
        $config['total_rows'] = $this->Fe2fe_m->fe_transfer_req_total_num_rows($searchParams,$task_access);
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['fe_transfer_result'] = $this->Fe2fe_m->fe_transfer_req_results($current_offset, $config['per_page'], $searchParams,$task_access);
        

        $asset_data = array();
        if(count($data['fe_transfer_result'])>0)
        {
            foreach ($data['fe_transfer_result'] as $key => $value)
            {
               $asset_data[$value['tool_order_id']] = $this->Fe2fe_m->get_asset_list($value['tool_order_id']);
            }
            $data['asset_data'] = $asset_data;
        }
        # Additional data
        $data['displayResults'] = 1;
        $data['users'] = get_fe_users($task_access);
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));

        $this->load->view('fe2fe/fe_transfer_request',$data);

    }
}