<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By koushik on 12th Nov 18 12:00PM

class Audit_trail extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Audit_model');
	}

	public function get_audit_page()
	{
		$sent_data = $this->uri->segment(2);
		if($sent_data=='')
		{
			redirect(SITEURL.'home'); exit();
		}

		$data['nestedView']['heading']="Audit Trail Report";
		$data['nestedView']['cur_page'] = 'audit_trail';
		$data['nestedView']['parent_page'] = 'audit_trail';

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Audit Trail Report';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Audit Trail Report','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchaudit', TRUE));
        if($psearch!='') 
        {
			$searchParams=array(
			'old_value'   => validate_string($this->input->post('old_value', TRUE)),
			'new_value'   => validate_string($this->input->post('new_value', TRUE)),
			'sso_user'    => validate_string($this->input->post('sso_user', TRUE)),
			'column_name' => validate_string($this->input->post('column_name', TRUE)),
			'trans_type'  => validate_string($this->input->post('trans_type', TRUE))
			);
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
			if($this->uri->segment(3)!='')
            {
				$searchParams=array(
				'old_value'   => $this->session->userdata('old_value'),
				'new_value'   => $this->session->userdata('new_value'),
				'sso_user'    => $this->session->userdata('sso_user'),
				'column_name' => $this->session->userdata('column_name'),
				'trans_type'  => $this->session->userdata('trans_type'),
				);
            }
            else 
            {
				$searchParams=array(
				'old_value'   => '',
				'new_value'   => '',
				'sso_user'    => '',
				'column_name' => '',
				'trans_type'  => ''
				);
                $this->session->set_userdata($searchParams);
            } 
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'get_audit_page/'.$sent_data.'/';
        # Total Records
        $config['total_rows'] = $this->Audit_model->audit_total_num_rows($searchParams,$sent_data);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $column_list = array();
        $auditResults = $this->Audit_model->audit_results($current_offset, $config['per_page'], $searchParams,$sent_data);
        foreach ($auditResults as $key => $value) 
        {
            $array = array($value['ad_id']=>$value['ad_id']);
            $ad_list = get_ad_list($array,$array);
        	foreach ($ad_list as $key1 => $value1)
        	{
        		$am_name = $this->Audit_model->get_audit_master_name($key1);
        		$result = $this->Audit_model->get_audit_child_data($key1,$searchParams);
                if(count($result)>0)
                {
                    $auditResults[$key]['acd'][$am_name]['final'][] = $result;
                    foreach ($result as $key2 => $value2) 
                    {
                        $column_list[] = $value2['column_name'];
                    }
                }
        	}
        }
        #additional data
        $data['display_name'] = get_display_name($sent_data);
        $data['auditResults'] = $auditResults;
        $data['sent_data'] = $sent_data;
        $data['columnList'] = array_unique($column_list);
        $data['transList'] = $this->Audit_model->get_trans_type();
        $this->load->view('audit/audit_view',$data);
	}

	public function download_audit()
	{
		$sent_data = $this->uri->segment(2);
		if($sent_data=='')
		{
			redirect(SITEURL.'home'); exit();
		}

		if($this->input->post('download_audit',TRUE)!='')
		{
			$searchParams=array(
			'old_value'   => validate_string($this->input->post('old_value', TRUE)),
			'new_value'   => validate_string($this->input->post('new_value', TRUE)),
			'sso_user'    => validate_string($this->input->post('sso_user', TRUE)),
			'column_name' => validate_string($this->input->post('column_name', TRUE)),
			'trans_type'  => validate_string($this->input->post('trans_type', TRUE))
			);
            $this->session->set_userdata($searchParams);

            $auditResults = $this->Audit_model->get_audit_trail_list($searchParams,$sent_data);
            foreach ($auditResults as $key => $value) 
	        {
	        	$result = $this->Common_model->get_data('audit_child_data',array('ad_id'=>$value['ad_id']));
	        	$rowspan = count($result);
	        	$auditResults[$key]['rowspan'] = $rowspan;
	        	$auditResults[$key]['acd'] = $result;
	        }
	        echo "<pre>"; print_r($auditResults); exit();
		}
	}

	public function get_audit_drop_down()
	{
		$sent_data = $this->input->post('sent_data',TRUE);
		
		#prepare column name drop down
		$columnList = $this->Audit_model->get_column_list($sent_data);
        $string1 = '';
        if(count($columnList)>0)
        {
            $string1.='<option value="">- Column Name -</option>';
            foreach ($columnList as $col) 
            {
                $string1.='<option value="'.$col['column_name'].'">'.$col['column_name'].'</option>';
            }
        }
        else
        {
            $string1.="<option value=''>- No Data Found -</option>";
        }
        
    	#prepare transaction type drop down
        $transList = $this->Audit_model->get_trans_type();
        $string2 = '';
        if(count($transList)>0)
        {
            $string2.='<option value="">- transaction Type -</option>';
            foreach ($transList as $ot) 
            {
                $string2.='<option value="'.$ot['trans_id'].'">'.$ot['name'].'</option>';
            }
        }
        else
        {
            $string2.="<option value=''>- No Data Found -</option>";
        }

        #header string
		$received = explode('~', $sent_data);
		$work_flow   = validate_string($received[2]);
		$header_name = $this->Common_model->get_value('audit_master',array('work_flow'=>$work_flow),'name');
		$string3 = "<h3>Audit Report (".$header_name.")</h3>";

        $data = array('result1'=>$string1,'result2'=>$string2,'result3'=>$string3);
        echo json_encode($data);
	}

	public function ajax_audit_report()
    {
        # Search Functionality
        $searchParams=array(
		'old_value'   => validate_string($this->input->post('old_value', TRUE)),
		'new_value'   => validate_string($this->input->post('new_value', TRUE)),
		'sso_user'    => validate_string($this->input->post('sso_user', TRUE)),
		'column_name' => validate_string($this->input->post('column_name', TRUE)),
		'trans_type'  => validate_string($this->input->post('trans_type', TRUE))
		);
        $this->session->set_userdata($searchParams);
        $data['searchParams'] = $searchParams;

        $sent_data = $this->input->post('sent_data',TRUE);
        $received = explode('~', $sent_data);
		$primary_key = validate_string(storm_decode($received[0]));
		$table_name  = validate_string($received[1]);
		$work_flow   = validate_string($received[2]);
		$details_of  = str_replace('%20', ' ', validate_string($received[3])); 

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig2();
        $config['base_url'] = '#';

        # Total Records
        $config['total_rows'] = $this->Audit_model->audit_total_num_rows($searchParams,$sent_data);
        $config['per_page'] = 5;
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        //echo $data['pagination_links'];exit;

        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $sn = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $auditResults = $this->Audit_model->audit_results($current_offset, $config['per_page'], $searchParams,$sent_data);

        foreach ($auditResults as $key => $value) 
        {
        	$result = $this->Common_model->get_data('audit_child_data',array('ad_id'=>$value['ad_id']));
        	$rowspan = count($result);
        	$auditResults[$key]['rowspan'] = $rowspan;
        	$auditResults[$key]['acd'] = $result;
        }

        # Additional data
        $result = '';
        $result.='<div class="col-sm-12 col-md-12">
                    <div class="table-responsive" style="margin-top: 10px;">
                    	<div class="col-md-12" align="left">
                    		<span>Details Of : <strong>'.@$details_of.'</strong></span>
                    	</div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center"><strong>S.No</strong></th>
	                                <th class="text-center"><strong>Work Flow</strong></th>
	                                <th class="text-center"><strong>Column</strong></th>
	                                <th class="text-center"><strong>Old Value</strong></th>
	                                <th class="text-center"><strong>New Value</strong></th>
	                                <th class="text-center"><strong>Type</strong></th>
	                                <th class="text-center"><strong>User</strong></th>
	                                <th class="text-center"><strong>Time</strong></th>
                                    
                                </tr>
                            </thead>
                            <tbody>';
        if(count(@$auditResults)>0)
        {   
            foreach(@$auditResults as $row)
            {
            	$crow = $row['acd'][0];
                $old_value = ($crow['old_value']!='')?$crow['old_value']:'--';
                $new_value = ($crow['new_value']!='')?$crow['new_value']:'--';
                $time = date('d-M-Y H:i A',strtotime($row['created_time']));

                $result.="<tr>";
                $result.='<td class="text-center" rowspan="'.$row['rowspan'].'">'.$sn++.'</td>';
                $result.='<td class="text-center" rowspan="'.$row['rowspan'].'">'.$row['work_flow_name'].'</td>';

				$result.='<td class="text-center">'.$crow['column_name'].'</td>';
				$result.='<td class="text-center">'.$old_value.'</td>';
				$result.='<td class="text-center">'.$new_value.'</td>';

                $result.='<td class="text-center" rowspan="'.$row['rowspan'].'">'.$row['transaction_type'].'</td>';
                $result.='<td class="text-center" rowspan="'.$row['rowspan'].'">'.$row['user_name'].'</td>';
                $result.='<td class="text-center" rowspan="'.$row['rowspan'].'">'.$time.'</td>';
                $result.='</tr>';

                $avail_records = count($row['acd']);
            	if($avail_records>1)
				{
					for($inc = 1;$inc<$avail_records;$inc++)
					{ 
						$crow = $row['acd'][$inc];
						$old_value = ($crow['old_value']!='')?$crow['old_value']:'--';
            			$new_value = ($crow['new_value']!='')?$crow['new_value']:'--';
						
						$result.='<tr>';
						$result.='<td class="text-center">'.$crow['column_name'].'</td>';
						$result.='<td class="text-center">'.$old_value.'</td>';
						$result.='<td class="text-center">'.$new_value.'</td>';
						$result.='</tr>';
					}
				}
            }
        } 
        else 
        {
            $result.='<tr><td colspan="8" align="center"><span class="label label-primary">No Records Found</span></td></tr>';
        }
        $result.='</tbody></table></div></div>';
        $result.='<div class="row">';
        $result.='<div class="col-sm-12">';
        $result.='<div class="pull-left">';
        $result.='<div class="dataTables_info" role="status" aria-live="polite">'.@$papermessage.'</div>';
        $result.='</div>';
        $result.='<div class="pull-right">';
        $result.='<div class="dataTables_paginate paging_bootstrap_full_number" id="auditPagination">'.@$data['pagination_links'].'</div>';
        $result.='</div>';
        $result.='</div>';
        $result.='</div>';
                            
        $push_data = array('result'=>$result);
        echo json_encode($push_data);
    }
}