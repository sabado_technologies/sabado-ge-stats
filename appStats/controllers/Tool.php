<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By Maruthi on 13th June 17 8:30PM

class Tool extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Tool_m');
    }

    public function get_tool_master_tools()
    {
        #page authentication
        $task_access = page_access_check('manage_tool');
        $part_name = validate_string($_REQUEST['string']);
        $country_id = validate_number($_REQUEST['part_type']);

        if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
            $country = $_SESSION['header_country_id'];
        }
        elseif($task_access == 1 || $task_access ==2)
        {
            $country = $_SESSION['s_country_id'];
        }
        else
        {
            $country = $country_id;
        }
        $data = get_tool_master_tools($part_name,$country);
        echo json_encode($data);

    }
    
    public function tool()
    {
       
        $data['nestedView']['heading']="Tool Master Data";
        $data['nestedView']['cur_page'] = 'manage_tool';
        $data['nestedView']['parent_page'] = 'manage_tool';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/tool.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Tool Master Data';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Master Data','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtool', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'part_number'       => validate_string($this->input->post('part_number', TRUE)),
            'part_description'  => validate_string($this->input->post('part_description', TRUE)),
            'tool_code'         => validate_string($this->input->post('tool_code', TRUE)),
            'part_level_id'     => validate_number($this->input->post('part_level_id', TRUE)),
            'country_id'        => validate_number(@$this->input->post('country_id',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'part_number'      => $this->session->userdata('part_number'),
                'part_description' => $this->session->userdata('part_description'),
                'part_level_id'    => $this->session->userdata('part_level_id'),
                'tool_code'        => $this->session->userdata('tool_code'),
                'country_id'       => $this->session->userdata('country_id')
                );
            }
            else {
                $searchParams=array(
                'part_number'       => '',
                'part_description'  => '',
                'part_level_id'     => '',
                'tool_code'         => '',
                'country_id'        => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'tool/';
        # Total Records
        $config['total_rows'] = $this->Tool_m->tool_total_num_rows($searchParams,$task_access);
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $toolResults = $this->Tool_m->tool_results($current_offset, $config['per_page'], $searchParams,$task_access);
        foreach ($toolResults as $key => $value) 
        {
            $toolResults[$key]['tool_parts'] = $this->Tool_m->get_tool_parts($value['tool_id']);
        }
    
        # Additional data
        $data['part_level_list'] = $this->Common_model->get_data('part_level',array()); 
        $data['task_access'] = $task_access;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['toolResults'] = $toolResults; 
        $data['displayResults'] = 1;
        $data['current_offset'] = $current_offset;

        $this->load->view('tool/tool_view',$data);
    }

    public function add_tool()
    {
        $data['nestedView']['heading']="Add New Tool";
        $data['nestedView']['cur_page'] = 'check_manage_tool';
        $data['nestedView']['parent_page'] = 'manage_tool';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']= "Select Country";
                $data['nestedView']['cur_page'] = 'check_manage_user';
                $data['nestedView']['parent_page'] = 'manage_tool';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Master Data','class'=>'','url'=>SITE_URL.'tool');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_tool';
                $data['cancel'] = SITE_URL.'tool';
                $this->load->view('user/intermediate_page',$data); 
            }
        }
        if(@$country_id !='')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/tool.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New Tool';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Master Data','class'=>'','url'=>SITE_URL.'tool');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Tool','class'=>'active','url'=>'');

            # Additional data
            $data['flg'] = 1;
            $data['asset_level_details'] = $this->Common_model->get_data('asset_level');
            $data['tool_type_details'] = $this->Common_model->get_data('tool_type',array('status'=>1));
            $data['supplier_detials'] = $this->Common_model->get_data('supplier',array('sup_type_id'=>1,'task_access'=>$task_access,'country_id'=>$country_id));
            $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
            $data['cal_supplier_details'] = $this->Common_model->get_data('supplier',array('calibration'=>1,'task_access'=>$task_access,'country_id'=>$country_id));
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['eq_model'] = $this->Common_model->get_dropdown('equipment_model', 'eq_model_id', 'name', array('status'=>1));
            $data['part_details'] = $this->Common_model->get_data('tool',array('asset_level_id'=>3,'status'=>1,'country_id'=>$country_id));
            $data['tool_code'] = $this->Tool_m->get_latest_tool_code($country_id);
            $data['form_action'] = SITE_URL.'insert_tool';
            $data['displayResults'] = 0;
            $data['country_id'] = $country_id;
            $this->load->view('tool/tool_view',$data);
        }   
    }

    public function insert_tool()
    {
        #page authentication
        $task_access = page_access_check('manage_tool');

        if(validate_string($this->input->post('submitTool',TRUE)) != '')
        {
            $part_number = validate_string($this->input->post('part_number',TRUE));  
            $country_id = validate_number($this->input->post('country_id',TRUE));
            $tool_code = validate_number($this->input->post('tool_code',TRUE));
            // check for part number unique
            $qry = "SELECT * FROM tool WHERE part_number = '".$part_number."' AND country_id = '".$country_id."'" ;
            $available = $this->Common_model-> get_no_of_rows($qry);
            // check for tool code unique
            $qry2 = "SELECT * FROM tool WHERE tool_code = '".$tool_code."' AND country_id = '".$country_id."' ";
            $available2 = $this->Common_model-> get_no_of_rows($qry2);

            if($available > 0 || $available2  > 0)
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> given : '.$part_number.' or '.$tool_code.' already exist! please check.</div>'); 
                redirect(SITE_URL.'tool'); exit();

            }  
            $asset_level_id = validate_number($this->input->post('asset_level_id',TRUE));
            if($asset_level_id == 3)
            {
                $part_level_number = 2;
            }
            else
            {
                $part_level_number = 1;
            }
            $cal_type_id = validate_number($this->input->post('cal_type_id',TRUE));
            if($cal_type_id==1)
            {
                $cal_supplier_id = validate_number($this->input->post('cal_supplier_id',TRUE));
                if($cal_supplier_id == '')
                {
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> given : Please Give Calibration Supplier if Tool required Calibration.</div>'); 
                     redirect(SITE_URL.'tool'); exit(); 
                }
            }
            else
            {
                $cal_supplier_id = NULL;
            }
            //gowri 11-april
            $allow_order_for1year=$this->input->post('remarks2',TRUE);
            if($allow_order_for1year==1)
            {
                $remarks2=1;
                $allow_order=1;
            }
            else
            {
                $remarks2=NULL;
                $allow_order=2;
            }
            //end
            $length = validate_number($this->input->post('length',TRUE));
            if($length == '') $length = NULL;
            $breadth = validate_number($this->input->post('breadth',TRUE));
            if($breadth == '') $breadth = NULL;
            $height = validate_number($this->input->post('height',TRUE));
            if($height == '') $height = NULL;
            $insert_data = array(
                'part_number'      => $part_number,
                'tool_code'        => $tool_code,
                'part_level_id'    => $part_level_number,
                'model'            => validate_string($this->input->post('model',TRUE)),
                'hsn_code'         => validate_string($this->input->post('hsn_code',TRUE)),
                'cal_supplier_id'  => $cal_supplier_id,
                'part_description' => validate_string($this->input->post('part_description',TRUE)),
                'supplier_id'      => validate_number($this->input->post('supplier_id',TRUE)),
                'kit'              => validate_number($this->input->post('kit_id',TRUE)),
                'asset_type_id'    => validate_number($this->input->post('asset_type_id',TRUE)),
                'tool_type_id'     => validate_number($this->input->post('tool_type_id',TRUE)),
                'manufacturer'     => validate_string($this->input->post('manufacturer',TRUE)),
                'weight'           => validate_string($this->input->post('weight',TRUE)),
                'length'           => $length,
                'breadth'          => $breadth,
                'height'           => $height,
                'gst_percent'      => validate_string($this->input->post('gst_percent',TRUE)),
                'modality_id'      => validate_number($this->input->post('modality_id',TRUE)),
                'asset_level_id'   => validate_number($this->input->post('asset_level_id',TRUE)),
                'cal_type_id'      => $cal_type_id,
                'remarks'          => validate_string($this->input->post('remarks',TRUE)),
                'remarks2'         => $remarks2,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1,
                'country_id'       => $country_id
            );
            $this->db->trans_begin();
            $tool_id = $this->Common_model->insert_data('tool',$insert_data); 

            #audit data
            $remarks = "Tool:".$part_number." Creaton";
            $ad_id = audit_data('tool_master',$tool_id,'tool',1,'',$insert_data,array('tool_id'=>$tool_id),array(),$remarks,'',array(),'','',$country_id);

            $eq_model = $this->input->post('eq_model',TRUE);
            if(count($eq_model)>0)
            {
                foreach ($eq_model as $key => $eq_model_id) 
                {
                    $eq_model_tool = array('tool_id'=>$tool_id,'eq_model_id'=>$eq_model_id);
                    $this->Common_model->insert_data('equipment_model_tool',$eq_model_tool);

                    #audit trail
                    $insert_data = array('eq_model_id'=>$eq_model_id);
                    audit_data('equipment_model_tool',$eq_model_id,'equipment_model_tool',1,$ad_id,$insert_data,array('eq_model_id'=>$eq_model_id),array(),'','',array(),'','',$country_id);
                } 
            }
            
            $part_arr = $this->input->post('part_arr',TRUE);
            $quantity = $this->input->post('quantity',TRUE);
            foreach ($part_arr as $key => $value) 
            {
                if($value !='' && $quantity[$key]!='')
                {
                    $tool_part = array('tool_main_id'=>$tool_id,'tool_sub_id'=>$value,'quantity'=>$quantity[$key]);
                    $this->Common_model->insert_data('tool_part',$tool_part);

                    #audit trail
                    $insert_data = array(
                        'tool_sub_id' => $value,
                        'quantity'    => $quantity[$key]
                    );
                    audit_data('tool_part',$value,'tool_part',1,$ad_id,$insert_data,array('tool_id'=>$value),array(),'','',array(),'','',$country_id);
                }
                
            }

            if($this->db->trans_status === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'tool'); exit();
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> Tool : '.$tool_code.' has been Created successfully! </div>');
                redirect(SITE_URL.'tool'); exit();

            }
        } 
        else
        {
            redirect(SITE_URL.'tool'); exit();
        }     
    }

    public function edit_tool()
    {
       
        $tool_id=@storm_decode($this->uri->segment(2));
        if($tool_id=='')
        {
            redirect(SITE_URL.'tool');
            exit;
        }        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Tool";
        $data['nestedView']['cur_page'] = 'check_manage_tool';
        $data['nestedView']['parent_page'] = 'manage_tool';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/tool.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Edit Tool';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Master Data','class'=>'','url'=>'tool');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Tool','class'=>'active','url'=>'');

        $data['eq_model_selected'] = array_column($this->Common_model->get_data('equipment_model_tool', array('tool_id'=>$tool_id,'status'=>1),array('eq_model_id'),"", '1'), 'eq_model_id');
        
        $data['eq_model'] = $this->Common_model->get_dropdown('equipment_model', 'eq_model_id', 'name', array('status'=>1));        
        
        # Additional data
        $data['flg'] = 2;
        $row = $this->Common_model->get_data_row('tool',array('tool_id'=>$tool_id));
        $country_id = $row['country_id'];
        $data['asset_level_details'] = $this->Common_model->get_data('asset_level');
        $data['tool_type_details'] = $this->Common_model->get_data('tool_type',array('status'=>1));
        $data['supplier_detials'] = $this->Common_model->get_data('supplier',array('sup_type_id'=>1,'task_access'=>$task_access,'country_id'=>$country_id));
        $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
        $data['cal_supplier_details'] = $this->Common_model->get_data('supplier',array('calibration'=>1,'task_access'=>$task_access,'country_id'=>$country_id));
        $data['part_details'] = $this->Common_model->get_data('tool',array('asset_level_id'=>3,'status'=>1,'tool_id !='=>$tool_id));
        $data['country_id'] = $country_id;
        $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
        $data['form_action'] = SITE_URL.'update_tool';
        $data['displayResults'] = 0;

        # Data
        $data['lrow'] = $row;
        $data['component_details'] = $this->Tool_m->get_active_sub_component($tool_id);
        $data['count_c'] = count($data['component_details']);
        $this->load->view('tool/tool_view',$data);
    }

    public function update_tool()
    {
        #page authentication
        $task_access = page_access_check('manage_tool');

        $tool_id=validate_number($this->input->post('tool_id',TRUE));        
        if($tool_id==''){
            redirect(SITE_URL.'tool');
            exit;
        }
        $trow = $this->Common_model->get_data_row('tool',array('tool_id'=>$tool_id));
        $part_number = validate_string($this->input->post('part_number',TRUE));  
        $tool_code = validate_string($this->input->post('tool_code',TRUE));  
        $country_id = $this->Common_model->get_value('tool',array('tool_id'=>$tool_id),'country_id');

        $tool_part_data = array('tool_id'=>$tool_id,'part_number'=>$part_number,'country_id'=>$country_id);
        $tool_code_data = array('tool_id'=>$tool_id,'tool_code'=>$tool_code,'country_id'=>$country_id);
        
        $available = $this->Tool_m->check_part_number_availability($tool_part_data);
        $available2 = $this->Tool_m->check_tool_code_availability($tool_code_data);
        if($available > 0 || $available2  > 0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> given part number : '.$part_number.' or tool code '.$tool_code.' already exist! please check. </div>'); 
            redirect(SITE_URL.'edit_tool/'.@storm_encode($tool_id));  

        } 
        $old_asset_level_id = $this->Common_model->get_data('tool',array('tool_id'=>$tool_id),'asset_level_id');
        $asset_level_id = validate_number($this->input->post('asset_level_id',TRUE));

        if($old_asset_level_id == 3)
        {
            if($asset_level_id != $old_asset_level_id)
            {
                $check_sub_tool = $this->Common_model->get_data('tool_part',array('tool_sub_id'=>$tool_id));
                if(count($check_sub_tool)>0)
                {
                   $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> Asset Level Cant Change for this tool</div>'); 
                    redirect(SITE_URL.'tool');  exit();
                }
            }
        }
        
        if($asset_level_id == 3)
        {
            $part_level_number = 2;
        }
        else
        {
            $part_level_number = 1;
        }
        $cal_type_id = validate_number($this->input->post('cal_type_id',TRUE));
        if($cal_type_id==1)
        {
            $cal_supplier_id = validate_number($this->input->post('cal_supplier_id',TRUE));
            if($cal_supplier_id == '')
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> given : Please Give Calibration Supplier if Tool required Calibration.</div>'); 
                 redirect(SITE_URL.'tool'); exit(); 
            }
        }
        else
        {
            $cal_supplier_id = NULL;
        }
        //gowri 11-april
        $allow_order_for1year=$this->input->post('remarks2',TRUE);
        if($allow_order_for1year==1)
            {
                $remarks2=1;
                $allow_order=1;
            }
            else
            {
                $remarks2=NULL;
                $allow_order=2;
            }
            //end
        $length = validate_number($this->input->post('length',TRUE));
        if($length == '') $length = NULL;
        $breadth = validate_number($this->input->post('breadth',TRUE));
        if($breadth == '') $breadth = NULL;
        $height = validate_number($this->input->post('height',TRUE));
        if($height == '') $height = NULL;

        $update_data = array(
            'part_number'      => $part_number,
            'tool_code'        => $tool_code,
            'part_level_id'    => $part_level_number,
            'model'            => validate_string($this->input->post('model',TRUE)),  
            'hsn_code'         => validate_string($this->input->post('hsn_code',TRUE)),
            'cal_supplier_id'=> $cal_supplier_id,
            'part_description' => validate_string($this->input->post('part_description',TRUE)),
            'supplier_id'      => validate_number($this->input->post('supplier_id',TRUE)),
            'kit'              => validate_number($this->input->post('kit_id',TRUE)),
            'weight'           => validate_string($this->input->post('weight',TRUE)),
            'asset_type_id'    => validate_number($this->input->post('asset_type_id',TRUE)),
            'tool_type_id'     => validate_number($this->input->post('tool_type_id',TRUE)),
            'manufacturer'     => validate_string($this->input->post('manufacturer',TRUE)),
            'length'           => $length,
            'breadth'          => $breadth,
            'height'           => $height,
            'gst_percent'      => validate_number($this->input->post('gst_percent',TRUE)),
            'modality_id'      => validate_number($this->input->post('modality_id',TRUE)),
            'asset_level_id'   => validate_string($this->input->post('asset_level_id',TRUE)),
            'cal_type_id'      => $cal_type_id,
            'remarks'          => validate_string($this->input->post('remarks',TRUE)),
            //gowri 11-april
            'remarks2'          =>$remarks2,
            //end
            'modified_by'      => $this->session->userdata('sso_id'),
            'modified_time'    => date('Y-m-d H:i:s'),
            'status'           => 1,
            'country_id'       => $country_id
        );
        $update_where = array('tool_id' => $tool_id);
        $this->db->trans_begin();
        $this->Common_model->update_data('tool',$update_data,$update_where); 

        #audit data
        unset($update_data['modified_by'],$update_data['modified_time'],$update_data['status']);
        $final_arr = array_diff_assoc($update_data, $trow);

        if(count($final_arr)>0)
        {
            $a_remarks = "Tool:".$part_number." Modification";
            $ad_id = audit_data('tool_master',$tool_id,'tool',2,'',$final_arr,array('tool_id'=>$tool_id),$trow,$a_remarks,'',array(),'','',$country_id);
        }
        

        #old eq model arr
        $old_eq_arr = array(0);
        $old_equip = $this->Common_model->get_data('equipment_model_tool',array('tool_id'=>$tool_id,'status'=>1));
        if(count($old_equip)>0)
        {
            $old_eq_arr = array();
            foreach ($old_equip as $key => $value) {
                $old_eq_arr[] = $value['eq_model_id'];
            }
        }
        #update eqmodel from post value
        $eq_model = $this->input->post('eq_model',TRUE);
        if(count($eq_model)>0 && $asset_level_id !=3)
        {
            #new eq
            foreach ($eq_model as $key => $eq_model_id) 
            {
                if(!in_array($eq_model_id,$old_eq_arr))
                {
                    $this->Tool_m->tool_eq_model_insert_update($tool_id,$eq_model_id);

                    #audit data
                    if(!isset($ad_id))
                    {
                        $ad_id = audit_data('tool_master',$tool_id,'tool',2,'',array(),array('tool_id'=>$tool_id),array(),'','',array(),'','',$country_id);
                    }
                    $eq_data = array('eq_model_id'=>$eq_model_id);
                    audit_data('equipment_model_tool',$eq_model_id,'equipment_model_tool',1,$ad_id,$eq_data,array('eq_model_id'=>$eq_model_id),array(),'','',array(),'','',$country_id);
                }
            } 

            #removed eq
            foreach ($old_eq_arr as $key => $value) 
            {
                if($value!=0)
                {
                    if(!in_array($value, $eq_model))
                    {
                        $update_eq = array('status'=>2);
                        $update_eq_where = array('tool_id'=>$tool_id,'eq_model_id'=>$value);
                        $this->Common_model->update_data('equipment_model_tool',$update_eq,$update_eq_where);

                        #audit data
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('tool_master',$tool_id,'tool',2,'',array(),array('tool_id'=>$tool_id),array(),'','',array(),'','',$country_id);
                        }
                        $old_data = array('eq_model_id'=>$value);
                        audit_data('equipment_model_tool',$value,'equipment_model_tool',3,$ad_id,array(),array('eq_model_id'=>$value),$old_data,'','',array(),'','',$country_id);
                    }
                }
                
            }
        }
        else//removed
        {
            foreach ($old_eq_arr as $key => $value) 
            {
                if($value!=0)
                {
                    $update_eq = array('status'=>2);
                    $update_eq_where = array('tool_id'=>$tool_id,'eq_model_id'=>$value);
                    $this->Common_model->update_data('equipment_model_tool',$update_eq,$update_eq_where);

                    #audit data
                    if(!isset($ad_id))
                    {
                        $ad_id = audit_data('tool_master',$tool_id,'tool',2,'',array(),array('tool_id'=>$tool_id),array(),'','',array(),'','',$country_id);
                    }
                    $old_data = array('eq_model_id'=>$value);
                    audit_data('equipment_model_tool',$value,'equipment_model_tool',3,$ad_id,array(),array('eq_model_id'=>$value),$old_data,'','',array(),'','',$country_id);
                }
            }
        }

        #old tool part arr
        $old_tp_arr = array(0);
        $old_tool_part = $this->Common_model->get_data('tool_part',array('tool_main_id'=>$tool_id,'status'=>1));
        if(count($old_tool_part)>0)
        {
            $old_tp_arr = array();
            foreach ($old_tool_part as $key => $value) {
                $old_tp_arr[] = $value['tool_sub_id'];
            }
        }

        #update tool part from post value
        $part_arr = $this->input->post('part_arr',TRUE);
        $quantity = $this->input->post('quantity',TRUE);
        if($asset_level_id !=3)
        {
            #for new insertion and updation
            foreach ($part_arr as $key => $value) 
            {
                if($value !='' && $quantity[$key]!='')
                {
                    if(in_array($value, $old_tp_arr))//update
                    {
                        $old_qty = $this->Common_model->get_value('tool_part',array('tool_main_id'=>$tool_id,'tool_sub_id'=>$value,'status'=>1),'quantity');
                        if($old_qty != $quantity[$key])
                        {
                            $this->Tool_m->tool_part_insert_update($tool_id,$value,$quantity[$key]);

                            #audit for updation
                            if(!isset($ad_id))
                            {
                                $ad_id = audit_data('tool_master',$tool_id,'tool',2,'',array(),array('tool_id'=>$tool_id),array(),'','',array(),'','',$country_id);
                            }
                            $old_data = array('quantity'=>$old_qty,'tool_sub_id'=>$value);
                            $new_data = array('quantity'=>$quantity[$key],'tool_sub_id'=>$value);
                            audit_data('tool_part',$value,'tool_part',2,$ad_id,$new_data,array('tool_id'=>$value),$old_data,'','',array(),'','',$country_id);
                        }
                    }
                    else//insert
                    {
                        $this->Tool_m->tool_part_insert_update($tool_id,$value,$quantity[$key]);

                        #audit for updation
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('tool_master',$tool_id,'tool',2,'',array(),array('tool_id'=>$tool_id),array(),'','',array(),'','',$country_id);
                        }
                        $new_data = array('quantity'=>$quantity[$key],'tool_sub_id'=>$value);
                        audit_data('tool_part',$value,'tool_part',1,$ad_id,$new_data,array('tool_id'=>$value),array(),'','',array(),'','',$country_id);
                    }
                }
            }

            #for removed part list
            foreach ($old_tp_arr as $key => $value) 
            {
                if($value !='' && count($part_arr)>0 && $value!=0)
                {
                    if(!in_array($value, $part_arr))//removed
                    {
                        $old_qty = $this->Common_model->get_value('tool_part',array('tool_main_id'=>$tool_id,'tool_sub_id'=>$value,'status'=>1),'quantity');
                        $update_tp = array('status'=>2);
                        $update_tp_where = array('tool_main_id'=>$tool_id,'tool_sub_id'=>$value);
                        $this->Common_model->update_data('tool_part',$update_tp,$update_tp_where);

                        #audit for updation
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('tool_master',$tool_id,'tool',2,'',array(),array('tool_id'=>$tool_id),array(),'','',array(),'','',$country_id);
                        }
                        $old_data = array('quantity'=>$old_qty,'tool_sub_id'=>$value);
                        audit_data('tool_part',$value,'tool_part',3,$ad_id,array(),array('tool_id'=>$value),$old_data,'','',array(),'','',$country_id);
                    }
                }
            } 
        }
        else
        {
            #for removed part list
            foreach ($old_tp_arr as $key => $value) 
            {
                if($value !='' && $value!=0)
                {
                    $old_qty = $this->Common_model->get_value('tool_part',array('tool_main_id'=>$tool_id,'tool_sub_id'=>$value,'status'=>1),'quantity');
                    $update_tp = array('status'=>2);
                    $update_tp_where = array('tool_main_id'=>$tool_id,'tool_sub_id'=>$value);
                    $this->Common_model->update_data('tool_part',$update_tp,$update_tp_where);

                    #audit for updation
                    if(!isset($ad_id))
                    {
                        $ad_id = audit_data('tool_master',$tool_id,'tool',2,'',array(),array('tool_id'=>$tool_id),array(),'','',array(),'','',$country_id);
                    }
                    $old_data = array('quantity'=>$old_qty,'tool_sub_id'=>$value);
                    audit_data('tool_part',$value,'tool_part',3,$ad_id,array(),array('tool_id'=>$value),$old_data,'','',array(),'','',$country_id);
                }
            }
        }
   
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.
            </div>'); 
            redirect(SITE_URL.'edit_tool/'.@storm_encode($tool_id));  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Tool :<strong>'.$part_number.'</strong> Details has been updated successfully! </div>');
            redirect(SITE_URL.'tool');  
        }
    }

    public function deactivate_tool($encoded_id)
    { 
        #page authentication
        $task_access = page_access_check('manage_tool');

        $tool_id=@storm_decode($encoded_id);
        if($tool_id==''){
            redirect(SITE_URL.'tool');
            exit;
        }
        $where = array('tool_id' => $tool_id);
        //deactivating tool
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('tool',$data_arr, $where);

        $country_id = $this->Common_model->get_value('tool',array('tool_id'=>$tool_id),'country_id');
        $old_data = array('tool_status'=>'Active');
        $new_data = array('tool_status'=>'Deactivated');
        audit_data('tool_master',$tool_id,'tool',2,'',$new_data,array('tool_id'=>$tool_id),$old_data,'Tool  has Deactivated','',array(),'','',$country_id);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Tool has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'tool');
    }

    // checking part number uniqueness
    public function check_part_number_availability()
    {
        $part_number = validate_string($this->input->post('part_number',TRUE));
        $tool_id = validate_number($this->input->post('tool_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('part_number'=>$part_number,'tool_id'=>$tool_id,'country_id'=>$country_id);
        $result = $this->Tool_m->check_part_number_availability($data);
        echo $result;
    }
    // checking tool code uniqueness
    public function check_tool_code_availability()
    {
        $tool_code = $this->input->post('tool_code',TRUE);
        $tool_id = $this->input->post('tool_id',TRUE);
        $country_id = $this->input->post('country_id',TRUE);
        $data = array('tool_code'=>$tool_code,'tool_id'=>$tool_id,'country_id'=>$country_id);
        $result = $this->Tool_m->check_tool_code_availability($data);
        echo $result;
    }

    public function bulkupload_tool()
    {
        $data['failed_upload'] = @$this->uri->segment(2);
        $data['nestedView']['heading']="Bulk Upload Tool";
        $data['nestedView']['cur_page'] = 'manage_tool';
        $data['nestedView']['parent_page'] = 'manage_tool';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Bulk Upload Tool';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Tool','class'=>'','url'=>SITE_URL.'tool');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Bulk Upload Tool','class'=>'active','url'=>'');
        if($data['failed_upload'] == 10)
        {
            $data['form_action'] = SITE_URL.'submit_toolupload';
        }     
        else
        {
            $data['form_action'] = SITE_URL.'wge_submit_toolupload';
        }
        $data['supplier'] = $this->Common_model->get_data('supplier',array('status'=>1,'sup_type_id'=>1,'task_access'=>$task_access)); 
        $data['cal_supplier'] = $this->Common_model->get_data('supplier',array('status'=>1,'calibration'=>1,'task_access'=>$task_access));   
        $data['eq_model_results']= $this->Common_model->get_data('equipment_model',array('status'=>1));
        $this->load->view('tool/tool_upload_view',$data);
    }


    public function missed_bulkupload_tool_view()
    {
        #page authentication
        $task_access = page_access_check('manage_tool');

          $upload_id = @$this->uri->segment(2);
          if($upload_id == '')
          {
            redirect(SITE_URL.'tool'); exit();
          }

          $data['nestedView']['heading']="Missed List of Tool Bulk Upload";
          $data['nestedView']['cur_page'] = 'manage_tool';
          $data['nestedView']['parent_page'] = 'manage_tool';

          # include files
          $data['nestedView']['js_includes'] = array();
          $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
          $data['nestedView']['css_includes'] = array();

          # Breadcrumbs
          $data['nestedView']['breadCrumbTite'] = 'Missed List of Tool Bulk Upload';
          $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
          $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Tool','class'=>'','url'=>SITE_URL.'tool');
          $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Missed Tool Bulk Upload','class'=>'active','url'=>'');
      
           $qry ="SELECT MAX(upload_id) as upload_id FROM upload WHERE status = 2 AND type = 1";
            $res = $this->db->query($qry);
            $results = $res->row_array();
            $upload_id = $results['upload_id'];
            //echo $upload_id;exit;
            
          # Default Records Per Page - always 10
          /* pagination start */
          $config = get_paginationConfig();
          $config['base_url'] = SITE_URL.'missed_bulkupload_tool_view/'.$upload_id.'/';
          # Total Records
          $config['total_rows'] = $this->Tool_m->missed_tool_total_num_rows($upload_id);

          $config['per_page'] = getDefaultPerPageRecords();
          $data['total_rows'] = $config['total_rows'];
          $this->pagination->initialize($config);
          $data['pagination_links'] = $this->pagination->create_links();
          $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
          if ($data['pagination_links'] != '') {
              $data['last'] = $this->pagination->cur_page * $config['per_page'];
              if ($data['last'] > $data['total_rows']) {
                  $data['last'] = $data['total_rows'];
              }
              $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
          }
          $data['sn'] = $current_offset + 1;
          /* pagination end */

          # Loading the data array to send to View
          $data['missedResults'] = $this->Tool_m->missed_tool_results($current_offset, $config['per_page'], $upload_id);
          //echo '<pre>'; print_r($data['missedResults']);exit;
          #additional Data 
          $data['upload_id'] = $upload_id; 
          $this->load->view('tool/missed_tool_upload_view',$data);
    }
    public function wge_submit_toolupload()
    {   
        #page authentication
        $task_access = page_access_check('manage_tool');

        if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
        { 
            $this->db->trans_begin();
            $modality = $this->Common_model->get_data('modality',array('status'=>1));
            $modality_arr =array();
            foreach ($modality as $key => $value) 
            {
                $modality_code =trim(strtolower($value['modality_code']));
                $modality_id = $value['modality_id'];
                $modality_arr[$modality_code] = $modality_id;
            }

            $tool_type_data = $this->Common_model->get_data('tool_type',array('status'=>1));
            $tool_type_arr =array();
            foreach ($tool_type_data as $key => $value) 
            {
                $tool_type_name =trim(strtolower($value['name']));
                $tool_type_data_id = $value['tool_type_id'];
                $tool_type_arr[$tool_type_name] = $tool_type_data_id;
            }

            $eq_model_data = $this->Common_model->get_data('equipment_model',array('status'=>1));
            $eq_model_data_arr =array();
            foreach ($eq_model_data as $key => $value) 
            { 
                $eq_model_name =trim(strtolower($value['name']));
                $eq_model_data_id = $value['eq_model_id'];
                $eq_model_data_arr[$eq_model_name] = $eq_model_data_id;
            } 

            $filename=$_FILES["uploadCsv"]["tmp_name"];            
            if($_FILES["uploadCsv"]["size"] > 0)
            {
                $file = fopen($filename, "r");
                $i=0;    
                $j=0;
                
                $upload_data = array(
                            'type'         => 1, // for Tools Upload
                            'status'       => 1,                                
                            'created_by'   => $this->session->userdata('sso_id'),
                            'file_name'    => $_FILES["uploadCsv"]["name"],
                            'created_time' => date('Y-m-d H:i:s')
                         );
                $upload_id = $this->Common_model->insert_data('upload',$upload_data);
                $insert = 0;
                $successfully_inserted = 0;
                $unsuccessfully_inserted = 0; $i=0;$j=0;$asset_level_st= '';
                while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                { 
                    if($j==0) 
                    {
                     $j++;
                     continue;
                    }  
                    $asset_level = validate_string($emapData[0]);
                    $modality_code = validate_string(trim(strtolower($emapData[1])));
                    $tool_type = validate_string(trim(strtolower($emapData[2])));                  
                    $kit = validate_string(trim($emapData[3]));
                    $asset_type = validate_string(trim(strtolower($emapData[4])));
                    $part_number = validate_string(trim($emapData[5]));
                    $part_description = validate_string($emapData[6]);
                    $tool_code = validate_string((int)$emapData[7]);
                    $part_level = validate_string(trim($emapData[8]));
                    $quantity = validate_string($emapData[9]);
                    $length = validate_string($emapData[10]);
                    $breadth = validate_string($emapData[11]);
                    $height = validate_string($emapData[12]);
                    $weight = validate_string($emapData[13]);
                    $calibration = validate_string(trim(strtolower($emapData[14])));
                    $cal_supplier_code = validate_string($emapData[15]);
                    $supplier_code = validate_string($emapData[16]);
                    $manufacturer = validate_string($emapData[17]);
                    $eq_model = validate_string(trim(strtolower($emapData[18])));                    
                    $hsn_code = validate_string(trim($emapData[19]));
                    $model  = validate_string($emapData[20]);                    
                    $gst_percent = validate_string($emapData[21]);
                    $country = validate_string($emapData[22]);
                    $country_id = '';
                    if($country!='')
                    {
                        $country_id = $this->Common_model->get_value('location',array('name'=>$country,'level_id'=>2),'location_id');
                    }

                    #convert toolcode to 4 digit
                    if($tool_code!='')
                    {
                        $tc_length = strlen($tool_code);
                        if($tc_length<4)
                        {
                            $tool_code = get_tool_code($tool_code);
                        }
                    }
                    
                    if($asset_level !='')
                    {   
                        if($asset_level !='' && $modality_code !='' && $tool_type!='' && $kit!='' && $asset_type  !='' && $part_number   !='' && $part_description!='' && $tool_code   !='' && $part_level!='' && $calibration!='' && $supplier_code!=''&& $manufacturer !='' && $hsn_code!='' && $model !=''&& 
                            $gst_percent !='' && $country!='' && $country_id !='')                           
                        {
                            if($insert != 0)
                            {   
                                $return_val = wge_bulk_upload_tools($tool_failed_data,
                                $success_part_array,$failed_part_array,$records_status,$upload_id,
                                $country_id);

                                if($return_val == 0){ $successfully_inserted++; }
                                else{ $unsuccessfully_inserted++; }
                            } 

                            $asset_remarks_string = '';
                            $error = 0;

                            #asset Level Validation
                            $asset_level_id = get_asset_level_id($asset_level);
                            if($asset_level_id =='')
                            {
                              $error = 1;
                              $asset_remarks_string.='Asset Level '.@$asset_level.' Not Exist. ';
                            }

                            $error_helper = error_helper(@$asset_level_id,@$asset_remarks_string,@$modality_code,@$tool_type,@$kit,@$asset_type,@$part_level,@$part_number,@$tool_code,@$calibration,@$supplier_code,@$cal_supplier_code,@$eq_model,@$modality_arr,@$tool_type_arr,@$eq_model_data_arr,@$error,@$country_id);

                            $error               = $error_helper[0];
                            $asset_remarks_string= $error_helper[1];
                            $tool_type_id        = $error_helper[2];
                            $kit_id              = $error_helper[3];
                            $asset_type_id       = $error_helper[4];
                            $part_level_id       = $error_helper[5];
                            $cal_type_id         = $error_helper[6];
                            $loop_eq_model_arr   = $error_helper[7];
                            $modality_id         = $error_helper[8];
                            $supplier_id         = $error_helper[9]; 
                            $cal_supplier_id     = $error_helper[10];
                            $existed_tool_id     = $error_helper[11];
                            $failed_part_array = array();
                            $success_part_array = array();
                            $pno_arr = array();
                           
                            $tool_failed_data = array(                                    
                                'asset_level_id' => @$asset_level,
                                'upload_id'      => @$upload_id                               
                            );

                            $failed_part_data = array(
                            'modality'         => strtoupper(@$modality_code),
                            'tool_type'        => strtoupper(@$tool_type),
                            'kit'              => @$kit,
                            'asset_type'       => strtoupper(@$asset_type),
                            'part_number'      => @$part_number,
                            'part_description' => @$part_description,
                            'tool_code'        => @$tool_code,
                            'part_level'       => @$part_level,
                            'length'           => @$length,
                            'breadth'          => @$breadth,
                            'height'           => @$height,
                            'weight'           => @$weight,
                            'calibration'      => @$calibration,        
                            'supplier'         => @$supplier_code,
                            'manufacturer'     => @$manufacturer,
                            'quantity'         => @$quantity,     
                            'eq_model'         => @$eq_model,
                            'cal_supplier_id'  => @$cal_supplier_code,
                            'hsn_code'         => @$hsn_code,
                            'model'            => @$model,
                            'gst_percent'      => @$gst_percent,
                            'tool_part_remarks'=> @$asset_remarks_string,
                            'country_name'     => @$country);
                            
                            $failed_part_array[] = @$failed_part_data; 
                           
                            if($error == 1)
                            {
                                $records_status = 2;
                            }
                            else// asset success transaction to insert into asset master table 
                            {
                                $pno_arr[] = @$part_number;
                                $success_part_data = array(     
                                    'part_number'      => @$part_number,
                                    'tool_code'        => @$tool_code,
                                    'part_description' => @$part_description,
                                    'supplier_id'      => @$supplier_id,
                                    'kit'              => @$kit_id,
                                    'asset_type_id'    => @$asset_type_id,
                                    'tool_type_id'     => @$tool_type_id,
                                    'manufacturer'     => @$manufacturer,
                                    'length'           => @$length,
                                    'breadth'          => @$breadth,
                                    'height'           => @$height,
                                    'weight'           => @$weight,
                                    'modality_id'      => @$modality_id,
                                    'asset_level_id'   => @$asset_level_id,
                                    'cal_type_id'      => @$cal_type_id,
                                    'part_level_id'    => @$part_level_id,
                                    'quantity'         => @$quantity,
                                    'eq_model_ids_arr' => @$loop_eq_model_arr,
                                    'cal_supplier_id'  => @$cal_supplier_id,
                                    'hsn_code'         => @$hsn_code,
                                    'model'            => @$model,
                                    'gst_percent'      => @$gst_percent,
                                    'country_id'       => @$country_id,
                                    'created_by'       => $this->session->userdata('sso_id'),
                                    'created_time'     => date('Y-m-d H:i:s')
                                );
                                $records_status = 1;
                                $success_part_array[] = @$success_part_data;
                            }                  
                        } 
                        else// checking what are the L0 items missed 
                        { 
                            $asset_remarks_string = '';                            
                            if(@$asset_level=='')      $asset_remarks_string.= 'Asset Level,';
                            if(@$modality_code=='')    $asset_remarks_string.= 'Modality Code,';
                            if(@$tool_type=='')        $asset_remarks_string.= 'Tool Type, ';
                            if(@$kit == '')            $asset_remarks_string.= 'Kit Status,';
                            if(@$asset_type=='')       $asset_remarks_string.= 'Asset Type,';
                            if(@$part_number=='')      $asset_remarks_string.= 'Part Number, ';
                            if(@$part_description=='') $asset_remarks_string.= 'Part Description,';
                            if(@$tool_code=='')        $asset_remarks_string.= 'Tool Code,';
                            if(@$part_level=='')       $asset_remarks_string.= 'Sub Inventory, ';
                            if(@$calibration=='')      $asset_remarks_string.= 'Calibration, ';
                            if(@$supplier_code=='')    $asset_remarks_string.= 'Supplier ID,';
                            if(@$manufacturer=='')     $asset_remarks_string.= 'Manufacturer,';
                            if(@$hsn_code=='')         $asset_remarks_string.= 'HSN Code, ';
                            if(@$model=='')            $asset_remarks_string.= 'Model, ';
                            if(@$gst_percent=='')      $asset_remarks_string.= 'GST Percentage,  ';    
                            if(@$country == '')        $asset_remarks_string.='Country Name ';  
                            $asset_remarks_string.=' Are Missed';

                            $tool_failed_data = array(              
                                'asset_level_id' => @$asset_level,
                                'upload_id'      => @$upload_id
                            );

                            $failed_part_data = array(
                                'modality'         => strtoupper($modality_code),
                                'tool_type'        => strtoupper($tool_type),
                                'kit'              => @$kit,
                                'asset_type'       => strtoupper($asset_type),
                                'part_number'      => @$part_number,
                                'part_description' => @$part_description,
                                'tool_code'        => @$tool_code,
                                'part_level'       => @$part_level,
                                'length'           => @$length,
                                'breadth'          => @$breadth,
                                'height'           => @$height,
                                'calibration'      => @$calibration,
                                'supplier'         => @$supplier,
                                'manufacturer'     => @$manufacturer,
                                'quantity'         => @$quantity,
                                'eq_model'         => @$eq_model,
                                'cal_supplier_id'  => @$cal_supplier_id,
                                'hsn_code'         => @$hsn_code,
                                'model'            => @$model,
                                'gst_percent'      => @$gst_percent,
                                'tool_part_remarks'=> @$asset_remarks_string,
                                'country_name'     => @$country
                            );
                            $success_part_array[] = @$success_part_data;
                            $failed_part_array[] = @$failed_part_data;   
                            $records_status =2;                           
                        } 
                        $insert++;               
                    } // check empty row end
                    else// inserting L1 items
                    {
                        $asset_level_id ='';
                        $error = 0;
                        $asset_remarks_string = '';

                        $error_helper = error_helper(@$asset_level_id,@$asset_remarks_string,
                            @$modality_code,@$tool_type,@$kit,@$asset_type,@$part_level,
                            @$part_number,@$tool_code,@$calibration,@$supplier_code,
                            @$cal_supplier_code,@$eq_model,@$modality_arr,@$tool_type_arr,
                            @$eq_model_data_arr,@$error,@$country_id);

                        $error               = $error_helper[0];
                        $asset_remarks_string= $error_helper[1];
                        $tool_type_id        = $error_helper[2];
                        $kit_id              = $error_helper[3];
                        $asset_type_id       = $error_helper[4];
                        $part_level_id       = $error_helper[5];
                        $cal_type_id         = $error_helper[6];
                        $loop_eq_model_arr   = $error_helper[7];
                        $modality_id         = $error_helper[8];
                        $supplier_id         = $error_helper[9]; 
                        $cal_supplier_id     = $error_helper[10];
                        $existed_tool_id     = $error_helper[11]; 
                            
                        if(in_array($part_number, $pno_arr))
                        {
                        $error = 1;
                        $asset_remarks_string.='Duplicate Part Numbers Found '.@$part_number.'. Please give unique part numbers In Kit';     
                        }
                        $pno_arr[] =  $part_number;

                        $failed_part_data = array(
                            'modality'         => strtoupper(@$modality_code),
                            'tool_type'        => strtoupper(@$tool_type),
                            'kit'              => @$kit,
                            'asset_type'       => strtoupper(@$asset_type),
                            'part_number'      => @$part_number,
                            'part_description' => @$part_description,
                            'tool_code'        => @$tool_code,
                            'part_level'       => @$part_level,
                            'length'           => @$length,
                            'breadth'          => @$breadth,
                            'height'           => @$height,
                            'weight'           => @$weight,
                            'calibration'      => @$calibration,            
                            'supplier'         => @$supplier_code,
                            'manufacturer'     => @$manufacturer,
                            'quantity'         => @$quantity,
                            'eq_model'         => @$eq_model,
                            'cal_supplier_id'  => @$cal_supplier_code,
                            'hsn_code'         => @$hsn_code,
                            'model'            => @$model,
                            'gst_percent'      => @$gst_percent,
                            'tool_part_remarks'=> @$asset_remarks_string,
                            'country_name'     => @$country
                        );
                        if($error == 1 )
                        {
                            $records_status =2;
                        }    
                        else// asset succes transaction to insert into asset master table
                        {
                            $success_part_data = array(     
                            'part_number'      => @$part_number,
                            'tool_code'        => @$tool_code,
                            'part_description' => @$part_description,
                            'supplier_id'      => @$supplier_id,
                            'kit'              => @$kit_id,
                            'asset_type_id'    => @$asset_type_id,
                            'tool_type_id'     => @$tool_type_id,
                            'manufacturer'     => @$manufacturer,
                            'length'           => @$length,
                            'breadth'          => @$breadth,
                            'height'           => @$height,
                            'weight'           => @$weight,
                            'modality_id'      => @$modality_id,
                            'asset_level_id'   => 3,
                            'cal_type_id'      => @$cal_type_id,
                            'part_level_id'    => @$part_level_id,
                            'quantity'         => @$quantity,
                            'eq_model_ids_arr' => @$loop_eq_model_arr,
                            'cal_supplier_id'  => @$cal_supplier_id,
                            'hsn_code'         => @$hsn_code,
                            'model'            => @$model,
                            'gst_percent'      => @$gst_percent,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'created_time'     => date('Y-m-d H:i:s'),
                            'country_id'       => @$country_id
                            );
                        }

                        $success_part_array[] = @$success_part_data;
                        $failed_part_array[] = @$failed_part_data;
                    }
                }

                if($insert!=0)
                {
                    $return_val = wge_bulk_upload_tools($tool_failed_data,$success_part_array,
                        $failed_part_array,$records_status,$upload_id,$country_id);
                    if($return_val == 0){ $successfully_inserted++; }
                    else{ $unsuccessfully_inserted++; }
                }
                fclose($file);
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div><strong>Error!
                    </strong> something went wrong! please check.</div>'); 
                    redirect(SITE_URL.'tool'); exit();
                }
                else
                {
                    $this->db->trans_commit();
                    if(@$successfully_inserted != 0 && @$unsuccessfully_inserted !=0)
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> '.@$successfully_inserted.' Tools added Successfully. and '.$unsuccessfully_inserted.' Tools not added. Please Check Below For Missed Uplods !
                        </div>');
                        redirect(SITE_URL.'missed_bulkupload_tool_view/1'); exit();  
                    }
                    if(@$successfully_inserted==0)
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Error!</strong> '.$unsuccessfully_inserted.' Tools are not Uploaded. Please Check Below Missed Uploads !
                        </div>');
                        redirect(SITE_URL.'missed_bulkupload_tool_view/1');  exit();
                    }
                    else
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> '.$successfully_inserted.' Tools has been Added successful!
                        </div>');
                    }
                    redirect(SITE_URL.'tool');  exit();
                } 
            }
            else
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>Error!</strong> Please Upload Matched CSV Data. </div>');  
                redirect(SITE_URL.'tool'); exit();
            }
        }
    }

    public function download_missed_tool_uploads()
    { 
        #page authentication
        $task_access = page_access_check('manage_tool');

        $qry ="SELECT MAX(upload_id) as upload_id FROM upload WHERE status = 2 AND type =1 ";
        $res = $this->db->query($qry);
        $results = $res->row_array();
        $upload_id = $results['upload_id'];
        $tools = $this->Tool_m->download_missed_tool_results($upload_id);
        $header = '';
        $data ='';
        $titles = array('Asset Level',' Modality','Tool Classification','Kit','Asset Type','Part Number',
            'Part Description','Tool Code','Part Level','Quantity','Length','Breadth','Height','Weight','Calibration Required or Not','Calibration Supplier Code',
            'Supplier Code','Manufacturer','Equipment Model','HSN Code','Model','GST%','Country','Remarks');
        $data = '<table border="1">';
        $data.='<thead>';
        $data.='<tr>';

        foreach ( $titles as $title)
        {
            $data.= '<th align="center">'.$title.'</th>';
        }
        $data.='</tr>';
        $data.='</thead>';
        $data.='<tbody>';
         
        if(count($tools)>0)
        {
            $p = 0;
           $first_upload_tool_id = $tools[0]['upload_tool_id']; 
            foreach($tools as $row)
            {
                $next_upload_tool_id = $row['upload_tool_id'];
                $data.='<tr>';
                if($p== 0) 
                { 
                    $data.='<td align="center">'.$row['asset_level_id'].'</td>';                   
                    
                }
                else
                {
                    if($first_upload_tool_id == $next_upload_tool_id)
                    {
                        $data.='<td align="center"></td>';                   
                        
                    }
                    else
                    {

                        $data.='<td align="center">'.$row['asset_level_id'].'</td>';                   
                        
                    }
                }
                $data.='<td align="center">'.$row['modality'].'</td>';                
                $data.='<td align="center">'.$row['tool_type'].'</td>';                   
                $data.='<td align="center">'.$row['kit'].'</td>';                   
                $data.='<td align="center">'.$row['asset_type'].'</td>';
                $data.='<td align="center">'.$row['part_number'].'</td>';                
                $data.='<td align="center">'.$row['part_description'].'</td>';                   
                $data.='<td align="center">'.$row['tool_code'].'</td>';                   
                $data.='<td align="center">'.$row['part_level'].'</td>';
                $data.='<td align="center">'.$row['quantity'].'</td>';               
                $data.='<td align="center">'.$row['length'].'</td>';                   
                $data.='<td align="center">'.$row['breadth'].'</td>';                   
                $data.='<td align="center">'.$row['height'].'</td>';   
                $data.='<td align="center">'.$row['weight'].'</td>';
                $data.='<td align="center">'.$row['calibration'].'</td>';                
                $data.='<td align="center">'.$row['cal_supplier_id'].'</td>';                   
                $data.='<td align="center">'.$row['supplier'].'</td>';                   
                $data.='<td align="center">'.$row['manufacturer'].'</td>';                   
                
                
                $data.='<td align="center">'.$row['eq_model'].'</td>';                
                $data.='<td align="center">'.$row['hsn_code'].'</td>';
                $data.='<td align="center">'.$row['model'].'</td>';
                $data.='<td align="center">'.$row['gst_percent'].'</td>'; 
                $data.='<td align="center">'.$row['country_name'].'</td>';               
                $data.='<td align="center">'.$row['tool_part_remarks'].'</td>';
                
                $data.='</tr>'; 
                $p++;
                $first_upload_tool_id = $next_upload_tool_id;
                               
            }
        }
        else
        {
            $data.='<tr><td colspan="'.(count($titles)+1).'" align="center">No Results Found</td></tr>';
        }
        $data.='</tbody>';
        $data.='</table>';
        $time = date("Ymdhis");
        $xlFile='Tools'.$time.'.xls'; 
        header("Content-type: application/x-msdownload"); 
        # replace excelfile.xls with whatever you want the filename to default to
        header("Content-Disposition: attachment; filename=".$xlFile."");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $data;       
    }


    public function activate_tool($encoded_id)
    {
        #page authentication
        $task_access = page_access_check('manage_tool');

        $tool_id=@storm_decode($encoded_id);
        
        if($tool_id==''){
            redirect(SITE_URL.'tool_id');
            exit;
        }
        $where = array('tool_id' => $tool_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('tool',$data_arr, $where);
        $country_id = $this->Common_model->get_value('tool',array('tool_id'=>$tool_id),'country_id');
        $old_data = array('tool_status'=>'Deactive');
        $new_data = array('tool_status'=>'Activated');
        audit_data('tool_master',$tool_id,'tool',2,'',$new_data,array('tool_id'=>$tool_id),$old_data,'Tool has Activated','',array(),'','',$country_id);
        
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Tool has been Activated successfully!
        </div>');
        redirect(SITE_URL.'tool');
    }
    public function deactivate_tool_sub_component()
    {
        #page authentication
        $task_access = page_access_check('manage_tool');

        $tool_part_id = validate_number($this->input->post('tool_part_id',TRUE));
        $update_data = array('status'=>2);
        $update_where = array('tool_part_id'=>$tool_part_id);
        $res = $this->Common_model->update_data('tool_part',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }
    
}