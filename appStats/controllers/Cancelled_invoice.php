<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
created_by : Srilekha
Date : 21-09-2017
*/
class Cancelled_invoice extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Cancelled_invoice_m');
        $this->load->model('Fe_delivery_m');
        $this->load->model('Wh_stock_transfer_m');
        $this->load->model('Calibration_m');
        $this->load->model('Wh_repair_m');
        $this->load->model('Pickup_point_m');
	}

	public function cancelled_invoice()
    {
        $data['nestedView']['heading']="Cancelled Invoices";
		$data['nestedView']['cur_page'] = "cancelled_invoice";
		$data['nestedView']['parent_page'] = 'cancelled_invoice';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Cancelled Invoices';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Cancelled Invoices','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchinvoice',TRUE));
        $cancelled_date = validate_string($this->input->post('cancelled_date',TRUE));
        if($cancelled_date!='')
        {
            $cancelled_date = format_date($cancelled_date);
        }
        else
        {
            $cancelled_date='';
        }
        if($psearch!='') 
        {
            $searchParams=array(
            'invoice_no'     => validate_string($this->input->post('invoice_no', TRUE)),
            'cancelled_date' => $cancelled_date,
            'whp_id'         => validate_number($this->input->post('wh_id',TRUE)),
            'country_id'     => $this->input->post('country_id',TRUE)
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'invoice_no'     => $this->session->userdata('invoice_no'),
                'cancelled_date' => $this->session->userdata('cancelled_date'),
                'whp_id'         => $this->session->userdata('whp_id'),
                'country_id'     => $this->session->userdata('country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'invoice_no'     => '',
                'cancelled_date' => '',
                'whp_id'         => '',
                'country_id'     => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;
        //print_r($data['search_data']); exit;
		# Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'cancelled_invoice/';
        # Total Records
        $config['total_rows'] = $this->Cancelled_invoice_m->cancelled_invoice_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['cal_list_Results'] = $this->Cancelled_invoice_m->cancelled_invoice_list_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['displayResults'] = 1;
    
		$this->load->view('cancelled_invoice/cancelled_invoice_list',$data);
	}
    public function view_cancelled_invoice()
    {
        $ph_id=storm_decode($this->uri->segment(2));
        if($ph_id=="")
        {
            redirect(SITE_URL.'cancelled_invoice'); exit();
        }
        $data['ph_check'] = 1;
        $ph_data = $this->Common_model->get_data_row('print_history',array('ph_id'=>$ph_id));
        if($ph_data['order_delivery_id']!='')
        {
            $tool_order_id = $this->Common_model->get_value('order_delivery',array('order_delivery_id'=>$ph_data['order_delivery_id']),'tool_order_id');
            $order_type = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'order_type');
            if($order_type == 2)
            {
                $delivery = $this->Cancelled_invoice_m->get_delivery_details($tool_order_id,$ph_id);
                $data['tool_order'] = $this->Fe_delivery_m->get_fe_address($tool_order_id);
                $country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
                $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id,'status'=>1),'name');

                $ordered_tool_list = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status'=>1));
                $ordered_tool_arr = array();
                foreach ($ordered_tool_list as $row) 
                {
                    $ordered_asset_list = $this->Common_model->get_data('ordered_asset',array('ordered_tool_id'=>$row['ordered_tool_id']));
                    if(count($ordered_asset_list)>0)
                    {
                        $check = 0;
                        foreach ($ordered_asset_list as $key => $value) 
                        {
                            if($value['status'] == 2)
                            {
                                $check++;
                            }
                        }
                        if($row['quantity'] != $check)
                        {
                            $ordered_tool_arr[] = $row['ordered_tool_id'];
                        }
                    }
                    else
                    {
                        $ordered_tool_arr[] = $row['ordered_tool_id'];
                    }
                }

                $tools_list = $this->Fe_delivery_m->get_invoiced_tools($tool_order_id,$ordered_tool_arr);

                $total_amount = 0;
                foreach ($tools_list as $key => $value) 
                {
                    $cost = $value['cost_in_inr'];
                    $gst_percent = $value['gst_percent'];
                    $gst = explode("%", $gst_percent);
                    $tax_amount = ($cost * $gst[0])/100;
                    $amount = $cost+$tax_amount;
                    $total_amount+= ($cost+$tax_amount);
                    
                    $tools_list[$key]['taxable_value'] = $cost;
                    $tools_list[$key]['tax_amount'] = $tax_amount;
                    $tools_list[$key]['amount'] = $amount;
                }
                $data['total_amount'] = $total_amount;
                $data['delivery'] = $delivery;
                $data['tools_list'] = $tools_list;
                $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$delivery['billed_to']));
                
                $this->load->view('fe_delivery/invoice_print',$data);
                
            }
            else if($order_type == 1)
            {
                $check_st = $this->Common_model->get_value('st_tool_order',array('tool_order_id'=>$tool_order_id),'stock_transfer_id');
                if($check_st=='')
                { 
                     $check_st_type = 0; 
                }
                else
                { 
                    $check_st_type = 1; 
                }
                $country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
                $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id),'name');
                $from_address = $this->Cancelled_invoice_m->get_delivery_details_wh_st($tool_order_id,$ph_id);
                if($from_address['fe_check']==1)
                {
                    $data['to_address'] = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id,'status'=>1));

                    $sso = $this->Wh_stock_transfer_m->get_fe_sso($tool_order_id);
                    $data['sso_name'] = $sso['sso_name'];
                    $data['mobile_no'] = $sso['mobile_no'];
                
                }
                else if($from_address['fe_check']==0)
                {
                    $data['to_address'] = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id,'status'=>1));
                    $data['to_wh_arr'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['to_wh_id']));
                }
                $tools_list = $this->Wh_stock_transfer_m->get_invoiced_tools($tool_order_id);
                $total_amount = 0;
                foreach ($tools_list as $key => $value)
                {
                    $cost = $value['cost_in_inr'];
                    $gst_percent = $value['gst_percent'];
                    $gst = explode("%", $gst_percent);
                    $tax_amount = ($cost * $gst[0])/100;
                    $amount = $cost+$tax_amount;
                    $total_amount+= ($cost+$tax_amount);
                    $tools_list[$key]['taxable_value'] = $cost;
                    $tools_list[$key]['tax_amount'] = $tax_amount;
                    $tools_list[$key]['amount'] = $amount;
                }
                $data['total_amount'] = $total_amount;
                $data['from_address'] = $from_address;
                $data['tools_list'] = $tools_list;
                $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['billed_to']));
                $this->load->view('wh_stock_transfer/invoice_print',$data);
            }
        }
        else if($ph_data['rc_shipment_id']!='')
        {
            $rc_order_id = $this->Common_model->get_value('rc_shipment',array('rc_shipment_id'=>$ph_data['rc_shipment_id']),'rc_order_id');
            $rc_type = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rc_type');
            if($rc_type == 2)//calibration
            {
                $from_address = $this->Cancelled_invoice_m->get_from_address($rc_order_id,$ph_id); 
                $data['to_address'] = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id)); 
                $tools_list = $this->Calibration_m->get_calibration_tool($rc_order_id);
                $rc_order_arr = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
                $country_id = $rc_order_arr['country_id'];
                $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id,'status'=>1),'name');
                $total_amount = 0;
                foreach ($tools_list as $key => $value)
                {
                    $cost = $value['cost_in_inr'];
                    $gst_percent = $value['gst_percent'];
                    $gst = explode("%", $gst_percent);
                    $tax_amount = ($cost * $gst[0])/100;
                    $amount = $cost+$tax_amount;
                    $total_amount+= ($cost+$tax_amount);
                    $tools_list[$key]['taxable_value'] = $cost;
                    $tools_list[$key]['tax_amount'] = $tax_amount;
                    $tools_list[$key]['amount'] = $amount;
                }
                $data['total_amount'] = $total_amount;
                $data['from_address'] = $from_address;
                $data['tools_list'] = $tools_list;
                $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['billed_to']));
                $this->load->view('calibration/invoice_print',$data);
            }
            if($rc_type == 1)//repair
            {
                $from_address = $this->Cancelled_invoice_m->get_from_address_1($rc_order_id,$ph_id); 
                $data['to_address'] = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id)); 
                $tools_list = $this->Wh_repair_m->get_calibration_tool($rc_order_id);
                $rc_order_arr = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
                $country_id = $rc_order_arr['country_id'];
                $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id,'status'=>1),'name');
                $total_amount = 0;
                foreach ($tools_list as $key => $value)
                {
                    $cost = $value['cost_in_inr'];
                    $gst_percent = $value['gst_percent'];
                    $gst = explode("%", $gst_percent);
                    $tax_amount = ($cost * $gst[0])/100;
                    $amount = $cost+$tax_amount;
                    $total_amount+= ($cost+$tax_amount);
                    $tools_list[$key]['taxable_value'] = $cost;
                    $tools_list[$key]['tax_amount'] = $tax_amount;
                    $tools_list[$key]['amount'] = $amount;
                }
                $data['total_amount'] = $total_amount;
                $data['from_address'] = $from_address;
                $data['tools_list'] = $tools_list;
                $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['billed_to']));
                $this->load->view('repair/invoice_print',$data);
            }
        }
        else if($ph_data['return_order_id']!='')
        {
            $return_order_id = $ph_data['return_order_id'];
            $from_address = $this->Cancelled_invoice_m->get_from_address_2($return_order_id,$ph_id);
            $return_type_id = $from_address['return_type_id'];
            $data['currency_name'] = $from_address['currency_name'];
            if($return_type_id == 1 || $return_type_id == 2)
            {
                $ro_to_wh_id = $this->Common_model->get_value('return_order',array('return_order_id'=>$return_order_id),'ro_to_wh_id');
                $data['to_address'] = $this->Pickup_point_m->get_to_address1($ro_to_wh_id);
            }
            else if($return_type_id == 4)
            {
                $data['to_address'] = $this->Pickup_point_m->get_to_address2($return_order_id);
            }
            
            $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
            $order_status_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id,'current_stage_id'=>8),'order_status_id');
            $tools_list = $this->Pickup_point_m->get_requested_tools($order_status_id);
            $total_amount = 0;
            foreach ($tools_list as $key => $value)
            {
                $cost = $value['cost_in_inr'];
                $gst_percent = $value['gst_percent'];
                $gst = explode("%", $gst_percent);
                $tax_amount = ($cost * $gst[0])/100;
                $amount = $cost+$tax_amount;
                $total_amount+= ($cost+$tax_amount);
                $tools_list[$key]['taxable_value'] = $cost;
                $tools_list[$key]['tax_amount'] = $tax_amount;
                $tools_list[$key]['amount'] = $amount;
            }
            $data['total_amount'] = $total_amount;
            $data['from_address'] = $from_address;
            $data['tools_list'] = $tools_list;
            $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['billed_to']));
            $this->load->view('pickup/invoice_print',$data);
        }
    }
} 
?>