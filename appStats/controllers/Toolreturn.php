<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Toolreturn extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Toolreturn_model');
        $this->load->model('Alerts_m');
        $this->load->model('Order_m');
	}

	public function downloadtoolreturn()
	{
		if(validate_string($this->input->post('downloadtoolreturn',TRUE))==1)
		{
			$searchParams=array(
                                'cr_order_number'             => validate_string($this->input->post('order_number', TRUE)),
                                'cr_order_delivery_type_id'   => validate_string($this->input->post('order_delivery_type_id', TRUE)),
                                'crd_sso_id'                  => validate_string($this->input->post('crd_sso_id', TRUE)),
                                'crd_country_id'              => validate_string($this->input->post('crd_country_id', TRUE)),                               
                               );
            $task_access = page_access_check('crossed_return_date_orders');
			$orderResults = $this->Toolreturn_model->crossedReturnDateOrdersResults($searchParams,$task_access);            
            $header = '';
            $data ='';
            $titles = array('Order Number','SSO','Country','Order Type','Request Date','Return Date','Status','Tool Number',
                'Tool Code','Tool Description','Ordered Qty','Avail Qty');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';            
            $data.='<tbody>';
            if(count($orderResults)>0)
            {                
                foreach($orderResults as $row)
                {
                    $orderedTools = $this->Order_m->get_ordered_tools($row['tool_order_id']);
                    //echo '<pre>';print_r($orderedTools);exit;
                    if($row['current_stage_id'] == 4)
                    {
                        if(checkSTorderExist($row['tool_order_id'])==1)
                        {
                            $status = 'In Stock Transfer';
                        }
                        else
                        {
                            $status = getReasonForOrderAtAdmin($row['tool_order_id']);
                        }
                    }
                   /* else
                    {
                        $status = $row['name'];
                    }*/
                    
                    if($row['order_delivery_type_id'] == 1)
                    {         
                        //echo '<pre>';print_r($row)       ;exit;
                        $address['system_id'] = $row['system_id'];
                        $address['site_id'] = $row['site_id'];
                        $rest =  $this->Order_m->get_install_basedata($row['system_id']);        
                        $address['c_name'] = $rest['c_name'];
                        
                    }
                    else
                    {
                        if($row['order_delivery_type_id'] == 2)
                        {
                            $address['warehouse'] = getWhCodeAndName($row['wh_id']);                    
                        }
                    }
                    $status = getOpenOrderStatus($row);

                    $data.='<tr>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['order_number'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['sso'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['country_name'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$row['order_type'].'</td>'; 
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['request_date']).'</td>';                
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.full_indian_format($row['return_date']).'</td>';
                    $data.='<td rowspan="'.count($orderedTools).'" style="vertical-align: middle; text-align: center;">'.$status.'</td>'; 
                   
                    foreach ($orderedTools as $key => $value) {
                            //if($i!=1)  $data.='<tr>';
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_number'].'</td>'; 
                            $data.='<td style="vertical-align: middle; text-align: center;">'.'`'.$value['tool_code'].'</td>';  
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['part_description'].'</td>'; 
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['quantity'].'</td>';
                            $data.='<td style="vertical-align: middle; text-align: center;">'.$value['available_quantity'].'</td>';                    

                            $data.='</tr>';          
                            //$i++;
                    } 
                   //$data.='</tr>';  
                      
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            //echo $data;exit;
            $time = date("Y-m-d H:i:s");
            $xlFile='ToolreturnDownloads'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
		}
	}

}