<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Role extends Base_Controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Role_m');
	}

	public function role_page()
    {
        
        $data['nestedView']['heading']=" Manage Roles";
		$data['nestedView']['cur_page'] = 'role';
		$data['nestedView']['parent_page'] = 'role';

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = ' Manage Roles';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Roles','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('search_role', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'rolename' => validate_string($this->input->post('role_name',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                'rolename' => $this->session->userdata('rolename')
                               );
            }
            else {
                $searchParams=array(
                    'rolename' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'role_page/';
        # Total Records
        $config['total_rows'] = $this->Role_m->role_total_num_rows($searchParams);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['roleResults'] = $this->Role_m->role_results($current_offset, $config['per_page'], $searchParams);
        
        # Additional data
        $data['displayResults'] = 1;
        $this->load->view('roles/role_view',$data);
    }

    public function add_role()
    {
        $data['nestedView']['heading']="Add New Role";
		$data['nestedView']['cur_page'] = 'role';
		$data['nestedView']['parent_page'] = 'role';

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/role.js"></script>';

		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Add New Role';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Roles','class'=>'','url'=>SITE_URL.'role_page');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Role','class'=>'active','url'=>'');
        # Additional data
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_role';
        $data['displayResults'] = 0;
        $this->load->view('roles/role_view',$data);
    }

    public function insert_role()
    {
        $role_name = validate_string($this->input->post('role_name',TRUE));
        $role_data = array('name' => $role_name,'role_id'=>0);
        $unique = $this->Role_m->check_role_name_availability($role_data);
        if($unique>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> '.$role_name.' already exist! please check.</div>'); 
            redirect(SITE_URL.'role_page'); exit();
        }
        if($unique==0)
        {
            $data=array(
            'name'         => $role_name,
            'status'       => 1,
            'created_by'   => $this->session->userdata('sso_id'),
            'created_time' => date('Y-m-d H:i:s')
            );

            $this->db->trans_begin();    
            $role_id = $this->Common_model->insert_data('role',$data); 

            $child_role_array = array('parent_role_id' => $role_id,
                                      'child_role_id'  => $role_id,
                                      'status'         => 1);
            $this->Common_model->insert_data('role_login',$child_role_array);

            if ($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>'); 
               
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Role : <strong>'.$role_name.'</strong> has been Added successfully!</div>');
                      
            }
        }
        redirect(SITE_URL.'role_page');
    }   

    public function edit_role()
    {
        $role_id=@storm_decode($this->uri->segment(2));
        if($role_id=='')
        {
            redirect(SITE_URL.'role_page');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Role ";
		$data['nestedView']['cur_page'] = 'role';
		$data['nestedView']['parent_page'] = 'role';

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/role.js"></script>';

		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Edit Role ';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Roles','class'=>'','url'=>SITE_URL.'role_page');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Role','class'=>'active','url'=>'');       
        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_role';
        $data['displayResults'] = 0;
       
        # Data
        $row = $this->Common_model->get_data_row('role',array('role_id'=>$role_id));
        $data['lrow'] = $row;
        $this->load->view('roles/role_view',$data);
    }

    public function update_role()
    {
        $role_id=validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($role_id==''){
            redirect(SITE_URL.'role_page');
            exit;
        }
        $role_name = validate_string($this->input->post('role_name',TRUE));
        $data = array('name'=>$role_name,'role_id'=>$role_id);
        $unique = $this->Role_m->check_role_name_availability($data);
        if($unique>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> '.$role_name.' already exist! please check.</div>'); 
            redirect(SITE_URL.'role_page'); exit();
        }
        else
        {
            // GETTING INPUT TEXT VALUES
            $data = array( 
                        'name'          => $role_name,
                        'modified_by'   => $this->session->userdata('sso_id'),
                        'modified_time' => date('Y-m-d H:i:s'),
                        'status'        => 1
                    );
            $where = array('role_id'=>$role_id);
            $this->db->trans_begin();
            $this->Common_model->update_data('role',$data,$where);

            if($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>');  
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Role : <strong>'.$role_name.'</strong>  has been updated successfully! </div>');
            }   
        }
        redirect(SITE_URL.'role_page'); exit();
    }

    public function deactivate_role($encoded_id)
    {
        $role_id=@storm_decode($encoded_id);
        if($role_id==''){
            redirect(SITE_URL.'role_page');
            exit;
        }
        $where = array('role_id' => $role_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('role',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Role has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'role_page');
    }

    public function activate_role($encoded_id)
    {
        $role_id=@storm_decode($encoded_id);
        if($role_id==''){
            redirect(SITE_URL.'role_page');
            exit;
        }
        $where = array('role_id' => $role_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('role',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Role has been Activated successfully!
        </div>');
        redirect(SITE_URL.'role_page');
	}
    public function is_role_exist()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $role_id = validate_number($this->input->post('role_id',TRUE));
        $data = array('name'=>$name,'role_id'=>$role_id);
        $result = $this->Role_m->check_role_name_availability($data);
        echo $result;
    }

    public function role_task_assign()
    {
        $role_id=@storm_decode($this->uri->segment(2));
        if($role_id=='')
        {
            redirect(SITE_URL.'role_page');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Assign Tasks";
        $data['nestedView']['cur_page'] = 'role';
        $data['nestedView']['parent_page'] = 'role';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/role.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Assign Tasks ';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Roles','class'=>'','url'=>SITE_URL.'role_page');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Assign Tasks','class'=>'active','url'=>'');

        $psearch=validate_string($this->input->post('search_role_task', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'page_id' => validate_string($this->input->post('page_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(3)!='')
            {
            $searchParams=array(
            'page_id' => $this->session->userdata('page_id')
                               );
            }
            else {
                $searchParams=array(
                'page_id' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_dat'] = $searchParams;



        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'role_task_assign/'.$this->uri->segment(2).'/';
        # Total Records
        $config['total_rows'] = $this->Role_m->get_task_pages_total_rows($searchParams);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $pages = $this->Role_m->get_task_pages_results($current_offset, $config['per_page'], $searchParams);
        $data['encoded_id']=$this->uri->segment(2);

        $data['role_name']      = $this->Common_model->get_value('role',array('role_id'=>$role_id),'name');
        $data['pageList']       = $this->Common_model->get_data('page',array('status' => 1));
        $data['current_offset'] = $current_offset;
        $data['page_task']      = $pages;
        $data['role_id']        = $role_id;
        $data['displayResults'] = 1;
        $this->load->view('roles/role_page_task_view',$data);
    }

    public function insert_role_task()
    {
        if($this->input->post('submit_role_task')!='')
        {
            $task_arr=$this->input->post('task',TRUE);
            $hidden_task_arr=$this->input->post('hidden_value',TRUE);
            $encoded_id=$this->input->post('encoded_id',TRUE);
            $current_offset=$this->input->post('c_offset',TRUE);
            $role_id=$this->input->post('role_id',TRUE);
            $this->db->trans_begin();
            
            foreach(@$hidden_task_arr as $key=>$value)
            {
                $this->Common_model->delete_data('role_page',array('role_id'=>$role_id,'page_id'=>$value));
                if(count($task_arr)>0)
                {
                    if(in_array($value, $task_arr))
                    {
                        $arr=array(
                        'role_id'     => $role_id,
                        'page_id'     => $task_arr[$value],
                        'status'      => 1
                        );
                        $this->Common_model->insert_data('role_page',$arr);
                    }
                }
            }
            if($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>');  
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Tasks Has Been Assigned successfully! </div>');
            }
            
        }
        redirect(SITE_URL . 'role_task_assign/'.$encoded_id.'/'.$current_offset);
    }

    public function role_login()
    {
        $role_id=@storm_decode($this->uri->segment(2));
        if($role_id=='')
        {
            redirect(SITE_URL.'role_page');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Assign Roles ";
        $data['nestedView']['cur_page'] = 'role';
        $data['nestedView']['parent_page'] = 'role';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/role.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Assign Roles ';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Roles','class'=>'','url'=>SITE_URL.'role_page');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Assign Roles','class'=>'active','url'=>'');

        $psearch=validate_string($this->input->post('search_role_task', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'p_role' => validate_string($this->input->post('p_role',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(3)!='')
            {
            $searchParams=array(
            'p_role' => $this->session->userdata('p_role')
                               );
            }
            else {
                $searchParams=array(
                'p_role' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_dat'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'role_login/'.$this->uri->segment(2).'/';
        # Total Records
        $config['total_rows'] = $this->Role_m->get_roles_login_total_rows($searchParams);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $pages = $this->Role_m->get_roles_login_results($current_offset, $config['per_page'], $searchParams);

        $data['encoded_id']=$this->uri->segment(2);
        $data['role_name']=$this->Common_model->get_value('role',array('role_id'=>$role_id),'name');
        $data['current_offset']=$current_offset;
        $data['page_task']=$pages;
        $data['role_id']=$role_id;
        $data['displayResults'] = 1;
        $this->load->view('roles/role_login_view',$data);
    }

    public function insert_role_login()
    {
        if($this->input->post('submit_role_task')!='')
        {
            $task_arr=$this->input->post('task',TRUE);
            $hidden_task_arr=$this->input->post('hidden_value',TRUE);
            $encoded_id=$this->input->post('encoded_id',TRUE);
            $current_offset=$this->input->post('c_offset',TRUE);
            $role_id=$this->input->post('role_id',TRUE);

            $this->db->trans_begin();
            foreach(@$hidden_task_arr as $key=>$value)
            {
                $this->Common_model->delete_data('role_login',array('parent_role_id'=>$role_id,'child_role_id'=>$value));
                if(count($task_arr)>0)
                {
                    if(in_array($value, $task_arr))
                    {
                        $arr=array(
                        'parent_role_id' => $role_id,
                        'child_role_id'  => $task_arr[$value],
                        'status'         => 1
                        );
                        $this->Common_model->insert_data('role_login',$arr);
                    }
                }
            }
            if($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>');  
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Roles Has Been Assigned successfully! </div>');
            }            
        }
        redirect(SITE_URL . 'role_login/'.$encoded_id.'/'.$current_offset);
    }

    public function role_task()
    {
        $role_id=@storm_decode($this->uri->segment(2));
        if($role_id=='')
        {
            redirect(SITE_URL.'role_page');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Assign Sub Tasks ";
        $data['nestedView']['cur_page'] = 'role';
        $data['nestedView']['parent_page'] = 'role';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/role.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Assign Sub Tasks ';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Roles','class'=>'','url'=>SITE_URL.'role_page');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Assign Sub Tasks','class'=>'active','url'=>'');

        $psearch=validate_string($this->input->post('search_role_task', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'page_id'   => validate_number($this->input->post('page_id',TRUE)),
            'task_id'   => validate_string($this->input->post('task_id',TRUE)),
            'p_role_id' => validate_number($this->input->post('role_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(3)!='')
            {
            $searchParams=array(
            'task_id'   => $this->session->userdata('task_id'),
            'page_id'   => $this->session->userdata('page_id'),
            'p_role_id' => $this->session->userdata('p_role_id')
                               );
            }
            else {
                $searchParams=array(
                'task_id'   => '',
                'page_id'   => '',
                'p_role_id' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_dat'] = $searchParams;



        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'role_task/'.$this->uri->segment(2).'/';
        # Total Records
        $config['total_rows'] = $this->Role_m->get_pages_total_rows($searchParams,$role_id);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $pages = $this->Role_m->get_pages_results($current_offset, $config['per_page'], $searchParams,$role_id);
        $data['encoded_id']=$this->uri->segment(2);

        foreach($pages as $key=>$value)
        {
            $tasks = $this->Common_model->get_data('task',array('page_id'=>$value['page_id'],'status'=>1));
            $pages[$key]['page_tasks']=$tasks;
        }
        $data['pages']      = $this->Common_model->get_data('page',array('status'=>1));
        $data['roles_list'] = $this->Role_m->get_child_roles($role_id);
        $data['taskList']   = $this->Common_model->get_data('task',array('status'=>1));
        $data['role_name']  = $this->Common_model->get_value('role',array('role_id'=>$role_id),'name');
        $data['current_offset'] = $current_offset;
        $data['page_task']      = $pages;
        $data['role_id']        = $role_id;
        $data['displayResults'] = 1;
        $this->load->view('roles/role_task_view',$data);
    }

    public function insert_assign_task()
    {
        if($this->input->post('submit_role_task')!='')
        {
            $task_arr=$this->input->post('task',TRUE);
            $task_radio=$this->input->post('task_radio',TRUE);
            $hiddeen_task_arr=$this->input->post('hidden_value',TRUE);
            $encoded_id=$this->input->post('encoded_id',TRUE);
            $current_offset=$this->input->post('c_offset',TRUE);
            $role_id=$this->input->post('role_id',TRUE);
            $this->db->trans_begin();
            foreach(@$hiddeen_task_arr as $key=>$value)
            {
                $this->Common_model->delete_data('role_task',array('role_id'=>$role_id,'task_id'=>$value));
                if(isset($task_arr[$value]) && isset($task_radio[$value]))
                {
                    $arr=array(
                    'role_id'     => $role_id,
                    'task_id'     => $task_arr[$value],
                    'task_access' => $task_radio[$value],
                    'status'      => 1
                    );
                    $this->Common_model->insert_data('role_task',$arr);
                }
            }

            if($this->db->trans_status()===FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Something went wrong. Please check. </div>');  
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Tasks Has Been Assigned successfully! </div>');
            }
            
        }
        redirect(SITE_URL . 'role_task/'.$encoded_id.'/'.$current_offset);
    }

}