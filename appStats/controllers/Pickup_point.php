<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
/*
created_by : Koushik
Date : 26-07-2017
*/
class Pickup_point extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Pickup_point_m');
	}

	public function pickup_list()
    {
        $data['nestedView']['heading']="Arrange Pickup for FE Returns";
		$data['nestedView']['cur_page'] = "pickup_list";
		$data['nestedView']['parent_page'] = 'pickup_list';

		#page Authencation
		$task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Arrange Pickup for FE Returns';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Arrange Pickup for FE Returns','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchpickup',TRUE));	
        if($psearch!='') 
        {
            $searchParams=array(
			'tool_order_number' => validate_string($this->input->post('tool_order_number', TRUE)),
			'ssoid'			    => validate_string($this->input->post('ssoid',TRUE)),
			'date'			    => validate_string($this->input->post('date',TRUE)),
			'return_number'	    => validate_string($this->input->post('return_number',TRUE)),
			'whid'              => validate_number($this->input->post('whid',TRUE)),
			'country_id'        => validate_number($this->input->post('country_id',TRUE)),
			'return_type_id'    => validate_number($this->input->post('return_type_id',TRUE))
			);
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
			if($this->uri->segment(2)!='')
            {
				$searchParams=array(
				'tool_order_number' => $this->session->userdata('tool_order_number'),
				'ssoid'             => $this->session->userdata('ssoid'),
				'date'		        => $this->session->userdata('date'),
				'return_number'	    => $this->session->userdata('return_number'),
				'whid'	            => $this->session->userdata('whid'),
				'country_id'	    => $this->session->userdata('country_id'),
				'return_type_id'    => $this->session->userdata('return_type_id')
				);
            }
            else {
                $searchParams=array(
                    'tool_order_number' => '',
                    'ssoid'             => '',
                    'date'				=> '',
                    'return_number'		=> '',
                    'page_redirect_pk'	=> '',
                    'whid'              => '',
                    'country_id'        => '',
                    'return_type_id'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;

		# Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'pickup_list/';
        # Total Records
        $config['total_rows'] = $this->Pickup_point_m->pickup_list_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['pickup_Results'] = $this->Pickup_point_m->pickup_list_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        
        # Additional data
        $data['displayResults'] = 1;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['return_type'] = $this->Common_model->get_data('return_type',array('return_type_id != '=>3));
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;

		$this->load->view('pickup/pickup_list',$data);
	}


	public function view_pickup_details()
	{
		$return_order_id = storm_decode($this->uri->segment(2));
		$page_redirect_pk = $this->session->userdata('page_redirect_pk');
		if($return_order_id == '' || $page_redirect_pk == 1)
		{
			redirect(SITE_URL.'pickup_list'); exit();
		}

		$data['nestedView']['heading']="Pickup Courier Details";
		$data['nestedView']['cur_page'] = "check_pickup";
		$data['nestedView']['parent_page'] = 'pickup_list';
		$data['enableFormWizard'] = 1;

		#page Authentication
		$task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['css_includes'] = array();
		$data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux-responsive.min.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/fuelux/loader.min.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/pickup.js"></script>';

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Pickup Courier Details';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Arrange Pickup for FE Returns','class'=>'','url'=>SITE_URL.'pickup_list');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Pickup Courier Details','class'=>'active','url'=>'');

		#additional Data
		$return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
		$data['country_id'] = $return_order['country_id'];
		if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
		{
			$trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['ro_to_wh_id']));
			$trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
			$trrow['check_val'] = 0;
		}
		else if($return_order['return_type_id'] == 4)
		{
			$trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
			$sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
			$sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
			$trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];
			$trrow['check_val'] = 1;
		}
		$trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');


		$rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
		$rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
		//echo $this->db->last_query();exit;
		$asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
		$data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>2));
		$data['rrow'] = $rrow;
		$data['trrow'] = $trrow;
		$data['shipped_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$rrow['wh_id']));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$rrow['country_id']));
		$data['asset_list'] = $asset_list;
		$data['flg'] = 1;
		$data['displayResults'] = 0;
		$data['form_action'] = SITE_URL.'insert_pickup_delivery_details';
		$this->load->view('pickup/pickup_list',$data);
	}

	public function insert_pickup_delivery_details()
	{
		$return_order_id = validate_number(storm_decode($this->input->post('return_order_id',TRUE)));
		$tool_order_id   = validate_number(storm_decode($this->input->post('tool_order_id',TRUE)));
		$rto_id = validate_number(storm_decode($this->input->post('rto_id',TRUE)));
		if($return_order_id == '' || $tool_order_id == '' || $rto_id == '')
		{
			redirect(SITE_URL.'pickup_list'); exit();
		}

		#page authentication
		$task_access = page_access_check('pickup_list');

		$return_details = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
		$trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
		$courier_type   = validate_number($this->input->post('courier_type',TRUE));
		$courier_name   = validate_string($this->input->post('courier_name',TRUE));
		$vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
		$courier_number = validate_string($this->input->post('courier_number',TRUE));
		$contact_person = validate_string($this->input->post('contact_person',TRUE));
		$phone_number   = validate_string($this->input->post('phone_number',TRUE));
		$expected_arrival_date = validate_string(format_date($this->input->post('expected_arrival_date',TRUE)));
		$doc_type       = $this->input->post('document_type',TRUE);
		$remarks        = $this->input->post('remarks',TRUE);
		if($remarks == ""){ $remarks = NULL; }

		if($courier_type == 1)
		{
			$phone_number   = NULL;
			$contact_person = NULL;
			$vehicle_number = NULL;
		}
		else if($courier_type == 2)
		{
			$courier_name   = NULL;
			$courier_number = NULL;
			$vehicle_number = NULL;
		}
		else if($courier_type == 3)
		{
			$courier_name   = NULL;
			$courier_number = NULL;
		}

		$print_type = validate_number($this->input->post('print_type',TRUE));
		$ro_wh_id = $return_details['wh_id'];
		$country_id = $trow['country_id'];
        $this->db->trans_begin();
        $print_id = get_current_print_id($print_type,$ro_wh_id);

		#update retur order with courier detials
		$update_return_order = array(
			'courier_type'   => $courier_type,
			'print_id' 	     => $print_id,
			'courier_name'   => $courier_name,
			'courier_number' => $courier_number,
			'vehicle_number' => $vehicle_number,
			'contact_person' => $contact_person,
			'phone_number'   => $phone_number,
			'billed_to'	     => validate_number($this->input->post('billed_to',TRUE)),
			'address1'       => validate_string($this->input->post('add1',TRUE)),
			'address2'       => validate_string($this->input->post('add2',TRUE)),
			'address3'       => validate_string($this->input->post('add3',TRUE)),
			'address4'       => validate_string($this->input->post('add4',TRUE)),
			'zip_code'       => validate_string($this->input->post('pin1',TRUE)),
			'gst_number'     => validate_string($this->input->post('gst1',TRUE)),
			'pan_number'     => validate_string($this->input->post('pan1',TRUE)),
			'expected_arrival_date' => $expected_arrival_date,
			'remarks'        => $remarks,
			'modified_by'    => $this->session->userdata('sso_id'),
			'modified_time'  => date('Y-m-d H:i:s'),
			'country_id'     => $country_id,
			'status'		 => 10);
		$update_where = array('return_order_id'=>$return_order_id);
		$this->Common_model->update_data('return_order',$update_return_order,$update_where);

		#audit data
		unset($update_return_order['modified_by'],$update_return_order['modified_time']);
		$final_arr = array_diff_assoc($update_return_order, $return_details);
		if(count($final_arr)>0)
		{
			$remarks = "Pickup Details for Return Order No:".$return_details['return_number'];
			$ad_id = audit_data('tool_pickup',$return_order_id,'return_order',2,'',$final_arr,array('return_order_id'=>$return_order_id),$return_details,$remarks,'',array(),'tool_order',$tool_order_id,$country_id);
		}
		#print_history
        $insert_print_history = array(
        	'print_id'          => $print_id,
			'remarks'           => 'Print Creation',
			'return_order_id'   => $return_order_id,
			'created_by'        => $this->session->userdata('sso_id'),
			'created_time'      => date('Y-m-d H:i:s'),
			'status'            => 1
		);
        $this->Common_model->insert_data('print_history',$insert_print_history);

		#insert attached document in return_doc table
		if(count($doc_type)>0)
		{
			foreach ($doc_type as $key => $value) 
	        {
	            if($_FILES['support_document_'.$key]['name']!='')
	            {
	                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
	                $config['upload_path'] = asset_document_path();
	                $config['allowed_types'] = 'gif|jpg|png|pdf';
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                $this->upload->do_upload('support_document_'.$key);
	                $fileData = $this->upload->data();
	                $support_document = $config['file_name'];
	            }
	            else
	            {
	                $support_document = '';
	            }
	            $insert_doc = array(
	            	'document_type_id' => $value,
					'name'             => $support_document,
					'doc_name'		   => $_FILES['support_document_'.$key]['name'],
					'return_order_id'  => $return_order_id,
					'created_by'       => $this->session->userdata('sso_id'),
					'created_time'     => date('Y-m-d H:i:s'),
					'status'           => 1
				);
	            if($value!='' && $support_document != '')
	            {
	                $this->Common_model->insert_data('return_doc',$insert_doc);
	            }
	        }
		}

		$os_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id,'current_stage_id'=>8),'order_status_id');

		#update tool order status from 8-waiting for pickup and insert 9-in transit FE TO wh
		$update_osh    = array('end_time'         => date('Y-m-d H:i:s'));
		$update_where1 = array('tool_order_id'    => $tool_order_id,
							   'current_stage_id' => 8,//waiting for pickup
							   'rto_id'	          => $rto_id,
							   'end_time'		  => NULL);
		$this->Common_model->update_data('order_status_history',$update_osh,$update_where1);

		$insert_osh = array('tool_order_id'    => $tool_order_id,
							'current_stage_id' => 9,//in transit
							'rto_id'	       => $rto_id,
							'created_time'	   => date('Y-m-d H:i:s'),
							'created_by'	   => $this->session->userdata('sso_id'),
							'status'		   => 1);
		$order_status_id = $this->Common_model->insert_data('order_status_history',$insert_osh);

		#insert tools in transit assets
		$order_asset = $this->Pickup_point_m->get_oah_data($os_id);
		foreach ($order_asset as $row) 
		{
			$insert_order_asset = array(
				'ordered_asset_id' => $row['ordered_asset_id'],
				'order_status_id'  => $order_status_id,
				'created_by'       => $this->session->userdata('sso_id'),
				'created_time'     => date('Y-m-d H:i:s'),
				'status'		   => $row['status']);
			$oah_id = $this->Common_model->insert_data('order_asset_history',$insert_order_asset);
			$oahealth = $this->Common_model->get_data('order_asset_health',array('oah_id'=>$row['oah_id']));
			foreach ($oahealth as $key => $value) 
			{
				$insert_oahealth = array(
					'oah_id'             => $oah_id,
					'asset_condition_id' => $value['asset_condition_id'],
					'part_id'            => $value['part_id'],
					'remarks'			 => $value['remarks']
				);
				$this->Common_model->insert_data('order_asset_health',$insert_oahealth);
			}
		}

		#FE to FE transfer by courier
		if($return_details['return_type_id'] == 4)
		{
			$fe2_tool_order_id = $return_details['tool_order_id'];
			$fe2_trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe2_tool_order_id));

			#audit data
			$remarks = "In Transit from FE to FE, Return Order No:".$return_details['return_number']." For FE2 Tool Order No:".$fe2_trow['order_number'];
			$ad_id2 = audit_data('in_transit_to_fe',$order_status_id,'order_status_history',2,'',array('Current Stage' => 'Waiting For FE to Ack'),array('tool_order_id'=>$fe2_tool_order_id),array('Current Stage'=>'Waiting For Pickup'),$remarks,'',array(),'tool_order',$fe2_tool_order_id,$fe2_trow['country_id']);

			#audit data
			$old_oa = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$fe2_tool_order_id,'status'=>1));

			#update order address of second FE
			$update_oa_fe2 = array(
				'address1' => validate_string($this->input->post('address1',TRUE)),
				'address2' => validate_string($this->input->post('address2',TRUE)),
				'address3' => validate_string($this->input->post('address3',TRUE)),
				'address4' => validate_string($this->input->post('address4',TRUE)),
				'pin_code' => validate_string($this->input->post('pin_code',TRUE)),
				'gst_number' =>validate_string($this->input->post('gst_number',TRUE)),
				'pan_number' =>validate_string($this->input->post('pan_number',TRUE))
			);
			$update_oa_fe2_where = array('tool_order_id'=>$fe2_tool_order_id);
			$this->Common_model->update_data('order_address',$update_oa_fe2,$update_oa_fe2_where);

			#audit data
			$final_arr = array_diff_assoc($update_oa_fe2, $old_oa);
			if(count($final_arr)>0)
			{
				if(!isset($ad_id))
				{
					$ad_id = audit_data('tool_pickup',$return_order_id,'return_order',2,'',array(),array('return_order_id'=>$return_order_id),array(),'','',array(),'tool_order',$tool_order_id,$country_id);
				}
				$remarks = "Updated Ship To address for Tool order No:".$fe2_trow['order_number']." while pickup arranged for return no:".$return_details['return_number'];
				audit_data('pickup_fe2_order_address',$old_oa['order_address_id'],'order_address',2,$ad_id,$final_arr,array('tool_order_id'=>$fe2_tool_order_id),$old_oa,$remarks,'',array(),'tool_order',$fe2_tool_order_id,$fe2_trow['country_id']);
			}

			$insert_fe2_osh = array(
				'tool_order_id'    => $fe2_tool_order_id,
				'current_stage_id' => 6,
				'rto_id'		   => $rto_id,
				'created_by'	   => $this->session->userdata('sso_id'),
				'created_time'	   => date('Y-m-d H:i:s'),
				'status'		   => 1
			);
			$fe2_os_id = $this->Common_model->insert_data('order_status_history',$insert_fe2_osh);

			$oah = $this->Common_model->get_data('order_asset_history',array('order_status_id'=>$os_id));
			
			foreach ($oah as $key => $value) 
			{
				$oa = $this->Common_model->get_data_row('ordered_asset',array('ordered_asset_id'=>$value['ordered_asset_id']));

				$asset_id = $oa['asset_id'];
				$asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

				$fe1ot_tool_id = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$oa['ordered_tool_id']),'tool_id');

				$fe2ot_id = $this->Common_model->get_value('ordered_tool',array('tool_id'=>$fe1ot_tool_id,'tool_order_id'=>$fe2_tool_order_id),'ordered_tool_id');
				
				$insert_fe2_oa = array(
					'ordered_tool_id' => $fe2ot_id,
					'asset_id'		  => $asset_id,
					'created_by'	  => $this->session->userdata('sso_id'),
					'created_time'	  => date('Y-m-d H:i:s'),
					'status'	      => 1
				);
				$fe2_oa_id = $this->Common_model->insert_data('ordered_asset',$insert_fe2_oa);

				$insert_fe2_oah = array(
					'ordered_asset_id' => $fe2_oa_id,
				    'order_status_id'  => $fe2_os_id,
				    'created_by'	   => $this->session->userdata('sso_id'),
				   	'created_time'	   => date('Y-m-d H:i:s'),
				    'status'		   => 1,
				    'remarks'		   => "inserted by wh for fe to fe transfer"
				);
				$fe2_oah_id = $this->Common_model->insert_data('order_asset_history',$insert_fe2_oah);

				#audit data for pickup 
				$new_data = array('transaction_status' => 'Asset has been Picked');
				$old_data = array('transaction_status' => 'Waiting for Pickup');
				$remarks = "Asset has been picked from FE to FE for Return Order No".$return_details['return_number'];
				audit_data('tool_pickup_assets',$value['oah_id'],'order_asset_history',2,$ad_id,$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'return_order',$return_order_id,$country_id);

				#audit data for in transit to FE
				$new_data = array('transaction_status' => 'In Transit to FE');
				$old_data = array('transaction_status' => 'Asset has been Picked');
				$remarks = "In Transit to FE from other FE Tool Order No".$fe2_trow['order_number'];
				audit_data('in_transit_to_fe_assets',$fe2_oah_id,'order_asset_history',2,$ad_id2,$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'return_order',$return_order_id,$fe2_trow['country_id']);


				//fe1 oah_id data
				$fe1_oahealth = $this->Common_model->get_data('order_asset_health',array('oah_id'=>$value['oah_id']));
				foreach ($fe1_oahealth as $row) 
				{
					$insert_fe2_oahealth = array(
						'oah_id' 			 => $fe2_oah_id,
						'asset_condition_id' => $row['asset_condition_id'],
						'part_id'		     => $row['part_id'],
						'remarks'		     => $row['remarks']);
					$this->Common_model->insert_data('order_asset_health',$insert_fe2_oahealth);
				}

				#audit data
				$old_data = array(
					'tool_availability' => get_asset_position($asset_id),
					'asset_status_id'   => $asset_arr['status']
				);
				#update asset
				$status = ($asset_arr['status']!=12)?8:$asset_arr['status'];
				$update_a = array(
				    'status'              => $status,
				    'availability_status' => 1,
				    'modified_by'         => $this->session->userdata('sso_id'),
				    'modified_time'       => date('Y-m-d H:i:s')
				);
				$update_a_where = array('asset_id'=>$asset_id);
				$this->Common_model->update_data('asset',$update_a,$update_a_where);

				#update asset_status_history
				$update_ash = array('end_time' => date('Y-m-d H:i:s'));
				$update_ash_where = array('asset_id' =>$asset_id);
				$this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

				$insert_ash = array(
					'asset_id'         => $asset_id,
					'status'           => $status,
					'current_stage_id' => 6,
					'tool_order_id'    => $fe2_tool_order_id,
					'created_by'	   => $this->session->userdata('sso_id'),
					'created_time'	   => date('Y-m-d H:i:s')
				);
				$this->Common_model->insert_data('asset_status_history',$insert_ash);

				#update asset position
				$update_ap = array('to_date' => date('Y-m-d H:i:s'));
				$update_ap_where = array('asset_id'=> $asset_id,'to_date'=>NULL);
				$this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);

				$fe2_sso_id = $fe2_trow['sso_id'];

				$insert_ap = array(
					'asset_id' => $asset_id,
					'transit'  => 1,
					'sso_id'   => $fe2_sso_id,
					'from_date'=> date('Y-m-d H:i:s')
				);
				$this->Common_model->insert_data('asset_position',$insert_ap);

				#audit data
				$new_data = array(
					'tool_availability' => get_asset_position($asset_id),
					'asset_status_id'   => $status
				);
				$remarks = "In Transit For Tool Order No:".$fe2_trow['order_number'];
				audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,$ad_id,array(),'tool_order',$fe2_tool_order_id,$fe2_trow['country_id']);
			}
			//closing  of order_asset_history loop

			$fe2_tool_count = $this->Pickup_point_m->get_fe2_tool_count($fe2_tool_order_id);

			$fe2_asset_count = $this->Pickup_point_m->get_fe2_asset_count($fe2_tool_order_id);

			if($fe2_tool_count == $fe2_asset_count)
			{
				$update_fe2_to = array('current_stage_id'=> 6);
				$update_fe2_to_where = array('tool_order_id' => $fe2_tool_order_id);
				$this->Common_model->update_data('tool_order',$update_fe2_to,$update_fe2_to_where);

				#TODO when FE to FE Partial sending then need to Modify
				#update avail qty in ordered_tool
				$ot_arr = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$fe2_tool_order_id,'status <',3));
				foreach ($ot_arr as $keyot => $valueot) 
				{
					$req_qty = $valueot['quantity'];
					$update_ot = array(
						'available_quantity'=> $req_qty,
						'status'            => 1,
						'remarks'           => 'FE TO FE Transfer by Courier',
						'modified_by'       => $this->session->userdata('sso_id'),
						'modified_time'     => date('Y-m-d H:i:s')
					);
					$update_ot_where = array('ordered_tool_id'=>$valueot['ordered_tool_id']);
					$this->Common_model->update_data('ordered_tool',$update_ot,$update_ot_where);
				}
			}
		}
		else if($return_details['return_type_id'] == 1 || $return_details['return_type_id'] == 2)
		{
			#audit data
			$remarks = "In Transit from FE to WH, Return Order No:".$return_details['return_number'];
			$ad_id2 = audit_data('in_transit_to_wh',$order_status_id,'order_status_history',2,'',array('Current Stage' => 'Intransit From FE to WH'),array('return_order_id'=>$return_order_id),array('Current Stage' => 'Waiting For Pickup'),$remarks,'',array('Current Stage'=>'Waiting For WH to Ack'),'tool_order',$tool_order_id,$country_id);

			$fe1_oah = $this->Common_model->get_data('order_asset_history',array('order_status_id'=>$os_id));
			foreach ($fe1_oah as $key => $value) 
			{
				$fe1_oa = $this->Common_model->get_data_row('ordered_asset',array('ordered_asset_id'=>$value['ordered_asset_id']));
				$asset_id = $fe1_oa['asset_id'];
				#audit data for pickup 
				$new_data = array('transaction_status' => 'Asset has been Picked');
				$old_data = array('transaction_status' => 'Waiting for Pickup');
				$remarks = "Asset has been picked from FE to WH for Return Order No".$return_details['return_number'];
				audit_data('tool_pickup_assets',$value['oah_id'],'order_asset_history',2,$ad_id,$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'return_order',$return_order_id,$country_id);

				#audit data for in transit to FE
				$new_data = array('transaction_status' => 'In Transit to WH');
				$old_data = array('transaction_status' => 'Asset has been Picked');
				$remarks = "In Transit from FE to WH, for Return Order No".$return_details['return_number'];
				audit_data('in_transit_to_fe_assets',$value['oah_id'],'order_asset_history',2,$ad_id2,$new_data,array('asset_id'=>$asset_id),array(),$remarks,'',array(),'return_order',$return_order_id,$country_id);
				$asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

				#audit data
				$old_data = array(
					'tool_availability' => get_asset_position($asset_id),
					'asset_status_id'   => $asset_arr['status']
				);
				#update asset
				$status = ($asset_arr['status']!=12)?8:$asset_arr['status'];
				$update_a = array(
				    'status'              => $status,
				    'availability_status' => 1,
				    'modified_by'         => $this->session->userdata('sso_id'),
				    'modified_time'       => date('Y-m-d H:i:s')
				);
				$update_a_where = array('asset_id'=>$asset_id);
				$this->Common_model->update_data('asset',$update_a,$update_a_where);

				#update asset_status_history
				$update_ash = array('end_time' => date('Y-m-d H:i:s'));
				$update_ash_where = array('asset_id' =>$asset_id);
				$this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);
				$insert_ash = array(
					'asset_id'         => $asset_id,
					'status'           => $status,
					'current_stage_id' => 9,
					'tool_order_id'    => $tool_order_id,
					'created_by'	   => $this->session->userdata('sso_id'),
					'created_time'	   => date('Y-m-d H:i:s')
				);
				$this->Common_model->insert_data('asset_status_history',$insert_ash);

				#update asset position
				$update_ap = array('to_date' => date('Y-m-d H:i:s'));
				$update_ap_where = array('asset_id'=> $asset_id,'to_date'=>NULL);
				$this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);

				$ro_to_wh_id = $return_details['ro_to_wh_id'];

				$insert_ap = array('asset_id' => $asset_id,
								   'transit'  => 1,
								   'wh_id'    => $ro_to_wh_id,
								   'from_date'=> date('Y-m-d H:i:s'));
				$this->Common_model->insert_data('asset_position',$insert_ap);

				#audit data
				$new_data = array(
					'tool_availability' => get_asset_position($asset_id),
					'asset_status_id'   => $status
				);
				$remarks = "In Transit to WH, Return Order No:".$return_details['return_number'];
				audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,$ad_id,array(),'return_order',$return_order_id,$country_id);
			}
			decideMainOrderStatus($tool_order_id);
		}

		if ($this->db->trans_status() === FALSE)
        {
			$this->db->trans_rollback();
			$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-times-circle"></i></div>
			<strong>Error!</strong> something went wrong! please check.</div>'); 
			redirect(SITE_URL.'pickup_list'); exit(); 
        }
        else
        {
			$this->db->trans_commit();
			$this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-check"></i></div>
			<strong>Success!</strong> Shipment Details Has been Submitted successfully!
			</div>'); 
            redirect(SITE_URL.'pickup_invoice_print/'.storm_encode($return_order_id)); exit();
        }
	}


	public function closed_pickup_list()
    {
        $data['nestedView']['heading']="Closed Pickup Requests";
		$data['nestedView']['cur_page'] = "closed_pickup_list";
		$data['nestedView']['parent_page'] = 'closed_pickup_list';

		#page Authentication
		$task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Closed Pickup Requests';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Pickup Requests','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchclosedpickup',TRUE));	
        if($psearch!='') 
        {
            $searchParams=array(
			'tool_order_number' => validate_string($this->input->post('tool_order_number', TRUE)),
			'ssoid'			    => validate_string($this->input->post('ssoid',TRUE)),
			'date'			    => validate_string($this->input->post('date',TRUE)),
			'return_number'	    => validate_string($this->input->post('return_number',TRUE)),
			'whid'	            => validate_string($this->input->post('whid',TRUE)),
			'country_id'	    => validate_string($this->input->post('country_id',TRUE)),
			'return_type_id'    => validate_number($this->input->post('return_type_id',TRUE))
			);
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
			if($this->uri->segment(2)!='')
            {
            	$searchParams=array(
				'tool_order_number' => $this->session->userdata('tool_order_number'),
				'ssoid'             => $this->session->userdata('ssoid'),
				'date'		        => $this->session->userdata('date'),
				'return_number'	    => $this->session->userdata('return_number'),
				'whid'       	    => $this->session->userdata('whid'),
				'country_id'	    => $this->session->userdata('country_id'),
				'return_type_id'    => $this->session->userdata('return_type_id')
				);
            }
            else 
            {
                $searchParams=array(
				'tool_order_number' => '',
				'ssoid'             => '',
				'date'				=> '',
				'return_number'		=> '',
				'page_redirect_pk'  => '',
				'whid'              => '',
				'country_id'        => '',
				'return_type_id'    => ''
                );
                $this->session->set_userdata($searchParams);
            }   
        }
        $data['search_data'] = $searchParams;

		# Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_pickup_list/';
        # Total Records
        $config['total_rows'] = $this->Pickup_point_m->closed_pickup_list_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['closed_pickup_Results'] = $this->Pickup_point_m->closed_pickup_list_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        
        # Additional data
        $data['displayResults'] = 1;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['return_type'] = $this->Common_model->get_data('return_type',array('return_type_id != '=>3));
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;
		$this->load->view('pickup/closed_pickup_list',$data);
	}

	public function view_pickup_detials()
	{
		$return_order_id = storm_decode($this->uri->segment(2));
		if($return_order_id == '')
		{
			redirect(SITE_URL.'closed_pickup_list'); exit();
		}

		$data['nestedView']['heading']="Closed FE Pickup Details";
		$data['nestedView']['cur_page'] = "closed_pickup_list";
		$data['nestedView']['parent_page'] = 'closed_pickup_list';

		#page Authentication
		$task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Closed FE Pickup Details';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Pickup Requests','class'=>'','url'=>SITE_URL.'closed_pickup_list');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Pickup Details','class'=>'active','url'=>'');

		#additional data
		$return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
		if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
		{
			$trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['ro_to_wh_id']));
			$trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
		}
		else if($return_order['return_type_id'] == 4)
		{
			$trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
			$sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
			$sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
			$trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];


		}
		$trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');


		$rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
		$rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
		$asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
		
		$data['flg'] = 1;
		$data['rrow'] = $rrow;
		$data['trrow'] = $trrow;
		$data['asset_list'] = $asset_list;
		$data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
		$data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));
		$this->load->view('pickup/closed_pickup_list',$data);
	}


	public function open_fe_returns()
	{
		$data['nestedView']['heading']="Acknowledge FE Returned Tools";
		$data['nestedView']['cur_page'] = "open_fe_returns";
		$data['nestedView']['parent_page'] = 'open_fe_returns';

		#page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Acknowledge FE Returned Tools';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge FE Returned Tools','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchpickup',TRUE));	
        $date = validate_string($this->input->post('date',TRUE));
        if($date!='')
        {
        	$arrival_date = date('Y-m-d',strtotime($date));
        }
        else
        {
        	$arrival_date = '';
        }
        
        if($psearch!='') 
        {
			$searchParams=array(
			'tool_order_number' => validate_string($this->input->post('tool_order_number', TRUE)),
			'ssoid'			    => validate_string($this->input->post('ssoid',TRUE)),
			'date'			    => $arrival_date,
			'return_number'	    => validate_string($this->input->post('return_number',TRUE)),
			'whc_id'            => validate_number(@$this->input->post('wh_id',TRUE)),
			'country_id'        => validate_number(@$this->input->post('country_id',TRUE))
			);
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
			if($this->uri->segment(2)!='')
            {
				$searchParams=array(
				'tool_order_number' => $this->session->userdata('tool_order_number'),
				'ssoid'             => $this->session->userdata('ssoid'),
				'date'		        => $this->session->userdata('date'),
				'return_number'	    => $this->session->userdata('return_number'),
				'whc_id'       	    => $this->session->userdata('whc_id'),
				'country_id'   	    => $this->session->userdata('country_id')
				);
            }
            else 
            {
				$searchParams=array(
				'tool_order_number' 	=> '',
				'ssoid'             	=> '',
				'date'					=> '',
				'return_number'			=> '',
				'page_redirect_fe_ack' 	=> '',
				'whc_id'            	=> '',
				'country_id'        	=> ''
				);
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;
		
		# Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'open_fe_returns/';
        # Total Records
        $config['total_rows'] = $this->Pickup_point_m->open_fe_returns_list_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['open_fe_returns_Results'] = $this->Pickup_point_m->open_fe_returns_list_results($current_offset, $config['per_page'], $searchParams,$task_access);
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));

        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;
        # Additional data
        $data['displayResults'] = 1;
		$this->load->view('pickup/open_fe_returns',$data);
	}

	public function acknowledge_fe_return_tools()
	{
		$return_order_id = storm_decode($this->uri->segment(2));
		$page_redirect_fe_ack = $this->session->userdata('page_redirect_fe_ack');
		if($return_order_id == '' || $page_redirect_fe_ack==1)
		{
			redirect(SITE_URL.'open_fe_returns'); exit();
		}

		$data['nestedView']['heading']="Acknowledge FE Returned Tools";
		$data['nestedView']['cur_page'] = "check_pickup";
		$data['nestedView']['parent_page'] = 'open_fe_returns';

		#page Authentication
		$task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/acknowledge_fe_return.js"></script>';

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Acknowledge FE Returned Tools';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open FE Returns','class'=>'','url'=>SITE_URL.'open_fe_returns');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge FE Returned Tools','class'=>'active','url'=>'');

		#additional data
		$rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
		$rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
		$asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
		foreach ($asset_list as $key => $value) 
		{
			$components = $this->Pickup_point_m->get_asset_components($value['asset_id'],$rrow['order_status_id']);
			$asset_list[$key]['components'] = $components;
		}

		#to select 9th stage value
		$os_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id,'current_stage_id'=>8),'order_status_id');
		if($os_id =='')
		{
			$os_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id,'current_stage_id'=>9),'order_status_id');
		}
		$order_asset = $this->Common_model->get_data('order_asset_history',array('order_status_id'=>$os_id));
		$asset_val = array();
		foreach ($order_asset as $row) 
		{
			$oahealth = $this->Common_model->get_data('order_asset_health',array('oah_id'=>$row['oah_id']));
			$assetid = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$row['ordered_asset_id']),'asset_id');
			foreach ($oahealth as $key => $value) 
			{
				$asset_val[$assetid][$value['part_id']]['asset_condition_id'] = $value['asset_condition_id'];
				$asset_val[$assetid][$value['part_id']]['remarks'] = $value['remarks'];
			}
		}

		$data['asset_val'] = $asset_val;
		$data['flg'] = 2;
		$data['rrow'] = $rrow;
		$data['asset_list'] = $asset_list;
		$data['form_action'] = SITE_URL.'submit_acknowledge_fe_returns';
		$data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>2));
		$data['docs'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));

		$this->load->view('pickup/open_fe_returns',$data);
	}

	public function submit_acknowledge_fe_returns()
	{
		#page Authentication
		$task_access = page_access_check('open_fe_returns');

		$return_order_id = validate_number(storm_decode($this->input->post('return_order_id',TRUE)));
		$tool_order_id   = validate_number(storm_decode($this->input->post('tool_order_id',TRUE)));

		$ro_arr = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
		$to_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
		$country_id = $to_arr['country_id'];
		$ro_to_wh_id = $ro_arr['ro_to_wh_id'];
		$rto_id   = validate_number(storm_decode($this->input->post('rto_id',TRUE)));
		
		if($return_order_id == '' || $tool_order_id == '' || $rto_id == '' || $ro_to_wh_id == '')
		{
			redirect(SITE_URL.'pickup_list'); exit();
		}
		$_SESSION['page_redirect_fe_ack'] = 1;
		$asset_status = $this->input->post('asset_status',TRUE);
		$asset_condition_id = $this->input->post('asset_condition_id',TRUE);
		$remarks_arr = $this->input->post('remarks',TRUE);
		$doc_type = $this->input->post('document_type',TRUE);
		$sub_inventory = $this->input->post('sub_inventory',TRUE);

		$rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
		$asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);

		$order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'rto_id'=>$rto_id,'current_stage_id'=>9),'order_status_id');

		foreach ($asset_list as $key => $value) 
		{
			$components = $this->Pickup_point_m->get_asset_components($value['asset_id'],$order_status_id);
			$asset_list[$key]['components'] = $components;
		}

		$os_id = $order_status_id;

		$this->db->trans_begin();

		#update in transit if missing
		foreach ($asset_list as $key => $value) 
		{
			if($asset_status[$value['asset_id']] == 3)
			{
				$asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$value['asset_id']));

				$ordered_asset_id = $this->Common_model->get_value('ordered_asset',array('asset_id'=>$value['asset_id']),'ordered_asset_id');
				$update_order_asset = array('status'=>3,'remarks'=>'Missing is identified at wh');
				$update_where1 = array('ordered_asset_id'=>$ordered_asset_id,'order_status_id'=>$os_id);
				// modified by maruthi on 7th dec'17
				$this->Common_model->update_data('order_asset_history',$update_order_asset,$update_where1);

				#insert defective asset
				$insert_defective_asset = array(
					'asset_id'         => $value['asset_id'],
					'type'	           => 2,
					'current_stage_id' => 9,
					'wh_id'	   		   => $ro_to_wh_id,
					'created_by'       => $this->session->userdata('sso_id'),
					'created_time'     => date('Y-m-d H:i:s'),
					'trans_id'		   => $tool_order_id,
					'status'           => 1);
				$defective_asset_id = $this->Common_model->insert_data('defective_asset',$insert_defective_asset);

				foreach ($value['components'] as $key => $val)
				{
					$remarks1 = $remarks_arr[$value['asset_id']][$val['part_id']];
					if($remarks1 == ''){ $remarks1 = NULL; }
					
					$insert_dhealth = array(
						'defective_asset_id' => $defective_asset_id,
						'part_id'            => $val['part_id'],
						'asset_condition_id' => 3,
						'remarks'			 => $remarks1);
					$this->Common_model->insert_data('defective_asset_health',$insert_dhealth);
				}

				#update asset waiting for admin approval
				#audit data
				$old_data = array(get_da_key() => get_da_status($value['asset_id'],$asset_arr['approval_status']));

				#update asset
				$update_a = array(
				    'approval_status'     => 1,
				    'modified_by'         => $this->session->userdata('sso_id'),
				    'modified_time'       => date('Y-m-d H:i:s')
				);
				$update_a_where = array('asset_id'=>$value['asset_id']);
				$this->Common_model->update_data('asset',$update_a,$update_a_where);

				#audit data
				$new_data =array(get_da_key() =>get_da_status($value['asset_id'],1));

				$remarks = "identified As missed asset in Wh while ack Return Order No:".$ro_arr['return_number'];
				audit_data('asset_master',$value['asset_id'],'asset',2,'',$new_data,array('asset_id'=>$value['asset_id']),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$country_id);

				#send email notification
				sendDefectiveOrMissedAssetMail($defective_asset_id,2,$country_id,2,$tool_order_id); 
			}
		}
		#update order_status_history
		$update_osh = array('end_time' => date('Y-m-d H:i:s'));
		$update_where3 = array('rto_id'=>$rto_id,'current_stage_id'=>9,'end_time'=>NULL);
		$this->Common_model->update_data('order_status_history',$update_osh,$update_where3);

		$insert_osh = array(
			'rto_id'           => $rto_id,
		    'current_stage_id' => 10,
		    'tool_order_id'    => $tool_order_id,
		    'created_by'       => $this->session->userdata('sso_id'),
		    'created_time'     => date('Y-m-d H:i:s')
		);
		$order_status_id = $this->Common_model->insert_data('order_status_history',$insert_osh);

		#audit data
		$old_data = array('current stage'=>'Intransit to WH');
		$new_data = array('current stage'=>'Acknowledged at WH');
		$remarks = "Acknowledged At wh for FE Return Order:".$ro_arr['return_number'];
		$ad_id = audit_data('ack_fe_return',$order_status_id,'order_status_history',2,'',$new_data,array('return_order_id'=>$return_order_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$country_id);

		#insert asset - health 
		foreach ($asset_list as $row) 
		{
			$count = 0;
			$asset_id = $row['asset_id'];
			if($asset_status[$row['asset_id']] == 1)
			{
				
				$asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

				foreach ($row['components'] as $key => $value) 
				{
					$asset_condition = $asset_condition_id[$asset_id][$value['part_id']];
					if($asset_condition != 1)
					{
						$count++;
					}
				}
				if($count>0)
				{
					$status = 2;
				}
				else
				{
					$status = 1;
				}
				$ordered_asset_id = $this->Common_model->get_value('ordered_asset',array('asset_id'=>$asset_id),'ordered_asset_id');
				$insert_oahistory = array(
					'ordered_asset_id' => $ordered_asset_id,
					'order_status_id'  => $order_status_id,
					'created_by'       => $this->session->userdata('sso_id'),
					'created_time'     => date('Y-m-d H:i:s'),
					'status'		   => $status
				);
				$oah_id = $this->Common_model->insert_data('order_asset_history',$insert_oahistory);

				#audit data
				$new_data = array('transaction_status' => 'Received at Wh');
				$old_data = array('transaction_status' => 'In Transit to WH');
				$remarks = "received at wh while acknowledging return order:".$ro_arr['return_number'];
				audit_data('fe_return_assets',$oah_id,'order_asset_history',2,$ad_id,$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'return_order',$return_order_id,$country_id);

				if($count>0)
				{
					$check = $asset_arr['approval_status'];
					if($check == 0)
					{
						$insert_defective_asset = array(
							'asset_id'         => $asset_id,
							'type'	           => 1,
							'current_stage_id' => 9,
							'wh_id'			   => $ro_to_wh_id,
							'created_by'       => $this->session->userdata('sso_id'),
							'created_time'     => date('Y-m-d H:i:s'),
							'trans_id'		   => $tool_order_id,
							'status'           => 1
						);
						$defective_asset_id = $this->Common_model->insert_data('defective_asset',$insert_defective_asset);
					}
				}
				foreach ($row['components'] as $key => $value) 
				{
					$remarks1 = $remarks_arr[$asset_id][$value['part_id']];
					if($remarks1 == ''){ $remarks1 = NULL; }
					$acondition = $asset_condition_id[$asset_id][$value['part_id']];
					$insert_oassethealth = array(
						'asset_condition_id' => $acondition,
						'remarks'            => $remarks1,
						'oah_id'             => $oah_id,
						'part_id'            => $value['part_id']);
					$this->Common_model->insert_data('order_asset_health',$insert_oassethealth);

					if($count>0)
					{
						$check = $asset_arr['approval_status'];
						if($check == 0)
						{
						$insert_dhealth = array(
							'defective_asset_id' => $defective_asset_id,
							'part_id'            => $value['part_id'],
							'asset_condition_id' => $acondition,
							'remarks'			 => $remarks1
						);
						$this->Common_model->insert_data('defective_asset_health',$insert_dhealth);
						}
					}	
				}
				if($count>0)
				{
					$check = $asset_arr['approval_status'];
					if($check == 0)
					{
						#send Notification To Admin
                    	sendDefectiveOrMissedAssetMail($defective_asset_id,1,$country_id,2,$tool_order_id);
					}
				}
				#audit data
				$old_data = array(
					'wh_id'             => $asset_arr['wh_id'],
					'sub_inventory'     => $asset_arr['sub_inventory'],
					get_da_key()        => get_da_status($asset_id,$asset_arr['approval_status']),
					'asset_status_id'   => $asset_arr['status'],
					'tool_availability' => get_asset_position($asset_id)
				);

				#update asset position as In Ware House
				$update_asset_position = array('to_date' => date('Y-m-d H:i:s'));
				$update_where2 = array('asset_id' => $asset_id);
				$this->Common_model->update_data('asset_position',$update_asset_position,$update_where2);

				$insert_asset_position = array(
					'asset_id' => $asset_id,
					'transit'  => 0,
					'wh_id'	   => $ro_to_wh_id,
					'from_date'=> date('Y-m-d H:i:s'),
					'status'   => 1
				);
				$this->Common_model->insert_data('asset_position',$insert_asset_position);

				$sub_inventory_location = $sub_inventory[$asset_id];
				if($sub_inventory_location == '') 
				{ 
					$sub_inventory_location = NULL;
				}

				#update asset
				$approval_status = ($count>0)?1:0;
				$update_asset = array(
					'wh_id'           => $ro_to_wh_id,
					'status'          => 1,
					'approval_status' => $approval_status,
					'sub_inventory'   => $sub_inventory_location,
					'modified_by'     => $this->session->userdata('sso_id'),
					'modified_time'   => date('Y-m-d H:i:s')
				);
				$update_where_asset = array('asset_id'=> $asset_id);
				$this->Common_model->update_data('asset',$update_asset,$update_where_asset);

				#audit data
				$new_data = array(
					'wh_id'             => $ro_to_wh_id,
					'sub_inventory'     => $sub_inventory_location,
					get_da_key()        => get_da_status($asset_id,$approval_status),
					'asset_status_id'   => 1,
					'tool_availability' => get_asset_position($asset_id)
				);
				if($approval_status == 1)
				{
					$remarks = "identified As Defective asset in Wh while ack Return Order No:".$ro_arr['return_number'];
				}
				else
				{
					$remarks = "Acknowledged Asset at Wh, return Order No:".$ro_arr['return_number'];
				}
				
				audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$country_id);

				#update and insert asset status history
				$update_asset_status = array('end_time' => date('Y-m-d H:i:s'));
				$update_wer_as = array('asset_id' => $asset_id,'end_time'=>NULL);
				$this->Common_model->update_data('asset_status_history',$update_asset_status,$update_wer_as);

				$insert_asset_status   = array(
					'asset_id'     => $asset_id,
					'status'       => 1,
					'created_by'   => $this->session->userdata('sso_id'),
					'created_time' => date('Y-m-d H:i:s'));
				$this->Common_model->insert_data('asset_status_history',$insert_asset_status);
			}
			else
			{
				$ordered_asset_id = $this->Common_model->get_value('ordered_asset',array('asset_id'=>$asset_id),'ordered_asset_id');
				$insert_oahistory = array(
					'ordered_asset_id' => $ordered_asset_id,
					'order_status_id'  => $order_status_id,
					'created_by'       => $this->session->userdata('sso_id'),
					'created_time'     => date('Y-m-d H:i:s'),
					'status'		   => 3);
				$oah_id = $this->Common_model->insert_data('order_asset_history',$insert_oahistory);

				#audit data
				$new_data = array('transaction_status' => 'Not Received');
				$old_data = array('transaction_status' => 'In Transit to WH');
				$remarks = "Asset is identified as missed, while acknowledging return order:".$ro_arr['return_number'];
				audit_data('fe_return_assets',$oah_id,'order_asset_history',2,$ad_id,$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'return_order',$return_order_id,$country_id);

				foreach ($row['components'] as $key => $value) 
				{
					$remarks1 = $remarks_arr[$asset_id][$value['part_id']];
					
					if($remarks1 == ''){ $remarks1 = NULL; }
					$acondition = $asset_condition_id[$asset_id][$value['part_id']];
					$insert_oassethealth = array(
						'asset_condition_id' => 3,
						'remarks'            => $remarks1,
						'oah_id'             => $oah_id,
						'part_id'            => $value['part_id']);
					$this->Common_model->insert_data('order_asset_health',$insert_oassethealth);						
				}
			}
		}

		#insert attached document in return_doc table
		if(count($doc_type)>0)
		{
			foreach ($doc_type as $key => $value) 
	        {
	            if($_FILES['support_document_'.$key]['name']!='')
	            {
	                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
	                $config['upload_path'] = asset_document_path();
	                $config['allowed_types'] = 'gif|jpg|png|pdf';
	         
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                $this->upload->do_upload('support_document_'.$key);
	                $fileData = $this->upload->data();
	                $support_document = $config['file_name'];
	            }
	            else
	            {
	                $support_document = '';
	            }
	            $insert_doc = array(
	            	'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'		   => $_FILES['support_document_'.$key]['name'],
                    'return_order_id'  => $return_order_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
	            if($value!='' && $support_document != '')
	            {
	                $this->Common_model->insert_data('return_doc',$insert_doc);
	            }
	        }
		}

		#update return order with received date
		$updatereturnorder = array(
			'received_date' => date('Y-m-d H:i:s'),
			'status'        => 10
		);
		$updaterowhere = array('return_order_id' => $return_order_id);
		$this->Common_model->update_data('return_order',$updatereturnorder,$updaterowhere);

		#update tool order if previous status is 9
		
		$fe1AuditOldCurrentStage = array('current_stage_id'=> $to_arr['current_stage_id']);
		$ninthAndtemthStatus = decideMainOrderStatus($tool_order_id);
        if($ninthAndtemthStatus[1] == 1)        
            $fe1AuditNewCurrentStage = 10;
        else if($ninthAndtemthStatus[2]==1)
            $fe1AuditNewCurrentStage = 9;
        if(isset($fe1AuditNewCurrentStage))
        {
            $finalArr = array('current_stage_id'=> $fe1AuditNewCurrentStage);
            $blockArr = array('blockName'=> "Tool Order");
            $remarksString = "Acknowledgement Done for ".$ro_arr['return_number']." with Order Number ".$to_arr['order_number'];
            tool_order_audit_data("tool_order",$to_arr['tool_order_id'],"tool_order",2,'',$finalArr,$blockArr,$fe1AuditOldCurrentStage,$remarksString,'',array(),"tool_order",$to_arr['tool_order_id'],$to_arr['country_id']);
        }

		if ($this->db->trans_status() === FALSE)
        {
			$this->db->trans_rollback();
			$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-times-circle"></i></div>
			<strong>Error!</strong> something went wrong! please check.</div>'); 
			redirect(SITE_URL.'open_fe_returns'); exit(); 
        }
        else
        {
			$return_number = $this->Common_model->get_value('return_order',array('return_order_id'=>$return_order_id),'return_number');
			$this->db->trans_commit();
			$this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-check"></i></div>
			<strong>Success!</strong> Acknowledgement has been successfully captured for Return Number : <strong> '.$return_number.'</strong> </div>');
			redirect(SITE_URL.'closed_fe_returns'); exit();
        }
	}

	public function closed_fe_returns()
    {
    	$data['nestedView']['heading']="Closed Acknowledged FE Returns";
		$data['nestedView']['cur_page'] = "closed_fe_returns";
		$data['nestedView']['parent_page'] = 'closed_fe_returns';

		#page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Closed Acknowledged FE Returns';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Acknowledged FE Returns','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchpickup',TRUE));
        $date = validate_string($this->input->post('date',TRUE));
        if($date!='')
        {
        	$return_date = date('Y-m-d',strtotime($date));
        }
        else
        {
        	$return_date = '';
        }
        if($psearch!='') 
        {
            $searchParams=array(
			'tool_order_number' => validate_string($this->input->post('tool_order_number', TRUE)),
			'ssoid'			    => validate_string($this->input->post('ssoid',TRUE)),
			'date'			    => $return_date,
			'return_number'	    => validate_string($this->input->post('return_number',TRUE)),
			'whc_id'            => validate_number(@$this->input->post('wh_id',TRUE)),
			'country_id'        => validate_number(@$this->input->post('country_id',TRUE))
			);
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
			if($this->uri->segment(2)!='')
            {
            	$searchParams=array(
				'tool_order_number' => $this->session->userdata('tool_order_number'),
				'ssoid'             => $this->session->userdata('ssoid'),
				'date'		        => $this->session->userdata('date'),
				'return_number'	    => $this->session->userdata('return_number'),
				'whc_id'       	    => $this->session->userdata('whc_id'),
				'country_id'   	    => $this->session->userdata('country_id')
				);
            
			}
            else 
            {
                $searchParams=array(
				'tool_order_number' => '',
				'ssoid'             => '',
				'date'				=> '',
				'return_number'		=> '',
				'whc_id'            => '',
				'country_id'        => ''
				);
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;

		# Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_fe_returns/';
        # Total Records
        $config['total_rows'] = $this->Pickup_point_m->closed_fe_returns_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['closed_fe_returns_Results'] = $this->Pickup_point_m->closed_fe_returns_results($current_offset, $config['per_page'], $searchParams,$task_access);
        //echo $this->db->last_query(); exit;

        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;
        # Additional data
        $data['displayResults'] = 1;
		$this->load->view('pickup/closed_fe_returns',$data);
	}

	public function view_closed_fe_returns()
	{
		$return_order_id = storm_decode($this->uri->segment(2));
		if($return_order_id == '')
		{
			redirect(SITE_URL.'closed_fe_returns'); exit();
		}

		$data['nestedView']['heading']="Closed Acknowledged FE Return Details";
		$data['nestedView']['cur_page'] = "closed_fe_returns";
		$data['nestedView']['parent_page'] = 'closed_fe_returns';

		#page Authentication
		$task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/acknowledge_fe_return.js"></script>';
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Closed Acknowledged FE Return Details';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Acknowledged FE Returns','class'=>'','url'=>SITE_URL.'closed_fe_returns');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Acknowledged FE Return Details','class'=>'active','url'=>'');

		$return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
		
		if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
		{
			$trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['ro_to_wh_id']));
			$trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
		}
		else if($return_order['return_type_id'] == 4)
		{
			$trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
			$sso_id = $this->Common_model->get_value('tool_order_id',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
			$sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
			$trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];
		}
		$trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');

		$rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
		$rrow = $this->Pickup_point_m->get_closed_return_details($return_order_id,$rto_id);
		$asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
		foreach ($asset_list as $key => $value) 
		{
			$components = $this->Pickup_point_m->get_asset_components($value['asset_id'],$rrow['order_status_id']);
			$asset_list[$key]['components'] = $components;
		}
		#to select 9th stage value
		$os_id = $rrow['order_status_id'];
		$order_asset = $this->Common_model->get_data('order_asset_history',array('order_status_id'=>$os_id));
		$asset_val = array();
		foreach ($order_asset as $row) 
		{
			$oahealth = $this->Common_model->get_data('order_asset_health',array('oah_id'=>$row['oah_id']));
			$assetid = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$row['ordered_asset_id']),'asset_id');
			foreach ($oahealth as $key => $value) 
			{
				$asset_val[$assetid]['asset_status'] = $row['status'];
				$asset_val[$assetid][$value['part_id']]['asset_condition_id'] = $value['asset_condition_id'];
				$asset_val[$assetid][$value['part_id']]['remarks'] = $value['remarks'];

			}
		}
		
		$data['flg'] = 1;
		$data['rrow'] = $rrow;
		$data['trrow'] = $trrow;
		$data['asset_list'] = $asset_list;
		$data['asset_val'] = $asset_val;
		$data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
		$data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));
		$this->load->view('pickup/closed_fe_returns',$data);
	}

	public function pickup_invoice_print()
    {
        $return_order_id = storm_decode($this->uri->segment(2));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'closed_pickup_list'); exit();
        }

        $_SESSION['page_redirect_pk'] = 1;
		$from_address = $this->Pickup_point_m->get_from_address($return_order_id);
		$data['currency_name'] = $from_address['currency_name'];
        $return_type_id = $from_address['return_type_id'];

		if($return_type_id == 1 || $return_type_id == 2)
		{
			$ro_to_wh_id = $this->Common_model->get_value('return_order',array('return_order_id'=>$return_order_id),'ro_to_wh_id');
			$data['to_address'] = $this->Pickup_point_m->get_to_address1($ro_to_wh_id);
		}
		else if($return_type_id == 4)
		{
			$data['to_address'] = $this->Pickup_point_m->get_to_address2($return_order_id);
		}
		$to_address = $data['to_address'];
		$rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
		$order_status_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id,'current_stage_id'=>8),'order_status_id');
		$tools_list = $this->Pickup_point_m->get_requested_tools($order_status_id);
		// created by maruhti on 7th dec'17
		$assets_list = $this->Pickup_point_m->get_requested_assets($order_status_id);
		$total_amount = 0; $tax_amount = 0;
		foreach ($tools_list as $key => $value)
		{
			$cost = $value['cost_in_inr'];
			$gst_percent = $value['gst_percent'];
			$gst = explode("%", $gst_percent);
			$cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
            if($from_address['print_type'] == 3)
            {
                $cgst_percent = round($gst[0]/2,2);
                $cgst_amt = ($cost*$cgst_percent)/100;
                $sgst_percent = $cgst_percent;
                $sgst_amt = $cgst_amt; 
            }
            else if($from_address['print_type'] == 4)
            {
                $igst_percent = $gst[0];
                $igst_amt = ($cost*$igst_percent)/100;   
            }
            $tax_amount = ($cost * $gst[0])/100;
            $amount = $cost+$tax_amount;
            $total_amount+= ($cost+$tax_amount);
            $tools_list[$key]['cgst_percent'] = $cgst_percent;
            $tools_list[$key]['cgst_amt'] = $cgst_amt;
            $tools_list[$key]['sgst_percent'] = $sgst_percent;
            $tools_list[$key]['sgst_amt'] = $sgst_amt;
            $tools_list[$key]['igst_percent'] = $igst_percent;
            $tools_list[$key]['igst_amt'] = $igst_amt;
            $tools_list[$key]['taxable_value'] = $cost;
            $tools_list[$key]['tax_amount'] = $tax_amount;
            $tools_list[$key]['amount'] = $amount;
		}
		$data['total_amount'] = $total_amount;
		$data['from_address'] = $from_address;
		$data['tools_list'] = $tools_list;
		$data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['billed_to']));
		// created by maruthi on 7th dec'17
		if($return_type_id == 4)
		{
			sendPickupMailToFE2($from_address,$to_address,$assets_list);
		}
		if($return_type_id ==1 || $return_type_id == 2)
		{
			#Sending mail to FE when pickup point Action is taken
        	pickuppointactiontaken($return_order_id,$from_address,$to_address);
		}
		$this->load->view('pickup/dc_print',$data);
    }

    public function cancel_pickup_print()
    {
        $return_order_id = storm_decode($this->uri->segment(2));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'closed_pickup_list'); exit();
        }
        $data['nestedView']['heading']="Cancel Generated Print";
        $data['nestedView']['cur_page'] = "check_pickup";
        $data['nestedView']['parent_page'] = 'closed_pickup_list';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Cancel Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Pickup Request','class'=>'','url'=>SITE_URL.'closed_pickup_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Cancel Generated Print','class'=>'active','url'=>'');
        $data['cancel_type_list'] = $this->Common_model->get_data('cancel_type',array('status'=>1));
        $ro = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        $data['print_type'] = $this->Common_model->get_value('print_format',array('print_id'=>$ro['print_id']),'print_type');
        $data['format_number'] = $this->Common_model->get_value('print_format',array('print_id'=>$ro['print_id']),'format_number');
        $data['ro'] = $ro;
        $this->load->view('pickup/cancel_pickup_print',$data);
    }

    public function insert_cancel_pickup_print()
    {
    	#page Authentication
    	$task_access = page_access_check('closed_pickup_list');

        $return_order_id = validate_number(storm_decode($this->input->post('return_order_id',TRUE)));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'closed_pickup_list'); exit;
        }

        $update_print_history = array(
        	'remarks'       => validate_string($this->input->post('reason',TRUE)),
			'modified_by'   => $this->session->userdata('sso_id'),
			'modified_time' => date('Y-m-d H:i:s'));
        $update_print_where = array('return_order_id'=>$return_order_id,'modified_time'=>NULL);
        $this->db->trans_begin();
        $this->Common_model->update_data('print_history',$update_print_history,$update_print_where);

        $print_type = validate_number($this->input->post('print_type',TRUE));
        $rt = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        $tool_order_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'tool_order_id');
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $country_id = $trow['country_id'];
        
        #audit data
        $old_data = array('print_id' => $rt['print_id']);

        $wh_id = $rt['wh_id'];
        $print_id = get_current_print_id($print_type,$wh_id);

        $update_ro = array(
        	'print_id'      => $print_id,
			'modified_by'   => $this->session->userdata('sso_id'),
			'modified_time' => date('Y-m-d H:i:s')
		);
        $update_ro_where = array('return_order_id'=>$return_order_id);
        $this->Common_model->update_data('return_order',$update_ro,$update_ro_where);

        #audit data
        $new_data = array('print_id' => $print_id);
        $remarks = "Updated Print Format for Return Order No:".$rt['return_number'];
        audit_data('tool_pickup',$return_order_id,'return_order',2,'',$new_data,array('return_order_id'=>$return_order_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$country_id);

        $insert_print_history = array(
        	'print_id' => $print_id,
			'remarks'  => 'Print Got updated',
			'return_order_id' => $return_order_id,
			'created_by' => $this->session->userdata('sso_id'),
			'created_time' => date('Y-m-d H:i:s')
		);
        $this->Common_model->insert_data('print_history',$insert_print_history);
        if ($this->db->trans_status() === FALSE)
        {
			$this->db->trans_rollback();
			$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-times-circle"></i></div>
			<strong>Error!</strong> something went wrong! please check.</div>'); 
			redirect(SITE_URL.'closed_pickup_list'); 
			exit(); 
        }
        else
        {
			$this->db->trans_commit();
			$this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-check"></i></div>
			<strong>Success!</strong> Shipment Print Has been Updated successfully!
			</div>'); 
			redirect(SITE_URL.'pickup_invoice_print/'.storm_encode($return_order_id)); 
        } 
    }

    public function update_pickup_print()
    {
        $return_order_id = storm_decode($this->uri->segment(2));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'closed_pickup_list'); exit();
        }
        $data['nestedView']['heading']="Update Generated Print";
        $data['nestedView']['cur_page'] = "check_pickup";
        $data['nestedView']['parent_page'] = 'closed_pickup_list';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/update_pickup.js"></script>';

        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Pickup Requests','class'=>'','url'=>SITE_URL.'closed_pickup_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Generated Print','class'=>'active','url'=>'');

        $ro = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        if($ro['return_type_id'] == 1 || $ro['return_type_id'] == 2)
		{
			$trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$ro['ro_to_wh_id']));
			$trrow['check_val'] = 0;
		}
		else if($ro['return_type_id'] == 4)
		{
			$trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$ro['tool_order_id']));
			$trrow['check_val'] = 1;
		}
		$rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
		$rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
        $data['print_type'] = $this->Common_model->get_value('print_format',array('print_id'=>$ro['print_id']),'print_type');
        $data['format_number'] = $this->Common_model->get_value('print_format',array('print_id'=>$ro['print_id']),'format_number');
        $data['print_date'] = $this->Common_model->get_value('print_format',array('print_id'=>$ro['print_id']),'created_time');
        $data['ro'] = $ro;
        $data['rrow'] = $rrow;
        $data['trrow'] = $trrow;
        $swh_id = $rrow['wh_id'];
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$swh_id));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$rrow['country_id']));
        $this->load->view('pickup/update_pickup_print',$data);
    }

    public function update_pickup_print_details()
    {
    	#page Authentication
    	$task_access = page_access_check('closed_pickup_list');

        $return_order_id = validate_number(storm_decode($this->input->post('return_order_id',TRUE)));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'closed_pickup_list'); exit();
        }

        $courier_type = validate_number($this->input->post('courier_type',TRUE));
        $courier_name = validate_string($this->input->post('courier_name',TRUE));
        $courier_number = validate_string($this->input->post('courier_number',TRUE));
        $contact_person = validate_string($this->input->post('contact_person',TRUE));
        $phone_number = validate_string($this->input->post('phone_number',TRUE));
        $expected_arrival_date = validate_string(format_date($this->input->post('expected_delivery_date',TRUE)));
        $vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
        $remarks        = validate_string($this->input->post('remarks',TRUE));
        $date = validate_string($this->input->post('print_date',TRUE));
        $print_date = date('Y-m-d',strtotime($date));
        if($remarks == ""){ $remarks = NULL; }

        if($courier_type == 1)
        {
            $phone_number   = NULL;
            $contact_person = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 2)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 3)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
        }
        $rt = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        $tool_order_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'tool_order_id');
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $country_id = $trow['country_id'];
        $update_details = array(
			'courier_type'  => $courier_type,
			'contact_person'=> $contact_person,
			'phone_number'  => $phone_number,
			'courier_name'  => $courier_name,
			'courier_number'=> $courier_number,
			'vehicle_number'=> $vehicle_number,
			'expected_arrival_date'=> $expected_arrival_date,
			'remarks'       => $remarks,
			'billed_to'		=> validate_number($this->input->post('billed_to',TRUE)),
			'address1'      => validate_string($this->input->post('add1',TRUE)),
			'address2'      => validate_string($this->input->post('add2',TRUE)),
			'address3'      => validate_string($this->input->post('add3',TRUE)),
			'address4'      => validate_string($this->input->post('add4',TRUE)),
			'zip_code'      => validate_string($this->input->post('pin1',TRUE)),
			'gst_number'    => validate_string($this->input->post('gst1',TRUE)),
			'pan_number'    => validate_string($this->input->post('pan1',TRUE)),
			'modified_by'   => $this->session->userdata('sso_id'),
			'modified_time' => date('Y-m-d H:i:s')
		);
        $update_details_where = array('return_order_id'=>$return_order_id);
        $this->db->trans_begin();
        $this->Common_model->update_data('return_order',$update_details,$update_details_where);

        #audit data
        unset($update_details['modified_by'],$update_details['modified_time']);
        $final_arr = array_diff_assoc($update_details, $rt);
        if(count($final_arr)>0)
        {
        	$ad_id = audit_data('tool_pickup',$return_order_id,'return_order',2,'',$final_arr,array('return_order_id'=>$return_order_id),$rt,$remarks,'',array(),'tool_order',$tool_order_id,$country_id);
        }
        
        $print_id = $rt['print_id'];

        #update order address if return type is 4
        if($rt['return_type_id']==4)
        {
        	$fe2_tool_order_id = $rt['tool_order_id'];
        	$trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe2_tool_order_id));

        	$old_oa = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$fe2_tool_order_id,'status'=>1));
        	$update_oa_fe2 = array(
        	'address1' => validate_string($this->input->post('address1',TRUE)),
			'address2' => validate_string($this->input->post('address2',TRUE)),
			'address3' => validate_string($this->input->post('address3',TRUE)),
			'address4' => validate_string($this->input->post('address4',TRUE)),
			'pin_code' => validate_string($this->input->post('pin_code',TRUE)),
			'gst_number' => validate_string($this->input->post('gst_number',TRUE)),
			'pan_number' => validate_string($this->input->post('pan_number',TRUE)));
			$update_oa_fe2_where = array('tool_order_id'=>$fe2_tool_order_id);
			$this->Common_model->update_data('order_address',$update_oa_fe2,$update_oa_fe2_where);

			#audit data
			$final_arr = array_diff_assoc($update_oa_fe2, $old_oa);
			if(count($final_arr)>0)
			{
				if(!isset($ad_id))
				{
					$ad_id = audit_data('tool_pickup',$return_order_id,'return_order',2,'',array(),array('return_order_id'=>$return_order_id),array(),'','',array(),'tool_order',$tool_order_id,$country_id);
				}

				$remarks = "Updated Ship To address for Tool order No:".$trow['order_number']." while pickup arranged for return no:".$rt['return_number'];

				audit_data('pickup_fe2_order_address',$old_oa['order_address_id'],'
					order_address',2,$ad_id,$final_arr,array('tool_order_id'=>$fe2_tool_order_id),$old_oa,$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);
			}
        }
        #update print date
        update_print_format_date($print_date,$print_id);

        if ($this->db->trans_status() === FALSE)
        {
			$this->db->trans_rollback();
			$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-times-circle"></i></div>
			<strong>Error!</strong> something went wrong! please check.</div>'); 
			redirect(SITE_URL.'closed_pickup_list'); exit(); 
        }
        else
        {
			$this->db->trans_commit();
			$this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-check"></i></div>
			<strong>Success!</strong> Shipment Details Has been Updated successfully!
			</div>'); 
            redirect(SITE_URL.'pickup_invoice_print/'.storm_encode($return_order_id)); 
        } 
    }

    public function download_open_fe_returns()
    {
    	#page authentication
        $task_access = page_access_check('open_fe_returns');

    	if(validate_number($this->input->post('download_open_fe_returns',TRUE))==1)
        {
        	$date = validate_string($this->input->post('date',TRUE));
	        if($date!='')
	        {
	        	$arrival_date = date('Y-m-d',strtotime($date));
	        }
	        else
	        {
	        	$arrival_date = '';
	        }
	    	$searchParams=array(
				'tool_order_number' => validate_string($this->input->post('tool_order_number', TRUE)),
				'ssoid'			    => validate_string($this->input->post('ssoid',TRUE)),
				'date'			    => $arrival_date,
				'return_number'	    => validate_string($this->input->post('return_number',TRUE)),
				'whc_id'            => validate_number(@$this->input->post('wh_id',TRUE)),
				'country_id'        => validate_number(@$this->input->post('country_id',TRUE))
			);

	        $result_data = $this->Pickup_point_m->download_open_fe_returns($searchParams,$task_access);

	        $header = '';
            $data ='';
            $titles = array('S.No.','Return Number','SSO Detail','Order Number','Need to Ack by Warehouse','Pickup Point','Expected Arrival Date','Tool Number','Tool Description','Asset Number','Tool Availability','Asset Status','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            
            $data.='<tbody>';
            if(count($result_data)>0)
            {
            	$sno = 1;
                foreach($result_data as $row)
                {
                    $assets_arr = $this->Pickup_point_m->get_assets_in_open_fe_returns($row['return_order_id']);
                    $asset_count = count($assets_arr);
                    $exp_date = full_indian_format($row['expected_arrival_date']);
                    $data.='<tr>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$sno++.'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['return_number'].'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['sso_name'].'</td>';  
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.@$row['order_number'].'</td>'; 
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$row['wh_name'].'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$row['pickup_address'].'</td>';                    
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$exp_date.'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['part_number'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['part_description'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['asset_number'].'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.get_asset_position($assets_arr[0]['asset_id']).'</td>';
                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[0]['asset_status'].'</td>';
                    $data.='<td rowspan="'.$asset_count.'" style="vertical-align: middle; text-align: center;">'.$row['country'].'</td>';
                   	$data.='</tr>';

                   	if($asset_count>1)
                   	{
                   		for($i=1;$i<$asset_count;$i++)
                   		{
                   			$data.='<tr>';
			                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['part_number'].'</td>';
                                $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['part_description'].'</td>';
			                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['asset_number'].'</td>';
			                    $data.='<td style="vertical-align: middle; text-align: center;">'.get_asset_position($assets_arr[$i]['asset_id']).'</td>';
			                    $data.='<td style="vertical-align: middle; text-align: center;">'.$assets_arr[$i]['asset_status'].'</td>';
			                $data.='</tr>';
                   		}
                   	}
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            //echo $data;exit;
            //echo $data;exit;
            $time = date("Y-m-d H:i:s");
            $xlFile='Open_fe_returns'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
	    }
    }
}