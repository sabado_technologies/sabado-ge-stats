<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Install_base extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Install_base_model');
	}

	public function install_base()
    {
        $data['nestedView']['heading']="Manage Install Base Details";
		$data['nestedView']['cur_page'] = "install_base";
		$data['nestedView']['parent_page'] = 'install_base';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Manage Install Base Details';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Install Base Details','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchinstallbase',TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'customer_name'   => validate_string($this->input->post('customer_name', TRUE)),
            'site_id'         => validate_string($this->input->post('site_id',TRUE)),
            'system_id'       => validate_string($this->input->post('system_id',TRUE)),
            'customer_number' => validate_string($this->input->post('customer_number',TRUE)),
            'asset_number'    => validate_string($this->input->post('asset_number',TRUE)),
            'status'          => validate_string($this->input->post('status',TRUE)),
            'country_id'      => validate_number($this->input->post('country_id'))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'customer_name'   => $this->session->userdata('customer_name'),
                'site_id'         => $this->session->userdata('site_id'),
                'system_id'       => $this->session->userdata('system_id'),
                'customer_number' => $this->session->userdata('customer_number'),
                'asset_number'    => $this->session->userdata('asset_number'),
                'status'          => $this->session->userdata('status'),
                'country_id'      => $this->session->userdata('country_id'));
            }
            else 
            {
                $searchParams=array(
                    'customer_name'   => '',
                    'site_id'         => '',
                    'system_id'       => '',
                    'customer_number' => '',
                    'asset_number'    => '',
                    'status'          => '',
                    'country_id'      => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'install_base/';
        # Total Records
        $config['total_rows'] = $this->Install_base_model->ib_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['ibResults'] = $this->Install_base_model->ib_results($current_offset, $config['per_page'], $searchParams,$task_access);
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        
        # Additional data
        $data['displayResults'] = 1;
        $this->load->view('install_base/install_base_view',$data);

    }
    public function add_install_base()
    {
        $data['nestedView']['heading']="Add New Install Base Details";
		$data['nestedView']['cur_page'] = 'check_install_base';
		$data['nestedView']['parent_page'] = 'install_base';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Select Country";
                $data['nestedView']['cur_page'] = 'check_manage_user';
                $data['nestedView']['parent_page'] = 'install_base';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Install Base','class'=>'','url'=>SITE_URL.'install_base');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_install_base';
                $data['cancel'] = SITE_URL.'install_base';
                $this->load->view('user/intermediate_page',$data); 
            }
        }
        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/install_base.js"></script>';

            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New Install Base Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage IB','class'=>'','url'=>SITE_URL.'install_base');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New IB Details','class'=>'active','url'=>'');

            # Additional data
            $data['modalityList'] = $this->Common_model->get_data('modality',array('status' => 1));
            $data['country_id'] = $country_id;
            
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['locationList'] = $this->Install_base_model->get_location_area($country_id);
            $data['flg'] = 1;
            $data['form_action'] = SITE_URL.'insert_install_base';
            $data['displayResults'] = 0;
            $this->load->view('install_base/install_base_view',$data);
        }
		
    }

    public function insert_install_base()
    {
        #page authentication
        $task_access = page_access_check('install_base');

        if(validate_string($this->input->post('submit_install_base',TRUE))!= '')
        {  
            $customer_number = validate_string(trim($this->input->post('customer_number',TRUE)));   
            $customer_name = validate_string(trim($this->input->post('customer_name',TRUE)));
            $service_region = validate_string(trim($this->input->post('service_region',TRUE)));
            $site_id = validate_string(trim($this->input->post('site_id',TRUE)));
            $location_id = validate_number(trim($this->input->post('location_id',TRUE)));
            $address1 = validate_string(trim($this->input->post('address_1',TRUE)));
            $address2 = validate_string(trim($this->input->post('address_2',TRUE)));
            $address3 = validate_string(trim($this->input->post('address_3',TRUE)));
            $address4 = validate_string(trim($this->input->post('address_4',TRUE)));
            $zip_code = validate_string(trim($this->input->post('zip_code',TRUE)));
            $system_id = validate_string(trim($this->input->post('system_id',TRUE)));
            $country_id = validate_number($this->input->post('country_id',TRUE));
            $asset_number = validate_string($this->input->post('asset_number',TRUE));
            $modality_id = validate_number($this->input->post('modality_id',TRUE));
            $product_description = validate_string($this->input->post('product_description',TRUE));
            $model_type = validate_string($this->input->post('model_type',TRUE));

            $this->db->trans_begin();
            $cust_avail = $this->Common_model->get_data_row('customer',array('customer_number'=>$customer_number,'country_id'=>$country_id));
            if(count($cust_avail) > 0)
            {
                $customer_id = $cust_avail['customer_id'];
                if(trim($cust_avail['name']) != $customer_name || trim($cust_avail['service_region'])!=$service_region )
                {
                    $update_c = array(
                        'customer_number' => $customer_number,
                        'name'            => $customer_name,
                        'service_region'  => $service_region,
                        'country_id'      => $country_id,
                        'created_by'      => $this->session->userdata('sso_id'),
                        'created_time'    => date('Y-m-d H:i:s'),
                        'status'          => 1
                    );
                    $update_c_where = array('customer_id'=>$customer_id);
                    $this->Common_model->update_data('customer',$update_c,$update_c_where);
                }
            }
            else
            {
                $cust_data = array(
                    'customer_number' => $customer_number,
                    'name'            => $customer_name,
                    'service_region'  => $service_region,
                    'country_id'      => $country_id,
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s'),
                    'status'          => 1
                );
                $customer_id = $this->Common_model->insert_data('customer',$cust_data);
            }

            #check the site_id availablity
            $site_avail = $this->Common_model->get_data_row('customer_site',array('site_id'=>$site_id,'customer_id'=>$customer_id,'country_id'=>$country_id));
            if(count($site_avail) > 0)
            {
                $customer_site_id = $site_avail['customer_site_id'];
                if(trim($site_avail['location_id'])!=$location_id || trim($site_avail['address1']) !=$address1 || trim($site_avail['address2']) !=$address2 || trim($site_avail['address3']) !=$address3 || trim($site_avail['address4']) !=$address4 || trim($site_avail['zip_code']) !=$zip_code)
                {
                    $update_cs = array(
                        'customer_id'   => $customer_id,
                        'site_id'       => $site_id,
                        'location_id'   => $location_id,
                        'address1'      => $address1,
                        'address2'      => $address2,
                        'address3'      => $address3,
                        'address4'      => $address4,
                        'country_id'    => $country_id,
                        'zip_code'      => $zip_code,
                        'modified_by'   => $this->session->userdata('sso_id'),
                        'modified_time' => date('Y-m-d H:i:s'),
                        'status'        => 1
                    );
                    $update_cs_where = array('customer_site_id'=>$customer_site_id);
                    $this->Common_model->update_data('customer_site',$update_cs,$update_cs_where);

                    #update cs history
                    $update_csh = array('end_time'=>date('Y-m-d H:i:s'));
                    $update_csh_where = array('customer_site_id'=>$customer_site_id,'end_time'=>NULL);
                    $this->Common_model->update_data('cs_history',$update_csh,$update_csh_where);

                    #insert cs history
                    $insert_csh = array(
                        'customer_site_id' => $customer_site_id,
                        'location_id'  => $location_id,
                        'address1'     => $address1,
                        'address2'     => $address2,
                        'address3'     => $address3,
                        'address4'     => $address4,
                        'zip_code'     => $zip_code,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s')
                    );
                    $this->Common_model->insert_data('cs_history',$insert_csh);
                }
            }
            else
            {
                $cust_site_data = array(
                    'customer_id'  => $customer_id,
                    'site_id'      => $site_id,
                    'location_id'  => $location_id,
                    'address1'     => $address1,
                    'address2'     => $address2,
                    'address3'     => $address3,
                    'address4'     => $address4,
                    'country_id'   => $country_id,
                    'zip_code'     => $zip_code,
                    'created_by'   => $this->session->userdata('sso_id'),
                    'created_time' => date('Y-m-d H:i:s'),
                    'status'       => 1
                );
                $customer_site_id = $this->Common_model->insert_data('customer_site',$cust_site_data);

                #insert cs history
                $insert_csh = array(
                    'customer_site_id' => $customer_site_id,
                    'location_id'  => $location_id,
                    'address1'     => $address1,
                    'address2'     => $address2,
                    'address3'     => $address3,
                    'address4'     => $address4,
                    'zip_code'     => $zip_code,
                    'created_by'   => $this->session->userdata('sso_id'),
                    'created_time' => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('cs_history',$insert_csh);
            }

            
            $sys_arr = $this->Common_model->get_data_row('install_base',array('system_id' => $system_id,'customer_site_id'=>$customer_site_id,'country_id'=>$country_id));
            if(count($sys_arr)>0)
            {
                if($sys_arr['asset_number']!=$asset_number || $sys_arr['modality_id']!=$modality_id || $sys_arr['product_description']!=$product_description || $sys_arr['model_type']!=$model_type)
                {
                    $system_data = array(
                        'customer_site_id'    => $customer_site_id,
                        'system_id'           => $system_id,
                        'asset_number'        => $asset_number,
                        'modality_id'         => $modality_id,
                        'product_description' => $product_description,
                        'model_type'          => $model_type,
                        'country_id'          => $country_id,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s'),
                        'status'              => 1
                    );
                    $update_sys_where = array('install_base_id'=>$sys_arr['install_base_id']);
                    $this->Common_model->update_data('install_base',$system_data,$update_sys_where);
                }
            }
            else
            {
                $system_data = array(
                    'customer_site_id'    => $customer_site_id,
                    'system_id'           => $system_id,
                    'asset_number'        => $asset_number,
                    'modality_id'         => $modality_id,
                    'product_description' => $product_description,
                    'model_type'          => $model_type,
                    'country_id'          => $country_id,
                    'created_by'          => $this->session->userdata('sso_id'),
                    'created_time'        => date('Y-m-d H:i:s'),
                    'status'              => 1
                );
                $install_base_id = $this->Common_model->insert_data('install_base',$system_data);
            }

            if($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong>Something Went Wrong! Please check.</div>'); 
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Install Base details for System ID : <strong>'.$system_id.' </strong> has been Added successfully!
                </div>');
            }
        }
        redirect(SITE_URL.'install_base'); exit();
    }

    public function edit_install_base()
    {
        $install_base_id=@storm_decode($this->uri->segment(2));
        if($install_base_id=='')
        {
            redirect(SITE_URL.'install_base');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Install Base Details";
		$data['nestedView']['cur_page'] = 'check_install_base';
		$data['nestedView']['parent_page'] = 'install_base';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        $country_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'country_id');
        if(@$country_id!='')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/install_base.js"></script>';
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit Install Base Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage IB','class'=>'','url'=>SITE_URL.'install_base');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit IB Details','class'=>'active','url'=>'');

            # Additional data
            $data['modalityList'] = $this->Common_model->get_data('modality',array('status' => 1));
            $data['locationList'] = $this->Install_base_model->get_location_area($country_id);
            $data['lrow'] = $this->Install_base_model->get_records_by_ib($install_base_id);
            $data['flg'] = 2;
            $data['country_id'] = $country_id;
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['form_action'] = SITE_URL.'update_install_base';
            $data['displayResults'] = 0;

            $this->load->view('install_base/install_base_view',$data);
        }
    }

    public function update_install_base()
    {
        #page authentication
        $task_access = page_access_check('install_base');

        $install_base_id=validate_string(storm_decode($this->input->post('encoded_id',TRUE)));
        if($install_base_id=='')
        {
            redirect(SITE_URL.'install_base'); exit();
        }

        if(validate_string($this->input->post('submit_install_base',TRUE))!= '')
        {
            $customer_number = validate_string(trim($this->input->post('customer_number',TRUE)));   
            $customer_name = validate_string(trim($this->input->post('customer_name',TRUE)));
            $service_region = validate_string(trim($this->input->post('service_region',TRUE)));
            $site_id = validate_string(trim($this->input->post('site_id',TRUE)));
            $location_id = validate_number(trim($this->input->post('location_id',TRUE)));
            $address1 = validate_string(trim($this->input->post('address_1',TRUE)));
            $address2 = validate_string(trim($this->input->post('address_2',TRUE)));
            $address3 = validate_string(trim($this->input->post('address_3',TRUE)));
            $address4 = validate_string(trim($this->input->post('address_4',TRUE)));
            $zip_code = validate_string(trim($this->input->post('zip_code',TRUE)));
            $system_id = validate_string(trim($this->input->post('system_id',TRUE)));
            $country_id = validate_number($this->input->post('country_id',TRUE));
            $asset_number = validate_string($this->input->post('asset_number',TRUE));
            $modality_id = validate_number($this->input->post('modality_id',TRUE));
            $product_description = validate_string($this->input->post('product_description',TRUE));
            $model_type = validate_string($this->input->post('model_type',TRUE));

            $this->db->trans_begin();
            $cust_avail = $this->Common_model->get_data_row('customer',array('customer_number'=>$customer_number,'country_id'=>$country_id));
            if(count($cust_avail) > 0)
            {
                $customer_id = $cust_avail['customer_id'];
                if(trim($cust_avail['name']) != $customer_name || trim($cust_avail['service_region'])!=$service_region )
                {
                    $update_c = array(
                        'customer_number' => $customer_number,
                        'name'            => $customer_name,
                        'service_region'  => $service_region,
                        'country_id'      => $country_id,
                        'created_by'      => $this->session->userdata('sso_id'),
                        'created_time'    => date('Y-m-d H:i:s'),
                        'status'          => 1
                    );
                    $update_c_where = array('customer_id'=>$customer_id);
                    $this->Common_model->update_data('customer',$update_c,$update_c_where);
                }
            }
            else
            {
                $cust_data = array(
                    'customer_number' => $customer_number,
                    'name'            => $customer_name,
                    'service_region'  => $service_region,
                    'country_id'      => $country_id,
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s'),
                    'status'          => 1
                );
                $customer_id = $this->Common_model->insert_data('customer',$cust_data);
            }

            #check the site_id availablity
            $site_avail = $this->Common_model->get_data_row('customer_site',array('site_id'=>$site_id,'customer_id'=>$customer_id,'country_id'=>$country_id));
            if(count($site_avail) > 0)
            {
                $customer_site_id = $site_avail['customer_site_id'];
                if(trim($site_avail['location_id'])!=$location_id || trim($site_avail['address1']) !=$address1 || trim($site_avail['address2']) !=$address2 || trim($site_avail['address3']) !=$address3 || trim($site_avail['address4']) !=$address4 || trim($site_avail['zip_code']) !=$zip_code)
                {
                    $update_cs = array(
                        'customer_id'   => $customer_id,
                        'site_id'       => $site_id,
                        'location_id'   => $location_id,
                        'address1'      => $address1,
                        'address2'      => $address2,
                        'address3'      => $address3,
                        'address4'      => $address4,
                        'country_id'    => $country_id,
                        'zip_code'      => $zip_code,
                        'modified_by'   => $this->session->userdata('sso_id'),
                        'modified_time' => date('Y-m-d H:i:s'),
                        'status'        => 1
                    );
                    $update_cs_where = array('customer_site_id'=>$customer_site_id);
                    $this->Common_model->update_data('customer_site',$update_cs,$update_cs_where);

                    #update cs history
                    $update_csh = array('end_time'=>date('Y-m-d H:i:s'));
                    $update_csh_where = array('customer_site_id'=>$customer_site_id,'end_time'=>NULL);
                    $this->Common_model->update_data('cs_history',$update_csh,$update_csh_where);

                    #insert cs history
                    $insert_csh = array(
                        'customer_site_id' => $customer_site_id,
                        'location_id'  => $location_id,
                        'address1'     => $address1,
                        'address2'     => $address2,
                        'address3'     => $address3,
                        'address4'     => $address4,
                        'zip_code'     => $zip_code,
                        'created_by'   => $this->session->userdata('sso_id'),
                        'created_time' => date('Y-m-d H:i:s')
                    );
                    $this->Common_model->insert_data('cs_history',$insert_csh);
                }
            }
            else
            {
                $cust_site_data = array(
                    'customer_id'  => $customer_id,
                    'site_id'      => $site_id,
                    'location_id'  => $location_id,
                    'address1'     => $address1,
                    'address2'     => $address2,
                    'address3'     => $address3,
                    'address4'     => $address4,
                    'country_id'   => $country_id,
                    'zip_code'     => $zip_code,
                    'created_by'   => $this->session->userdata('sso_id'),
                    'created_time' => date('Y-m-d H:i:s'),
                    'status'       => 1
                );
                $customer_site_id = $this->Common_model->insert_data('customer_site',$cust_site_data);

                #insert cs history
                $insert_csh = array(
                    'customer_site_id' => $customer_site_id,
                    'location_id'  => $location_id,
                    'address1'     => $address1,
                    'address2'     => $address2,
                    'address3'     => $address3,
                    'address4'     => $address4,
                    'zip_code'     => $zip_code,
                    'created_by'   => $this->session->userdata('sso_id'),
                    'created_time' => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('cs_history',$insert_csh);
            }

            $system_data = array(
                'customer_site_id'    => $customer_site_id,
                'system_id'           => $system_id,
                'asset_number'        => $asset_number,
                'modality_id'         => $modality_id,
                'product_description' => $product_description,
                'model_type'          => $model_type,
                'country_id'          => $country_id,
                'modified_by'         => $this->session->userdata('sso_id'),
                'modified_time'       => date('Y-m-d H:i:s'),
                'status'              => 1
            );
            $update_sys_where = array('install_base_id'=>$install_base_id);
            $this->Common_model->update_data('install_base',$system_data,$update_sys_where);

            if($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong>Something Went Wrong! Please check.</div>'); 
                redirect(SITE_URL.'install_base'); exit();
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Install Base details for System ID :<strong>'.$system_id.'</strong> has been Updated successfully!
                </div>');
                redirect(SITE_URL.'install_base'); exit();
            }
        }
        else
        {
            redirect(SITE_URL.'install_base'); exit();
        }
    }

    public function deactivate_install_base($encoded_id)
    {
        #page authentication
        $task_access = page_access_check('install_base');

        $install_base_id=@storm_decode($encoded_id);
        if($install_base_id==''){
            redirect(SITE_URL.'install_base');
            exit;
        }
        $data_arr = array(
            'status'        => 2,
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $where = array('install_base_id' => $install_base_id);
        $this->Common_model->update_data('install_base',$data_arr, $where);

        $system_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'system_id');
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Install Base Details for system id : '.$system_id.' has been deactivated successfully!
        </div>');
        redirect(SITE_URL.'install_base');
    }

    public function activate_install_base($encoded_id)
    {
        #page authentication
        $task_access = page_access_check('install_base');

        $install_base_id=@storm_decode($encoded_id);
        if($install_base_id==''){
            redirect(SITE_URL.'install_base');
            exit;
        }
        $system_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'system_id');
        $where = array('install_base_id' => $install_base_id);
        $data_arr = array(
            'status'        => 1,
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $this->Common_model->update_data('install_base',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Success!</strong> Install Base Details for System ID : '.$system_id.' has been Activated successfully!</div>');
        redirect(SITE_URL.'install_base');
	}

    // checking System ID number uniqueness
    public function is_ib_system_idExist()
    {
        $system_id = validate_string($this->input->post('system_id',TRUE));
        $install_base_id = validate_number($this->input->post('install_base_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('system_id'=>$system_id,'install_base_id'=>$install_base_id,'country_id'=>$country_id);
        $result = $this->Install_base_model->is_ib_system_idExist($data);
        echo $result;
    }

    // checking Site ID number uniqueness
    public function is_ib_site_id_Exist()
    {
        $customer_num = validate_string($this->input->post('customer_num',TRUE));
        $customer_site_id = validate_string($this->input->post('customer_site_id',TRUE));
        $site_id = validate_string($this->input->post('site_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));

        $data = array('customer_num'=>$customer_num,'customer_site_id'=>$customer_site_id,'site_id' => $site_id,'country_id'=>$country_id);
        $result = $this->Install_base_model->is_ib_site_id_Exist($data);
        echo $result;
    }

    public function get_customer_details_by_id()
    {
        $customer_id = validate_string($this->input->post('customer_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $result = $this->Install_base_model->get_customer_details_by_id($customer_id,$country_id);
        echo json_encode($result);
    }

    public function bulkupload_install_base()
    {
        $data['nestedView']['heading']="IB Bulk Upload";
        $data['nestedView']['cur_page'] = 'install_base';
        $data['nestedView']['parent_page'] = 'install_base';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/install_base.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Install Base Bulk Upload';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage IB','class'=>'','url'=>SITE_URL.'install_base');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'IB Bulk Upload','class'=>'active','url'=>'');

        #additional Data        
        $data['form_action'] = SITE_URL.'insert_bulkupload_install_base';  
        $this->load->view('install_base/install_base_upload_view',$data);
    }

    public function insert_bulkupload_install_base()
    {
        #page authentication
        $task_access = page_access_check('install_base');

        if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
        { 
            $filename=$_FILES["uploadCsv"]["tmp_name"];            
            if($_FILES["uploadCsv"]["size"] > 0)
            {
                $this->db->trans_begin();
                $file = fopen($filename, "r");
                $missed = 0;  
                $update = 0;  
                $insert = 0; 
                $loop = 0;
               
                // inserting in upload table with default status 
                $upload_data = array(
                    'type'         => 3, // for Install base Upload
                    'status'       => 1,                                
                    'created_by'   => $this->session->userdata('sso_id'),
                    'created_time' => date('Y-m-d H:i:s'),
                    'file_name'    => $_FILES["uploadCsv"]["name"]
                );
                $upload_id = $this->Common_model->insert_data('upload',$upload_data);

                #loop the records fetched from csv file
                while (($ibData = fgetcsv($file, 10000, ",")) !== FALSE)
                {
                    #exclude the first record as contain heading
                    if($loop==0) { $loop++; continue; } 

                    #assign data to variable as fetched individually
                    $asset_number        = validate_string(trim(@$ibData[0]));
                    $system_id           = validate_string(trim(@$ibData[1]));
                    $modality_code       = validate_string(trim(@$ibData[2]));
                    $product_description = validate_string(trim(@$ibData[3]));
                    $model_type          = validate_string(trim(@$ibData[4]));
                    $customer_number     = validate_string(trim(@$ibData[5]));
                    $customer_name       = validate_string(trim(@$ibData[6]));
                    $site_id             = validate_string(trim(@$ibData[7]));
                    $address1            = validate_string(trim(@$ibData[8]));
                    $address2            = validate_string(trim(@$ibData[9]));
                    $address3            = validate_string(trim(@$ibData[10]));
                    $address4            = validate_string(trim(@$ibData[11]));
                    $zip_code            = validate_string(trim(@$ibData[12]));
                    $city_name           = validate_string(trim(@$ibData[13]));
                    $service_region      = validate_string(trim(@$ibData[14]));
                    $country             = validate_string(trim(@$ibData[15]));
                    $country_id = '';
                    if($country!='')
                    {
                        $country_id=$this->Common_model->get_value('location',array('name'=>$country,'level_id'=>2),'location_id');
                    }

                    if(@$asset_number !='' && @$system_id !='' && @$modality_code !='' && @$product_description !='' && @$model_type !='' && @$customer_number !='' && @$customer_name !='' && @$site_id !='' && @$address1 !='' && @$zip_code !='' && @$city_name !=''&& @$service_region !='' && $country_id !='' &&$country!='')
                    {
                        $check = 0; 
                        $remark_error = ''; 
                        $update_count = 0; 
                        $insert_count = 0;

                        #check modality
                        $modality_id = $this->Common_model->get_value('modality',array('modality_code'=>@$modality_code),'modality_id');
                        if($modality_id=='')
                        {
                            $check++;
                            $remark_error.= 'Modality Code : "'.$modality_code.'" is Not available in system, ';
                        }
                        #check location
                        $location_id = $this->Install_base_model->check_location_city($country_id,$city_name);

                        if($location_id == '' || $location_id == 0)
                        {
                            $check++;
                            $remark_error.='City Name :'.$city_name.' is not Available.';
                        }

                        if($check == 0)//no errors
                        {
                            #customer data
                            $cust_arr = $this->Common_model->get_data_row('customer',array('customer_number' => @$customer_number,'country_id'=>$country_id));
                            if(count($cust_arr)>0)
                            {
                                $customer_id = $cust_arr['customer_id'];
                                $update_c = array(
                                    'customer_number' => @$customer_number,
                                    'name'            => @$customer_name,
                                    'service_region'  => @$service_region,
                                    'country_id'      => @$country_id,
                                    'modified_by'     => $this->session->userdata('sso_id'),
                                    'modified_time'   => date('Y-m-d H:i:s'),
                                    'status'          => 1
                                );
                                $update_c_where = array('customer_id'=>$customer_id);
                                $this->Common_model->update_data('customer',$update_c,$update_c_where);
                                if(trim($cust_arr['name']) != $customer_name || 
                                    trim($cust_arr['service_region']) !=$service_region)
                                {
                                    if($update_count == 0){ $update_count++; }
                                }
                            }
                            else
                            {
                                $insert_cust = array(
                                    'customer_number' => @$customer_number,
                                    'name'            => @$customer_name,
                                    'service_region'  => @$service_region,
                                    'country_id'      => @$country_id,
                                    'created_by'      => $this->session->userdata('sso_id'),
                                    'created_time'    => date('Y-m-d H:i:s'),
                                    'status'          => 1
                                );
                                $customer_id = $this->Common_model->insert_data('customer',$insert_cust);
                                if($insert_count==0){ $insert_count++; }
                            }

                            #customer site
                            $cs_arr = $this->Common_model->get_data_row('customer_site',array('site_id'=>$site_id,'customer_id'=>$customer_id,'country_id'=>$country_id));
                            if(count($cs_arr)>0)
                            {
                                $customer_site_id = $cs_arr['customer_site_id'];
                                if(trim($cs_arr['location_id']) !=$location_id || trim($cs_arr['address1']) != $address1 || trim($cs_arr['address2']) != $address2 || trim($cs_arr['address3']) != $address3 || trim($cs_arr['address4']) != $address4 || trim($cs_arr['zip_code']) != $zip_code)
                                {
                                    $update_cs = array(
                                        'location_id' => $location_id,
                                        'address1'    => $address1,
                                        'address2'    => $address2,
                                        'address3'    => $address3,
                                        'address4'    => $address4,
                                        'zip_code'    => $zip_code,
                                        'country_id'  => $country_id,
                                        'modified_by'  => $this->session->userdata('sso_id'),
                                        'modified_time'=> date('Y-m-d H:i:s'),
                                        'status'       => 1
                                    );
                                    $update_cs_where = array('customer_site_id'=>$customer_site_id);
                                    $this->Common_model->update_data('customer_site',$update_cs,$update_cs_where);

                                    #update cs history
                                    $update_csh = array('end_time'=>date('Y-m-d H:i:s'));
                                    $update_csh_where = array('customer_site_id'=>$customer_site_id,'end_time'=>NULL);
                                    $this->Common_model->update_data('cs_history',$update_csh,$update_csh_where);

                                    #insert cs history
                                    $insert_csh = array(
                                        'customer_site_id' => $customer_site_id,
                                        'location_id'  =>  $location_id,
                                        'address1'     =>  $address1,
                                        'address2'     =>  $address2,
                                        'address3'     =>  $address3,
                                        'address4'     =>  $address4,
                                        'zip_code'     =>  $zip_code,
                                        'created_by'   =>  $this->session->userdata('sso_id'),
                                        'created_time' =>  date('Y-m-d H:i:s')
                                    );
                                    $this->Common_model->insert_data('cs_history',$insert_csh);

                                    if($update_count == 0){ $update_count++; }
                                }
                            }
                            else
                            {
                                $insert_cs = array(
                                    'customer_id' => $customer_id,
                                    'site_id'     => $site_id,
                                    'location_id' => $location_id,
                                    'address1'    => $address1,
                                    'address2'    => $address2,
                                    'address3'    => $address3,
                                    'address4'    => $address4,
                                    'zip_code'    => $zip_code,
                                    'country_id'  => $country_id,
                                    'created_by'  => $this->session->userdata('sso_id'),
                                    'created_time'=> date('Y-m-d H:i:s'),
                                    'status'      => 1
                                );
                                $customer_site_id = $this->Common_model->insert_data('customer_site',$insert_cs);

                                #insert cs history
                                $insert_csh = array(
                                    'customer_site_id' => $customer_site_id,
                                    'location_id'  => $location_id,
                                    'address1'     => $address1,
                                    'address2'     => $address2,
                                    'address3'     => $address3,
                                    'address4'     => $address4,
                                    'zip_code'     => $zip_code,
                                    'created_by'   => $this->session->userdata('sso_id'),
                                    'created_time' => date('Y-m-d H:i:s')
                                );
                                $this->Common_model->insert_data('cs_history',$insert_csh);
                                if($insert_count==0){ $insert_count++; }
                            }

                            #install base
                            $ib_arr = $this->Common_model->get_data_row('install_base',array('system_id'=>$system_id,'customer_site_id'=>$customer_site_id,'country_id'=>$country_id));
                            if(count($ib_arr)>0)
                            { 
                                $update_ib = 
                                array(
                                    'asset_number'        => $asset_number,
                                    'modality_id'         => $modality_id,
                                    'product_description' => $product_description,
                                    'model_type'          => $model_type,
                                    'country_id'          => $country_id,
                                    'status'              => 1,
                                    'modified_by' => $this->session->userdata('sso_id'),
                                    'modified_time' => date('Y-m-d H:i:s')
                                );

                                $update_ib_where = array('install_base_id'=>$ib_arr['install_base_id']);
                                $this->Common_model->update_data('install_base',$update_ib,$update_ib_where);

                                if($update_count == 0){ $update_count++; }
                            }
                            else
                            {
                                $insert_ib =array(
                                    'customer_site_id'    => $customer_site_id,
                                    'system_id'           => $system_id,
                                    'asset_number'        => $asset_number,
                                    'modality_id'         => $modality_id,
                                    'product_description' => $product_description,
                                    'model_type'          => $model_type,
                                    'country_id'          => $country_id,
                                    'status'              => 1,
                                    'created_by'          => $this->session->userdata('sso_id'),
                                    'created_time'        => date('Y-m-d H:i:s')
                                );
                                $this->Common_model->insert_data('install_base',$insert_ib);
                                if($insert_count==0){ $insert_count++; }
                            }

                            if($update_count != 0)
                            {
                                $update++;
                            }
                            else
                            {
                                $insert++;
                            }
                        }
                        else
                        {
                            $insert_missed_record = array(
                                'upload_id'           => $upload_id,
                                'asset_number'        => $asset_number,
                                'system_id'           => $system_id,
                                'modality_name'       => $modality_code,
                                'product_description' => $product_description,
                                'model_type'          => $model_type,
                                'customer_number'     => $customer_number,
                                'customer_name'       => $customer_name,
                                'site_id'             => $site_id,
                                'address1'            => $address1,
                                'address2'            => $address2,
                                'address3'            => $address3,
                                'address4'            => $address4,
                                'country_name'        => $country,
                                'zip_code'            => $zip_code,
                                'city_name'           => $city_name,
                                'service_region'      => $service_region,
                                'remarks'             => $remark_error,
                                'created_by'          => $this->session->userdata('sso_id'),
                                'created_time'        => date('Y-m-d H:i:s'),
                                'status'              => 1
                            );
                            $this->Common_model->insert_data('upload_install_base',$insert_missed_record);
                            $missed++;
                        }
                    }
                    else
                    {
                        $remarks_string ='';
                        if($asset_number =='') $remarks_string.='Asset Number, ';
                        if($system_id =='') $remarks_string.='System ID, ';
                        if($modality_code =='') $remarks_string.='Modality, ';
                        if($product_description =='') $remarks_string.='Product Description, ';
                        if($model_type =='') $remarks_string.='Model Type, ';
                        if($customer_number =='') $remarks_string.='Customer Number, ';
                        if($customer_name =='') $remarks_string.='Customer Name, ';
                        if($site_id =='') $remarks_string.='Site ID, ';
                        if($address1 =='') $remarks_string.='Address1, ';
                        if($zip_code =='') $remarks_string.='Zip Code, ';
                        if($city_name =='') $remarks_string.='City, ';
                        if($service_region =='') $remarks_string.='Service Region, ';
                        if($country == '' || $country_id == '') $remarks_string.='Country, ';
                        $remarks_string.=' Is missing/Not Available.';

                        $insert_missed_record = array(
                        'upload_id'           => $upload_id,
                        'asset_number'        => $asset_number,
                        'system_id'           => $system_id,
                        'modality_name'       => $modality_code,
                        'product_description' => $product_description,
                        'model_type'          => $model_type,
                        'customer_number'     => $customer_number,
                        'customer_name'       => $customer_name,
                        'site_id'             => $site_id,
                        'address1'            => $address1,
                        'address2'            => $address2,
                        'address3'            => $address3,
                        'address4'            => $address4,
                        'zip_code'            => $zip_code,
                        'country_name'        => $country,
                        'city_name'           => $city_name,
                        'service_region'      => $service_region,
                        'remarks'             => $remarks_string,
                        'created_by'          => $this->session->userdata('sso_id'),
                        'created_time'        => date('Y-m-d H:i:s'),
                        'status'              => 1
                        );
                        $this->Common_model->insert_data('upload_install_base',$insert_missed_record);
                        $missed++;
                    }
                    $loop++;
                }
                fclose($file);
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.</div>'); 
                    redirect(SITE_URL.'install_base');  
                }
                else
                {
                    $this->db->trans_commit();
                    if($missed>0)
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> Successfully Inserted Records : <strong>'.$insert.'</strong>, Updated Records : <strong>'.$update.'</strong>, Missed records : <strong>'.$missed.'</strong> Check below!.</div>');
                        redirect(SITE_URL.'missed_ib_list/'.storm_encode($upload_id));  exit();
                    }
                    else if($loop==1)
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-times-circle"></i></div>
                        <strong>Error!</strong> No records Captured. Uploaded Empty file!</div>'); 
                        redirect(SITE_URL.'bulkupload_install_base'); exit();
                    }
                    else
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> Successfully Inserted Records : <strong>'.$insert.'</strong>, Updated Records : <strong>'.$update.'</strong> !</div>');
                        redirect(SITE_URL.'install_base'); exit();
                    }
                }
            } 
            else
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');
                redirect(SITE_URL.'bulkupload_install_base'); exit();
            }  
        }
        else
        {
            redirect(SITE_URL.'bulkupload_install_base'); exit();
        }
    }

    public function missed_ib_list()
    {
        $upload_id = @storm_decode($this->uri->segment(2));
        if($upload_id == '')
        {
        redirect(SITE_URL.'install_base'); exit();
        }

        $data['nestedView']['heading']="Missed List of IB Bulk Upload";
        $data['nestedView']['cur_page'] = 'install_base';
        $data['nestedView']['parent_page'] = 'install_base';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Missed List of IB Bulk Upload';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Install Base','class'=>'','url'=>SITE_URL.'install_base');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Missed IB Bulk Upload','class'=>'active','url'=>'');

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL.'missed_ib_list/'.storm_encode($upload_id).'/';
        # Total Records
        $config['total_rows'] = $this->Install_base_model->missed_ib_total_num_rows($upload_id);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($data['pagination_links'] != '') {
          $data['last'] = $this->pagination->cur_page * $config['per_page'];
          if ($data['last'] > $data['total_rows']) {
              $data['last'] = $data['total_rows'];
          }
          $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['missedResults'] = $this->Install_base_model->missed_ib_results($current_offset, $config['per_page'], $upload_id);

        #additional Data 
        $data['upload_id'] = $upload_id; 
        $this->load->view('install_base/missed_ib_upload_view',$data);
    }

    public function download_missed_ib_uploads()
    {
        #page authentication
        $task_access = page_access_check('install_base');
        
        $upload_id = @storm_decode($this->uri->segment(2));
        if($upload_id == '')
        {
            redirect(SITE_URL.'install_base'); exit();
        }
        $print_data = $this->Common_model->get_data('upload_install_base',array('upload_id'=>$upload_id));
        $header = '';
        $data ='';
        $titles = array('Asset#','System ID','Modality','Product Description','Model Type','Customer#','Customer Name','Site ID','Address1','Address2','Address3','Address4','Zip Code','City','Service Region','Country Name','Remarks');
        $data = '<table border="1">';
        $data.='<thead>';
        $data.='<tr>';
        foreach ( $titles as $title)
        {
            $data.= '<th align="center">'.$title.'</th>';
        }
        $data.='</tr>';
        $data.='</thead>';
        $data.='<tbody>';
         
        if(count($print_data)>0)
        {
            
            foreach($print_data as $row)
            {
                $data.='<tr>';                
                $data.='<td align="center">'.$row['asset_number'].'</td>';                   
                $data.='<td align="center">'.$row['system_id'].'</td>';                   
                $data.='<td align="center">'.$row['modality_name'].'</td>';                   
                $data.='<td align="center">'.$row['product_description'].'</td>'; 
                $data.='<td align="center">'.$row['model_type'].'</td>';                   
                $data.='<td align="center">'.$row['customer_number'].'</td>';                   
                $data.='<td align="center">'.$row['customer_name'].'</td>';                   
                $data.='<td align="center">'.$row['site_id'].'</td>'; 
                $data.='<td align="center">'.$row['address1'].'</td>';                   
                $data.='<td align="center">'.$row['address2'].'</td>';                   
                $data.='<td align="center">'.$row['address3'].'</td>';                   
                $data.='<td align="center">'.$row['address4'].'</td>'; 
                $data.='<td align="center">'.$row['zip_code'].'</td>';                   
                $data.='<td align="center">'.$row['city_name'].'</td>';                   
                $data.='<td align="center">'.$row['service_region'].'</td>';
                $data.='<td align="center">'.$row['country_name'].'</td>';                   
                $data.='<td align="center">'.$row['remarks'].'</td>'; 
                $data.='</tr>';
               
            }
        }
        else
        {
            $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Results Found</td></tr>';
        }
        $data.='</tbody>';
        $data.='</table>';
        $time = date("Ymdhis");
        $xlFile='missed_ib_'.$time.'.xls'; 
        header("Content-type: application/x-msdownload"); 
        # replace excelfile.xls with whatever you want the filename to default to
        header("Content-Disposition: attachment; filename=".$xlFile."");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $data; 
    }
}