<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class User extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function onbehalf_fe_ajax()
    {
        #page authentication
        $task_access = page_access_check('raise_order');
        $sso_string = validate_string($_REQUEST['string']);
        $country_id = validate_number($_REQUEST['part_type']);
        $data = $this->User_model->onbehalf_fe_ajax($task_access,$sso_string,$country_id);
        echo json_encode($data);
    }

    public function sso_dropdown_list()
    {
        #page authentication
        $task_access = page_access_check('raise_order');
        $sso_string = validate_string($_REQUEST['string']);

        $data = $this->User_model->sso_dropdown_list($task_access,$sso_string);
        echo json_encode($data);
    }

    public function all_sso_dropdown_list()
    {
        $sso_string = validate_string($_REQUEST['string']);
        $role_id = validate_number($_REQUEST['part_type']);
        $data = $this->User_model->all_sso_dropdown_list($sso_string,$role_id);
        echo json_encode($data);
    }

    public function get_reporting_manager_list()
    {
        #page authentication
        $task_access = page_access_check('manage_user');

        $sso_string = validate_string($_REQUEST['string']);
        $country_id = validate_number($_REQUEST['part_type']);

        $data = $this->User_model->get_reporting_manager_list($sso_string,$country_id);
        echo json_encode($data);
    }

    public function get_fe_position_list()
    {
        #page authentication
        $task_access = page_access_check('manage_user');

        $city_string = validate_string($_REQUEST['string']);
        $country_id = validate_number($_REQUEST['part_type']);

        $data = $this->User_model->get_fe_position_list($city_string,$country_id);
        echo json_encode($data);
    }

    public function user()
    {
        $data['nestedView']['heading']="Manage User";
        $data['nestedView']['cur_page'] = 'manage_user';
        $data['nestedView']['parent_page'] = 'manage_user';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Manage User';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage User','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchUser',TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'ssonum'         => validate_string($this->input->post('sso_num',TRUE)),
            'username'       => validate_string($this->input->post('user_name',TRUE)),
            'branchnum'      => validate_string($this->input->post('branch_num',TRUE)),
            'designationnum' => validate_string($this->input->post('designation_num',TRUE)),
            'roleid'         => validate_number($this->input->post('role_id',TRUE)),
            'whid'           => validate_number($this->input->post('wh_id',TRUE)),
            'fe_position'    => validate_string($this->input->post('fe_position',TRUE)),
            'country_id'     => validate_number(@$this->input->post('country_id',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'ssonum'         => $this->session->userdata('ssonum'),
                'username'       => $this->session->userdata('username'),
                'branchnum'      => $this->session->userdata('branchnum'),
                'designationnum' => $this->session->userdata('designationnum'),
                'roleid'         => $this->session->userdata('roleid'),
                'whid'           => $this->session->userdata('whid'),
                'fe_position'    => $this->session->userdata('fe_position'),
                'country_id'     => $this->session->userdata('country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'ssonum'         => '',
                'username'       => '',
                'branchnum'      => '',
                'designationnum' => '',
                'roleid'         => '',
                'whid'           => '',
                'fe_position'    => '',
                'country_id'     => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'user/';
        # Total Records
        $config['total_rows'] = $this->User_model->user_total_num_rows($searchParams,$task_access);
       
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') 
        {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) 
            {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['userResults'] = $this->User_model->user_results($current_offset, $config['per_page'], $searchParams,$task_access);

        //echo "<prE>"; print_r($data['userResults']); exit();

        # Additional data
        $data['branchList'] = $this->Common_model->get_data('branch',array('status'=>1,'task_access'=>$task_access));
        $data['whList']     = $this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));

        $data['roleList']   = $this->Common_model->get_data('role',array('status'=>1));
        //$data['locList'] = $this->User_model->get_all_locations_by_country($task_access);
        $data['designationList'] = $this->Common_model->get_data('designation',array('status'=>1));
        $data['displayResults'] = 1;

        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $this->load->view('user/user_view',$data);
    }

    public function user_country()
    {
        $sso_id = storm_decode($this->uri->segment(2));
        if($sso_id == '')
        {
            redirect(SITE_URL.'user'); exit();
        }
        $data['nestedView']['heading']="Assign User Countries";
        $data['nestedView']['cur_page'] = 'check_manage_user';
        $data['nestedView']['parent_page'] = 'manage_user';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][]='<link rel="stylesheet" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        //$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Assign User Countries';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage User','class'=>'','url'=>SITE_URL.'user');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Assign User Countries','class'=>'active','url'=>'');

        # Additional data
        $user_data = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
        $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$user_data['country_id']),'name');
        $data['user_data'] = $user_data;
        $region_id = $this->Common_model->get_value('location',array('location_id'=>$user_data['country_id']),'region_id');
        $data['country_list'] = $this->Common_model->get_data('location',array('level_id'=>2,'status'=>1,'region_id'=>$region_id));
        $assigned_country = $this->Common_model->get_data('user_country',array('sso_id'=>$sso_id,'status'=>1));
        $assign_arr = array();
        if(count($assigned_country)>0)
        {
            foreach ($assigned_country as $key => $value) 
            {
                $assign_arr[] = $value['country_id'];
            }
        }
        else
        {
            $assign_arr[] = 0;
        }
        $data['assigned_country'] = $assign_arr;
        $data['region_id'] = $region_id;
        $data['form_action'] = SITE_URL.'insert_user_country';
        $this->load->view('user/user_country_view',$data);
    }

    public function insert_user_country()
    {
        #page authentication
        $task_access = page_access_check('manage_user');

        $sso_id = storm_decode($this->input->post('encoded_id',TRUE));
        if($sso_id == '')
        {
            redirect(SITE_URL.'user'); exit();
        }
        if($this->input->post('submitUser')!='')
        {
            $country_list = $this->input->post('country',TRUE);
            $country_region = $this->input->post('country_region',TRUE);
            $user_role = $this->input->post('user_role',TRUE);
            if($country_region == 3 && $user_role == 9)
            {
                if(count($country_list)>1){
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Sorry!</strong> You can not assign more than 2 countries for Tool Coordinator.</div>'); 
                    redirect(SITE_URL.'user'); exit();
                }
            }
            $user_data = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
            #deactivate previous user country data
            $assigned_country = $this->Common_model->get_data('user_country',array('sso_id'=>$sso_id));
            if(count($assigned_country)>0)
            {
                $update_data = array('status'=>2);
                $update_where = array('sso_id'=>$sso_id);
                $this->Common_model->update_data('user_country',$update_data,$update_where);
            }

            #insert new user country data            
            $this->db->trans_begin();
            foreach ($country_list as $key => $value) 
            {
                $this->User_model->insert_update_uc($value,$sso_id,1);
            }

            #insert user dedicated country
            if(!in_array($user_data['country_id'], $country_list))
            {
                $this->User_model->insert_update_uc($user_data['country_id'],$sso_id,1);
            }

            if($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong>Something Went Wrong! Please check.</div>');
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> For User : <strong>'.$user_data['name'].'</strong> Countries has been Assigned successfully!</div>');
                       
            }
        }
        redirect(SITE_URL.'user');
    }

    public function add_user()
    {
        $data['nestedView']['heading']="Add New User";
        $data['nestedView']['cur_page'] = 'check_manage_user';
        $data['nestedView']['parent_page'] = 'manage_user';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']= "Select Country";
                $data['nestedView']['cur_page'] = 'check_manage_user';
                $data['nestedView']['parent_page'] = 'manage_user';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage User','class'=>'','url'=>SITE_URL.'user');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_user';
                $data['cancel'] = SITE_URL.'user';
                $this->load->view('user/intermediate_page',$data); 
            }
        }

        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['css_includes'][]='<link rel="stylesheet" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css">';

            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New User';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage User','class'=>'','url'=>SITE_URL.'user');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New User','class'=>'active','url'=>'');

            # Additional data
            $data['branchList'] = $this->Common_model->get_data('branch',array('status'=>1,'country_id'=>$country_id));
            $data['warehouseList'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
            $data['roleList']   = $this->Common_model->get_data('role',array('status'=>1));
            //$data['fe_locationList'] = $this->User_model->get_location_city($country_id);
            $data['modalityList']=$this->Common_model->get_data('modality',array('status'=>1));
            //$data['r_managerList'] = $this->User_model->get_rm_user($country_id);
            $this->Common_model->get_data('user',array('status'=>1,'country_id'=>$country_id));
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['flg'] = 1;
            $data['form_action'] = SITE_URL.'insert_user';
            $data['displayResults'] = 0;
            $data['country_id'] = $country_id;
            $this->load->view('user/user_view',$data);
        }
    }

    public function insert_user()
    {
        #page authentication
        $task_access = page_access_check('manage_user');

        if(validate_string($this->input->post('submitUser',TRUE)) != '')
        {
            $sso_num = validate_string($this->input->post('sso_num',TRUE));
            $user_data = array('sso_num'=>$sso_num);
            $check_sso = $this->User_model->is_user_sso_Exist($user_data);
            if($check_sso>0)
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> SSO ID : '.$sso_num.' already exist! please check.</div>'); 
                redirect(SITE_URL.'user'); exit();
            }
            #insert_user_destails
            $location_id  = validate_number($this->input->post('location_id',TRUE));
            $role_id = validate_number($this->input->post('role_id',TRUE));
            $rm_id = validate_string($this->input->post('rm_id',TRUE));
            $country_id = validate_number($this->input->post('c_id',TRUE));
            if($rm_id == ''){ $rm_id = NULL; }
            if($role_id != 5 && $role_id != 6 && $location_id == '')
            {
                $location_id = NULL;
            }
            $insert_user = array(
            'sso_id'        => $sso_num,
            'name'          => validate_string($this->input->post('user_name',TRUE)),
            'rm_id'         => $rm_id,
            'email'         => validate_string($this->input->post('email',TRUE)),
            'mobile_no'     => validate_string($this->input->post('mobile_num',TRUE)),
            'role_id'       => $role_id,
            'designation_id'=> validate_number($this->input->post('designation_id',TRUE)),
            'branch_id'     => validate_number($this->input->post('branch_id',TRUE)),
            'address1'      => validate_string($this->input->post('address1',TRUE)),
            'address2'      => validate_string($this->input->post('address2',TRUE)),
            'fe_position'   => validate_number($this->input->post('fe_position',TRUE)),
            'wh_id'         => validate_number($this->input->post('warehouse_id',TRUE)),
            'location_id'   => $location_id,
            'created_by'    => $this->session->userdata('sso_id'),
            'created_time'  => date('Y-m-d H:i:s'),
            'status'        => 1,
            'country_id'    => $country_id);

            $this->db->trans_begin();
            $sso_id = $this->Common_model->insert_data('user',$insert_user);
            
            #audit data
            $remarks = "User Creation";
            $ad_id = audit_data('user_master',$sso_id,'user',1,'',$insert_user,array('sso_id'=>$sso_id),array(),$remarks,'',array(),'','',$country_id);

            $modalities=$this->input->post('modality',TRUE);
            if($role_id == 5 || $role_id == 6)
            {
                if(count($modalities)>0)
                {
                    $exception_arr = array('sso_id');
                    foreach($modalities as $key=>$value)
                    {
                        $insert_modality  = array(
                            'sso_id'      => $sso_id,
                            'modality_id' => $value,
                            'status'      => 1,
                            'from_date'   => date('Y-m-d')
                        );
                        $this->Common_model->insert_data('user_modality',$insert_modality);

                        #audit data
                        audit_data('user_modality',$value,'user_modality',1,$ad_id,$insert_modality,array('modality_id'=>$value),array(),NULL,NULL,$exception_arr,'','',$country_id);
                    }
                }
            }

            $wh_id = validate_number($this->input->post('warehouse_id',TRUE));
            if($role_id == 3)
            {
                #change all wh to inactive 2 for sso id
                $update_wh = array('status'=>2);
                $update_wh_where = array('sso_id'=>$sso_id);
                $this->Common_model->update_data('user_wh',$update_wh,$update_wh_where);

                #multiple warehouses checked
                $warehouses=$this->input->post('warehouse_arr',TRUE);
                if(count($warehouses)>0)
                {
                    foreach($warehouses as $key=>$wh_result_id)
                    {
                        $this->User_model->insert_update_wh_zone($sso_id,$wh_result_id,1);

                        #audit data
                        audit_data('user_wh',$wh_result_id,'user_wh',1,$ad_id,array('wh_id'=>$wh_result_id),array('wh_id'=>$wh_result_id),array(),'','',array(),'','',$country_id);
                    }
                }

                #default wh entry
                if(!in_array($wh_id, $warehouses))
                {
                    $this->User_model->insert_update_wh_zone($sso_id,$wh_id,1);
                    #audit data
                    audit_data('user_wh',$wh_id,'user_wh',1,$ad_id,array('wh_id'=>$wh_id),array('wh_id'=>$wh_id),array(),'','',array(),'','',$country_id);   
                }
            }

            #insert_user_history
            $insert_user_history = array(
            'sso_id'         => $sso_id,
            'role_id'        => validate_number($this->input->post('role_id',TRUE)),
            'designation_id' => validate_number($this->input->post('designation_id',TRUE)),
            'branch_id'      => validate_number($this->input->post('branch_id',TRUE)),
            'rm_id'          => $rm_id,
            'location_id'    => $location_id,
            'fe_position'    => validate_number($this->input->post('fe_position',TRUE)),
            'wh_id'          => validate_number($this->input->post('warehouse_id',TRUE)),
            'start_date'     => date('Y-m-d H:i:s'),
            'created_by'     => $this->session->userdata('sso_id'),
            'created_time'   => date('Y-m-d H:i:s'),
            'status'         => 1,
            'remarks'        => 'User Creation'
                                         );
            $this->Common_model->insert_data('user_history',$insert_user_history);

            #check for asean country
            $country_arr = $this->Common_model->get_data_row('location',array('level_id'=>2,'location_id'=>$country_id));
            $region_id = $country_arr['region_id'];
            $country_name = $country_arr['name'];
            $check_region_id = get_asean_region_id();
            $asean_country_name = get_asean_country_name();
            if(($check_region_id == $region_id) && ($country_name != $asean_country_name)  && (!in_array($role_id, exclude_Asean_for_roles())))
            {
                #deactivate previous user country data
                $update_data = array('status'=>2);
                $update_where = array('sso_id'=>$sso_id);
                $this->Common_model->update_data('user_country',$update_data,$update_where);

                #insert with actual country
                $this->User_model->insert_update_uc($country_id,$sso_id,1);
                #audit data
                audit_data('user_country',$country_id,'user_country',1,$ad_id,array('location_id'=>$country_id),array('country_id'=>$country_id),array(),'','',array(),'','',$country_id);

                #insert with ASEAN country
                $asean_country_id = $this->Common_model->get_value('location',array('level_id'=>2,'name'=>$asean_country_name),'location_id');
                $this->User_model->insert_update_uc($asean_country_id,$sso_id,1);
                #audit data
                audit_data('user_country',$asean_country_id,'user_country',1,$ad_id,array('location_id'=>$asean_country_id),array('country_id'=>$asean_country_id),array(),'','',array(),'','',$country_id);
            }
            
            if($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong>Something Went Wrong! Please check.</div>');
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> User :<strong>'.$sso_id.'</strong> has been Added successfully!</div>');
                       
            }
        }
        redirect(SITE_URL.'user'); exit();
    }

    public function edit_user()
    {
        $sso_id=@storm_decode($this->uri->segment(2));
        if($sso_id=='')
        {
            redirect(SITE_URL.'user'); exit;
        }

        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit User Details";
        $data['nestedView']['cur_page'] = 'check_manage_user';
        $data['nestedView']['parent_page'] = 'manage_user';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->Common_model->get_value('user',array('sso_id'=>$sso_id),'country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']="Edit User Details";
                $data['nestedView']['cur_page'] = 'check_manage_user';
                $data['nestedView']['parent_page'] = 'manage_user';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Edit User Details';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage User','class'=>'','url'=>SITE_URL.'user');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit User Details','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['selected_country'] = $this->Common_model->get_value('user',array('sso_id'=>$sso_id),'country_id');
                $data['form_action'] = SITE_URL.'edit_user/'.storm_encode($sso_id).'';
                $data['cancel'] = SITE_URL.'user';
                $this->load->view('user/intermediate_page',$data); 
            }
        }
        if(@$country_id != '')
        {
            # include files
            $data['nestedView']['css_includes'] = array();

            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Edit User Details';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage User','class'=>'','url'=>SITE_URL.'user');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit User Details','class'=>'active','url'=>'');

            # Additional data
            $data['flg'] = 2;
            $data['form_action'] = SITE_URL.'update_user';
            $data['displayResults'] = 0;

            # Data
            $data['branchList']   = $this->Common_model->get_data('branch',array('status'=>1,'country_id'=>$country_id));
            $data['roleList']     = $this->Common_model->get_data('role',array('status'=>1));
            //$data['fe_locationList'] = $this->User_model->get_location_city($country_id);
            $data['modalities']=$this->Common_model->get_data('user_modality',array('sso_id'=>$sso_id,'status'=>1));
            $data['modalityList']=$this->Common_model->get_data('modality',array('status'=>1));
            $data['warehouseList'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            //$data['r_managerList'] = $this->Common_model->get_data('user',array('status'=>1,'sso_id !='=>$sso_id,'country_id'=>$country_id));
            $row = $this->User_model->get_user_details_by_id($sso_id);
            $data['lrow'] = $row;

            $role_id = $row['role_id'];
            $zone_wh_list = array();
            $sso_wh_arr = array();
            if($role_id == 3)
            {
                $data['location_zn_List'] = $this->User_model->get_zonal_data($country_id);
                $location_id = $row['location_id'];
                if($location_id!='')
                {
                    $zone_wh_list = $this->User_model->get_wh_by_zone_id($location_id);
                    $sso_wh_arr = $this->Common_model->get_data('user_wh',array('sso_id'=>$sso_id,'status'=>1));
                }
            }
            else if($role_id == 5)
            {
                $data['location_zn_List'] = $this->User_model->get_zonal_data($country_id);
            }
            else if($role_id == 6)
            {
                $data['location_zn_List'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
            }

            $data['zone_wh_list'] = $zone_wh_list;
            $data['sso_wh_arr'] = $sso_wh_arr;
            $data['designationList'] = $this->User_model->get_designationList($role_id);
            $data['country_id'] = $country_id;
            $this->load->view('user/user_view',$data);
        }
    }

    public function update_user()
    {
        #page authentication
        $task_access = page_access_check('manage_user');
        $sso_id=validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($sso_id=='')
        {
            redirect(SITE_URL.'user');
            exit;
        }
        $old_record = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
        
        if(validate_string($this->input->post('submitUser',TRUE)) != '')
        {
            $sso_num   = validate_string($this->input->post('sso_num',TRUE));

            #check sso unique value
            $user_data = array('sso_num'=>$sso_num,'sso_id'=>$sso_id);
            $check_sso = $this->User_model->is_user_sso_Exist($user_data);
            if($check_sso>0)
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> SSO ID : '.$sso_num.' already exist! please check.</div>'); 
                redirect(SITE_URL.'user'); exit();
            }

            #getting input text values
            $role_id        = validate_number($this->input->post('role_id',TRUE));
            $branch_id      = validate_number($this->input->post('branch_id',TRUE));
            $designation_id = validate_number($this->input->post('designation_id',TRUE));
            $location_id    = validate_number($this->input->post('location_id',TRUE));
            $fe_position    = validate_number($this->input->post('fe_position',TRUE));
            $wh_id = validate_number($this->input->post('warehouse_id',TRUE));
            $rm_id = validate_string($this->input->post('rm_id',TRUE));
            $country_id    = validate_number($this->input->post('c_id',TRUE));
            if($rm_id == ''){ $rm_id = NULL; }
            if($role_id != 5 && $role_id != 6 && $location_id == '')
            {
                $location_id = NULL;
            }

            #update user record
            $update_user = array(
            'sso_id'    => $sso_num,
            'name'      => validate_string($this->input->post('user_name',TRUE)),
            'rm_id'     => $rm_id,
            'email'     => validate_string($this->input->post('email',TRUE)),
            'mobile_no' => validate_string($this->input->post('mobile_num',TRUE)),
            'role_id'   => $role_id,
            'designation_id' => $designation_id,
            'branch_id' => $branch_id,
            'address1'  => validate_string($this->input->post('address1',TRUE)),
            'address2'  => validate_string($this->input->post('address2',TRUE)),
            'fe_position' => $fe_position,
            'wh_id'     => validate_string($this->input->post('warehouse_id',TRUE)),
            'location_id' => $location_id,
            'modified_by' => $this->session->userdata('sso_id'),
            'modified_time'  => date('Y-m-d H:i:s'),
            'status'      => 1,
            'country_id'  => $country_id);
           
            $update_where = array('sso_id' => $sso_id);
            $this->db->trans_begin();
            $this->Common_model->update_data('user',$update_user,$update_where);

            #audit data
            unset($update_user['modified_time'],$update_user['modified_by'],$update_user['status']);
            $final_arr1 = array_diff_assoc($update_user, $old_record);
            if(count($final_arr1)>0)
            {
                $remarks_a = "User Modification";
                $ad_id = audit_data('user_master',$sso_id,'user',2,'',$final_arr1,array('sso_id'=>$sso_id),$old_record,$remarks_a,'',array(),'','',$country_id);
            }
            
            if($role_id == 5 || $role_id == 6)
            {
                $new_modality = $this->input->post('modality',TRUE);
                if(count($new_modality)>0 && $sso_id == $sso_num)
                {
                    $old_m = $this->Common_model->get_data('user_modality',array('sso_id'=>$sso_id,'status'=>1));

                    $old_modality = array();
                    foreach ($old_m as $key => $value) 
                    {
                        $old_modality[] = $value['modality_id'];
                    }
                    #inactivate old modality records
                    if(count($old_modality)>0)
                    {
                        $old_modality_data = array_diff($old_modality,$new_modality);
                        if(count($old_modality_data)>0)
                        {
                            foreach ($old_modality_data as $key11 => $value11) 
                            {
                                $update_data11=array('status'=>2,'to_date'=>date('Y-m-d'));
                                $update_data_where11 = array('sso_id'=>$sso_id,'modality_id'=>$value11);
                                $this->Common_model->update_data('user_modality',$update_data11,$update_data_where11);

                                #audit data removed.
                                if(!isset($ad_id))
                                {
                                    $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                                }
                                $removed_record = array('to_date'=>date('Y-m-d'),'modality_id'=>$value11);
                                audit_data('user_modality',$value11,'user_modality',3,$ad_id,array(),array('modality_id'=>$value11),$removed_record,'','',array(),'','',$country_id);
                            }
                        }
                    }
                    
                    #insert new records
                    $new_modality_data = array_diff($new_modality,$old_modality);
                    if(count($new_modality_data)>0)
                    {
                        $exception_arr = array('sso_id');
                        foreach ($new_modality_data as $key => $value) 
                        {
                            $insert_modality1  = array(
                                'sso_id'      =>$sso_id,
                                'modality_id' =>$value,
                                'status'      =>1,
                                'from_date'   =>date('Y-m-d')
                            );
                            $this->Common_model->insert_data('user_modality',$insert_modality1);
                            #audit data
                            if(!isset($ad_id))
                            {
                                $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                            }
                            audit_data('user_modality',$value,'user_modality',1,$ad_id,$insert_modality1,array('modality_id'=>$value),array(),NULL,NULL,$exception_arr,'','',$country_id);
                        } 
                    }
                }
                else
                {
                    if(count($new_modality)>0)
                    {
                        $exception_arr = array('sso_id');
                        foreach($new_modality as $key=>$value)
                        {
                            $insert_modality  = array(
                                'sso_id'      => $sso_num,
                                'modality_id' => $value,
                                'status'      => 1,
                                'from_date'   => date('Y-m-d')
                            );
                            $this->Common_model->insert_data('user_modality',$insert_modality);

                            #audit data
                            if(!isset($ad_id))
                            {
                                $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                            }
                            audit_data('user_modality',$value,'user_modality',1,$ad_id,$insert_modality,array('modality_id'=>$value),array(),NULL,NULL,$exception_arr,'','',$country_id);
                        }
                    }
                    $old_m = $this->Common_model->get_data('user_modality',array('sso_id'=>$sso_id,'status'=>1));
                    if(count($old_m)>0)
                    {
                        foreach ($old_m as $key => $value) 
                        {
                            $modality_id = $value['modality_id'];
                            $update_data11=array('status'=>2,'to_date'=>date('Y-m-d'));
                            $update_data_where11 = array('sso_id'=>$sso_id,'modality_id'=>$modality_id);
                            $this->Common_model->update_data('user_modality',$update_data11,$update_data_where11);

                            #audit data removed.
                            if(!isset($ad_id))
                            {
                                $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                            }
                            $removed_record = array('to_date'=>date('Y-m-d'),'modality_id'=>$modality_id);
                            audit_data('user_modality',$modality_id,'user_modality',3,$ad_id,array(),array('modality_id'=>$modality_id),$removed_record,'','',array(),'','',$country_id);
                        }
                    }
                }
            }
            else
            {
                $old_m = $this->Common_model->get_data('user_modality',array('sso_id'=>$sso_id,'status'=>1));
                if(count($old_m)>0)
                {
                    foreach ($old_m as $key => $value) 
                    {
                        $modality_id = $value['modality_id'];
                        $update_data11=array('status'=>2,'to_date'=>date('Y-m-d'));
                        $update_data_where11 = array('sso_id'=>$sso_id,'modality_id'=>$modality_id);
                        $this->Common_model->update_data('user_modality',$update_data11,$update_data_where11);

                        #audit data removed.
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                        }
                        $removed_record = array('to_date'=>date('Y-m-d'),'modality_id'=>$modality_id);
                        audit_data('user_modality',$modality_id,'user_modality',3,$ad_id,array(),array('modality_id'=>$modality_id),$removed_record,'','',array(),'','',$country_id);
                    }
                }
            }

            #update logistic user entries
            $wh_id = validate_number($this->input->post('warehouse_id',TRUE));
            if($role_id == 3)
            {
                $old_wh_arr = $this->Common_model->get_data('user_wh',array('status'=>1,'sso_id'=>$sso_id));
                $old_wh_array = array();
                foreach ($old_wh_arr as $key => $value) 
                {
                    $old_wh_array[] = $value['wh_id'];
                }
                #multiple warehouses checked
                $warehouses = $this->input->post('warehouse_arr',TRUE);

                if(count($old_wh_array)>0 && count($warehouses)>0)
                {
                    $array = array_diff($old_wh_array,$warehouses);
                    if(count($array)>0)
                    {
                        foreach ($array as $key11 => $value11) 
                        {
                            $update_data11=array('status'=>2);
                            $update_data_where11 = array('sso_id'=>$sso_id,'wh_id'=>$value11);
                            $this->Common_model->update_data('user_wh',$update_data11,$update_data_where11);

                            #audit data removed.
                            if(!isset($ad_id))
                            {
                                $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                            }
                            $removed_record = array('wh_id'=>$value11);
                            audit_data('user_wh',$value11,'user_wh',3,$ad_id,array(),array('wh_id'=>$value11),$removed_record,'','',array(),'','',$country_id);
                        }
                    }
                }
                if(count($old_wh_array)>0 && count($warehouses)>0)
                {
                    $new_wh_arr = array_diff($warehouses, $old_wh_array);
                }
                else
                {
                    $new_wh_arr = $warehouses;
                }
                
                if(count($new_wh_arr)>0)
                {
                    foreach($new_wh_arr as $key=>$wh_result_id)
                    {
                        $this->User_model->insert_update_wh_zone($sso_id,$wh_result_id,1);

                        #audit data.
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                        }
                        $insert_record = array('wh_id'=>$wh_result_id);
                        audit_data('user_wh',$wh_result_id,'user_wh',1,$ad_id,$insert_record,array('wh_id'=>$wh_result_id),array(),'','',array(),'','',$country_id);
                    }
                }
                
                #default wh entry
                if(count($warehouses)>0)
                {
                    if(!in_array($wh_id, $warehouses))
                    {
                        $this->User_model->insert_update_wh_zone($sso_id,$wh_id,1);

                        #audit data.
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                        }
                        $insert_record = array('wh_id'=>$wh_id);
                        audit_data('user_wh',$wh_id,'user_wh',1,$ad_id,$insert_record,array('wh_id'=>$wh_id),array(),'','',array(),'','',$country_id);
                    }
                }
            }
            else
            {
                #change all wh to inactive 2 for sso id
                $old_wh_arr = $this->Common_model->get_data('user_wh',array('status'=>1,'sso_id'=>$sso_id));
                if(count($old_wh_arr)>0)
                {
                    #audit data.
                    if(!isset($ad_id))
                    {
                        $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                    }
                    foreach ($old_wh_arr as $key => $value) 
                    {
                        $update_wh = array('status'=>2);
                        $update_wh_where = array('sso_id'=>$sso_id,'wh_id'=>$value['wh_id']);
                        $this->Common_model->update_data('user_wh',$update_wh,$update_wh_where);

                        $old_data = array('wh_id'=>$value['wh_id']);
                        audit_data('user_wh',$value['wh_id'],'user_wh',3,$ad_id,array(),array('wh_id'=>$value['wh_id']),$old_data,'','',array(),'','',$country_id);
                    }
                }
            }
            
            if($sso_id != $sso_num)
            {
                $update_user_rm = array('rm_id'=>NULL);
                $update_user_rm_where = array('rm_id'=>$sso_id);
                $this->Common_model->update_data('user',$update_user_rm,$update_user_rm_where);

                $this->Common_model->delete_data('user_history',array('sso_id'=>$sso_id));
            }

            #check and update the user history
            $check = 0; $remark_error = '';
            if($old_record['role_id'] != $role_id )
            {
                $check++;
                $remark_error.='User role, ';
            }
            if($old_record['rm_id'] != $rm_id )
            {
                $check++;
                $remark_error.='Reporting Manager, ';
            }
            if($old_record['branch_id'] != $branch_id )
            {
                $check++;
                $remark_error.='Branch, ';
            }
            if($old_record['designation_id'] != $designation_id )
            {
                $check++;
                $remark_error.='Designation, ';
            }
            if($old_record['location_id'] != $location_id )
            {
                $check++;
                $remark_error.='Location, ';
            }
            if($old_record['fe_position'] != $fe_position)
            {
                $check++;
                $remark_error.='FE position, ';
            }
            if($old_record['wh_id'] != $wh_id)
            {
                $check++;
                $remark_error.='Warehouse, ';
            }
            if($old_record['sso_id'] != $sso_num)
            {
                $check++;
                $remark_error.='User SSO, ';
            }
            $remark_error.='Is Changed';
            if($check>0)
            {
                #close the old User History record
                $update_old_record = array('end_date' => date('Y-m-d H:i:s'),'status'=>2,'remarks'=>$remark_error);
                $update_old_where  = array('sso_id' => $sso_id);
                $this->Common_model->update_data('user_history',$update_old_record,$update_old_where);

                #insert new User History Record
                $insert_user_history = array(
                    'sso_id'         => $sso_num,
                    'role_id'        => $role_id,
                    'designation_id' => $designation_id,
                    'branch_id'      => $branch_id,
                    'location_id'    => $location_id,
                    'fe_position'    => $fe_position,
                    'wh_id'          => $wh_id,
                    'rm_id'          => $rm_id,
                    'start_date'     => date('Y-m-d H:i:s'),
                    'created_by'     => $this->session->userdata('sso_id'),
                    'created_time'   => date('Y-m-d H:i:s'),
                    'status'         => 1,
                    'remarks'        => 'Updated User Details'
                    );
                $this->Common_model->insert_data('user_history',$insert_user_history);
            }

            #check for asean country
            $country_arr = $this->Common_model->get_data_row('location',array('level_id'=>2,'location_id'=>$country_id));
            $region_id = $country_arr['region_id'];
            $country_name = $country_arr['name'];
            $check_region_id = get_asean_region_id();
            $asean_country_name = get_asean_country_name();
            if(($check_region_id == $region_id) && ($country_name != $asean_country_name)  && (!in_array($role_id, exclude_Asean_for_roles())))
            {
                #deactivate previous user country data
                $update_data = array('status'=>2);
                $update_where = array('sso_id'=>$sso_id);
                $this->Common_model->update_data('user_country',$update_data,$update_where);

                #insert with actual country
                $this->User_model->insert_update_uc($country_id,$sso_id,1);

                #insert with ASEAN country
                $asean_country_id = $this->Common_model->get_value('location',array('level_id'=>2,'name'=>$asean_country_name),'location_id');
                $this->User_model->insert_update_uc($asean_country_id,$sso_id,1);
            }
            else if(in_array($role_id, exclude_Asean_for_roles()) && ($check_region_id == $region_id) && ($country_name != $asean_country_name))
            {
                #deactivate previous user country data
                $update_data = array('status'=>2);
                $update_where = array('sso_id'=>$sso_id);
                $this->Common_model->update_data('user_country',$update_data,$update_where);
            }
            
            if($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong>Something Went Wrong! Please check.</div>');
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> SSO : <strong>'.$sso_id.'</strong> has been Updated successfully!</div>');
                       
            }
        } 
        redirect(SITE_URL.'user'); exit();
    }      

    //created by gowri on 27/aug/18
    public function get_wh_by_zone_user()
    {
        $location_id = validate_number($this->input->post('location_id',TRUE));
        $result = $this->User_model->get_wh_by_zone($location_id);
        $data = array('result'=>$result);
        echo json_encode($data);
    }//ended by gowri on 27/aug/18

    public function deactivate_user($encoded_id)
    {
        $task_access = page_access_check('manage_user');
        $sso_id=@storm_decode($encoded_id);
        if($sso_id==''){
            redirect(SITE_URL.'user');
            exit;
        }
        
        #deactivating user
        $data_arr = array('status' => 2);
        $where_1 = array('sso_id' => $sso_id);
        $this->Common_model->update_data('user',$data_arr, $where_1);

        #audit data
        $old_data = array('user_status'=>'Active');
        $new_data = array('user_status'=>'Deactivated');
        audit_data('user_master',$sso_id,'user',2,'',$new_data,array('sso_id'=>$sso_id),$old_data,'','',array(),'','',$country_id);

        //deactivate the user history
        $data_arr_history = array(
            'status'   => 2,
            'end_date' => date('Y-m-d H:i:s'),
            'remarks'  => 'User Deactivated'
        );
        $where_2 = array('sso_id' => $sso_id,'status'=>1);
        $this->Common_model->update_data('user_history',$data_arr, $where_2);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> User has been deactivated successfully! </div>');
        redirect(SITE_URL.'user');

    }

    public function activate_user($encoded_id)
    {
        $task_access = page_access_check('manage_user');
        $sso_id = @storm_decode($encoded_id);
        if($sso_id==''){
            redirect(SITE_URL.'sso_id');
            exit;
        }
        
        #Activate user
        $data_arr = array('status' => 1);
        $where = array('sso_id' => $sso_id);
        $this->Common_model->update_data('user',$data_arr, $where);

        #audit data
        $old_data = array('user_status'=>'Deactive');
        $new_data = array('user_status'=>'Activated');
        audit_data('user_master',$sso_id,'user',2,'',$new_data,array('sso_id'=>$sso_id),$old_data,'','',array(),'','',$country_id);

        #insert new User History Record
        $user_data = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
        $insert_user_history = array(
            'sso_id'         => $sso_id,
            'role_id'        => $user_data['role_id'],
            'designation_id' => $user_data['designation_id'],
            'branch_id'      => $user_data['branch_id'],
            'location_id'    => $user_data['location_id'],
            'start_date'     => date('Y-m-d H:i:s'),
            'created_by'     => $this->session->userdata('sso_id'),
            'created_time'   => date('Y-m-d H:i:s'),
            'status'         => 1,
            'remarks'        => 'Activated User Details'
        );
        $this->Common_model->insert_data('user_history',$insert_user_history);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> User has been Activated successfully! </div>');
        redirect(SITE_URL.'user');
    }

    #checking SSO ID number uniqueness
    public function is_user_sso_Exist()
    {
        $sso_num = validate_string($this->input->post('sso_num',TRUE));
        $sso_id = validate_number($this->input->post('sso_id',TRUE));

        $data = array('sso_num'=>$sso_num,'sso_id'=>$sso_id);
        $result = $this->User_model->is_user_sso_Exist($data);
        echo $result;
    }

    public function get_designation_by_role()
    {
        $role_id = validate_number($this->input->post('role_id',TRUE));
        $result = $this->User_model->get_designation_by_role($role_id);
        $data = array('result'=>$result);
        echo json_encode($data);
    }

    public function get_location_by_role()
    {
        $role_id = validate_number($this->input->post('role_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $result = $this->User_model->get_location_by_role($role_id,$country_id);
        $data = array('result'=>$result);
        echo json_encode($data);
    }

    public function get_locations_by_role()
    {
        $role_id = validate_number($this->input->post('role_id',TRUE));
        $result = $this->User_model->get_locations_by_role($role_id);
        echo $result;
    }

    public function bulkupload_user()
    {
        $data['nestedView']['heading']="User Bulk Upload";
        $data['nestedView']['cur_page'] = 'manage_user';
        $data['nestedView']['parent_page'] = 'manage_user';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/user.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'User Bulk Upload';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage User','class'=>'','url'=>SITE_URL.'user');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'User Bulk Upload','class'=>'active','url'=>'');

        #additional Data        
        $data['form_action'] = SITE_URL.'insert_bulkupload_user';  
        $this->load->view('user/user_upload_view',$data);
    } 

  public function insert_bulkupload_user()
  {
      #page authentication
      $task_access = page_access_check('manage_user');
      if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
      { 
          $filename=$_FILES["uploadCsv"]["tmp_name"];            
          if($_FILES["uploadCsv"]["size"] > 0)
          {
              $this->db->trans_begin();
              $file = fopen($filename, "r");
              $i=0;    
              $j=0;
              $insert = 0;
              $check = 0; 
              $update = 0;

                #inserting in upload table with default status 
                $upload_data = array(
                    'type'         => 4, // for User Upload
                    'status'       => 1,                                
                    'created_by'   => $this->session->userdata('sso_id'),
                    'created_time' => date('Y-m-d H:i:s'),
                    'file_name'    => $_FILES["uploadCsv"]["name"]
                );
                $upload_id = $this->Common_model->insert_data('upload',$upload_data);

              #loop the records fetched from csv file
              while (($userData = fgetcsv($file, 10000, ",")) !== FALSE)
              { 

                  #exclude the first record as contain heading
                  if($j==0) { $j++; continue; } 

                  #assign data to variable as feteched individually
                  $sso_id           = validate_string(trim($userData[0]));
                  $name             = validate_string(trim($userData[1]));
                  $rm_id            = validate_string(trim($userData[2]));
                  $email            = validate_string(trim($userData[3]));
                  $mobile           = validate_string(trim($userData[4]));
                  $role_name        = validate_string(trim($userData[5]));
                  $designation_name = validate_string(trim($userData[6]));
                  $wh_code          = validate_string(trim($userData[7]));
                  $branch_name      = validate_string(trim($userData[8]));
                  $address1         = validate_string(trim($userData[9]));
                  $address2         = validate_string(trim($userData[10]));
                  $fe_position_name = validate_string(trim($userData[11]));
                  $assigned_location= validate_string(trim($userData[12]));
                  $modalityList     = validate_string(trim($userData[13]));
                  $country          = validate_string(trim($userData[14]));
                  $country_id = '';
                  if($country!='')
                  {
                     $country_id = $this->Common_model->get_value('location',array('name'=>$country,'level_id'=>2),'location_id');
                  }
                  

                    if($sso_id !='' && $name !='' && $email !='' && $mobile !='' 
                      && $role_name !='' && $designation_name !='' && $wh_code !=''
                      && $branch_name !='' && $address1 !='' && $fe_position_name !='' 
                      && @$country_id !='' && $country !='')
                    {
                        $check =0; $remark_error = '';

                        $check_sso = $this->Common_model->get_data('user',array('sso_id'=>trim($sso_id)));

                        #check Role Name
                        $role_name = trim($role_name);
                        $role_id = $this->Common_model->get_value('role',array('name'=>$role_name),'role_id');
                        if($role_id == '')
                        {
                          $role_id = 0;
                          $check++;
                          $remark_error.= 'Role :"'.$role_name.'" is Not Available in System, ';
                        }

                        if($rm_id!=0 && $rm_id!='')
                        {
                            $check_rm = $this->Common_model->get_data('user',array('sso_id'=>trim($rm_id),'country_id'=>$country_id));
                            if(count(@$check_rm)==0 && @$rm_id!=0)
                            {
                              $check++;
                              $remark_error.= 'Reporting Manager ID : "'.@$rm_id.'" is not Available in System, ';
                            }
                        }
                        else if($rm_id == '')
                        {
                            $rm_id = 0;
                        }
                        
                        if($role_id != '')
                        {
                            if($role_id==2 || $role_id==5 || $role_id==6)
                            {
                                if($rm_id==0)
                                {
                                    $check++;
                                    $remark_error.= 'Reporting Manager SSO ID is Mandatory, ';
                                }
                            }
                        }
                        
                        #check email format
                        $email = trim($email);
                        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {} 
                        else 
                        {
                          $check++;
                          $remark_error.= 'Email :"'.$email.'" is Invalid, ';
                        }

                        #check mobile format
                        $mobile = trim($mobile);

                        #check designation Name
                        $designation_name = trim($designation_name);
                        $designation_id = $this->User_model->check_designation_by_role_id($role_id,$designation_name);
                        if($designation_id == '')
                        {
                          $check++;
                          $remark_error.= 'Designation :"'.$designation_name.'" is Not Available in System/Not matched with Role Name, ';
                        }

                        #check Branch
                        $branch_name = trim($branch_name);
                        $branch_id = $this->Common_model->get_value('branch',array('name'=>$branch_name,'country_id'=>$country_id),'branch_id');
                        if($branch_id == '')
                        {
                          $check++;
                          $remark_error.= 'Branch :"'.$branch_name.'" is Not Available in System, ';
                        }

                        #check Warehouse Code
                        $wh_code = trim($wh_code);
                        $wh_id = $this->Common_model->get_value('warehouse',array('wh_code'=>$wh_code,'country_id'=>$country_id,'status'=>1),'wh_id');
                        if($wh_id == '')
                        {
                            $check++;
                            $remark_error.= 'Warehouse Code :"'.$wh_code.'" is Not Available in System, ';
                        }

                        #check FE Position
                        $fe_position_name = trim($fe_position_name);
                        $fe_position=$this->User_model->get_fe_position_id($country_id,$fe_position_name);
                        if($fe_position == '' || $fe_position==0)
                        {
                            $check++;
                            $remark_error.='FE Position '.$fe_position_name.' is not available in System. ';
                        } 

                        #check for national and Zonal
                        if(@$role_id !='')
                        {
                            if(@$role_id == 5 || @$role_id == 6)
                            {
                                #check assigned Location availablity
                                if($assigned_location == '')
                                {
                                    $check++;
                                    $remark_error.='Assigned Location is missing. ';
                                }
                                else
                                {
                                    $assigned_location = trim($assigned_location);
                                    if($role_id == 5)
                                    {
                                        //$location_id = $this->Common_model->get_value('location',array('name'=>$assigned_location,'level_id'=>3),'location_id');
                                        $location_id=$this->User_model->get_zone_id($country_id,$assigned_location);
                                    }
                                    else if($role_id == 6)
                                    {
                                        $location_id = $this->Common_model->get_value('location',array('name'=>$assigned_location,'level_id'=>2),'location_id');
                                    }
                                    if($location_id == '')
                                    {
                                        $check++;
                                        $remark_error.='Assigned Location : '.$assigned_location.' is Not Available in system/Wrongly Mapped. ';   
                                    }
                                    
                                }

                                #check modalitylist
                                if($modalityList == '')
                                {
                                    $check++;
                                    $remark_error.='Modality is Missing. ';
                                }
                                else
                                {
                                    $modality_arr = array();
                                     $modality_name = explode('/', $modalityList);
                                     foreach ($modality_name as $key => $value) 
                                     {
                                        $m_name = trim($value);
                                        $modality_id = $this->Common_model->get_value('modality',array('modality_code'=>$m_name),'modality_id');
                                        if($modality_id == '')
                                        {
                                            $check++;
                                            $remark_error.='Modality Code : '.$name.' is Invalid. ';
                                        }
                                        else
                                        {
                                            $modality_arr[] = $modality_id;
                                        }
                                     }
                                }
                            }
                        }

                        if($check>0)
                        {
                            $insert_missed_record = array(
                              'upload_id'        => $upload_id,
                              'sso_id'           => $sso_id,
                              'name'             => $name,
                              'rm_id'            => $rm_id,
                              'email'            => $email,
                              'mobile_number'    => $mobile,
                              'role_name'        => $role_name,
                              'designation_name' => $designation_name,
                              'branch_name'      => $branch_name,
                              'wh_code'          => $wh_code,
                              'address1'         => $address1,
                              'address2'         => $address2,
                              'fe_position'      => $fe_position_name,
                              'assigned_location'=> $assigned_location,
                              'modality_list'    => $modalityList,
                              'remarks'          => $remark_error,
                              'country_name'     => $country,
                              'created_by'       => $this->session->userdata('sso_id'),
                              'created_time'     => date('Y-m-d H:i:s'),
                              'status'           => 1
                                                   );
                          $this->Common_model->insert_data('upload_user',$insert_missed_record);
                          $i++;
                        }
                        else if(count(@$check_sso) == 0)
                        {
                            #insert User record
                            if(@$role_id != 5 && @$role_id != 6)
                            {
                              $location_id = NULL;
                            }
                            $insert_user = array(
                                'sso_id'         => $sso_id,
                                'name'           => $name,
                                'rm_id'          => $rm_id,
                                'email'          => $email,
                                'mobile_no'      => $mobile,
                                'role_id'        => $role_id,
                                'designation_id' => $designation_id,
                                'branch_id'      => $branch_id,
                                'wh_id'          => $wh_id,
                                'address1'       => $address1,
                                'address2'       => $address2,
                                'location_id'    => $location_id,
                                'country_id'     => $country_id,
                                'fe_position'    => $fe_position,
                                'created_by'     => $this->session->userdata('sso_id'),
                                'created_time'   => date('Y-m-d H:i:s'),
                                'status'         => 1
                            );
                            $user_id = $this->Common_model->insert_data('user',$insert_user);

                            #audit data
                            $remarks_a = "User Creation";
                            $ad_id = audit_data('user_master',$user_id,'user',1,'',$insert_user,array('sso_id'=>$user_id),array(),$remarks_a,'',array(),'','',$country_id);

                            #insert_user_history
                            $insert_user_history = array(
                            'sso_id'         => $sso_id,
                            'role_id'        => $role_id,
                            'designation_id' => $designation_id,
                            'branch_id'      => $branch_id,
                            'location_id'    => $location_id,
                            'wh_id'          => $wh_id,
                            'fe_position'    => $fe_position,
                            'rm_id'          => $rm_id,
                            'start_date'     => date('Y-m-d H:i:s'),
                            'created_by'     => $this->session->userdata('sso_id'),
                            'created_time'   => date('Y-m-d H:i:s'),
                            'status'         => 1,
                            'remarks'        => 'User Creation'
                                         );
                          $this->Common_model->insert_data('user_history',$insert_user_history);

                          if(@$role_id == 5 || @$role_id == 6)
                          {
                            foreach($modality_arr as $key=>$value)
                            {
                                $insert_modality  = array(
                                'sso_id'      =>$user_id,
                                'modality_id' =>$value,
                                'status'      =>1,
                                'from_date'   =>date('Y-m-d'));
                                $this->Common_model->insert_data('user_modality',$insert_modality);

                                $insert_data = array('modality_id'=>$value,'from_date'=>date('Y-m-d'));
                                audit_data('user_modality',$value,'user_modality',1,$ad_id,$insert_data,array('modality_id'=>$value),array(),'','',array(),'','',$country_id);
                            }
                          }

                            #check for asean country
                            $country_arr = $this->Common_model->get_data_row('location',array('level_id'=>2,'location_id'=>$country_id));
                            $region_id = $country_arr['region_id'];
                            $country_name = $country_arr['name'];
                            $check_region_id = get_asean_region_id();
                            $asean_country_name = get_asean_country_name();
                            if(($check_region_id == $region_id) && ($country_name != $asean_country_name)  && (!in_array($role_id, exclude_Asean_for_roles())))
                            {
                                $sso_id = $user_id;
                                #deactivate previous user country data
                                $update_data = array('status'=>2);
                                $update_where = array('sso_id'=>$sso_id);
                                $this->Common_model->update_data('user_country',$update_data,$update_where);

                                #insert with actual country
                                $this->User_model->insert_update_uc($country_id,$sso_id,1);
                                #audit data
                                audit_data('user_country',$country_id,'user_country',1,$ad_id,array('location_id'=>$country_id),array('country_id'=>$country_id),array(),'','',array(),'','',$country_id);

                                #insert with ASEAN country
                                $asean_country_id = $this->Common_model->get_value('location',array('level_id'=>2,'name'=>$asean_country_name),'location_id');
                                $this->User_model->insert_update_uc($asean_country_id,$sso_id,1);
                                #audit data
                                audit_data('user_country',$asean_country_id,'user_country',1,$ad_id,array('location_id'=>$asean_country_id),array('country_id'=>$asean_country_id),array(),'','',array(),'','',$country_id);
                            }

                          $insert++;
                        }
                        else
                        {
                            if($role_id != 5 && $role_id != 6)
                            {
                              $location_id = NULL;
                            }
                            
                            $old_user_record = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
                            $update_user_data = array(
                            'name'           => $name,
                            'rm_id'          => $rm_id,
                            'email'          => $email,
                            'mobile_no'      => $mobile,
                            'role_id'        => $role_id,
                            'designation_id' => $designation_id,
                            'branch_id'      => $branch_id,
                            'wh_id'          => $wh_id,
                            'address1'       => $address1,
                            'address2'       => $address2,
                            'location_id'    => $location_id,
                            'country_id'     => $country_id,
                            'fe_position'    => $fe_position,
                            'created_by'     => $this->session->userdata('sso_id'),
                            'created_time'   => date('Y-m-d H:i:s'),
                            'status'         => 1
                            );
                          $uuwhere = array('sso_id'=>$sso_id);
                          $this->Common_model->update_data('user',$update_user_data,$uuwhere);

                          $final_arr = array_diff_assoc($update_user_data, $old_user_record);
                          if(count($final_arr)>0)
                          {
                            $remarks_a = "User Modification";
                            $ad_id = audit_data('user_master',$sso_id,'user',2,'',$final_arr,array('sso_id'=>$sso_id),$old_user_record,$remarks_a,'',array(),'','',$country_id);
                          }

                          $uuhd = array('end_date'=>date('Y-m-d H:i:s'));
                          $uuhw = array('sso_id'=>$sso_id,'end_date'=>NULL);
                          $this->Common_model->update_data('user_history',$uuhd,$uuhw);

                          #insert_user_history
                          $insert_user_history = array(
                            'sso_id'         => $sso_id,
                            'role_id'        => $role_id,
                            'designation_id' => $designation_id,
                            'branch_id'      => $branch_id,
                            'location_id'    => $location_id,
                            'wh_id'          => $wh_id,
                            'fe_position'    => $fe_position,
                            'rm_id'          => $rm_id,
                            'start_date'     => date('Y-m-d H:i:s'),
                            'created_by'     => $this->session->userdata('sso_id'),
                            'created_time'   => date('Y-m-d H:i:s'),
                            'status'         => 1,
                            'remarks'        => 'User Creation'
                          );
                          $this->Common_model->insert_data('user_history',$insert_user_history);

                            $update_where = array('sso_id'=>$sso_id);
                            if($role_id == 5 || $role_id == 6)
                            {
                                $new_modality = $modality_arr;
                                if(count($new_modality)>0)
                                {
                                    $old_m = $this->Common_model->get_data('user_modality',array('sso_id'=>$sso_id,'status'=>1));

                                    $old_modality = array();
                                    foreach ($old_m as $key => $value) 
                                    {
                                        $old_modality[] = $value['modality_id'];
                                    }
                                    #inactivate old modality records
                                    if(count($old_modality)>0)
                                    {
                                        $old_modality_data = array_diff($old_modality,$new_modality);
                                        if(count($old_modality_data)>0)
                                        {
                                            foreach ($old_modality_data as $key11 => $value11) 
                                            {
                                                $update_data11=array('status'=>2,'to_date'=>date('Y-m-d'));
                                                $update_data_where11 = array('sso_id'=>$sso_id,'modality_id'=>$value11);
                                                $this->Common_model->update_data('user_modality',$update_data11,$update_data_where11);

                                                #audit data removed.
                                                if(!isset($ad_id))
                                                {
                                                    $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                                                }
                                                $removed_record = array('to_date'=>date('Y-m-d'),'modality_id'=>$value11);
                                                audit_data('user_modality',$value11,'user_modality',3,$ad_id,array(),array('modality_id'=>$value11),$removed_record,'','',array(),'','',$country_id);
                                            }
                                        }
                                    }
                                    
                                    #insert new records
                                    $new_modality_data = array_diff($new_modality,$old_modality);
                                    if(count($new_modality_data)>0)
                                    {
                                        $exception_arr = array('sso_id');
                                        foreach ($new_modality_data as $key => $value) 
                                        {
                                            $insert_modality1  = array(
                                                'sso_id'      =>$sso_id,
                                                'modality_id' =>$value,
                                                'status'      =>1,
                                                'from_date'   =>date('Y-m-d')
                                            );
                                            $this->Common_model->insert_data('user_modality',$insert_modality1);
                                            #audit data
                                            if(!isset($ad_id))
                                            {
                                                $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                                            }
                                            audit_data('user_modality',$value,'user_modality',1,$ad_id,$insert_modality1,array('modality_id'=>$value),array(),NULL,NULL,$exception_arr,'','',$country_id);
                                        } 
                                    }
                                }
                            }
                            else
                            {
                                $old_m = $this->Common_model->get_data('user_modality',array('sso_id'=>$sso_id,'status'=>1));
                                if(count($old_m)>0)
                                {
                                    foreach ($old_m as $key => $value) 
                                    {
                                        $modality_id = $value['modality_id'];
                                        $update_data11=array('status'=>2,'to_date'=>date('Y-m-d'));
                                        $update_data_where11 = array('sso_id'=>$sso_id,'modality_id'=>$modality_id);
                                        $this->Common_model->update_data('user_modality',$update_data11,$update_data_where11);

                                        #audit data removed.
                                        if(!isset($ad_id))
                                        {
                                            $ad_id = audit_data('user_master',$sso_id,'user',2,'',array(),array('sso_id'=>$sso_id),array(),'','',array(),'','',$country_id);
                                        }
                                        $removed_record = array('to_date'=>date('Y-m-d'),'modality_id'=>$modality_id);
                                        audit_data('user_modality',$modality_id,'user_modality',3,$ad_id,array(),array('modality_id'=>$modality_id),$removed_record,'','',array(),'','',$country_id);
                                    }
                                }
                            }

                            #check for asean country
                            $country_arr = $this->Common_model->get_data_row('location',array('level_id'=>2,'location_id'=>$country_id));
                            $region_id = $country_arr['region_id'];
                            $country_name = $country_arr['name'];
                            $check_region_id = get_asean_region_id();
                            $asean_country_name = get_asean_country_name();
                            if(($check_region_id == $region_id) && ($country_name != $asean_country_name)  && (!in_array($role_id, exclude_Asean_for_roles())))
                            {
                                #deactivate previous user country data
                                $update_data = array('status'=>2);
                                $update_where = array('sso_id'=>$sso_id);
                                $this->Common_model->update_data('user_country',$update_data,$update_where);

                                #insert with actual country
                                $this->User_model->insert_update_uc($country_id,$sso_id,1);

                                #insert with ASEAN country
                                $asean_country_id = $this->Common_model->get_value('location',array('level_id'=>2,'name'=>$asean_country_name),'location_id');
                                $this->User_model->insert_update_uc($asean_country_id,$sso_id,1);
                            }
                            $update++;
                        }
                    }
                    else
                    {
                      $remarks_string ='';
                      if($sso_id =='')           $remarks_string.='SSO ID, ';
                      if($name =='')             $remarks_string.='Name, ';
                      if($email =='')            $remarks_string.='Email, ';
                      if($mobile =='')           $remarks_string.='Mobile Number, ';
                      if($role_name =='')        $remarks_string.='Role, ';
                      if($designation_name =='') $remarks_string.='Designation, ';
                      if($branch_name =='')      $remarks_string.='Branch, ';
                      if($wh_code =='')          $remarks_string.='Warehouse_code, ';
                      if($address1 =='')         $remarks_string.='Address1, ';
                      if($fe_position_name =='') $remarks_string.='FE Position, ';
                      if($country == '' )         $remarks_string.='Country, ';

                      if($country_id == '')      $remarks_string.='Country ';
                      $remarks_string.='Is missing/Not Available.';
                      

                      $insert_missed_record = array(
                              'upload_id'        => $upload_id,
                              'sso_id'           => $sso_id,
                              'name'             => $name,
                              'rm_id'            => $rm_id,
                              'email'            => $email,
                              'mobile_number'    => $mobile,
                              'role_name'        => $role_name,
                              'designation_name' => $designation_name,
                              'branch_name'      => $branch_name,
                              'wh_code'          => $wh_code,
                              'address1'         => $address1,
                              'address2'         => $address2,
                              'fe_position'      => $fe_position_name,
                              'country_name'     => $country,
                              'assigned_location'=> $assigned_location,
                              'modality_list'    => $modalityList,
                              'remarks'          => $remarks_string,
                              'created_by'       => $this->session->userdata('sso_id'),
                              'created_time'     => date('Y-m-d H:i:s'),
                              'status'           => 1
                                                   );
                      $this->Common_model->insert_data('upload_user',$insert_missed_record);
                      $i++;
                  }
                  $j++;
              }
              fclose($file);
              if ($this->db->trans_status() === FALSE)
              {
                  $this->db->trans_rollback();
                  $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                 </div>'); 
                  redirect(SITE_URL.'user');  
              }
              else
              {
                  $this->db->trans_commit();
                  if($i>0)
                  {
                    $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> successfully Inserted Records : <strong>'.$insert.'</strong>, Updated Records : <strong>'.$update.'</strong>, Missed records : <strong>'.$i.'</strong> Check below!.
                    </div>');
                      redirect(SITE_URL.'missed_user_list/'.storm_encode($upload_id));  exit();
                  }
                  else if($j==1)
                  {
                      $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <div class="icon"><i class="fa fa-times-circle"></i></div>
                      <strong>Error!</strong> No records Captured. Uploaded Empty file!
                      </div>'); 
                      redirect(SITE_URL.'bulkupload_user'); exit();
                  }
                  else
                  {
                      $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <div class="icon"><i class="fa fa-check"></i></div>
                      <strong>Success!</strong> No of records inserted :<strong>'.$insert.'</strong>, Updated : <strong>'.$update.'</strong>  successfully! 
                      </div>');
                      redirect(SITE_URL.'user'); exit();
                  }
              }
          } 
          else
          {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
            redirect(SITE_URL.'bulkupload_user'); exit();
          }  
      }
      else
      {
          redirect(SITE_URL.'bulkupload_user'); exit();
      }
  }
  public function missed_user_list()
  {
    
      $upload_id = @storm_decode($this->uri->segment(2));
      if($upload_id == '')
      {
        redirect(SITE_URL.'user'); exit();
      }

      $data['nestedView']['heading']="Missed List of User Bulk Upload";
      $data['nestedView']['cur_page'] = 'manage_user';
      $data['nestedView']['parent_page'] = 'manage_user';

      #page authentication
      $task_access = page_access_check($data['nestedView']['parent_page']);

      # include files
      $data['nestedView']['js_includes'] = array();
      $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
      $data['nestedView']['css_includes'] = array();

      # Breadcrumbs
      $data['nestedView']['breadCrumbTite'] = 'Missed List of User Bulk Upload';
      $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage User','class'=>'','url'=>SITE_URL.'user');
      $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Missed User Bulk Upload','class'=>'active','url'=>'');
  
      # Default Records Per Page - always 10
      /* pagination start */
      $config = get_paginationConfig();
      $config['base_url'] = SITE_URL.'missed_user_list/'.storm_encode($upload_id).'/';
      # Total Records
      $config['total_rows'] = $this->User_model->missed_user_total_num_rows($upload_id);

      $config['per_page'] = getDefaultPerPageRecords();
      $data['total_rows'] = $config['total_rows'];
      $this->pagination->initialize($config);
      $data['pagination_links'] = $this->pagination->create_links();
      $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      if ($data['pagination_links'] != '') {
          $data['last'] = $this->pagination->cur_page * $config['per_page'];
          if ($data['last'] > $data['total_rows']) {
              $data['last'] = $data['total_rows'];
          }
          $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
      }
      $data['sn'] = $current_offset + 1;
      /* pagination end */

      # Loading the data array to send to View
      $data['missedResults'] = $this->User_model->missed_user_results($current_offset, $config['per_page'], $upload_id);

      #additional Data 
      $data['upload_id'] = $upload_id; 
      $this->load->view('user/missed_user_upload_view',$data);
  }

  public function download_missed_user_list()
  {
    
    #page authentication
    $task_access = page_access_check('manage_user');

    $upload_id = @storm_decode($this->uri->segment(2));
    if($upload_id == '')
    {
        redirect(SITE_URL.'user'); exit();
    }
    $print_data = $this->Common_model->get_data('upload_user',array('upload_id'=>$upload_id));
    $header = '';
    $data ='';
    $titles = array('SSO ID','Name','Reporting Manager(SSO ID)','Email','Mobile Number','Role','Designation','Warehouse Code','Branch','Address1','Address2','FE Position','Assigned Location','Modality Code','Country Name','Remarks');
    $data = '<table border="1">';
    $data.='<thead>';
    $data.='<tr>';
    foreach ( $titles as $title)
    {
        $data.= '<th align="center">'.$title.'</th>';
    }
    $data.='</tr>';
    $data.='</thead>';
    $data.='<tbody>';
     
    if(count($print_data)>0)
    {
      foreach($print_data as $row)
      {
          if($row['rm_id']==0){ $rm_id = "";}
          else { $rm_id = $row['rm_id']; }
          $data.='<tr>';                
          $data.='<td align="center">'.$row['sso_id'].'</td>';                   
          $data.='<td align="center">'.$row['name'].'</td>';                   
          $data.='<td align="center">'.$rm_id.'</td>';                   
          $data.='<td align="center">'.$row['email'].'</td>'; 
          $data.='<td align="center">'.$row['mobile_number'].'</td>';                   
          $data.='<td align="center">'.$row['role_name'].'</td>';                   
          $data.='<td align="center">'.$row['designation_name'].'</td>'; 
          $data.='<td align="center">'.$row['wh_code'].'</td>';                   
          $data.='<td align="center">'.$row['branch_name'].'</td>'; 
          $data.='<td align="center">'.$row['address1'].'</td>';                   
          $data.='<td align="center">'.$row['address2'].'</td>';                   
          $data.='<td align="center">'.$row['fe_position'].'</td>';    
          $data.='<td align="center">'.$row['assigned_location'].'</td>';
          $data.='<td align="center">'.$row['modality_list'].'</td>';
          $data.='<td align="center">'.$row['country_name'].'</td>'; 
          $data.='<td align="center">'.$row['remarks'].'</td>'; 
          $data.='</tr>';
         
      }
    }
    else
    {
        $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Results Found</td></tr>';
    }
    $data.='</tbody>';
    $data.='</table>';
    $time = date("Ymdhis");
    $xlFile='missed_userList_'.$time.'.xls'; 
    header("Content-type: application/x-msdownload"); 
    # replace excelfile.xls with whatever you want the filename to default to
    header("Content-Disposition: attachment; filename=".$xlFile."");
    header("Pragma: no-cache");
    header("Expires: 0");
    echo $data;
  }

    public function fe_login_update()
    {

        if(validate_number($this->input->post('submitUser',TRUE))!='')
        {
            $sso_id = $this->session->userdata('sso_id');
            $old_fe_position = validate_string($this->input->post('old_fe_position',TRUE));
            $new_fe_position = validate_string($this->input->post('new_fe_position',TRUE));

            if(($old_fe_position != $new_fe_position) && $old_fe_position !='' && $new_fe_position != '')
            {
                $this->db->trans_begin();

                #check
                $check = 0;
                $old_rec = $this->Common_model->get_data_row('fe_position_change',array('approval_status' => 1,'sso_id' => $sso_id));
                if(count($old_rec)>0)
                {
                    $requested_location = $old_rec['new_fe_position'];
                    if($new_fe_position != $requested_location)
                    {
                        #update old record to reject
                        $update_data = array(
                        'approval_status' => 3,
                        'modified_by'     => $sso_id,
                        'modified_time'   => date('Y-m-d H:i:s'),
                        'status'          => 1);
                        $update_data_where = array('fpc_id' => $old_rec['fpc_id']);
                        $this->Common_model->update_data('fe_position_change',$update_data,$update_data_where);

                        #insert new request
                        $insert_data = array(
                        'old_fe_position' => $old_fe_position,
                        'new_fe_position' => $new_fe_position,
                        'approval_status' => 1,
                        'sso_id'          => $sso_id,
                        'created_by'      => $sso_id,
                        'created_time'    => date('Y-m-d H:i:s'),
                        'status'          => 1);
                        $this->Common_model->insert_data('fe_position_change',$insert_data);
                    }
                }
                else
                {
                    $insert_data = array(
                    'old_fe_position' => $old_fe_position,
                    'new_fe_position' => $new_fe_position,
                    'approval_status' => 1,
                    'sso_id'          => $sso_id,
                    'created_by'      => $sso_id,
                    'created_time'    => date('Y-m-d H:i:s'),
                    'status'          => 1);
                    $this->Common_model->insert_data('fe_position_change',$insert_data);
                }

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.</div>'); 
                }
                else
                {
                    $this->db->trans_commit();
                    $this->session->set_flashdata('response','<div class=" col-md-offset-1 col-md-10 alert alert-success alert-white rounded" style="margin-top:10px;">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> Location Change Request is intimated to Admin for <strong>Approval !</strong> 
                    </div>');
                }
                $data['l_modal'] = 2;
                $this->session->set_userdata($data);
                
            }
            else if($old_fe_position == $new_fe_position && $old_fe_position !='' && $new_fe_position != '')
            {
                $data['l_modal'] = 2;
                $this->session->set_userdata($data);
            }
        }
        else
        {
            $data['l_modal'] = 2;
            $this->session->set_userdata($data);
        }
        redirect(SITE_URL);
    }

    public function assign_user_country()
    {
        $super_admin_arr = $this->Common_model->get_data('user',array('role_id'=>4));
        foreach ($super_admin_arr as $key => $value) 
        {
            $sso_id = $value['sso_id'];
            $country_id = $value['country_id'];
            $region_id = $this->Common_model->get_value('location',array('location_id'=>$country_id),'region_id');

            if($region_id!='')
            {
                $assigned_country = $this->Common_model->get_data('user_country',array('sso_id'=>$sso_id));
                if(count($assigned_country)>0)
                {
                    $update_data = array('status'=>2);
                    $update_where = array('sso_id'=>$sso_id);
                    $this->Common_model->update_data('user_country',$update_data,$update_where);
                }

                #insert new user country data

                $country_list = $this->Common_model->get_data('location',array('region_id'=>$region_id));
                foreach ($country_list as $key1 => $value1) 
                {
                    $this->User_model->insert_update_uc($value1['location_id'],$sso_id,1);
                }
            }
        }
        $this->session->set_flashdata('response','<div class=" col-md-offset-1 col-md-10 alert alert-success alert-white rounded" style="margin-top:10px;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> User Countries Assigned Successfully! 
        </div>');
        redirect(SITE_URL.'home'); exit();
    }

    public function assign_asean_as_default_country()
    {
        $region_id = get_asean_region_id();
        $asean_users = $this->User_model->get_asean_users($region_id);
        $asean_country_id = $this->Common_model->get_value('location',array('name'=>get_asean_country_name(),'level_id'=>2),'location_id');
        foreach ($asean_users as $key => $value) 
        {
            $sso_id = $value['sso_id'];
            $country_id = $value['country_id'];

            $update = array('status'=>2);
            $update_where = array('sso_id'=>$sso_id);
            $this->Common_model->update_data('user_country',$update,$update_where);

            $this->User_model->insert_update_uc($country_id,$sso_id,1);
            $this->User_model->insert_update_uc($asean_country_id,$sso_id,1);
        }
        $this->session->set_flashdata('response','<div class=" col-md-offset-1 col-md-10 alert alert-success alert-white rounded" style="margin-top:10px;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Asean Assigned to Asean Country Users Successfully! 
        </div>');
        redirect(SITE_URL.'home'); exit();
    }
}