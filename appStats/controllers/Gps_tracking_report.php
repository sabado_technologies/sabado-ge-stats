<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Gps_tracking_report extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Gps_tracking_report_m');
    }

	public function gps_report_view()
    {
        $data['nestedView']['heading']="Tool Tracking Report";
		$data['nestedView']['cur_page'] = 'gps_tracking_report';
		$data['nestedView']['parent_page'] = 'gps_tracking_report';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Tool Tracking Report';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Tracking Report','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('search', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'tool_number'      => validate_string($this->input->post('tool_number', TRUE)),
                'tool_description' => validate_string($this->input->post('tool_description', TRUE)),
                'country_id'       => validate_number($this->input->post('country_id',TRUE)),
                'asset_number'     => validate_string($this->input->post('asset_number',TRUE)),
                'gps_name'         => validate_string($this->input->post('gps_name',TRUE)),
                'uid_number'       => validate_string($this->input->post('uid_number',TRUE)),
                'serial_no'        => validate_string($this->input->post('serial_no',TRUE))
        	);
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
			if($this->uri->segment(2)!='')
            {
	            $searchParams=array(
    	            'tool_number'      => $this->session->userdata('tool_number'),
    	            'tool_description' => $this->session->userdata('tool_description'),
    	            'country_id'       => $this->session->userdata('country_id'),
    	        	'asset_number'     => $this->session->userdata('asset_number'),
    	        	'gps_name'         => $this->session->userdata('gps_name'),
    	        	'uid_number'       => $this->session->userdata('uid_number'),
                    'serial_no'        => $this->session->userdata('serial_no')
	        	);
            }
            else 
            {
                $searchParams=array(
                    'tool_number'      => '',
                    'tool_description' => '',
                    'country_id'       => '',
                    'asset_number'     => '',
                    'gps_name'         => '',
                    'uid_number'       => '',
                    'serial_no'        => ''
            	);
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'gps_report_view/';
        # Total Records
        $config['total_rows'] = $this->Gps_tracking_report_m->gps_report_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['gps_results'] = $this->Gps_tracking_report_m->gps_report_results($current_offset, $config['per_page'], $searchParams,$task_access);
        # Additional data
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));

        $this->load->view('gps/gps_report_view',$data);
    }

    public function download_gps_report()
    {
    	#Page Authentication
    	$task_access = page_access_check('gps_tracking_report');

    	if($this->input->post('download_gps_tracker_report',TRUE)!='')
    	{
    		$searchParams=array(
                'tool_number'      => validate_string($this->input->post('tool_number', TRUE)),
                'tool_description' => validate_string($this->input->post('tool_description', TRUE)),
                'country_id'       => validate_number($this->input->post('country_id',TRUE)),
                'asset_number'     => validate_string($this->input->post('asset_number',TRUE)),
                'gps_name'         => validate_string($this->input->post('gps_name',TRUE)),
                'uid_number'       => validate_string($this->input->post('uid_number',TRUE)),
                'serial_no'        => validate_string($this->input->post('serial_no',TRUE))
        	);

        	$gps_list = $this->Gps_tracking_report_m->download_gps_list($searchParams,$task_access);

        	$header = '';
            $data ='';
            $titles = array('GPS Name','UID Number','Asset Number','Serial Number','Tool Description','Tool Availability','Country');
            $data = '<table border="1">';
            $data.='<thead>';
            $data.='<tr>';
            foreach ( $titles as $title)
            {
                $data.= '<th align="center">'.$title.'</th>';
            }
            $data.='</tr>';
            $data.='</thead>';
            $data.='<tbody>';
            if(count($gps_list)>0)
            {                
                foreach($gps_list as $row)
                {
                    $tool_desc = '--';
                    if($row['tool_desc']!='')
                    {
                    	$tool_desc = $row['tool_desc'];
                    }

                    $asset_number = '--';
                    if($row['asset_number']!='')
                    {
                    	$asset_number = $row['asset_number'];
                    }

                    $tool_availability = '--';
                    if($row['asset_id']!='')
                    {
                    	$tool_availability = get_asset_position($row['asset_id']);
                    }

                    $data.='<tr>';
                    $data.='<td align="center">'.$row['name'].'</td>'; 
                    $data.='<td align="center">'.$row['uid_number'].'</td>';  
                    $data.='<td align="center">'.$asset_number.'</td>'; 
                    $data.='<td align="center">'.$row['serial_number'].'</td>'; 
                    $data.='<td align="center">'.$tool_desc.'</td>';
                    $data.='<td align="center">'.$tool_availability.'</td>';
                    $data.='<td align="center">'.$row['country_name'].'</td>';
                    $data.='</tr>';
                }
            }
            else
            {
                $data.='<tr><td colspan="'.(count($titles)).'" align="center">No Records Found</td></tr>';
            }
            $data.='</tbody>';
            $data.='</table>';
            $time = date("Y-m-d H:i:s");
            $xlFile='GPS_Tracker_report_'.$time.'.xls'; 
            header("Content-type: application/x-msdownload"); 
            # replace excelfile.xls with whatever you want the filename to default to
            header("Content-Disposition: attachment; filename=".$xlFile."");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
    	}
    	else
    	{
    		redirect(SITE_URL.'gps_report_view'); exit();
    	}
    }
} ?>