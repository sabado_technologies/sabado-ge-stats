<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Repair extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Repair_m');
        $this->load->model('Wh_repair_m');
	}

	// Srilekha
	public function repair_tool()
	{
        $data['nestedView']['heading']="Repair Tools";
		$data['nestedView']['cur_page'] = 'repair';
		$data['nestedView']['parent_page'] = 'repair_tool';
        
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Repair Tools';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Repair Tools','class'=>'active','url'=>'');

        #Logic For Cart Functionality 
        if(isset($_POST['add']))
        {
           
            if(!isset($_SESSION['r_asset_id']))
            {
                $_SESSION['r_asset_id'] = array();
            }
            $a = count(@$_POST['asset_id']);
            for ($i=0; $i < $a; $i++)
            {   
                $_SESSION['r_asset_id'][@$_POST['asset_id'][$i]] = @$_POST['asset_id'][$i];
                $_SESSION['r_wh'][] = @$_POST['asset_wh'][$_POST['asset_id'][$i]];
            }
        }
       
        if(!isset($_POST['reset']) && !isset($_POST['searchtools']) && !isset($_POST['add']) && !isset($_POST['repair_tool']))
        {
            if(isset($_SESSION['r_first_url_count']))
            {
                $_SESSION['r_last_url_count'] = base_url(uri_string());
                
                if(strcmp($_SESSION['r_first_url_count'],$_SESSION['r_last_url_count']))
                {                
                }
                else
                {
                    if(isset($_SESSION['r_asset_id'])) unset($_SESSION['r_asset_id']);
                    if(isset($_SESSION['r_wh'])){ unset($_SESSION['r_wh']); }
                }
            }
            else
            {
                $_SESSION['r_first_url_count'] = base_url(uri_string());
                if(isset($_SESSION['r_asset_id'])) unset($_SESSION['r_asset_id']);
                if(isset($_SESSION['r_wh'])){ unset($_SESSION['r_wh']); }
            }
        }

        # Search Functionality
        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'crn'          => validate_string($this->input->post('crn', TRUE)),
                'tool_no'      => validate_string($this->input->post('tool_no', TRUE)),
                'tool_desc'    => validate_string($this->input->post('tool_desc',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'whr_id'       => validate_number($this->input->post('wh_id',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->input->post('reset')!='')
            {
                $searchParams=array(
                    'crn'          => '',
                    'tool_no'      => '',
                    'tool_desc'    => '',
                    'asset_number' => '',
                    'whr_id'       => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
            else if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'crn'          => $this->session->userdata('crn'),
                    'tool_no'      => $this->session->userdata('tool_no'),
                    'tool_desc'    => $this->session->userdata('tool_desc'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'whr_id'       => $this->session->userdata('whr_id'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'crn'          => '',
                    'tool_no'      => '',
                    'tool_desc'    => '',
                    'asset_number' => '',
                    'whr_id'       => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }   
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'repair_tool/';
        # Total Records

        $config['total_rows'] = $this->Repair_m->repair_total_num_rows($searchParams,$task_access); 
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));

        # Loading the data array to send to View
        $calibration_results = $this->Repair_m->repair_results($current_offset, $config['per_page'], $searchParams,$task_access);
        # Additional data
        $data['calibration_results']=$calibration_results;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['displayResults'] = 1;
        $data['current_offset'] = $current_offset;

        $this->load->view('repair/repair_tool_list',$data);
	}

	// Srilekha
	public function add_repair_supplier()
	{
		# Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Add Repair Supplier Details";
        $data['nestedView']['cur_page'] = 'check_repair';
        $data['nestedView']['parent_page'] = 'repair_tool';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Add Repair Supplier Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Repair Tools','class'=>'','url'=>SITE_URL.'repair_tool');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add Repair Supplier Details','class'=>'active','url'=>'');


        if(isset($_SESSION['r_asset_id']))
        {
            $asset_data=array();
            foreach ($_SESSION['r_asset_id'] as $rc_asset_id => $qty)
            {
                $result_data = $this->Repair_m->get_rc_details($rc_asset_id);
                $rc_asset_id = $rc_asset_id;
                $asset_data[] = $result_data; 
            } 
            $data['assets']=$asset_data;
        }

        $country_id = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'country_id');
        $data['repair_supplier']=$this->Common_model->get_data('supplier',array('calibration'=>1,'status'=>1,'country_id'=>$country_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        $data['form_action'] = SITE_URL.'insert_repair_supplier';

        $this->load->view('repair/add_supplier_view',$data);
	}

    public function removerepairToolFromCart()
    {
        #page authentication
        $task_access = page_access_check('repair_tool');

        $rc_asset_id=validate_number($this->input->post('rc_asset_id',TRUE));
        if(isset($_SESSION['r_asset_id'][$rc_asset_id]))
        {
            unset($_SESSION['r_asset_id'][$rc_asset_id]);
            echo 1;
        }
    }

	// Srilekha
	public function insert_repair_supplier()
	{
		#page authentication
        $task_access = page_access_check('repair_tool');
        $rc_asset_id_arr=$this->input->post('rc_asset_id',TRUE);
        $warehouse_id=$this->input->post('wh_id',TRUE);
        $wh_id = $warehouse_id[0];
        $count = 0;
        foreach ($warehouse_id as $row) 
        {
            if($wh_id !== $row)
            {
                $count++;
            }
        }
        if($count>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> All selected Assets Must be in Warehouse</div>'); 
            redirect(SITE_URL.'repair_tool'); exit; 
        }
        else
        {   
            foreach ($rc_asset_id_arr as $rc_asset_id) 
            {
               $asset_country=$rc_asset_id;
            }
            $country_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$asset_country),'country_id');

            // Dynamic CR number Generation
            $number=get_repair_rcb_number($country_id);
            if(count($number) == 0)
            {
                $f_order_number = "1-00001";
            }
            else
            {
                $num = ltrim($number['rcb_number'],"RRB");
                $result = explode("-", $num);
                $running_no = (int)$result[1];
                if($running_no == 99999)
                {
                    $first_num = $result[0]+1;
                    $second_num = get_running_sno_five_digit(1);  
                }
                else
                {
                    $first_num = $result[0];
                    $val = $running_no+1;
                    $second_num = get_running_sno_five_digit($val);
                }
                $f_order_number = $first_num.'-'.$second_num;
            }
            $order_number = 'RRB'.$f_order_number;
            $expected_delivery_date=validate_string(date('Y-m-d',strtotime($this->input->post('delivery_date',TRUE))));
            $expected_return_date=validate_string(date('Y-m-d',strtotime($this->input->post('return_date',TRUE))));

            $data = array(
            'supplier_id'            => validate_number($this->input->post('supplier_id',TRUE)),
            'rcb_number'             => $order_number,
            'contact_person'         => validate_string($this->input->post('contact_person',TRUE)),
            'wh_id'                  => $wh_id,
            'rma_number'             => validate_string($this->input->post('rma_number',TRUE)),
            'phone_number'           => validate_string($this->input->post('phone_number',TRUE)),
            'gst_number'             => validate_string($this->input->post('gst_number',TRUE)),
            'pan_number'             => validate_string($this->input->post('pan_number',TRUE)),
            'pin_code'               => validate_string($this->input->post('zip_code',TRUE)),
            'expected_delivery_date' => $expected_delivery_date,
            'expected_return_date'   => $expected_return_date,
            'rc_type'                => 1,
            'address1'               => validate_string($this->input->post('address_1',TRUE)),
            'address2'               => validate_string($this->input->post('address_2',TRUE)),
            'address3'               => validate_string($this->input->post('address_3',TRUE)),
            'address4'               => validate_string($this->input->post('address_4',TRUE)),
            'created_by'             => $this->session->userdata('sso_id'),
            'created_time'           => date('Y-m-d H:i:s'),
            'remarks'                => validate_string($this->input->post('remarks',TRUE)),
            'status'                 => 1,
            'country_id'             => $country_id
                        );
            $this->db->trans_begin();
            // Update Calibration data
            $rc_order_id=$this->Common_model->insert_data('rc_order',$data);

            // Document Type Retrieving
            $document_type = $this->input->post('document_type',TRUE);
            if($document_type[1]!='')
            {
                foreach ($document_type as $key => $value) 
                {
                    if($_FILES['support_document_'.$key]['name']!='')
                    {
                        $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                        $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                        $config['upload_path'] = asset_document_path();
                        $config['allowed_types'] = 'gif|jpg|png|pdf';
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        $this->upload->do_upload('support_document_'.$key);
                        $fileData = $this->upload->data();
                        $support_document = $config['file_name'];
                        $original_name    = $config['org_name'];
                    }
                    else
                    {
                        $support_document = '';
                    }
                    $insert_doc = array(
                    'document_type_id' => $value,
                    'rc_order_id'      => $rc_order_id,
                    'name'             => $support_document,
                    'doc_name'         => $original_name,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                    );

                    if($value!='' && $support_document != '')
                    {
                        // Insert data into calibration_doc
                        $this->Common_model->insert_data('rc_doc',$insert_doc);
                    }
                }
            }
            
            foreach($rc_asset_id_arr as $rc_asset_id)
            {
                $asset_id = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'asset_id');
                
                //update_rc_status_history
                $update_rcsh = array(
                    'modified_by'   => $this->session->userdata('sso_id'),
                    'modified_time' => date('Y-m-d H:i:s')
                );
                $update_rcsh_where = array('rc_asset_id'=>$rc_asset_id,'modified_time'=>NULL);
                $this->Common_model->update_data('rc_status_history',$update_rcsh,$update_rcsh_where);

                //insert_rc_status_history
                $insert_rcsh = array(
                    'rc_asset_id'      => $rc_asset_id,
                    'current_stage_id' => 21,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
                $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

                //update_rc_asset
                $this->Common_model->update_data('rc_asset',array('current_stage_id'=>21,'rc_order_id'=>$rc_order_id),array('rc_asset_id'=>$rc_asset_id));

                // Update Asset_status_history
                $ash_id=$this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
                $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));

                //insert asset_status_history
                $ash_data=array(
                'status'           => 10,
                'asset_id'         => $asset_id,
                'rc_asset_id'      => $rc_asset_id,
                'current_stage_id' => 21,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                );
                // Insert data into asset_status_history
                $this->Common_model->insert_data('asset_status_history',$ash_data);

                #audit_data
                $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                $old_data = array('asset_status_id'=>$asset_arr['status']);
                $new_data = array('asset_status_id'=>10);
                $remarks_arr = "Asset linked for Repair RRB:".$order_number;
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks_arr,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'repair_tool'); exit; 
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Supplier Details  has been Captured successfully With RRB Number : <strong>'.$order_number.'</strong> !
                </div>');
                redirect(SITE_URL.'repair_tool'); exit;
            }
        }
	}

    public function delete_repair_cr_request()
    {
        #page Authentication
        $task_access = page_access_check('repair_tool');

        $rc_asset_id = storm_decode($this->uri->segment(2));
        if($rc_asset_id == '')
        {
            redirect(SITE_URL.'repair_tool'); exit();
        }

        $this->db->trans_begin();

        $rc_row = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));
        $asset_id = $rc_row['asset_id'];
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

        #update rc asset //cancelled
        $update_rca = array(
            'status'           => 10,
            'current_stage_id' => 29,
            'modified_by'      => $this->session->userdata('sso_id'),
            'modified_time'    => date('Y-m-d H:i:s')
        );
        $update_rca_where = array('rc_asset_id'=>$rc_asset_id);
        $this->Common_model->update_data('rc_asset',$update_rca,$update_rca_where);

        if($asset_arr['status'] == 10)
        {
            #audit data
            $old_data = array('asset_status_id'=>$asset_arr['status']);

            $update_a = array(
                'status'              => 1,
                'approval_status'     => 0,
                'availability_status' => 1,
                'modified_by'         => $this->session->userdata('sso_id'),
                'modified_time'       => date('Y-m-d H:i:s')
            );
            $update_a_where = array('asset_id'=>$asset_id);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            #audit data
            $new_data = array('asset_status_id' => 1);
            $remarks = "Repair:".$rc_row['rc_number']." req is cancelled";
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);
        }

        #Update Defective Asset
        if($rc_row['defective_asset_id']!='')
        {
            $this->Common_model->update_data('defective_asset',array('status'=>10),array('defective_asset_id'=>$rc_row['defective_asset_id']));
        }
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'repair_tool'); exit; 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> CR Number :<strong>'.$rc_row['rc_number'].'</strong> has been Deleted Successfully ! </div>');
            redirect(SITE_URL.'repair_tool'); exit;
        }        
    }

    // Srilekha
    public function open_repair_request()
    {
        $data['nestedView']['heading']="Open Repair Requests";
        $data['nestedView']['cur_page'] = 'open_repair';
        $data['nestedView']['parent_page'] = 'open_repair_request';
        
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Open Repair Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Repair Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'   => validate_string($this->input->post('rcb_number', TRUE)),
                'whr_id'       => validate_string($this->input->post('wh_id',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whr_id'       => $this->session->userdata('whr_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else
            {
                $searchParams=array(
                    'rcb_number'   => '',
                    'whr_id'       => '',
                    'asset_number' => '',
                    'rc_number'    => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }   
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'open_repair_request/';
        # Total Records

        $config['total_rows'] = $this->Repair_m->open_repair_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Repair_m->open_repair_results($current_offset, $config['per_page'], $searchParams,$task_access);

        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Repair_m->get_asset_details($value['rc_order_id']);
            $calibration_results[$key]['asset_list']=$part_list;
        }
        
        # Additional data
        $data['calibration_results']=$calibration_results;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['displayResults'] = 1;
        $data['flg']=1;
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;
        
        $this->load->view('repair/open_repair_tool_list',$data);
    }
    // Srilekha
    public function edit_open_repair_request()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'open_repair_request'); exit;
        }
       
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Update Repair Requests";
        $data['nestedView']['cur_page'] = 'check_repair';
        $data['nestedView']['parent_page'] = 'open_repair_request';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Repair Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Repair Requests','class'=>'','url'=>SITE_URL.'open_repair_request');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Repair Details','class'=>'active','url'=>'');


        $data['cal_row']=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $country_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'country_id');
        $data['repair_supplier']=$this->Common_model->get_data('supplier',array('status'=>1,'country_id'=>$country_id));
        $data['asset_details']=$this->Repair_m->get_asset_details($rc_order_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        
        $attach_document = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['rc_order_id']=$rc_order_id;
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document; 
        $data['flg'] = 2;
        
        $data['form_action'] = SITE_URL.'update_repair_request';
        $this->load->view('repair/open_repair_tool_list',$data);
    }

    // Srilekha
    public function update_repair_request()
    {
        #page Authentication
        $task_access = page_access_check('open_repair_request');

        $rc_order_id=validate_number($this->input->post('rc_order_id',TRUE));
        $rcb_number=$this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $expected_delivery_date=validate_string(date('Y-m-d',strtotime($this->input->post('delivery_date',TRUE))));
        $expected_return_date=validate_string(date('Y-m-d',strtotime($this->input->post('return_date',TRUE))));

        $data=array(
        'supplier_id'            => validate_number($this->input->post('supplier_id',TRUE)),
        'contact_person'         => validate_string($this->input->post('contact_person',TRUE)),
        'rma_number'             => validate_string($this->input->post('rma_number',TRUE)),
        'phone_number'           => validate_string($this->input->post('phone_number',TRUE)),
        'gst_number'             => validate_string($this->input->post('gst_number',TRUE)),
        'pan_number'             => validate_string($this->input->post('pan_number',TRUE)),
        'pin_code'               => validate_string($this->input->post('zip_code',TRUE)),
        'expected_delivery_date' => $expected_delivery_date,
        'expected_return_date'   => $expected_return_date,
        'rc_type'                => 1,
        'address1'               => validate_string($this->input->post('address_1',TRUE)),
        'address2'               => validate_string($this->input->post('address_2',TRUE)),
        'address3'               => validate_string($this->input->post('address_3',TRUE)),
        'address4'               => validate_string($this->input->post('address_4',TRUE)),
        'modified_by'            => $this->session->userdata('sso_id'),
        'modified_time'          => date('Y-m-d H:i:s'),
        'remarks'                => validate_string($this->input->post('remarks',TRUE)),
        'status'                 => 1);
        $this->db->trans_begin();

        // Update Calibration data
        $this->Common_model->update_data('rc_order',$data,array('rc_order_id'=>$rc_order_id));
        // Document Type Retrieving
        $document_type = $this->input->post('document_type',TRUE);
        foreach ($document_type as $key => $value) 
        {
            if($_FILES['support_document_'.$key]['name']!='')
            {
                $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                $config['upload_path'] = asset_document_path();
                $config['allowed_types'] = 'gif|jpg|png|pdf';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('support_document_'.$key);
                $fileData = $this->upload->data();
                $support_document = $config['file_name'];
                $original_name    = $config['org_name'];
            }
            else
            {
                $support_document = '';
            }
            $insert_doc = array(
                'document_type_id' => $value,
                'rc_order_id'      => $rc_order_id,
                'name'             => $support_document,
                'doc_name'         => $original_name,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1
            );

            if($value!='' && $support_document != '')
            {
                // Insert data into calibration_doc
                $this->Common_model->insert_data('rc_doc',$insert_doc);
            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'open_repair_request');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Supplier Details Has been updated successfully for RR Number : <strong>'.$rcb_number.'</strong> !
            </div>');
            redirect(SITE_URL.'open_repair_request');
        }
    }

    // Srilekha
    public function edit_admin_repair_request()
    {
        $rc_asset_id=@storm_decode($this->uri->segment(2));
        $rc_order_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_order_id');
        if($rc_asset_id == '' )
        {
            redirect(SITE_URL.'open_repair_request'); exit;
        }

         # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Update Repair Details";
        $data['nestedView']['cur_page'] = 'check_repair';
        $data['nestedView']['parent_page'] = 'open_repair_request';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';
        

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Repair Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Repair Requests','class'=>'','url'=>SITE_URL.'open_repair_request');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Repair Details','class'=>'active','url'=>'');

       
        $data['cal_row']=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>23,'status'=>1),'rcsh_id');
        $rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id),'rcah_id');
        $asset_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'asset_id');
        $asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
        $selected_asset_health=array();
        foreach($asset_health as $row)
        {
            $selected_asset_health[$row['part_id']]['asset_condition_id']=$row['asset_condition_id'];
            $selected_asset_health[$row['part_id']]['remarks']=$row['remarks'];
        }
        $data['selected_asset_health']=$selected_asset_health;
        $asset_details=$this->Repair_m->get_asset_details_row($rc_order_id,$asset_id);
        $part_list=$this->Repair_m->get_asset_part_details($asset_details['asset_id']);
        $asset_details['asset_health_list']=$part_list;
        $data['asset_details']=$asset_details;
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id,'status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('rca_doc',array('rc_asset_id'=>$rc_asset_id,'status'=>1));
        $data['rc_order_id']=$rc_order_id;
        $data['rc_asset_id']=$rc_asset_id;
        $data['flg']=2;
        $data['form_action'] = SITE_URL.'update_admin_repair_request';
        $this->load->view('repair/edit_repair_request',$data);
    }

    //
    public function update_admin_repair_request()
    {
        $rc_order_id=validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        $rc_asset_id=validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        if($rc_order_id=='' || $rc_asset_id=='')
        {
            redirect(SITE_URL.'open_repair_request'); exit();
        }

        $status=$this->input->post('status',TRUE);
        // Fetching CR number

        $rc_arr = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));
        $rc_number = $rc_arr['rc_number'];
        // Fetching cal_rc_asset_id for CR number
        $cal_rc_asset_id = $rc_arr['cal_rc_asset_id'];

        // Fetching Asset_id for cal_rc_asset_id
        $asset_id = $rc_arr['asset_id'];
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

        $this->db->trans_begin();
        $document_type = $this->input->post('document_type',TRUE);
        if($document_type[1]!='')
        {
            foreach ($document_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['org_name']  = $_FILES['support_document_'.$key]['name'];
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $original_name    = $config['org_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'rc_asset_id'      => $rc_asset_id,
                    'name'             => $support_document,
                    'doc_name'         => $original_name,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );

                if($value!='' && $support_document != '')
                {
                    // Insert data into calibration_doc
                    $this->Common_model->insert_data('rca_doc',$insert_doc);
                }
            }
        }
        
        // If Asset is Repaired
        if($status == 1)
        {
            $update_rca = array(
                'current_stage_id'=> 24,
                'modified_by'     => $this->session->userdata('sso_id'),
                'modified_time'   => date('Y-m-d H:i:s'),
                'status'          => 10
            );
            $update_rca_where = array('rc_asset_id'=>$rc_asset_id);
            $this->Common_model->update_data('rc_asset',$update_rca,$update_rca_where);

            $update_rcsh = array(
                'modified_time' => date('Y-m-d H:i:s'),
                'modified_by'   => $this->session->userdata('sso_id')
            );
            $update_rcsh_where = array('rc_asset_id'=>$rc_asset_id,'modified_time'=>NULL);
            $this->Common_model->update_data('rc_status_history',$update_rcsh,$update_rcsh_where);

            $insert_rcsh = array(
                'rc_asset_id'      => $rc_asset_id,
                'current_stage_id' => 24,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 10
            );
            $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

            if($cal_rc_asset_id != '')
            {
                $update_cal_rca = array('status' => 1);
                $update_cal_rca_where = array('rc_asset_id' =>$cal_rc_asset_id);
                $this->Common_model->update_data('rc_asset',$update_cal_rca,$update_cal_rca_where);

                #audit data
                $old_data = array(
                    'asset_status_id'     => $asset_arr['status'],
                    'availability_status' => $asset_arr['availability_status']
                );

                $update_a = array(
                    'status'              => 4,
                    'availability_status' => 1,
                    'approval_status'     => 0,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                #audit data
                $new_data = array(
                    'asset_status_id'     => 4,
                    'availability_status' => 1
                );
                $remarks = " RR No:".$rc_arr['rc_number']." is Repaired and waiting for Calibration Approval";
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                $update_ah = array(
                    'end_time'    => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('sso_id'),
                    'end_time'    => date('Y-m-d H:i:s')
                );
                $update_ah_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$update_ah,$update_ah_where);

                // Fetching Current stage for cal_rc_asset_id
                $current_stage_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$cal_rc_asset_id),'current_stage_id');

                $insert_ah = array(
                    'asset_id'         => $asset_id,
                    'current_stage_id' => $current_stage_id,
                    'rc_asset_id'      => $cal_rc_asset_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 4
                );
                $this->Common_model->insert_data('asset_status_history',$insert_ah);
            }
            else
            {
                #audit data
                $old_data = array(
                    'asset_status_id'     => $asset_arr['status'],
                    'availability_status' => $asset_arr['availability_status']
                );
                $update_a = array(
                    'status'              => 1,
                    'availability_status' => 1,
                    'approval_status'     => 0,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                #audit data
                $new_data = array(
                    'asset_status_id'     => 1,
                    'availability_status' => 1
                );
                $remarks = " RR No:".$rc_arr['rc_number']." is Repaired";
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                $update_ah = array(
                    'end_time' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('sso_id')
                );
                $update_ah_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$update_ah,$update_ah_where);

                $insert_ah = array(
                    'asset_id'         => $asset_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
                $this->Common_model->insert_data('asset_status_history',$insert_ah);
            }

            // Update Rc Order if all assets are repaired
            $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
            $check_stat = 0;
            foreach ($rc_asset_data as $key => $value) 
            {
                if($value['status']!=10)
                {
                    $check_stat++;
                }
            }
            if($check_stat == 0)
            {
                $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id));
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'open_repair_request');  exit(); 
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> RR Number : <strong>'.$rc_number.'</strong> Has been closed successfully with Repair Status : <strong>Repaired</strong></div>');
                redirect(SITE_URL.'open_repair_request'); exit();
            }
        }
        // If not Repaired
        if($status == 2)
        {
            $reason=validate_number($this->input->post('reason',TRUE));
            // Sent for Scrap
            if($reason == 1)
            {
                #audit data
                $old_data = array(
                    'asset_status_id'     => $asset_arr['status'],
                    'availability_status' => $asset_arr['availability_status']
                );

                $update_a = array(
                    'status'              => 11,
                    'availability_status' => 2,
                    'approval_status'     => 0,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                #audit data
                $new_data = array(
                    'asset_status_id'     => 11,
                    'availability_status' => 2
                );
                $remarks = "RR No:".$rc_arr['rc_number']." changed to Scraped";
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                $update_ah = array(
                    'end_time'    => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('sso_id')
                );
                $update_ah_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$update_ah,$update_ah_where);

                $insert_ah = array(
                    'asset_id'         => $asset_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 11
                );
                $this->Common_model->insert_data('asset_status_history',$insert_ah);

                $repair_cs_id = 25;
                $calibration_cs_id = 18;
            }
            //out of tolerance 
            else if($reason == 2)
            {
                #audit data
                $old_data = array(
                    'asset_status_id'     => $asset_arr['status'],
                    'availability_status' => $asset_arr['availability_status']
                );

                $update_a = array(
                    'status'              => 9,
                    'availability_status' => 2,
                    'approval_status'     => 0,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s')
                );
                $update_a_where = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_a,$update_a_where);

                #audit data
                $new_data = array(
                    'asset_status_id'     => 10,
                    'availability_status' => 2
                );
                $remarks = "RR No:".$rc_arr['rc_number']." Changed to Out of Tolerance";
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

                $update_ah = array(
                    'end_time' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('sso_id')
                );
                $update_ah_where = array('asset_id'=>$asset_id,'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$update_ah,$update_ah_where);

                $insert_ah = array(
                    'asset_id'         => $asset_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 9
                );
                $this->Common_model->insert_data('asset_status_history',$insert_ah);

                $repair_cs_id = 26;
                $calibration_cs_id = 27;
            }

            #update repair rc_asset
            $update_rca = array(
                'current_stage_id' => $repair_cs_id,
                'modified_by'      => $this->session->userdata('sso_id'),
                'modified_time'    => date('Y-m-d H:i:s'),
                'status'           => 10
            );
            $update_rca_where = array('rc_asset_id'=>$rc_asset_id);
            $this->Common_model->update_data('rc_asset',$update_rca,$update_rca_where);

            $update_rcsh = array(
                'modified_time' => date('Y-m-d H:i:s'),
                'modified_by'   => $this->session->userdata('sso_id')
            );
            $update_rcsh_where = array('rc_asset_id'=>$rc_asset_id,'modified_time'=>NULL);
            $this->Common_model->update_data('rc_status_history',$update_rcsh,$update_rcsh_where);

            $insert_rcsh = array(
                'rc_asset_id'      => $rc_asset_id,
                'current_stage_id' => $repair_cs_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 10
            );
            $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

            if($cal_rc_asset_id != '')
            {
                #update calibration rc_asset
                $update_crca = array(
                    'current_stage_id' => $calibration_cs_id,
                    'modified_by'       => $this->session->userdata('sso_id'),
                    'modified_time'     => date('Y-m-d H:i:s'),
                    'status'            => 10
                );
                $update_crca_where = array('rc_asset_id'=>$cal_rc_asset_id);
                $this->Common_model->update_data('rc_asset',$update_crca,$update_crca_where);

                $update_crcsh = array(
                    'modified_time' => date('Y-m-d H:i:s'),
                    'modified_by'    => $this->session->userdata('sso_id')
                );
                $update_crcsh_where = array('rc_asset_id'=>$cal_rc_asset_id,'modified_time'=>NULL);
                $this->Common_model->update_data('rc_status_history',$update_crcsh,$update_crcsh_where);

                $insert_crcsh = array(
                    'rc_asset_id'      => $cal_rc_asset_id,
                    'current_stage_id' => $calibration_cs_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 10
                );
                $this->Common_model->insert_data('rc_status_history',$insert_crcsh);

                // Update Rc Order if all assets are Calibrated
                $cal_rc_order_id = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$cal_rc_asset_id),'rc_order_id');
                $rc_asset_data1=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$cal_rc_order_id));
                $check_stat1 = 0;
                foreach ($rc_asset_data1 as $key => $value) 
                {
                    if($value['status']!=10)
                    {
                        $check_stat1++;
                    }
                }
                if($check_stat1 == 0)
                {
                    $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$cal_rc_order_id));
                }
            }

            // Update Rc Order if all assets are repaired
            $rc_asset_data=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
            $check_stat = 0;
            foreach ($rc_asset_data as $key => $value) 
            {
                if($value['status']!=10)
                {
                    $check_stat++;
                }
            }
            if($check_stat == 0)
            {
                $this->Common_model->update_data('rc_order',array('status'=>10),array('rc_order_id'=>$rc_order_id));
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'open_repair_request');  exit(); 
            }
            else
            {
                $this->db->trans_commit();
                if($reason == 1)
                {
                    $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> RR Number : <strong>'.$rc_number.'</strong> Has been closed successfully with Asset Status : <strong>Scraped</strong></div>');
                    redirect(SITE_URL.'open_repair_request'); exit();
                }
                else if ($reason == 2) 
                {
                    $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> RR Number : <strong>'.$rc_number.'</strong> Has been closed successfully with Asset Status : <strong>Out Of Tolerance</strong></div>');
                    redirect(SITE_URL.'open_repair_request'); exit();
                }
            }
        }
    }

    // Srilekha
    public function deactivate_repair_doc()
    {
        $repair_doc_id = validate_number($this->input->post('doc_id',TRUE));
        $update_data = array('status'=>2);
        $update_where = array('rcd_id'=>$repair_doc_id);
        $res = $this->Common_model->update_data('rc_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    // Srilekha
    public function activate_repair_doc()
    {
        $repair_doc_id = validate_number($this->input->post('doc_id',TRUE));
        $update_data = array('status'=>1);
        $update_where = array('rcd_id'=>$repair_doc_id);
        $res = $this->Common_model->update_data('rc_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }
    
    // Srilekha
    public function view_open_repair_request()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id =='')
        {
            redirect(SITE_URL.'open_repair_request'); exit;
        }

        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View Open Repair Request Details";
        $data['nestedView']['cur_page'] = 'open_repair';
        $data['nestedView']['parent_page'] = 'open_repair_request';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View Open Repair Request Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Repair Requests','class'=>'','url'=>SITE_URL.'open_repair_request');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View Open Repair Request Details','class'=>'active','url'=>'');

        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Repair_m->get_asset_details($rc_order_id);
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['rcb_number'] = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $data['attach_document'] = $this->Repair_m->get_documents_by_stage($rc_order_id);
        $data['cal_row']=$cal_row;

        $this->load->view('repair/open_repair_request_view',$data);
    }

    public function remove_repair_asset()
    {
        $rc_asset_id=validate_number($this->input->post('rc_asset_id',TRUE));
        $res=$this->Common_model->update_data('rc_asset',array('current_stage_id'=>20,'rc_order_id'=>NULL),array('rc_asset_id'=>$rc_asset_id));

        $this->Common_model->delete_data('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>21));

        $update_rc_status_history = array('modified_time'=>NULL);
        $update_rcsh_where = array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>20);
        $this->Common_model->update_data('rc_status_history',$update_rc_status_history,$update_rcsh_where);

        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function remove_repair_all_assets()
    {
        #page Authentication
        $page_access = page_access_check('open_repair_request');

        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'open_repair_request'); exit;
        }
        $rc_asset_id=$this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));
        $rcb_number=$this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $this->db->trans_begin();
        foreach($rc_asset_id as $row)
        {
            $this->Common_model->update_data('rc_asset',array('current_stage_id'=>20,'rc_order_id'=>NULL),array('rc_asset_id'=>$row['rc_asset_id'],'current_stage_id'=>21));

            $this->Common_model->delete_data('rc_status_history',array('rc_asset_id'=>$row['rc_asset_id'],'current_stage_id'=>21));

            $update_rc_status_history = array('modified_time'=>NULL);
            $update_rcsh_where = array('rc_asset_id'=>$row['rc_asset_id'],'current_stage_id'=>20);
            $this->Common_model->update_data('rc_status_history',$update_rc_status_history,$update_rcsh_where);
        }
        
        $this->Common_model->update_data('rc_order',array('status'=>4),array('rc_order_id'=>$rc_order_id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'open_repair_request');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> RRB Number :<strong>'.$rcb_number.'</strong> has been Cancelled Successfully !
            </div>');
            redirect(SITE_URL.'open_repair_request');
        }
        
    }
    
    // Srilekha
    public function vendor_wh_repair_request()
    {
        $data['nestedView']['heading']="Acknowledge Repair Returned Tools";
        $data['nestedView']['cur_page'] = 'vendor_repair';
        $data['nestedView']['parent_page'] = 'vendor_wh_repair_request';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Acknowledge Repair Returned Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Repair Returned Tools','class'=>'active','url'=>'');

        # Search Functionality

        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'   => validate_string($this->input->post('rcb_number', TRUE)),
                'whr_id'       => validate_string($this->input->post('wh_id',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whr_id'       => $this->session->userdata('whr_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'rcb_number'   => '',
                    'whr_id'       => '',
                    'asset_number' => '',
                    'rc_number'    => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            } 
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'vendor_wh_repair_request/';
        # Total Records

        $config['total_rows'] = $this->Repair_m->wh_vendor_repair_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Repair_m->wh_vendor_repair_results($current_offset, $config['per_page'], $searchParams,$task_access);

        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Repair_m->get_vendor_assets($value['rc_order_id']);

            $calibration_results[$key]['asset_list']=$part_list;
        }

        # Additional data
        $data['calibration_results']=$calibration_results;

        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['displayResults'] = 1;
        $data['flg']=1;
        $data['current_offset'] = $current_offset;

        $this->load->view('repair/vendor_wh_repair_tool_list',$data);        
    }

    // Srilekha
    public function edit_vendor_wh_repair_request()
    {
        $rc_asset_id=@storm_decode($this->uri->segment(2));
        if($rc_asset_id == '')
        {
            redirect(SITE_URL.'vendor_wh_repair_request'); exit();
        }

        #page authentication
        $task_access = page_access_check('vendor_wh_repair_request');
        if($task_access == 1)
        {
            $warehouse_id = $this->session->userdata('swh_id');
        }
        elseif($task_access == 2 || $task_access == 3)
        {
            if($this->input->post('SubmitWarehouse')!='')
            {
                $warehouse_id = $this->input->post('warehouse_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']= "Select Warehouse";
                $data['nestedView']['cur_page'] = 'check_repair';
                $data['nestedView']['parent_page'] = 'vendor_wh_repair_request';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Warehouse';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Repair Returned Tools','class'=>'','url'=>SITE_URL.'vendor_wh_repair_request');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Warehouse','class'=>'active','url'=>'');

                $country_id = $this->Repair_m->get_country_from_rc_asset($rc_asset_id);
                $data['warehouse_list']=$this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));

                $data['form_action'] = SITE_URL.'edit_vendor_wh_repair_request/'.storm_encode($rc_asset_id);
                $data['task_access'] = $task_access;
                $data['cancel'] = SITE_URL.'vendor_wh_repair_request';
                $this->load->view('calibration/intermediate_page',$data); 
            }
        }

        if(@$warehouse_id!='')
        {  
            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Acknowledge Returned Tool";
            $data['nestedView']['cur_page'] = 'check_repair';
            $data['nestedView']['parent_page'] = 'vendor_wh_repair_request';

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Acknowledge Returned Tool';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Repair Returned Tools','class'=>'','url'=>SITE_URL.'vendor_wh_repair_request');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Returned Tool','class'=>'active','url'=>'');


            $asset_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'asset_id');
            $data['rc_number'] = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_number');
            $rc_order_id=$this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_order_id');
            $order_details=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
            $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$order_details['supplier_id']));
            $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$order_details['wh_id']));
            $data['order_details']=$order_details;
            $data['rc_order_id']=$rc_order_id;
            $data['rc_asset_id']=$rc_asset_id;
            $data['asset_list']=$this->Repair_m->get_vendor_assets_row($rc_order_id,$asset_id);
            $data['flg']=2;
            $data['add_wh_id'] = $warehouse_id;
            $add_wh_list = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$warehouse_id));
            $data['wh_name'] = $add_wh_list['wh_code'].' -('.$add_wh_list['name'].')';

            $this->load->view('repair/vendor_wh_repair_tool_list',$data);
        }
    }

    public function vendor_repair_asset_details()
    {
        #page Autentication
        $task_access = page_access_check('vendor_wh_repair_request');

        $rc_order_id=validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        $rc_asset_id=validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        $add_wh_id = validate_number(storm_decode($this->input->post('add_wh_id',TRUE)));
        if($rc_order_id =='' || $rc_asset_id == '' || $add_wh_id == '')
        {
            redirect(SITE_URL.'vendor_wh_repair_request'); exit();
        }
        
        $asset_number = validate_string($this->input->post('asset_number',TRUE));
        $asset_number = trim(@$asset_number);
        $check_asset=$this->Repair_m->check_asset_number($rc_asset_id,$asset_number);
        // If the Scanned Asset Number is invalid
        if($check_asset['asset_id'] =='')
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Invalid Asset Number : <strong> '.$asset_number.' </strong>!</div>'); 
            redirect(SITE_URL.'edit_vendor_wh_repair_request/'.storm_encode($rc_asset_id)); 
            exit();
        }
        else
        {
            $order_details=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
            $data['nestedView']['heading']="Acknowledge Repaired Tool";
            $data['nestedView']['cur_page'] = 'check_repair';
            $data['nestedView']['parent_page'] = 'vendor_wh_repair_request';

            # include files
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/health_calibration.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = "Acknowledge Repaired Tool : ".$asset_number."";
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Repair Returned Tools','class'=>'','url'=>SITE_URL.'vendor_wh_repair_request');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Acknowledge Repaired Tool','class'=>'active','url'=>'');

            #additional data
            $asset_details = $this->Repair_m->get_asset_part_details($check_asset['asset_id']);

            
            $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$order_details['wh_id']));
            $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$order_details['supplier_id']));
            $data['rc_number'] = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_number');
            $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>22),'rcsh_id');
            $rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id,'end_time'=>NULL),'rcah_id');
           
            $asset_health=$this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id));
            
            $selected_asset_health=array();
            foreach($asset_health as $row)
            {
                $selected_asset_health[$row['part_id']]['asset_condition_id']=$row['asset_condition_id'];
                $selected_asset_health[$row['part_id']]['remarks']=$row['remarks'];
            }

          
            $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
            $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
            $data['selected_asset_health']=$selected_asset_health;
            $data['asset_details'] = $asset_details;
            $data['order_details'] = $order_details;
            $data['rc_order_id']=$rc_order_id;
            $data['rc_asset_id']=$rc_asset_id;
            $data['add_wh_id'] = $add_wh_id;
            $add_wh_list = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$add_wh_id));
            $data['wh_name'] = $add_wh_list['wh_code'].' -('.$add_wh_list['name'].')';

            $data['form_action'] = SITE_URL.'update_vendor_wh_repair_request';


            $this->load->view('repair/acknowledge_tools',$data);
        }
    }

    // Srilekha
    public function update_vendor_wh_repair_request()
    {
        #page Autentication
        $task_access = page_access_check('vendor_wh_repair_request');

        $rc_order_id=validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        $rc_asset_id=validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        $add_wh_id  =validate_number(storm_decode($this->input->post('add_wh_id',TRUE)));
        if($rc_order_id=='' || $rc_asset_id=='')
        {
            redirect(SITE_URL.'vendor_wh_repair_request'); exit();
        }
        
        $rcsh_id=$this->Common_model->get_value('rc_status_history',array('rc_asset_id'=>$rc_asset_id,'current_stage_id'=>22),'rcsh_id');
        $rcah_id=$this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rc_asset_id,'rcsh_id'=>$rcsh_id,'end_time'=>NULL),'rcah_id');

        $rc_arr = $this->Common_model->get_data_row('rc_asset',array('rc_asset_id'=>$rc_asset_id));
        $asset_id=$rc_arr['asset_id'];
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $asset_number=$asset_arr['asset_number'];
        $this->db->trans_begin();
        $doc_type = $this->input->post('document_type',TRUE);
        if($doc_type[1]!='')
        {
            foreach ($doc_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    @$config['org_name']  = $_FILES['support_document_'.$key]['name'];
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $original_name=@$config['org_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array('document_type_id' => $value,
                                    'name'             => $support_document,
                                    'rc_asset_id'      => $rc_asset_id,
                                    'doc_name'         => @$original_name,
                                    'created_by'       => $this->session->userdata('sso_id'),
                                    'created_time'     => date('Y-m-d H:i:s'),
                                    'status'           => 1);

                if($value!='' && $support_document != '')
                {
                    $this->Common_model->insert_data('rca_doc',$insert_doc);
                }
            }
        }

        $this->Common_model->update_data('rc_status_history',array('modified_time'=>date('Y-m-d H:i:s'),'modified_by'=>$this->session->userdata('sso_id')),array('rcsh_id'=>$rcsh_id));

        $rcsh_data=array(
                        'rc_asset_id'       =>  $rc_asset_id,
                        'current_stage_id'  =>  23,
                        'created_by'        =>  $this->session->userdata('sso_id'),
                        'created_time'      =>  date('Y-m-d H:i:s'),
                        'status'            =>  1
                        );
        $new_rcsh_id=$this->Common_model->insert_data('rc_status_history',$rcsh_data);
        $rcah_data=array(
                    'rc_asset_id'       =>  $rc_asset_id,
                    'rcsh_id'           =>  $new_rcsh_id,
                    'created_by'        =>  $this->session->userdata('sso_id'),
                    'created_time'      =>  date('Y-m-d H:i:s'),
                    'status'            =>  1,
                   );
        $new_rcah_id=$this->Common_model->insert_data('rc_asset_history',$rcah_data);
        $asset_condition_arr = $this->input->post('asset_condition_id',TRUE);
        $remarks_arr =  $this->input->post('remarks',TRUE);
        $asset_details=$this->Repair_m->get_asset_part_details($asset_id);
        foreach ($asset_details as $key => $value)
        {
            $remarks = $remarks_arr[$value['part_id']];
            if($remarks == '') $remarks = NULL; 
            $insert_data3 = array('rcah_id'            => $new_rcah_id,
                                  'part_id'            => $value['part_id'],
                                  'asset_condition_id' => $asset_condition_arr[$value['part_id']],
                                  'created_by'         => $this->session->userdata('sso_id'),
                                  'created_time'       => date('Y-m-d H:i:s'),
                                  'remarks'            => $remarks);
            $this->Common_model->insert_data('rc_asset_health',$insert_data3);
          
        }

        $this->Common_model->update_data('rc_asset',array('current_stage_id'=>23),array('rc_asset_id'=>$rc_asset_id));

        $ash_id=$this->Common_model->get_value('asset_status_history',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id,'end_time'=>NULL),'ash_id');
        $this->Common_model->update_data('asset_status_history',array('end_time'=>date('Y-m-d H:i:s')),array('ash_id'=>$ash_id));
        $ash_arr=array(
                        'status'            =>  10,
                        'asset_id'          =>  $asset_id,
                        'current_stage_id'  =>  23,
                        'created_by'        =>  $this->session->userdata('sso_id'),
                        'created_time'      =>  date('Y-m-d H:i:s'),
                        'rc_asset_id'       =>  $rc_asset_id
                      );
        $this->Common_model->insert_data('asset_status_history',$ash_arr);

        #audit data
        $old_data = array(
            'tool_availability' => get_asset_position($asset_id),
            'wh_id'             => $asset_arr['wh_id'],
            'sub_inventory'     => $asset_arr['sub_inventory']
        );

        // Update Asset Position
        $asset_position_id=$this->Common_model->get_value('asset_position',array('asset_id'=>$asset_id,'to_date'=>NULL),'asset_position_id');
        $this->Common_model->update_data('asset_position',array('to_date'=>date('Y-m-d')),array('asset_position_id'=>$asset_position_id));
        $asset_position_data=array(
                                'asset_id'          =>  $asset_id,
                                'wh_id'             =>  $add_wh_id,
                                'from_date'         =>  date('Y-m-d'),
                                'transit'           =>  0
                              );
        $this->Common_model->insert_data('asset_position',$asset_position_data);

        #update asset
        $swh_id = $add_wh_id;
        $sub_inventory_location = validate_string($this->input->post('sub_inventory',TRUE));
        if($sub_inventory_location == '') { $sub_inventory_location =NULL; }

        $update_asset = array('wh_id'         => $swh_id,
                              'sub_inventory' => $sub_inventory_location,
                              'modified_by'   => $this->session->userdata('sso_id'),
                              'modified_time' => date('Y-m-d H:i:s'));
        $update_where_asset = array('asset_id'=> $asset_id);
        $this->Common_model->update_data('asset',$update_asset,$update_where_asset);

        #audit data
        $new_data = array(
            'tool_availability' => get_asset_position($asset_id),
            'wh_id'             => $swh_id,
            'sub_inventory'     => $sub_inventory_location
        );
        $remarks = "From Repair Vendor RR:".$rc_arr['rc_number']." Return back to Inventory";
        audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'rc_asset',$rc_asset_id,$asset_arr['country_id']);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'vendor_wh_repair_request');  
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset <strong>' .$asset_number.'</strong> has been Acknowledged successfully!</div>');
            redirect(SITE_URL.'vendor_wh_repair_request');  exit;
        }
    }

    // Srilekha
    public function view_vendor_wh_repair_request()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id =='')
        {
            redirect(SITE_URL.'closed_repair_delivery_list'); exit;
        }
         
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Closed Repair Delivery Details";
        $data['nestedView']['cur_page'] = 'closed_repair_delivery_list';
        $data['nestedView']['parent_page'] = 'closed_repair_delivery_list';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Repair Delivery Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Repair Delivery List','class'=>'','url'=>SITE_URL.'closed_repair_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Repair Delivery Details','class'=>'active','url'=>'');

        $cal_row = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Repair_m->get_vendor_closed_assets($rc_order_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $attach_document = $this->Repair_m->get_documents_by_stage($rc_order_id);
        $data['rc_order_id']=$rc_order_id;
        $data['count_c'] = count($attach_document);
        $data['cal_row']=$cal_row;
        $data['attach_document'] = $attach_document;

        $this->load->view('repair/vendor_wh_repair_tool_view',$data);
    }
    // Srilekha
    public function closed_repair_request()
    {
        $data['nestedView']['heading']="Closed Repair Requests";
        $data['nestedView']['cur_page'] = 'closed_repair';
        $data['nestedView']['parent_page'] = 'closed_repair_request';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Repair Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Repair Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'   => validate_string($this->input->post('rcb_number', TRUE)),
                'whr_id'       => validate_string($this->input->post('wh_id',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whr_id'       => $this->session->userdata('whr_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'rcb_number'   => '',
                    'whr_id'       => '',
                    'asset_number' => '',
                    'rc_number'    => '',
                    'country_id'   => '',
                    'serial_no'    => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_repair_request/';
        # Total Records

        $config['total_rows'] = $this->Repair_m->closed_repair_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Repair_m->closed_repair_results($current_offset, $config['per_page'], $searchParams,$task_access);
        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Repair_m->get_closed_asset_details($value['rc_order_id']);
            $calibration_results[$key]['asset_list']=$part_list;
        }
        # Additional data
        $data['calibration_results']=$calibration_results;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['displayResults'] = 1;
        $data['flg']=1;
        $this->load->view('repair/closed_repair_tool_list',$data);
    }

    // Srilekha
    public function view_closed_repair_request()
    {
        $rc_order_id=storm_decode($this->uri->segment(2));
        if($rc_order_id =='')
        {
            redirect(SITE_URL.'closed_repair_request'); exit;
        }

        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Closed Repair Details";
        $data['nestedView']['cur_page'] = 'closed_repair';
        $data['nestedView']['parent_page'] = 'closed_repair_request';

        #page autentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Repair Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Repair Requests','class'=>'','url'=>SITE_URL.'closed_repair_request');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Repair Details','class'=>'active','url'=>'');

        
        $cal_row=$this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $data['supplier']=$this->Common_model->get_data_row('supplier',array('supplier_id'=>$cal_row['supplier_id']));
        $data['warehouse']=$this->Common_model->get_data_row('warehouse',array('wh_id'=>$cal_row['wh_id']));
        $data['asset_details']=$this->Repair_m->get_closed_asset_details($rc_order_id);

        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        $data['main_doc'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['attach_document'] = $this->Repair_m->get_documents_by_stage($rc_order_id);
        $data['rcb_number'] = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $data['rc_order_id']=$rc_order_id;
        $data['cal_row']=$cal_row;
        $this->load->view('repair/closed_repair_tool_view',$data);
    }

    // Srilekha
    public function edit_wh_repair_request()
    {
        /*if($_SESSION['role_id']!=3)
        {
        header('Location: '.SITE_URL.'home');exit;   
        }*/
        $repair_id=@storm_decode($this->uri->segment(2));
        if($repair_id == '')
        {
            redirect(SITE_URL.'wh_repair_request'); exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Generate Repair Delivery";
        $data['nestedView']['cur_page'] = 'check_repair';
        $data['nestedView']['parent_page'] = 'wh_repair';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/fuelux/loader.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/health_calibration.js"></script>';


        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux-responsive.min.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        # Breadcrumbs
        $data['enableFormWizard'] = 1;
        $data['nestedView']['breadCrumbTite'] = 'Generate Repair Delivery';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Repair Delivery Requests','class'=>'','url'=>SITE_URL.'wh_repair_request');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Repair Delivery','class'=>'active','url'=>'');

        #additional data
        $asset_id=$this->Common_model->get_value('repair_asset',array('repair_id'=>$repair_id),'asset_id');
        $tool_id = $this->Repair_m->get_top_tool_id($asset_id);
        $data['lrow'] = $this->Repair_m->asset_details_view($asset_id,$tool_id);
        $data['asset_details']=$this->Repair_m->get_asset_part_details($asset_id);
        $data['repair_row']=$this->Repair_m->repair_details_view($repair_id);
        $rsh_id=$this->Common_model->get_value('repair_status_history',array('repair_id'=>$repair_id,'end_time'=>NULL,'current_stage_id'=>17),'rsh_id');
        $rah_id=$this->Common_model->get_value('repair_asset_history',array('rsh_id'=>$rsh_id,'end_time'=>NULL),'rah_id');
        $avail = $this->Common_model->get_data('repair_asset_health',array('rah_id'=>$rah_id));

        if(count($avail)>0)
        {
            $chealth = array();
            foreach ($data['asset_details'] as $key => $value) 
            {
                $dat = $this->Repair_m->get_repair_asset_health($rah_id,$value['part_id']);
                $chealth[$value['part_id']]['asset_condition_name'] = $dat['asset_condition_name'];
                $chealth[$value['part_id']]['remarks'] = $dat['remarks'];
            }
            $data['chealth'] = $chealth;
        }
        else
        {   
            $data['chealth'] = array();
            $defective_asset_id = $this->Common_model->get_value('repair',array('repair_id'=>$repair_id),'defective_asset_id');
            $defect_health = $this->Common_model->get_data('defective_asset_health',array('defective_asset_id'=>$defective_asset_id));
            $ddata = array();

            foreach ($defect_health as $key => $value) 
            {
                $ddata[$value['part_id']]['ac'] = $value['asset_condition_id'];
                $ddata[$value['part_id']]['remarks'] = $value['remarks'];
            }
            $data['ddata'] = $ddata;
        }
        $data['repair_id']=$repair_id;
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        $data['attach_document'] = $this->Common_model->get_data('repair_doc',array('repair_id'=>$repair_id,'status'=>1));
        
        $data['flg']=1;
        $data['form_action'] = SITE_URL.'insert_repair_delivery_details';

        $this->load->view('repair/wh_repair_wizard',$data);

    }
    // Srilekha
    public function submit_asset_health_repair()
    {
       /* if($_SESSION['role_id']!=3)
        {
        header('Location: '.SITE_URL.'home');exit;   
        }*/

        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        $repair_id = validate_number(storm_decode($this->input->post('repair_id',TRUE)));
        if($asset_id == '' || $repair_id== '')
        {
            redirect(SITE_URL.'wh_repair_request'); exit();
        }
        $check_rsh = $this->Common_model->get_value('repair_status_history',array('repair_id'=>$repair_id,'end_time'=>NULL),'rsh_id');
        if($check_rsh>17)
        {
            redirect(SITE_URL.'wh_repair_request'); exit();
        }

        $asset_condition_arr = $this->input->post('asset_condition_id',TRUE);
        $remarks_arr =  $this->input->post('remarks',TRUE);
        $asset_details=$this->Repair_m->get_asset_part_details($asset_id);
        $rsh_id=$this->Common_model->get_value('repair_status_history',array('repair_id'=>$repair_id,'end_time'=>NULL,'current_stage_id'=>17),'rsh_id');
        $repair_asset_id = $this->Common_model->get_value('repair_asset',array('repair_id'=>$repair_id,'asset_id'=>$asset_id),'repair_asset_id');

        $insert_rah    = array('rsh_id' => $rsh_id,
                               'repair_asset_id' => $repair_asset_id,
                               'created_by'      => $this->session->userdata('sso_id'),
                               'created_time'    => date('Y-m-d H:i:s'),
                               'status'          => 1);
        $this->db->trans_begin();
        $rah_id = $this->Common_model->insert_data('repair_asset_history',$insert_rah);
        
        foreach ($asset_details as $key => $value)
        {
            $remarks1 = $remarks_arr[$value['part_id']];
            if($remarks1 == '') $remarks1 = NULL; 
            $insert_data3 = array('rah_id'             => $rah_id,
                                  'part_id'            => $value['part_id'],
                                  'asset_condition_id' => $asset_condition_arr[$value['part_id']],
                                  'remarks'            => $remarks1);
            $this->Common_model->insert_data('repair_asset_health',$insert_data3);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'wh_repair_request'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset has been Linked successfully!</div>');
            redirect(SITE_URL.'edit_wh_repair_request/'.storm_encode($repair_id)); exit;
        }
    }
    // Srilekha
    public function view_wh_repair_request()
    {
        /*if($_SESSION['role_id']!=3)
        {
        header('Location: '.SITE_URL.'home');exit;   
        }*/
        $repair_id=@storm_decode($this->uri->segment(2));
        if($repair_id == '')
        {
            redirect(SITE_URL.'wh_repair_request'); exit;
        }
        $asset_id=$this->Common_model->get_value('repair_asset',array('repair_id'=>$repair_id),'asset_id');
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View Repair Requests";
        $data['nestedView']['cur_page'] = 'wh_repair';
        $data['nestedView']['parent_page'] = 'wh_repair';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/calibration.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View Repair Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Repair Delivery Requests','class'=>'','url'=>SITE_URL.'wh_repair_request');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View Repair Requests','class'=>'active','url'=>'');

        $tool_id = $this->Repair_m->get_top_tool_id($asset_id);
        $data['lrow'] = $this->Repair_m->asset_details_view($asset_id,$tool_id);
        $data['repair_row']=$this->Repair_m->repair_details_view($repair_id);

        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        $data['attach_document'] = $this->Repair_m->get_documents_by_stage($repair_id);

        $this->load->view('repair/wh_repair_request_view',$data);
    }

    //koushik 
    public function wh_repair()
    {
        unset($_SESSION['scanned_assets_list']);
        $data['nestedView']['heading']="Repair Shipment Requests";
        $data['nestedView']['cur_page'] = 'wh_repair_request';
        $data['nestedView']['parent_page'] = 'wh_repair';
        
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Repair Shipment Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Repair Shipment Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $ship_by_date = $this->input->post('ship_by_date',TRUE);
            if($ship_by_date != '')
            {
                $date = strtotime("+2 days", strtotime($ship_by_date));
                $ship_by_date =  date("Y-m-d", $date);
            }
            $searchParams=array(
                'crb_number'   => validate_string($this->input->post('crb_number', TRUE)),
                'supplier_id'  => validate_number($this->input->post('supplier_id', TRUE)),
                'whr_id'       => validate_number($this->input->post('wh_id', TRUE)),
                'ship_by_date' => $ship_by_date,
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'crb_number'   => $this->session->userdata('crb_number'),
                    'supplier_id'  => $this->session->userdata('supplier_id'),
                    'whr_id'       => $this->session->userdata('whr_id'),
                    'ship_by_date' => $this->session->userdata('ship_by_date'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'crb_number'           => '',
                    'supplier_id'          => '',
                    'whr_id'               => '',
                    'ship_by_date'         => '',
                    'page_redirect_repair' => '',
                    'asset_number'         => '',
                    'rc_number'            => '',
                    'country_id'           => '',
                    'serial_no'            => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'wh_repair_request/';
        # Total Records
        $config['total_rows'] = $this->Wh_repair_m->wh_calibration_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Wh_repair_m->wh_calibration_results($current_offset, $config['per_page'], $searchParams,$task_access);

        if(count($calibration_results)>0)
        {
            foreach ($calibration_results as $key => $value) 
            {
                $cal_asset = $this->Wh_repair_m->get_cal_asset_list($value['rc_order_id']);
                $check = $this->Repair_m->check_scanned_asset_list($value['rc_order_id']);
                $calibration_results[$key]['check_scanned_asset'] = $check;
                $calibration_results[$key]['cal_asset'] = $cal_asset;
            }
        }
        # Additional data
        $data['calibration_results']=$calibration_results;
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['supplier_list'] = $this->Common_model->get_data('supplier',array('status'=>1,'task_access'=>$task_access));
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['current_offset'] = $current_offset;

        $this->load->view('repair/wh_repair_request_list',$data);
    }

    public function unlink_repair_scanned_asset_individually()
    {
        $rc_order_id = storm_decode($this->uri->segment(2));
        $rc_asset_id = storm_decode($this->uri->segment(3));
        if($rc_order_id == '' || $rc_asset_id=='')
        {
            redirect(SITE_URL.'wh_repair_wizard/'.storm_encode($rc_order_id)); exit();
        }
        unset($_SESSION['scanned_assets_list'][$rc_asset_id]);
        $rc_asset_number = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_number');
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Assets has been unlinked for RR Number : <strong>'.$rc_asset_number.'</strong> !
        </div>'); 
        redirect(SITE_URL.'wh_repair_wizard/'.storm_encode($rc_order_id)); exit();
    }

    public function wh_repair_wizard()
    {
        $rc_order_id = @storm_decode($this->uri->segment(2));
        $country_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'country_id');
        $page_redirect_repair = $this->session->userdata('page_redirect_repair');
        if($rc_order_id == '' || $page_redirect_repair == 1)
        {
            redirect(SITE_URL.'wh_repair');
            exit;
        }  

        $data['nestedView']['heading']="Generate Repair Shipment";
        $data['nestedView']['cur_page'] = 'check_repair';
        $data['nestedView']['parent_page'] = 'wh_repair';
        $data['enableFormWizard'] = 1;

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux-responsive.min.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/fuelux/loader.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/wh_repair_wizard.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Generate Repair Shipment';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Repair Shipment Requests','class'=>'','url'=>SITE_URL.'wh_calibration');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Repair Shipment','class'=>'active','url'=>'');

        #additional data
        $crow = $this->Wh_repair_m->get_cal_asset_list($rc_order_id);

        $check_no = 0;
        foreach ($crow as $key => $value) 
        {
            $result = get_rc_scanned_assets_details($value['rc_asset_id']);
            $check = $result['asset_count'];
            if($check == 0)
            {
                $check_no++;
                $crow[$key]['check_scan'] = 0;
            }
            else
            {
                $crow[$key]['check_scan'] = 1;
            }
        }

        if($check_no>0)
        {
            $check_no = 1;
        }
        $data['check_no'] = $check_no;
        $swh_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'wh_id');
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$swh_id));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));
        $data['rc_order']= $this->Wh_repair_m->get_cal_sup_detials($rc_order_id);
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>5));
        $data['docs'] = $this->Common_model->get_data('rc_doc',array('rc_order_id'=>$rc_order_id));
        $data['flg'] = 1;
        $data['crow'] = $crow;
        $data['country_id'] = $country_id;
        $data['form_action'] = SITE_URL.'insert_wh_repair_wizard';
        $this->load->view('repair/wh_repair_wizard',$data);
    }

    public function scan_repair_asset()
    {
        $rc_asset_id = @storm_decode($this->uri->segment(2));

        if($rc_asset_id == '')
        {
            redirect(SITE_URL.'wh_repair');
            exit;
        } 
       
        $crow = $this->Wh_repair_m->get_cal_asset_detail($rc_asset_id);
        $data['nestedView']['heading']="Scan QR Code";
        $data['nestedView']['cur_page'] = 'check_repair';
        $data['nestedView']['parent_page'] = 'wh_repair';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Scan QR Code';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Repair Shipment Requests','class'=>'','url'=>SITE_URL.'wh_repair');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Repair Shipment','class'=>'','url'=> SITE_URL.'wh_repair_wizard/'.storm_encode($crow['rc_order_id']));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scan QR Code','class'=>'active','url'=>'');

        #additional data
        $data['crow'] = $crow;
        $this->load->view('repair/wh_scan_repair_asset',$data);
    }

    public function scanned_repair_asset_detials()
    {
        #page Authentication
        $task_access = page_access_check('wh_repair');

        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        $rc_asset_id = validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        $asset_number = validate_string($this->input->post('asset_number',TRUE));
        $asset_number = trim(@$asset_number);

        $asset_id = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number),'asset_id');

        $check_asset = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id,'asset_id'=>$asset_id),'rc_asset_id');
        if($check_asset == '')
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Invalid Asset Number : <strong> '.$asset_number.' </strong>!</div>'); 
            redirect(SITE_URL.'scan_repair_asset/'.storm_encode($rc_asset_id)); 
            exit();
        }

        /*koushik 11-10-2018 check asset is involved in ST or not */
        $st_entry = check_for_st_entry($asset_id);
        if(count($st_entry)>0)
        {
            $stn_number = $st_entry['stn_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
            redirect(SITE_URL.'scan_repair_asset/'.storm_encode($rc_asset_id)); exit();
        }
        else
        {

            $crow = $this->Wh_repair_m->get_cal_asset_detail($rc_asset_id);
            $data['nestedView']['heading']="Scanned Asset Details";
            $data['nestedView']['cur_page'] = 'check_repair';
            $data['nestedView']['parent_page'] = 'wh_repair';

            # include files
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/health.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = "Asset Details : ".$asset_number."";
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Repair Shipment Requests','class'=>'','url'=>SITE_URL.'wh_repair');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Repair Shipment','class'=>'','url'=> SITE_URL.'wh_repair_wizard/'.storm_encode($crow['rc_order_id']));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scan QR Code','class'=>'','url'=> SITE_URL.'scan_repair_asset/'.storm_encode($rc_asset_id));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scanned Asset Details','class'=>'active','url'=>'');

            #additional data
            $current_stage_id = 20;
            $rc = $this->Wh_repair_m->get_rcsh_by_rc_asset($rc_asset_id,$current_stage_id);
            $rcah_id = $this->Common_model->get_value('rc_asset_history',array('rcsh_id'=>$rc['rcsh_id']),'rcah_id');
            $asset_details = $this->Wh_repair_m->get_asset_part_details($asset_id);
            foreach ($asset_details as $key => $value) 
            {
                $asset_details[$key]['status'] = $this->Common_model->get_value('rc_asset_health',array('part_id'=>$value['part_id'],'rcah_id'=>$rcah_id),'asset_condition_id');
                $asset_details[$key]['remarks'] = $this->Common_model->get_value('rc_asset_health',array('part_id'=>$value['part_id'],'rcah_id'=>$rcah_id),'remarks');
            }
            $data['asset_details'] = $asset_details;
            $data['crow'] = $crow;
            
            $this->load->view('repair/wh_scan_repair_asset',$data);
        }
    }

    public function insert_scanned_repair_asset()
    {
        #page Authentication
        $task_access = page_access_check('wh_repair');

        $rc_asset_id = validate_number(storm_decode($this->input->post('rc_asset_id',TRUE)));
        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        $asset_number = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'asset_number');
        $asset_conditon = $this->input->post('asset_condition_id',TRUE);
        $remarks = $this->input->post('remarks',TRUE);
        if($rc_asset_id == '' || $asset_id == '')
        {
            redirect(SITE_URL.'wh_repair'); exit();
        }

        /*koushik 11-10-2018 check asset is involved in ST or not */
        $st_entry = check_for_st_entry($asset_id);
        if(count($st_entry)>0)
        {
            $stn_number = $st_entry['stn_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
            redirect(SITE_URL.'scan_repair_asset/'.storm_encode($rc_asset_id)); exit();
        }

        $part_list = array();
        $asset_details = $this->Wh_repair_m->get_asset_part_details($asset_id);
        foreach ($asset_details as $key => $value) 
        {
            $insert_rcahealth = array(
                'part_id'            => $value['part_id'],
                'asset_condition_id' => $asset_conditon[$value['part_id']],
                'remarks'            => $remarks[$value['part_id']]
            );
            $part_list[] = $insert_rcahealth;
        }

        $_SESSION['scanned_assets_list'][$rc_asset_id] = 
            array('asset_id'     => $asset_id,
                  'asset_number' => $asset_number,
                  'part_list_arr'=> $part_list);

        $rc_order_id = $this->Common_model->get_value('rc_asset',array('rc_asset_id'=>$rc_asset_id),'rc_order_id');
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'wh_repair_wizard/'.storm_encode($rc_order_id));  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset Is Scanned Successfully!
            </div>');
            redirect(SITE_URL.'wh_repair_wizard/'.storm_encode($rc_order_id)); exit();
        }
    }

    public function insert_wh_repair_wizard()
    {
        $rc_order_id = validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'wh_repair'); exit();
        }
        $courier_type = validate_number($this->input->post('courier_type',TRUE));
        $courier_name = validate_string($this->input->post('courier_name',TRUE));
        $courier_number = validate_string($this->input->post('courier_number',TRUE));
        $contact_person = validate_string($this->input->post('contact_person',TRUE));
        $phone_number = validate_string($this->input->post('phone_number',TRUE));
        $shipment_date = validate_string(format_date($this->input->post('shipment_date',TRUE)));
        $doc_type = $this->input->post('document_type',TRUE);
        $vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
        $remarks        = validate_string($this->input->post('remarks',TRUE));
        if($remarks == ""){ $remarks = NULL; }

        if($courier_type == 1)
        {
            $phone_number   = NULL;
            $contact_person = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 2)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 3)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
        }

        $print_type = validate_number($this->input->post('print_type',TRUE));
        $rco_wh_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'wh_id');
        $this->db->trans_begin();
        $print_id = get_current_print_id($print_type,$rco_wh_id);

        $insert_rc_ship = array(
            'rc_order_id'   => $rc_order_id,
            'print_id'      => $print_id,
            'courier_name'  => $courier_name,
            'courier_type'  => $courier_type,
            'vehicle_number'=> $vehicle_number,
            'phone_number'  => $phone_number,
            'contact_person'=> $contact_person,
            'courier_number'=> $courier_number,
            'shipment_date' => $shipment_date,
            'remarks'       => $remarks,
            'billed_to'     => validate_number($this->input->post('billed_to',TRUE)),
            'created_by'    => $this->session->userdata('sso_id'),
            'created_time'  => date('Y-m-d H:i:s'),
            'status'        => 1
        );
        $rc_shipment_id = $this->Common_model->insert_data('rc_shipment',$insert_rc_ship);

        #Update Order Address
        $update_rco = array(
        'address1'     => validate_string($this->input->post('address1',TRUE)),
        'address2'      => validate_string($this->input->post('address2',TRUE)),
        'address3'      => validate_string($this->input->post('address3',TRUE)),
        'address4'      => validate_string($this->input->post('address4',TRUE)),
        'pin_code'      => validate_string($this->input->post('pin_code',TRUE)),
        'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
        'pan_number'    => validate_string($this->input->post('pan_number',TRUE)),
        'modified_by'   => $this->session->userdata('sso_id'),
        'modified_time' => date('Y-m-d H:i:s')
        );
        $update_rco_where = array('rc_order_id'=>$rc_order_id,'status'=>1);
        $this->Common_model->update_data('rc_order',$update_rco,$update_rco_where);

        #print_history
        $insert_print_history = array(
            'print_id'          => $print_id,
            'remarks'           => 'Print Creation',
            'rc_shipment_id'    => $rc_shipment_id,
            'created_by'        => $this->session->userdata('sso_id'),
            'created_time'      => date('Y-m-d H:i:s'),
            'status'            => 1
        );
        $this->Common_model->insert_data('print_history',$insert_print_history);

        if($doc_type[1]!='')
        {
            foreach ($doc_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'         => $_FILES['support_document_'.$key]['name'],
                    'rc_order_id'      => $rc_order_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1);

                if($value!='' && $support_document != '')
                {
                    $this->Common_model->insert_data('rc_doc',$insert_doc);
                }
            }
        }

        $rc_asset_list = $this->Common_model->get_data('rc_asset',array('rc_order_id'=>$rc_order_id));

        foreach ($rc_asset_list as $rca) 
        {
            $rc_asset_id = $rca['rc_asset_id'];
            $result = get_rc_scanned_assets_details($rc_asset_id);
            $asset_id = $result['asset_id'];
            $asset_details = $result['part_list_arr'];

            $current_stage_id = 21;
            $rc_21 = $this->Wh_repair_m->get_rcsh_by_rc_asset($rca['rc_asset_id'],$current_stage_id);

            #insert_rc_asser_history
            $insert_rcah = array(
                'rc_asset_id' => $rc_asset_id,
                'rcsh_id'     => $rc_21['rcsh_id'],
                'created_by'  => $this->session->userdata('sso_id'),
                'created_time'=> date('Y-m-d H:i:s'),
                'status'      => 1,
                'remarks'     => 'Scanned At WH'
            );
            $rcah_id = $this->Common_model->insert_data('rc_asset_history',$insert_rcah);

            foreach ($asset_details as $key => $value) 
            {
                $insert_rcahealth = array(
                    'part_id'            => $value['part_id'],
                    'rcah_id'            => $rcah_id,
                    'asset_condition_id' => $value['asset_condition_id'],
                    'remarks'            => $value['remarks'],
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('rc_asset_health',$insert_rcahealth);
            }

            //update rcsh from 21 to 22
            $update_rcsh = array(
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $update_rcsh_where = array(
                'rcsh_id'=>$rc_21['rcsh_id'],
                'modified_time'=>NULL
            );
            $this->Common_model->update_data('rc_status_history',$update_rcsh,$update_rcsh_where);

            //insert rcsh with 22 - at cal vendor
            $insert_rcsh = array(
                'rc_asset_id'      => $rca['rc_asset_id'],
                'current_stage_id' => 22,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1
            );
            $rcsh_id_22 = $this->Common_model->insert_data('rc_status_history',$insert_rcsh);

            //update rc_asset with 22
            $update_rca = array(
                'current_stage_id' => 22,
                'modified_by'      => $this->session->userdata('sso_id'),
                'modified_time'    => date('Y-m-d H:i:s')
            );
            $update_rca_where = array(
                'rc_asset_id'=>$rca['rc_asset_id'],
                'rc_order_id'=>$rc_order_id
            );
            $this->Common_model->update_data('rc_asset',$update_rca,$update_rca_where);

            //insert rca history - with 22
            $insert_rcahis = array(
                'rc_asset_id' => $rca['rc_asset_id'],
                'rcsh_id'     => $rcsh_id_22,
                'created_by'  => $this->session->userdata('sso_id'),
                'created_time'=> date('Y-m-d H:i:s'),
                'status'      => 1
            );
            $rcah_id_22 = $this->Common_model->insert_data('rc_asset_history',$insert_rcahis);

            $rcah_id_21 = $this->Common_model->get_value('rc_asset_history',array('rc_asset_id'=>$rca['rc_asset_id'],'rcsh_id'=>$rc_21['rcsh_id']),'rcah_id');

            $rcahealth_21 = $this->Common_model->get_data('rc_asset_health',array('rcah_id'=>$rcah_id_21));

            foreach ($rcahealth_21 as $key => $value) 
            {
                //insert rca_health for 22
                $insert_rcahealth = array(
                    'rcah_id'            => $rcah_id_22,
                    'part_id'            => $value['part_id'],
                    'asset_condition_id' => $value['asset_condition_id'],
                    'remarks'            => $value['remarks'],
                    'created_by'         =>$this->session->userdata('sso_id'),
                    'created_time'       =>date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('rc_asset_health',$insert_rcahealth);
            }
            
            //update asset_status_history 21
            $update_ash = array(
                'end_time'    => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('sso_id')
            );
            $update_ash_where = array(
                'asset_id'    => $rca['asset_id'],
                'end_time'    => NULL,
                'rc_asset_id' => $rca['rc_asset_id']);
            $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

            //insert asset_status_history with 22
            $insert_ash = array(
                'asset_id'         => $rca['asset_id'],
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1,
                'rc_asset_id'      => $rca['rc_asset_id'],
                'current_stage_id' => 22
            );
            $this->Common_model->insert_data('asset_status_history',$insert_ash);

            #audit data
            $old_data = array('tool_availability' => get_asset_position($rca['asset_id']));
            //update asset_position to in transit
            $update_ap = array('to_date' => date('Y-m-d H:i:s'));
            $update_ap_where = array(
                'asset_id' => $rca['asset_id'],
                'to_date'  => NULL);
            $this->Common_model->update_data('asset_position',$update_ap,$update_ap_where);

            $supplier_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rca['rc_order_id']),'supplier_id');
            //insert asset_position from intransit to supplier
            $insert_ap = array(
                'asset_id'    => $rca['asset_id'], 
                'transit'     => 0,
                'supplier_id' => $supplier_id,
                'from_date'   => date('Y-m-d H:i:s'),
                'status'      => 1);
            $this->Common_model->insert_data('asset_position',$insert_ap);

            #audit data
            $new_data = array('tool_availability' => get_asset_position($rca['asset_id']));
            $remarks = "Repair asset:".$rca['rc_number']." shipped to Repair Vendor";
            audit_data('asset_master',$rca['asset_id'],'asset',2,'',$new_data,array('asset_id'=>$rca['asset_id']),$old_data,$remarks,'',array(),'rc_asset',$rca['rc_asset_id'],$rca['country_id']);
        }
        //update rc_order from 1 to 2
        $update_rco = array('status'=>2);
        $update_rco_where = array('rc_order_id' => $rc_order_id);
        $this->Common_model->update_data('rc_order',$update_rco,$update_rco_where);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'wh_repair'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Details Has been Submitted successfully!
            </div>'); 
            redirect(SITE_URL.'repair_invoice_print/'.storm_encode($rc_order_id));
        }
    }

    public function update_repair_print()
    {
        $rc_order_id = storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'closed_repair_delivery_list'); exit();
        }
        $data['nestedView']['heading']="Update Generated Print";
        $data['nestedView']['cur_page'] = "check_repair";
        $data['nestedView']['parent_page'] = 'closed_repair_delivery_list';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/update_wh_repair.js"></script>';

        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Repair Shipment List','class'=>'','url'=>SITE_URL.'closed_repair_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Generated Print','class'=>'active','url'=>'');

        
        $crow = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id));
        $wh_ship = $this->Common_model->get_data_row('rc_shipment',array('rc_order_id'=>$rc_order_id));

        #additional data
        $data['rc_order_id'] = $rc_order_id;
        $data['crow'] = $crow;
        $data['wh_ship'] = $wh_ship;
        $data['print_list'] = $this->Common_model->get_data_row('print_format',array('print_id'=>$wh_ship['print_id']));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$crow['country_id']));
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$crow['wh_id']));
        $this->load->view('repair/update_repair_print',$data);
    }

    public function update_repair_print_details()
    {
        #page authentication
        $task_access = page_access_check('closed_repair_delivery_list');

        $rc_order_id = validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'closed_repair_delivery_list'); exit();
        }
        $rcs = $this->Common_model->get_data_row('rc_shipment',array('rc_order_id'=>$rc_order_id));
        $courier_type = validate_number($this->input->post('courier_type',TRUE));
        $courier_name = validate_string($this->input->post('courier_name',TRUE));
        $courier_number = validate_string($this->input->post('courier_number',TRUE));
        $contact_person = validate_string($this->input->post('contact_person',TRUE));
        $phone_number = validate_string($this->input->post('phone_number',TRUE));
        $shipment_date = validate_string(format_date($this->input->post('shipment_date',TRUE)));
        $vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
        $remarks        = validate_string($this->input->post('remarks',TRUE));
        $date = validate_string($this->input->post('print_date'));
        $print_date = date('Y-m-d',strtotime($date));
        if($remarks == ""){ $remarks = NULL; }

        if($courier_type == 1)
        {
            $phone_number   = NULL;
            $contact_person = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 2)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 3)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
        }
        $update_rcs = array(
            'rc_order_id'   => $rc_order_id,
            'courier_name'  => $courier_name,
            'courier_type'  => $courier_type,
            'vehicle_number'=> $vehicle_number,
            'phone_number'  => $phone_number,
            'contact_person'=> $contact_person,
            'courier_number'=> $courier_number,
            'shipment_date' => $shipment_date,
            'remarks'       => $remarks,
            'billed_to'     => validate_number($this->input->post('billed_to',TRUE)),
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s'),
            'status'        => 1);
        $update_rcs_where = array('rc_shipment_id'=>$rcs['rc_shipment_id']);
        $this->db->trans_begin();
        $this->Common_model->update_data('rc_shipment',$update_rcs,$update_rcs_where);

         #Update rc order
        $update_rco = array(
            'address1'      => validate_string($this->input->post('address1',TRUE)),
            'address2'      => validate_string($this->input->post('address2',TRUE)),
            'address3'      => validate_string($this->input->post('address3',TRUE)),
            'address4'      => validate_string($this->input->post('address4',TRUE)),
            'pin_code'      => validate_string($this->input->post('pin_code',TRUE)),
            'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
            'pan_number'    => validate_string($this->input->post('pan_number',TRUE)),
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s'));
        $update_rco_where = array('rc_order_id'=>$rc_order_id,'status'=>2);
        $this->Common_model->update_data('rc_order',$update_rco,$update_rco_where);

        #update print date
        update_print_format_date($print_date,$rcs['print_id']);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'closed_repair_delivery_list'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Details Has been Updated successfully!
            </div>'); 
            redirect(SITE_URL.'repair_invoice_print/'.storm_encode($rc_order_id)); 
        } 
    }

    public function cancel_repair_print()
    {
        $rc_order_id = storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'closed_repair_delivery_list'); exit();
        }
        $data['nestedView']['heading']="Cancel Generated Print";
        $data['nestedView']['cur_page'] = "check_repair";
        $data['nestedView']['parent_page'] = 'closed_repair_delivery_list';

        #page Authenctication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Cancel Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Repair Shipment List','class'=>'','url'=>SITE_URL.'closed_repair_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Cancel Generated Print','class'=>'active','url'=>'');
        
        $print_id = $this->Common_model->get_value('rc_shipment',array('rc_order_id'=>$rc_order_id),'print_id');

        #additional data
        $data['rc_order_id'] = $rc_order_id;
        $data['crb_number'] = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'rcb_number');
        $data['print_list'] = $this->Common_model->get_data_row('print_format',array('print_id'=>$print_id));
        $data['cancel_type_list'] = $this->Common_model->get_data('cancel_type',array('status'=>1));
        $this->load->view('repair/cancel_repair_print',$data);
    }

    public function insert_cancel_repair_print()
    {
        #page Authentication
        $task_access = page_access_check('closed_repair_delivery_list');

        $rc_order_id = validate_number(storm_decode($this->input->post('rc_order_id',TRUE)));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'closed_repair_delivery_list'); exit;
        }
        $rcs = $this->Common_model->get_data_row('rc_shipment',array('rc_order_id'=>$rc_order_id));

        $update_print_history = array('remarks'=>validate_string($this->input->post('reason',TRUE)),
                                      'modified_by' => $this->session->userdata('sso_id'),
                                      'modified_time' => date('Y-m-d H:i:s'));
        $update_print_where = array('rc_shipment_id'=>$rcs['rc_shipment_id'],'modified_time'=>NULL);
        $this->db->trans_begin();
        $this->Common_model->update_data('print_history',$update_print_history,$update_print_where);

        $print_type = validate_number($this->input->post('print_type',TRUE));
        $rco_wh_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'wh_id');
        $print_id = get_current_print_id($print_type,$rco_wh_id);

        $update_rcs = array('print_id'=>$print_id,
                            'modified_by' => $this->session->userdata('sso_id'),
                            'modified_time' => date('Y-m-d H:i:s'));
        $update_rcs_where = array('rc_shipment_id'=>$rcs['rc_shipment_id']);
        $this->Common_model->update_data('rc_shipment',$update_rcs,$update_rcs_where);

        $insert_print_history = array('print_id' => $print_id,
                                      'remarks'  => 'Print Got updated',
                                      'rc_shipment_id' => $rcs['rc_shipment_id'],
                                      'created_by' => $this->session->userdata('sso_id'),
                                      'created_time' => date('Y-m-d H:i:s'));
        $this->Common_model->insert_data('print_history',$insert_print_history);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'closed_repair_delivery_list'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Print Has been Updated successfully!
            </div>'); 
            redirect(SITE_URL.'repair_invoice_print/'.storm_encode($rc_order_id)); 
        } 
    }

    public function repair_invoice_print()
    {
        /* #page Authentication
        $task_access = page_access_check('wh_repair');*/

        $rc_order_id = storm_decode($this->uri->segment(2));
        if($rc_order_id == '')
        {
            redirect(SITE_URL.'wh_repair'); exit();
        }
        $_SESSION['page_redirect_repair'] = 1;
        $from_address = $this->Wh_repair_m->get_from_address($rc_order_id); 
        $country_id = $this->Common_model->get_value('rc_order',array('rc_order_id'=>$rc_order_id),'country_id');
        $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id),'name');

        $data['to_address'] = $this->Common_model->get_data_row('rc_order',array('rc_order_id'=>$rc_order_id)); 
        $tools_list = $this->Wh_repair_m->get_calibration_tool($rc_order_id);
        $total_amount = 0;
        foreach ($tools_list as $key => $value)
        {
            $cost = $value['cost_in_inr'];
            $gst_percent = $value['gst_percent'];
            $gst = explode("%", $gst_percent);
            $cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
            if($from_address['print_type'] == 3)
            {
                $cgst_percent = round($gst[0]/2,2);
                $cgst_amt = ($cost*$cgst_percent)/100;
                $sgst_percent = $cgst_percent;
                $sgst_amt = $cgst_amt; 
            }
            else if($from_address['print_type'] == 4)
            {
                $igst_percent = $gst[0];
                $igst_amt = ($cost*$igst_percent)/100;   
            }
            $tax_amount = ($cost * $gst[0])/100;
            $amount = $cost+$tax_amount;
            $total_amount+= ($cost+$tax_amount);
            $tools_list[$key]['cgst_percent'] = $cgst_percent;
            $tools_list[$key]['cgst_amt'] = $cgst_amt;
            $tools_list[$key]['sgst_percent'] = $sgst_percent;
            $tools_list[$key]['sgst_amt'] = $sgst_amt;
            $tools_list[$key]['igst_percent'] = $igst_percent;
            $tools_list[$key]['igst_amt'] = $igst_amt;
            $tools_list[$key]['taxable_value'] = $cost;
            $tools_list[$key]['tax_amount'] = $tax_amount;
            $tools_list[$key]['amount'] = $amount;
        }
        $data['total_amount'] = $total_amount;
        $data['from_address'] = $from_address;
        $data['tools_list'] = $tools_list;
        $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['billed_to']));
        $this->load->view('repair/dc_print',$data);
    }

    //Srilekha
    public function closed_repair_delivery_list()
    {
        $data['nestedView']['heading']="Closed Repair Shipment List";
        $data['nestedView']['cur_page'] = 'closed_repair_delivery_list';
        $data['nestedView']['parent_page'] = 'closed_repair_delivery_list';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Repair Shipment List';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Repair Shipment List','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchcalibration', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'rcb_number'   => validate_string($this->input->post('rcb_number', TRUE)),
                'whc_id'       => validate_string($this->input->post('wh_id',TRUE)),
                'asset_number' => validate_string($this->input->post('asset_number',TRUE)),
                'rc_number'    => validate_string($this->input->post('rc_number',TRUE)),
                'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
                'serial_no'    => validate_string($this->input->post('serial_no',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'rcb_number'   => $this->session->userdata('rcb_number'),
                    'whc_id'       => $this->session->userdata('whc_id'),
                    'asset_number' => $this->session->userdata('asset_number'),
                    'rc_number'    => $this->session->userdata('rc_number'),
                    'country_id'   => $this->session->userdata('country_id'),
                    'serial_no'    => $this->session->userdata('serial_no')
                );
            }
            else 
            {
                $searchParams=array(
                    'rcb_number'          => '',
                    'whc_id'              => '',
                    'page_redirect_repair'=> '',
                    'asset_number'        => '',
                    'rc_number'           => '',
                    'country_id'          => '',
                    'serial_no'           => ''

                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_repair_delivery_list/';
        # Total Records

        $config['total_rows'] = $this->Wh_repair_m->closed_cal_delivery_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $calibration_results = $this->Wh_repair_m->closed_cal_delivery_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        foreach($calibration_results as $key=>$value)
        {
            $part_list=$this->Wh_repair_m->get_vendor_closed_assets($value['rc_order_id']);
            $calibration_results[$key]['asset_list']=$part_list;
        }
        
        # Additional data
        $data['calibration_results']=$calibration_results;
        $data['warehouse']=$this->Common_model->get_data('warehouse',array('status'=>1,'task_access'=>$task_access));
        $data['displayResults'] = 1;
        $data['flg']=1;
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['current_offset'] = $current_offset;
        $this->load->view('repair/closed_repair_delivery_list',$data);
    }
}