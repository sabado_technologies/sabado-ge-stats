<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');

class Gps_api extends CI_Controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Gps_tracking_report_m');
    }

    public function gps_assets()
    {
    	$sso_id = @$this->session->userdata('sso_id');
    	if($sso_id == '')
    	{
    		$task_access = '';
    	}
    	else
    	{
    		$task_access = page_access_check('gps_api');
    	}
    	$records = $this->Gps_tracking_report_m->get_gps_linked_assets($task_access);
    	if(count($records)==0)
		{
			$message = 'No GPS Linked Assets Available';
			echo json_encode($message);
		}
    	else
    	{
    		$record_data = array();
    		foreach ($records as $key => $value) 
    		{
    			$record_data[$key] = $value;
    			$record_data[$key]['tool_availability'] = get_asset_position($value['asset_id']);
    		}
    		echo json_encode($record_data);
    	}	
    }
} ?>