<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
 
class GPS extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('GPS_model');
	}

	public function gps_tracking()
    {
        $data['nestedView']['heading']="Manage Tool Tackers";
		$data['nestedView']['cur_page'] = 'gps_tracking';
		$data['nestedView']['parent_page'] = 'gps_tracking';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Manage Tool Trackers';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Tool Trackers','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtracking', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'gps_name'      => validate_string($this->input->post('gps_name', TRUE)),
                'gps_uid'       => validate_string($this->input->post('gps_uid',TRUE)),
                'gps_country'   => validate_number($this->input->post('country_id',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'gps_name'      => $this->session->userdata('gps_name'),
                    'gps_uid'       => $this->session->userdata('gps_uid'),
                    'gps_country'   => $this->session->userdata('gps_country')
                );
            }
            else 
            {
                $searchParams=array(
                    'gps_name'      => '',
                    'gps_uid'       => '',
                    'gps_country'   => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'gps_tracking/';
        # Total Records
        $config['total_rows'] = $this->GPS_model->gps_total_num_rows($searchParams,$task_access);

        $config['per_page'] =getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['gpsResults'] = $this->GPS_model->gps_results($current_offset, $config['per_page'], $searchParams,$task_access);

        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('gps/gps_view',$data);
    }
    public function add_gps_tracking()
    {
        $data['nestedView']['heading']="Add New Tool Tracker";
        $data['nestedView']['cur_page'] = 'gps_tracking';
        $data['nestedView']['parent_page'] = 'gps_tracking';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($this->session->userdata('header_country_id')!='')
            {
                $country_id = $this->session->userdata('header_country_id');
            }
            else if($this->input->post('SubmitCountry')!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
            else
            {
                $data['nestedView']['heading']= "Select Country";
                $data['nestedView']['cur_page'] = 'gps_tracking';
                $data['nestedView']['parent_page'] = 'gps_tracking';

                # include files
                $data['nestedView']['css_includes'] = array();
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Select Country';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Tool Trackers','class'=>'','url'=>SITE_URL.'tool');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Select Country','class'=>'active','url'=>'');

                $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
                $data['form_action'] = SITE_URL.'add_gps_tracking';
                $data['cancel'] = SITE_URL.'gps_tracking';
                $this->load->view('user/intermediate_page',$data); 
            }
        }
        if(@$country_id !='')
        {
            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/gps.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Add New Tool Tracker';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Tool Trackers','class'=>'','url'=>SITE_URL.'tool');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Tool Tracker','class'=>'active','url'=>'');

            # Additional data
            $data['flg'] = 1;
            $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
            $data['form_action'] = SITE_URL.'insert_gps_tracking';
            $data['displayResults'] = 0;
            $data['country_id'] = $country_id;
            $this->load->view('gps/gps_view',$data);
        }   
    }

    public function insert_gps_tracking()
    {
        #page authentication
        $task_access = page_access_check('gps_tracking');
    	$gps_name =validate_string($this->input->post('gps_name',TRUE));  
        $gps_uid=validate_string($this->input->post('gps_uid',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $gps_tool_id=0;
    	$gps_uid_available = $this->GPS_model->is_gps_uid_Exist($gps_uid,$gps_tool_id,$country_id);
    	if($gps_uid_available>0)
    	{
    		$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-times-circle"></i></div>
		    <strong>Error!</strong> GPS Name : '.$gps_uid.' already exist! please check.</div>'); 
    		redirect(SITE_URL.'gps_tracking'); exit();
        }  
        $this->db->trans_begin();
    	$data = array('name'         => $gps_name,
                      'uid_number'   => $gps_uid,
                      'country_id'   => $country_id,
                      'created_by'   => $this->session->userdata('sso_id'),
                      'created_time' => date('Y-m-d H:i:s'),
                      'status'       => 1);

        $gps_tool_id = $this->Common_model->insert_data('gps_tool',$data);
        
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.</div>');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Tool Tracker has been Added successfully!</div>');
        }
        redirect(SITE_URL.'gps_tracking');  
    }

    public function edit_gps_tracking()
    {
        $gps_tool_id=@storm_decode($this->uri->segment(2));
        if($gps_tool_id=='')
        {
            redirect(SITE_URL.'gps_tracking');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit tool Trackers";
		$data['nestedView']['cur_page'] = 'gps_tracking';
		$data['nestedView']['parent_page'] = 'gps_tracking';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/gps.js"></script>';
		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Edit Tool Trackers';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Tool Trackers','class'=>'','url'=>SITE_URL.'gps_tracking');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Tool Trackers','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_gps_tracking';
        $data['displayResults'] = 0;

        # Data
        $row = $this->Common_model->get_data_row('gps_tool',array('gps_tool_id'=>$gps_tool_id));
        $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$row['country_id']),'name');
        $data['row'] = $row;
        $data['country_id'] = $row['country_id'];
        $this->load->view('gps/gps_view',$data);
    }

    public function update_gps_tracking()
    {
        #page authentication
        $task_access = page_access_check('gps_tracking');

        $gps_tool_id= validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($gps_tool_id=='')
        {
            redirect(SITE_URL.'gps_tracking');
            exit;
        }
        
        $gps_name = validate_string($this->input->post('gps_name',TRUE));
        $gps_uid = validate_string($this->input->post('gps_uid',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));

        $gps_uid_available = $this->GPS_model->is_gps_uid_Exist($gps_uid,$gps_tool_id,$country_id);
        if($gps_uid_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> GPS Name : '.$gps_name.' already exist! please check.</div>'); 
            redirect(SITE_URL.'gps_tracking'); exit();
        }  
        $this->db->trans_begin();
        $data=array('name'          => $gps_name,
                    'uid_number'    => $gps_uid,
                    'country_id'    => $country_id,
                    'modified_by'   => $this->session->userdata('sso_id'),
                    'modified_time' => date('Y-m-d H:i:s'),
                    'status'        => 1);
        $where = array('gps_tool_id'=> $gps_tool_id);         
    	$res = $this->Common_model->update_data('gps_tool',$data,$where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.. </div>');    
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Tool Tracker has been Updated successfully!</div>');
        }
        redirect(SITE_URL.'gps_tracking');  
    }

    public function deactivate_gps_tracking($encoded_id)
    {
        #page authentication
        $task_access = page_access_check('gps_tracking');
        
        $gps_tool_id=@storm_decode($encoded_id);
        if($gps_tool_id==''){
            redirect(SITE_URL.'gps_tracking');
            exit;
        }
        $where = array('gps_tool_id' => $gps_tool_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('gps_tool',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<div class="icon"><i class="fa fa-check"></i></div>
		<strong>Success!</strong> Tool Tracker has been deactivated successfully!</div>');
        redirect(SITE_URL.'gps_tracking');
    }

    public function activate_gps_tracking($encoded_id)
    {
       #page authentication
        $task_access = page_access_check('gps_tracking');
        $gps_tool_id=@storm_decode($encoded_id);
        if($gps_tool_id==''){
            redirect(SITE_URL.'gps_tracking');
            exit;
        }
        $where = array('gps_tool_id' => $gps_tool_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('gps_tool',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<div class="icon"><i class="fa fa-check"></i></div>
		<strong>Success!</strong> Tool Tracker has been Activated successfully!
	    </div>');
        redirect(SITE_URL.'gps_tracking');
	}

    public function is_gps_uid_exist()
    {
        $uid_number = validate_string($this->input->post('uid_number',TRUE));
        $gps_tool_id = validate_number($this->input->post('gps_tool_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $result = $this->GPS_model->is_gps_uid_exist($uid_number,$gps_tool_id,$country_id);
        echo $result;
    }


}