<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By Maruthi on 20th july 17 8:30PM

class Order extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Order_m');
        $this->load->model('Fe2fe_m');
        $this->load->model('Fe_delivery_m');
        $this->load->model('Pickup_point_m');
        $this->load->model('Stock_transfer_m');
    }

    // Raise Request
    public function raise_order()
    {
        $_SESSION['order_page'] = 0; // for redirecting to first page
        $main_task_access = page_access_check('raise_order');

        #check for tool order_id
        $check_tool_order_id = validate_number($this->uri->segment(2));
        if($check_tool_order_id == '')
        {
            $tool_order_id = storm_decode($this->uri->segment(2));
        }
        else
        {
            $tool_order_id = '';
        }

        if($main_task_access == 2 || $main_task_access == 3)
        { 
            $data['onbehalf'] = 1;
            if($tool_order_id =='' || $tool_order_id == 0)  // adding
            {              
                $data['onbehalf_fe'] = validate_string(@$this->input->post('onbehalf_fe',TRUE));
                $data['onbehalf_country'] = $this->input->post('country_id',TRUE);             
                $data['onbehalf'] = 1;
                if($data['onbehalf_fe'] =='')
                {
                    $data['onbehalf_fe'] = $_SESSION['transactionUser'];//getting the onbehalf every time
                    $data['onbehalf_country'] = $_SESSION['transactionCountry']; 
                }
                else
                {
                    $_SESSION['transactionUser'] = $data['onbehalf_fe'];//first time setting the onbehalf
                    $_SESSION['transactionCountry'] = $data['onbehalf_country']; 
                }
                $accessRole = getRoleByUser($_SESSION['transactionUser']);
                $user_task_access = page_access_check('raise_order',$accessRole);  // helping for selected user role have the access or not 
            }
            else // editing 
            {
                if(!isset($_SESSION['tool_id']))
                {
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Sorry!</strong> As too many tabs opened, We are unable to do Update Transaction.Please close all tabs and do the update Transaction in Single tab.  </div>'); 
                    redirect(SITE_URL.'open_order'); exit();
                }          
                $_SESSION['transactionUser'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'sso_id');
                $_SESSION['transactionCountry'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
            }
        } 
        else
        {
            if($tool_order_id =='' || $tool_order_id == 0)
            {
                $data['onbehalf_country'] = @$this->input->post('country_id',TRUE);
                $conditionRole = getRoleByUser($_SESSION['sso_id']);
                $task_access = getPageAccess('raise_order',$conditionRole);
                if($data['onbehalf_country'] !='')
                {
                    $_SESSION['transactionCountry'] = $data['onbehalf_country'];                    
                }             
                if($task_access == 1 && count(get_user_countries($_SESSION['sso_id'])) <=1)
                {
                    $_SESSION['transactionCountry'] = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['sso_id']),'country_id');
                }
                if($this->session->userdata('header_country_id')!='')                
                {
                    $_SESSION['transactionCountry'] =  $this->session->userdata('header_country_id');
                }
                $_SESSION['transactionUser'] = $_SESSION['sso_id'];
            }
            else
            {
                $_SESSION['transactionUser'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'sso_id');
                $_SESSION['transactionCountry'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
            }
        }
          
        if($main_task_access == 1)
        {
            $conditionRole = getRoleByUser($_SESSION['sso_id']);
            $conditionAccess = getPageAccess('raise_order',$conditionRole);             
            if($conditionRole == 5 || $conditionRole ==6)
            {
                if(checkAsean($_SESSION['transactionCountry'],1))
                {
                    $whs = $this->Common_model->get_value('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1),'wh_id');
                    $data['warehouseData'] = $this->Common_model->get_data('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1),'wh_id');
                }
                else
                {
                    $location_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'location_id');
                    $data['warehouseData'] = get_warehouses_under_location($location_id,0,$_SESSION['transactionUser']);
                    $whs = get_whs_string(array('order_sso_id'=>$_SESSION['sso_id']),0,$conditionAccess); 
                }                
            }       
            else
            {
               if(checkAsean($_SESSION['transactionCountry'],1)) 
               {
                    $whs = $this->Common_model->get_value('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1),'wh_id');
               }
               else
               {
                    $whs = $_SESSION['s_wh_id'];
               }                
            }
        }
        else
        {            
            $conditionRole = getRoleByUser($_SESSION['transactionUser']);
            $conditionAccess = getPageAccess('raise_order',$conditionRole);
            if($conditionRole == 5 || $conditionRole ==6)
            {
                if(checkAsean($_SESSION['transactionCountry'],1)) 
                {
                    $whs = $this->Common_model->get_value('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1),'wh_id');
                    $data['warehouseData'] = $this->Common_model->get_data('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1));
                }
                else
                {
                    $location_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'location_id');
                    $data['warehouseData'] = get_warehouses_under_location($location_id,0,$_SESSION['transactionUser']);
                    $whs = get_whs_string(array('order_sso_id'=>$_SESSION['transactionUser']),0,$conditionAccess);
                }
            }                
            else
            {
                if($conditionAccess == 1 || $conditionAccess == 2)
                {
                    if($conditionAccess ==1)
                    {
                        if(checkAsean($_SESSION['transactionCountry'],1)) 
                            $whs = $this->Common_model->get_value('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1),'wh_id');
                        else
                            $whs = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'wh_id');
                    }
                    else
                    {
                        $whsData = $this->Common_model->get_data('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1));
                        $whs = convertIntoIndexedArray($whsData,'wh_id',1);
                    }
                    $data['warehouseData'] = $this->Common_model->get_data('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1));
                }
                else
                {
                    $data['warehouseData'] = $this->Common_model->get_data('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1));
                    $whsData = $this->Common_model->get_data('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1));
                    $whs = convertIntoIndexedArray($whsData,'wh_id',1);
                }
            }
        }

        if($tool_order_id!='')
        {
            $data['nestedView']['heading']="Update Tool Order";
            $data['nestedView']['cur_page'] = 'open_order';
            $data['nestedView']['parent_page'] = 'open_order';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Update Tool Order';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Orders','class'=>'','url'=>SITE_URL.'open_order');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Tool Order','class'=>'active','url'=>'');

            $on_behalf = '';
            if($_SESSION['transactionUser']!= $_SESSION['sso_id']) 
            {
                $on_behalf = "On Behalf Of : <strong>".getNameBySSO($_SESSION['transactionUser'])."</strong>";
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>$on_behalf,'class'=>'active','url'=>'');
            }
        }
        else
        {
            $data['nestedView']['heading']="Raise Order";
            if($main_task_access == 2 || $main_task_access == 3)
            {
                $data['nestedView']['cur_page'] = 'onbehalf_fe_1';
                $data['nestedView']['parent_page'] = 'raise_order';
            }
            else
            {
                $data['nestedView']['cur_page'] = 'raise_order_1';
                $data['nestedView']['parent_page'] = 'raise_order';
            }
            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Raise Order';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise Order','class'=>'active','url'=>'');
            $on_behalf = '';
            if($main_task_access!=1)
            {
                $on_behalf = 'On Behalf Of : <strong>'.getNameBySSO($_SESSION['transactionUser']).'</strong>';
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>$on_behalf,'class'=>'active','url'=>''); 
            }

            if(!isset($_POST['reset']) && !isset($_POST['search']) && !isset($_POST['add']) && !isset($_POST['raise_order']))
            {   
                if(isset($_SESSION['first_url_count']))
                {                    
                    $_SESSION['last_url_count'] = base_url(uri_string());
                    if(!strcmp($_SESSION['first_url_count'],$_SESSION['last_url_count']))
                    {
                        if(isset($_SESSION['tool_id'])) unset($_SESSION['tool_id']);
                        if(isset($SESSION['quantity'])) unset($_SESSION['quantity']);
                        $_SESSION['dummy'] = 0;
                    }
                }
                else
                {
                    $_SESSION['first_url_count'] = base_url(uri_string());
                    if(isset($_SESSION['tool_id'])) unset($_SESSION['tool_id']);
                    if(isset($SESSION['quantity'])) unset($_SESSION['quantity']);
                    $_SESSION['dummy'] = 0;
                }
            }
        }

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/order.js"></script>';
        $data['nestedView']['css_includes'] = array();
                
        #Logic For Cart Functionality 
        if(isset($_POST['add']))
        {
            if(!isset($_SESSION['tool_id']))
            {
                $_SESSION['tool_id'] = array();
            }
            $a = count(@$_POST['tool_id']);
            for ($i=0; $i < $a; $i++)
            {   
                if(@$_POST['quantity'][$i] > 0)
                $_SESSION['tool_id'][@$_POST['tool_id'][$i]] = @$_POST['quantity'][$i];
            }
            $_SESSION['dummy'] = 1;
        }

        #Search Functionality
        if($tool_order_id!='')
        {
            $seg_val = $this->uri->segment(3);
        }
        else
        {
            $seg_val = $this->uri->segment(2);
        }
        $psearch=validate_string($this->input->post('search', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'part_number'      => validate_string($this->input->post('part_number', TRUE)),
                'part_description' => validate_string($this->input->post('part_description', TRUE)),
                'modality_id'      => validate_number($this->input->post('modality_id', TRUE)),
                'ro_country_id'    => validate_number($this->input->post('ro_country_id', TRUE)),
                'ro_wh_id'         => validate_number($this->input->post('ro_wh_id', TRUE))
            );
            $this->session->set_userdata($searchParams);
        }
        else if($seg_val!='')
        {
            $searchParams=array(
                'part_number'      => $this->session->userdata('part_number'),
                'part_description' => $this->session->userdata('part_description'),
                'modality_id'      => $this->session->userdata('modality_id'),
                'ro_country_id'    => $this->session->userdata('ro_country_id'),
                'ro_wh_id'         => $this->session->userdata('ro_wh_id')
            );
        }
        else 
        {
            $searchParams=array(
                'part_number'      => '',
                'part_description' => '',  
                'modality_id'      => '',
                'ro_country_id'    => '',
                'ro_wh_id'         => ''
            );
            $this->session->set_userdata($searchParams);
        }

        $config = get_paginationConfig();
        $data['searchParams'] = $searchParams;
        # Default Records Per Page - always 10
        /* pagination start */
        if($tool_order_id !='')
        {
            $config['base_url'] = SITE_URL.'raise_order/'.storm_encode($tool_order_id).'/';
            $segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        }
        else
        {
            $config['base_url'] = SITE_URL.'raise_order/';
            $segment = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        }
         
        # Total Records
        $config['total_rows'] = $this->Order_m->tool_total_num_rows($searchParams,$whs);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = $segment;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        # Loading the data array to send to View
        $data['toolResults'] = $this->Order_m->tool_results($current_offset, $config['per_page'], $searchParams,$whs);
        
        # Additional data
        $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
        $data['eq_details'] = $this->Common_model->get_data('equipment_model',array('status'=>1));
        $data['displayResults'] = 1;
        $data['current_segment']= $current_offset;
        $data['tool_order_id'] = $tool_order_id;
        $data['conditionAccess'] = $conditionAccess;
        $data['conditionRole'] = $conditionRole;

        $this->load->view('order/raise_order',$data);
    }

    public function onbehalf_fe()
    {
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Raise Order On behalf of FE";
        $data['nestedView']['cur_page'] = 'raise_order';
        $data['nestedView']['parent_page'] = 'raise_order';

        # page Authentication
        $task_access = getPageAccess('raise_order',$_SESSION['s_role_id']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Raise Order On behalf of FE';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise Order On behalf of FE','class'=>'active','url'=>'');

        #additional Data
        if($task_access == 3 )
        {
            $countriesData = get_countries_string(array('order_sso_id'=>$_SESSION['sso_id']),0,$task_access,1);          
            $data['countriesData'] = $this->Common_model->get_data('location',array('task_access'=>$task_access,'ids'=>$countriesData,'status'=>1));
        }
        unset($_SESSION['transactionUser']);unset($_SESSION['onbehalf_fe']);unset($_SESSION['tool_id']);unset($_SESSION['quantity']);unset($_SESSION['first_url_count']);unset($_SESSION['last_url_count']);
        $data['form_action'] = SITE_URL.'raise_order';
        $data['task_access'] = $task_access;
        $this->load->view('order/onbehalf_fe',$data);
    }
    
    public function issue_tool()
    {
        if($_SESSION['order_page'] == 1)
        {
            redirect(SITE_URL.'open_order'); exit();
        }
        $tool_order_id = storm_decode($this->uri->segment(2));
        $transactionRole = getRoleByUser($_SESSION['transactionUser']);
        $task_access = getPageAccess('raise_order',$transactionRole);  // helping for selected user role have the access or not 

        if($tool_order_id!='')
        {
            $data['nestedView']['heading']="Update Tool Order";
            $data['nestedView']['cur_page'] = 'open_order';
            $data['nestedView']['parent_page'] = 'open_order';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Update Tool Order';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Orders','class'=>'','url'=>SITE_URL.'open_order');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Tool Order','class'=>'active','url'=>'');

            $on_behalf = '';
            if($_SESSION['transactionUser']!= $_SESSION['sso_id']) 
            {
                $on_behalf = "On Behalf Of : <strong>".getNameBySSO($_SESSION['transactionUser'])."</strong>";
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>$on_behalf,'class'=>'active','url'=>'');
            }
            $data['cancel_url'] = SITE_URL.'open_order';
        }
        else
        {
            $data['nestedView']['heading']="Issue Tool";
            $data['nestedView']['cur_page'] = 'raise_order';
            $data['nestedView']['parent_page'] = 'raise_order';

             # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Issue Tool';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));

            if($task_access!=1)
            {
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise Order','class'=>'','url'=>SITE_URL.'onbehalf_fe');
            }
            else
            {
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Raise Order','class'=>'','url'=>SITE_URL.'raise_order');
            }
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Issue Tool','class'=>'active','url'=>'');
            $on_behalf = '';
            if($_SESSION['transactionUser']!= $_SESSION['sso_id']) 
            {
                $on_behalf = "On Behalf Of : <strong>".getNameBySSO($_SESSION['transactionUser'])."</strong>";
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>$on_behalf,'class'=>'active','url'=>'');
            }
            if($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 4)
            {
                $data['cancel_url'] = SITE_URL.'onbehalf_fe';
            }
            else
            {
                $data['cancel_url'] = SITE_URL.'raise_order';
            }
        }
        
        # include files
        $data['nestedView']['js_includes'] = array();
        
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/order.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        if(isset($_SESSION['tool_id']))
        {            
            $result_data = array();
            foreach ($_SESSION['tool_id'] as $tool_id => $qty)
            {
                $result_data[] = $this->Common_model->get_data_row('tool',array('tool_id'=>$tool_id,'status'=>1));
            }            
            $data['tools'] = $result_data;
        }
        $data['service_type'] = $this->Common_model->get_data('service_type',array('status'=>1));
        $sso_id = $_SESSION['transactionUser'];
        if($transactionRole == 5 || $transactionRole == 6) 
        {
            $location_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'location_id');            
            $data['wh_data'] = get_warehouses_under_location($location_id,0,$_SESSION['transactionUser']);
            if(checkAsean($_SESSION['transactionCountry'],1))
            {
               $data['ship_by_wh_data']  = $this->Common_model->get_data('warehouse',array('country_id'=>$_SESSION['transactionCountry'],'status'=>1));                
            }
            else
            {
                $data['ship_by_wh_data'] = $data['wh_data'];
            }
            
            $wh_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'wh_id');                            
            
        }
        else
        {
            $wh_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'wh_id');                
            $country_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'country_id');
            if($task_access == 1 || $task_access == 2)
            {  
                if($task_access == 1)
                {
                    $data['wh_data'] = $this->Common_model->get_data('warehouse',array('wh_id'=>$wh_id,'status'=>1));
                }
                else
                {
                    $data['wh_data'] = $this->Common_model->get_data('warehouse',array('country_id'=>$country_id,'status'=>1));
                }
            }
            else
            { 
                $data['wh_data'] = $this->Common_model->get_data('warehouse',array('country_id'=>$country_id,'status'=>1));                            
            }
        }
        
        
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>2));        
        $data['fe_tool'] = $this->Common_model->get_data('tool',array('status'=>1,'remarks2'=>1,'country_id'=>$_SESSION['transactionCountry']));
        $data['wh_id']  = $wh_id; 
        $data['selected_ordered_to_wh'] = $wh_id;  
        $data['sso_id'] = $sso_id;

        # Additional data
        $data['flg'] = 1;  
        $data['form_action'] = SITE_URL.'insert_order';  

        // editing of the orer 
        if($tool_order_id!='')      
        {
            $data['form_action'] = SITE_URL.'update_order';  
            $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);
            if($data['order_info']['order_delivery_type_id'] == 1)
            {
                $cust_name_arr = $this->Order_m->get_cust_info_by_site($data['order_info']['site_id']);
                $data['c_name'] = $cust_name_arr['c_name'];
            }
            
            $data['sso_id']= $data['order_info']['sso_id'];
            $sso_id = $data['sso_id'];
            $data['sso_id'] = $sso_id;
            $country_id = $data['order_info']['country_id'];
            $role_id = $this->Common_model->get_value('user',array('sso_id'=>$sso_id),'role_id');           
            $data['wh_id'] = $data['order_info']['wh_id'];            
            if($role_id == 5 || $role_id== 6) 
            {
                $data['wh_data'] = get_warehouses_under_location($location_id,0,$sso_id);    
                if(checkAsean($country_id,1))
                {
                    $data['ship_by_wh_data']  = $this->Common_model->get_data('warehouse',array('country_id'=>$country_id,'status'=>1));                                 
                }
                else
                {
                    $location_id = $this->Common_model->get_value('user',array('sso_id'=>$sso_id),'location_id');
                    $dat['ship_by_wh_data'] = $data['wh_data'];
                }
            }
            else
            {   
                $wh_id = $this->Common_model->get_value('user',array('sso_id'=>$sso_id),'wh_id');                
                if($task_access == 1 || $task_access == 2)
                {  
                    if($task_access == 1)
                    {
                        $data['wh_data'] = $this->Common_model->get_data('warehouse',array('wh_id'=>$wh_id,'status'=>1));
                    }
                    else
                    {
                        $data['wh_data'] = $this->Common_model->get_data('warehouse',array('country_id'=>$country_id,'status'=>1));
                    }
                }
                else
                {
                    $data['wh_data'] = $this->Common_model->get_data('warehouse',array('country_id'=>$country_id,'status'=>1));                            
                }
            }

            $data['service_type'] = $this->Common_model->get_data('service_type',array('status'=>1));
            $data['document_type'] = $this->Common_model->get_data('document_type',array('status'=>1));
            $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1));
            $data['fe_users'] = get_fe_users($task_access,$_SESSION['transactionUser'],0,getCountryIdWithToolOrder($tool_order_id));
            $attach_document = $this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));
            $data['fe_tool'] = $this->Common_model->get_data('tool',array('status'=>1,'remarks2'=>1,'country_id'=>$_SESSION['transactionCountry']));
            $data['count_c'] = count($attach_document);
            $data['docs'] = $attach_document;
            $data['flg'] = 2; 

        }
        $data['conditionAccess'] = $task_access;
        $data['conditionRole'] = $transactionRole;
        $this->load->view('order/issue_tool',$data);
    }

    public function removeToolFromIssueCart()
    {
        $tool_id = validate_string($this->input->post('tool_id',TRUE));
        if(isset($_SESSION['tool_id'][$tool_id]))
        {
            unset($_SESSION['tool_id'][$tool_id]);
            echo 1;
        } 
    }

    public function insert_order()
    {
        $_SESSION['order_page'] = 1;
        if($this->input->post('submitorder',TRUE) != '')
        {
            if(count(@$_SESSION['tool_id']) == 0 )
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please add more than 1 tool to cart and proceed </div>'); 
                redirect(SITE_URL.'issue_tool'); exit(); 
            }
            $order_delivery_type_id = validate_number($this->input->post('delivery_type_id',TRUE)); 
            $deploy_date = format_date(validate_string(@$this->input->post('deploy_date',TRUE)),'Y-m-d');
            $request_date = format_date(validate_string(@$this->input->post('request_date',TRUE)),'Y-m-d');
            $return_date = format_date(validate_string(@$this->input->post('return_date',TRUE)),'Y-m-d');            
            $conditionRole = validate_string(@$this->input->post('conditionRole',TRUE));            
            $ordered_tools = array();
            $final_sso_id = validate_string($this->input->post('final_sso_id',TRUE));
            $sso_id = $final_sso_id;            
            $userInfo = getUserDetailsByUser($sso_id);            
            $role_id = $userInfo['role_id'];
            $conditionAccess = getPageAccess('raise_order',$conditionRole);
            if($role_id == 5 || $role_id == 6 || $conditionAccess == 2 || $conditionAccess == 3)
            {
                if(checkAsean($_SESSION['transactionCountry'],1))               
                { 
                    $wh_id = getAseanWarehouse($_SESSION['transactionCountry'],1);
                }
                else
                { 
                    $wh_id =  validate_number($this->input->post('wh_id',TRUE));    
                }                
                $fe_to_wh_id =($order_delivery_type_id == 2)?validate_string($this->input->post('fe_to_wh_id',TRUE)):NULL;
            }
            else
            {
                if(checkAsean($_SESSION['transactionCountry'],1))               
                {
                    $wh_id = getAseanWarehouse($_SESSION['transactionCountry'],1);
                }
                else
                {
                    $wh_id = $this->Common_model->get_value('user',array('sso_id'=>$sso_id),'wh_id');
                }
                $fe_to_wh_id =($order_delivery_type_id == 2)?validate_string($this->input->post('fe_to_wh_id',TRUE)):NULL;
            }
            $country_id = $_SESSION['transactionCountry'];
            
            // inserting into tool Order
            $service_type_id  = validate_number($this->input->post('service_type_id',TRUE)); 
            $pin_code         = validate_string($this->input->post('pin_code',TRUE));
            $siebel_number    = validate_string($this->input->post('srn_number',TRUE));
            $fe_check         = validate_number($this->input->post('fe_check',TRUE));
            $fe1_order_number = validate_string(@$this->input->post('fe1_order_number',TRUE));
            $site_id          = validate_string($this->input->post('site_id',TRUE));
            $system_id        = validate_string($this->input->post('system_id',TRUE));
            $install_base_id  = validate_string($this->input->post('install_base_id',TRUE));
            $site_readiness   = validate_string($this->input->post('site_readiness',TRUE));
            $site_remarks     = validate_string($this->input->post('site_remarks',TRUE));
            $address_remarks  = validate_string($this->input->post('address_remarks',TRUE));
            $check_address    = validate_string($this->input->post('check_address',TRUE));
            $check_address    = ($order_delivery_type_id == 1)?$check_address:0;
            $service_type_remarks = validate_string(@$this->input->post('service_type_remarks',TRUE));
            $toolsWithQty     =  $this->input->post('quantity',TRUE);
            $notes = validate_string(@$this->input->post('notes',TRUE));
            $fe1_order_number = ($fe_check == 1)?$fe1_order_number:NULL;

            // Checking Weather FE1 Having the Same Tools Which FE2 Required
            if($fe_check == 1)
            {
                checkOrderNumberAvailability(2,$fe1_order_number,$toolsWithQty,$country_id);
            }                    
            if($order_delivery_type_id ==1)
            {
                $customer_site_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'customer_site_id');
                $add_data  = $this->Common_model->get_data_row('customer_site',array('customer_site_id'=>$customer_site_id));
                if($check_address == 0)
                {
                    $address1 = $add_data['address1'];
                    $address2 = $add_data['address2']; 
                    $address3 = $add_data['address3']; 
                    $address4 = $add_data['address4']; 
                    $pin_code = $add_data['zip_code']; 
                }
                else
                {                            
                    $address1 = validate_string($this->input->post('ct_address_1',TRUE));
                    $address2 = validate_string($this->input->post('ct_address_2',TRUE));
                    $address3 = validate_string($this->input->post('ct_address_3',TRUE));
                    $address4 = validate_string($this->input->post('ct_address_4',TRUE));
                    $pin_code = validate_string($this->input->post('ct_pin_code',TRUE));   
                }
                $location_id = $add_data['location_id'];
            }
            if($order_delivery_type_id == 2)
            {
                $install_base_id = NULL;
                $address1 = validate_string($this->input->post('wh_address_1',TRUE));
                $address2 = validate_string($this->input->post('wh_address_2',TRUE));
                $address3 = validate_string($this->input->post('wh_address_3',TRUE));
                $address4 = validate_string($this->input->post('wh_address_4',TRUE));
                $pin_code = validate_string($this->input->post('wh_pin_code',TRUE));
                $location_id = $this->Common_model->get_value('warehouse',array('wh_id' =>$wh_id),'location_id');
            }
            if($order_delivery_type_id == 3)
            {
                $install_base_id = NULL;
                $address1 = validate_string($this->input->post('address_1',TRUE));
                $address2 = validate_string($this->input->post('address_2',TRUE));
                $address3 = validate_string($this->input->post('address_3',TRUE));
                $address4 = validate_string($this->input->post('address_4',TRUE));
                $pin_code = validate_string($this->input->post('pin_code',TRUE));
            }
            if($check_address ==1) $current_stage_id = 4;
            if($fe_check == 1) $current_stage_id = 4;
            if(@$current_stage_id!=4) $current_stage_id = 5;

            // Order Number Generation
            $number = $this->Order_m->get_latest_order_record($country_id);
            if(count($number) == 0)
            {
                $f_order_number = "1-00001";
            }
            else
            {
                $num = ltrim($number['order_number'],"T");                    
                $result = explode("-", $num);
                $running_no = (int)$result[1];
                if($running_no == 99999)
                {
                    $first_num = $result[0]+1;
                    $second_num = get_running_sno_five_digit(1);  
                }
                else
                {
                    $first_num = $result[0];
                    $val = $running_no+1;
                    $second_num = get_running_sno_five_digit($val);
                }
                $f_order_number = $first_num.'-'.$second_num;
            }
            $order_number ="T".$f_order_number;
            $this->db->trans_begin();
            $order_data = array(
                'sso_id'                 => $sso_id,
                'service_type_id'        => $service_type_id,
                'site_readiness'         => $site_readiness,
                'site_remarks'           => $site_remarks,
                'service_type_remarks'   => @$service_type_remarks,
                'order_number'           => $order_number,
                'order_delivery_type_id' => $order_delivery_type_id,
                'current_stage_id'       => 5, // default as 5
                'wh_id'                  => $wh_id,
                'to_wh_id'               => $fe_to_wh_id,
                'request_date'           => $request_date,
                'return_date'            => $return_date,
                'deploy_date'            => $deploy_date,
                'siebel_number'          => $siebel_number,
                'fe_check'               => $fe_check,
                'fe1_order_number'       => $fe1_order_number,
                'country_id'             => $country_id,
                'order_type'             => 2,                    
                'created_by'             => $this->session->userdata('sso_id'),
                'created_time'           => date('Y-m-d H:i:s')
            );
            if(getTaskPreference("tool_order_notes",$_SESSION['transactionCountry']))
                $order_data['notes'] = $notes;
            
            $tool_order_id = $this->Common_model->insert_data('tool_order',$order_data);

            $order_address_data = array(
                'address1'      => $address1,
                'address2'      => $address2,
                'address3'      => $address3,
                'address4'      => $address4,
                'pin_code'      => $pin_code,
                'remarks'       => $address_remarks,
                'check_address' => $check_address,
                'site_id'       => $site_id,
                'install_base_id' => $install_base_id,
                'gst_number'    => @$gst_number,
                'pan_number'    => @$pan_number,
                'system_id'     => $system_id,
                'tool_order_id' => $tool_order_id,
                'location_id'   => @$location_id,
                'created_by'    => $this->session->userdata('sso_id'),
                'created_time'  => date('Y-m-d H:i:s')
            );
            $order_address_id = $this->Common_model->insert_data('order_address',$order_address_data);
            # Audit start
            $order_data['address1'] = $order_address_data['address1'];
            $order_data['address2'] = $order_address_data['address2'];
            $order_data['city'] = $order_address_data['address3'];
            $order_data['state'] = $order_address_data['address4'];
            $order_data['pin_code'] = $order_address_data['pin_code'];
            $order_data['pan_number'] = $order_address_data['pan_number'];
            $order_data['gst_number'] = $order_address_data['gst_number'];
            if($order_data['order_delivery_type_id'] == 1)
            {
                $order_data['customer'] = $this->Common_model->get_value_new("customer",array("customer_id"=>$add_data['customer_id']),'concat(name, " - ",customer_number)');
                $order_data['site_id'] = $order_address_data['site_id'];
                $order_data['system_id'] = $order_address_data['system_id'];
                $order_data['check_address'] = $order_address_data['check_address'];
                if($order_data['check_address'] == 1)
                    $order_data['address_remarks'] = $order_address_data['remarks'];
            }

            unset($order_data['tool_order_id']);
            if($order_data['fe_check'] ==0)
            {
                unset($order_data['fe1_order_number']);
            }            
            if($order_data['service_type_id'] !=1)
            {
                unset($order_data['site_readiness']);
                unset($order_data['site_remarks']);
            }
            if($order_data['service_type_id']!=5)
            {
                unset($order_data['service_type_remarks']);
            }
            $blockArr  = array('tool_order_id'=> $tool_order_id);
            $remarksString = "Created Tool Order with No : ".$order_number;
            $ad_id = tool_order_audit_data('tool_order',$tool_order_id,'tool_order',1,'',$order_data,$blockArr,array(),$remarksString,'',array(),"tool_order",$tool_order_id,$country_id);
            # Audit End

            $order_extd_days_data = array(
                'tool_order_id' => $tool_order_id,
                'return_date'   => $return_date,
                'created_by'    => $this->session->userdata('sso_id'),
                'created_time'  => date('Y-m-d H:i:s')
            );
            $this->Common_model->insert_data('order_extended_days',$order_extd_days_data);

            // inseting documents
            $document_type = ($this->input->post('document_type',TRUE));
            foreach ($document_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = order_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $doc_name = $_FILES['support_document_'.$key]['name'];
                }
                else
                {
                    $support_document = '';
                }

                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => @$support_document,
                    'doc_name'         => @$doc_name,
                    'tool_order_id'    => @$tool_order_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s')
                );
                if($value!='')
                    $order_doc_id = $this->Common_model->insert_data('order_doc',$insert_doc);
            }

            // looping tools and get asset which are available
            $ordered_tools = array();
            foreach ($_POST['quantity'] as $tool_id => $qty)
            {
                $ordered_tools[$tool_id] = $qty;
            }
            
            foreach ($ordered_tools as $tool_id => $quantity)
            {
                $searchParams = array(
                    'wh_id'     => $wh_id,
                    'tool_id'   => $tool_id
                );

                $available_assets = available_assets($searchParams);
                $available_assets_count = count($available_assets);
                // sending request to admin if any of the quantity is not fullfilled by assigned warehouse
                if(@$available_assets_count < $quantity) $current_stage_id = 4;

                // looping all the assets and assigning to lock In period on basis of First Come First Locked In till Ordered Quantity
                if(count($available_assets_count) > 0)
                {
                    $available_quantity = 0;
                    $order_tool_data = array(
                        'tool_order_id'      => $tool_order_id,
                        'tool_id'            => $tool_id,
                        'quantity'           => $quantity,
                        'available_quantity' => $available_quantity,
                        'status'             => 1,
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s')
                    ); 
                    $ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);
                    # Audit start
                    $order_tool_data = unsetOrderedToolsExceptionColumns($order_tool_data);
                    $order_tool_data['tool_status'] = "Added";
                    $auditOrderedToolBlock = array('tool_id'=>$tool_id);
                    $auditOrderedToolId = tool_order_audit_data('ordered_tools',$ordered_tool_id,'ordered_tool_id',1,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$tool_order_id,$country_id);
                    # Audit end
                    // don't lock any assets if that order checked for fe check 
                    if($fe_check!=1)
                    {
                        // lock the assets
                        foreach ($available_assets as $key => $value)
                        {
                            $available_quantity++; 
                            // updating asset table 
                            $asset_main_status = $value['asset_main_status'];
                            $asset_actual_status  = ($value['asset_main_status']==1)?2:$asset_main_status;
                            $where = array('asset_id'=>$value['asset_id']);

                            $u_data  = array(
                                'status'        => $asset_actual_status,
                                'modified_by'   => $this->session->userdata('sso_id'),
                                'modified_time' => date('Y-m-d H:i:s')
                            );
                            $this->Common_model->update_data('asset',$u_data,$where);

                            # Audit Start
                            assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"locked_assets",1,$auditOrderedToolId,$remarks=NULL,$trans_ad_id=NULL,"tool_order",$tool_order_id,$country_id);
                            $auditAssetRemarks = "Locked for ".$order_data['order_number'];
                            assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$country_id);
                            # Audit End

                            # Update insert asset status history
                            $assetStatusHistoryArray = array(
                                'asset_id'          => $value['asset_id'],
                                'created_by'        => $this->session->userdata('sso_id'),
                                'status'            => $asset_actual_status,
                                'ordered_tool_id'   => $ordered_tool_id,
                                'current_stage_id'  => 4,
                                'tool_order_id'     => $tool_order_id
                            );
                            updateInsertAssetStatusHistory($assetStatusHistoryArray);

                            if($available_quantity == $quantity) break;
                        }                        
                        $status = ($available_quantity == $quantity)?1:2;
                    }
                    // unlocking and updating the status to 2
                    else
                    {
                        $status = 2;
                        $current_stage_id = 4;

                    }
                    $this->Common_model->update_data('ordered_tool',array('status'=>$status,'available_quantity'=>$available_quantity),array('ordered_tool_id'=>$ordered_tool_id));
                    # Audit Start
                    updatingAuditData($auditOrderedToolId,array('available_quantity'=>$available_quantity));
                    # Audit End
                    
                    if($status == 2) $current_stage_id = 4;                         
                }
                else
                {
                    $order_tool_data = array(
                        'tool_order_id'      => $tool_order_id,
                        'tool_id'            => $tool_id,
                        'available_quantity' => 0,
                        'quantity'           => $quantity,
                        'status'             => 2, 
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s')
                    ); 
                    $ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$ordered_tool_data);
                    
                    # Audit start      
                    $order_tool_data  = unsetOrderedToolsExceptionColumns($order_tool_data);
                    $order_tool_data['tool_status'] = "Added";
                    $auditOrderedToolBlock = array('tool_id'=>$tool_id);
                    $auditOrderedToolId = tool_order_audit_data('ordered_tools',$tool_id,'tool',1,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$tool_order_id,$country_id);
                    # Audit end
                }
            }
            $this->Common_model->update_data('tool_order',array('current_stage_id'=>$current_stage_id),array('tool_order_id'=>$tool_order_id));         

            $order_status_history_data = array(
                'current_stage_id'  => $current_stage_id,
                'tool_order_id'     => $tool_order_id,
                'remarks'           => '',
                'created_by'        => $this->session->userdata('sso_id'),
                'created_time'      => date('Y-m-d H:i:s'),
                'status'            =>  1, 
                );
            $this->Common_model->insert_data('order_status_history',$order_status_history_data);
            $f_current_stage_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'current_stage_id');
            # Audit Start
            $updateArray = array('current_stage_id'=>$f_current_stage_id);
            updatingAuditData($ad_id,$updateArray);
            # Audit End

            // send email if the current stage is at 4 
            if($f_current_stage_id == 4)
            {
                // sending the email if address check is 1 
                $f_order_delivery_type_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'order_delivery_type_id');
                if($f_order_delivery_type_id == 1)
                {
                    $f_check_address = $this->Common_model->get_value('order_address',array('tool_order_id'=>$tool_order_id),'check_address');                    
                    if($f_check_address == 1 && getUserStatus($sso_id) ==1)
                        sendAddressChangeNotification($tool_order_id);  // uncomment again 
                    
                }
                // send the email if tools are not available                
                $order_status = $this->Common_model->get_data('ordered_tool',array('status'=>2,'tool_order_id'=>$tool_order_id));                
            }          
            
            if(getUserStatus($sso_id) == 1)
            {
                #Send Mail to Fe while placing a Tool Order Request
                sendtoolordernotificationtofe($tool_order_id,1);

                if(!getTaskPreference("tool_order_mail",$order_data['country_id'])) {
                    // tool order mail to SA/Admin/Tc by default
                    sendToolsNotAvailableNotification($tool_order_id,1);    
                }                

                #Send mail to FE1 in FE-FE transfer
                if($fe_check==1)
                {
                    #send mail to FE1 when tool order is requested from other FE
                    sendmailtofe1fortoolorder($tool_order_id,trim($fe1_order_number)); 
                    #send mail to Admins when tool order is requested from other FE
                    sendmailtoadminforfe2fetoolorder($tool_order_id,trim($fe1_order_number)); 

                }
            }            
            if($current_stage_id == 4)            
            {             
                if(@$status == 2 && getUserStatus($sso_id) == 1 )  // because eventhough tools are available also mail is triggering
                    sendToolsNotAvailableNotification($tool_order_id);  // By Default Notification  // uncomment again
                // updating current stage in order 
                $this->Common_model->update_data('tool_order',array('current_stage_id'=>$current_stage_id),array('tool_order_id'=>$tool_order_id));

                # Audit start              
                $updateArray = array('current_stage_id'=>$current_stage_id);
                updatingAuditData($ad_id,$updateArray);
                # Audit end
            }

            if($current_stage_id == 5 && getUserStatus($sso_id) == 1)
            {   
                // send mails to logistic if tools are available             
                sendToolsNotAvailableNotification($tool_order_id,1,$order_data['wh_id']);
            }
            // unsetting the sessions created in raise order
            unsetToolOrderSessions();            
            if($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'raise_order'); exit();
            }
            else
            { 
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Order has been Created successfully With Order Number :<strong>'.$order_number.'</strong> </div>');
                redirect(SITE_URL.'open_order'); exit();
            }
        } 
        else
        {
            redirect(SITE_URL.'raise_order'); exit();
        }     
    }

    public function chackSiteIDAvailability()
    {
        $site_id = validate_string($this->input->post('site_id',TRUE));
        $data = array('site_id'=>$site_id);        
        $transactionUser = $_SESSION['transactionUser'];
        $role = getRoleByUser($transactionUser);
        $task_access = getPageAccess('raise_order',$role);
        $ids = get_countries_string(array('order_sso_id'=>$transactionUser),0,$task_access,1);
        $result = $this->Common_model->get_data('customer_site',array('site_id'=>$site_id,'ids'=>$ids));
        $result_data = count($result);
        if($result_data == 0) 
        {
            echo $result_data;
        }              
        else
        {
            echo 1;
        }
    }
    
    public function chackSystemIDAvailability()
    {
        $system_id = validate_string($this->input->post('system_id',TRUE));    
        $transactionUser = validate_string($this->input->post('transactionUser',TRUE));        
        $transactionCountry = $this->Common_model->get_value('user',array('sso_id'=>$transactionUser),'country_id');
        $role = getRoleByUser($transactionUser);
        $task_access = getPageAccess('raise_order',$role);
        if(isset($transactionCountry))
        {
            $result = $this->Order_m->get_install_basedata($system_id,$transactionCountry);
        }
        else
        {
            $ids = get_countries_string(array('order_sso_id'=>$transactionUser),0,$task_access);
            $result = $this->Order_m->get_install_basedata($system_id,0,$ids);
        }

        $result_data = count($result);
        if($result_data == 0)    
        {
            echo $result_data;
        }        
        else
        {
            echo 1;
        }
    }


    public function checkToolsAvailabilityAgain()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        $requiredTools = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status'=>2));
        $ordered_tools = array();
        $ordTool = array();
        $order_info = $this->Order_m->get_order_info($tool_order_id);
        # Audit for Tool Order
        $statusArr = array('current_stage_id'=>4,'tool_order_id'=>$tool_order_id);
        $oldData = array('current_stage' => getOpenOrderStatus($statusArr));
        $newData = array('current_stage' => getOpenOrderStatus($statusArr));
        $blockArr = array('blockArr'=> "Tool Order");
        $remarksString = "Tool Availability Checked Again by Admin/SA for Order".$order_info['order_number'];
        $ad_id = tool_order_audit_data('tool_order',$tool_order_id,'tool_order',2,'',$newData,$blockArr,$oldData,$remarksString,'',array(),"tool_order",$tool_order_id,$order_info['country_id']);
        # Audit End
        $this->db->trans_begin();
        foreach ($requiredTools as $key => $values)
        {
            $quantity = ($values['quantity']- $values['available_quantity']);
            $original_available_quantity = $values['available_quantity'];
            $ordered_tool_id = $values['ordered_tool_id'];
            $available_quantity = 0;
            $searchParams = array(
                    'wh_id'     => $order_info['wh_id'],
                    'tool_id'   => $values['tool_id']
                );

            $available_assets = available_assets($searchParams);
            $available_assets_count = count($available_assets);
            // sending request to admin if any of the quantity is not fullfilled by assigned warehouse
            if(@$available_assets_count < $quantity) $current_stage_id = 4;

            // looping all the assets and assigning to lock In period on basis of First Come First Locked In till Ordered Quantity
            if(count($available_assets_count) > 0)
            {
                $oldArr = array(
                    'available_quantity' => $values['available_quantity'],
                    'quantity'           => $values['quantity'],
                    'status'             => NULL
                );    
                $order_tool_data = array(
                    'available_quantity' => $values['available_quantity'],
                    'quantity'           => $values['quantity'],
                    'status'             => "Updated"
                );    
                $auditOrderedToolBlock = array('tool_id'=> $values['tool_id']);
                $auditOrderedToolId = tool_order_audit_data('ordered_tools',$ordered_tool_id,'ordered_tool',2,$ad_id,$order_tool_data,$auditOrderedToolBlock,$oldArr,'','',array(),"tool_order",$tool_order_id,$order_info['country_id']);             
                foreach ($available_assets as $key => $value)
                {
                    $available_quantity++; 
                    $asset_main_status = $value['asset_main_status'];
                    $asset_actual_status  = ($value['asset_main_status']==1)?2:$asset_main_status;
                    // updating asset table 

                    $where = array('asset_id'=>$value['asset_id']);
                    $u_data  = array(
                        'status'        => $asset_actual_status,
                        'modified_by'   => $this->session->userdata('sso_id'),
                        'modified_time' => date('Y-m-d H:i:s')
                        );
                    $this->Common_model->update_data('asset',$u_data,$where);

                    # Audit Start
                        $auditAssetRemarks = "Locked for ".$order_info['order_number'];
                        assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"locked_assets",1,$auditOrderedToolId,'','',"tool_order",$tool_order_id,$order_info['country_id']);
                        assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$order_info['country_id']);
                    # Audit End

                    # Update insert asset status history
                    $assetStatusHistoryArray = array(
                        'asset_id'          => $value['asset_id'],
                        'created_by'        => $this->session->userdata('sso_id'),
                        'status'            => $asset_actual_status,
                        'ordered_tool_id'   => $values['ordered_tool_id'], // gives new tool_order id
                        'current_stage_id'  => 4,
                        'tool_order_id'     => $tool_order_id
                    );
                    updateInsertAssetStatusHistory($assetStatusHistoryArray);
                    if($available_quantity == $quantity) break;
                }                        
                $status = ($available_quantity == $quantity)?1:2;
                $finalAvailableQty = ($available_quantity+$original_available_quantity);
                $this->Common_model->update_data('ordered_tool',array('status'=>$status,'available_quantity'=>$finalAvailableQty),array('ordered_tool_id'=>$ordered_tool_id));
                # Audit Update for Ordered Tool
                $updateAuditData = array('available_quantity'=>$finalAvailableQty);
                updatingAuditData($auditOrderedToolId,$updateAuditData);
                # Audit End
                if($status == 2) $current_stage_id = 4;                         
            }
        }        

        if(@$current_stage_id != 4)            
            $current_stage_id = 5;
        if(@$current_stage_id == 5)
        {               
            $order_status = $this->Common_model->get_data('ordered_tool',array('status'=>2,'tool_order_id'=>$tool_order_id));
            // tools availability and address check and fe check then only update or move the order to warehosue 
            if(count($order_status) == 0 && $order_info['check_address'] ==0 && $order_info['fe_check'] ==0 )
            {
                $order_status_history_data = array(
                        'current_stage_id'  => 5,
                        'tool_order_id'     => $tool_order_id,
                        'remarks'           => '',
                        'created_by'        => $this->session->userdata('sso_id'),
                        'created_time'      => date('Y-m-d H:i:s'),
                        'status'            =>  1, 
                        );
                $this->Common_model->insert_data('order_status_history',$order_status_history_data); 
                $this->Common_model->update_data('tool_order',array('current_stage_id'=>5),array('tool_order_id'=>$tool_order_id)); 
                $updateAuditData = array('current_stage'=>"At WH");
                updatingAuditData($ad_id,$updateAuditData);
                if(getUserStatus() == 1)
                    sendToolsNotAvailableNotification($tool_order_id,1);
            }                
        }        

        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'raiseSTforOrder'); exit();
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Tools Availability has been checked successfully For Order Number '.$order_info['order_number'].' </div>');
            redirect(SITE_URL.'raiseSTforOrder'); exit();
        } 
    }

    public function getAddressByID()
    {
        $system_id = validate_string($this->input->post('system_id',TRUE));                
        $result = $this->Order_m->get_install_basedata($system_id);
        echo json_encode($result);
    }

    public function getWarehouseAddress()
    {
        $wh_id = @$this->input->post('warehouse_id',TRUE);
        $result = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$wh_id));
        echo json_encode($result);
    }

    public function htmlWarehouseDropdown()
    {
        $sso_id = $this->input->post("sso_id",TRUE);
        $location_id = $this->Common_model->get_value('user',array('sso_id'=>$sso_id),'location_id');        
        echo get_warehouses_under_location($location_id,$html=1,$sso_id,@$transactionCountry);
    }

    // Open Orders
    public function open_order()
    {
        $data['nestedView']['heading']="Open Orders";
        $data['nestedView']['cur_page'] = 'open_order';
        $data['nestedView']['parent_page'] = 'open_order';

        unset($_SESSION['order_update']);
        $task_access = page_access_check('open_order');

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Open Orders';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Orders','class'=>'active','url'=>'');

        # Search Functionality
        $psearch= ($this->input->post('open_order', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'order_number'     => validate_string($this->input->post('order_number', TRUE)),
            'request_date'     => validate_string($this->input->post('request_date', TRUE)),
            'return_date'      => validate_string($this->input->post('return_date', TRUE)),
            'search_fe_sso_id' => validate_string($this->input->post('search_fe_sso_id', TRUE)),
            'current_stage_id' => validate_number($this->input->post('current_stage_id', TRUE)),
            'ro_country_id'    => validate_number($this->input->post('ro_country_id', TRUE)),
            'order_delivery_type_id' => validate_number($this->input->post('order_delivery_type_id', TRUE)));
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order_number'     => $this->session->userdata('order_number'),
                'request_date'     => $this->session->userdata('request_date'),
                'return_date'      => $this->session->userdata('return_date'),
                'search_fe_sso_id' => $this->session->userdata('search_fe_sso_id'),
                'current_stage_id' => $this->session->userdata('current_stage_id'),
                'ro_country_id'    => $this->session->userdata('ro_country_id'),
                'order_delivery_type_id' => $this->session->userdata('order_delivery_type_id')
                );
            }
            else 
            {
                $searchParams=array(
                'order_number'     => '',
                'request_date'     => '',
                'return_date'      => '',
                'search_fe_sso_id' => '',
                'current_stage_id' => '',
                'ro_country_id'    => '',
                'order_delivery_type_id' => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'open_order/';

        # Total Records
        $config['total_rows'] = $this->Order_m->order_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Order_m->order_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        # Additional data
        $data['displayResults'] = 1;
        $conditionAccess =page_access_check('open_order');
        $data['task_access'] = $conditionAccess;
        $data['countriesData'] = $this->Common_model->get_data('location',array('task_access'=>$conditionAccess,'status'=>1));
        $data['current_stage'] = $this->Common_model->get_data('current_stage',array('workflow_type'=>2,'current_stage_id!='=>10));
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1));
        $this->load->view('order/open_order',$data);
    }

    public function order_info()
    {
        //echo count($_SESSION['tool_id']);exit;
        $tool_order_id=@storm_decode($this->uri->segment(2));
        $cancel_form_action =@storm_decode($this->uri->segment(3));
        if(@$cancel_form_action == 1 ) // 1 For To go To Closed Order At FE/Admin
        {
            $data['form_action'] = SITE_URL.'closed_order';

            # Data Array to carry the require fields to View and Model
            $data['nestedView']['heading']="Order Information";
            $data['nestedView']['cur_page'] = 'closed_order';
            $data['nestedView']['parent_page'] = 'closed_order';

            #page Authentication
            $task_access = page_access_check($data['nestedView']['parent_page']);

            # include files
            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['css_includes'] = array();
            
            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = 'Order Information';
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Order','class'=>'','url'=>SITE_URL.'closed_order');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
        }   
        else
        {
            if(@$cancel_form_action == 2) // For Address Change At Admin
            {
                $data['form_action'] = SITE_URL.'address_change';
                # Data Array to carry the require fields to View and Model
                $data['nestedView']['heading']="Order Information";
                $data['nestedView']['cur_page'] = 'address_change';
                $data['nestedView']['parent_page'] = 'address_change';

                # include files
                $data['nestedView']['js_includes'] = array();
                $data['nestedView']['css_includes'] = array();
                
                # Breadcrumbs
                $data['nestedView']['breadCrumbTite'] = 'Order Information';
                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Address Change','class'=>'','url'=>SITE_URL.'address_change');
                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
            }
            else
            {
                if(@$cancel_form_action == 3) // For Crossed Return Date Orders
                {
                    $data['form_action'] = SITE_URL.'crossed_return_date_orders'; 
                    # Data Array to carry the require fields to View and Model
                    $data['nestedView']['heading']="Order Information";
                    $data['nestedView']['cur_page'] = 'crossed_return_date_orders';
                    $data['nestedView']['parent_page'] = 'crossed_return_date_orders';

                    # include files
                    $data['nestedView']['js_includes'] = array();
                    $data['nestedView']['css_includes'] = array();
                    
                    # Breadcrumbs
                    $data['nestedView']['breadCrumbTite'] = 'Order Information';
                    $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Crossed Return date Orders','class'=>'','url'=>SITE_URL.'crossed_return_date_orders');
                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
                }
                else
                {
                    if(@$cancel_form_action == 4) // For Closed Order In Transfer
                    {
                        $data['form_action'] = SITE_URL.'closed_wh_delivery_list'; 
                        # Data Array to carry the require fields to View and Model
                        $data['nestedView']['heading']="Order Information";
                        $data['nestedView']['cur_page'] = 'closed_wh_delivery_list';
                        $data['nestedView']['parent_page'] = 'closed_wh_delivery_list';

                        # include files
                        $data['nestedView']['js_includes'] = array();
                        $data['nestedView']['css_includes'] = array();
                        
                        # Breadcrumbs
                        $data['nestedView']['breadCrumbTite'] = 'Order Information';
                        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Wh Delivery List','class'=>'','url'=>SITE_URL.'closed_wh_delivery_list');
                        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
                    }
                    else
                    {
                        if(@$cancel_form_action == 5)
                        {
                            $data['form_action'] = SITE_URL.'wh_stock_transfer_list'; 
                            # Data Array to carry the require fields to View and Model
                            $data['nestedView']['heading']="Order Information";
                            $data['nestedView']['cur_page'] = 'wh_stock_transfer_list';
                            $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';

                            # include files
                            $data['nestedView']['js_includes'] = array();
                            $data['nestedView']['css_includes'] = array();
                            
                            # Breadcrumbs
                            $data['nestedView']['breadCrumbTite'] = 'Order Information';
                            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Wh Stock Transfer List','class'=>'','url'=>SITE_URL.'wh_stock_transfer_list');
                            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
                        }
                        else
                        {
                            if(@$cancel_form_action ==6 ) // 6 For Exceeded ORder Duration
                            {
                                $data['form_action'] = SITE_URL.'exceededOrderDuration';
                                # Data Array to carry the require fields to View and Model
                                $data['nestedView']['heading']="Exceeded Order Duration";
                                $data['nestedView']['cur_page'] = 'exceededOrderDuration';
                                $data['nestedView']['parent_page'] = 'exceededOrderDuration';

                                # include files
                                $data['nestedView']['js_includes'] = array();
                                $data['nestedView']['css_includes'] = array();
                                
                                #page Autentication
                                $task_access = page_access_check('exceededOrderDuration');

                                # Breadcrumbs
                                $data['nestedView']['breadCrumbTite'] = 'Order Information';
                                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Exceeded Order Duration','class'=>'','url'=>SITE_URL.'exceededOrderDuration');
                                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
                            }
                            else
                            {
                                if(@$cancel_form_action == 7)
                                {

                                    $data['form_action'] = SITE_URL.'raiseSTforOrder'; 
                                    # Data Array to carry the require fields to View and Model
                                    $data['nestedView']['heading']="Order Information";
                                    $data['nestedView']['cur_page'] = 'raiseSTforOrder';
                                    $data['nestedView']['parent_page'] = 'raiseSTforOrder';

                                    # include files
                                    $data['nestedView']['js_includes'] = array();
                                    $data['nestedView']['css_includes'] = array();

                                    # Breadcrumbs
                                    $data['nestedView']['breadCrumbTite'] = 'Order Information';
                                    $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'','url'=>SITE_URL.'raiseSTforOrder');
                                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
                                }
                                else
                                {
                                    if(@$cancel_form_action == 8)   
                                    {
                                        $data['form_action'] = SITE_URL.'openSTforOrder'; 
                                        # Data Array to carry the require fields to View and Model
                                        $data['nestedView']['heading']="List Of ST For Order";
                                        $data['nestedView']['cur_page'] = 'openSTforOrder';
                                        $data['nestedView']['parent_page'] = 'openSTforOrder';

                                        # include files
                                        $data['nestedView']['js_includes'] = array();
                                        $data['nestedView']['css_includes'] = array();

                                        # Breadcrumbs
                                        $data['nestedView']['breadCrumbTite'] = 'List Of Stock Transfer';
                                        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                                        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'List Of Stock Transfer','class'=>'','url'=>SITE_URL.'openSTforOrder');
                                        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
                                    }
                                    else
                                    {
                                        if(@$cancel_form_action == 9)
                                        {
                                            $data['form_action'] = SITE_URL.'closedSTforOrder'; 
                                            $data['nestedView']['heading']="Closed ST For Order";
                                            $data['nestedView']['cur_page'] = 'closedSTforOrder';
                                            $data['nestedView']['parent_page'] = 'closedSTforOrder';

                                            # include files
                                            $data['nestedView']['js_includes'] = array();
                                            $data['nestedView']['css_includes'] = array();

                                            # Breadcrumbs
                                            $data['nestedView']['breadCrumbTite'] = 'List Of Closed Stock Transfer';
                                            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                                            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'List Of Closed Stock Transfer','class'=>'active','url'=>SITE_URL.'closedSTforOrder');
                                            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');
                                        }
                                        else
                                        {
                                            if(@$cancel_form_action == 11)
                                            {                                                
                                                $data['form_action'] = SITE_URL.'receive_order'; 
                                                # Data Array to carry the require fields to View and Model
                                                $data['nestedView']['heading']="Order Information";
                                                $data['nestedView']['cur_page'] = 'receive_order';
                                                $data['nestedView']['parent_page'] = 'receive_order';

                                                # include files
                                                $data['nestedView']['js_includes'] = array();
                                                $data['nestedView']['css_includes'] = array();  

                                                # Breadcrumbs
                                                $data['nestedView']['breadCrumbTite'] = 'Order Information';
                                                $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                                                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Order','class'=>'','url'=>SITE_URL.'receive_order');
                                                $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');       
                                            }
                                            else
                                            {
                                                if(@$cancel_form_action == 12)
                                                {
                                                    $data['form_action'] = SITE_URL.'closed_receive_order'; 
                                                    # Data Array to carry the require fields to View and Model
                                                    $data['nestedView']['heading']="Order Information";
                                                    $data['nestedView']['cur_page'] = 'closed_receive_order';
                                                    $data['nestedView']['parent_page'] = 'closed_receive_order';

                                                    # include files
                                                    $data['nestedView']['js_includes'] = array();
                                                    $data['nestedView']['css_includes'] = array();

                                                    # Breadcrumbs
                                                    $data['nestedView']['breadCrumbTite'] = 'Order Information';
                                                    $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                                                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Order','class'=>'','url'=>SITE_URL.'closed_receive_order');
                                                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');          
                                                }
                                                else
                                                {
                                                    $data['form_action'] = SITE_URL.'open_order'; 
                                                    # Data Array to carry the require fields to View and Model
                                                    $data['nestedView']['heading']="Order Information";
                                                    $data['nestedView']['cur_page'] = 'open_order';
                                                    $data['nestedView']['parent_page'] = 'open_order';

                                                    # include files
                                                    $data['nestedView']['js_includes'] = array();
                                                    $data['nestedView']['css_includes'] = array();

                                                    # Breadcrumbs
                                                    $data['nestedView']['breadCrumbTite'] = 'Order Information';
                                                    $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
                                                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Order','class'=>'','url'=>SITE_URL.'open_order');
                                                    $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Order Information','class'=>'active','url'=>'');          
                                                }
                                            }
                                        }
                                    }                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'open_order');
            exit;
        }        
        
        if(@$cancel_form_action == 1)                   
            $data['tools'] =  $this->Order_m->get_ordered_tools($tool_order_id,1);
        else
            $data['tools'] =  $this->Order_m->get_ordered_tools($tool_order_id);

        $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);
        
        // echo '<pre>'; print_r($data['tools']);
        // echo '<pre>'; print_r($data['order_info']);exit;
        

        $attach_document = $this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));
        //echo '<pre>';print_r($data['wh_data']);exit;
        $data['count_c'] = count($attach_document);
        $data['attach_document'] = $attach_document; 

        # Data
        /*$row = $this->Asset_m->parent_asset_details($asset_id);
        $data['lrow'] = $row[0];
        $data['child_parts'] = $this->Asset_m->child_asset_details($asset_id);*/
        $this->load->view('order/order_info',$data);
    }

    public function edit_order()
    {
        $main_task_access = page_access_check('open_order');
        unsetToolOrderSessions();
        if(isset($_SESSION['order_update']))
        {
            redirect(SITE_URL.'open_order'); exit();
        }        
        $tool_order_id=@storm_decode($this->uri->segment(2));

        $return_data = checkMultipleReturnsForFE2Order($tool_order_id);
        if(count($return_data)>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> Tool Order '.$return_data['fe1_order_number'].' has been already raised for your Tool Order '.$return_data['fe2_order_number'].' Which is In Progress. </div>'); 
            redirect(SITE_URL.'open_order'); exit();
        }

        if($tool_order_id=='')
        {
            redirect(SITE_URL.'open_order');
            exit;
        }        
        $_SESSION['order_edit_tool_order_id'] = $tool_order_id;
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Update Tool Order";
        $data['nestedView']['cur_page'] = 'open_order';
        $data['nestedView']['parent_page'] = 'open_order';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/order.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Tool Order';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Open Orders','class'=>'','url'=>SITE_URL.'open_order');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Tool Order','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_order';                               
        $data['tools'] =  $this->Order_m->get_ordered_tools($tool_order_id);
        $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);
        if($data['order_info']['order_delivery_type_id'] == 1)
        {
            $cust_name_arr = $this->Order_m->get_cust_info_by_site($data['order_info']['site_id']);
            $data['c_name'] = $cust_name_arr['c_name'];
        }        
        $data['sso_id']= $data['order_info']['sso_id'];
        $sso_id = $data['sso_id'];
        $data['sso_id'] = $sso_id;
        $_SESSION['transactionUser'] = $sso_id;

        #breadcrumb
        $on_behalf = '';
        if($_SESSION['transactionUser']!= $_SESSION['sso_id']) 
        {
            $on_behalf = "On Behalf Of : <strong>".getNameBySSO($_SESSION['transactionUser'])."</strong>";
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>$on_behalf,'class'=>'active','url'=>'');
        }

        $_SESSION['transactionCountry'] = $data['order_info']['country_id'];
        $role_id = $this->Common_model->get_value('user',array('sso_id'=>$sso_id),'role_id');
        $data['conditionAccess'] = getPageAccess('raise_order',$role_id) ; 
        $task_access = $data['conditionAccess'];
        $data['conditionRole'] = $role_id;
        $data['wh_id'] = $data['order_info']['wh_id'];  
        $wh_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'wh_id');                
        if($role_id == 5 || $role_id == 6 ) 
        {
            $location_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'location_id');                
            $data['wh_data'] = get_warehouses_under_location($location_id,0,$_SESSION['transactionUser']);
            if(checkAsean($_SESSION['transactionCountry'],1))
            {
               $data['ship_by_wh_data']  = getAseanWarehouse($_SESSION['transactionCountry'],2);               
            }
            else
            {
                $data['ship_by_wh_data'] = $data['wh_data'];
            }                
        }
        else
        {
           
            $task_access = getPageAccess('raise_order',$role_id) ;      
            if($task_access == 1 || $task_access == 2)
            {  
                if($task_access == 1)
                    $data['wh_data'] = $this->Common_model->get_data('warehouse',array('wh_id'=>$wh_id,'status'=>1));
                else
                    $data['wh_data'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
            }
            else
            {
                $data['wh_data'] = $this->Common_model->get_data('warehouse',array('country_id'=>getCountryIdWithToolOrder($tool_order_id),'status'=>1));    
            }
        }
        //echo '<pre>';print_r($data['wh_data']);
        $data['selected_ordered_to_wh'] = $wh_id;
        //echo $data['selected_ordered_to_wh'];exit;
        $data['service_type'] = $this->Common_model->get_data('service_type',array('status'=>1));        
        $data['document_type'] = $this->Common_model->get_data('document_type',array('status'=>1));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>2));
        $attach_document = $this->Common_model->get_data('order_doc',array('tool_order_id'=>$tool_order_id));
        $data['fe_users'] = get_fe_users($task_access,$_SESSION['transactionUser'],0,getCountryIdWithToolOrder($tool_order_id));
        $data['count_c'] = count($attach_document);
        $data['docs'] = $attach_document;
        $data['flg'] = 2; 

        // setting the session variables
        foreach ($data['tools'] as $key => $value) 
        {
            $_SESSION['tool_id'][$value['tool_id']]  = $value['quantity'];
        }
        $data['fe_tool'] = $this->Common_model->get_data('tool',array('status'=>1,'remarks2'=>1,'country_id'=>$_SESSION['transactionCountry']));
        # Data 
        $data['cancel_url'] = SITE_URL.'open_order';
        $this->load->view('order/issue_tool',$data);
    }
    
    public function update_order()
    {
        $_SESSION['order_update'] = 2;
        if($this->input->post('submitorder',TRUE) != '')
        {     
            if(count(@$_SESSION['tool_id']) == 0 )
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> As too many tabs opened, We are unable to do Update Transaction.Please close all tabs and do the update Transaction in Single tab. </div>'); 
                redirect(SITE_URL.'open_order'); exit(); 
            }  
            # Post data     
            $tool_order_id              = validate_number($this->input->post('tool_order_id',TRUE));
            $final_sso_id               = validate_string($this->input->post('final_sso_id',TRUE));
            $orderInfo                  = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
            $order_number               = $orderInfo['order_number'];
            $old_fe_check               = $orderInfo['fe_check'];
            $mainSSO                    = $orderInfo['sso_id'];
            $old_address_check          = $this->Common_model->get_value('order_address',array('tool_order_id'=>$tool_order_id),'check_address');
            $current_stage_id           = validate_number($this->input->post('current_stage_id',TRUE));
            $service_type_id            = validate_number($this->input->post('service_type_id',TRUE));
            $check_address              = validate_number($this->input->post('check_address',TRUE));            
            $fe_check                   = validate_string($this->input->post('fe_check',TRUE));
            $fe1_order_number           = validate_string($this->input->post('fe1_order_number',TRUE));
            $deploy_date                = format_date(validate_string(@$this->input->post('deploy_date',TRUE)),'Y-m-d');
            $request_date               = format_date(validate_string(@$this->input->post('request_date',TRUE)),'Y-m-d');
            $return_date                = format_date(validate_string(@$this->input->post('return_date',TRUE)),'Y-m-d');
            $order_delivery_type_id     = validate_number($this->input->post('delivery_type_id',TRUE));
            $fe_to_wh_id                = validate_string($this->input->post('fe_to_wh_id',TRUE));

            $fe1_order_number = ($fe_check == 1)?$fe1_order_number:NULL;
            $old_wh_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');           
            if($fe_check == 1)
            {
                checkOrderNumberAvailability(2,$fe1_order_number,$_POST['quantity'],$_SESSION['transactionCountry']);
            }

            $ordered_tools = array();
            if($check_address==1)
                $current_stage_id = 4;
            
            if($fe_check == 1)
                $current_stage_id = 4;

            $sso_id = $final_sso_id;            
            $role_id = getRoleByUser($sso_id);
            
            $check_address = ($order_delivery_type_id == 1)?$check_address:0;            
            if($role_id == 5 || $role_id == 6)
            {
                $wh_id = validate_string($this->input->post('wh_id',TRUE));
                $fe_to_wh_id =($order_delivery_type_id == 2)?validate_string($this->input->post('fe_to_wh_id',TRUE)):NULL;
            }
            else
            {
                if(checkAsean($_SESSION['transactionCountry'],1))
                {
                    $wh_id = getAseanWarehouse($_SESSION['transactionCountry'],1);   
                }
                else
                {
                    $wh_id = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'wh_id');                
                }
                $fe_to_wh_id =($order_delivery_type_id == 2)?$fe_to_wh_id:NULL;
            }
            $userInfo = getUserDetailsByUser($sso_id);
            // inserting into tool Order            
            $service_type_id = validate_number($this->input->post('service_type_id',TRUE));                    
            $pin_code        = validate_number($this->input->post('pin_code',TRUE));            
            $deploy_date     = format_date(validate_string($this->input->post('deploy_date',TRUE)),'Y-m-d');
            $request_date    = format_date(validate_string($this->input->post('request_date',TRUE)),'Y-m-d');
            $return_date     = format_date(validate_string($this->input->post('return_date',TRUE)),'Y-m-d');  
            $siebel_number = validate_string($this->input->post('srn_number',TRUE));
            $site_id = validate_string($this->input->post('site_id',TRUE));
            $system_id = validate_string($this->input->post('system_id',TRUE));
            $install_base_id =  validate_string($this->input->post('install_base_id',TRUE));
            $site_readiness =  validate_string($this->input->post('site_readiness',TRUE));
            $site_remarks   =  validate_string($this->input->post('site_remarks',TRUE));
            $address_remarks=  validate_string($this->input->post('address_remarks',TRUE));
            $notes = validate_string(@$this->input->post('notes',TRUE));
            
            $service_type_remarks = validate_string($this->input->post('service_type_remarks',TRUE));
            if($order_delivery_type_id ==1)
            {
                $customer_site_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id,'country_id'=>$userInfo['country_id']),'customer_site_id');
                $add_data  = $this->Common_model->get_data_row('customer_site',array('customer_site_id'=>$customer_site_id,'country_id'=>$userInfo['country_id']));
                if($check_address == 0)
                {
                    $address1        = $add_data['address1'];
                    $address2        = $add_data['address2']; 
                    $address3        = $add_data['address3']; 
                    $address4        = $add_data['address4']; 
                    $pin_code        = $add_data['zip_code']; 
                }
                else
                {                            
                    $address1        = validate_string($this->input->post('ct_address_1',TRUE));
                    $address2        = validate_string($this->input->post('ct_address_2',TRUE));
                    $address3        = validate_string($this->input->post('ct_address_3',TRUE));
                    $address4        = validate_string($this->input->post('ct_address_4',TRUE));
                    $pin_code        = validate_string($this->input->post('ct_pin_code',TRUE));   
                }
                $location_id = $add_data['location_id'];
            }
            if($order_delivery_type_id == 2)
            {
                $install_base_id = NULL;
                $address1        = validate_string($this->input->post('wh_address_1',TRUE));
                $address2        = validate_string($this->input->post('wh_address_2',TRUE));
                $address3        = validate_string($this->input->post('wh_address_3',TRUE));
                $address4        = validate_string($this->input->post('wh_address_4',TRUE));
                $pin_code        = validate_string($this->input->post('wh_pin_code',TRUE));
                $location_id     = $this->Common_model->get_value('warehouse',array('wh_id' =>$wh_id),'location_id');
            }
            if($order_delivery_type_id == 3)
            {
                $install_base_id = NULL;
                $address1        = validate_string($this->input->post('address_1',TRUE));
                $address2        = validate_string($this->input->post('address_2',TRUE));
                $address3        = validate_string($this->input->post('address_3',TRUE));
                $address4        = validate_string($this->input->post('address_4',TRUE));
                $pin_code        = validate_string($this->input->post('pin_code',TRUE));
            }

            # Old data for Audit

            # get the required data from database which are used in edit transaction
            $oldOrderData = $this->Order_m->get_order_info($tool_order_id);
            $orderedTools =  $this->Order_m->get_ordered_tools($tool_order_id);
            # As Current stage getting from defined logic, we should not pass current_stage_id to the audit array
            $statusArr = array('current_stage_id'=>$oldOrderData['current_stage_id'],'tool_order_id'=>$tool_order_id);
            $oldOrderStatus = getOpenOrderStatus($statusArr);
            $auditOldOrderData = array(
                'service_type_id'        => $oldOrderData['service_type_id'],
                'order_delivery_type_id' => $oldOrderData['order_delivery_type_id'],
                'wh_id'                  => $oldOrderData['wh_id'],
                'current_stage'          => $oldOrderStatus,
                'to_wh_id'               => $oldOrderData['to_wh_id'],
                'fe_check'               => $oldOrderData['fe_check'],
                'request_date'           => $oldOrderData['request_date'],
                'return_date'            => $oldOrderData['return_date'],
                'deploy_date'            => $oldOrderData['deploy_date'],
                'siebel_number'          => $oldOrderData['siebel_number'],
                'order_type'             => $oldOrderData['order_type'],
                'address1'               => $oldOrderData['address1'],
                'address2'               => $oldOrderData['address2'],
                'address3'               => $oldOrderData['address3'],
                'address4'               => $oldOrderData['address4'],
                'pin_code'               => $oldOrderData['pin_code'],
                'pan_number'             => $oldOrderData['pan_number'],
                'gst_number'             => $oldOrderData['gst_number']
            );
            
            if($oldOrderData['order_delivery_type_id'] ==1)
            {
                $auditOldOrderData['customer']       = $this->Common_model->get_value_new("customer",array("customer_id"=>$add_data['customer_id']),'concat(name, " - ",customer_number)');
                $auditOldOrderData['site_id']        = $oldOrderData['site_id'];
                $auditOldOrderData['system_id']      = $oldOrderData['system_id'];
                $auditOldOrderData['check_address']  = $oldOrderData['check_address'];
                if($oldOrderData['check_address']==1)
                    $auditOldOrderData['address_remarks']        = $oldOrderData['address_change_remarks'];
            }

            // chaekcing all the available tools in order are available in warehouse
            $this->db->trans_begin();
            $order_data = array(
                    'sso_id'                 => $sso_id,
                    'service_type_id'        => $service_type_id,
                    'service_type_remarks'   => @$service_type_remarks,
                    'order_delivery_type_id' => $order_delivery_type_id,            
                    'wh_id'                  => $wh_id,
                    'current_stage_id'       => $current_stage_id,
                    'to_wh_id'               => $fe_to_wh_id,
                    'fe_check'               => $fe_check,
                    'fe1_order_number'       => $fe1_order_number,
                    'site_readiness'         => $site_readiness,
                    'site_remarks'           => $site_remarks,
                    'request_date'           => $request_date,
                    'return_date'            => $return_date,
                    'deploy_date'            => $deploy_date,
                    'siebel_number'          => $siebel_number,
                    'order_type'             => 2,
                    'modified_by'            => $this->session->userdata('sso_id'),
                    'modified_time'          => date('Y-m-d H:i:s'));
            if(getTaskPreference("tool_order_notes",$_SESSION['transactionCountry']))
                $order_data['notes'] = $notes;
            
            $this->Common_model->update_data('tool_order',$order_data,array('tool_order_id'=>$tool_order_id));
            # Audit Start
            # removing the unnecessary variables from updated data
            unset($order_data['sso_id'],$order_data['modified_by'],$order_data['modified_time'],$order_data['current_stage_id']);
            # Pushing the required data into old array
            if( $auditOldOrderData['fe_check']!=$oldOrderData['fe_check'] ) // previously 1 now 0
            {
                $auditOldOrderData['fe1_order_number'] = $oldOrderData['fe1_order_number'];
            }
            else
            {
                if($order_data['fe1_order_number']!=$oldOrderData['fe1_order_number']) // old value is 1 and new also 1 but order number changes  
                {
                    $auditOldOrderData['fe1_order_number'] = $oldOrderData['fe1_order_number'];
                }
                else
                {
                    unset($order_data['fe1_order_number']);
                }
            }

            if( ($order_data['service_type_id']!=1) && ($oldOrderData['service_type_id']!=1) )
            {
                unset($order_data['site_readiness']);
                unset($order_data['site_remarks']);
            }
            if( ($order_data['service_type_id']==1) && ($oldOrderData['service_type_id']!=1) )
            {
                if($order_data['site_readiness'] == 1)
                    unset($order_data['site_remarks']);
            }
            if( ($order_data['service_type_id']==1) && ($oldOrderData['service_type_id']==1) )
            {
                if(@$order_data['site_readiness'] = 1)
                {
                    unset($order_data['site_readiness']);
                    unset($order_data['site_remarks']);       
                }
            }

            if( $auditOldOrderData['service_type_id'] == 5 )
            {
                $auditOldOrderData['service_type_remarks'] = $oldOrderData['service_type_remarks'];
            }
            else
            {
                if($order_data['service_type_id'] != 5)
                    unset($order_data['service_type_remarks']);
            }
            
            # Audit End
            $this->Common_model->update_data('order_extended_days',array('return_date'=>$return_date),array('tool_order_id'=>$tool_order_id,'status'=>1));

            $order_address_data = array(
                'address1'      => $address1,
                'address2'      => $address2,
                'address3'      => $address3,
                'address4'      => $address4,
                'gst_number'    => @$gst_number,
                'pan_number'    => @$pan_number,
                'pin_code'      => $pin_code,
                'remarks'       => $address_remarks,
                'check_address' => $check_address,
                'site_id'       => $site_id,
                'system_id'     => $system_id,
                'install_base_id' => $install_base_id,
                'tool_order_id' => $tool_order_id,
                'location_id'   => @$location_id,                    
                'created_by'    => $this->session->userdata('sso_id'),
                'created_time'  => date('Y-m-d H:i:s')
            );
            
            $this->Common_model->update_data('order_address',$order_address_data,array('tool_order_id'=>$tool_order_id));


            $order_data['address1'] = $order_address_data['address1'];
            $order_data['address2'] = $order_address_data['address2'];
            $order_data['address3'] = $order_address_data['address3'];
            $order_data['address4'] = $order_address_data['address4'];
            $order_data['pin_code'] = $order_address_data['pin_code'];
            $order_data['pan_number'] = $order_address_data['pan_number'];
            $order_data['gst_number'] = $order_address_data['gst_number'];
            if($order_data['order_delivery_type_id'] == 1)
            {
                $order_data['customer']         = $this->Common_model->get_value_new("customer",array("customer_id"=>$add_data['customer_id']),'concat(name, " - ",customer_number)');
                $order_data['site_id']          = $order_address_data['site_id'];
                $order_data['system_id']        = $order_address_data['system_id'];
                $order_data['check_address']    = $order_address_data['check_address'];
                if($order_address_data['check_address'] == 1)
                    $order_data['address_remarks']          = $order_address_data['remarks'];
            }
            
            $finalArr = array_diff_assoc($order_data, $auditOldOrderData);
            $statusArr = array('tool_order_id'=>$tool_order_id,'current_stage_id'=>$current_stage_id);
            # As current stage defined at the end of the transaction, we need to insert old and new values by default
            $finalArr['current_stage'] = getOpenOrderStatus($statusArr);
            $toolOrderExceptionColumns = array('tool_order_id');
            $blockArr = array("tool_order_id"=>$tool_order_id);
            $remarks_arr = "Updated Tool Order : ".$oldOrderData['order_number']." Details ";
            $ad_id = tool_order_audit_data('tool_order',$tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$auditOldOrderData,$remarks_arr,'',array(),"tool_order",$tool_order_id,$oldOrderData['country_id']);

            // inseting documents

            $document_type = ($this->input->post('document_type',TRUE));
            foreach ($document_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = order_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                    $doc_name = $_FILES['support_document_'.$key]['name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'         => @$doc_name,
                    'tool_order_id'    => @$tool_order_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s')
                );     
                if($value!='')
                {   
                    $order_doc_id = $this->Common_model->insert_data('order_doc',$insert_doc);
                }
            }
            
            
            // unlock the assets and decide the order status 
            if($old_wh_id == $wh_id)  // for FE,Zonal,National if there is no change in warehouse selected
            {        
                if($fe_check == 1)
                {   
                    // unlock the assets which are linked for that order
                    $arr_to_unlock = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status<'=>3));
                    $dbs_tools = array();
                    if(count($arr_to_unlock)>0)
                    {
                        foreach ($arr_to_unlock as $key => $value)
                        {
                            $tool_id = $value['tool_id'];
                            $ordered_tool_id = $value['ordered_tool_id'];
                            $quantity =  $value['quantity'];
                            $auditOrderedToolId = unlockingByToolOrderId($tool_order_id,0,0,$ordered_tool_id,$ad_id,2);
                            // updating the quantity and status based on posted from view page
                            $available_quantity = 0;
                            if(array_key_exists($tool_id, $_POST['quantity']))
                            {
                               $ordered_tool_status = 2;
                               $auditOrderedToolStatus = 'Updated';
                               $new_required_qty = $_POST['quantity'][$tool_id]; 
                            }
                            else
                            {
                                $ordered_tool_status = 3;
                                $auditOrderedToolStatus = 'Removed';
                                $new_required_qty = $quantity;
                            }
                            // updating the ordered tool that saying that pending and making the available quantity to 0
                            $updateData = array(
                                'status'                => $ordered_tool_status,
                                'quantity'              => $new_required_qty,
                                'available_quantity'    => $available_quantity
                            );
                            $this->Common_model->update_data('ordered_tool',$updateData,array('ordered_tool_id'=>$ordered_tool_id));
                            # Audit Start
                            $auditUpdateData = array(
                                'tool_status'           => $auditOrderedToolStatus,
                                'quantity'              => $new_required_qty,
                                'available_quantity'    => $available_quantity
                            );
                            updatingAuditData($auditOrderedToolId,$auditUpdateData);
                            # Audit End
                            // pushing the dbs tools to insert the newly added tools from view page
                            $dbs_tools[$tool_id] = $quantity;
                        }
                    }
                    // inserting the list of new tools added to card  
                    // and skipping the old tools because old tools quantity  and status already updated in above loop
                    foreach ($_POST['quantity'] as $key => $value)
                    {
                        // posted values are new tools
                        if(!array_key_exists($key, $dbs_tools))
                        {                            
                            $order_tool_data = array(
                                'tool_order_id'      => $tool_order_id,
                                'tool_id'            => $key,
                                'available_quantity' => 0,
                                'quantity'           => $value,
                                'status'             => 2,
                                'created_by'         => $this->session->userdata('sso_id'),
                                'created_time'       => date('Y-m-d H:i:s')
                            ); 
                            $ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);    
                            # Audit Start
                            $order_tool_data = unsetOrderedToolsExceptionColumns($order_tool_data);
                            $order_tool_data['tool_status'] = "Added";
                            $auditOrderedToolBlock = array("tool_id"=> $tool_id);
                            $auditOrderedToolId = tool_order_audit_data('ordered_tools',$ordered_tool_id,'ordered_tool',2,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$tool_order_id,$oldOrderData['country_id']);
                            # Audit End
                        }
                    }
                }
                else
                {
                    // lock the assets because previously he said he will get from fe/from wh now/also he is asking warehouse guy to lock the assets for his order and ship                
                    // updating and inserting the ordered tool
                    $arr_to_lock = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status<'=>3));
                    $dbs_tools = array();
                    $dbs_odt_id = array();
                    $dbsToolAvailable = array();
                    foreach ($arr_to_lock as $key => $value) {
                        $dbs_tools[$value['tool_id']] = $value['quantity'];
                        $dbsToolAvailable[$value['tool_id']] = $value['available_quantity'];
                        $dbs_odt_id[$value['tool_id']] = $value['ordered_tool_id'];
                    }
                    // updating old tools quantity and unlocking the tool which is removed from 
                    foreach ($dbs_tools as $tool_id => $quantity) {                        
                        if(array_key_exists($tool_id,$_POST['quantity']))
                        {
                            // tool previously also available then update the qty                              
                            $quantity = $_POST['quantity'][$tool_id];
                            $available_quantity = $dbsToolAvailable[$tool_id];
                            $ordered_tool_id = $dbs_odt_id[$tool_id];
                            # checking for Audit Entry
                            if( !( ($old_fe_check == 0) && ($fe_check == 0) && ($dbs_tools[$tool_id] == $quantity) && ($quantity == $available_quantity) ) )
                            {
                                # Audit Start
                                $oldOrderToolData = array(
                                    'quantity'              => $dbs_tools[$tool_id],
                                    'available_quantity'    => $dbsToolAvailable[$tool_id]
                                );
                                $newOrderToolData = array(
                                    'quantity'              => NULL,
                                    'available_quantity'    => NULL,
                                    "tool_status"           => "Updated"
                                );
                                $finalArr  = array_diff_assoc($newOrderToolData, $oldOrderToolData);
                                $auditOrderedToolBlock = array('tool_id'=>$tool_id);
                                $auditOrderedToolId = tool_order_audit_data('ordered_tools',$tool_id,'tool',2,$ad_id,$finalArr,$auditOrderedToolBlock,$oldOrderToolData,'','',array(),"tool_order",$tool_order_id,$oldOrderData['country_id']);                            
                                # Audit End

                                $updateData = array('quantity'=>$quantity,'status'=>2);
                                $updateWhere = array('ordered_tool_id'=>$ordered_tool_id);
                                $this->Common_model->update_data('ordered_tool',$updateData,$updateWhere);

                                
                                // need to lock till quantity
                                if($quantity > $available_quantity)
                                {
                                    $searchParams = array(
                                        'wh_id'     => $wh_id,
                                        'tool_id'   => $tool_id
                                    );
                                    $available_assets = available_assets($searchParams);
                                    $available_assets_count = count($available_assets);
                                    // sending request to admin if any of the quantity is not fullfilled by assigned warehouse
                                    $required_qty  = ($quantity - $available_quantity) ;
                                    if(@$available_assets_count < $required_qty) $lock_current_stage_id = 4;

                                    // looping all the assets and assigning to lock In period on basis of First Come First Locked In till Ordered Quantity
                                    if($available_assets_count > 0 && $required_qty >0)
                                    {
                                        // upadting the ordered tool data with quantity and status
                                         // lock the assets
                                        foreach ($available_assets as $key => $value)
                                        {
                                            $available_quantity++; 
                                            // updating asset table 
                                            $asset_main_status = $value['asset_main_status'];
                                            $asset_actual_status = ($value['asset_main_status']==1)?2:$asset_main_status;
                                            
                                            # Audit Start
                                                $auditAssetRemarks = "Locked for ".$oldOrderData['order_number'];
                                                assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"locked_assets",2,$auditOrderedToolId,$remarks=NULL,$trans_ad_id=NULL,$oldOrderData['country_id']);
                                                assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,$oldOrderData['country_id']);
                                            # Audit End

                                            # Update Asset
                                            $u_data  = array(
                                                'status'=>$asset_actual_status,
                                                'modified_by'=>$this->session->userdata('sso_id'),
                                                'modified_time'=>date('Y-m-d H:i:s')                                            
                                            );
                                            $where = array('asset_id'=>$value['asset_id']);
                                            $this->Common_model->update_data('asset',$u_data,$where);

                                            # updating Asset status history
                                            $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
                                            $u_where = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
                                            $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);

                                            # inserting into asset history
                                            $asset_status_history_data = array(
                                                    'status'            => $asset_actual_status,
                                                    'asset_id'          => $value['asset_id'],
                                                    'tool_order_id'     => $tool_order_id,
                                                    'ordered_tool_id'   => $ordered_tool_id,
                                                    'current_stage_id'  => 5,
                                                    'created_by'        => $this->session->userdata('sso_id'),
                                                    'created_time'      => date('Y-m-d H:i:s'));
                                            $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);

                                            if($available_quantity == $quantity) break;
                                        }                        
                                        $status = ($available_quantity == $quantity)?1:2;                       
                                        # Updating Ordered Tool             
                                        $updateData = array('status'=>$status,'available_quantity'=>$available_quantity);
                                        $updateWhere = array('ordered_tool_id'=>$ordered_tool_id);
                                        $this->Common_model->update_data('ordered_tool',$updateData,$updateWhere);                                     
                                    }
                                    else
                                    {
                                        $order_tool_data = array(                                
                                            'status'         => 2,                             
                                            'modified_by'    => $this->session->userdata('sso_id'),
                                            'modified_time'  => date('Y-m-d H:i:s')
                                        ); 
                                        $updateWhere = array('ordered_tool_id'=> $ordered_tool_id);
                                        $this->Common_model->update_data('ordered_tool',$order_tool_data,$updateWhere);
                                    }
                                }
                                // need to unlock till auantity
                                else
                                {
                                    $main_needed_locked_assets = $available_quantity - $quantity;
                                    $where = array('ordered_tool_id'=>$ordered_tool_id);                                
                                    // list of required to unlock has already completed so no need to check below
                                    if($main_needed_locked_assets !=0)
                                    {  
                                        $locked_assets = $this->Order_m->get_locked_assets($ordered_tool_id,$available_quantity);
                                        
                                        if($locked_assets)
                                        {
                                            foreach ($locked_assets as $key => $value)
                                            {
                                                $asset_main_status = $this->Common_model->get_value('asset',array('asset_id'=>$value['asset_id']),'status');                                                
                                                $asset_actual_status = ($asset_main_status ==2)?1:$asset_main_status;

                                                 # Audit Start
                                                $auditAssetRemarks  = "Unlocked for ".$oldOrderData['order_number'];
                                                assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"locked_assets",2,$odtAndAuditOdt[$ordered_tool_id],$remarks=NULL,$trans_ad_id=NULL,$oldOrderData['country_id']);
                                                assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,$oldOrderData['country_id']);
                                                # Audit End

                                                # Update Asset
                                                $updateData = array(
                                                    'status'        => $asset_actual_status,
                                                    'modified_by'   => $this->session->userdata('sso_id'),
                                                    'modified_time' => date('Y-m-d H:i:s')
                                                );
                                                $updateWhere = array('asset_id'=>$value['asset_id']);
                                                $this->Common_model->update_data('asset',$updateData,$updateWhere);

                                                # Update Asset status history
                                                $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
                                                $u_where = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
                                                $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);     
                                                
                                                # Insert asset status history
                                                $asset_status_history_data = array(
                                                        'status'            => $asset_actual_status,
                                                        'asset_id'          => $value['asset_id'],
                                                        'tool_order_id'     => $tool_order_id,
                                                        'ordered_tool_id'   => $ordered_tool_id,
                                                        'current_stage_id'  => 4,
                                                        'created_by'        => $this->session->userdata('sso_id'),
                                                        'created_time'      => date('Y-m-d H:i:s'));
                                                $this->Common_model->insert_data('asset_status_history',$asset_status_history_data); 
                                                // eventhogh too many tools are unlocked thats not a problem because he can order for some other and can intimated to admin
                                                // decide weather this incriment can put inside or out side
                                                $main_needed_locked_assets--;             
                                                if($main_needed_locked_assets == 0)
                                                    break;
                                            }
                                        }                                    
                                    }
                                    // updating the available quantity and status even though some of the assets not unlinked
                                    $this->Common_model->update_data('ordered_tool',array('available_quantity'=>$quantity,'status'=>1),array('ordered_tool_id'=>$ordered_tool_id));
                                    # For Audit updating the available quantity
                                    $available_quantity = $quantity;
                                } 
                                # Audit Start
                                $auditUpdateData = array(
                                    'quantity'              => $quantity,
                                    'available_quantity'    => $available_quantity
                                );
                                updatingAuditData($auditOrderedToolId,$auditUpdateData);
                                # Audit End
                            }
                        }
                        else
                        {
                            $auditOrderedToolId = unlockingByToolOrderId($tool_order_id,0,0,$dbs_odt_id[$tool_id],$ad_id,3);
                        }
                    }

                    // inserting newly added tools only
                    foreach($_POST['quantity'] as $key => $value) {
                        if(!array_key_exists($key, $dbs_tools))
                        {
                            $order_tool_data = array(
                                    'tool_order_id'      => $tool_order_id,
                                    'tool_id'            => $key,
                                    'quantity'           => $value,
                                    'available_quantity' => 0,                                    
                                    'status'             => 2,
                                    'created_by'         => $this->session->userdata('sso_id'),
                                    'created_time'       => date('Y-m-d H:i:s')); 
                            $ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);  
                            # Audit Start
                            $order_tool_data = unsetOrderedToolsExceptionColumns($order_tool_data);
                            $order_tool_data['tool_status'] = "Added";
                            $auditOrderedToolBlock = array("tool_id"=> $key);
                            $auditOrderedToolId = tool_order_audit_data('ordered_tools',$ordered_tool_id,'ordered_tool',2,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$tool_order_id,$oldOrderData['country_id']);

                            $quantity = $value;
                            $tool_id = $key;
                            $available_quantity = 0;
                            if($quantity > $available_quantity)
                            {
                                $searchParams = array(
                                    'wh_id'     => $wh_id,
                                    'tool_id'   => $tool_id
                                );
                                $available_assets = available_assets($searchParams);
                                $available_assets_count = count($available_assets);
                                // sending request to admin if any of the quantity is not fullfilled by assigned warehouse
                                $required_qty  = ($quantity - $available_quantity) ;
                                if(@$available_assets_count < $required_qty) $lock_current_stage_id = 4;

                                // looping all the assets and assigning to lock In period on basis of First Come First Locked In till Ordered Quantity
                                if($available_assets_count > 0 && $required_qty >0)
                                {
                                    
                                    // upadting the ordered tool data with quantity and status
                                     // lock the assets
                                    foreach ($available_assets as $key => $value)
                                    {
                                        $available_quantity++; 
                                        // updating asset table 
                                        $asset_main_status = $value['asset_main_status'];
                                        $asset_actual_status = ($value['asset_main_status']==1)?2:$asset_main_status;                                        

                                        # Audit Start
                                            $auditAssetRemarks = "Locked for ".$oldOrderData['order_number'];
                                            assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"locked_assets",2,$auditOrderedToolId,$remarks=NULL,$trans_ad_id=NULL,$oldOrderData['country_id']);
                                            assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,$oldOrderData['country_id']);
                                        # Audit End

                                        # Update Asset
                                        $u_data  = array(
                                            'status'=>$asset_actual_status,
                                            'modified_by'=>$this->session->userdata('sso_id'),
                                            'modified_time'=>date('Y-m-d H:i:s')                                            
                                        );
                                        $where = array('asset_id'=>$value['asset_id']);
                                        $this->Common_model->update_data('asset',$u_data,$where);

                                        // inserting into asset history
                                        $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
                                        $u_where = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
                                        $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);
                                        # Updating into asset status history
                                        $asset_status_history_data = array(
                                                'status'            => $asset_actual_status,
                                                'asset_id'          => $value['asset_id'],
                                                'tool_order_id'     => $tool_order_id,
                                                'ordered_tool_id'   => $ordered_tool_id,
                                                'current_stage_id'  => 5,
                                                'created_by'        => $this->session->userdata('sso_id'),
                                                'created_time'      => date('Y-m-d H:i:s'));
                                        $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);

                                        if($available_quantity == $quantity) break;
                                    } 

                                    $status = ($available_quantity == $quantity)?1:2;                                    
                                    $updateData = array('status'=>$status,'available_quantity'=>$available_quantity);
                                    $updateWhere = array('ordered_tool_id'=>$ordered_tool_id);
                                    $this->Common_model->update_data('ordered_tool',$updateData,$updateWhere);                  
                                }
                                else
                                {
                                    $order_tool_data = array(                                
                                        'status'           => 2,                             
                                        'modified_by'      => $this->session->userdata('sso_id'),
                                        'modified_time'    => date('Y-m-d H:i:s')
                                    ); 
                                    $updateWhere = array('ordered_tool_id'=> $ordered_tool_id);
                                    $this->Common_model->update_data('ordered_tool',$order_tool_data,$updateWhere);
                                } 

                            }
                            # Audit Start
                            $auditUpdateData = array(
                                'available_quantity'    => $available_quantity
                            );
                            updatingAuditData($auditOrderedToolId,$auditUpdateData);
                            # Audit End
                        }
                    }
                }
            }
            else   // warehouse is been changed incase from old warehouse to new warehouse
            {
                // need to lock unlock the all assets from warehouse and lock the new assets

                unlockingByToolOrderId($tool_order_id,0,0,0,$ad_id,1,1);
                $ordered_tools = array();
                foreach ($_POST['quantity'] as $tool_id => $qty)
                {
                    $ordered_tools[$tool_id] = $qty;
                }
                foreach ($ordered_tools as $tool_id => $quantity)
                {
                    $searchParams = array(
                        'wh_id'     => $wh_id,
                        'tool_id'   => $tool_id
                    );
                    $available_assets = available_assets($searchParams);
                    $available_assets_count = count($available_assets);
                    // sending request to admin if any of the quantity is not fullfilled by assigned warehouse
                    if(@$available_assets_count < $quantity) $current_stage_id = 4;
                    // looping all the assets and assigning to lock In period on basis of First Come First Locked In till Ordered Quantity
                    if(count($available_assets_count) > 0)
                    {
                        $available_quantity = 0;

                        $order_tool_data = array(
                            'tool_order_id'      => $tool_order_id,
                            'tool_id'            => $tool_id,
                            'available_quantity' => $available_quantity,
                            'quantity'           => $quantity,
                            'status'             => 1,
                            'created_by'         => $this->session->userdata('sso_id'),
                            'created_time'       => date('Y-m-d H:i:s')); 
                        $ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$order_tool_data);
                        # Audit Start
                        $order_tool_data = unsetOrderedToolsExceptionColumns($order_tool_data);
                        $order_tool_data['tool_status'] = "Added";
                        $auditOrderedToolBlock = array("tool_id"=> $tool_id);
                        $auditOrderedToolId = tool_order_audit_data('ordered_tools',$ordered_tool_id,'ordered_tool',1,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$tool_order_id,$oldOrderData['country_id']);
                        # Audit End
                        // don't lock any assets if that order checked for fe check 
                        if($fe_check!=1)
                        {
                            // lock the assets
                            foreach ($available_assets as $key => $value)
                            {
                                $available_quantity++; 
                                // updating asset table 
                                $asset_main_status = $value['asset_main_status'];
                                $asset_actual_status  = ($value['asset_main_status']==1)?2:$asset_main_status;

                                # Audit Start
                                $auditAssetRemarks = "Locked for ".$order_number;
                                assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"locked_assets",2,$auditOrderedToolId,$remarks=NULL,$trans_ad_id=NULL,$oldOrderData['country_id']);
                                assetStatusAudit($value['asset_id'],$asset_main_status,$asset_actual_status,"asset_master",2,"",$auditAssetRemarks,$ad_id,$oldOrderData['country_id']);
                                # Audit End

                                $where = array('asset_id'=>$value['asset_id']);
                                $u_data  = array('status'=>$asset_actual_status);
                                $this->Common_model->update_data('asset',$u_data,$where);

                                // inserting into asset history
                                $u_ash_data = array('end_time'=>date('Y-m-d H:i:s'));
                                $u_where = array('asset_id'=>$value['asset_id'],'end_time'=>NULL);
                                $this->Common_model->update_data('asset_status_history',$u_ash_data,$u_where);

                                $asset_status_history_data = array(
                                'status'           => $asset_actual_status,
                                'asset_id'         => $value['asset_id'],
                                'tool_order_id'    => $tool_order_id,
                                'ordered_tool_id'  => $ordered_tool_id,
                                'current_stage_id' => 4, // only lock the assets in first stage only
                                'created_by'       => $this->session->userdata('sso_id'),
                                'created_time'     => date('Y-m-d H:i:s'));
                                $this->Common_model->insert_data('asset_status_history',$asset_status_history_data);
                                if($available_quantity == $quantity) break;
                            }  

                            # Audit Start
                            $auditUpdateData = array('available_quantity'=> $available_quantity);
                            updatingAuditData($auditOrderedToolId,$auditUpdateData);
                            # Audit End
                            $status = ($available_quantity == $quantity)?1:2;
                        }
                        // unlocking and updating the status to 2
                        else
                        {
                            $status = 2;
                        }
                        $this->Common_model->update_data('ordered_tool',array('status'=>$status,'available_quantity'=>$available_quantity),array('ordered_tool_id'=>$ordered_tool_id));                        
                        if($status == 2) $current_stage_id = 4;                         
                    }
                    else
                    {
                        $order_tool_data = array(
                            'tool_order_id'      => $tool_order_id,
                            'tool_id'            => $tool_id,
                            'available_quantity' => 0,
                            'quantity'           => $quantity,
                            'status'             => 2, 
                            'created_by'         => $this->session->userdata('sso_id'),
                            'created_time'       => date('Y-m-d H:i:s')); 
                        $ordered_tool_id = $this->Common_model->insert_data('ordered_tool',$ordered_tool_data);
                        # Audit Start
                        $order_tool_data = unsetOrderedToolsExceptionColumns($order_tool_data);
                        $order_tool_data['tool_status'] = "Added";
                        $auditOrderedToolBlock = array("tool_id"=> $tool_id);
                        $auditOrderedToolId = tool_order_audit_data('ordered_tools',$tool_id,'tool',1,$ad_id,$order_tool_data,$auditOrderedToolBlock,array(),'','',array(),"tool_order",$tool_order_id,$oldOrderData['country_id']);
                        # Audit End
                    }
                }

                $this->Common_model->update_data('tool_order',array('current_stage_id'=>$current_stage_id),array('tool_order_id'=>$tool_order_id));

                $order_status_history_data = array(
                    'current_stage_id'  => $current_stage_id,
                    'tool_order_id'     => $tool_order_id,
                    'remarks'           => '',
                    'created_by'        => $this->session->userdata('sso_id'),
                    'created_time'      => date('Y-m-d H:i:s'),
                    'status'            =>  1, 
                    );
                $this->Common_model->insert_data('order_status_history',$order_status_history_data);
                $f_current_stage_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'current_stage_id');

            }
            $f_check_address = $this->Common_model->get_value('order_address',array('tool_order_id'=>$tool_order_id),'check_address');
            if($f_check_address == 1 && getUserStatus() == 1)
               sendAddressChangeNotification($tool_order_id);  // uncomment againn

            $orderedTools = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$tool_order_id,'status'=>2));
            // tools not found in didicated warehouse
            if(count($orderedTools) > 0)
            {
                $current_stage_id = 4; 
                $osh_data  = $this->Common_model->get_data('order_status_history',array('tool_order_id'=> $tool_order_id));
                if(count($osh_data) > 0 )
                {
                    //update and insert
                    $u_data = array('end_time'=>date('Y-m-d H:i:s'));
                    $u_where = array('tool_order_id'=>$tool_order_id,'end_time'=>NULL);
                    $this->Common_model->update_data('order_status_history',$u_data,$u_where);
                }
                
                $order_status_history_data = array(
                        'current_stage_id'  => $current_stage_id,
                        'tool_order_id'     => $tool_order_id,
                        'remarks'           => '',
                        'created_by'        => $this->session->userdata('sso_id'),
                        'created_time'      => date('Y-m-d H:i:s'),
                        'status'            =>  1, 
                        );
                 $this->Common_model->insert_data('order_status_history',$order_status_history_data);
                 if($fe_check == 0 && getUserStatus($mainSSO) == 1)
                 {
                    sendToolsNotAvailableNotification($tool_order_id);  // uncomment again
                 }
                    
            }
            else
            {
               
                // adddress chage and fe check both are disabled condition check and tools available condition
                
                if($f_check_address == 1 || $fe_check == 1 || count($orderedTools) > 0)
                {   
                    $current_stage_id = 4;
                }
                else
                {
                    $current_stage_id = 5;
                }

                $osh_data  = $this->Common_model->get_data('order_status_history',array('tool_order_id'=> $tool_order_id));
                if(count($osh_data) > 0 )
                {
                    //update and insert
                    $u_data = array('end_time'=>date('Y-m-d H:i:s'));
                    $u_where = array('tool_order_id'=>$tool_order_id,'end_time'=>NULL);
                    $this->Common_model->update_data('order_status_history',$u_data,$u_where);
                }
                
                $order_status_history_data = array(
                        'current_stage_id'  => $current_stage_id,
                        'tool_order_id'     => $tool_order_id,
                        'remarks'           => '',
                        'created_by'        => $this->session->userdata('sso_id'),
                        'created_time'      => date('Y-m-d H:i:s'),
                        'status'            =>  1, 
                        );
                 $this->Common_model->insert_data('order_status_history',$order_status_history_data);                          
            }

            // finally updating the tool order status and order status histoty
            $this->Common_model->update_data('tool_order',array('current_stage_id'=>$current_stage_id),array('tool_order_id'=>$tool_order_id));

            # Audit Start
            $statusArr = array('current_stage_id'=>$current_stage_id,'tool_order_id'=>$tool_order_id);
            $auditUpdateData = array('current_stage'=>getOpenOrderStatus($statusArr));
            updatingAuditData($ad_id,$auditUpdateData);
            # Audit End
            if(getUserStatus($mainSSO) == 1)
            {
                # Send mail to Admins when tool order is updated 
                if(!getTaskPreference("tool_order_mail",$order_data['country_id'])) {
                    sendtoolordernotificationtofeupdate($tool_order_id);
                }        
                # Send mail to FE when tool order is updated
                sendtoolordernotificationtofe($tool_order_id,2);  
            }

            // sending the mails to warhouse when status is at WH
            if($current_stage_id == 5 && getUserStatus($mainSSO) == 1)  // uncomment again
            {
                sendtoolordernotificationtofeupdate($tool_order_id,1,$order_data['wh_id']); // Update mail to Logistic
            }
            
            if($this->db->trans_status() === FALSE)
            { 
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.</div>'); 
                redirect(SITE_URL.'raise_order'); exit();
            }
            else
            { 
                $this->db->trans_commit();
                unsetToolOrderSessions();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-check"></i></div>
                    <strong>Success!</strong> Order No : <strong>'.$order_number.'</strong> Details has been Updated successfully!</div>');
                redirect(SITE_URL.'open_order'); exit();
            }
        } 
        else
        {
            redirect(SITE_URL.'raise_order'); exit();
        }     
    }

    // Closed Orders
    public function cancel_order()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id =='')
        {
            redirect(SITE_URL.'open_order');
            exit;
        } 
        $orderInfo = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $this->db->trans_begin();
        $return_data = checkMultipleReturnsForFE2Order($tool_order_id);
        if(count($return_data)>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Sorry!</strong> Tool Order '.$return_data['fe1_order_number'].' has been already raised for your Tool Order '.$return_data['fe2_order_number'].' Which is In Progress. </div>'); 
            redirect(SITE_URL.'open_order'); exit();
        }
        # Audit Start
        $oldStatus = $this->Common_model->get_value('current_stage',array('current_stage_id'=>$orderInfo['current_stage_id']),'name');
        $oldData = array(
            'current stage'  => $oldStatus
        );
        $newData = array(
            'current stage'  => "Cancelled"
        );
        $finalArr = array_diff_assoc($newData, $oldData);

        $blockArr = array("blockName"=> "Tool Order");
        $ad_id = tool_order_audit_data('tool_order',$tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$oldData,'','',array(),"tool_order",$tool_order_id,$orderInfo['country_id']);    
        unlockingByToolOrderId($tool_order_id,0,0,0,$ad_id,3);
        # Audit End

        $wh_data = array(
            'status'             => 4,
            'current_stage_id'   => 10,
            'modified_by'        => $this->session->userdata('sso_id'),
            'modified_time'      => date('Y-m-d H:i:s'),
            'remarks'            => 'Cancelled The Order'
        );
        $this->Common_model->update_data('tool_order',$wh_data,array('tool_order_id'=>$tool_order_id));
        $order_number  = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'order_number');
        $insertData = array(
            'status'            => 1,
            'created_by'        => $_SESSION['sso_id'],
            'current_stage_id'  => 10,
            'remarks'           => 'Cancelled The Order',
            'tool_order_id'=>$tool_order_id
        );
        $this->Common_model->insert_data('order_status_history',$insertData);
        if($this->db->trans_status() === FALSE)
        { 
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'open_order'); exit();
        }
        else
        { 
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Order has been Cancelled successfully With Order Number: '.$order_number.'  </div>');
            redirect(SITE_URL.'closed_order'); exit();
        }
    }
    // Closed Orders
    public function closed_order()
    {
        /*$a =  base_url(uri_string()); echo $a;exit;*///echo '<pre>';print_r($_POST);exit;        //echo $_SERVER['REQUEST_URI'];exit;        //echo $_GET['link'];exit;
        $data['nestedView']['heading']="Closed Orders";
        $data['nestedView']['cur_page'] = 'closed_order';
        $data['nestedView']['parent_page'] = 'closed_order';
        $task_access = page_access_check('raise_order');

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Orders';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Orders','class'=>'active','url'=>'');

        # Search Functionality
        $psearch= $this->input->post('closed_order',TRUE);
        if($psearch!='') 
        {
            $searchParams=array(
            'order_number'     => validate_string($this->input->post('order_number', TRUE)),
            'deploy_date'      => validate_string($this->input->post('deploy_date', TRUE)),
            'return_date'      => validate_string($this->input->post('return_date', TRUE)),
            'closed_fe_sso_id' => validate_string($this->input->post('closed_fe_sso_id', TRUE)),
            'status'           => validate_number($this->input->post('status', TRUE)),
            'co_country_id'    => validate_number($this->input->post('co_country_id', TRUE)),
            'order_delivery_type_id' => validate_number($this->input->post('order_delivery_type_id', TRUE)));
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order_number'     => $this->session->userdata('order_number'),
                'deploy_date'      => $this->session->userdata('deploy_date'),
                'return_date'      => $this->session->userdata('return_date'),
                'closed_fe_sso_id' => $this->session->userdata('closed_fe_sso_id'),
                'status'           => $this->session->userdata('status'),
                'co_country_id'    => $this->session->userdata('co_country_id'),
                'order_delivery_type_id' => $this->session->userdata('order_delivery_type_id')
                );
            }
            else 
            {
                $searchParams=array(
                'order_number'           => '',
                'order_delivery_type_id' => '',
                'deploy_date'            => '',
                'return_date'            => '',
                'closed_fe_sso_id'       => '',
                'status'                 => '',
                'co_country_id'          => '' 
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $searchParams['status'] =($searchParams['status'] =='')?10:$searchParams['status'];
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_order/';

        # Total Records
        $config['total_rows'] = $this->Order_m->closed_order_total_num_rows($searchParams,$task_access);
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Order_m->closed_order_results($current_offset, $config['per_page'], $searchParams,$task_access);
        
        # Additional data
        $final_results = array();
        if(count($data['orderResults']) > 0)
        {
            foreach ($data['orderResults'] as $key => $value) 
            {
                $final_results[$value['tool_order_id']] = $this->Order_m->getReturnsForOrder($value['tool_order_id']);
            }
        }
        $data['final_results'] = $final_results;
        $data['current_stage'] = $this->Common_model->get_data('current_stage',array('workflow_type'=>2,'current_stage_id!='=>10));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));        
        $data['task_access'] = $task_access;
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1));
        $data['displayResults'] = 1;

        $this->load->view('order/closed_order',$data);

    }
    public function viewFeClosedReturnDetails()
    {
        $return_order_id = storm_decode($this->uri->segment(2));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'closed_fe_returns'); exit();
        }

        $data['nestedView']['heading']="View FE Closed Return Details";
        $data['nestedView']['cur_page'] = "closed_order";
        $data['nestedView']['parent_page'] = 'closed_order';

        # include files
        $data['nestedView']['js_includes'] = array();
        
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'View FE Closed Return Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Order','class'=>'','url'=>SITE_URL.'closed_order');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'View FE Closed  Return Details','class'=>'active','url'=>'');

        $return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
        {
            $trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['ro_to_wh_id']));
            $trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
        }
        else 
        {
            $trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
            $sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
            $sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
            $trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];
        }

        $trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');

        $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
        $rrow = $this->Order_m->get_fe_closed_return_details($return_order_id,$rto_id);
        $asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
        foreach ($asset_list as $key => $value) 
        {
            $components = $this->Pickup_point_m->get_asset_components($value['asset_id'],$rrow['order_status_id']);
            $asset_list[$key]['components'] = $components;
        }
        #to select 9th stage value
        $os_id = $rrow['order_status_id'];
        $order_asset = $this->Common_model->get_data('order_asset_history',array('order_status_id'=>$os_id));
        $asset_val = array();
        foreach ($order_asset as $row) 
        {
            $oahealth = $this->Common_model->get_data('order_asset_health',array('oah_id'=>$row['oah_id']));
            $assetid = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$row['ordered_asset_id']),'asset_id');
            foreach ($oahealth as $key => $value) 
            {
                $asset_val[$assetid]['asset_status'] = $row['status'];
                $asset_val[$assetid][$value['part_id']]['asset_condition_id'] = $value['asset_condition_id'];
                $asset_val[$assetid][$value['part_id']]['remarks'] = $value['remarks'];

            }
        }
        
        $data['flg'] = 1;
        $data['rrow'] = $rrow;
        $data['trrow'] = $trrow;
        $data['asset_list'] = $asset_list;
        $data['asset_val'] = $asset_val;
        $data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));
        $this->load->view('order/closed_order',$data);
    }

    public function deactivate_attached_order_rec()
    {
        $order_doc_id = validate_string($this->input->post('asset_doc_id',TRUE));
        $update_data = array('status'=>2);
        $update_where = array('order_doc_id'=>$order_doc_id);
        $res = $this->Common_model->update_data('order_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function activate_attached_order_rec()
    {
        $order_doc_id = validate_string($this->input->post('asset_doc_id',TRUE));
        $update_data = array('status'=>1);
        $update_where = array('order_doc_id'=>$order_doc_id);
        $res = $this->Common_model->update_data('order_doc',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }
    
    public function receive_order()
    {
        $task_access = page_access_check('receive_order');
        $data['nestedView']['heading']="Ack From WH";
        $data['nestedView']['cur_page'] = 'receive_order';
        $data['nestedView']['parent_page'] = 'receive_order';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Ack From WH';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack From WH','class'=>'active','url'=>'');

        # Search Functionality       
        $psearch=validate_string($this->input->post('receive_order', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'order_number'  => validate_string($this->input->post('order_number', TRUE)),
            'deploy_date'   => validate_string($this->input->post('deploy_date', TRUE)),
            'return_date'   => validate_string($this->input->post('return_date', TRUE)),
            'rec_sso_id'    => validate_string($this->input->post('rec_sso_id', TRUE)),
            'rec_country_id' => validate_string($this->input->post('rec_country_id', TRUE)),
            'order_delivery_type_id' => validate_number($this->input->post('order_delivery_type_id', TRUE)));
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order_number' => $this->session->userdata('order_number'),
                'order_delivery_type_id' => $this->session->userdata('order_delivery_type_id'),
                'deploy_date' => $this->session->userdata('deploy_date'),
                'return_date' => $this->session->userdata('return_date'),
                'rec_sso_id' => $this->session->userdata('rec_sso_id'),
                'rec_country_id' => $this->session->userdata('rec_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'order_number'           => '',
                'order_delivery_type_id' => '',
                'deploy_date'            => '',
                'return_date'            => '',
                'rec_sso_id'             => '',
                'rec_country_id'         => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'receive_order/';

        # Total Records
        $config['total_rows'] = $this->Order_m->receive_order_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $orderResults = $this->Order_m->receive_order_results($current_offset, $config['per_page'], $searchParams,$task_access);

        #stock transfers
        $st_data = array();         
        foreach ($orderResults as $key => $value) 
        {
            if($value['stock_transfer_id']!='')
            {
                $st_data[$value['tool_order_id']] = $this->Stock_transfer_m->stnForOrder($value['tool_order_id'],1);                
            }
        }    
        # Additional data
        $data['st_data'] = $st_data;
        $data['orderResults'] = $orderResults;
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;
        $this->load->view('order/receive_order',$data);
    }

    public function receive_order_details()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'open_order');
            exit;
        }        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Ack From WH Details ";
        $data['nestedView']['cur_page'] = 'receive_order';
        $data['nestedView']['parent_page'] = 'receive_order';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/fe_receive_order.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Ack From WH Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack From WH','class'=>'','url'=>SITE_URL.'receive_order');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack From WH Details','class'=>'active','url'=>'');

        # Additional data
        $getData = array('tool_order_id'=>$tool_order_id,'current_stage_id'=>6);
        $data['order_status_id'] = $this->Common_model->get_value('order_status_history',$getData,'order_status_id');
        $return_assets = $this->Order_m->wh_return_initialted_assets($tool_order_id);
        $return_parts = array();
        foreach ($return_assets as $key => $value) 
        {
            $return_parts[$value['oah_id']]['part_number']       = $value['part_number'];
            $return_parts[$value['oah_id']]['asset_number']      = $value['asset_number'];
            $return_parts[$value['oah_id']]['asset_id']          = $value['asset_id'];
            $return_parts[$value['oah_id']]['asset_status']      = $value['asset_status'];
            $return_parts[$value['oah_id']]['ordered_asset_id']  = $value['ordered_asset_id'];
            $return_parts[$value['oah_id']]['part_description']  = $value['part_description'];
            $return_parts[$value['oah_id']]['health_data'] = $this->Order_m->get_transit_asset_data($value['oah_id']);
        }
        $data['return_assets'] = $return_assets;
        $data['return_parts'] = $return_parts;
        $data['return_info'] = $this->Order_m->get_return_info($tool_order_id);                   

        // list details
        $data['tool_order_id'] = $tool_order_id;
        $data['trow'] = $this->Order_m->get_order_info($tool_order_id);
        $data['drow'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        # Data
        $this->load->view('order/receive_order_details',$data);
    }

    public function insert_fe_received_order()
    {
        $this->db->trans_begin();    
        # Post data    
        $oah_condition_id = $this->input->post('oah_condition_id',TRUE);
        $oa_health_id_arr = $this->input->post('oa_health_id',TRUE);
        $remarks_arr = $this->input->post('remarks',TRUE);
        $order_status_id = validate_number($this->input->post('order_status_id',TRUE));
        $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
        $order_number = validate_string($this->input->post('order_number',TRUE));
        $oah_oa_health_id_part_id = $this->input->post('oah_oa_health_id_part_id',TRUE);
        $oah_id_arr = $this->input->post('oah_id',TRUE); 

        # Order information
        $orderInfo = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $ordered_sso_id = $orderInfo['sso_id'];
        $country_id = $orderInfo['country_id'];
        # Audit Start
        $oldArr = array(
            'current_stage_id'  => $orderInfo['current_stage_id']
        );
        $newArr = array(
            'current_stage_id'  => 7
        );
        $blockArr = array( "blockName" => "Order Acknowledgement");
        $ad_id = tool_order_audit_data("tool_order_acknowledgement",$orderInfo['tool_order_id'],"tool_order",1,'',$newArr,$blockArr,$oldArr,'','',array(),"tool_order",$tool_order_id,$country_id);
        # Audit End
        $k=0; $j=0; $unsuccessful = 0;
        foreach ($oah_id_arr as $oah_id => $ordered_asset_id)
        {
            $asset_id = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$ordered_asset_id),'asset_id');
            $assetData = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
            $asset_number = $assetData['asset_number'];
            //updating end time of previous stage
            if($k == 0 )
            {
                $end_time = array('end_time'=>date('Y-m-d H:i:s'));
                $this->Common_model->update_data('order_status_history',$end_time,array('order_status_id'=>$order_status_id)); 
            } 
            // updating the previous asset status 
            // please careful here from post it was 2 but to tell asset status in that transaction we are
            // inserting that status to 3 to say that is missed
            $replaced_status = ($oah_condition_id[$ordered_asset_id] == 2)?3:$oah_condition_id[$ordered_asset_id];
            $u_data = array('status'    => $replaced_status);
            $u_where = array('oah_id'   => $oah_id);
            $this->Common_model->update_data('order_asset_history',$u_data,$u_where);           
            if($k == 0)
            {  
                // inserting new order status
                $new_order_status_history_data = array(
                    'current_stage_id'   => 7,
                    'tool_order_id'      => $tool_order_id,  
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s'),
                    'status'             => 1
                );
                $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);  
                $updateData = array(
                    'current_stage_id'  => 7,
                    'modified_by'       => $this->session->userdata('sso_id'),
                    'modified_time'     => date('Y-m-d H:i:s')
                );
                $this->Common_model->update_data('tool_order',$updateData,array('tool_order_id'=>$tool_order_id));
            }
            if($oah_condition_id[$ordered_asset_id] == 1)
            {
                // inserting fe owned
                // inserting the new record
                $new_asset_history = array(
                    'ordered_asset_id'   => $ordered_asset_id,
                    'order_status_id'    => $new_order_status_id,
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s'),
                    'status'             => $oah_condition_id[$ordered_asset_id]
                );
                $new_oah_id = $this->Common_model->insert_data('order_asset_history',$new_asset_history);
                # Audit Start for transaction asset                
                $old_data_array  = array('transaction_status' => 'Shipped');
                $acd_array = array('transaction_status'  => 'Received');                
                $blockArra = array('asset_id' => $asset_id);

                $auditOahId = tool_order_audit_data("ack_assets",$new_oah_id,"order_asset_history",1,$ad_id,$acd_array,$blockArra,$old_data_array,$orderInfo['order_number'],'',array(),"tool_order",$tool_order_id,$country_id);

                $defective_status = 0;
                foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                {
                    // updating the transist health
                    $part_id = @$value[0];
                    $asset_condition_id = @$oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                    $remarks = @$remarks_arr[$oah_id][$oa_health_id][$part_id];
                    $health_data = array(
                        'asset_condition_id'  => $asset_condition_id,
                        'remarks'             => $remarks
                    );
                    $health_where = array('oa_health_id'=>$oa_health_id);
                    $this->Common_model->update_data('order_asset_health',$health_data,$health_where);

                    // inserting FE Owned
                    $order_asset_health = array(
                        'asset_condition_id' => $asset_condition_id,
                        'part_id'            => $part_id,
                        'oah_id'             => $new_oah_id,
                        'remarks'            => $remarks
                    );
                    $new_oahl_id = $this->Common_model->insert_data('order_asset_health',$order_asset_health);

                    // to tell overall asset status 
                    if($asset_condition_id != 1)
                    {
                        $defective_status = 2;
                        $unsuccessful = 1;
                    }
                }

                # Old Audit Asset and its position
                $oldAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $assetData['status']
                );
                $auditOldAssetData = auditAssetData($oldAssetData);

                $approval = $assetData['approval_status'];
                // if defective alert to admin
                if(@$defective_status == 2)
                { 
                    // ealier not waiting for approval and now need approval
                    if($approval == 0)
                    {
                        // inserting into defective asset 
                        $defective_asset = array(
                            'asset_id'           => $asset_id,
                            'current_stage_id'   => 6,
                            'trans_id'           => $tool_order_id,
                            'sso_id'             => $ordered_sso_id,
                            'created_by'         => $this->session->userdata('sso_id'),
                            'created_time'       => date('Y-m-d H:i:s'),
                            'type'               => 1,
                            'status'             => 2
                        );
                        $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                        // comment here and update the approval status when defective asset request is initiated to handle
                        foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                        { 
                            $part_id = $value[0];
                            $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                            $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];

                            $order_asset_health_data = array(
                                'defective_asset_id'    => $defective_asset_id,
                                'part_id'               => $part_id,
                                'asset_condition_id'    => $asset_condition_id,
                                'remarks'               => $remarks
                            );
                            $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                        } 
                        // send Notification To Admin
                        sendDefectiveOrMissedAssetMail($defective_asset_id,1,$country_id,2,$tool_order_id); 
                    }
                    // uipdate owned asset status
                    $this->Common_model->update_data('order_asset_history',array('status'=>$defective_status),array('oah_id'=>$new_oah_id));
                }

                #if the asset is in our of calibration then don't chnge the asset status to FD and keep it as Out of calibration
                $newAssetStatus = ($assetData['status']==8)?3:$assetData['status'];
                # transaction Updating the Asset
                $updateData = array(
                    'status'              => $newAssetStatus,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s') 
                    );
                if($approval == 0 && @$defective_status ==2)
                    $updateData['approval_status'] = 1;
                $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                # update insert asset position
                $transUpdateInsertPosition = array(
                    'asset_id' => $asset_id,
                    'transit'  => 0,
                    'sso_id'   => $ordered_sso_id,
                    'status'   => $newAssetStatus
                );
                updateInsertAssetPosition($transUpdateInsertPosition);

                # Update insert asset status history
                $assetStatusHistoryArray = array(
                    'asset_id'          => $asset_id,
                    'created_by'        => $this->session->userdata('sso_id'),
                    'status'            => $newAssetStatus,
                    'current_stage_id'  => 7,
                    'tool_order_id'     => $tool_order_id
                );
                updateInsertAssetStatusHistory($assetStatusHistoryArray);

                # Audit Asset start
                $newAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $newAssetStatus
                );
                if($defective_status == 2 || $approval == 1) // old approval required or new approval required
                {
                    $newAssetData['type'] = $defective_status;
                }   
                $auditNewAssetData = auditAssetData($newAssetData,2);
                $auditAssetRemarks = "Received for ".$orderInfo['order_number'];
                assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$orderInfo['country_id']);            
                # Audit Asset End

                $j++;  // to close the order if this value is zero
            }            
            // inserting missed asset
            else
            { 

                $unsuccessful = 1;
                $defective_status = 3;
                $approval = $assetData['approval_status'];

                # Audit Start for transaction asset                
                $auditAssetOldData  = array(
                    'transaction_status'    => "Shipped"
                );
                $auditAssetNewData = array(
                    'transaction_status'    =>  "Not Received"
                );
                $blockArr = array("asset_id" => $asset_id);
                $auditOahId = tool_order_audit_data("ack_assets",$oah_id,"order_asset_history",1,$ad_id,$auditAssetNewData,$blockArr,$auditAssetOldData,$orderInfo['order_number'],'',array(),"tool_order",$tool_order_id,$country_id);

                # Audit Old Asset and its position
                $oldAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $assetData['status']
                );
                $auditOldAssetData = auditAssetData($oldAssetData);

                $newAssetStatus = ($assetData['status']==8)?8:$assetData['status'];
                // 0 for not waititng for approval
                if($approval == 0)
                {
                    $defective_asset = array(
                        'asset_id'         => $asset_id,
                        'current_stage_id' => 6,
                        'trans_id'         => $tool_order_id,
                        'sso_id'           => $ordered_sso_id,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'type'             => 2,
                        'status'           => 3
                    );
                    $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                    // update asset status, approval status,asset postion when missed request is handled.
                    // current stage id is useful when doing fetching any repoort
                    # Audit start for defective asset
                    $oldHealth = array();
                    $newHealth = array();
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    { 
                        $part_id = $value[0];
                        $order_asset_health_data = array(
                            'defective_asset_id'    => $defective_asset_id,
                            'part_id'               => $part_id,
                            'asset_condition_id'    => 3,
                            'remarks'               => "Missed"
                        );
                        $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                    }
                    // send Notification To Admin
                    sendDefectiveOrMissedAssetMail($defective_asset_id,2,$country_id,2,$tool_order_id); 
                }
                # transaction Updating the Asset
                $updateData = array(
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s') 
                    );
                if($approval == 0)
                    $updateData['approval_status'] = 1;
                $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                # Audit Asset and its position
                $newAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $assetData['status']
                );
                if($defective_status == 3 || $approval == 1) // old approval required or new approval required
                {
                    $newAssetData['type'] = $defective_status;
                }   
                $auditNewAssetData = auditAssetData($newAssetData,2);
                $auditAssetRemarks = "Missed While Receiving ".$orderInfo['order_number'];
                assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$country_id);            
                # Audit Asset End
                $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$order_status_id)); 
            }
            $k++; // to insert into order status history only for first time
        }

        // updating FE owned stage        
        if(@$unsuccessful == 1 && $j > 0)
            $this->Common_model->update_data('order_status_history',array('status'=>$defective_status),array('order_status_id'=>$new_order_status_id));
        
        // all the acknowledged assets not received
        if($j == 0)
        {
            // Updating the Tool Order
            $updateDataOrder = array(
                'current_stage_id'  => 10,
                'status'            => 10,
                'modified_by'       => $_SESSION['sso_id'],
                'modified_time'     => date('Y-m-d H:i:s'),
                'remarks'           => "As FE not received the any tools, Tool Order closed" // newly added this remarks on 25thDec18
            );
            $this->Common_model->update_data('tool_order',$updateDataOrder,array('tool_order_id'=>$tool_order_id));
            // inserting new order status
            $end_time = array('end_time'=>date('Y-m-d H:i:s'));
            $this->Common_model->update_data('order_status_history',$end_time,array('order_status_id'=>$new_order_status_id)); 

            $new_order_status_history_data = array(
                'current_stage_id'   => 10,
                'tool_order_id'      => $tool_order_id,  
                'created_by'         => $this->session->userdata('sso_id'),
                'created_time'       => date('Y-m-d H:i:s'),
                'status'             => 2
            );
            $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);  
            $auditUpdateData = array('current_stage_id'=>10);
            updatingAuditData($ad_id,$auditUpdateData);
        }

        if($this->db->trans_status() === FALSE)
        { 
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'receive_order');  
        }
        else
        { 
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <div class="icon"><i class="fa fa-check"></i></div>
                                <strong>Success!</strong> Order No: <strong>'.$order_number.'</strong> has been Acknowledged successfully!
                             </div>');
            redirect(SITE_URL.'receive_order');  
        } 
    }

    public function closed_receive_order()
    {
        $data['nestedView']['heading']="Closed FE Ack From WH";
        $data['nestedView']['cur_page'] = 'closed_receive_order';
        $data['nestedView']['parent_page'] = 'closed_receive_order';
        $task_access = page_access_check('receive_order');
        $data['task_access'] = $task_access;
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed FE Ack From WH';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Ack From WH','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('receive_order', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'order_number'    => validate_string($this->input->post('order_number', TRUE)),
            'deploy_date'     => validate_string($this->input->post('deploy_date', TRUE)),
            'return_date'     => validate_string($this->input->post('return_date', TRUE)),
            'ack_from'        => validate_string($this->input->post('ack_from', TRUE)),
            'rec_sso_id'     => validate_string($this->input->post('crec_sso_id', TRUE)),
            'crec_country_id' => validate_string($this->input->post('crec_country_id', TRUE)),
            'order_delivery_type_id'  => validate_number($this->input->post('order_delivery_type_id', TRUE)));
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order_number'           => $this->session->userdata('order_number'),
                'order_delivery_type_id' => $this->session->userdata('order_delivery_type_id'),
                'deploy_date'            => $this->session->userdata('deploy_date'),
                'return_date'            => $this->session->userdata('return_date'),
                'ack_from'               => $this->session->userdata('ack_from'),
                'rec_sso_id'            => $this->session->userdata('rec_sso_id'),
                'crec_country_id'        => $this->session->userdata('crec_country_id'),
                );
            }
            else 
            {
                $searchParams=array(
                'order_number'           => '',
                'order_delivery_type_id' => '',
                'deploy_date'            => '',
                'return_date'            => '',
                'ack_from'               => '',
                'crec_country_id'        => '',
                'rec_sso_id'            => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_receive_order/';

        # Total Records
        $config['total_rows'] = $this->Order_m->closed_fe_receive_order_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $orderResults = $this->Order_m->closed_fe_receive_order_results($current_offset, $config['per_page'], $searchParams,$task_access);

        #stock transfers
        $st_data = array(); 
        foreach ($orderResults as $key => $value) 
        {
            if($value['stock_transfer_id']!='')
            {
                $st_data[$value['tool_order_id']] = $this->Stock_transfer_m->stnForOrder($value['tool_order_id'],1);
            }
        }
         
        # Additional data
        $data['displayResults'] = 1;
        $data['orderResults'] = $orderResults;
        $data['st_data'] = $st_data;
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));

        $this->load->view('order/closed_fe_receive_order',$data);

    }
    public function closed_receive_st_fe_order_details()
    {
        //echo count($_SESSION['tool_id']);exit;
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'closed_receive_order');
            exit;
        }
        $data['tool_order_id'] = $tool_order_id;
        //echo $tool_order_id;exit;        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Closed Ack WH Order Details ";
        $data['nestedView']['cur_page'] = 'closed_receive_order';
        $data['nestedView']['parent_page'] = 'closed_receive_order';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/fe_receive_order.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Ack From WH Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack From WH','class'=>'','url'=>SITE_URL.'closed_receive_order');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack From WH Details','class'=>'active','url'=>'');

        # Additional data
                        

        // list details
        $data['trow'] = $this->Order_m->get_order_info($tool_order_id);
        if($data['trow']['order_type'] == 2)
            $order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>7),'order_status_id');
        else
            $order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>3),'order_status_id');            
        $asset_data = $this->Order_m->getAssetDataByOrderStatusID($order_status_id);
        
        $data['drow'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        # Data
        $data['asset_data'] = $asset_data;
        /*$row = $this->Asset_m->parent_asset_details($asset_id);
        $data['lrow'] = $row[0];
        $data['child_parts'] = $this->Asset_m->child_asset_details($asset_id);*/
        //echo '<pre>';print_r($data);exit;
        $this->load->view('order/closed_receive_st_order_details',$data);
    }

    // Raise Pickup
    public function raise_pickup()
    {
        /*$a =  base_ur1l(uri_string()); echo $a;exit;*///echo '<pre>';print_r($_POST);exit;        //echo $_SERVER['REQUEST_URI'];exit;        //echo $_GET['link'];exit;
        $task_access = page_access_check('raise_pickup');
        $data['nestedView']['heading']="Tool Return";
        $data['nestedView']['cur_page'] = 'raise_pickup';
        $data['nestedView']['parent_page'] = 'raise_pickup';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';
        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Tool Return';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Return','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=$this->input->post('raise_pickup', TRUE);
        if($psearch!='') 
        {
            $searchParams=array(
            'order_number' => validate_string($this->input->post('order_number', TRUE)),
            'order_delivery_type_id'  => validate_number($this->input->post('order_delivery_type_id',TRUE)),
            'deploy_date' => validate_string($this->input->post('deploy_date', TRUE)),
            'return_date' => validate_string($this->input->post('return_date', TRUE)),
            'rt_sso_id' => validate_string($this->input->post('rt_sso_id', TRUE)),
            'rt_country_id' => validate_string($this->input->post('rt_country_id', TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'order_number'           => $this->session->userdata('order_number'),
                'order_delivery_type_id' => $this->session->userdata('order_delivery_type_id'),
                'deploy_date'            => $this->session->userdata('deploy_date'),
                'return_date'            => $this->session->userdata('return_date'),
                'rt_sso_id'              => $this->session->userdata('rt_sso_id'),
                'rt_country_id'          => $this->session->userdata('rt_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'order_number'           => '',
                'order_delivery_type_id' => '',
                'deploy_date'            => '',
                'return_date'            => '',
                'rt_sso_id'              => '',
                'rt_country_id'          => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'raise_pickup/';

        # Total Records
        $config['total_rows'] = $this->Order_m->owned_order_total_num_rows($searchParams,$task_access);
        //echo $this->db->last_query();exit;
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Order_m->owned_order_results($current_offset, $config['per_page'], $searchParams,$task_access);

        $owned_tools_list = array();
        foreach ($data['orderResults'] as $key => $value) 
        {
            $data['orderResults'][$key]['owned_tools_list'] = $this->Order_m->getOwnedAssetsDetailsByOrderId($value['tool_order_id']);

        }
        $data['order_type'] = $this->Common_model->get_data('order_delivery_type',array('status'=>1));
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access)); 
        $data['task_access'] = $task_access;
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('order/owned_order',$data);

    }

    #Owned tools
    public function owned_order_details()
    {
        $tool_order_id = @storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'raise_pickup'); exit();
        }

        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading'] = 'Initiate Tool Return';
        $data['nestedView']['cur_page'] = 'raise_pickup_second';
        $data['nestedView']['parent_page'] = 'raise_pickup';

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';        
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/receive_order.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Initiate Tool Return';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Return','class'=>'','url'=>SITE_URL.'raise_pickup');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Initiate Tool Return','class'=>'active','url'=>'');

        #logic
        $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $returnUser = $tool_order_arr['sso_id'];
        $returnUserCountry = $tool_order_arr['country_id'];

        #to select by default in return To WH
        $selected_ordered_to_wh = $tool_order_arr['to_wh_id'];
        if($selected_ordered_to_wh=='')
        {
            if(checkAsean($returnUserCountry,1))
            {
                $selected_ordered_to_wh = $this->Common_model->get_value('user',array('sso_id'=>$returnUser),'wh_id');
            }
            else
            {
                $selected_ordered_to_wh = $tool_order_arr['wh_id'];
            }
        }
        $data['selected_ordered_to_wh'] = $selected_ordered_to_wh;

        #return parts
        $return_parts = array();
        $data['owned_assets'] = $this->Order_m->getOwnedAssetsDetailsByOrderId($tool_order_id);
        foreach ($data['owned_assets'] as $key => $value) 
        {
            $health_data = $this->Order_m->get_transit_asset_data($value['oah_id']);
            $return_parts[$value['oah_id']] = 
            array('part_number'      => $value['part_number'],
                  'asset_number'     => $value['asset_number'],
                  'asset_id'         => $value['asset_id'],
                  'oah_id'           => $value['oah_id'],
                  'ordered_asset_id' => $value['ordered_asset_id'],
                  'part_description' => $value['part_description'],
                  'health_data'      => $health_data
            );
        }
        $data['return_parts'] = $return_parts;

        $conditionRole = getRoleByUser($returnUser);
        $conditionAccess = getPageAccess('raise_pickup',$conditionRole);
        if($conditionAccess == '')
        {
             $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> User Role has changed. Please Check!</div>');  
            redirect(SITE_URL.'raise_pickup'); exit();
        }
        else if($conditionAccess == 1 )
        {
            $data['all_wh_data'] = $this->Common_model->get_data('warehouse',array('country_id'=>$returnUserCountry,'status'=>1));
            $sso_arr = $this->Common_model->get_data_row('user',array('sso_id'=>$returnUser));
            if($conditionRole == 5 || $conditionRole ==6)
            {
                #return from address
                $data['wh_data'] = get_warehouses_under_location($sso_arr['location_id'],0,$returnUser);
            }
            else
            {
                #return from address
                $data['wh_data'] = $this->Common_model->get_data('warehouse',array('wh_id'=>$sso_arr['wh_id']));
            }
        }
        else if($conditionAccess == 2)
        {
            $data['wh_data'] = $this->Common_model->get_data('warehouse',array('country_id'=>$returnUserCountry,'status'=>1));
            $data['all_wh_data'] = $data['wh_data']; 
        }
        else if($conditionAccess == 3)
        {
            $data['wh_data'] = get_whs_string(array('order_sso_id'=>$returnUser),0,$conditionAccess,1);
            $data['all_wh_data'] = $data['wh_data'];
        }

        #ship by address
        if(checkAsean($returnUserCountry,1))
        {
            $data['ship_by_wh_data'] = $data['all_wh_data'];
        }
        else
        {
            $data['ship_by_wh_data'] = $data['wh_data'];
        }

        # Additional data
        $data['tool_order_id'] = $tool_order_id;
        $data['conditionAccess'] = $conditionAccess;
        $data['conditionRole'] = $conditionRole;
        $data['returnUser'] = $returnUser;
        $_SESSION['returnUser'] = $returnUser;
        $data['return_type'] = $this->Common_model->get_data('return_type',array('status'=>1));
        $data['fe1_order_number'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'order_number');
        $data['order_info'] = $this->Order_m->get_order_info($tool_order_id);
        $data['wh_id'] = $tool_order_arr['wh_id'];
        $data['cancel_form_action'] =  SITE_URL.'raise_pickup';
        $data['flg'] = 1;
        $this->load->view('order/owned_order_details',$data);
    }

    #checking order number uniqueness
    public function check_order_number_availability()
    {
        $order_number = validate_string($this->input->post('order_number',TRUE));
        $sso_id = validate_string($this->input->post('sso_id',TRUE));
        $parent_page = validate_string($this->input->post('parent_page',TRUE));
        $transactionCountry = validate_string($this->input->post('transactionCountry',TRUE));
        $keys = $this->input->post('keys',TRUE);        
        $values = $this->input->post('values',TRUE);
        $final_array = array_combine($keys, $values);
        $conditionAccess = getPageAccess($parent_page,getRoleByUser($sso_id));
        echo json_encode(checkOrderNumberAvailability(1,$order_number,$final_array,$transactionCountry));
    }
    
    public function pickup_sso_dropdown()
    {
        $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
        $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $returnUser = $tool_order_arr['sso_id'];
        $returnUserCountry = $tool_order_arr['country_id'];

        $oah_arr = $this->input->post('oah_id',TRUE);
        $tool_arr = array();
        foreach ($oah_arr as $oah_id => $ordered_asset_id)
        {
            $tool_arr[] = $this->Order_m->get_tool_id_by_oa_id($ordered_asset_id);
        }
        $_SESSION['return_tool_arr'] = array_unique($tool_arr);
        $fe_users = $this->Order_m->get_check_with_sso($_SESSION['return_tool_arr'],$returnUserCountry);
        $string = '';
        if(count($fe_users)>0)
        {
            $string.='<option value="">- SSO -</option>';
            foreach ($fe_users as $ot) 
            {
                $string.='<option value="'.$ot['sso_id'].'">'.$ot['sso_id'].'-('.$ot['name'].')</option>';
            }
        }
        else
        {
            $string.="<option value=''>- No Data Found -</option>";
        }
        
        $data = array('result1'=>$string);
        echo json_encode($data);
    }

    public function check_order_matching()
    {
        $type = 1; //client validation
        validate_fe2_order_number($type);
    }

    public function insert_fe_owned_order()
    {
        $tool_order_id = validate_number($this->input->post('tool_order_id',TRUE));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'raise_pickup'); exit();
        }

        #post data
        $install_base_id =validate_number($this->input->post('install_base_id',TRUE));
        $owned_asset_count = validate_number($this->input->post('owned_assets_count',TRUE));
        $order_status_id = validate_number($this->input->post('order_status_id',TRUE));
        $oah_id_with_ordered_asset_id = $this->input->post('oah_id',TRUE);
        $oah_condition_id = $this->input->post('oah_condition_id',TRUE);
        $oa_health_id_arr = $this->input->post('oa_health_id',TRUE);
        $remarks_arr = $this->input->post('remarks',TRUE);
        $oah_oa_health_id_part_id = $this->input->post('oah_oa_health_id_part_id',TRUE); // to get part ID
        $check_address = validate_number($this->input->post('check_address',TRUE));
        $address_remarks = validate_string($this->input->post('address_remarks',TRUE));
        $fe1_order_number = validate_string($this->input->post('fe1_order_number',TRUE));
        $return_type_id = validate_number($this->input->post('return_type_id',TRUE));
        $order_delivery_type_id = validate_number($this->input->post('delivery_type_id',TRUE));
        $to_wh_id = validate_string($this->input->post('to_wh_id',TRUE));
        $from_wh_id = validate_string($this->input->post('from_wh_id',TRUE));
        $zonal_wh_id = validate_string($this->input->post('zonal_wh_id',TRUE));
        $order_number = validate_string($this->input->post('order_number',TRUE));
        $site_id = validate_string($this->input->post('site_id',TRUE));
        $system_id = validate_string($this->input->post('system_id',TRUE));
        $return_order_notes = validate_string(@$this->input->post('return_order_notes',TRUE));


        $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $returnUser = $tool_order_arr['sso_id'];
        $returnUserCountry = $tool_order_arr['country_id'];
        $sso_id = $returnUser;
        $ordered_sso_id = $returnUser;
        $conditionRole = getRoleByUser($returnUser);
        $conditionAccess = getPageAccess('raise_pickup',$conditionRole);

        #checking the condition whether fe2 needed fe1 assets or not
        $type = 2; //server validation
        $m = validate_fe2_order_number($type);

        $this->db->trans_begin();        
        #all posted are missing then order should closed based on, total assets == posted assets and no insertion into return order
        if($m == count($oah_id_with_ordered_asset_id))
        {
           #update the order asset history status and insert in defective asset
            foreach ($oah_id_with_ordered_asset_id as $oah_id => $ordered_asset_id)
            {
                $asset_id = $this->Common_model->get_value('ordered_asset',array('ordered_asset_id'=>$ordered_asset_id),'asset_id');
                $assetData = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id)); 
                $asset_number = $assetData['asset_number'];
                           
                $replaced_status = 3; //missed status
                $u_data = array('status'=>$replaced_status);
                $u_where = array('oah_id'=>$oah_id);
                $this->Common_model->update_data('order_asset_history',$u_data,$u_where);

                # Update insert asset status history

                $assetStatusHistoryArray = array(
                    'asset_id'          => $asset_id,
                    'created_by'        => $this->session->userdata('sso_id'),
                    'status'            => 6,
                    'current_stage_id'  => 7,
                    'tool_order_id'     => $tool_order_id
                );
                updateInsertAssetStatusHistory($assetStatusHistoryArray);

                # Audit Old Asset and its position
                $oldAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $assetData['status']
                );
                $auditOldAssetData = auditAssetData($oldAssetData);

                #inserting missed asset
                $unsuccessful = 1;
                $defective_status = 3;
                $approval = $assetData['approval_status'];
                if($approval == 0)
                {
                    $defective_asset = array(
                        'asset_id'         => $asset_id,
                        'current_stage_id' => 7,
                        'trans_id'         => $tool_order_id,
                        'sso_id'           => $returnUser,
                        'created_by'       => $_SESSION['sso_id'],
                        'created_time'     => date('Y-m-d H:i:s'),
                        'type'             => 2,
                        'status'           => 3
                    );
                    $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);

                    #current stage id is useful when doing fetching any report
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    { 
                        $part_id = $value[0];
                        $order_asset_health_data = array(
                            'defective_asset_id' => $defective_asset_id,
                            'part_id'            => $part_id,
                            'asset_condition_id' => 3,
                            'remarks'            => "Missed"
                        );
                        $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                    }
                    // send Notification To Admin
                    sendDefectiveOrMissedAssetMail($defective_asset_id,2,$returnUserCountry,2,$tool_order_id);  
                }
                # transaction Updating the Asset
                $updateData = array(
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s') 
                    );
                if($approval == 0)
                    $updateData['approval_status'] = 1;
                $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                # Audit Asset and its position
                $newAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $assetData['status'] 
                );
                if($defective_status == 3 || $approval == 1) // old approval required or new approval required
                {
                    $newAssetData['type'] = $defective_status;
                }   
                $auditNewAssetData = auditAssetData($newAssetData,2);
                $remarksString = "Missed at FE after Acknowledgement done for order number".$orderInfo['order_number'];
                assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$remarksString,'',"tool_order",$tool_order_id,$orderInfo['country_id']);            
                # Audit Asset End
            }
        }
        else //insert into return order bcz some of the assets are returned
        {
            if($return_type_id == 1)
            {
                $fe_owned_asset_status = 4;
                $fe2_tool_order_id = NULL;
                $return_approval = 0;
            }
            else if($return_type_id == 2)   
            {
                $fe_owned_asset_status = 5;
                $fe2_tool_order_id = NULL;
                $return_approval=0;
            }
            else if($return_type_id == 3 || $return_type_id == 4)
            {
                $fe2_tool_order_id_dat = $this->Common_model->get_data('tool_order',array('order_number'=>trim($order_number),'current_stage_id'=>4,'country_id'=>$returnUserCountry));
                $fe2_tool_order_id = $fe2_tool_order_id_dat[0]['tool_order_id'];
                
                $fe_owned_asset_status = 6;
                $return_approval = 1; 
            }

            #for asean
            $sso_country = $this->Common_model->get_value('user',array('sso_id'=>$returnUser),'country_id');
            {
                $check_wh_id = $from_wh_id;
                if($returnUserCountry != $sso_country)
                {
                    $from_wh_id = $to_wh_id;
                }
            }

            $wh_acknowledge_flg = 0;
            if($order_delivery_type_id ==1)
            {
                $country_identity = $this->Common_model->get_value('user',array('sso_id'=>$returnUser),'country_id');
                $customer_site_id = $this->Common_model->get_value('install_base',array('install_base_id'=>$install_base_id),'customer_site_id');
                $add_data  = $this->Common_model->get_data_row('customer_site',array('customer_site_id'=>$customer_site_id));
                $location_id = $add_data['location_id'];
                if($check_address == 0)
                {
                    $address1 = $add_data['address1'];
                    $address2 = $add_data['address2']; 
                    $address3 = $add_data['address3']; 
                    $address4 = $add_data['address4']; 
                    $pin_code = $add_data['zip_code']; 
                }
                else
                {                            
                    $address1 = validate_string($this->input->post('ct_address_1',TRUE));
                    $address2 = validate_string($this->input->post('ct_address_2',TRUE));
                    $address3 = validate_string($this->input->post('ct_address_3',TRUE));
                    $address4 = validate_string($this->input->post('ct_address_4',TRUE));
                    $pin_code = validate_string($this->input->post('ct_pin_code',TRUE));
                }
            }
            if($order_delivery_type_id == 2)
            {
                $install_base_id = NULL;
                if(($return_type_id!= 3 && $return_type_id!=4) && ($to_wh_id == $check_wh_id))
                {
                    $wh_acknowledge_flg = 1;  //then direct hand to hand from fe to wh
                }
                $wh_data = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$check_wh_id));
                $address1 = validate_string($wh_data['address1']);
                $address2 = validate_string($wh_data['address2']);
                $address3 = validate_string($wh_data['address3']);
                $address4 = validate_string($wh_data['address4']);
                $pin_code = validate_string($wh_data['pin_code']);
                $location_id = $wh_data['location_id'];
            }
            if($order_delivery_type_id == 3)
            {
                $install_base_id = NULL;
                $address1 = validate_string($this->input->post('address_1',TRUE));
                $address2 = validate_string($this->input->post('address_2',TRUE));
                $address3 = validate_string($this->input->post('address_3',TRUE));
                $address4 = validate_string($this->input->post('address_4',TRUE));
                $pin_code = validate_string($this->input->post('pin_code',TRUE));
            }

            #decide wh_id and to_wh_id
            #inserting into return order table
            if($order_delivery_type_id == 2)
            { 
                $final_to_wh_id = $to_wh_id;     
                $wh_id = $from_wh_id;
            }
            else//if zonal person selects customer or other then FE need to select by which wh need to pick up the tools
            {
                if($conditionAccess==2 || $conditionAccess==3 || $conditionRole==5 || $conditionRole==6)
                {
                    $wh_id = $zonal_wh_id;
                    $final_to_wh_id = $to_wh_id; 
                }
                else
                {
                    $wh_id = $from_wh_id;
                    if($return_type_id == 1 || $return_type_id == 2)
                    {
                        $final_to_wh_id = $to_wh_id;
                    }
                }
            }

            #Return Number Generation
            $number = $this->Order_m->get_return_latest_record($returnUserCountry);
            if(count($number) == 0)
            {
                $f_order_number = "1-00001";
            }
            else
            {
                $num = ltrim($number['return_number'],"RT");                    
                $result = explode("-", $num);
                $running_no = (int)$result[1];
                if($running_no == 99999)
                {
                    $first_num = $result[0]+1;
                    $second_num = get_running_sno_five_digit(1);  
                }
                else
                {
                    $first_num = $result[0];
                    $val = $running_no+1;
                    $second_num = get_running_sno_five_digit($val);
                }
                $f_order_number = $first_num.'-'.$second_num;
            }
            $return_number = "RT".$f_order_number;
            $check_address = ($order_delivery_type_id == 1)?$check_address:0;

            $return_order_data = array(
                'return_approval'       => @$return_approval,
                'return_number'         => $return_number,
                'order_delivery_type_id'=> $order_delivery_type_id,
                'wh_id'                 => @$wh_id,
                'site_id'               => @$site_id,
                'system_id'             => @$system_id,
                'address_check'         => @$check_address,
                'address_remarks'       => @$address_remarks,
                'address1'              => @$address1,
                'address2'              => @$address2,
                'address3'              => @$address3,
                'address4'              => @$address4,
                'install_base_id'       => @$install_base_id,                
                'zip_code'              => @$pin_code,
                'pan_number'            => @$pan_number,
                'gst_number'            => @$gst_number,
                'return_type_id'        => $return_type_id,
                'ro_to_wh_id'           => @$final_to_wh_id,                
                'tool_order_id'         => $fe2_tool_order_id,
                'return_approval'       => @$return_approval,
                'location_id'           => @$location_id,
                'country_id'            => $returnUserCountry,                
                'created_by'            => $this->session->userdata('sso_id'),
                'created_time'          => date('Y-m-d H:i:s'),
                'expected_arrival_date' => date('Y-m-d')                
            );
            if(getTaskPreference("tool_return_notes",$return_order_data['country_id']))
                $return_order_data['return_order_notes'] = $return_order_notes;

            $return_order_id = $this->Common_model->insert_data('return_order',$return_order_data);
            $return_order_data['pickup_point_id'] = $return_order_data['order_delivery_type_id'];
            $return_order_data['shipby_wh_id'] = $return_order_data['wh_id'];
            
            if($return_order_data['order_delivery_type_id'] !=1)
            {
                unset($return_order_data['site_id'],$return_order_data['system_id'],$return_order_data['address_check'],$return_order_data['address_remarks']);
            }
            if($return_order_data['order_delivery_type_id']==1)
            {
                $return_order_data['customer'] = $this->Common_model->get_value_new("customer",array("customer_id"=>$add_data['customer_id']),'concat(name, " - ",customer_number)');
                if($return_order_data['address_check'] != 1) 
                    unset($return_order_data['address_remarks']);
            }
            unset($return_order_data['location_id'],$return_order_data['order_delivery_type_id'],$return_order_data['wh_id'],$return_order_data['tool_order_id'],$return_order_data['install_base_id']);

            if($return_order_data['return_type_id']<3)
            {
                unset($return_order_data['return_approval']);
            }
            else
            {
                $return_order_data['fe2_order_number'] = $order_number;
            }

            # Audit start
            $remarks_str = "Tool Order Return, RT No:".$return_number;
            $blockArr = array('return_order_id'=> $return_order_id);
            $ad_id = tool_order_audit_data('return_order',$return_order_id,'return_order',1,'',$return_order_data,$blockArr,array(),$remarks_str,'',array(),"tool_order",$tool_order_id,$returnUserCountry);

            $return_tool_order_data = array(
                'return_order_id' => $return_order_id,
                'tool_order_id'   => $tool_order_id,
                'created_by'      => $this->session->userdata('sso_id'),
                'created_time'    => date('Y-m-d H:i:s')
            );
            $rto_id = $this->Common_model->insert_data('return_tool_order',$return_tool_order_data);

            #Update order status history
            $end_time = array('end_time'=>date('Y-m-d H:i:s'));
            $this->Common_model->update_data('order_status_history',$end_time,array('order_status_id'=>$order_status_id));
            
            $trans_curr_stage = (@$wh_acknowledge_flg == 1)?9:8;
            $new_osh_data = array(
                'current_stage_id' => $trans_curr_stage,
                'tool_order_id'    => $tool_order_id,  
                'rto_id'           => $rto_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s'),
                'status'           => 1
            );
            $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_osh_data);
            
            foreach ($oah_id_with_ordered_asset_id as $oah_id => $ordered_asset_id)
            {
                $oa_arr = $this->Order_m->get_order_asset_arr($ordered_asset_id);
                $asset_id = $oa_arr['asset_id'];
                $assetData = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
                $asset_number = $assetData['asset_number'];
                           
                $replaced_status = ($oah_condition_id[$ordered_asset_id] == 2)?3:$fe_owned_asset_status;
                $u_data = array('status'=>$replaced_status);
                $u_where = array('oah_id'=>$oah_id);
                $this->Common_model->update_data('order_asset_history',$u_data,$u_where);

                if($oah_condition_id[$ordered_asset_id] == 1)
                {
                    #inserting fe owned
                    $new_asset_history = array(
                        'ordered_asset_id' => $ordered_asset_id,
                        'order_status_id'  => $new_order_status_id,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => $oah_condition_id[$ordered_asset_id]
                    );
                    $new_oah_id = $this->Common_model->insert_data('order_asset_history',$new_asset_history);

                    # Audit Start for transaction asset                
                    $old_data_array  = array('transaction_status' => 'Owned');
                    $acd_array = array('transaction_status'  => 'Return Initiated');                
                    $blockArra = array('asset_id' => $asset_id);

                    $auditOahId = tool_order_audit_data("return_assets",$new_oah_id,"order_asset_history",1,$ad_id,$acd_array,$blockArra,$old_data_array,$tool_order_arr['order_number'],'',array(),"tool_order",$tool_order_id,$returnUserCountry);
                    $defective_status = 0;
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    {
                        $part_id = $value[0];
                        $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                        $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];
                        #updating the transit health
                        
                        $health_data = array(
                            'asset_condition_id' => $asset_condition_id,
                            'remarks'            => $remarks
                        );
                        $health_where = array('oa_health_id'=>$oa_health_id);
                        $this->Common_model->update_data('order_asset_health',$health_data,$health_where);

                        #inserting FE Owned
                        $order_asset_health = array(
                            'asset_condition_id' => $asset_condition_id,
                            'part_id'            => $part_id,
                            'oah_id'             => $new_oah_id,
                            'remarks'            => $remarks
                        );
                        $new_oahl_id = $this->Common_model->insert_data('order_asset_health',$order_asset_health);

                        #to tell overall asset status 
                        if($asset_condition_id != 1)
                        {
                            $defective_status = 2;
                            $unsuccessful = 1;
                        }
                    }

                    # Old Audit Asset and its position
                    $auditOldAssetData = auditAssetData(array());
                    $approval = $assetData['approval_status'];
                    #if defective, send alert to admin
                    if(@$defective_status == 2)
                    { 
                        if($approval == 0)
                        {
                            #inserting into defective asset 
                            $defective_asset = array(
                                'asset_id'           => $asset_id,
                                'trans_id'           => $tool_order_id,
                                'sso_id'             => $ordered_sso_id,
                                'created_by'         => $this->session->userdata('sso_id'),
                                'type'               => 1, 
                                'status'             => 2,
                                'current_stage_id'   => 8
                            );
                            $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                            
                            foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                            { 
                                $part_id = $value[0];
                                $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                                $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];
                                $order_asset_health_data = array(
                                    'defective_asset_id' => $defective_asset_id,
                                    'part_id'            => $part_id,
                                    'asset_condition_id' => $asset_condition_id,
                                    'remarks'            => $remarks
                                );
                                $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                            }
                            #send Notification To Admin
                            sendDefectiveOrMissedAssetMail($defective_asset_id,1,$returnUserCountry,2,$tool_order_id); 
                        }
                        #update owned asset status
                        $this->Common_model->update_data('order_asset_history',array('status'=>$defective_status),array('oah_id'=>$new_oah_id));
                    }

                    # transaction Updating the Asset
                    $assetNewStatus = $assetData['status'];
                    $updateData = array(
                        'status'              => $assetNewStatus,
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s') 
                        );
                    if($approval == 0 && @$defective_status == 2)
                        $updateData['approval_status'] = 1;
                    $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));
                    # Update insert asset status history
                    $assetStatusHistoryArray = array(
                        'asset_id'          => $asset_id,
                        'created_by'        => $this->session->userdata('sso_id'),
                        'status'            => $assetNewStatus,
                        'current_stage_id'  => 7,
                        'tool_order_id'     => $tool_order_id
                    );
                    //echo "<pre>";print_r($assetStatusHistoryArray);exit;
                    updateInsertAssetStatusHistory($assetStatusHistoryArray);

                    # Audit Asset start
                    $newAssetData = array();
                    if($defective_status == 2 || $approval == 1) // old approval required or new approval required
                    {
                        $newAssetData['type'] = $defective_status;
                        $auditNewAssetData = auditAssetData($newAssetData,2);
                        $auditAssetRemarks = "Initiated the Return for ".$return_number;
                        assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$returnUserCountry);            
                        # Audit Asset End
                    }   
                    
                }
                else// inserting missed asset
                {
                    $unsuccessful =1;
                    $defective_status = 3;
                    $approval = $assetData['approval_status'];

                    # Audit Start for transaction asset                
                    $auditAssetOldData  = array(
                        'transaction_status'    => "Owned"
                    );
                    $auditAssetNewData = array(
                        'transaction_status'    =>  "Missed"
                    );
                    $blockArr = array("asset_id" => $asset_id);
                    # as new oah id not available old oah id only inserting 
                    $auditOahId = tool_order_audit_data("return_assets",$oah_id,"order_asset_history",1,$ad_id,$auditAssetNewData,$blockArr,$auditAssetOldData,$tool_order_arr['order_number'],'',array(),"tool_order",$tool_order_id,$returnUserCountry);

                    # Audit Old Asset and its position
                    $oldAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    $auditOldAssetData = auditAssetData($oldAssetData);
                    if($approval == 0)
                    {
                        
                        $defective_asset = array(
                            'asset_id'         => $asset_id,
                            'current_stage_id' => 8,
                            'trans_id'         => $tool_order_id,
                            'sso_id'           => $ordered_sso_id,
                            'created_by'       => $this->session->userdata('sso_id'),
                            'type'             => 2,
                            'status'           => 3 
                        );
                        $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                        foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                        { 
                            $part_id = $value[0];
                            $order_asset_health_data = array(
                                'defective_asset_id' => $defective_asset_id,
                                'part_id'            => $part_id,
                                'asset_condition_id' => 3,
                                'remarks'            => "Missed"
                                );
                            $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                        }
                        #send Notification To Admin
                        sendDefectiveOrMissedAssetMail($defective_asset_id,2,$returnUserCountry,2,$tool_order_id);  
                    }
                    $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$order_status_id)); 

                    # transaction Updating the Asset
                    $updateData = array(
                        'modified_by'         => $this->session->userdata('sso_id'),
                        'modified_time'       => date('Y-m-d H:i:s') 
                        );
                    if($approval == 0)
                        $updateData['approval_status'] = 1;
                    $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                    # Audit Asset and its position
                    $newAssetData = array(
                        'asset_id'          => $asset_id,
                        'asset_status_id'   => $assetData['status']
                    );
                    if($defective_status == 3 || $approval == 1) // old approval required or new approval required
                    {
                        $newAssetData['type'] = $defective_status;
                    }   
                    $auditNewAssetData = auditAssetData($newAssetData,2);
                    $auditAssetRemarks = "Missed for the Return ".$return_number;
                    assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$returnUserCountry);            
                    # Audit Asset End
                }
            }

            #updating order status based on pickup statge
            if(@$unsuccessful ==1 && $new_order_status_id!='')
            {
                $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$new_order_status_id));
            }

            #sending mail alert to admin if fe to fe approval needed
            if($return_type_id == 3 || $return_type_id == 4)
            {
                $order_status_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id),'order_status_id');
                $results = $this->Order_m->getAssetDataByOrderStatusID($order_status_id);
                #checking fe returns any tool or not
                if(count($results) > 0 && getUserStatus($returnUser) == 1 )
                {
                    sendFe2FeTransferNotificationToAdmin($return_number,$rto_id,$tool_order_id,$fe2_tool_order_id,$returnUserCountry);
                }
            }
            else if($return_type_id == 1 || $return_type_id == 2)
            {
                $descide8thStage = decideMainOrder8thstage($tool_order_id);
                if($descide8thStage == 1)
                {
                    $oldArr = array('current_stage_id'=>$tool_order_arr['current_stage_id']);
                    $newArr = array('current_stage_id'=>8,'return_number'=> $return_order_data['return_number']);
                    $blockArr = array('blockName'=>"Tool Order");
                    tool_order_audit_data('tool_order',$tool_order_id,"tool_order",2,'',$newArr,$blockArr,$oldArr,$tool_order_arr['order_number'],'',array(),"tool_order",$tool_order_id,$returnUserCountry);
                }
                if(getUserStatus($returnUser) == 1)
                    sendfetowhtoolreturn($return_number,$rto_id,$tool_order_id,$fe2_tool_order_id,$returnUserCountry,$to_wh_id,$from_wh_id);
            }
        }

        #conditions to close the order 
        // total assets == posted assets
        if($owned_asset_count == count($oah_id_with_ordered_asset_id))
        {
            // all posted assets are defective
            if($m == count($oah_id_with_ordered_asset_id))
            {
                #check owned assets  = sum of WH received and missed 
                #if except missed assets rest of them are received by wh or not then only close the order
                $received_assets = count(get_current_stage_involved_assets($tool_order_id,7));
                $wh_received_assets = count(get_current_stage_involved_assets($tool_order_id,10));
                $missed_assets = count(get_current_stage_involved_assets($tool_order_id,7,array(3)));
                $returned_assets = ($wh_received_assets+$missed_assets);
                if($received_assets == $returned_assets )
                {                
                    $new_order_status_history_data = array(
                        'current_stage_id' => 10,
                        'tool_order_id'    => $tool_order_id,  
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $new_order_status_id = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);
                    $this->Common_model->update_data('tool_order',array('current_stage_id'=>10,'status'=>10),array('tool_order_id'=>$tool_order_id));
                    $oldArr = array('current_stage_id'=>$tool_order_arr['current_stage_id']);
                    $newArr = array('current_stage_id'=>10);
                    $blockArr = array('blockName'=>"Tool Order");
                    tool_order_audit_data('tool_order',$tool_order_id,"tool_order",2,'',$newArr,$blockArr,$oldArr,$tool_order_arr['order_number'],'',array(),"tool_order",$tool_order_id,$returnUserCountry);

                }
            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Return No :<strong>'.$return_number.'</strong> has been successfully initiated for the Order No :<strong>'.$fe1_order_number.'</strong></div>');
        }
        redirect(SITE_URL.'raise_pickup'); exit(); 
    }
        
    public function open_fe_return_initiated_list()
    {
        $data['nestedView']['heading']="Pending Pickup Requests";
        $data['nestedView']['cur_page'] = 'open_fe_return_initiated_list';
        $data['nestedView']['parent_page'] = 'open_fe_return_initiated_list';

        #Page Authentication
        $task_access = page_access_check('raise_pickup');

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Pending Pickup Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Pending Pickup Requests','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=$this->input->post('open_fe_return_initiated_list', TRUE);
        if($psearch!='') 
        {
            $searchParams=array(
            'ri_order_number'   => validate_string(trim($this->input->post('order_number', TRUE))),
            'ri_return_type_id' => validate_string(trim($this->input->post('return_type_id', TRUE))),
            'ri_return_number'  => validate_string(trim($this->input->post('return_number', TRUE))),
            'ri_country_id'     => validate_string(trim($this->input->post('ri_country_id', TRUE))),
            'ri_sso_id'         => validate_string(trim($this->input->post('ri_sso_id', TRUE)))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'ri_order_number'   => $this->session->userdata('ri_order_number'),
                'ri_return_type_id' => $this->session->userdata('ri_return_type_id'),
                'ri_return_number'  => $this->session->userdata('ri_return_number'),
                'ri_country_id'     => $this->session->userdata('ri_country_id'),
                'ri_sso_id'         => $this->session->userdata('ri_sso_id')
                );
            }
            else 
            {
                $searchParams=array(
                'ri_order_number'   => '',
                'ri_return_type_id' => '',
                'ri_return_number'  => '',
                'ri_country_id'     => '',
                'ri_sso_id'         => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'open_fe_return_initiated_list/';

        # Total Records
        $config['total_rows'] = $this->Order_m->feReturnInitiatedTotalNumRows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Order_m->feReturnInitiatedResults($current_offset, $config['per_page'], $searchParams,$task_access);
        $assets_data = array();
        if(count($data['orderResults'])>0)
        {
            foreach ($data['orderResults'] as $key => $value)
            {
               $asset_data[$value['order_status_id']] = $this->Order_m->getAssetDataByOrderStatusID($value['order_status_id']);
            }
            $data['asset_data'] = $asset_data;
        }
        $data['return_type_arr'] = $this->Common_model->get_data('return_type',array('status'=>1));
        $data['displayResults'] = 1;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access)); 
        $data['task_access'] = $task_access;
        $this->load->view('order/open_fe_return_initiated_list',$data);
    }

    public function viewOpenReturnDetials()
    {
        $return_order_id = storm_decode($this->uri->segment(2));
        $redirect = storm_decode($this->uri->segment(3));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'open_fe_return_initiated_list'); exit();
        }
        if($redirect == 1)
        {
            $data['cancel_form_action'] = SITE_URL.'open_fe_return_initiated_list';
            $data['nestedView']['heading']="Pending Pickup from WH Details";
            $data['nestedView']['breadCrumbTite'] = 'Pending Pickup from WH Details';

            $data['nestedView']['cur_page'] = "open_fe_return_initiated_list";
            $data['nestedView']['parent_page'] = 'open_fe_return_initiated_list';
        }
        else
        {
            $data['cancel_form_action'] = SITE_URL.'closed_fe_return_initiated_list';
            $data['nestedView']['heading']="View Closed Return Details";
            $data['nestedView']['breadCrumbTite'] = 'View Closed Return Details';

            $data['nestedView']['cur_page'] = "closed_fe_return_initiated_list";
            $data['nestedView']['parent_page'] = 'closed_fe_return_initiated_list';
        }

        

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL));
        if($redirect == 1)
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Pending Pickup from WH ','class'=>'','url'=>SITE_URL.'open_fe_return_initiated_list');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Pending Pickup from WH Details','class'=>'active','url'=>'');
        }
        else
        {
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Return List','class'=>'','url'=>SITE_URL.'closed_fe_return_initiated_list');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Return Details','class'=>'active','url'=>'');
        }
        #additional data
        $return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
        {
            $trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['ro_to_wh_id']));
            $trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
        }
        else 
        {
            $trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
            $sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
            $sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
            $trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];


        }
        $trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');

        $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
        $rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
        $asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
        
        $data['flg'] = 1;
        $data['rrow'] = $rrow;
        $data['trrow'] = $trrow;
        $data['asset_list'] = $asset_list;
        $data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));
        $this->load->view('order/open_fe_return_initiated_list',$data);
    }

    public function closed_fe_return_initiated_list()
    {
        $data['nestedView']['heading']="Closed FE Return Initiated List List";
        $data['nestedView']['cur_page'] = 'closed_fe_return_initiated_list';
        $data['nestedView']['parent_page'] = 'closed_fe_return_initiated_list';
        $task_access = page_access_check('closed_fe_return_initiated_list');

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';

        $data['nestedView']['css_includes'] = array();


        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed FE Return Initiated List';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Return Initiated List','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch= validate_string($this->input->post('closed_fe_return_initiated_list', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'ric_order_number'   => validate_string($this->input->post('order_number', TRUE)),
            'ric_return_type_id' => validate_string($this->input->post('return_type_id', TRUE)),
            'ric_return_number'  => validate_string($this->input->post('return_number', TRUE)),
            'ric_sso_id'         => validate_string($this->input->post('ric_sso_id', TRUE)),
            'ric_country_id'     => validate_string($this->input->post('ric_country_id', TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'ric_order_number'   => $this->session->userdata('ric_order_number'),
                'ric_return_type_id' => $this->session->userdata('ric_return_type_id'),
                'ric_return_number'  => $this->session->userdata('ric_return_number'),
                'ric_sso_id'         => $this->session->userdata('ric_sso_id'),
                'ric_country_id'         => $this->session->userdata('ric_country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'ric_order_number'   => '',
                'ric_return_type_id' => '',
                'ric_return_number'  => '',
                'ric_sso_id'         => '',
                'ric_country_id'     => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_fe_return_initiated_list/';

        # Total Records
        $config['total_rows'] = $this->Order_m->closedfeReturnInitiatedTotalNumRows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */
        # Loading the data array to send to View
        $data['orderResults'] = $this->Order_m->closedfeReturnInitiatedResults($current_offset, $config['per_page'], $searchParams,$task_access);
        $assets_data = array();
        if(count($data['orderResults'])>0)
        {
            foreach ($data['orderResults'] as $key => $value)
            {
               $asset_data[$value['order_status_id']] = $this->Order_m->getAssetDataByOrderStatusID($value['order_status_id']);
            }
            $data['asset_data'] = $asset_data;
        }

        $data['return_type_arr'] = $this->Common_model->get_data('return_type',array('status'=>1));
        $data['displayResults'] = 1;
        $data['country_list'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access)); 
        $data['task_access'] = $task_access;
        $this->load->view('order/closed_fe_return_initiated_list',$data);

    }
    public function viewClosedReturnDetials()
    {
        
        $return_order_id = storm_decode($this->uri->segment(2));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'closed_pickup_list'); exit();
        }

        $data['nestedView']['heading']="Closed FE Pickup Details";
        $data['nestedView']['cur_page'] = "closed_fe_return_initiated_list";
        $data['nestedView']['parent_page'] = 'closed_fe_return_initiated_list';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed FE Pickup Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Pickup Requests','class'=>'','url'=>SITE_URL.'closed_fe_return_initiated_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed FE Pickup Details','class'=>'active','url'=>'');

        #additional data
        $return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
        {
            $trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['ro_to_wh_id']));
            $trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
        }
        else if($return_order['return_type_id'] == 4 || $return_order['return_type_id'] == 3)
        {
            $trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
            $sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
            $sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
            $trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];


        }
        $trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');


        $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
        $rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
        $asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
        
        $data['flg'] = 1;
        $data['rrow'] = $rrow;
        $data['trrow'] = $trrow;
        $data['asset_list'] = $asset_list;
        $data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));
        $this->load->view('order/closed_pickup_list',$data);
    }
    
    public function get_inventory_tools_availability()
    {
        $tool_id=$this->input->post('tool_id',TRUE);
        $transactionUser=$this->input->post('transactionUser',TRUE);
        $transactionCountry=$this->input->post('transactionCountry',TRUE);

        $result=$this->Common_model->get_data_row('tool',array('tool_id'=>$tool_id,'country_id'=>$transactionCountry));
        $tool_sub_parts = get_sub_parts_data($tool_id);

        $print='<div align="center">
                    <div class="form-group">
                        <label class="col-md-12 control-label">Tool Number : <span class="tool_number control-label">'.$result['part_number'].'</span></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label">Tool Description : <span class="tool_description control-label">'.$result['part_description'].'</span></label>
                    </div>
                </div>';
        
        
        $print.='<div class="table-responsive">
                <table class="table table-bordered hover" style="width:90%;" align="center"> 
                    <thead>
                        <tr>
                            <th class="text-center" width="5%"><strong>S.No</strong></th>
                            <th class="text-center" width="35%"><strong>Tool Number</strong></th>
                            <th class="text-center" width="35%"><strong>Tool Description</strong></th>
                            <th class="text-center" width="10%"><strong>Tool Level</strong></th>
                        </tr>
                    </thead>
                    <tboby>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">'.$result['part_number'].'</td>
                        <td class="text-center">'.$result['part_description'].'</td>
                        <td class="text-center">L0</td>
                    </tr>';

        if(count($tool_sub_parts)>0)
        {
            $sn_no = 2;
            foreach($tool_sub_parts as $row)
            {
                $tool_sub_part_data = get_tool_parts($row['tool_id']);
                $part_level = ($tool_sub_part_data['part_level_id']==1)?'L0':'L1';
                $print.='
                <tr>
                    <td class="text-center">'.$sn_no++.'</td>
                    <td class="text-center">'.$tool_sub_part_data['part_number'].'</td>
                    <td class="text-center">'.$tool_sub_part_data['part_description'].'</td>
                    <td class="text-center">'.$part_level.'</td>
                </tr>';
            }
        } 
        $print.='</tbody>
                 </table>
                 </div><br>';

        $warehouse=count_of_warehouse($tool_id,$transactionUser,$transactionCountry);
        if(count($warehouse)>0)
        {
           $print.='<div class="table-responsive">
                    <table class="table table-bordered hover" style="width:50%;" align="center"> 
                        <thead>
                            <tr>
                                <th class="text-center"><strong>Warehouse</strong></th>
                                <th class="text-center"><strong>Quantity</strong></th>
                            </tr>
                        </thead>
                        <tboby>';
            foreach($warehouse as $row)
            {
                $quantity=count(count_of_availability($tool_id,$row['wh_id']));
                if($quantity>0)
                {
                   $print.='
                        <tr>
                            <td class="text-center">'.$row['wh_code'].' - '.'('.$row['name'].')'.'</td>
                            <td class="text-center">'.$quantity.'</td
                        </tr>'; 
                }
                
            }
            $print.='</tbody>
                     </table>
                     </div>';
        }
        else
        {
            $print.='<br><br><h4 class="text-center"><b>No Qty Available In Other Inventory </b></h4></div>';
        }
        echo $print;
    }

    public function cancel_return_request()
    {
        $return_order_id = storm_decode($this->uri->segment(2));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'open_fe_return_initiated_list'); exit();
        }
        $data['nestedView']['heading']="Cancel Return Request";
        $data['nestedView']['cur_page'] = 'open_fe_return_initiated_list';
        $data['nestedView']['parent_page'] = 'open_fe_return_initiated_list';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/owned_assets.js"></script>';

        $data['nestedView']['css_includes'] = array();


        $data['nestedView']['breadCrumbTite'] = 'Cancel Return Request';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Pending Pickup from WH','class'=>'','url'=>SITE_URL.'open_fe_return_initiated_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Cancel Return Request','class'=>'active','url'=>'');

        #additional data
        $return_order = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        if($return_order['return_type_id'] == 1 || $return_order['return_type_id'] == 2)
        {
            $trrow = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$return_order['ro_to_wh_id']));
            $trrow['to_name'] = $trrow['wh_code'].' -('.$trrow['name'].')';
        }
        else 
        {
            $trrow = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$return_order['tool_order_id']));
            $sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$return_order['tool_order_id']),'sso_id');
            $sso = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));
            $trrow['to_name'] = $sso['sso_id'].' - '.$sso['name'];
        }
        if(isset($ttrow['location_id']))
        {
            $trrow['location_name'] = $this->Common_model->get_value('location',array('location_id'=>$trrow['location_id']),'name');
        }
        else
        {
            $ttrow['location_name'] = '';
        }
        
        $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
        $rrow = $this->Pickup_point_m->get_return_details($return_order_id,$rto_id);
        $asset_list = $this->Pickup_point_m->get_asset_list_by_osh($rrow['order_status_id']);
        
        $data['flg'] = 1;
        $data['rrow'] = $rrow;
        $data['trrow'] = $trrow;
        $data['asset_list'] = $asset_list;
        $data['documenttypeDetails']=$this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('return_doc',array('return_order_id'=>$return_order_id));
        $this->load->view('order/cancel_return_request',$data);
    }

    public function submit_cancel_return_request()
    {
        $return_order_id = storm_decode($this->uri->segment(2));
        if($return_order_id == '')
        {
            redirect(SITE_URL.'open_fe_return_initiated_list'); exit();
        }
        $this->db->trans_begin();

        $returnInfo = $this->Common_model->get_data_row('return_order',array('return_order_id'=>$return_order_id));
        $fe1_tool_order_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'tool_order_id');
        $tool_order_arr = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$fe1_tool_order_id));
        $return_number = $returnInfo['return_number'];
        // updating owned asset status with the same status when he acknowledged from shipment process
        $rto_id = $this->Common_model->get_value('return_tool_order',array('return_order_id'=>$return_order_id),'rto_id');
        $order_status_id = $this->Common_model->get_value('order_status_history',array('rto_id'=>$rto_id),'order_status_id');
        

        // update order status history 
        $this->Common_model->update_data('order_status_history',array('end_time'=>date('Y-m-d H:i:s'),'status'=>3),array('order_status_id'=>$order_status_id));
        //echo $order_status_id;exit;
        $data['owned_assets'] = $this->Order_m->getOwnedAssetsDetailsByOrderId($fe1_tool_order_id);

        // reinitiating the return process to the rejected assets
        $return_assets = $this->Fe2fe_m->get_return_initialted_assets($rto_id);
        // loping the return initiated assets and getting the status from shipment and updating in fe owned status

        // ship order status id
        $ship_order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$fe1_tool_order_id,'current_stage_id'=>6),'order_status_id');
        $owned_order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$fe1_tool_order_id,'current_stage_id'=>7),'order_status_id');

        // done't change the Remarks hard coded value because this will effect the status of workflow
        // ***************** 
        $this->Common_model->update_data('tool_order',array('current_stage_id'=>7),array('tool_order_id'=>$fe1_tool_order_id));

        # Audit start  
        $oldReturnOrderStatus = returnOrderStatusWithRtoId($rto_id);
        // done't change the Remarks hard coded value because this will effect the status of workflow
        // ***************** 
        $this->Common_model->update_data('return_order',array('return_approval'=>0,'status'=>10,'remarks'=>'Cancelled By FE'),array('return_order_id'=>$return_order_id));

        # Return order Audit
        $return_order_data = array(
            'return_status'   => "Cancelled",
            'remarks'         => "Cancelled By FE"
        );
        $oldArr = array(
            'return_status'   => $oldReturnOrderStatus
        );
        $blockArr = array('return number'=> $return_number);
        $remarks_str = 'Return Order : '.$return_number.' is Cancelled by FE.';
        $ad_id = tool_order_audit_data('return_order',$return_order_id,'return_order',2,'',$return_order_data,$blockArr,$oldArr,$remarks_str,'',array(),"tool_order",$fe1_tool_order_id,$tool_order_arr['country_id']);

        # End of Audit

        foreach ($return_assets as $key => $value)
        {
            $shp_condition = array('order_status_id'=>$ship_order_status_id,'ordered_asset_id'=>$value['ordered_asset_id']);
            $shipment_asset_condition = $this->Common_model->get_value('order_asset_history',$shp_condition,'status');
            // update owned order asset status
            $owned_condition = array('order_status_id'=>$owned_order_status_id,'ordered_asset_id'=>$value['ordered_asset_id']);
            $this->Common_model->update_data('order_asset_history',array('status'=>$shipment_asset_condition),$owned_condition);

            # Audit Start for transaction asset                
            $old_data_array  = array('transaction_status' => 'Return Initiated');
            $acd_array = array('transaction_status'  => 'Owned');                
            $blockArra = array('asset_id' => $value['asset_id']);
            $auditOahId = tool_order_audit_data("return_assets",$value['oah_id'],"order_asset_history",1,$ad_id,$acd_array,$blockArra,$old_data_array,$tool_order_arr['order_number'],'',array(),"tool_order",$fe1_tool_order_id,$tool_order_arr['country_id']);
            # End of Audit
        }

        # Tool Order Audit
        $oldArr = array('current_stage_id' => $tool_order_arr['current_stage_id']);
        $newArr = array(
            'current_stage_id' => 7,
            'return_number'    => $return_number
        );
        $remarks_str = 'Return Order : '.$return_number.' is Cancelled by FE.';
        $blockArr = array('blockName'=>"Tool Order");
        tool_order_audit_data('tool_order',$fe1_tool_order_id,"tool_order",2,'',$newArr,$blockArr,$oldArr,$remarks_str,'',array(),"tool_order",$fe1_tool_order_id,$tool_order_arr['country_id']);

        if ($this->db->trans_status() === FALSE)
        { 
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'open_fe_return_initiated_list');  
        }
        else
        {
            $this->db->trans_commit();            
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Return Request : <strong>'.$return_number.'</strong> Has been Cancelled successfully !
            </div>');
            redirect(SITE_URL.'open_fe_return_initiated_list');
        }
    }
    public function receive_st_fe_order_details()
    {
        $tool_order_id=@storm_decode($this->uri->segment(2));
        if($tool_order_id=='')
        {
            redirect(SITE_URL.'receive_order');
            exit;
        }
        $data['tool_order_id'] = $tool_order_id;
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Ack From WH Details ";
        $data['nestedView']['cur_page'] = 'receive_order';
        $data['nestedView']['parent_page'] = 'receive_order';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/fe_receive_order.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';

        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Ack From WH Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack From WH','class'=>'','url'=>SITE_URL.'receive_order');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Ack From WH Details','class'=>'active','url'=>'');

        # Additional data
        $data['order_status_id'] = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>2),'order_status_id');
        $return_assets = $this->Order_m->wh_return_initialted_assets($tool_order_id,1);

        $return_parts = array();
        foreach ($return_assets as $key => $value) 
        {
            $return_parts[$value['oah_id']]['part_number']       = $value['part_number'];
            $return_parts[$value['oah_id']]['asset_number']      = $value['asset_number'];
            $return_parts[$value['oah_id']]['asset_id']          = $value['asset_id'];
            $return_parts[$value['oah_id']]['asset_status']      = $value['asset_status'];
            $return_parts[$value['oah_id']]['ordered_asset_id']  = $value['ordered_asset_id'];
            $return_parts[$value['oah_id']]['ordered_tool_id']   = $value['ordered_tool_id'];
            $return_parts[$value['oah_id']]['part_description']  = $value['part_description'];
            $return_parts[$value['oah_id']]['health_data'] = $this->Order_m->get_transit_asset_data($value['oah_id']);
        }
        $data['return_assets'] = $return_assets;
        $data['return_parts'] = $return_parts;
        $data['return_info'] = $this->Order_m->get_return_info($tool_order_id);                   
        // list details
        $data['trow'] = $this->Order_m->get_order_info($tool_order_id);
        $data['drow'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $main_tool_order = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        $sso_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$main_tool_order),'sso_id') ;
        $data['sso_name'] = getNameBySSO($sso_id);
        $this->load->view('order/receive_st_order_details',$data);
    }

    public function insert_st_fe_received_order()
    {
        $order_status_id = validate_string($this->input->post('order_status_id',TRUE));        
        $oah_oa_health_id_part_id = $this->input->post('oah_oa_health_id_part_id',TRUE);
        $oah_id_arr = $this->input->post('oah_id',TRUE);
        $remarks_arr = $this->input->post('remarks',TRUE);
        $oa_health_id_arr = $this->input->post('oa_health_id',TRUE);        
        $tool_order_id = validate_string($this->input->post('tool_order_id',TRUE));
        $oah_condition_id = $this->input->post('oah_condition_id',TRUE);
        $assetAndOrderedAsset = $this->input->post('assetAndOrderedAsset',TRUE); 

        $this->db->trans_begin();
        # getting tool order and st data for transaction data
        $stData = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $country_id = $stData['country_id'];
        $stn_number = $stData['stn_number'];
        $actual_tool_order_id = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        $orderData = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$actual_tool_order_id));  
        $ordered_sso_id = $orderData['sso_id'];
        $k=0; $j=0; $unsuccessful = 0; $m=0; // for inserting into fe order order status history table

        $mainOrderedTool = array();
        $auditOrderedToolIdArr = array();
        # Audit Start
        $oldArr = array(
            'current_stage'  => "Intransit WH to WH"
        );
        $newArr = array(
            'current_stage'  => "Closed"
        );            
        $blockArr = array( "blockName" => "Stock Transfer Ack at FE");
        $ad_id = tool_order_audit_data("st_ack",$stData['tool_order_id'],"tool_order",2,'',$newArr,$blockArr,$oldArr,'','',array(),"tool_order",$actual_tool_order_id,$country_id);

        foreach ($oah_id_arr as $oah_id => $ordered_asset_id)
        {
            $orderedAssetData = $this->Common_model->get_data_row('ordered_asset',array('ordered_asset_id'=>$ordered_asset_id));
            $asset_id = $orderedAssetData['asset_id'];
            $stOrderedToolId = $orderedAssetData['ordered_tool_id'];
            $assetData = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
            $asset_number = $assetData['asset_number'];
            
            //updating end time of previous stage
            if($k ==0 )
            {   
                $end_time = array('end_time'=>date('Y-m-d H:i:s'));
                $this->Common_model->update_data('order_status_history',$end_time,array('order_status_id'=>$order_status_id)); 
            } 
            // updating the previous asset status 
            // please careful here from post it was 2 but to tell asset status in that transaction we are
            // inserting that status to 3 to say that is missed
            $replaced_status = ($oah_condition_id[$ordered_asset_id] == 2)?3:$oah_condition_id[$ordered_asset_id];
            $u_data = array('status'=>$replaced_status);
            $u_where = array('oah_id'=>$oah_id);
            $this->Common_model->update_data('order_asset_history',$u_data,$u_where);
            if($replaced_status!=2 && $m==0)
            {
                $m++;
            }
            // get stock transfer tool id
            $st_tool_id = $this->Stock_transfer_m->get_tool_id_by($ordered_asset_id);
            // updating the actual ordered tool data i.e quantity            
            // inserting even the asset is missed in acknowledge status 
            if($k == 0 )
            {   
                // inserting new order status
                $new_order_status_history_data = array(
                    'current_stage_id'   => 3,
                    'tool_order_id'      => $tool_order_id,  
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s'),
                    'status'             => 1
                    );
                $new_order_status_id_of_st = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);  
                $k++;
                
            } 

            // inserting into FE order status history
            if($m==1)
            {
                $fe_order_status_history_id_6 = $this->Common_model->get_value('order_status_history',array('current_stage_id'=>6,'tool_order_id'=>$actual_tool_order_id),'order_status_id');
                if(@$fe_order_status_history_id_6 =='')
                {
                    $fe_order_status_history = array(
                        'current_stage_id'   => 6,
                        'tool_order_id'      => $actual_tool_order_id,  
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 1
                        );
                    $fe_order_status_history_id_6 = $this->Common_model->insert_data('order_status_history',$fe_order_status_history);  
                }
                $fe_order_status_history_id_7 = $this->Common_model->get_value('order_status_history',array('current_stage_id'=>7,'tool_order_id'=>$actual_tool_order_id),'order_status_id');
                if(@$fe_order_status_history_id_7 =='')
                {
                    $new_order_status_history_data = array(
                        'current_stage_id'   => 7,
                        'tool_order_id'      => $actual_tool_order_id,  
                        'created_by'         => $this->session->userdata('sso_id'),
                        'created_time'       => date('Y-m-d H:i:s'),
                        'status'             => 1
                        );
                    $fe_order_status_history_id_7 = $this->Common_model->insert_data('order_status_history',$new_order_status_history_data);                  
                }
            }


            if($oah_condition_id[$ordered_asset_id] == 1)
            {
                if(!isset($main_ad))
                {
                    # Audit Start for Tool Order
                    $remarksString = "Stock Transfer".$stData['stn_number']." Acknowledged by FE";
                    $blockArr =array('blockName'=> "Tool Order") ;
                    $oldData = array('current_stage'=> "In Stock Transfer");     
                    $finalArr = array('current_stage'=>"In Stock Transfer");
                    $main_ad = tool_order_audit_data("tool_order",$actual_tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$oldData,$remarksString,'',array(),"tool_order",$actual_tool_order_id,$country_id);    
                }

                $actualOdtQtyAvailqty = $this->Common_model->get_data_row('ordered_tool',array('tool_order_id'=>$actual_tool_order_id,'tool_id'=>$st_tool_id,'status<'=>3));
                // updating the available quantity
                $q = 1;
                $qry = 'UPDATE ordered_tool SET available_quantity = available_quantity + "'.$q.'" 
                        WHERE ordered_tool_id = "'.$actualOdtQtyAvailqty['ordered_tool_id'].'"  ';
                $this->db->query($qry);
                // updating the status to 2 by comparing available quantity and quantity
                // updating the status to 2 by comparing available quantity and quantity
                if(!in_array($actualOdtQtyAvailqty['ordered_tool_id'], $mainOrderedTool))
                {
                    $mainOrderedTool[] = $actualOdtQtyAvailqty['ordered_tool_id'];
                    # Audit Start
                    $oldOrderToolData = array(
                        'quantity'              => $actualOdtQtyAvailqty['quantity'],
                        'available_quantity'    => $actualOdtQtyAvailqty['available_quantity'],
                    );
                    $newOrderToolData = array(
                        'quantity'              => $actualOdtQtyAvailqty['quantity'],
                        'available_quantity'    => ($actualOdtQtyAvailqty['available_quantity']+1),
                        "tool_status"           => "Updated"
                    );
                    $auditOrderedToolBlock = array('tool_id'=>$actualOdtQtyAvailqty['tool_id']);
                    $auditOrderedToolId = tool_order_audit_data('ordered_tools',$actualOdtQtyAvailqty['ordered_tool_id'],'ordered_tool',2,$main_ad,$newOrderToolData,$auditOrderedToolBlock,$oldOrderToolData,'','',array(),"tool_order",$actual_tool_order_id,$country_id); 
                    $auditOrderedToolIdArr[$actualOdtQtyAvailqty['ordered_tool_id']] = $auditOrderedToolId;
                }
                else
                {
                    $updateAuditData = array('available_quantity'=>($actualOdtQtyAvailqty['available_quantity']+1));
                    updatingAuditData($auditOrderedToolIdArr[$actualOdtQtyAvailqty['ordered_tool_id']],$updateAuditData);
                }

                $quantity = $actualOdtQtyAvailqty['quantity'];
                $available_quantity = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$actualOdtQtyAvailqty['ordered_tool_id']),'available_quantity');
                if($quantity == $available_quantity)
                    $this->Common_model->update_data('ordered_tool',array('status'=>1),array('ordered_tool_id'=>$actualOdtQtyAvailqty['ordered_tool_id']));
                // inserting into ordered asset and order asset histotry

                // inserting into ordered asset
                $fe_ordered_asset = array(
                    'ordered_tool_id' =>  $actualOdtQtyAvailqty['ordered_tool_id'],
                    'asset_id'        =>  $asset_id,
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s')
                );
                $fe_ordered_asset_id = $this->Common_model->insert_data('ordered_asset',$fe_ordered_asset);

                // inserting into ordere asset history 6th 
                $fe_order_asset_histoty_6 = array(
                    'ordered_asset_id'   => $fe_ordered_asset_id,
                    'order_status_id'    => $fe_order_status_history_id_6,
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s')
                );
                $fe_oah_id_6 =$this->Common_model->insert_data('order_asset_history',$fe_order_asset_histoty_6);

                // inserting into ordere asset history 7th
                $fe_order_asset_histoty_7 = array(
                    'ordered_asset_id'   => $fe_ordered_asset_id,
                    'order_status_id'    => $fe_order_status_history_id_7,
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s')
                );
                $fe_oah_id_7 = $this->Common_model->insert_data('order_asset_history',$fe_order_asset_histoty_7);

                // inserting the new record
                $new_asset_history = array(
                    'ordered_asset_id'   => $ordered_asset_id,
                    'order_status_id'    => $new_order_status_id_of_st,
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s'),
                    'status'             => $oah_condition_id[$ordered_asset_id]
                );

                $new_oah_id_st = $this->Common_model->insert_data('order_asset_history',$new_asset_history);

                # Audit Start for transaction asset                
                $old_data_array  = array('transaction_status' => 'Shipped');
                $acd_array = array('transaction_status'  => 'Received');                
                $blockArra = array('asset_id' => $asset_id);
                $auditOahId = tool_order_audit_data("st_ack_assets",$new_oah_id_st,"order_asset_history",1,$ad_id,$acd_array,$blockArra,$old_data_array,$stData['stn_number'],'',array(),"tool_order",$orderData['tool_order_id'],$country_id);


                $defective_status =1;
                foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                {   
                    // updating and inserting sts health
                    $part_id = $value[0];
                    $asset_condition_id = $oa_health_id_arr[$oah_id][$oa_health_id][$part_id];
                    $remarks = $remarks_arr[$oah_id][$oa_health_id][$part_id];
                    $health_data = array(
                        'asset_condition_id'  => $asset_condition_id,
                        'remarks'             => $remarks
                        );
                    $health_where = array('oa_health_id'=>$oa_health_id);
                    $this->Common_model->update_data('order_asset_health',$health_data,$health_where);
                    // inserting FE Owned

                    // inserting into sts health
                    $order_asset_health = array(
                            'asset_condition_id' => $asset_condition_id,
                            'part_id'            => $part_id,
                            'oah_id'             => $new_oah_id_st,
                            'remarks'            => @$remarks
                        );
                    $new_oahl_id = $this->Common_model->insert_data('order_asset_health',$order_asset_health);
                    // updating and inserting fes 6th stage health
                    // there is no updatation of 6th stage
                    // inserting into fe 6th stage
                    $order_asset_health = array(
                        'asset_condition_id' => $asset_condition_id,
                        'part_id'            => $part_id,
                        'oah_id'             => $fe_oah_id_6,
                        'remarks'            => $remarks
                    );
                    $fe_oa_health_id_6th = $this->Common_model->insert_data('order_asset_health',$order_asset_health);

                    // updating and inserting fes 7th stage health
                    $health_data = array(
                        'asset_condition_id'  => $asset_condition_id,
                        'remarks'             => $remarks
                    );
                    $health_where = array('oa_health_id'=>$fe_oa_health_id_6th);
                    $this->Common_model->update_data('order_asset_health',$health_data,$health_where);
                    // inserting into fe 7th stage
                    $order_asset_health = array(
                        'asset_condition_id' => $asset_condition_id,
                        'part_id'            => $part_id,
                        'oah_id'             => $fe_oah_id_7,
                        'remarks'            => $remarks
                    );
                    $this->Common_model->insert_data('order_asset_health',$order_asset_health);
                    // to tell overall asset status                  

                    if($asset_condition_id != 1)
                    {
                        $defective_status = 2; 
                    }
                }

                # Old Audit Asset and its position
                $oldAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $assetData['status']
                );
                $auditOldAssetData = auditAssetData($oldAssetData);

                $approval = $assetData['approval_status'];

                if(@$defective_status == 2)
                {
                    // 0 for not waititng for approval
                    if($approval == 0)
                    {
                        // inserting into defective asset 
                        $defective_asset = array(
                            'asset_id'          => $asset_id,
                            'current_stage_id'  => 2,
                            'trans_id'          => $tool_order_id,
                            'sso_id'            => $ordered_sso_id,
                            'created_by'        => $this->session->userdata('sso_id'),
                            'created_time'      => date('Y-m-d H:i:s'),
                            'type'              => 1,
                            'status'            => 2
                        );
                        $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                        foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                        { 
                            $part_id = $value[0];
                            $order_asset_health_data = array(
                                'defective_asset_id'    => $defective_asset_id,
                                'part_id'               => $part_id,
                                'asset_condition_id'    => $oa_health_id_arr[$oah_id][$oa_health_id][$part_id],
                                'remarks'               => $remarks_arr[$oah_id][$oa_health_id][$part_id]
                            );
                            $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                        } 
                        // send Notification To Admin
                        sendDefectiveOrMissedAssetMail($defective_asset_id,1,$country_id,2,$tool_order_id);  
                    }
                    // uipdate owned asset status
                    $this->Common_model->update_data('order_asset_history',array('status'=>$defective_status),array('oah_id'=>$new_oah_id_st));
                }

                $newAssetStatus = ($assetData['status']==8)?3:$assetData['status'];
                # transaction Updating the Asset
                $updateData = array(
                    'status'              => $newAssetStatus,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s') 
                    );
                if($approval == 0 && @$defective_status ==2)
                    $updateData['approval_status'] = 1;
                $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                # update insert asset position
                $transUpdateInsertPosition = array(
                    'asset_id' => $asset_id,
                    'transit'  => 0,
                    'sso_id'   => $ordered_sso_id,
                    'status'   => $newAssetStatus
                );
                updateInsertAssetPosition($transUpdateInsertPosition);

                # Update insert asset status history
                $assetStatusHistoryArray = array(
                    'asset_id'          => $asset_id,
                    'created_by'        => $this->session->userdata('sso_id'),
                    'status'            => $newAssetStatus,
                    'current_stage_id'  => 3,
                    'ordered_tool_id'   => $stOrderedToolId,
                    'tool_order_id'     => $tool_order_id
                );
                updateInsertAssetStatusHistory($assetStatusHistoryArray);

                # Inserting Into FE 
                $assetStatusHistoryArray['tool_order_id'] = $actual_tool_order_id;
                $assetStatusHistoryArray['ordered_tool_id'] = $actualOdtQtyAvailqty['ordered_tool_id'];
                $assetStatusHistoryArray['current_stage_id'] = 7;
                updateInsertAssetStatusHistory($assetStatusHistoryArray);

                # Audit Asset start
                $newAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $newAssetStatus
                );
                if($defective_status == 2 || $approval == 1) // old approval required or new approval required
                {
                    $newAssetData['type'] = $defective_status;
                }   
                $auditNewAssetData = auditAssetData($newAssetData,2);
                $auditAssetRemarks = "Received for".$orderData['order_number'];
                assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$orderData['order_number'],$ad_id,"tool_order",$tool_order_id,$country_id);                            
                $j++;
            }            
            // inserting missed asset
            else
            {
                $defective_status = 3;
                $approval = $assetData['approval_status'];
                # Audit Start for transaction asset                
                $auditAssetOldData  = array(
                    'transaction_status'    => "Shipped"
                );
                $auditAssetNewData = array(
                    'transaction_status'    =>  "Not Received"
                );
                $blockArr = array("asset_id" => $asset_id);
                $auditOahId = tool_order_audit_data("st_ack_assets",$oah_id,"order_asset_history",2,$ad_id,$auditAssetNewData,$blockArr,$auditAssetOldData,$orderData['order_number'],'',array(),"tool_order",$tool_order_id,$country_id);

                # Audit Old Asset and its position
                $oldAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $assetData['status']
                );
                $auditOldAssetData = auditAssetData($oldAssetData);

                $newAssetStatus = ($assetData['status']==8)?3:$assetData['status'];

                // inserting into acknowlwdge status eventhough asset status is missed
                // inserting the new record
                $new_asset_history = array(
                    'ordered_asset_id'   => $ordered_asset_id,
                    'order_status_id'    => $new_order_status_id_of_st,
                    'created_by'         => $this->session->userdata('sso_id'),
                    'created_time'       => date('Y-m-d H:i:s'),
                    'status'             => 3
                );
                $new_oah_id = $this->Common_model->insert_data('order_asset_history',$new_asset_history);
                $defective_status =1;
                foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                { 
                    $part_id = $value[0];
                    $order_asset_health = array(
                        'asset_condition_id' =>3,
                        'part_id'            =>$part_id,
                        'oah_id'             =>$new_oah_id,
                        'remarks'            =>"Missed"
                    );
                    $this->Common_model->insert_data('order_asset_health',$order_asset_health);
                }
                $unsuccessful = 1; // reinitiating the order for stock transfer
                // 0 for not waititng for approval
                if($approval == 0)
                {
                    $defective_asset = array(
                        'asset_id'          => $asset_id,
                        'current_stage_id'  => 2,
                        'trans_id'          => $tool_order_id,
                        'created_by'        => $this->session->userdata('sso_id'),
                        'sso_id'            => $ordered_sso_id,
                        'created_time'      => date('Y-m-d H:i:s'),
                        'type'              => 2,
                        'status'            => 3
                    );
                    $defective_asset_id = $this->Common_model->insert_data('defective_asset',$defective_asset);
                    foreach ($oah_oa_health_id_part_id[$oah_id] as $oa_health_id => $value)
                    { 
                        $part_id = $value[0];
                        $order_asset_health_data = array(
                            'defective_asset_id'    => $defective_asset_id,
                            'part_id'               => $part_id,
                            'asset_condition_id'    => 3,
                            'remarks'               => "Missed"
                        );
                        $this->Common_model->insert_data('defective_asset_health',$order_asset_health_data);                    
                    }
                    // send Notification To Admin
                    sendDefectiveOrMissedAssetMail($defective_asset_id,2,$country_id,2,$tool_order_id);  
                }

                # transaction Updating the Asset
                $updateData = array(
                    'modified_by'         => $_SESSION['sso_id'],
                    'modified_time'       => date('Y-m-d H:i:s') 
                    );
                if($approval == 0)
                    $updateData['approval_status'] = 1;
                $this->Common_model->update_data('asset',$updateData,array('asset_id'=>$asset_id));

                # Audit Asset and its position
                $newAssetData = array(
                    'asset_id'          => $asset_id,
                    'asset_status_id'   => $assetData['status']
                );
                if($defective_status == 3 || $approval == 1) // old approval required or new approval required
                {
                    $newAssetData['type'] = $defective_status;
                }   
                $auditNewAssetData = auditAssetData($newAssetData,2);
                $auditAssetRemarks = "Missed While Receiving ".$stn_number;
                assetStatusAudit($asset_id,$auditOldAssetData,$auditNewAssetData,"asset_master",2,NULL,$auditAssetRemarks,$ad_id,"tool_order",$tool_order_id,$country_id,$sso_id,$device_type);
                # Audit Asset End
                $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$order_status_id)); 
            }           
            
        }
        // updating the actual order status
        $fullfilled_data = $this->Common_model->get_data('ordered_tool',array('tool_order_id'=>$actual_tool_order_id,'status'=>2));
        if(count($fullfilled_data) == 0)
        {        
            $this->Common_model->update_data('tool_order',array('current_stage_id'=>7),array('tool_order_id'=>$actual_tool_order_id));
            $updatingAuditData = array('current_stage'=>"At FE");
            updatingAuditData($main_ad,$updatingAuditData);
            $end_time = array('end_time'=>date('Y-m-d H:i:s'));
            $this->Common_model->update_data('order_status_history',$end_time,array('tool_order_id'=>$actual_tool_order_id,'current_stage_id'=>4));   
        }
        // updating FE owned stage        
        if(@$unsuccessful == 1)
        {
            if($j > 0)
                $this->Common_model->update_data('order_status_history',array('status'=>2),array('order_status_id'=>$new_order_status_id_of_st));

            // closing the st
            if($j ==0)  // not even received the single quantity
            {
                $this->Common_model->update_data('tool_order',array('status'=>6,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));               
                $stStatus = array('current_stage_id'=>3,'status'=>6,'fe_check'=>1);
                $updateAuditData =array('current_stage'=>stockTransferStatus($stStatus));
                updatingAuditData($ad_id,$updateAuditData);
            }
            else   /// received partially
            {
                $this->Common_model->update_data('tool_order',array('status'=>5,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));               
                $stStatus = array('current_stage_id'=>3,'status'=>5,'fe_check'=>1);
                $updateAuditData =array('current_stage'=>stockTransferStatus($stStatus));
                updatingAuditData($ad_id,$updateAuditData);
            }
            // re routing/initiating the order to admin for stock transfer
            $this->Common_model->update_data('st_tool_order',array('status'=>2),array('tool_order_id'=>$actual_tool_order_id,'stock_transfer_id'=>$tool_order_id));               
        }
        else
        {            
            $this->Common_model->update_data('tool_order',array('status'=>10,'current_stage_id'=>3),array('tool_order_id'=>$tool_order_id));
        }        
        if($this->db->trans_status() === FALSE)
        { 
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'receive_order');  
        }
        else
        { 
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <div class="icon"><i class="fa fa-check"></i></div>
                                <strong>Success!</strong> Stock Transfer with ST Number '.$stn_number.' has been Acknowledged!
                             </div>');
           redirect(SITE_URL.'receive_order');  
        }
    }

    public function closeShipmentAsPS()
    {   
        $tool_order_id = @storm_decode($this->uri->segment(2));      
        $this->db->trans_begin();
        if($tool_order_id!= '')
        {       
            $orderInfo = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));       
            $order_number  = $orderInfo['order_number'];
            $country_id = $orderInfo['country_id'];
            $check = checkForPartialClosing($tool_order_id);            
            if($check)
            {   
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Sorry!</strong> Please go to Open stock transfers for Order Number'.$order_number.' and Cancel the STs at Admin or Receive the Sts at FE then Close the Shipment Process as Partial movement.</div>');                 
                redirect(SITE_URL.'open_order'); 
            }
            else
            { 
                $finalArr = array('current_stage'=> "At FE");
                $oldData = array('current_stage'=> "In Stock Trnasfer");
                $blockArr = array('blockName'=> "Tool Order");
                $remarksString = "As Inventory not available for ordered tools, Shipment Process is Closed as Partial movement";
                $main_ad = tool_order_audit_data("tool_order",$tool_order_id,'tool_order',2,'',$finalArr,$blockArr,$oldData,$remarksString,'',array(),"tool_order",$tool_order_id,$orderInfo['country_id']);    

                $this->Common_model->update_data('tool_order',array('current_stage_id'=>7,'modified_by'=>$_SESSION['sso_id'],'modified_time'=>date('Y-m-d H:i:s')),array('tool_order_id'=>$tool_order_id));  

                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <div class="icon"><i class="fa fa-check"></i></div>
                                    <strong>Success!</strong> Partial movement has been successfully completed for Order number '.$order_number.'!
                                 </div>');                
                redirect(SITE_URL.'open_order');  
            }
        }         
    }
}