<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Wh_stock_transfer extends Base_controller
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Wh_stock_transfer_m');
    }
    
    public function wh_stock_transfer_list()
    {
        unset($_SESSION['scanned_assets_list']);
        $data['nestedView']['heading']="Stock Transfer Requests";
        $data['nestedView']['cur_page'] = 'wh_stock_transfer_list';
        $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

         # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['js_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Stock Transfer Requests';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer Requests','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchstocktransfer',TRUE));
        $courier_date = validate_string($this->input->post('courier_date',TRUE));
        if($courier_date != '')
        {
            $date = strtotime("+2 days", strtotime($courier_date));
            $courier_date =  date("Y-m-d", $date);
        }
        if($psearch!='') 
        {
            $searchParams=array(
            'stn_number'   => validate_string($this->input->post('stn_number', TRUE)),
            'to_wh_id'     => validate_number($this->input->post('to_wh_id', TRUE)),
            'courier_date' => $courier_date,
            'country_id'   => validate_number(@$this->input->post('country_id',TRUE)),
            'from_wh_id'   => validate_number(@$this->input->post('from_wh_id',TRUE)),
            'ssoid'        => validate_number(@$this->input->post('ssoid',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'stn_number'   => $this->session->userdata('stn_number'),
                'to_wh_id'     => $this->session->userdata('to_wh_id'),
                'courier_date' => $this->session->userdata('courier_date'),
                'country_id'   => $this->session->userdata('country_id'),
                'from_wh_id'   => $this->session->userdata('from_wh_id'),
                'ssoid'        => $this->session->userdata('ssoid')
                );
            }
            else 
            {
                $searchParams=array(
                'stn_number'       => '',
                'to_wh_id'         => '',
                'courier_date'     => '',
                'page_redirect_st' => '',
                'check_wh_scan'    => '',
                'country_id'       => '',
                'from_wh_id'       => '',
                'ssoid'            => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'wh_stock_transfer_list/';
        # Total Records
        $config['total_rows'] = $this->Wh_stock_transfer_m->st_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $st_results = $this->Wh_stock_transfer_m->st_results($current_offset, $config['per_page'], $searchParams,$task_access);

        foreach ($st_results as $key => $value) 
        {
            $st_tools = $this->Wh_stock_transfer_m->get_st_tools($value['tool_order_id']);
            $check = $this->Wh_stock_transfer_m->check_for_scanned_assets($value['tool_order_id']);
            $st_results[$key]['check_scanned_asset'] = $check;
            $st_results[$key]['tools_list'] = $st_tools;
        }


        # Additional data
        $swh_id = $this->session->userdata('swh_id');
        $data['st_results'] = $st_results;
        $data['to_wh_list'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['from_wh_list'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['task_access'] = $task_access;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['current_offset'] = $current_offset;
        $data['displayResults'] = 1;
        $this->load->view('wh_stock_transfer/wh_stock_transfer_list',$data);
    }

    public function unlink_st_scanned_assets_individually()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        $ordered_tool_id = storm_decode($this->uri->segment(3));
        if($tool_order_id=='' || $ordered_tool_id=='')
        {
            redirect(SITE_URL.'st_courier_details/'.storm_encode($tool_order_id)); exit();
        }

        unset($_SESSION['scanned_assets_list'][$ordered_tool_id]);
        $tool_id = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'tool_id');
        $tool_desc = $this->Common_model->get_value('tool',array('tool_id'=>$tool_id),'part_description');
        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Success!</strong> Assets has been unlinked for Tool : <strong>'.$tool_desc.'</strong> !</div>');  
        redirect(SITE_URL.'st_courier_details/'.storm_encode($tool_order_id)); exit();
    }

    public function st_courier_details()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'wh_stock_transfer_list'); exit();
        }

        $data['nestedView']['heading']="Generate Stock Transfer Shipment";
        $data['nestedView']['cur_page'] = 'check_wh_st';
        $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';
        $data['enableFormWizard'] = 1;

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/fuelux/css/fuelux-responsive.min.css" />';
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/fuelux/loader.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/wh_stock_transfer.js"></script>';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Generate Stock Transfer Shipment';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Stock Transfer Requests','class'=>'','url'=>SITE_URL.'wh_stock_transfer_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Stock Transfer Shipment','class'=>'active','url'=>'');

        #additional data
        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        if($check_st=='') //stock transfer
        {  
            $check_st_type = 0; //ST
        } 
        else if($check_st !='') //tool order
        { 
            $check_st_type = 1; //order
        } 

        $data['trow'] = $this->Wh_stock_transfer_m->get_tool_order_details($tool_order_id);
        $country_id = $data['trow']['country_id'];
        $tool_wh_id = $data['trow']['wh_id'];
        $data['country_id'] = $country_id;
        $data['tool_order_id'] = $tool_order_id;
        
        $order_tool = $this->Wh_stock_transfer_m->get_ordered_tool($tool_order_id,$country_id,$check_st_type);

        $present_count = 0;
        $actual_count = count($order_tool);
        foreach ($order_tool as $key => $value) 
        {
            $ordered_tool_id = $value['ordered_tool_id'];
            if($check_st_type == 0)//for ST
            {
                $avail_qty = $this->Wh_stock_transfer_m->get_avail_tool_for_wh_to_wh($ordered_tool_id,$tool_wh_id);
            }
            else if($check_st_type == 1)//for Order
            {
                $avail_qty = $this->Wh_stock_transfer_m->get_avail_tool($value['tool_id'],$tool_wh_id);
            }
            $result = get_scanned_assets_details($ordered_tool_id);
            $asset_count = $result['asset_count'];
            $assets_list = $result['assets_list'];
            
            $order_tool[$key]['avail_qty'] = check_for_minus($avail_qty-$asset_count);
            $order_tool[$key]['asset_count'] = $asset_count;
            $order_tool[$key]['assets'] = $assets_list;

            if($value['quantity'] == $asset_count)
            {
                $present_count++;
            }
        }
        $check_no = 1;
        if($actual_count == $present_count)
        {
            $check_no = 0;
        }

        $data['check_no'] = $check_no;
        $data['order_tool'] = $order_tool;
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$tool_wh_id));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id'=>$country_id));

        if($check_st_type==1)//order
        {
            $data['order_address'] = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id,'status'=>1));
            $sso = $this->Wh_stock_transfer_m->get_fe_sso($tool_order_id);
            $data['sso_name'] = $sso['sso_name'];
        }
        else if($check_st_type==0)//ST
        {
            $to_wh_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'to_wh_id');
            $data['order_address'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$to_wh_id));
        }
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>3));
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_wh_st_delivery';
        $this->load->view('wh_stock_transfer/wizard_wh_st_delivery',$data);
    }

    public function wh_st_asset()
    {
        $ordered_tool_id = @storm_decode($this->uri->segment(2));
        
        if($ordered_tool_id == '')
        {
            redirect(SITE_URL.'wh_stock_transfer_list');
            exit;
        } 
        $_SESSION['check_wh_scan'] = 0;

        $tool_order_id = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'tool_order_id');
        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');

        if($check_st=='') //stock transfer
        {  
            $check_st_type = 0; //ST
        } 
        else if($check_st !='') //tool order
        { 
            $check_st_type = 1; //order
        } 
        $country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
        $tool_wh_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');

        $trow = $this->Wh_stock_transfer_m->get_ordered_tool_detail($ordered_tool_id);
        if(@$trow['fe_check'] == 1)
        {
            $sso = $this->Wh_stock_transfer_m->get_fe_sso($tool_order_id);
            $data['sso_name'] = $sso['sso_name'];
        }
        $data['nestedView']['heading']="Scan QR Code";
        $data['nestedView']['cur_page'] = 'check_wh_st';
        $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Scan QR Code';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'ST Requests','class'=>'','url'=>SITE_URL.'wh_stock_transfer_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate ST Shipment','class'=>'','url'=> SITE_URL.'st_courier_details/'.storm_encode($trow['tool_order_id']));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scan QR Code','class'=>'active','url'=>'');

        $result = get_scanned_assets_details($ordered_tool_id);
        $asset_id_arr = $result['asset_id_arr'];
        if($check_st_type == 0)//for st
        {
            $data['fe_asset_sub_inventory'] = $this->Wh_stock_transfer_m->get_sub_inventory_for_wh_to_wh($ordered_tool_id,$asset_id_arr,$tool_wh_id);
        }
        else//for tool order
        {
            $data['fe_asset_sub_inventory'] = $this->Wh_stock_transfer_m->get_sub_inventory_asset($trow['tool_id'],$tool_wh_id,$asset_id_arr);
        }

        #additional data
        $data['trow'] = $trow;
        $this->load->view('wh_stock_transfer/wh_st_asset',$data);
    }

    public function scanned_asset_detials()
    {
        $check_wh_scan = $this->session->userdata('check_wh_scan');
        $tool_id = validate_number(storm_decode($this->input->post('tool_id',TRUE)));
        $ordered_tool_id = validate_number(storm_decode($this->input->post('ordered_tool_id',TRUE)));
        $asset_number = validate_string($this->input->post('asset_number',TRUE));
        if($check_wh_scan == 1 || $tool_id =="" || $ordered_tool_id == "" || $asset_number == "")
        {
            redirect(SITE_URL.'wh_stock_transfer_list'); exit();
        }
        $asset_number = trim(@$asset_number);
        $tool_order_id = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'tool_order_id');
        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');

        if($check_st=='') //stock transfer
        {  
            $check_st_type = 0; //ST
        } 
        else if($check_st !='') //tool order
        { 
            $check_st_type = 1; //order
        } 
        $country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
        $tool_wh_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');

        $result = get_scanned_assets_details($ordered_tool_id);
        $asset_id_arr = $result['asset_id_arr'];
        if($check_st_type == 0)
        {
            $check_asset = $this->Wh_stock_transfer_m->check_asset_1($asset_number,
                $asset_id_arr,$tool_wh_id,$ordered_tool_id);

            /*check asset is involved in ST or not */
            $st_entry = check_for_st_entry($check_asset['asset_id'],$tool_order_id);
            if(count($st_entry)>0)
            {
                $stn_number = $st_entry['stn_number'];
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
                redirect(SITE_URL.'wh_st_asset/'.storm_encode($ordered_tool_id)); exit();
            }
        }
        else//st for tool order
        {
            $check_asset = $this->Wh_stock_transfer_m->check_asset($tool_id,$asset_number,$tool_wh_id,$asset_id_arr);

            /*koushik 11-10-2018 check asset is involved in ST or not */
            $st_entry = check_for_st_entry($check_asset['asset_id']);
            if(count($st_entry)>0)
            {
                $stn_number = $st_entry['stn_number'];
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
                redirect(SITE_URL.'wh_st_asset/'.storm_encode($ordered_tool_id)); exit();
            }
        }
        if($check_asset['asset_id']=='')
        {
            $asset_identity = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number),'asset_id');
            if($asset_identity != '')
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong>Asset Number:<strong> '.$asset_number.' </strong> Is Linked For Other Stock Transfer Request!</div>'); 
                redirect(SITE_URL.'wh_st_asset/'.storm_encode($ordered_tool_id)); 
                exit();
            }
            else
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Invalid Asset Number:<strong> '.$asset_number.' </strong>!</div>'); 
                redirect(SITE_URL.'wh_st_asset/'.storm_encode($ordered_tool_id)); 
                exit();
            }
        }
        else
        {
            $trow = $this->Wh_stock_transfer_m->get_ordered_tool_detail($ordered_tool_id);
            if(@$trow['fe_check'] == 1)
            {
                $sso = $this->Wh_stock_transfer_m->get_fe_sso($tool_order_id);
                $data['sso_name'] = $sso['sso_name'];
            }
            $data['nestedView']['heading']="Scanned Asset Details";
            $data['nestedView']['cur_page'] = 'check_wh_st';
            $data['nestedView']['parent_page'] = 'wh_stock_transfer_list';

            #page Authentication
            $task_access = page_access_check($data['nestedView']['parent_page']);

            # include files
            $data['nestedView']['css_includes'] = array();
            $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

            $data['nestedView']['js_includes'] = array();
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
            $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/health.js"></script>';

            # Breadcrumbs
            $data['nestedView']['breadCrumbTite'] = "Asset Details : ".$asset_number."";
            $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'ST Requests','class'=>'','url'=>SITE_URL.'wh_stock_transfer_list');
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate ST Shipment','class'=>'','url'=> SITE_URL.'st_courier_details/'.storm_encode($trow['tool_order_id']));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'QR Code','class'=>'','url'=> SITE_URL.'wh_st_asset/'.storm_encode($ordered_tool_id));
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Scanned Asset Details','class'=>'active','url'=>'');

            #additional data
            $asset_details = $this->Wh_stock_transfer_m->get_asset_part_details($check_asset['asset_id'],$tool_wh_id);
            $data['ordered_tool_id'] = $ordered_tool_id;
            $data['asset_details'] = $asset_details;
            $data['trow'] = $trow;
            #display partial tolerance detials
            $data['display_cr_remarks'] = get_cr_remarks($check_asset['asset_id']);
            $this->load->view('wh_stock_transfer/wh_st_asset',$data);
        }
    }

    public function insert_scanned_asset()
    {
        $_SESSION['check_wh_scan'] = 1;
        $ordered_tool_id = validate_number(storm_decode($this->input->post('ordered_tool_id',TRUE)));
        $tool_order_id = $this->Common_model->get_value('ordered_tool',array('ordered_tool_id'=>$ordered_tool_id),'tool_order_id');
        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');

        if($check_st=='') //stock transfer
        {  
            $check_st_type = 0; //ST
        } 
        else //tool order
        { 
            $check_st_type = 1; //order
        }
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));

        $country_id = $trow['country_id'];
        $tool_wh_id = $trow['wh_id'];

        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $asset_number = $asset_arr['asset_number'];

        $asset_condition_id = $this->input->post('asset_condition_id',TRUE);
        $remarks = $this->input->post('remarks',TRUE);
        $asset_details = $this->Wh_stock_transfer_m->get_asset_part_details($asset_id,$tool_wh_id);
        
        /*modified by koushik*/
        $order_status_id = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>1,'end_time'=>NULL),'order_status_id'); 
        #check asset_condition_status
        $check_astatus = 0;
        foreach ($asset_details as $key => $value) 
        {
            if($asset_condition_id[$value['part_id']]!=1)
            {
                $check_astatus++;
            }
        }
        $this->db->trans_begin();
        if($check_st_type == 0)//st for wh to wh
        {
            /* check asset is involved in ST or not */
            $st_entry = check_for_st_entry($asset_id,$tool_order_id);
            if(count($st_entry)>0)
            {
                $stn_number = $st_entry['stn_number'];
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
                redirect(SITE_URL.'wh_st_asset/'.storm_encode($ordered_tool_id)); exit();
            }

            $oa_asset = $this->Common_model->get_data_row('ordered_asset',array('ordered_tool_id'=>$ordered_tool_id,'asset_id'=>$asset_id));

            #if scanned asset is not matching
            if(count($oa_asset)==0)
            {
                redirect(SITE_URL.'wh_stock_transfer_list'); exit();
            }

            $part_list = array();
            foreach ($asset_details as $key => $value) 
            {
                $remarks1 = $remarks[$value['part_id']];
                if($remarks1 == '') $remarks1 = NULL; 
                $insert_oah = array(
                    'part_id'            => $value['part_id'],
                    'asset_condition_id' => $asset_condition_id[$value['part_id']],
                    'remarks'            => $remarks1
                );
                $part_list[] = $insert_oah;
            }

            $_SESSION['scanned_assets_list'][$ordered_tool_id][] = 
            array(
                'asset_id'     => $asset_id,
                'asset_number' => $asset_number,
                'part_list_arr'=> $part_list
            );

            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset : <strong>'.$asset_number.'</strong> has been Linked successfully!</div>');
            redirect(SITE_URL.'st_courier_details/'.storm_encode($tool_order_id)); 
            exit();
        }
        else if($check_st_type == 1)//st for tool order
        {
            $check_oa = $this->Common_model->get_value('ordered_asset',array('ordered_tool_id'=>$ordered_tool_id,'asset_id'=>$asset_id),'ordered_asset_id');
            if(count($check_oa)>0)
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> Asset Already Scanned.</div>'); 
                redirect(SITE_URL.'wh_stock_transfer_list'); exit();
            }

            /*koushik 11-10-2018 check asset is involved in ST or not */
            $st_entry = check_for_st_entry($asset_id);
            if(count($st_entry)>0)
            {
                $stn_number = $st_entry['stn_number'];
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please scan other Asset or remove asset from ST to proceed !.</div>'); 
                redirect(SITE_URL.'wh_st_asset/'.storm_encode($ordered_tool_id)); exit();
            }

            if($check_astatus == 0)
            {
                $part_list = array();
                foreach ($asset_details as $key => $value) 
                {
                    $remarks1 = $remarks[$value['part_id']];
                    if($remarks1 == '') $remarks1 = NULL; 
                    $insert_oah = array(
                        'part_id'            => $value['part_id'],
                        'asset_condition_id' => $asset_condition_id[$value['part_id']],
                        'remarks'            => $remarks1
                    );
                    $part_list[] = $insert_oah;
                }

                $_SESSION['scanned_assets_list'][$ordered_tool_id][] = 
                array('asset_id'     => $asset_id,
                      'asset_number' => $asset_number,
                      'part_list_arr'=> $part_list);

                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Asset : <strong>'.$asset_number.'</strong> has been Linked successfully!</div>');
                redirect(SITE_URL.'st_courier_details/'.storm_encode($tool_order_id)); exit(); 
            }
            else //insert in defective asset
            {
                $insert_defective = array(
                    'asset_id'         => $asset_id,
                    'current_stage_id' => 1,//at wh-tools order
                    'type'             => 1,
                    'wh_id'            => $tool_wh_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'trans_id'         => $tool_order_id,
                    'status'           => 1
                );
                $defective_asset_id = $this->Common_model->insert_data('defective_asset',$insert_defective);

                foreach ($asset_details as $key => $value) 
                {
                    $remarks1 = $remarks[$value['part_id']];
                    if($remarks1 == '') $remarks1 = NULL; 
                    $insert_defect_health = 
                        array(
                        'asset_condition_id' => $asset_condition_id[$value['part_id']],
                        'part_id'            => $value['part_id'],
                        'remarks'            => $remarks1,
                        'defective_asset_id' => $defective_asset_id
                    );
                    $this->Common_model->insert_data('defective_asset_health',$insert_defect_health);
                }

                // send Notification To Admin
                sendDefectiveOrMissedAssetMail($defective_asset_id,1,$country_id,2,$tool_order_id);

                #audit data
                $old_data = array(get_da_key() => get_da_status($asset_id,$asset_arr['approval_status']));
                $update_data5 = array(
                    'approval_status' => 1,
                    'modified_time'   => date('Y-m-d H:i:s'),
                    'modified_by'     => $this->session->userdata('sso_id')
                );
                $update_where5 = array('asset_id' => $asset_id);
                $this->Common_model->update_data('asset',$update_data5,$update_where5);

                #audit data
                $new_data = array(get_da_key() => get_da_status($asset_id,1));
                $remarks = "Identified as defective in Wh";
                audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'defective_asset',$defective_asset_id,$asset_arr['country_id']);

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.</div>');
                    redirect(SITE_URL.'wh_st_asset/'.storm_encode($ordered_tool_id)); 
                    exit(); 
                }
                else
                {
                    $this->db->trans_commit();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Note!</strong> Asset : <strong>'.$asset_number.'</strong> has been Blocked and Intimated to Admin!</div>'); 
                    redirect(SITE_URL.'st_courier_details/'.storm_encode($tool_order_id)); exit(); 
                }
            }
        }
    }

    public function insert_wh_st_delivery()
    {
        $tool_order_id = validate_number(storm_decode($this->input->post('tool_order_id',TRUE)));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'wh_stock_transfer_list'); exit();
        }

        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $country_id = $trow['country_id'];
        #check ST order is at wh = 1 or 4
        if($trow['current_stage_id']!=1 && $trow['current_stage_id']!=4)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Something went wrong. please Try Again !.
            </div>');
            redirect(SITE_URL.'wh_stock_transfer_list'); exit();
        }

        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');

        if($check_st=='') //stock transfer
        {  
            $check_st_type = 0; //ST
        } 
        else if($check_st !='') //tool order
        { 
            $check_st_type = 1; //order
            $fe_trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$check_st));
        }

        #check for out of calibration tool
        $order_tool = $this->Wh_stock_transfer_m->get_ordered_tool($tool_order_id,$country_id,$check_st_type);
        if($check_st_type==1)
        {
            foreach ($order_tool as $key => $value) 
            {
                $ordered_tool_id = $value['ordered_tool_id'];
                $session = $_SESSION['scanned_assets_list'][$ordered_tool_id];
                if(count($session)>0)
                {
                    foreach ($session as $key1 => $value1) 
                    {
                        $asset_status = $this->Common_model->get_value('asset',array('asset_id'=>$value1['asset_id']),'status');
                        if($asset_status>2) // 1 at wh, 2 lock in period
                        {
                            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <div class="icon"><i class="fa fa-times-circle"></i></div>
                            <strong>Error!</strong> Invalid Assets are linked for Order Number:<strong>'.@$trow['stn_number'].'</strong>. please Try Again !.
                            </div>');
                            redirect(SITE_URL.'wh_stock_transfer_list'); exit();
                        }
                    }
                }
                else
                {
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> Something went wrong. please Try Again !.
                    </div>');
                    redirect(SITE_URL.'wh_stock_transfer_list'); exit();
                }
            }
        }

        $courier_type = validate_number($this->input->post('courier_type',TRUE));
        $courier_name = validate_string($this->input->post('courier_name',TRUE));
        $courier_number = validate_string($this->input->post('courier_number',TRUE));
        $contact_person = validate_string($this->input->post('contact_person',TRUE));
        $phone_number = validate_string($this->input->post('phone_number',TRUE));
        $expected_delivery_date = validate_string(format_date($this->input->post('expected_delivery_date',TRUE)));
        $doc_type = $this->input->post('document_type',TRUE);
        $vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
        $remarks        = validate_string($this->input->post('remarks',TRUE));

        if($remarks == ""){ $remarks = NULL; }
        if($courier_type == 1)
        {
            $phone_number   = NULL;
            $contact_person = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 2)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 3)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
        }

        $print_type = validate_number($this->input->post('print_type',TRUE));
        $tool_wh_id = $trow['wh_id'];
        $this->db->trans_begin();
        $print_id = get_current_print_id($print_type,$tool_wh_id);

        $insert_del_details = array(
            'tool_order_id' => $tool_order_id,
            'print_id'      => $print_id,
            'courier_type'  => $courier_type,
            'contact_person'=> $contact_person,
            'phone_number'  => $phone_number,
            'courier_name'  => $courier_name,
            'courier_number'=> $courier_number,
            'vehicle_number'=> $vehicle_number,
            'expected_delivery_date' => $expected_delivery_date,
            'remarks'       => $remarks,
            'billed_to'     => validate_number($this->input->post('billed_to',TRUE)),
            'created_by'    => $this->session->userdata('sso_id'),
            'created_time'  => date('Y-m-d H:i:s'),
            'status'        => 1
        );
        $order_delivery_id = $this->Common_model->insert_data('order_delivery',$insert_del_details);

        if($check_st_type == 1)
        {
            $remarks = "Shipment details for ST:".$trow['stn_number']." for Tool Order:".$fe_trow['order_number'];
            $ad_id = audit_data('st_order_delivery',$order_delivery_id,'order_delivery',1,'',$insert_del_details,array('ST Number'=>$trow['stn_number']),array(),$remarks,'',array(),'tool_order',$fe_trow['tool_order_id'],$fe_trow['country_id']);
        }

        #print_history
        $insert_print_history = array(
            'print_id'          => $print_id,
            'remarks'           => 'Print Creation',
            'order_delivery_id' => $order_delivery_id,
            'created_by'        => $this->session->userdata('sso_id'),
            'created_time'      => date('Y-m-d H:i:s'),
            'status'            => 1
        );
        $this->Common_model->insert_data('print_history',$insert_print_history);

        #Update Order Address
        $old_oa = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id));
        $update_oa = array(
            'address1'      => validate_string($this->input->post('address1',TRUE)),
            'address2'      => validate_string($this->input->post('address2',TRUE)),
            'address3'      => validate_string($this->input->post('address3',TRUE)),
            'address4'      => validate_string($this->input->post('address4',TRUE)),
            'pin_code'      => validate_string($this->input->post('pin_code',TRUE)),
            'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
            'pan_number'    => validate_string($this->input->post('pan_number',TRUE)),
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $update_oa_where = array('tool_order_id'=>$tool_order_id,'status'=>1);
        $this->Common_model->update_data('order_address',$update_oa,$update_oa_where);

        if($check_st_type == 1)
        {
            unset($update_oa['modified_by'],$update_oa['modified_time']);
            $final_arr = array_diff_assoc($update_oa, $old_oa);
            $Remarks = "Updated Address while shipping ST:".$trow['stn_number'];
            audit_data('st_order_address',$old_oa['order_address_id'],'order_address',2,$ad_id,$final_arr,array('ST Number'=>$trow['stn_number']),$old_oa,$remarks,'',array(),'tool_order',$fe_trow['tool_order_id'],$fe_trow['country_id']);
        }

        if(count($doc_type)>0)
        {
            foreach ($doc_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'         => $_FILES['support_document_'.$key]['name'],
                    'order_delivery_id'=> $order_delivery_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s'),
                    'status'           => 1
                );
                if($value!='' && $support_document != '')
                {
                    $this->Common_model->insert_data('order_delivery_doc',$insert_doc);
                }
            }
        }

        $order_status_id1 = $this->Common_model->get_value('order_status_history',array('tool_order_id'=>$tool_order_id,'current_stage_id'=>1,'end_time'=>NULL),'order_status_id');

        $update_osh = array('end_time' => date('Y-m-d H:i:s'));
        $update_osh_where = array('tool_order_id'=>$tool_order_id,'current_stage_id'=>1,'end_time'=>NULL);
        $this->Common_model->update_data('order_status_history',$update_osh,$update_osh_where);

        $insert_osh = array(
            'current_stage_id' => 2,//at transit fOR WH to WH
            'tool_order_id'    => $tool_order_id,
            'created_by'       => $this->session->userdata('sso_id'),
            'created_time'     => date('Y-m-d H:i:s'),
            'status'           => 1
        );
        $order_status_id2 = $this->Common_model->insert_data('order_status_history',$insert_osh);

        if($check_st_type == 1)
        {
            $remarks = "In transit details for ST Order No:".$trow['stn_number']." And Tool Order no:".$fe_trow['order_number'];
            $ad_id1 = audit_data('st_transit',$order_status_id2,'order_status_history',1,'',array(),array('ST Number'=>$trow['stn_number']),array(),$remarks,'',array(),'tool_order',$fe_trow['tool_order_id'],$fe_trow['country_id']);
        }

        if($check_st_type==0)//for ST
        {
            foreach ($order_tool as $key1 => $value1) 
            {
                $ordered_tool_id = $value1['ordered_tool_id'];
                $session = $_SESSION['scanned_assets_list'][$ordered_tool_id];
                foreach ($session as $key2 => $value2) 
                {
                    $asset_id = $value2['asset_id'];
                    $asset_details = $value2['part_list_arr'];
                    $ordered_asset_id = $this->Common_model->get_value('ordered_asset',array('asset_id'=>$asset_id,'ordered_tool_id'=>$ordered_tool_id),'ordered_asset_id');

                    #insert order asset history
                    $insert_oah1 = array(
                        'ordered_asset_id' => $ordered_asset_id,
                        'order_status_id'  => $order_status_id1,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $oah_id1 = $this->Common_model->insert_data('order_asset_history',$insert_oah1);

                    #insert order asset history
                    $insert_oah2 = array(
                        'ordered_asset_id' => $ordered_asset_id,
                        'order_status_id'  => $order_status_id2,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $oah_id2 = $this->Common_model->insert_data('order_asset_history',$insert_oah2);

                    #insert order asset health
                    foreach ($asset_details as $key3 => $value3) 
                    {
                        $insert_oahealth1 = array(
                            'oah_id'             => $oah_id1,
                            'part_id'            => $value3['part_id'],
                            'asset_condition_id' => $value3['asset_condition_id'],
                            'remarks'            => $value3['remarks']
                        );
                        $this->Common_model->insert_data('order_asset_health',$insert_oahealth1);

                        $insert_oahealth2 = array(
                            'oah_id'             => $oah_id2,
                            'part_id'            => $value3['part_id'],
                            'asset_condition_id' => $value3['asset_condition_id'],
                            'remarks'            => $value3['remarks']
                        );
                        $this->Common_model->insert_data('order_asset_health',$insert_oahealth2);
                    }

                    $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

                    $asset_status = $asset_arr['status'];

                    #audit data
                    $old_data = array(
                        'asset_status_id'   => $asset_arr['status'],
                        'tool_availability' => get_asset_position($asset_id)
                    );
                    if($asset_status == 2)
                    {
                        $update_a = array(
                            'status'        => 8,
                            'modified_by'   => $this->session->userdata('sso_id'),
                            'modified_time' => date('Y-m-d H:i:s')
                        );
                        $update_a_where = array('asset_id'=>$asset_id);
                        $this->Common_model->update_data('asset',$update_a,$update_a_where);
                    }

                    $update_ash = array(
                        'end_time' => date('Y-m-d H:i:s'),
                        'modified_by' =>$this->session->userdata('sso_id')
                    );
                    $update_ash_where = array(
                        'asset_id' =>$asset_id,
                        'end_time'=>NULL
                    );
                    $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                    $insert_ash = array(
                        'asset_id'        => $asset_id,
                        'created_by'      => $this->session->userdata('sso_id'),
                        'created_time'    => date('Y-m-d H:i:s'),
                        'tool_order_id'   => $tool_order_id,
                        'current_stage_id'=> 2,
                        'status'          => ($asset_status==2)?8:$asset_status
                    );
                    $this->Common_model->insert_data('asset_status_history',$insert_ash);

                    $update_ap = array('to_date' => date('Y-m-d H:i:s'));
                    $update_ap_w = array(
                        'asset_id' => $asset_id,
                        'to_date'  => NULL
                    );
                    $this->Common_model->update_data('asset_position',$update_ap,$update_ap_w);

                    $insert_ap = array(
                        'asset_id'  => $asset_id,
                        'transit'   => 1,
                        'from_date' => date('Y-m-d H:i:s'),
                        'status'    => 1,
                        'wh_id'     => $trow['to_wh_id']
                    );
                    $this->Common_model->insert_data('asset_position',$insert_ap);

                    #audit data
                    $new_data = array(
                        'asset_status_id'   => ($asset_status==2)?8:$asset_status,
                        'tool_availability' => get_asset_position($asset_id)
                    );
                    $remarks = "Asset has been shipped for ST Order No:".$trow['stn_number'];
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$trow['country_id']);
                }
            }
        }
        else if($check_st_type ==1)//for Tool Order
        {
            foreach ($order_tool as $key1 => $value1) 
            {
                $ordered_tool_id = $value1['ordered_tool_id'];
                $session = $_SESSION['scanned_assets_list'][$ordered_tool_id];
                foreach ($session as $key2 => $value2) 
                {
                    $asset_id = $value2['asset_id'];
                    $asset_details = $value2['part_list_arr'];

                    #insert ordered asset
                    $insert_oa = array(
                        'ordered_tool_id' => $ordered_tool_id,
                        'asset_id'        => $asset_id,
                        'created_by'      => $this->session->userdata('sso_id'),
                        'created_time'    => date('Y-m-d H:i:s'),
                        'status'          => 1
                    );
                    $ordered_asset_id = $this->Common_model->insert_data('ordered_asset',$insert_oa);

                    #insert order asset history
                    $insert_oah1 = array(
                        'ordered_asset_id' => $ordered_asset_id,
                        'order_status_id'  => $order_status_id1,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $oah_id1 = $this->Common_model->insert_data('order_asset_history',$insert_oah1);

                    #audit data for st assets
                    $new_data = array('transaction_status' => 'Shipped');
                    $remarks = "Shipped for ST:".$trow['stn_number'];
                    audit_data('st_assets',$oah_id1,'order_asset_history',1,$ad_id,$new_data,array('asset_id'=>$asset_id),array(),$remarks,'',array(),'tool_order',$fe_trow['tool_order_id'],$fe_trow['country_id']);

                    #insert order asset history
                    $insert_oah2 = array(
                        'ordered_asset_id' => $ordered_asset_id,
                        'order_status_id'  => $order_status_id2,
                        'created_by'       => $this->session->userdata('sso_id'),
                        'created_time'     => date('Y-m-d H:i:s'),
                        'status'           => 1
                    );
                    $oah_id2 = $this->Common_model->insert_data('order_asset_history',$insert_oah2);

                    #audit data for Assets in Transit
                    $new_data = array('transaction_status' => 'Shipped');
                    $remarks = "In Transit for ST:".$trow['stn_number'];
                    audit_data('st_transit_assets',$oah_id2,'order_asset_history',1,$ad_id1,$new_data,array('asset_id'=>$asset_id),array(),$remarks,'',array(),'tool_order',$fe_trow['tool_order_id'],$fe_trow['country_id']);

                    #insert order asset health
                    foreach ($asset_details as $key3 => $value3) 
                    {
                        $insert_oahealth1 = array(
                            'oah_id'             => $oah_id1,
                            'part_id'            => $value3['part_id'],
                            'asset_condition_id' => $value3['asset_condition_id'],
                            'remarks'            => $value3['remarks']
                        );
                        $this->Common_model->insert_data('order_asset_health',$insert_oahealth1);

                        $insert_oahealth2 = array(
                            'oah_id'             => $oah_id2,
                            'part_id'            => $value3['part_id'],
                            'asset_condition_id' => $value3['asset_condition_id'],
                            'remarks'            => $value3['remarks']
                        );
                        $this->Common_model->insert_data('order_asset_health',$insert_oahealth2);
                    }

                    $asset_status = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'status');
                    $ot_assets = $this->Common_model->get_data('asset_status_history',array('ordered_tool_id'=>$ordered_tool_id,'end_time'=>NULL,'status'=>2));

                    $oa_assets = $this->Common_model->get_data('ordered_asset',array('ordered_tool_id'=>$ordered_tool_id,'status <',3));
                    $asset_identity = 0;
                    if(count($ot_assets)>0 && count($oa_assets))
                    {
                        $ot_assets_arr = array();
                        foreach ($ot_assets as $key3 => $value3) 
                        {
                            $ot_assets_arr[] = $value3['asset_id'];
                        }
                        $oa_assets_arr = array();
                        foreach ($oa_assets as $key4 => $value4) 
                        {
                            $oa_assets_arr[] = $value4['asset_id'];
                        }
                        $final_assets = array_diff($oa_assets_arr,$ot_assets_arr);
                        foreach ($final_assets as $key5 => $value5) 
                        {
                            $asset_identity = $value5['asset_id'];
                            $asset_status_is = $this->Common_model->get_value('asset',array('asset_id'=>$asset_identity),'status');
                            if($asset_status_is == 2)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach ($ot_assets as $key5 => $value5) 
                        {
                            $asset_identity = $value5['asset_id'];
                            $asset_status_is = $this->Common_model->get_value('asset',array('asset_id'=>$asset_identity),'status');
                            if($asset_status_is == 2)
                            {
                                break;
                            }
                        }
                    }
                    #conditions if asset status is 1
                    if($asset_status == 1)
                    {
                        #unlink the attached asset
                        if($asset_identity>0)
                        {
                            $first_asset = $asset_identity;
                            #update ash
                            $update_ash = array(
                                'end_time'    => date('Y-m-d H:i:s'),
                                'modified_by' => $this->session->userdata('sso_id')
                            );
                            $update_ash_where = array(
                                'asset_id' => $first_asset,
                                'end_time' => NULL
                            );
                            $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                            #insert in ash
                            $insert_ash = array(
                                'asset_id'        => $first_asset,
                                'created_by'      => $this->session->userdata('sso_id'),
                                'created_time'    => date('Y-m-d H:i:s'),
                                'status'          => 1
                            );
                            $this->Common_model->insert_data('asset_status_history',$insert_ash);

                            $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$first_asset));
                            #audit data
                            $old_data = array(
                                'asset_status_id'     => $asset_arr['status'],
                                'availability_status' => $asset_arr['availability_status']
                            );

                            #update asset
                            $update_a = array(
                                'status'              => 1,
                                'availability_status' => 1,
                                'modified_by'         => $this->session->userdata('sso_id'),
                                'modified_time'       => date('Y-m-d H:i:s')
                            );
                            $update_a_where = array('asset_id'=>$first_asset);
                            $this->Common_model->update_data('asset',$update_a,$update_a_where);

                            #audit data
                            $new_data = array(
                                'asset_status_id'     => 1,
                                'availability_status' => 1
                            );
                            $remarks = "Asset is released while shipping ST:".$trow['stn_number'];
                            audit_data('asset_master',$first_asset,'asset',2,'',$new_data,array('asset_id'=>$first_asset),$old_data,$remarks,'',array(),'tool_order',$tool_order_id,$asset_arr['country_id']);
                        }
                    }
                    else if($asset_status == 2)
                    {
                        $ot_assets_arr = array();
                        foreach ($ot_assets as $key3 => $value3) 
                        {
                            $ot_assets_arr[] = $value3['asset_id'];
                        }
                        if(count($ot_assets_arr)>0)
                        {
                            if(!in_array($asset_id, $ot_assets_arr))
                            {
                                if($asset_identity>0)
                                {
                                    $old_ash = $this->Common_model->get_data_row('asset_status_history',array('end_time'=>NULL,'status'=>2,'asset_id'=>$asset_identity));

                                    $new_ash = $this->Common_model->get_data_row('asset_status_history',array('end_time'=>NULL,'status'=>2,'asset_id'=>$asset_id));
                                    if(count($old_ash)>0 && count($new_ash)>0)
                                    {
                                        if($new_ash['tool_order_id']!='' && $new_ash['ordered_tool_id']!='' && $old_ash['tool_order_id']!='' && $old_ash['ordered_tool_id']!='')
                                        {
                                            #update old ASH
                                            $update_ash = array(
                                                'end_time'    => date('Y-m-d H:i:s'),
                                                'modified_by' => $this->session->userdata('sso_id')
                                            );
                                            $update_ash_where = array(
                                                'asset_id' => $asset_id,
                                                'end_time' => NULL
                                            );
                                            $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                                            #insert for old ash
                                            $insert_ash = array(
                                                'asset_id'        => $asset_identity,
                                                'tool_order_id'   => $new_ash['tool_order_id'],
                                                'ordered_tool_id' => $new_ash['ordered_tool_id'],
                                                'created_by'      => $this->session->userdata('sso_id'),
                                                'created_time'    => date('Y-m-d H:i:s'),
                                                'status'          => 2
                                            );
                                            $this->Common_model->insert_data('asset_status_history',$insert_ash);

                                            #update FE ASH
                                            $update_ash = array(
                                                'end_time'    => date('Y-m-d H:i:s'),
                                                'modified_by' => $this->session->userdata('sso_id')
                                            );
                                            $update_ash_where = array(
                                                'asset_id' => $asset_identity,
                                                'end_time' => NULL
                                            );
                                            $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                                            #insert in new FE ash
                                            $insert_ash = array(
                                                'asset_id'        => $asset_id,
                                                'tool_order_id'   => $old_ash['tool_order_id'],
                                                'ordered_tool_id' => $old_ash['ordered_tool_id'],
                                                'created_by'      => $this->session->userdata('sso_id'),
                                                'created_time'    => date('Y-m-d H:i:s'),
                                                'status'          => 2
                                            );
                                            $this->Common_model->insert_data('asset_status_history',$insert_ash);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    /*end of loop*/
                    $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));

                    #audit data
                    $old_data = array(
                        'asset_status_id'     => $asset_arr['status'],
                        'availability_status' => $asset_arr['availability_status'],
                        'tool_availability'   => get_asset_position($asset_id)
                    );

                    $update_a = array(
                        'status'        => 8,
                        'b_inventory'   => 0,
                        'availability_status' => 1,
                        'modified_by'   => $this->session->userdata('sso_id'),
                        'modified_time' => date('Y-m-d H:i:s')
                    );
                    $update_a_where = array('asset_id'=>$asset_id);
                    $this->Common_model->update_data('asset',$update_a,$update_a_where);

                    $update_ash = array(
                        'end_time' => date('Y-m-d H:i:s'),
                        'modified_by' =>$this->session->userdata('sso_id')
                    );
                    $update_ash_where = array(
                        'asset_id' =>$asset_id,
                        'end_time'=>NULL
                    );
                    $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

                    $insert_ash = array(
                        'asset_id'        => $asset_id,
                        'created_by'      => $this->session->userdata('sso_id'),
                        'created_time'    => date('Y-m-d H:i:s'),
                        'tool_order_id'   => $tool_order_id,
                        'current_stage_id'=> 2,
                        'status'          => 8
                    );
                    $this->Common_model->insert_data('asset_status_history',$insert_ash);

                    $update_ap = array('to_date' => date('Y-m-d H:i:s'));
                    $update_ap_w = array(
                        'asset_id' => $asset_id,
                        'to_date'  => NULL
                    );
                    $this->Common_model->update_data('asset_position',$update_ap,$update_ap_w);

                    $sso_arr = $this->Wh_stock_transfer_m->get_fe_sso($tool_order_id);
                    $insert_ap = array(
                        'asset_id'  => $asset_id,
                        'transit'   => 1,
                        'from_date' => date('Y-m-d H:i:s'),
                        'status'    => 1,
                        'sso_id'    => $sso_arr['sso_id']
                    );
                    $this->Common_model->insert_data('asset_position',$insert_ap);

                    #audit data
                    $new_data = array(
                        'asset_status_id'     => 8,
                        'availability_status' => 1,
                        'tool_availability'   => get_asset_position($asset_id)
                    );
                    $remarks = "Asset shipped for ST No:".$trow['stn_number']." And Tool Order No:".$fe_trow['order_number'];
                    audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'tool_order',$trow['tool_order_id'],$trow['country_id']);
                }
            }
        }

        #update tool order
        $update_tool_order = array(
            'current_stage_id' => 2,//at transit for WH TO WH
            'modified_by'      => $this->session->userdata('sso_id'),
            'modified_time'    => date('Y-m-d H:i:s'),
            'status'           => 1
        );
        $update_where = array('tool_order_id' => $tool_order_id);
        $this->Common_model->update_data('tool_order',$update_tool_order,$update_where);

        if($check_st_type == 1)
        {
            $old_data = array('transaction_status' => 'At Wh');
            $new_data = array('transaction_status' => 'In Transit');
            $remarks = "ST:".$trow['stn_number']." is shipped for Tool Order:".$fe_trow['order_number'];
            audit_data('tool_order',$fe_trow['tool_order_id'],'tool_order',2,'',$new_data,array('tool_order_id'=>$fe_trow['tool_order_id']),$old_data,$remarks,'',array(),'tool_order',$fe_trow['tool_order_id'],$fe_trow['country_id']);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'wh_stock_transfer_list'); exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Details Has been Submitted successfully!
            </div>'); 
            redirect(SITE_URL.'wh_st_invoice_print/'.storm_encode($tool_order_id)); 
            exit(); 
        }
    }

    //Closed WH Delivery list
    public function closed_wh_delivery_list()
    {
        $data['nestedView']['heading']="Closed WH Shipment List";
        $data['nestedView']['cur_page'] = "closed_wh_delivery";
        $data['nestedView']['parent_page'] = 'closed_wh_delivery_list';

         #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed WH Shipment List';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed WH Shipment List','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchwhdelivery',TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
           'stn_number' => validate_string($this->input->post('stn_number', TRUE)),
           'to_wh_id'   => validate_string($this->input->post('to_wh_id',TRUE)),
           'country_id' => validate_number(@$this->input->post('country_id',TRUE)),  
           'from_wh_id' => validate_number(@$this->input->post('from_wh_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'stn_number' => $this->session->userdata('stn_number'),
                'to_wh_id'   => $this->session->userdata('to_wh_id'),
                'country_id' => $this->session->userdata('country_id'),
                'from_wh_id' => $this->session->userdata('from_wh_id')
                );
            }
            else 
            {
                $searchParams=array(
                'stn_number'       => '',
                'to_wh_id'         => '',
                'page_redirect_st' =>'',
                'country_id'       => '',
                'from_wh_id'       => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;
        
        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_wh_delivery_list/';
        # Total Records
        $config['total_rows'] = $this->Wh_stock_transfer_m->closed_wh_delivery_list_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['closed_wh_deliveryResults'] = $this->Wh_stock_transfer_m->closed_wh_delivery_list_results($current_offset, $config['per_page'], $searchParams,$task_access);
        //echo '<pre>';print_r($data['closed_wh_deliveryResults']);exit;
        
        # Additional data
        $data['displayResults'] = 1;
        $data['to_wh_list'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['from_wh_list'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access,'status'=>1));
        $data['task_access'] = $task_access;
        $data['current_offset'] = $current_offset;
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $this->load->view('wh_stock_transfer/closed_wh_delivery_list',$data);
    }

    public function view_wh_delivery_details()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_wh_delivery_list'); exit();
        }

        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        if($check_st=='') //stock transfer
        {  
            $check_st_type = 0; //ST
        } 
        else if($check_st !='') //tool order
        { 
            $check_st_type = 1; //order
        } 
        $country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
        $tool_wh_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');

        $data['nestedView']['heading']="Closed WH Delivery Details";
        $data['nestedView']['cur_page'] = "closed_wh_delivery";
        $data['nestedView']['parent_page'] = 'closed_wh_delivery_list';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed WH Delivery Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed WH Delivery List','class'=>'','url'=>SITE_URL.'closed_wh_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed WH Delivery Details','class'=>'active','url'=>'');

        $data['tool_order_id'] = $tool_order_id;
        $data['trow'] = $this->Wh_stock_transfer_m->get_tool_order_details($tool_order_id);
        if($data['trow']['fe_check'] == 1)//fe address
        {
            $sso = $this->Wh_stock_transfer_m->get_fe_sso($tool_order_id);
            $data['sso_name'] = $sso['sso_name'];
        }
        else //wh address
        {
            $data['warehouse'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$data['trow']['to_wh_id']));
        }
        $order_tool = $this->Wh_stock_transfer_m->get_ordered_tool_count($tool_order_id);
        
        $data['order_tool'] = $order_tool;
        $data['flg'] = 1;
        $data['drow'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1));
        $data['attach_document'] = $this->Common_model->get_data('order_delivery_doc',array('order_delivery_id'=>$data['drow']['order_delivery_id']));

        $this->load->view('wh_stock_transfer/closed_wh_delivery_list',$data);
    }

    public function wh_st_invoice_print()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_wh_delivery_list'); exit();
        }
        $_SESSION['page_redirect_st'] = 1;
        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        if($check_st=='')
        { 
             $check_st_type = 0; 
        }
        else
        {
            $check_st_type = 1; 
        }
        $country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
        $data['country_id'] = $country_id;
        $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id),'name');
        $from_address = $this->Wh_stock_transfer_m->get_delivery_details($tool_order_id);
        $data['to_address'] = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id,'status'=>1));

        if($from_address['fe_check']==1)
        {
            $sso = $this->Wh_stock_transfer_m->get_fe_sso($tool_order_id);
            $data['sso_name'] = $sso['sso_name'];
            $data['mobile_no'] = $sso['mobile_no'];
        
        }
        else if($from_address['fe_check']==0)
        {
            $data['to_wh_arr'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['to_wh_id']));
        }
        $tools_list = $this->Wh_stock_transfer_m->get_invoiced_tools($tool_order_id);
        $total_amount = 0;
        foreach ($tools_list as $key => $value)
        {
            $cost = $value['cost_in_inr'];
            $gst_percent = $value['gst_percent'];
            $gst = explode("%", $gst_percent);
            $cgst_percent = round($gst[0]/2,2);
            $cgst_amt = $sgst_amt = $igst_amt = $cgst_percent = $sgst_percent = $igst_percent = 0;
            if($from_address['print_type'] == 3)
            {
                $cgst_percent = round($gst[0]/2,2);
                $cgst_amt = ($cost*$cgst_percent)/100;
                $sgst_percent = $cgst_percent;
                $sgst_amt = $cgst_amt; 
            }
            else if($from_address['print_type'] == 4)
            {
                $igst_percent = $gst[0];
                $igst_amt = ($cost*$igst_percent)/100;   
            }
            $tax_amount = ($cost * $gst[0])/100;
            $amount = $cost+$tax_amount;
            $total_amount+= ($cost+$tax_amount);

            $tools_list[$key]['cgst_percent'] = $cgst_percent;
            $tools_list[$key]['cgst_amt'] = $cgst_amt;
            $tools_list[$key]['sgst_percent'] = $sgst_percent;
            $tools_list[$key]['sgst_amt'] = $sgst_amt;
            $tools_list[$key]['igst_percent'] = $igst_percent;
            $tools_list[$key]['igst_amt'] = $igst_amt;
            $tools_list[$key]['taxable_value'] = $cost;
            $tools_list[$key]['tax_amount'] = $tax_amount;
            $tools_list[$key]['amount'] = $amount;
        }
        $data['total_amount'] = $total_amount;
        $data['from_address'] = $from_address;
        $data['tools_list'] = $tools_list;
        $data['billed_to'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$from_address['billed_to']));
        $this->load->view('wh_stock_transfer/dc_print',$data);  
    }

    public function cancel_wh_print()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_wh_delivery_list'); exit();
        }
        $data['nestedView']['heading']="Cancel Generated Print";
        $data['nestedView']['cur_page'] = "check_wh_st";
        $data['nestedView']['parent_page'] = 'closed_wh_delivery_list';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Cancel Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed WH Shipment List','class'=>'','url'=>SITE_URL.'closed_wh_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Cancel Generated Print','class'=>'active','url'=>'');
        $data['cancel_type_list'] = $this->Common_model->get_data('cancel_type',array('status'=>1));
        $data['tool_order_id'] = $tool_order_id;
        $data['order_number'] = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'stn_number');
        $order_delivery_id = $this->Common_model->get_value('order_delivery',array('tool_order_id'=>$tool_order_id),'order_delivery_id');
        $print_id = $this->Common_model->get_value('order_delivery',array('order_delivery_id'=>$order_delivery_id),'print_id');
        $data['print_type'] = $this->Common_model->get_value('print_format',array('print_id'=>$print_id),'print_type');
         $data['format_number'] = $this->Common_model->get_value('print_format',array('print_id'=>$print_id),'format_number');

        $this->load->view('wh_stock_transfer/cancel_wh_print',$data);
    }

    public function insert_cancel_wh_print()
    {
        $tool_order_id = validate_number(storm_decode($this->input->post('tool_order_id',TRUE)));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_wh_delivery_list'); exit;
        }
        $order_delivery_id = $this->Common_model->get_value('order_delivery',array('tool_order_id'=>$tool_order_id),'order_delivery_id');

        $update_print_history = array('remarks'=>validate_string($this->input->post('reason',TRUE)),
                                      'modified_by' => $this->session->userdata('sso_id'),
                                      'modified_time' => date('Y-m-d H:i:s'));
        $update_print_where = array('order_delivery_id'=>$order_delivery_id,'modified_time'=>NULL);
        $this->db->trans_begin();
        $this->Common_model->update_data('print_history',$update_print_history,$update_print_where);

        $print_type = validate_number($this->input->post('print_type',TRUE));
        $tool_order_wh_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');
        $print_id = get_current_print_id($print_type,$tool_order_wh_id);

        $update_order_delivery = array('print_id'=>$print_id,
                                       'modified_by' => $this->session->userdata('sso_id'),
                                       'modified_time' => date('Y-m-d H:i:s'));
        $update_od_where = array('order_delivery_id'=>$order_delivery_id);
        $this->Common_model->update_data('order_delivery',$update_order_delivery,$update_od_where);

        $insert_print_history = array('print_id' => $print_id,
                                      'remarks'  => 'Print Got updated',
                                      'order_delivery_id' => $order_delivery_id,
                                      'created_by' => $this->session->userdata('sso_id'),
                                      'created_time' => date('Y-m-d H:i:s'));
        $this->Common_model->insert_data('print_history',$insert_print_history);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'closed_wh_delivery_list'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Print Has been Updated successfully!
            </div>'); 
            redirect(SITE_URL.'wh_st_invoice_print/'.storm_encode($tool_order_id)); 
        } 
    }

    public function update_wh_print()
    {
        $tool_order_id = storm_decode($this->uri->segment(2));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_wh_delivery_list'); exit();
        }
        
        $check_st = $this->Common_model->get_value('st_tool_order',array('stock_transfer_id'=>$tool_order_id),'tool_order_id');
        if($check_st=='') //stock transfer
        {  
            $check_st_type = 0; //ST
        } 
        else if($check_st !='') //tool order
        { 
            $check_st_type = 1; //order
        } 
        $country_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'country_id');
        $tool_wh_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$tool_order_id),'wh_id');
        
        $data['nestedView']['heading']="Update Generated Print";
        $data['nestedView']['cur_page'] = "check_wh_st";
        $data['nestedView']['parent_page'] = 'closed_wh_delivery_list';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/update_wh_stock_transfer.js"></script>';

        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Update Generated Print';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed WH Shipment List','class'=>'','url'=>SITE_URL.'closed_wh_delivery_list');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Update Generated Print','class'=>'active','url'=>'');

        $data['tool_order_id'] = $tool_order_id;
        $trow = $this->Common_model->get_data_row('tool_order',array('tool_order_id'=>$tool_order_id));
        $data['order_number'] = $trow['stn_number'];
        $data['od'] = $this->Common_model->get_data_row('order_delivery',array('tool_order_id'=>$tool_order_id));
        $order_delivery_id = $data['od']['order_delivery_id'];
        $print_id = $this->Common_model->get_value('order_delivery',array('order_delivery_id'=>$order_delivery_id),'print_id');
        $prow = $this->Common_model->get_data_row('print_format',array('print_id'=>$print_id));
        $data['print_type'] = $prow['print_type'];
        $data['print_date'] = $prow['created_time'];
        $data['format_number'] = $prow['format_number'];
        $to_wh_id = $trow['to_wh_id'];
        
        $data['order_address'] = $this->Common_model->get_data_row('order_address',array('tool_order_id'=>$tool_order_id,'status'=>1));
        
        
        //$swh_id = $this->session->userdata('swh_id');
        $tool_wh = $trow['wh_id'];
        $data['billed_from'] = $this->Common_model->get_data_row('warehouse',array('wh_id'=>$tool_wh));
        $data['billed_to_list'] = $this->Common_model->get_data('warehouse',array('status'=>1,'country_id '=>$country_id));
        $this->load->view('wh_stock_transfer/update_wh_print',$data);
    }

    public function update_wh_print_details()
    {
        $tool_order_id = validate_number(storm_decode($this->input->post('tool_order_id',TRUE)));
        if($tool_order_id == '')
        {
            redirect(SITE_URL.'closed_wh_delivery_list'); exit();
        }
        $order_delivery_id = $this->Common_model->get_value('order_delivery',array('tool_order_id'=>$tool_order_id),'order_delivery_id');

        $courier_type = validate_number($this->input->post('courier_type',TRUE));
        $courier_name = validate_string($this->input->post('courier_name',TRUE));
        $courier_number = validate_string($this->input->post('courier_number',TRUE));
        $contact_person = validate_string($this->input->post('contact_person',TRUE));
        $phone_number = validate_string($this->input->post('phone_number',TRUE));
        $expected_delivery_date = validate_string(format_date($this->input->post('expected_delivery_date',TRUE)));
        $vehicle_number = validate_string($this->input->post('vehicle_number',TRUE));
        $remarks        = validate_string($this->input->post('remarks',TRUE));
        $date = validate_string($this->input->post('print_date',TRUE));
        $print_date = date('Y-m-d',strtotime($date));
        if($remarks == ""){ $remarks = NULL; }

        if($courier_type == 1)
        {
            $phone_number   = NULL;
            $contact_person = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 2)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
            $vehicle_number = NULL;
        }
        else if($courier_type == 3)
        {
            $courier_name   = NULL;
            $courier_number = NULL;
        }
        $billed_to = validate_number($this->input->post('billed_to',TRUE));
        $update_details = array('tool_order_id' => $tool_order_id,
                                'courier_type'  => $courier_type,
                                'contact_person'=> $contact_person,
                                'phone_number'  => $phone_number,
                                'courier_name'  => $courier_name,
                                'courier_number'=> $courier_number,
                                'vehicle_number'=> $vehicle_number,
                                'expected_delivery_date'=> $expected_delivery_date,
                                'remarks'       => $remarks,
                                'billed_to'     => $billed_to,
                                'modified_by'    => $this->session->userdata('sso_id'),
                                'modified_time'  => date('Y-m-d H:i:s'),
                                'status'        => 1);
        $update_details_where = array('order_delivery_id'=>$order_delivery_id);
        $this->db->trans_begin();
        $this->Common_model->update_data('order_delivery',$update_details,$update_details_where);

         #Update Order Address
        $update_oa = array('address1'      => validate_string($this->input->post('address1',TRUE)),
                           'address2'      => validate_string($this->input->post('address2',TRUE)),
                           'address3'      => validate_string($this->input->post('address3',TRUE)),
                           'address4'      => validate_string($this->input->post('address4',TRUE)),
                           'pin_code'      => validate_string($this->input->post('pin_code',TRUE)),
                           'gst_number'    => validate_string($this->input->post('gst_number',TRUE)),
                           'pan_number'    => validate_string($this->input->post('pan_number',TRUE)),
                           'modified_by'   => $this->session->userdata('sso_id'),
                           'modified_time' => date('Y-m-d H:i:s'));
        $update_oa_where = array('tool_order_id'=>$tool_order_id,'status'=>1);
        $this->Common_model->update_data('order_address',$update_oa,$update_oa_where);

        $print_id = $this->Common_model->get_value('order_delivery',array('tool_order_id'=>$tool_order_id),'print_id');

        #update print date
        update_print_format_date($print_date,$print_id);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'closed_wh_delivery_list'); 
            exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Shipment Details Has been Updated successfully!
            </div>'); 
            redirect(SITE_URL.'wh_st_invoice_print/'.storm_encode($tool_order_id)); 
        } 
    }
}