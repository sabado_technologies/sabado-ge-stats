<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Replacement extends Base_Controller
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Replacement_m');
	}
	// Srilekha
	public function replace_tool()
	{
		$defective_asset_id=@storm_decode($this->uri->segment(2));
        
        if($defective_asset_id=='')
        {
            redirect(SITE_URL.'asset_condition_check'); exit;
        }
        $asset_id=$this->Common_model->get_value('defective_asset',array('defective_asset_id'=>$defective_asset_id),'asset_id');
         # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Replacement Tools";
        $data['nestedView']['cur_page'] = 'check_replacement';
        $data['nestedView']['parent_page'] = 'defective_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.icheck/icheck.min.js"></script>';
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href="'.assets_url().'js/jquery.icheck/skins/square/blue.css" />';

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Replacement Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Defective Asset Requests','class'=>'','url'=>SITE_URL.'defective_asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Replacement Tools','class'=>'active','url'=>'');
        $data['asset_id']=$asset_id;
        $data['defective_asset_id']=$defective_asset_id;
        $data['lrow'] = $this->Replacement_m->asset_details_view($asset_id);
        $data['m_status'] = $this->Common_model->get_value('defective_asset_health',array('defective_asset_id'=>$defective_asset_id),'asset_condition_id');
        
        $subComponent = $this->Replacement_m->get_sub_component_details($asset_id);
        foreach ($subComponent as $key => $value) 
        {
            $row = $this->Common_model->get_data_row('defective_asset_health',array('part_id'=>$value['part_id'],'defective_asset_id'=>$defective_asset_id));
            $asset_condition = $this->Common_model->get_value('asset_condition',array('asset_condition_id'=>$row['asset_condition_id']),'name');
            $subComponent[$key]['asset_condition'] = $asset_condition;
            $subComponent[$key]['asset_condition_id'] = $row['asset_condition_id'];
        }
        $data['subComponent']=$subComponent;
        $this->load->view('replacement/replacement_asset_view',$data);
	}
    // Srilekha
    public function confirm_replacement_tool()
    {
        
        $asset_id=validate_number(storm_decode($this->input->post('asset_id'),TRUE));
        $defective_asset_id=validate_number(storm_decode($this->input->post('defective_asset_id'),TRUE));
        if($asset_id=='' || $defective_asset_id == '')
        {
            redirect(SITE_URL.'defective_asset'); exit();
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Confirm Replacement Tools";
        $data['nestedView']['cur_page'] = 'check_replacement';
        $data['nestedView']['parent_page'] = 'defective_asset';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Confirm Replacement Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Defective Asset Requests','class'=>'','url'=>SITE_URL.'defective_asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Replacement Tools','class'=>'','url'=>SITE_URL.'replace_tool/'.storm_encode($asset_id));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Confirm Replacement Tools','class'=>'active','url'=>'');

        $tool_id = validate_number($this->input->post('tool_id',TRUE));
        $data['asset_id']=$asset_id;
        $data['defective_asset_id']=$defective_asset_id;
        $data['lrow'] = $this->Replacement_m->asset_details_view($asset_id);
        $sub_comp = $this->Replacement_m->get_sub_component_tool($tool_id,$defective_asset_id);
        $country_id = $this->Replacement_m->asset_country($defective_asset_id);
        $data['serial_number']=$this->Replacement_m->get_serial_number($tool_id,$sub_comp['part_id'],$country_id['country_id']);
        $data['sub_comp'] = $sub_comp;

        $this->load->view('replacement/confirm_replacement_asset_view',$data);
    }

    public function submit_replacement()
    {
        $asset_id = validate_number(storm_decode($this->input->post('asset_id'),TRUE));
        $defective_asset_id = validate_number(storm_decode($this->input->post('defective_asset_id'),TRUE));
        if($asset_id == "" || $defective_asset_id == "")
        {
            redirect(SITE_URL.'defective_asset'); exit();
        }

        $old_part_id = validate_number($this->input->post('old_part_id',TRUE));
        $new_part_id = validate_number($this->input->post('new_part_id',TRUE));
        $new_serial = $this->Common_model->get_value('part',array('part_id'=>$old_part_id),'serial_number');
        $new_asset_id = $this->Common_model->get_value('part',array('part_id'=>$new_part_id),'asset_id');

        #audit data
        $asset_arr1 = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $old_data1 = array( 
            'approval_status'     => get_da_status($asset_id,$asset_arr1['approval_status']),
            'asset_status_id'     => $asset_arr1['status'],
            'availability_status' => $asset_arr1['availability_status']
        );

        $asset_arr2 = $this->Common_model->get_data_row('asset',array('asset_id'=>$new_asset_id));
        $old_data2 = array(
            'asset_status_id'     => $asset_arr2['status'],
            'availability_status' => $asset_arr2['availability_status']
        );

        $reason = validate_string($this->input->post('reason',TRUE));

        $insert_replacment = array(
            'asset_id'           => $asset_id,
            'defective_asset_id' => $defective_asset_id,
            'created_by'         => $this->session->userdata('sso_id'),
            'created_time'       => date('Y-m-d H:i:s'),
            'status'             => 1,
            'reason'             => $reason
        );
        $this->db->trans_begin();
        $replacement_id = $this->Common_model->insert_data('replacement',$insert_replacment);

        $insert_replace_tool = array(
            'replacement_id'   => $replacement_id,
            'old_part_id'      => $old_part_id,
            'new_part_id'      => $new_part_id,
            'new_asset_id'     => $new_asset_id,
            'status'           => 1
        );
        $this->Common_model->insert_data('replaced_tool',$insert_replace_tool);


        $re = 'Replaced for '.$new_serial;
        $update_dahealth = array(
            'asset_condition_id' => 1,
            'remarks'            => $re,
            'part_id'            => $new_part_id);
        $update_dahealth_where = array(
            'defective_asset_id' => $defective_asset_id,
            'part_id'            => $old_part_id);
        $this->Common_model->update_data('defective_asset_health',$update_dahealth,$update_dahealth_where);

        $check_dhealth = 0;
        $dhealth = $this->Common_model->get_data('defective_asset_health',array('defective_asset_id'=>$defective_asset_id));
        foreach ($dhealth as $key => $value) 
        {
            if($value['asset_condition_id']!=1)
            {
                $check_dhealth++;
            }
        }
        if($check_dhealth == 0)
        {
            $update_dah = array(
                'status'               => 10,
                'request_handled_type' => 1,
                'modified_by'          => $this->session->userdata('sso_id'),
                'modified_time'        => date('Y-m-d H:i:s')
            );
            $update_dah_where = array('defective_asset_id'=>$defective_asset_id);
            $this->Common_model->update_data('defective_asset',$update_dah,$update_dah_where);
        }

        #update asset with new part id
        $update_old_asset_part = array(
            'asset_id'      => $asset_id,
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $update_oap_where = array('part_id' => $new_part_id);
        $this->Common_model->update_data('part',$update_old_asset_part,$update_oap_where);

        $apart = $this->Common_model->get_data('part',array('asset_id'=>$asset_id));
        $check_apart = 0;
        foreach ($apart as $key => $value) 
        {
            if($value['status']!=1)
            {
                $check_apart++;
            }
        }

        if($check_apart == 0 && $check_dhealth == 0)
        {
            $update_asset = array(
                'status'          => 1,
                'approval_status' => 0,
                'modified_by'     => $this->session->userdata('sso_id'),
                'modified_time'   => date('Y-m-d H:i:s'),
                'availability_status' => 1
            );
            $update_asset_where = array('asset_id' => $asset_id);
            $this->Common_model->update_data('asset',$update_asset,$update_asset_where);

            #audit data
            $new_data1 = array(
                'approval_status'     => get_da_status($asset_id,0),
                'asset_status_id'     => 1,
                'availability_status' => 1
            );
            $remarks1 = 'From Defective Req, Replacement happened';
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data1,array('asset_id'=>$asset_id),$old_data1,$remarks1,'',array(),'replacement',$replacement_id,$asset_arr1['country_id']);

            $update_ash = array('end_time' => date('Y-m-d H:i:s'));
            $update_ash_where = array('asset_id' => $asset_id,'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

            $insert_ash = array(
                'asset_id'     => $asset_id,    
                'status'       => 1,
                'created_by'   => $this->session->userdata('sso_id'),
                'created_time' => date('Y-m-d H:i:s')
            );
            $this->Common_model->insert_data('asset_status_history',$insert_ash);
        }

        $update_new_asset_part = array(
            'asset_id'      => $new_asset_id,
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s'),
            'status'        => 2,
            'remarks'       => 'Replacement happened'
        );
        $update_nap_where = array('part_id' => $old_part_id);
        $this->Common_model->update_data('part',$update_new_asset_part,$update_nap_where);

        $assetpart = $this->Common_model->get_data('part',array('asset_id'=>$new_asset_id));
        $check_assetpart = 0;
        foreach ($assetpart as $key => $value) 
        {
            if($value['status']!=1)
            {
                $check_assetpart++;
            }
        }
        if($check_assetpart > 0)
        {
            $asset_status = $this->Common_model->get_value('asset',array('asset_id'=>$new_asset_id),'status');
            if($asset_status != 5)
            {
                $update_new_asset = array(
                    'status'              => 5,
                    'availability_status' => 2,
                    'modified_by'         => $this->session->userdata('sso_id'),
                    'modified_time'       => date('Y-m-d H:i:s')
                );
                $update_na_where = array('asset_id' => $new_asset_id);
                $this->Common_model->update_data('asset',$update_new_asset,$update_na_where);

                #audit data
                $new_data2 = array(
                    'asset_status_id'     => 5,
                    'availability_status' => 2
                );
                $remarks2 = 'From Defective Req, Replacement happened';
                audit_data('asset_master',$new_asset_id,'asset',2,'',$new_data2,array('asset_id'=>$new_asset_id),$old_data2,$remarks2,'',array(),'replacement',$replacement_id,$asset_arr2['country_id']);

                $update_nash = array('end_time' => date('Y-m-d H:i:s'));
                $update_nash_where = array('asset_id' => $new_asset_id,'end_time'=>NULL);
                $this->Common_model->update_data('asset_status_history',$update_nash,$update_nash_where);

                $insert_nash = array(
                    'asset_id'     => $new_asset_id,    
                    'status'       => 5,
                    'created_by'   => $this->session->userdata('sso_id'),
                    'created_time' => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_data('asset_status_history',$insert_nash);
            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
            redirect(SITE_URL.'defective_asset'); exit(); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Part Is Replaced successfully!
            </div>');
            redirect(SITE_URL.'defective_asset'); exit();
        }
    }
    
    // Srilekha
    public function closed_replacement_tool()
    {
        
        $data['nestedView']['heading']="Closed Replacement Tools";
        $data['nestedView']['cur_page'] = 'closed_replacement_tool';
        $data['nestedView']['parent_page'] = 'closed_replacement_tool';

        #page Authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/repair.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Closed Replacement Tools';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Closed Replacement Tools','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtools', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'serial_number' => validate_string($this->input->post('serial_number', TRUE)),
            'tool_no'       => validate_string($this->input->post('tool_no', TRUE)),
            'tool_desc'     => validate_string($this->input->post('tool_desc',TRUE)),
            'asset_number'  => validate_string($this->input->post('asset_number',TRUE)),
            'country_id'    => validate_number($this->input->post('country_id',TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'serial_number' => $this->session->userdata('serial_number'),
                'tool_no'       => $this->session->userdata('tool_no'),
                'tool_desc'     => $this->session->userdata('tool_desc'),
                'asset_number'  => $this->session->userdata('asset_number'),
                'country_id'    => $this->session->userdata('country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'serial_number'  => '',
                'tool_no'        => '',
                'tool_desc'      => '',
                'asset_number'   => '',
                'country_id'     => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'closed_replacement_tool/';
        # Total Records
        $config['total_rows'] = $this->Replacement_m->closed_tool_total_num_rows($searchParams,$task_access); 

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['closed_results'] = $this->Replacement_m->closed_tool_results($current_offset, $config['per_page'], $searchParams,$task_access);

        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;

        $this->load->view('replacement/closed_replacement_tool_list',$data);
    }
}