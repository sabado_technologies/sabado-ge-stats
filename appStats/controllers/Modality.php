<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Modality extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Modality_model');
	}

	public function modality()
    {
        

        $data['nestedView']['heading']="Manage Modality";
		$data['nestedView']['cur_page'] = 'modality';
		$data['nestedView']['parent_page'] = 'modality';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Manage Modality';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Modality','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchmodality', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                'modality_name' => validate_string($this->input->post('modality_name', TRUE)),
                'modality_code' => validate_string($this->input->post('modality_code', TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'modality_name' => $this->session->userdata('modality_name'),
                    'modality_code' => $this->session->userdata('modality_code')
                );
            }
            else {
                $searchParams=array(
                    'modality_name' => '',
                    'modality_code' => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'modality/';
        # Total Records
        $config['total_rows'] = $this->Modality_model->modality_total_num_rows($searchParams);

        $config['per_page'] =getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['modalityResults'] = $this->Modality_model->modality_results($current_offset, $config['per_page'], $searchParams);
        
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('modality/modality_view',$data);

    }
    public function add_modality()
    {
        $data['nestedView']['heading']="Add New Modality";
		$data['nestedView']['cur_page'] = 'modality';
		$data['nestedView']['parent_page'] = 'modality';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/modality.js"></script>';
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        
		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Add New Modality';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Modality','class'=>'','url'=>SITE_URL.'modality');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Modality','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_modality';
        $data['displayResults'] = 0;
        $this->load->view('modality/modality_view',$data);
    }

    public function insert_modality()
    {
        #page authentication
        $task_access = page_access_check('modality');
    	$modality_name =validate_string($this->input->post('modality_name',TRUE));  
        $modality_code=validate_string($this->input->post('modality_code',TRUE));
        $modality_id=0;
    	$modalityname_available = $this->Modality_model->is_modalitynameExist($modality_name,$modality_id);
        $code_available = $this->Modality_model->is_modalitycodeExist($modality_code,$modality_id);
    	if($modalityname_available>0)
    	{
    		$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-times-circle"></i></div>
		    <strong>Error!</strong> Modality : '.$modality_name.' already exist! please check.
									 </div>'); 
    		redirect(SITE_URL.'modality'); exit();
        }
        if($code_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Modality Code: '.$modality_code.' already exist! please check.</div>'); 
            redirect(SITE_URL.'modality'); exit();
        }   
        $this->db->trans_begin();
    	$data = array(
            'name'         => $modality_name,
            'modality_code'=> $modality_code,
            'created_by'   => $this->session->userdata('sso_id'),
            'created_time' => date('Y-m-d H:i:s')
        );

        $modality_id = $this->Common_model->insert_data('modality',$data);

        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.</div>');  
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Modality has been Added successfully!</div>');
        }
        redirect(SITE_URL.'modality');  
    }

    public function edit_modality()
    {
        $modality_id=@storm_decode($this->uri->segment(2));
        if($modality_id=='')
        {
            redirect(SITE_URL.'modality');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Modality ";
		$data['nestedView']['cur_page'] = 'modality';
		$data['nestedView']['parent_page'] = 'modality';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/modality.js"></script>';
		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Edit Modality ';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Modality','class'=>'','url'=>'modality');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Modality ','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_modality';
        $data['displayResults'] = 0;

        # Data
        $row = $this->Common_model->get_data('modality',array('modality_id'=>$modality_id));
        $data['lrow'] = $row[0];
        $this->load->view('modality/modality_view',$data);
    }

    public function update_modality()
    {
        #page authentication
        $task_access = page_access_check('modality');
        $modality_id= validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($modality_id=='')
        {
            redirect(SITE_URL.'modality');
            exit;
        }

        $mod_arr = $this->Common_model->get_data_row('modality',array('modality_id'=>$modality_id));

        #GETTING INPUT TEXT VALUES
        $modality_name = validate_string($this->input->post('modality_name',TRUE));
        $modality_code= validate_string($this->input->post('modality_code',TRUE));

        #check modality name excluding $modality id record
        $modalityname_available = $this->Modality_model->is_modalitynameExist($modality_name,$modality_id);
        $code_available = $this->Modality_model->is_modalitycodeExist($modality_code,$modality_id);
        if($modalityname_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Modality : '.$modality_name.' already exist! please check.</div>'); 
            redirect(SITE_URL.'modality'); exit();
        }
        if($code_available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Modality Code: '.$modality_code.' already exist! please check.</div>'); 
            redirect(SITE_URL.'modality'); exit();
        }

        $this->db->trans_begin();
        $data=array(
            'name'          => $modality_name,
            'modality_code' => $modality_code,
            'status'        => 1,
            'modified_by'   => $this->session->userdata('sso_id'),
            'modified_time' => date('Y-m-d H:i:s')
        );
        $where = array('modality_id'=> $modality_id);
    	$res = $this->Common_model->update_data('modality',$data,$where);
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check.. </div>');    
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Modality has been Updated successfully!</div>');
        }
        redirect(SITE_URL.'modality');  
    }

    public function deactivate_modality($encoded_id)
    {
        #page authentication
        $task_access = page_access_check('modality');
        $modality_id=@storm_decode($encoded_id);
        if($modality_id==''){
            redirect(SITE_URL.'modality_id');
            exit;
        }
        $where = array('modality_id' => $modality_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('modality',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<div class="icon"><i class="fa fa-check"></i></div>
		<strong>Success!</strong> Modality has been deactivated successfully!
		</div>');
        redirect(SITE_URL.'modality');

    }

    public function activate_modality($encoded_id)
    {
       #page authentication
        $task_access = page_access_check('modality');
        $modality_id=@storm_decode($encoded_id);
        if($modality_id==''){
            redirect(SITE_URL.'modality_id');
            exit;
        }
        $where = array('modality_id' => $modality_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('modality',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<div class="icon"><i class="fa fa-check"></i></div>
		<strong>Success!</strong> Modality has been Activated successfully!
	    </div>');
        redirect(SITE_URL.'modality');
	}

    // checking uniqueness for modality name 
    public function is_modalitynameExist()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $modality_id = validate_number($this->input->post('modality_id',TRUE));
        $result = $this->Modality_model->is_modalitynameExist($name,$modality_id);
        echo $result;
    }
    // checking uniqueness for modality Code 
    public function is_modalitycodeExist()
    {
        $code = validate_string($this->input->post('name',TRUE));
        $modality_id = validate_number($this->input->post('modality_id',TRUE));
        $result = $this->Modality_model->is_modalitycodeExist($code,$modality_id);
        echo $result;
    }
}