<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By Maruthi on 13th June 17 8:30PM

class Asset extends Base_controller 
{
    public  function __construct() 
    {
        parent::__construct();
        $this->load->model('Asset_m');
        $this->load->model('Tool_m');
        $this->load->model('Defective_asset_m');
         $this->load->model('Calibration_certificates_m');
    }

    public function get_asset_master_tools()
    {
        #page authentication
        $task_access = page_access_check('manage_asset');
        $part_name = validate_string($_REQUEST['string']);
        $country_id = validate_number($_REQUEST['part_type']);

        if($task_access == 3 && $_SESSION['header_country_id']!='')
        {
            $country = $_SESSION['header_country_id'];
        }
        elseif($task_access == 1 || $task_access ==2)
        {
            $country = $_SESSION['s_country_id'];
        }
        else
        {
            $country = $country_id;
        }
        $data = get_tools_result($part_name,$country);
        echo json_encode($data);
    }
    
    public function asset()
    {
        $data['nestedView']['heading']="Manage Asset";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Manage Asset';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchasset', TRUE));
        if($psearch!='') 
        {
            $searchParams = array(
                'asset_wh_arr'       => $this->input->post('asset_wh_arr', TRUE),
                'asset_part_no'      => validate_string($this->input->post('asset_part_no', TRUE)),
                'asset_part_desc'    => validate_string($this->input->post('asset_part_desc',TRUE)),
                'a_asset_no'         => validate_string($this->input->post('a_asset_no', TRUE)),
                'asset_status_arr'   => $this->input->post('asset_status_arr', TRUE),
                'asset_modality_arr' => $this->input->post('asset_modality_arr',TRUE),
                'asset_country_id'   => validate_number($this->input->post('asset_country_id',TRUE)) ,
                'serial_no'          => validate_string($this->input->post('serial_no',TRUE)),
                'cal_type_id'        => validate_number($this->input->post('cal_type_id',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                    'asset_wh_arr'       => $this->session->userdata('asset_wh_arr'),
                    'asset_part_no'      => $this->session->userdata('asset_part_no'),
                    'asset_part_desc'    => $this->session->userdata('asset_part_desc'),
                    'a_asset_no'         => $this->session->userdata('a_asset_no'),
                    'asset_status_arr'   => $this->session->userdata('asset_status_arr'),
                    'asset_modality_arr' => $this->session->userdata('asset_modality_arr'),
                    'asset_country_id'   => $this->session->userdata('asset_country_id'),
                    'serial_no'          => $this->session->userdata('serial_no'),
                    'cal_type_id'        => $this->session->userdata('cal_type_id')
                );
            }
            else 
            {
                $searchParams=array(
                    'asset_wh_arr'       => '',
                    'asset_part_no'      => '',
                    'asset_part_desc'    => '',
                    'a_asset_no'         => '',
                    'asset_status_arr'   => '',
                    'asset_modality_arr' => '',
                    'asset_country_id'   => '',
                    'serial_no'          => '',
                    'cal_type_id'        => ''
                );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['searchParams'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'asset/';
        # Total Records
        $config['total_rows'] = $this->Asset_m->asset_total_num_rows($searchParams,$task_access);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['assetResults'] = $this->Asset_m->asset_results($current_offset, $config['per_page'], $searchParams,$task_access);
        # Additional data
        $data['displayResults'] = 1;
        $data['task_access'] = $task_access;
        $data['status_data'] = $this->Common_model->get_data('asset_status');
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['modality_details'] = $this->Common_model->get_data('modality',array('status'=>1));
        $data['wh_details'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access));

        $this->load->view('asset/asset_view',$data);
    }

    public function search_by_part()
    {
        $data['nestedView']['heading']="Manage Asset";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['css_includes'] = array();
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/asset.js"></script>';
       
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Add New Asset';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Asset','class'=>'active','url'=>'');
                
        # Additional data
        $data['search_by_part'] = 1;
        $data['toolDetails'] = array();
        if($task_access == 1 || $task_access== 2)
        {
            $data['toolDetails'] = $this->Common_model->get_data('tool',array('status'=>1,'country_id'=>$this->session->userdata('s_country_id')));
        }
        else if($task_access == 3 && $this->session->userdata('header_country_id')!='')
        {
            $data['toolDetails'] = $this->Common_model->get_data('tool',array('status'=>1,'country_id'=>$this->session->userdata('header_country_id')));
        }
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $data['selected_country'] = $this->session->userdata('header_country_id');
        $data['form_action'] = SITE_URL.'add_asset';
        $data['displayResults'] = 0;
        $this->load->view('asset/asset_view',$data);
    }

    public function get_country_based_tools()
    {
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $tools = $this->Common_model->get_data('tool',array('status'=>1,'country_id'=>$country_id));
        $tool_str = '';
        $tool_str.= "<select name='tool_id' required class='tool_list select3'>";
        $tool_str.="<option value=''>- Select Tool Number/Description -</option>";
        foreach ($tools as $key => $value) 
        {
            $tool_str.='<option value="'.$value['tool_id'].'">'.$value['part_number'].' / '.$value['part_description'].'</option>';  
        }
        $tool_str.="</select>";
        echo $tool_str;
    }

    #koushik
    public function add_asset()
    {
        $tool_id = validate_number($this->input->post('tool_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        if($tool_id == '' || $country_id=='')
        {
            redirect(SITE_URL.'asset'); exit();
        }
        $check_tool = $this->Common_model->get_value('tool',array('tool_id'=>$tool_id,'country_id'=>$country_id),'tool_id');
        if($check_tool=='')
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please Try Again.</div>'); 
            redirect(SITE_URL.'asset'); exit();
        }

        $data['nestedView']['heading']="Add New Asset";
        $data['nestedView']['cur_page'] = 'check_manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        if($task_access == 1 || $task_access == 2)
        {
            $country_id = $this->session->userdata('s_country_id');
        }
        elseif($task_access == 3)
        {
            if($tool_id!='')
            {
                $country_id = $this->input->post('country_id',TRUE);
            }
        }

        # include files
        $data['nestedView']['css_includes'][] = '<link rel="stylesheet" type="text/css" href=" '.assets_url().'js/jquery.icheck/skins/square/blue.css">';

        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/asset.js"></script>';
        $data['nestedView']['css_includes'] = array();
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Add New Asset';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Asset','class'=>'active','url'=>'');

        # Additional data
        $data['trow'] = $this->Asset_m->get_tool_related_info($tool_id);
        $data['eqrow'] = $this->Asset_m->get_eq_model_list($tool_id);
        $data['wh_details'] = $this->Asset_m->get_wh_country_1_2($country_id);
        $data['cal_supplier_details'] = $this->Common_model->get_data('supplier',array('calibration'=>1,'status'=>1,'country_id'=>$country_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>1));
        $data['status_details'] = $this->Common_model->get_data('asset_condition',array('asset_condition_id !='=>3));
        $data['mainComponent'] = $this->Common_model->get_data_row('tool',array('tool_id'=>$tool_id));
        $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id,'status'=>1),'name');
        $data['country_id'] = $country_id;
        $data['task_access'] = $task_access;
        $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
        $data['gps_tool_list'] = $this->Asset_m->get_gps_tracker_list($country_id);
        $data['currencyList'] = $this->Common_model->get_data('currency',array('status'=>1));
        $data['subComponent'] = $this->Asset_m->get_sub_component_details($tool_id);
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_asset';
        $data['displayResults'] = 0;
        $this->load->view('asset/asset_view',$data);
    }

    public function insert_asset()
    {
        #page authentication
        $task_access = page_access_check('manage_asset');
        $tool_id = validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($tool_id == '')
        {
            redirect(SITE_URL.'asset'); exit();
        }

        $tool_arr = $this->Common_model->get_data_row('tool',array('tool_id'=>$tool_id));

        $modality_code = $this->Common_model->get_value('modality',array('modality_id'=>$tool_arr['modality_id']),'modality_code');
        $tool_code = $tool_arr['tool_code'];
        $asset_level = $this->Common_model->get_value('asset_level',array('asset_level_id'=>$tool_arr['asset_level_id']),'name');
        $modality_id = $tool_arr['modality_id'];
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $short_name = $this->Common_model->get_value('location',array('location_id'=>$country_id),'short_name');
        $serial = get_current_serial_number($modality_id,$country_id);
        $serial_number = get_running_sno_five_digit($serial);
        $asset_number = $short_name.$modality_code.$tool_code.$asset_level.$serial_number;

        #check asset_condition
        $asset_condition = '';
        $m_status = $this->input->post('m_asset_condition_id',TRUE);
        if($m_status == 2)
        {
            $asset_condition = 2;
            $status = 5;
            $availability_status = 2;
        }
        $c_status = $this->input->post('c_status',TRUE);
        if(isset($c_status))
        {
            foreach ($c_status as $key => $value) 
            {
                if($value == 2)
                {
                    $asset_condition = 2;
                    $status = 5;
                    $availability_status = 2;
                }
            }
        }
       
        if($asset_condition == '')
        {
            $asset_condition = 1;
            $status = 1;
            $availability_status = 1;
        }

        $m_serial_number = $this->input->post('m_serial_number',TRUE);
        $data = array('serial_number'=>trim($m_serial_number),'tool_id'=>$tool_id,'tool_part_id'=>'','country_id'=>$country_id);
        $result = $this->Asset_m->check_serial_number_availability($data);
        if($result>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Please Fill Unique Serial Number for Main Part</div>'); 
            redirect(SITE_URL.'asset'); exit();
        }

        $c_serial_number = $this->input->post('c_serial_number',TRUE);
        $error = 0;
        if(count($c_serial_number)>0)
        {
            if(in_array($m_serial_number,$c_serial_number))
            {
                $error++;
            }
            if(count(array_unique($c_serial_number))!=count($c_serial_number))
            {
                $error++;
            }
        }

        if($error >0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Please Fill Unique Serial Numbers</div>'); 
            redirect(SITE_URL.'asset'); exit();
        }

        $calibrated_date = validate_string($this->input->post('calibrated_date',TRUE));
        if($calibrated_date == ''){ $calibrated_date = NULL;}
        else { $calibrated_date = date('Y-m-d',strtotime($calibrated_date)); }

        $calibration_due_date = validate_string($this->input->post('calibration_due_date',TRUE));
        if($calibration_due_date == ''){ $calibration_due_date = NULL;}
        else { $calibration_due_date = date('Y-m-d',strtotime($calibration_due_date)); }

        $date_of_use = validate_string($this->input->post('date_of_use',TRUE));
        if($date_of_use == ''){ $date_of_use = NULL;}
        else { $date_of_use = date('Y-m-d',strtotime($date_of_use)); }

        $cal_supplier_id = validate_string($this->input->post('cal_supplier_id',TRUE));
        if($cal_supplier_id == ''){ $cal_supplier_id = NULL;}
        
        $remarks = validate_string($this->input->post('remarks',TRUE));
        if($remarks == ''){ $remarks = NULL;}
        $wh_id = validate_number($this->input->post('wh_id',TRUE));
        $mwh_id = $wh_id;

        #insert asset details
        $asset_data = array(
            'asset_number'       => $asset_number,
            'running_sno'        => $serial,
            'cal_supplier_id'    => $cal_supplier_id,
            'cal_due_date'       => $calibration_due_date,
            'calibrated_date'    => $calibrated_date,
            'remarks'            => $remarks,
            'currency_id'        => validate_number($this->input->post('currency_id',TRUE)),
            'cost'               => validate_number($this->input->post('cost',TRUE)),
            'cost_in_inr'        => validate_number($this->input->post('cost_in_inr',TRUE)),
            'po_number'          => validate_string($this->input->post('po_number',TRUE)),
            'earpe_number'       => validate_string($this->input->post('earpe_number',TRUE)),
            'gps_tool_id'        => validate_number($this->input->post('gps_tool_id',TRUE)),
            'modality_id'        => $modality_id,
            'status'             => $status,
            'availability_status'=> $availability_status,
            'asset_condition_id' => $asset_condition,
            'supplier_id'        => $tool_arr['supplier_id'],
            'wh_id'              => $wh_id,
            'mwh_id'             => $mwh_id,
            'country_id'         => $country_id,
            'b_inventory'        => 1,
            'sub_inventory'      => validate_string($this->input->post('sub_inventory',TRUE)),
            'date_of_use'        => $date_of_use,
            'created_by'         => $this->session->userdata('sso_id'),
            'created_time'       => date('Y-m-d H:i:s')
        );
        $this->db->trans_begin();
        $asset_id = $this->Common_model->insert_data('asset',$asset_data);

        #audit data
        unset($asset_data['mwh_id'],$asset_data['b_inventory']);
        $asset_data['tool_availability'] = NULL;
        $asset_data['asset_status_id'] = $status;
        $remarks = "Asset Creation:".$asset_number;
        $ad_id = audit_data('asset_master',$asset_id,'asset',1,'',$asset_data,array('asset_id'=>$asset_id),array(),$remarks,'',array(),'','',$country_id);

        #insert asset history
        $asset_history = array(
            'wh_id'         => $mwh_id,
            'asset_id'      => $asset_id,
            'sub_inventory' => validate_string($this->input->post('sub_inventory',TRUE)),
            'from_date'     => date('Y-m-d')
        );
        $this->Common_model->insert_data('asset_history',$asset_history);

        #insert cal due date history
        if($cal_supplier_id != '' && $calibration_due_date != '' && $calibrated_date != '')
        {
            $insert_cal_due_date_his = array(
                'calibrated_date' => $calibrated_date,
                'cal_due_date'    => $calibration_due_date,
                'asset_id'        => $asset_id,
                'created_by'      => $this->session->userdata('sso_id'),
                'created_time'    => date('Y-m-d H:i:s'),
                'status'          => 1
            );
            $this->Common_model->insert_data('cal_due_date_history',$insert_cal_due_date_his);
        }

        #insert asset status history
        $insert_asset_status_his = array(
            'status'       => $status,
            'asset_id'     => $asset_id,
            'created_by'   => $this->session->userdata('sso_id'),
            'created_time' => date('Y-m-d H:i:s')
        );
        $this->Common_model->insert_data('asset_status_history',$insert_asset_status_his);

        #insert asset position
        $insert_asset_position = array(
            'asset_id' => $asset_id,
            'wh_id'    => validate_number($this->input->post('wh_id',TRUE)),
            'transit'  => 0,
            'from_date'=> date('Y-m-d H:i:s'),
            'status'   => 1
        );
        $ap_id = $this->Common_model->insert_data('asset_position',$insert_asset_position);
        updatingAuditData($ad_id,array('tool_availability'=>get_asset_position($asset_id)));

        #insert asset documents
        $document_type = $this->input->post('document_type',TRUE);
        foreach ($document_type as $key => $value) 
        {
            if($_FILES['support_document_'.$key]['name']!='')
            {
                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                $config['upload_path'] = asset_document_path();
                $config['allowed_types'] = 'gif|jpg|png|pdf';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('support_document_'.$key);
                $fileData = $this->upload->data();
                $support_document = $config['file_name'];
            }
            else
            {
                $support_document = '';
            }
            $insert_doc = array(
                'document_type_id' => $value,
                'name'             => $support_document,
                'doc_name'         => $_FILES['support_document_'.$key]['name'],
                'asset_id'         => $asset_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s')
            );
            if($value!='' && $support_document != '')
            {
                $this->Common_model->insert_data('asset_document',$insert_doc);
            }
        }

        $part_level_id = $tool_arr['part_level_id'];
        if($part_level_id == 3)
        {
            $part_level = 2;
            $quantity = $this->input->post('m_quantity',TRUE);
        }
        else
        {
            $part_level = 1;
            $quantity = 1;
        }

        #insert main component
        $main_part_data = array(
            'tool_id'       => $tool_id,
            'asset_id'      => $asset_id,
            'serial_number' => validate_string($this->input->post('m_serial_number',TRUE)),
            'status'        => validate_number($this->input->post('m_asset_condition_id',TRUE)),
            'remarks'       => validate_string($this->input->post('m_remarks',TRUE)),
            'quantity'      => $quantity,
            'part_level_id' => $part_level,
            'created_by'    => $this->session->userdata('sso_id'),
            'created_time'  => date('Y-m-d H:i:s'),
            'part_status'   => 1
        );
        $part_id = $this->Common_model->insert_data('part',$main_part_data);

        #audit data
        unset($main_part_data['asset_id']);
        $main_part_data['asset_condition_id'] = validate_number($this->input->post('m_asset_condition_id',TRUE));
        $remarks = "Main Item for Asset".$asset_number;
        audit_data('part',$part_id,'part',1,$ad_id,$main_part_data,array('tool_id'=>$tool_id),array(),$remarks,'',array(),'','',$country_id);

        $c_status  = $this->input->post('c_status',TRUE);
        $c_remarks = $this->input->post('c_remarks',TRUE);

        $toolComponent = $this->Asset_m->get_sub_component_details($tool_id);
        if(count($toolComponent)>0 && count($c_serial_number)>0)
        {
            foreach ($toolComponent as $tc)
            {
                foreach ($c_serial_number as $key => $value) 
                {
                    if($key == $tc['tool_id'])
                    {
                        $component_data = array(
                        'tool_id'       => $tc['tool_id'],
                        'asset_id'      => $asset_id,
                        'serial_number' => $c_serial_number[$tc['tool_id']],
                        'quantity'      => $tc['quantity'],
                        'part_level_id' => 2,
                        'created_by'    => $this->session->userdata('sso_id'),
                        'created_time'  => date('Y-m-d H:i:s'),
                        'status'        => $c_status[$tc['tool_id']],
                        'remarks'       => $c_remarks[$tc['tool_id']],
                        'part_status'   => 1);
                        $part_id = $this->Common_model->insert_data('part',$component_data);

                        #audit data
                        unset($component_data['asset_id']);
                        $component_data['asset_condition_id'] = $c_status[$tc['tool_id']];
                        $remarks = "Child Item for Asset".$asset_number;
                        audit_data('part',$part_id,'part',1,$ad_id,$component_data,array('tool_id'=>$tc['tool_id']),array(),$remarks,'',array(),'','',$country_id);
                    }
                }  
            }  
        }  

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset : <strong>'.$asset_number.'</strong> has been Created successfully!
            </div>');
        }
        if(@$this->input->post('generate_submit_tool',TRUE)!='')
        {
            redirect(SITE_URL.'asset_qr/'.storm_encode($asset_id));
        }
        else
        {
            redirect(SITE_URL.'asset');  
        }       
    }

    public function edit_asset()
    {
        $asset_id=@storm_decode($this->uri->segment(2));
        if($asset_id=='')
        {
            redirect(SITE_URL.'asset');
            exit;
        }        
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Asset Details";
        $data['nestedView']['cur_page'] = 'check_manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/asset.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Edit Asset Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Asset Details','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_asset';
        $data['displayResults'] = 0;
        $data['asset_id'] = $asset_id;

        $tool_id = $this->Asset_m->get_top_tool_id($asset_id);
        $assetDetails = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $data['assetDetails']=$assetDetails;
        $country_id=$assetDetails['country_id'];
        $data['country_id']=$country_id;
        $data['country_name'] = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
        $data['trow'] = $this->Asset_m->get_tool_related_info($tool_id);
        $data['eqrow'] = $this->Asset_m->get_eq_model_list($tool_id);
        $data['wh_details'] = $this->Asset_m->get_wh_country_1_2($country_id);
        $data['cal_supplier_details'] = $this->Common_model->get_data('supplier',array('calibration'=>1,'status'=>1,'country_id'=>$country_id));
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>1));
        $data['gps_tool_list'] = $this->Asset_m->get_gps_tracker_list($country_id);
        if($assetDetails['gps_tool_id']!='')
        {
            $gps_list = $this->Common_model->get_data_row('gps_tool',array('gps_tool_id'=>$assetDetails['gps_tool_id']));
            $count = count($data['gps_tool_list']);
            $data['gps_tool_list'][$count] = $gps_list;
        }
        $status = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id,'country_id'=>$country_id),'status');
        if($status == 7)
        {
             $data['status_details'] = $this->Common_model->get_data('asset_condition');
        }
        else
        {
             $data['status_details'] = $this->Common_model->get_data('asset_condition',array('asset_condition_id !='=>3));
        }
       
        $data['mainComponent'] = $this->Common_model->get_data_row('tool',array('tool_id'=>$tool_id));        
        $data['m_part_id'] = $this->Common_model->get_value('part',array('asset_id'=>$asset_id,'tool_id'=>$tool_id),'part_id');        
        $subComponent = $this->Asset_m->get_sub_component_details($tool_id);
        $data['subComponent'] = $subComponent;

        
        $data['currencyList'] = $this->Common_model->get_data('currency',array('status'=>1));
        $data['toolDetails'] = $this->Common_model->get_data_row('part',array('tool_id'=>$tool_id,'asset_id'=>$asset_id));
        $subComp = array();
        foreach ($subComponent as $key => $value) 
        {
            $row = $this->Common_model->get_data_row('part',array('tool_id'=>$value['tool_id'],'asset_id'=>$asset_id,'part_status'=>1));
            if(count($row)>0)
            {
                $subComp[$value['tool_id']]['serial_number'] = $row['serial_number'];
                $subComp[$value['tool_id']]['status'] = $row['status'];
                $subComp[$value['tool_id']]['remarks'] = $row['remarks'];
                $subComp[$value['tool_id']]['part_id'] = $row['part_id'];
            }
            else
            {
                $part_id2 = $this->Common_model->get_value('part',array('tool_id'=>$value['tool_id'],'asset_id'=>$asset_id,'part_status'=>2),'part_id');
                $subComp[$value['tool_id']]['serial_number'] = '';
                $subComp[$value['tool_id']]['status'] = '';
                $subComp[$value['tool_id']]['remarks'] = '';
                $subComp[$value['tool_id']]['part_id'] = $part_id2;
            }
            
        }
        $data['subComp'] = $subComp;

        $attach_document = $this->Common_model->get_data('asset_document',array('asset_id'=>$asset_id));
        $data['count_c'] = count($attach_document);
        $data['currency_name'] = $this->Common_model->get_value('currency',array('location_id'=>$country_id,'status'=>1),'name');
        $data['attach_document'] = $attach_document; 

        $this->load->view('asset/asset_view',$data);
    }

    public function change_status_to_at_wh()
    {
        $asset_id = storm_decode($this->uri->segment(2));
        if($asset_id=='')
        {
            redirect(SITE_URL.'asset'); exit();
        }

        /*check asset is involved in ST or not */
        $st_entry = check_for_st_entry($asset_id);
        if(count($st_entry)>0)
        {
            $stn_number = $st_entry['stn_number'];
            $asset_number = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'asset_number');
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please remove asset from ST, If asset is not available Cancel ST request to proceed !.</div>'); 
            redirect(SITE_URL.'asset'); exit();
        }

        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $s_array = array(2,5,6,7,9,11);
        if(in_array($asset_arr['status'], $s_array) && $asset_arr['approval_status']==0)
        {
            #audit data
            $old_data = array('asset_status_id'=>$asset_arr['status'],
                'tool_availability' => get_asset_position($asset_id));

            $this->db->trans_begin();
            #udpate asset status history
            $update_ash = array('end_time' => date('Y-m-d H:i:s'));
            $update_ash_where = array('asset_id' => $asset_id,'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$update_ash,$update_ash_where);

            $insert_ash = array(
                'asset_id'     => $asset_id,
                'status'       => 1,
                'created_time' => date('Y-m-d H:i:s'),
                'created_by'   => $this->session->userdata('sso_id')
            );
            $this->Common_model->insert_data('asset_status_history',$insert_ash);

            #update asset status value to 1
            $update_a = array(
                'status'        => 1,
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $update_a_where = array('asset_id'=>$asset_id);
            $this->Common_model->update_data('asset',$update_a,$update_a_where);

            #asset position
            $update_asset_position = array('to_date' => date('Y-m-d H:i:s'));
            $update_w = array('asset_id'=>$asset_id, 'to_date'=>NULL);
            $this->Common_model->update_data('asset_position',$update_asset_position,$update_w);

            $insert_asset_position = array(
                'asset_id'  => $asset_id,
                'transit'   => 0,
                'wh_id'     => $asset_arr['wh_id'],
                'from_date' => date('Y-m-d H:i:s'),
                'status'    => 1
            );
            $this->Common_model->insert_data('asset_position',$insert_asset_position);

            #change line item to active
            $part_arr = $this->Common_model->get_data('part',array('asset_id'=>$asset_id,'part_status'=>1));
            foreach ($part_arr as $key => $value) 
            {
                $this->Common_model->Update_data('part',array('status'=>1),array('part_id'=>$value['part_id']));
            }

            #audit data
            $new_data = array(
                'asset_status_id'   => 1,
                'tool_availability' => get_asset_position($asset_id)
            );
            $remarks = "Changed Status to 'At Wh' for Asset : ".$asset_arr['asset_number']." by using Release Btn";
            audit_data('asset_master',$asset_id,'asset',2,'',$new_data,array('asset_id'=>$asset_id),$old_data,$remarks,'',array(),'','',$asset_arr['country_id']);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check. </div>');
            }
            else
            {
                $asset_number = $asset_arr['asset_number'];
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Asset : <strong>'.$asset_number.'</strong> has been updated successfully to status <strong> At Wh</strong>! </div>');
            }
        }
        else
        {
            $asset_number = $asset_arr['asset_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Cannot Update Asset status for :<strong>'.$asset_number.'</strong>. Something went wrong! please check. </div>');
        }
        redirect(SITE_URL.'asset'); exit(); 
    }

    public function update_asset()
    {
        #page authentication
        $task_access = page_access_check('manage_asset');

        $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));        
        if($asset_id==''){ redirect(SITE_URL.'asset'); exit; }

        $tool_id = $this->Asset_m->get_top_tool_id($asset_id);
        $tool_arr = $this->Common_model->get_data_row('tool',array('tool_id'=>$tool_id));
        $country_id = validate_number($this->input->post('country_id'));
        $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
        $asset_arr['tool_availability'] = get_asset_position($asset_id);
        $asset_arr['asset_status_id'] = $asset_arr['status'];
        $asset_info = $asset_arr;

        $status_array = array(1,2,5,6,7,9,11);
        if(in_array($asset_arr['status'],$status_array))
        {
            $asset_condition = '';
            $m_status = validate_number($this->input->post('m_asset_condition_id',TRUE));
            if($m_status == 2)
            {
                $asset_condition = 2;
                $status = 5;
            }
            if($m_status == 3)
            {
                $asset_condition = 3;
                $status = 7;
            }

            $c_status = $this->input->post('c_status',TRUE);
            if(isset($c_status))
            {
                foreach ($c_status as $key => $value) 
                {
                    if($value == 2)
                    {
                        $asset_condition = 2;
                        $status = 5;
                    }
                    if($value == 3)
                    {
                        $asset_condition = 3;
                        $status = 7;
                    }
                }
            }
            if($asset_condition == '')
            {
                $asset_condition = 1;
                $status = 1;
            }
        }
        else
        {
            $asset_condition = $asset_info['asset_condition_id'];
            $status = $asset_info['status'];
        }

        $availability_status = $this->Common_model->get_value('asset_status',array('asset_status_id'=>$status),'type');
        $m_serial_number = $this->input->post('m_serial_number',TRUE);
        $part_type_id = $this->Common_model->get_value('part',array('tool_id'=>$tool_id,'asset_id'=>$asset_id,'part_level_id'=>1),'part_id');
        $data = array('serial_number'=>trim($m_serial_number),'tool_id'=>$tool_id,'tool_part_id'=>$part_type_id,'country_id'=>$country_id);
        $result = $this->Asset_m->check_serial_number_availability($data);
        if($result>0)
        {
            $asset_number = $asset_arr['asset_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>In Asset:<strong>'.$asset_number.'</strong> Please Fill Unique Serial Number for Main Part!</div>'); 
            redirect(SITE_URL.'asset'); exit();
        }
        $c_serial_number = $this->input->post('c_serial_number',TRUE);
        $error = 0;
        if(count($c_serial_number)>0)
        {
            if(in_array($m_serial_number,$c_serial_number))
            {
                $error++;
            }
            if(count(array_unique($c_serial_number))!=count($c_serial_number))
            {
                $error++;
            }
        }
        
        if($error >0)
        {
            $asset_number = $asset_arr['asset_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> In Asset:<strong>'.$asset_number.'</strong> Please Fill Unique Serial Number for Child Parts!</div>'); 
            redirect(SITE_URL.'asset'); exit();
        }

        $calibrated_date = validate_string($this->input->post('calibrated_date',TRUE));
        if($calibrated_date == ''){ $calibrated_date = NULL;}
        else { $calibrated_date = date('Y-m-d',strtotime($calibrated_date)); }

        $calibration_due_date = validate_string($this->input->post('calibration_due_date',TRUE));
        if($calibration_due_date == ''){ $calibration_due_date = NULL;}
        else { $calibration_due_date = date('Y-m-d',strtotime($calibration_due_date)); }

        $date_of_use = validate_string($this->input->post('date_of_use',TRUE));
        if($date_of_use == ''){ $date_of_use = NULL;}
        else { $date_of_use = date('Y-m-d',strtotime($date_of_use)); }

        $cal_supplier_id = validate_number($this->input->post('cal_supplier_id',TRUE));
        if($cal_supplier_id == ''){ $cal_supplier_id = NULL;}
        $remarks = $this->input->post('remarks',TRUE);
        if($remarks == ''){ $remarks = NULL;}

        $wh_id = validate_number($this->input->post('wh_id',TRUE));
        $mwh_id = $wh_id;
        $sub_inventory = validate_string($this->input->post('sub_inventory',TRUE));
        if($asset_arr['mwh_id'] != $mwh_id)
        {
            $b_inventory = 0;
        }
        else
        {
            $b_inventory = 1;
        }

        #update asset position
        $status_arr = array(5,6,7,9,11);
        if($asset_arr['wh_id']!=$wh_id || in_array($asset_info['status'], $status_arr))
        {
            $update_asset_position = array('to_date' => date('Y-m-d H:i:s'));
            $update_w = array('asset_id'=>$asset_id, 'to_date'=>NULL);
            $this->Common_model->update_data('asset_position',$update_asset_position,$update_w);

            $insert_asset_position = array(
                'asset_id'  => $asset_id,
                'transit'   => 0,
                'wh_id'     => $wh_id,
                'from_date' => date('Y-m-d H:i:s'),
                'status'    => 1
            );
            $this->Common_model->insert_data('asset_position',$insert_asset_position);
        }

        #update asset details
        $asset_data = array(
            'cal_supplier_id'    => $cal_supplier_id,
            'cal_due_date'       => $calibration_due_date,
            'calibrated_date'    => $calibrated_date,
            'supplier_id'        => $tool_arr['supplier_id'],
            'wh_id'              => $wh_id,
            'sub_inventory'      => $sub_inventory,
            'asset_condition_id' => $asset_condition,
            'mwh_id'             => $mwh_id,
            'b_inventory'        => $b_inventory,
            'country_id'         => $country_id,
            'date_of_use'        => $date_of_use,
            'currency_id'        => validate_number($this->input->post('currency_id',TRUE)),
            'cost'               => validate_number($this->input->post('cost',TRUE)),
            'cost_in_inr'        => validate_number($this->input->post('cost_in_inr',TRUE)),
            'po_number'          => validate_string($this->input->post('po_number',TRUE)),
            'earpe_number'       => validate_string($this->input->post('earpe_number',TRUE)),
            'gps_tool_id'        => validate_number($this->input->post('gps_tool_id',TRUE)),
            'modified_by'        => $this->session->userdata('sso_id',TRUE),
            'modified_time'      => date('Y-m-d H:i:s'),
            'remarks'            => validate_string($this->input->post('remarks',TRUE)), 
            'status'             => $status,
            'availability_status'=> $availability_status
        );

        $this->db->trans_begin();
        $update_where = array('asset_id'=>$asset_id);
        $this->Common_model->update_data('asset',$asset_data,$update_where);

        #audit data
        unset($asset_data['mwh_id'],$asset_data['b_inventory'],$asset_data['modified_by'],$asset_data['modified_time'],$asset_data['status']);
        $asset_data['tool_availability'] = get_asset_position($asset_id);
        $asset_data['asset_status_id'] = $status;
        $final_arr = array_diff_assoc($asset_data, $asset_arr);
        $remarks = "Modification in Asset:".$asset_arr['asset_number'];
        if(count($final_arr)>0)
        {
            $ad_id = audit_data('asset_master',$asset_id,'asset',2,'',$final_arr,array('asset_id'=>$asset_id),$asset_arr,$remarks,'',array(),'','',$country_id);
        }

        #update asset history
        if($asset_arr['mwh_id'] != $mwh_id || $asset_arr['sub_inventory'] != $sub_inventory)
        {
            #update old record
            $update_data1 = array('to_date'=>date('Y-m-d H:i:s'));
            $update_where1 = array('asset_id'=>$asset_id,'to_date'=>NULL);
            $this->Common_model->update_data('asset_history',$update_data1,$update_where1);

            # insert record
            $insert_data1 = array('wh_id'         => $mwh_id,
                                  'sub_inventory' => $sub_inventory,
                                  'asset_id'      => $asset_id,
                                  'from_date'     => date('Y-m-d H:i:s'));
            $this->Common_model->insert_data('asset_history',$insert_data1);
        }

        #update cal due date history
        if(@$cal_type_id == 1)
        {
            if($asset_arr['calibrated_date']!= $calibrated_date || $asset_arr['cal_due_date'] != $calibration_due_date)
            {
                $insert_cal_due_date_his = array(
                    'calibrated_date' => $calibrated_date,
                    'cal_due_date'    => $calibration_due_date,
                    'asset_id'        => $asset_id,
                    'created_by'      => $this->session->userdata('sso_id'),
                    'created_time'    => date('Y-m-d H:i:s'),
                    'status'          => 1
                );
                $this->Common_model->insert_data('cal_due_date_history',$insert_cal_due_date_his);
            }
        }

        #update asset status history
        if($status != $asset_arr['status'])
        {
            $update_ass_status_his = array('end_time' => date('Y-m-d H:i:s'));
            $update_asset_where = array('asset_id' => $asset_id,'end_time'=>NULL);
            $this->Common_model->update_data('asset_status_history',$update_ass_status_his,$update_asset_where);

            $insert_ash = array(
                'asset_id'     => $asset_id,
                'status'       => $status,
                'created_time' => date('Y-m-d H:i:s'),
                'created_by'   => $this->session->userdata('sso_id')
            );
            $this->Common_model->insert_data('asset_status_history',$insert_ash);
        }

        #insert asset documents
        $document_type = $this->input->post('document_type',TRUE);
        foreach ($document_type as $key => $value) 
        {
            if($_FILES['support_document_'.$key]['name']!='')
            {
                $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                $config['upload_path'] = asset_document_path();
                $config['allowed_types'] = 'gif|jpg|png|pdf';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('support_document_'.$key);
                $fileData = $this->upload->data();
                $support_document = $config['file_name'];
            }
            else
            {
                $support_document = '';
            }
            $insert_doc = array(
                'document_type_id' => $value,
                'name'             => $support_document,
                'doc_name'         => $_FILES['support_document_'.$key]['name'],
                'asset_id'         => $asset_id,
                'created_by'       => $this->session->userdata('sso_id'),
                'created_time'     => date('Y-m-d H:i:s')
            );
            if($value!='')
            {
                $this->Common_model->insert_data('asset_document',$insert_doc);
            }
        }

        #checking asset previous status is matching or not, then only allowing to update line item details
        if(in_array($asset_arr['status'],$status_array))
        {
            $part_level_id = $tool_arr['part_level_id'];
            if($part_level_id == 3)
            {
                $part_level = 2;
                $quantity = validate_number($this->input->post('m_quantity',TRUE));
            }
            else
            {
                $part_level = 1 ;
                $quantity = 1;
            }

            if($m_status ==1)
            {
                $remarks = NULL;
            }
            else
            {
                $remarks = validate_string($this->input->post('m_remarks',TRUE));
            }
            #update main component
            $m_serial_number = validate_string($this->input->post('m_serial_number',TRUE));
            $part_id = $this->Common_model->get_value('part',array('tool_id'=>$tool_id,'asset_id'=>$asset_id,'part_level_id'=>1),'part_id');

            $main_part_arr = $this->Common_model->get_data_row('part',array('part_id'=>$part_id));
            $main_part_arr['asset_condition_id'] = $main_part_arr['status'];

            $main_part_data = array(
                'serial_number' => $m_serial_number,
                'status'        => $this->input->post('m_asset_condition_id',TRUE),
                'remarks'       => $remarks,
                'quantity'      => $quantity,
                'part_level_id' => $part_level,
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s'),
                'part_status'   => 1
            );
            $update_where2 = array('asset_id'=>$asset_id,'tool_id'=>$tool_id);
            $this->Common_model->update_data('part',$main_part_data,$update_where2);

            #audit data
            unset($main_part_data['asset_id'],$main_part_data['modified_by'],$main_part_data['modified_time'],$main_part_data['status']);
            $main_part_data['asset_condition_id'] = validate_number($this->input->post('m_asset_condition_id',TRUE));
            $final_arr2 = array_diff_assoc($main_part_data, $main_part_arr);
            if(count($final_arr2)>0)
            {
                if(!isset($ad_id))
                {
                    $ad_id = audit_data('asset_master',$asset_id,'asset',2,'',array(),array('asset_id'=>$asset_id),array(),'','',array(),'','',$country_id);
                }
                $remarks = "Main Item for asset:".$asset_arr['asset_number'];
                audit_data('part',$part_id,'part',2,$ad_id,$final_arr2,array('tool_id'=>$main_part_arr['tool_id']),$main_part_arr,$remarks,'',array(),'','',$country_id);
            }
            
            $c_serial_number = $this->input->post('c_serial_number',TRUE);
            $c_status        = $this->input->post('c_status',TRUE);
            $c_remarks       = $this->input->post('c_remarks',TRUE);

            $toolComponent = $this->Common_model->get_data('tool_part',array('tool_main_id'=>$tool_id,'status'=>1));

            if(count($toolComponent)>0 && count($c_serial_number)>0)
            {
                foreach ($toolComponent as $tc)
                {
                    $tool_sub_id = $tc['tool_sub_id'];
                    $part_id = $this->Common_model->get_value('part',array('asset_id'=>$asset_id,'tool_id'=>$tool_sub_id,'status'=>1),'part_id');

                    if($part_id!='' && isset($c_serial_number[$tool_sub_id]))//update
                    {
                        $sub_part_arr = $this->Common_model->get_data_row('part',array('part_id'=>$part_id));
                        $sub_part_arr['asset_condition_id'] = $sub_part_arr['status'];

                        $serial_number = $c_serial_number[$tool_sub_id];
                        $status = $c_status[$tool_sub_id];
                        if($status ==1)
                        {
                            $remarks1 = NULL;
                        }
                        else
                        {
                            $remarks1 = $c_remarks[$tool_sub_id];
                        }
                        $component_data = array(
                            'serial_number' => $serial_number,
                            'quantity'      => $tc['quantity'],
                            'part_level_id' => 2,
                            'modified_by'   => $this->session->userdata('sso_id'),
                            'modified_time' => date('Y-m-d H:i:s'),
                            'status'        => $status,
                            'remarks'       => $remarks1,
                            'part_status'   => 1
                        );
                        $update_where3 = array('asset_id'=>$asset_id,'tool_id'=>$tool_sub_id);
                        $this->Common_model->update_data('part',$component_data,$update_where3);

                        #audit data
                        unset($component_data['modified_time'],$component_data['modified_by'],$component_data['status']);
                        $component_data['asset_condition_id'] = $status;
                        $final_arr3 = array_diff_assoc($component_data, $sub_part_arr);
                        if(count($final_arr3)>0)
                        {
                            if(!isset($ad_id))
                            {
                                $ad_id = audit_data('asset_master',$asset_id,'asset',2,'',array(),array('asset_id'=>$asset_id),array(),'','',array(),'','',$country_id);
                            }
                            $remarks = "Child Item for asset:".$asset_arr['asset_number'];
                            audit_data('part',$part_id,'part',2,$ad_id,$final_arr3,array('tool_id'=>$tool_sub_id),$sub_part_arr,$remarks,'',array(),'','',$country_id);
                        }
                    }
                    else if(isset($c_serial_number[$tool_sub_id]))//insert
                    {
                        $serial_number = $c_serial_number[$tool_sub_id];
                        $status = $c_status[$tool_sub_id];
                        if($status ==1)
                        {
                            $remarks1 = NULL;
                        }
                        else
                        {
                            $remarks1 = $c_remarks[$tool_sub_id];
                        }

                        $component_data = array(
                            'tool_id'       => $tool_sub_id,
                            'asset_id'      => $asset_id,
                            'serial_number' => $serial_number,
                            'quantity'      => $tc['quantity'],
                            'part_level_id' => 2,
                            'created_by'    => $this->session->userdata('sso_id'),
                            'created_time'  => date('Y-m-d H:i:s'),
                            'status'        => $status,
                            'remarks'       => $remarks1,
                            'part_status'   => 1
                        );
                        $part_id = $this->Common_model->insert_data('part',$component_data);

                        #audit data
                        unset($component_data['asset_id']);
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('asset_master',$asset_id,'asset',2,'',array(),array('asset_id'=>$asset_id),array(),'','',array(),'','',$country_id);
                        }
                        $remarks = "Child Item for asset:".$asset_arr['asset_number'];
                        audit_data('part',$part_id,'part',1,$ad_id,$component_data,array('tool_id'=>$tool_sub_id),array(),$remarks,'',array(),'','',$country_id);
                    }
                    else if($part_id!='' && !isset($c_serial_number[$tool_sub_id]))//removed
                    {
                        $update_p = array('part_status'=>2);
                        $update_p_where = array('part_id'=>$part_id,'asset_id'=>$asset_id);
                        $this->Common_model->update_data('part',$update_p,$update_p_where);

                        #audit data
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('asset_master',$asset_id,'asset',2,'',array(),array('asset_id'=>$asset_id),array(),'','',array(),'','',$country_id);
                        }
                        $remarks = "Removed from Asset:".$asset_arr['asset_number'];
                        audit_data('part',$part_id,'part',3,$ad_id,array(),array('tool_id'=>$tool_sub_id),$update_p,$remarks,'',array(),'','',$country_id);
                    }
                }
            }
            else if(count($toolComponent)>0)//all sub components are removed
            {
                foreach ($toolComponent as $key => $value) 
                {
                    $tool_sub_id = $value['tool_sub_id'];
                    $part_id = $this->Common_model->get_value('part',array('asset_id'=>$asset_id,'tool_id'=>$tool_sub_id,'status'=>1),'part_id');
                    if($part_id!='')
                    {
                        $update_p = array('part_status'=>2);
                        $update_p_where = array('part_id'=>$part_id,'asset_id'=>$asset_id);
                        $this->Common_model->update_data('part',$update_p,$update_p_where);

                        #audit data
                        if(!isset($ad_id))
                        {
                            $ad_id = audit_data('asset_master',$asset_id,'asset',2,'',array(),array('asset_id'=>$asset_id),array(),'','',array(),'','',$country_id);
                        }
                        $remarks = "Removed From Asset:".$asset_arr['asset_number'];
                        audit_data('part',$part_id,'part',3,$ad_id,array(),array('tool_id'=>$tool_sub_id),$update_p,$remarks,'',array(),'','',$country_id);
                    }
                }
            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check. </div>'); 
            redirect(SITE_URL.'edit_asset/'.@storm_encode($asset_id));  
        }
        else
        {
            $asset_number = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'asset_number');
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Asset : <strong>'.$asset_number.'</strong> has been updated successfully! </div>');
            redirect(SITE_URL.'asset');  
        }        
    }

    public function assetview_details()
    {
        $asset_id=@storm_decode($this->uri->segment(2));
        $defective_asset_id=@storm_decode($this->uri->segment(3)); // to add extra remarks in view page for to write missed remarks
        if($asset_id == '')
        {
            redirect(SITE_URL.'asset'); exit();
        }
        if(@$defective_asset_id!='')
        {
            $data['defective_asset_id'] = $defective_asset_id;
        }

        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="View Asset Details";
        if(@$defective_asset_id!='')
        {
            $data['nestedView']['cur_page'] = 'missed_asset';
            $data['nestedView']['parent_page'] = 'missed_asset';
        }
        else
        {
            $data['nestedView']['cur_page'] = 'manage_asset';
            $data['nestedView']['parent_page'] = 'manage_asset';
        }

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        if(@$defective_asset_id!='')
        {
            $data['nestedView']['breadCrumbTite'] = 'Missed Asset Details';
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Missed Asset Requests','class'=>'active','url'=>SITE_URL.'missed_asset');
        }
        else
        {
            $data['nestedView']['breadCrumbTite'] = 'View Asset Details';
            $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'active','url'=>SITE_URL.'asset');
        }
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Asset Details','class'=>'','url'=>'');

        #Data
        $data['asset_id']= $asset_id;
        $tool_id = $this->Asset_m->get_top_tool_id($asset_id);
        
        if(@$defective_asset_id =='')
        {
            $data['lrow'] = $this->Asset_m->asset_details_view($asset_id,$tool_id);
            $data['subComponent'] = $this->Asset_m->get_l1_sub_component_details($asset_id);
        }
        else
        {
            $data['lrow'] = $this->Defective_asset_m->asset_details_view($asset_id,$tool_id,$defective_asset_id);
            $data['subComponent'] = $this->Defective_asset_m->get_asset_part_list($defective_asset_id,1);
        }
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1));
        $asset_doc = $this->Common_model->get_data('asset_document',array('asset_id'=>$asset_id,'status'=>1));

        $asset_rc_doc = $this->Calibration_certificates_m->get_asset_rc_documents($asset_id);
        $asset_rca_doc = $this->Calibration_certificates_m->get_asset_rca_documents($asset_id);
        $asset_rc_asset_doc=array_merge($asset_doc,$asset_rc_doc);
        $asset_rc_rca_asset_doc=array_merge($asset_rc_asset_doc,$asset_rca_doc);
        $data['attach_document'] = $asset_rc_rca_asset_doc;
        #display partial tolerance detials
        $data['display_cr_remarks'] = get_cr_remarks($asset_id);

        $this->load->view('asset/asset_details_view',$data);
    }

    public function check_serial_number_availability()
    {
        $serial_number = validate_string($this->input->post('serial_number',TRUE));
        $tool_id = validate_number($this->input->post('tool_id',TRUE));
        $tool_part_id = validate_number($this->input->post('tool_part_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $data = array('serial_number'=>trim($serial_number),'tool_id'=>$tool_id,'tool_part_id'=>$tool_part_id,'country_id'=>$country_id);
        $result = $this->Asset_m->check_serial_number_availability($data);
        echo $result; 
    }

    public function deactivate_attached_record()
    {
        $asset_doc_id = validate_number($this->input->post('asset_doc_id',TRUE));
        $update_data = array('status'=>2);
        $update_where = array('asset_doc_id'=>$asset_doc_id);
        $res = $this->Common_model->update_data('asset_document',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function activate_attached_record()
    {
        $asset_doc_id = validate_number($this->input->post('asset_doc_id',TRUE));
        $update_data = array('status'=>1);
        $update_where = array('asset_doc_id'=>$asset_doc_id);
        $res = $this->Common_model->update_data('asset_document',$update_data,$update_where);
        if($res>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function deactivate_asset($encoded_id)
    {
        redirect(SITE_URL.'asset');
    }


    public function wge_bulkupload_asset()
    {
        $data['failed_upload'] = @storm_decode($this->uri->segment(2));
        //echo $data['failed_upload'] ;exit;
        $data['nestedView']['heading']="Bulk Upload Asset";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Bulk Upload Asset';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Bulk Upload Asset','class'=>'active','url'=>'');        
        
        $data['status'] = $this->Common_model->get_data('asset_status',array('type!='=>0));
        $data['supplier'] = $this->Common_model->get_data('supplier',array('status'=>1,'sup_type_id'=>1));        
        $data['cal_supplier'] = $this->Common_model->get_data('supplier',array('status'=>1,'calibration'=>1,'task_access'=>$task_access)); 
        $data['form_action'] =SITE_URL.'wge_submit_assetupload';
        $data['branch'] = $this->Common_model->get_data('warehouse',array('task_access'=>$task_access));
        $this->load->view('asset/wge_asset_upload_view',$data);
    }

    public function asset_wge_bulkupload_asset()
    {    
        $data['failed_upload'] = @storm_decode($this->uri->segment(2));
        //echo $data['failed_upload'] ;exit;
        $data['nestedView']['heading']="Bulk Upload Asset";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Bulk Upload Asset';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Bulk Upload Asset','class'=>'active','url'=>'');        
        
        $data['status'] = $this->Common_model->get_data('asset_status',array('type!='=>0));
        $data['supplier'] = $this->Common_model->get_data('supplier',array('status'=>1,'sup_type_id'=>1,'task_access'=>$task_access));        
        $data['cal_supplier'] = $this->Common_model->get_data('supplier',array('status'=>1,'calibration'=>1,'task_access'=>$task_access));  
        $data['branch'] = $this->Common_model->get_data('warehouse',array('status!='=>0,'task_access'=>$task_access));
        $data['withAssetNumberUpload'] = 1;
        $data['form_action'] = SITE_URL.'asset_wge_submit_assetupload';
        $this->load->view('asset/wge_asset_upload_view',$data);
    }

    public function tool_info()
    {    
        $data['nestedView']['heading']="Tool Info View";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Tool Info View';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Tool Info View','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searchtool', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'part_number'        => validate_string($this->input->post('part_number', TRUE)),
            'part_description'   => validate_string($this->input->post('part_description', TRUE)),
            'tool_code'          => validate_string($this->input->post('tool_code', TRUE)),
            'eq_model_id'        => validate_string($this->input->post('eq_model_id', TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
            'part_number'      => $this->session->userdata('part_number'),
            'part_description' => $this->session->userdata('part_description'),
            'eq_model_id'      => $this->session->userdata('eq_model_id'),
            'tool_code'        => $this->session->userdata('tool_code')
                              );
            }
            else {
                $searchParams=array(
                'part_number'       => '',
                'part_description'  => '',
                'eq_model_id'       => '',
                'tool_code'         => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['searchParams'] = $searchParams;


        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'tool_info/';
        # Total Records
        $config['total_rows'] = $this->Tool_m->tool_total_num_rows($searchParams);
        //echo $this->db->last_query();exit;
        $config['per_page'] = 30;
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['toolResults'] = $this->Tool_m->tool_results($current_offset, $config['per_page'], $searchParams);
        
        $data['eq_models'] =  array(''=>'Select Model') + $this->Common_model->get_dropdown('equipment_model', 'eq_model_id', 'name',array('status'=>1));
        # Additional data
        $data['displayResults'] = 1;

        $this->load->view('asset/tool_info_view',$data);

    }

    public function wge_submit_assetupload()
    {
        #page authentication
        $task_access = page_access_check('manage_asset');

        if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
        {                    
            $filename=$_FILES["uploadCsv"]["tmp_name"];            
            if($_FILES["uploadCsv"]["size"] > 0)
            {
                $file = fopen($filename, "r");
                $i=0;    
                $j=0;
                // inserting in upload table with default status 
                $upload_data = array(
                    'type'              => 2, // for Assets Upload
                    'status'            => 1,                                
                    'created_by'        => $this->session->userdata('sso_id'),
                    'file_name'         => $_FILES["uploadCsv"]["name"],
                    'created_time'      => date('Y-m-d H:i:s')
                 );
                $upload_id = $this->Common_model->insert_data('upload',$upload_data);
                $insert= 0;
                $successfully_inserted = 0;
                $unsuccessfully_inserted = 0;
                
                while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                { 
                    if($j==0) 
                    {
                     $j++;
                     continue;
                    }  
                    $wh_code = validate_string(trim($emapData[0]));
                    $sub_inventory  = validate_string($emapData[1]);
                    $part_number   = validate_string(trim($emapData[2]));
                    $serial_number  = validate_string(trim($emapData[3]));
                    $status_name = validate_string(trim($emapData[4]));
                    $status_remarks = validate_string($emapData[5]);
                    $cal_supplier_code = validate_string($emapData[6]);
                    $calibrated_date = validate_string($emapData[7]);
                    $cal_due_date = validate_string($emapData[8]);
                    $remarks = validate_string($emapData[9]);
                    $date_of_use = validate_string($emapData[10]);
                    $quantity =  validate_number($emapData[11]);
                    $po_number = validate_string($emapData[12]);
                    $earpe_number = validate_string($emapData[13]);
                    $cost = validate_number(trim($emapData[14]));
                    $currency = validate_string(trim($emapData[15]));
                    $cost_in_inr = validate_number(trim($emapData[16]));
                    $asset_number = validate_string($emapData[17]);
                    $running_sno = validate_string($emapData[18]);
                    $country = validate_string($emapData[19]);
                    $country_id = '';
                    if($country!='')
                    {
                        $country_id=$this->Common_model->get_value('location',array('name'=>$country,'level_id'=>2),'location_id');
                    }

                    if($wh_code !='' || $date_of_use !='' )
                    {  
                        if( $wh_code !='' && $date_of_use !='' && $po_number!='' && $earpe_number!='' && $currency!='' && $cost_in_inr!='' && $country_id!='' && $country !='')
                        { 
                            if($insert != 0)
                            { 
                                $return_val = wge_bulk_upload_assets(@$asset_success_data,@$asset_failed_data,@$success_part_array,
                                    @$failed_part_array,@$records_status,@$upload_id,@$asset_history_data);
                                if($return_val == 0)
                                    $successfully_inserted++; // counting succefully added records
                                else
                                    $unsuccessfully_inserted++; // counting unsuccefully  added records
                            } 

                            $failed_part_array = array();
                            $asset_failed_array = array();
                            $success_part_array = array();
                            $asset_success_array = array();
                            $sno_arr = array();
                            $pno_arr = array();
                            $asset_remarks_string = '';
                            $error = 0;

                            #warehouse Validation
                            $wh_id = $this->Common_model->get_value('warehouse',array('wh_code'=>$wh_code,'country_id'=>$country_id),'wh_id');
                            if($wh_id=='')
                            {
                                $error = 1;
                                $asset_remarks_string.='Warehouse Code: '.@$wh_code.' Not Exist, ';
                            }

                            #Tool Part Number Validation
                            $tool_data = $this->Common_model->get_data_row('tool',array('part_number'=>$part_number,'country_id'=>$country_id));
                            $pno_arr[] = $part_number;
                            if(count($tool_data) == 0)
                            {
                                $error = 1;
                                $asset_remarks_string.='Tool Part Number:'.@$part_number.' Not Exist, ';
                            }

                            #validate Serial Number koushik 11-07-2018
                            if(@$tool_data['tool_id']!='')
                            {
                                $tool_id = @$tool_data['tool_id'];
                                $data = array('serial_number'=>trim($serial_number),'tool_id'=>$tool_id,'tool_part_id'=>'','country_id'=>$country_id);
                                $serial_unique = $this->Asset_m->check_serial_number_availability($data);
                                if($serial_unique>0)
                                {
                                    $error = 1;
                                    $asset_remarks_string.='Serial Number :'.@$serial_number.' Already Exist for Same Tool, ';
                                }
                                else
                                {
                                    $sno_arr[] = $serial_number; 
                                }
                            }   
                            

                           if($error == 0)
                           {
                                $tool_cal_row = $tool_data;
                                $cal_type_id = $tool_cal_row['cal_type_id'];
                                if($cal_type_id == 1)
                                {
                                   $cal_supplier_id = $this->Common_model->get_value('supplier',array('supplier_code'=>@$cal_supplier_code,'calibration'=>1,'country_id'=>$country_id),'supplier_id');
                                   
                                   if($cal_supplier_id=='')
                                   {
                                      $error = 1;
                                      $cal_supplier_id =NULL;
                                      $asset_remarks_string.='Calibration Supplier: '.@$cal_supplier_code.' Not Exist, ';
                                   }

                                    if($calibrated_date =='')
                                    {
                                        $error = 1;
                                        $asset_remarks_string.='Calibrated Date Not Given, ';
                                    }
                                    else 
                                    {
                                        $calibrated_date =(@$calibrated_date!='')?format_date(@$calibrated_date,'Y-m-d'):'';
                                    }
                                    if($cal_due_date=='')
                                    {
                                        $error = 1;
                                        $asset_remarks_string.='Calibration Due Date Not Given, ';
                                    }
                                    else
                                    {
                                        $cal_due_date = (@$cal_due_date!='')?format_date(@$cal_due_date,'Y-m-d'):'';
                                    }
                                }
                                else
                                {
                                    $cal_supplier_id = NULL;
                                    $calibrated_date = '';
                                    $cal_due_date = '';
                                }
                            }
                            if($status_name == 'G' || $status_name == 'D')
                            {
                                $status_id = ($status_name =='G')?1:2;
                                $status_exist = 1;
                            }
                            else 
                            { 
                                $status_exist=''; 
                            }

                            if($status_exist=='')
                            {
                                $error = 1;
                                $asset_remarks_string.='Status  '.@$statu_name.' Not Exist, ';
                            }

                            if($serial_number =='')
                            {
                                $error = 1;
                                $asset_remarks_string.='Serial Number Not Exist, ';
                            }

                            if($country =='')
                            {
                                $error = 1;
                                $asset_remarks_string.='Country Not Exist, ';
                            }

                            if($po_number =='')
                            {
                              $error = 1;
                              $asset_remarks_string.='Po Number Not Exist, ';
                            }
                            if($earpe_number =='')
                            {
                              $error = 1;
                              $asset_remarks_string.='Earpe Number Not Exist, ';
                            }
                            if($currency == '')
                            {
                              $error = 1;
                              $asset_remarks_string.='Currency Not Exist, ';
                            }
                            else
                            {
                                $currency_id = $this->Common_model->get_value('currency',array('name'=>$currency),'currency_id');
                                if($currency_id =='')
                                {
                                    $error = 1;
                                    $currency_id = NULL;
                                    $asset_remarks_string.= 'Currency '.$currency.' Not Exist, ';
                                }

                            }
                            if($cost =='')
                            {
                              $error = 1;
                              $asset_remarks_string.='Cost Not Exist, ';
                            }
                            if($cost_in_inr =='')
                            {
                              $error = 1;
                              $asset_remarks_string.='Currency In INR, ';
                            }
                            
                            if($date_of_use == '')
                            {
                                $date_of_use = date('Y-m-d');
                            }
                            else
                            {
                                $date_of_use = format_date(@$date_of_use,'Y-m-d');
                            }

                            if($asset_number !='')
                            {
                                $check_avail_asset = $this->Common_model->get_value('asset',array('asset_number'=>$asset_number),'asset_id');
                                if($check_avail_asset !='')
                                {
                                    $error = 1;
                                    $asset_remarks_string.='Asset Number :'.$asset_number.' Already Exist, ';
                                }
                                if($running_sno=='')
                                {
                                    $error = 1;
                                    $asset_remarks_string.='Running Serial No is Missing, ';
                                }
                            }
                            

                            $asset_failed_data = array(          
                                'asset_number'           => @$asset_number,
                                'wh_code'                => @$wh_code,
                                'sub_inventory'          => @$sub_inventory,
                                'calibration_supplier'   => @$cal_supplier_id,
                                'cal_due_date'           => @$cal_due_date,
                                'date_of_use'            => @$date_of_use,
                                'asset_status'           => @$status_name,
                                'asset_remarks'          => @$remarks, 
                                'calibrated_date'        => @$calibrated_date,
                                'country_name'           => @$country,
                                'po_number'              => @$po_number,
                                'earpe_number'           => @$earpe_number,
                                'cost'                   => @$cost,
                                'cost_in_inr'            => @$cost_in_inr,
                                'country_name'           => @$country,
                                'currency'               => @$currency,
                                'upload_id'              => $upload_id
                            );

                            $failed_part_data = array(
                                'serial_number'       =>  @$serial_number,
                                'part_number'         =>  @$part_number,
                                'status'              =>  @$status_name,   
                                'quantity'            =>  @$quantity,
                                'status_remarks'      =>  @$status_remarks,
                                'part_upload_remarks' =>  @$asset_remarks_string
                            );
                            
                            $failed_part_array[] = $failed_part_data; 
                            $asset_failed_array[] = $asset_failed_data;
                           
                            if($error == 1 )
                            {
                                $records_status =2;
                            }

                            #asset success transaction to insert into asset master table 
                            else
                            {
                                $tool_row = $tool_data;
                                $part_level_id = $tool_row['part_level_id'];
                                $tool_id = $tool_row['tool_id'];
                                $tool_main_id = $tool_id;
                                $tool_code = $tool_row['tool_code'];
                                $supplier_id = $tool_row['supplier_id'];
                                $asset_level_id = $tool_row['asset_level_id'];
                                $part_level_id = $tool_row['part_level_id'];
                                $cal_supplier_id = ($cal_supplier_id =='')?NULL:$cal_supplier_id;
                                $status = ($status_name =='G')?1:2;
                                if($asset_level_id == 1){ $asset_level = '00'; }
                                if($asset_level_id == 2){ $asset_level = '01'; }
                                if($asset_level_id == 3){ $asset_level = '02'; }
                                $qty = $quantity;
                                $part_level_id = 1;
                                $modality_id = $tool_row['modality_id'];

                                if($asset_number == '')
                                {
                                    $modality_code = $this->Common_model->get_value('modality',array('modality_id'=>$modality_id),'modality_code');
                                    $short_name = $this->Common_model->get_value('location',array('location_id'=>$country_id),'short_name');
                                    $serial = get_current_serial_number($modality_id,$country_id);
                                    $running_sno = get_running_sno_five_digit($serial);
                                    $asset_number = $short_name.$modality_code.$tool_code.$asset_level.$running_sno;
                                }

                                $asset_success_data = array(     
                                    'asset_number'       => @$asset_number,
                                    'running_sno'        => @$running_sno,
                                    'remarks'            => @$remarks,
                                    'modality_id'        => @$modality_id,
                                    'cal_supplier_id'    => @$cal_supplier_id,
                                    'cal_due_date'       => @$cal_due_date,
                                    'calibrated_date'    => @$calibrated_date,  
                                    'cal_type_id'        => @$cal_type_id,
                                    'supplier_id'        => @$supplier_id,
                                    'wh_id'              => @$wh_id,
                                    'mwh_id'             => @$wh_id,
                                    'sub_inventory'      => @$sub_inventory,
                                    'date_of_use'        => @$date_of_use,
                                    'po_number'          => @$po_number,
                                    'earpe_number'       => @$earpe_number,
                                    'currency_id'        => @$currency_id,
                                    'country_id'         => @$country_id,
                                    'cost'               => @$cost,
                                    'cost_in_inr'        => @$cost_in_inr,
                                    'created_by'         => $this->session->userdata('sso_id'),
                                    'created_time'       => date('Y-m-d H:i:s'),
                                    'status'             => 1,//default 1 by endof transaction update status
                                    'asset_condition_id' => @$status,
                                    'availability_status'=> 1 //default 1 by end of tranction update status
                                );

                                // inserting into asset history
                                $asset_history_data = array(
                                    'wh_id'         => @$wh_id,
                                    'sub_inventory' => @$sub_inventory,
                                    'from_date'     => date('Y-m-d')
                                );

                                $success_part_data = array(
                                    'serial_number' => @$serial_number,
                                    'status'        => @$status,
                                    'part_status'   => 1,
                                    'tool_id'       => @$tool_id,
                                    'quantity'      => @$qty,
                                    'remarks'       => @$status_remarks,
                                    'part_level_id' => @$part_level_id,
                                    'created_by'    => $this->session->userdata('sso_id'),
                                    'created_time'  => date('Y-m-d H:i:s')
                                );
                                $success_part_array[] = @$success_part_data; 
                                $records_status = 1;                
                            }
                        } 

                        // checking what are the L0 items missed 
                        else
                        {  
                            $asset_remarks_string = '';                            
                            if(@$wh_code=='')       $asset_remarks_string.='Ware House Code, ';
                            if(@$date_of_use=='')   $asset_remarks_string.='Date Of use, ';
                            if(@$part_number == '') $asset_remarks_string.='Part Number, ';
                            if(@$serial_number=='') $asset_remarks_string.='Serial Number, ';
                            if(@$status_name=='')   $asset_remarks_string.='Status, ';
                            if(@$po_number=='')     $asset_remarks_string.='Po Number, ';
                            if(@$earpe_number=='')  $asset_remarks_string.='Earpe Number, ';
                            if(@$currency=='')      $asset_remarks_string.='Currency, ';
                            if(@$cost=='')          $asset_remarks_string.='Cost, ';
                            if(@$cost_in_inr=='')   $asset_remarks_string.='Cost In Inr, ';
                            if(@$country == '')     $asset_remarks_string.='Country Name, ';
                            $asset_remarks_string.=' Are Missed';

                            // capturing failed data to temp tables 
                            $asset_failed_data = array(                                    
                                'wh_code'               => @$wh_code,
                                'sub_inventory'         => @$sub_inventory,
                                'calibration_supplier'  => @$cal_supplier_code,
                                'cal_due_date'          => @$cal_due_date,
                                'date_of_use'           => @$date_of_use,
                                'asset_status'          => @$status_name,
                                'asset_remarks'         => @$remarks, 
                                'calibrated_date'       => @$calibrated_date,
                                'po_number'             => @$po_number,
                                'earpe_number'          => @$earpe_number,
                                'country_name'          => @$country,
                                'currency'              => @$currency,
                                'cost_in_inr'           => @$cost_in_inr,
                                'upload_id'             => @$upload_id
                            );

                            $failed_part_data = array(
                                'serial_number'       => @$serial_number,
                                'part_number'         => @$part_number,
                                'status'              => @$status_name,  
                                'quantity'            => @$qty,
                                'status_remarks'      => @$status_remarks,
                                'part_upload_remarks' => @$asset_remarks_string
                                );                               
                            
                            $asset_failed_array[] = @$asset_failed_data;
                            $failed_part_array[] = @$failed_part_data;
                            $records_status =2;                           
                        } 
                        $insert++;               
                    } // check empty row end
                    // inserting L1 items
                    else
                    {
                        $error = 0;
                        $asset_remarks_string = '';

                        #serial Number Validation
                        if($serial_number!='')
                        {      
                            if(in_array($serial_number, $sno_arr))
                            {
                                $error = 1;
                                $asset_remarks_string.='Duplicate Serial Number :'.@$serial_number.', ';    
                            }
                        }
                        else
                        {
                            $error = 1;
                            $asset_remarks_string.= 'Serial Number Missed';
                        }
                        // pushing the serial number to check the uniqness
                        $sno_arr[] = $serial_number;

                        #status Validation
                        if($status_name == 'G' || $status_name == 'D')
                        {
                            $status_id = ($status_name =='G')?1:2;
                        }
                        else
                        {
                            $error = 1;
                            $asset_remarks_string.='Status :'.@$status_name.' Not Exist, ';
                        }

                        if($country_id !='')
                        {
                            $tool_data = $this->Common_model->get_data_row('tool',array('part_number'=>$part_number,'country_id'=>$country_id));
                            if(count($tool_data) == 0)
                            {
                                $error = 1;
                                $asset_remarks_string.='Tool Part Number '.@$part_number.' Not Exist, ';
                            }
                        }
                        else
                        {
                            $error = 1;
                            $asset_remarks_string.='Country Is Missing, ';
                        }
                        

                        $failed_part_data = array(
                            'serial_number'       => @$serial_number,
                            'part_number'         => @$part_number,
                            'status'              => @$status_name, 
                            'quantity'            => @$qty,
                            'status_remarks'      => @$status_remarks,
                            'part_upload_remarks' => @$asset_remarks_string
                        );
                       if($error == 1)
                       {
                           $records_status = 2;
                       }
                       else
                       {
                            $tool_row = $tool_data;
                            $tool_id = $tool_row['tool_id'];
                            $asset_level_id = $tool_row['asset_level_id'];
                            
                            $tool_childs = $this->Common_model->get_data('tool_part',array('tool_main_id'=>@$tool_main_id,'tool_sub_id'=>$tool_id,'status'=>1));
                            if(count($tool_childs)>0)
                                $qty = $quantity;
                            else
                            {
                                $records_status = 2;
                                $error =1;
                                $qty =$quantity;
                                $asset_remarks_string.='This Part Number'.$part_number.' is not the part of this Tool set.Please Check ';
                            }
                            
                            if(in_array(@$part_number, @$pno_arr))
                            {
                                $error = 1;
                                $records_status = 2;
                                $asset_remarks_string.=' Duplicate Part Numbers Found :'.@$part_number.', ';     
                            }

                            $failed_part_data = array(
                                'serial_number'       =>  @$serial_number,
                                'part_number'         =>  @$part_number,
                                'status'              =>  @$status_name,     
                                'quantity'            =>  @$qty,
                                'status_remarks'      =>  @$status_remarks,
                                'part_upload_remarks' =>  @$asset_remarks_string
                            );

                            $success_part_data = array(
                                'serial_number'       => @$serial_number,
                                'status'              => @$status_id,
                                'tool_id'             => @$tool_id,
                                'quantity'            => @$qty,
                                'remarks'             => @$status_remarks,
                                'part_status'         => 1,
                                'part_level_id'       => 2, // For Child Levels Always Part Level is   2
                                'created_by'          => $this->session->userdata('sso_id'),
                                'created_time'        => date('Y-m-d H:i:s')
                            );
                        }
                        $success_part_array[] = @$success_part_data; 
                        $failed_part_array[]  = @$failed_part_data;
                    }
                     
                }
                if($insert!=0)
                {
                    $return_val = wge_bulk_upload_assets(@$asset_success_data,@$asset_failed_data,@$success_part_array,@$failed_part_array,@$records_status,@$upload_id,
                        @$asset_history_data);

                    if($return_val == 0)
                    {
                        $successfully_inserted++;
                    }
                    else
                    {
                        $unsuccessfully_inserted++;
                    }
                }
                fclose($file);
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <div class="icon"><i class="fa fa-times-circle"></i></div>
                    <strong>Error!</strong> something went wrong! please check.
                    </div>'); 
                    redirect(SITE_URL.'wge_bulkupload_asset');  
                }
                else
                {
                    $this->db->trans_commit();
                    if(@$successfully_inserted != 0 && @$unsuccessfully_inserted !=0)
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> Assets Inserted : <strong>'.@$successfully_inserted.'</strong> & Assets Not Inserted : <strong>'.$unsuccessfully_inserted.'</strong>. Please Check Below For Missed Uploads !
                        </div>');
                        redirect(SITE_URL.'missed_bulkupload_asset_view/'.storm_encode(1));  
                    }
                    if(@$successfully_inserted==0)
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Error!</strong> Assets :'.$unsuccessfully_inserted.' are not Uploaded. Please Check Below Missed Uploads !
                        </div>');
                        redirect(SITE_URL.'missed_bulkupload_asset_view/'.storm_encode(1));  
                    }
                    else
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <strong>Success!</strong> Assets :'.$successfully_inserted.' has been Added successfully !
                        </div>');
                    }
                    redirect(SITE_URL.'asset');  
                } 
            }
            else
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
                redirect(SITE_URL.'wge_bulkupload_asset'); exit();
            }
        }
    }

    public function asset_wge_submit_assetupload()
    {
        #page authentication
        $task_access = page_access_check('manage_asset');

        if(validate_string($this->input->post('bulkUpload',TRUE)) != "")
        {                    
            $filename=$_FILES["uploadCsv"]["tmp_name"];            
            if($_FILES["uploadCsv"]["size"] > 0)
            {
                $file = fopen($filename, "r");
                $i=0;    
                $j=0;
                // inserting in upload table with default status 
                $upload_data = array(
                            'type'              =>  2, // for Assets Upload
                            'status'            =>  1,                                
                            'created_by'        => $this->session->userdata('sso_id'),
                            'file_name'         => $_FILES["uploadCsv"]["name"],
                            'created_time'      => date('Y-m-d H:i:s')
                         );
                $upload_id = $this->Common_model->insert_data('upload',$upload_data);
                $insert= 0;
                $successfully_inserted = 0;
                $unsuccessfully_inserted = 0;$f_asset_success_data= array();
                
                while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                { 
                    if($j==0) 
                    {
                     $j++;
                     continue;
                    }  
                    $wh_code = validate_string(trim($emapData[0]));
                    $sub_inventory  = validate_string($emapData[1]);
                    $part_number   = validate_string(trim($emapData[2]));                    
                    $serial_number  = validate_string(trim($emapData[3]));                   
                    $status_name = validate_string(trim($emapData[4]));
                    $status_remarks = validate_string($emapData[5]);
                    $cal_supplier_code = validate_string($emapData[6]);
                    $calibrated_date = validate_string(trim($emapData[7]));
                    $cal_due_date = validate_string(trim($emapData[8]));
                    $remarks = validate_string($emapData[9]);
                    $date_of_use = validate_string($emapData[10]);
                    $quantity =  validate_number($emapData[11]);
                    $po_number = validate_string($emapData[12]);
                    $earpe_number = validate_string($emapData[13]);
                    $cost = validate_number(trim($emapData[14]));
                    $currency = validate_string(trim($emapData[15]));
                    $cost_in_inr = validate_number($emapData[16]);
                    $asset_number = validate_string($emapData[17]);
                    $running_sno = validate_string($emapData[18]);

                    //echo '<pre>';print_r($emapData);//exit;
                    if($wh_code !='' || $date_of_use !='' || $asset_number!='' )
                    {  //echo '123';
                //echo '123';exit;
                       //echo $wh_code;
                       //echo $date_of_use;//exit;
                        if( $wh_code !='' && $asset_number!='' && $date_of_use !='' && $po_number!='' && $earpe_number!='' && $currency!='' && $cost_in_inr!='' )                           
                        { //echo 'lklkl';exit;
                            if($insert != 0)
                            { //echo 'lk';exit;
                                //echo $serial.'<br>';
                                /*echo '<pre>';print_r($asset_success_data);//exit;
                                echo '<pre>';print_r($success_part_array);
                                echo '<pre>';print_r($failed_part_array);exit;*/
                                $return_val = wge_bulk_upload_assets(@$asset_success_data,@$asset_failed_data,@$success_part_array,
                                    @$failed_part_array,@$records_status,@$upload_id,@$asset_history_data);
                                if($return_val == 0)
                                    $successfully_inserted++; // counting succefully added records
                                else
                                    $unsuccessfully_inserted++; // counting unsuccefully  added records
                            } 

                           $failed_part_array = array();
                           $asset_failed_array = array();
                           $success_part_array = array();
                           $asset_success_array = array();
                           $sno_arr = array();
                           $pno_arr = array();
                           $asset_remarks_string = '';
                           $error = 0;
                           $wh_id = $this->Common_model->get_value('warehouse',array('wh_code'=>$wh_code),'wh_id'); 
                           if($wh_id=='')
                           {
                              $error = 1;
                              $asset_remarks_string.='Warehouse Code '.@$wh_code.' Not Exit. ';
                           } 
                           //echo 'pp'.$asset_remarks_string;exit;
                            $tool_data = $this->Common_model->get_data('tool',array('part_number'=>$part_number));
                            //echo '<pre>'; print_r($tool_data);exit;
                            $pno_arr[] = $part_number;
                            if(count($tool_data) == 0)
                           {
                              $error = 1;
                              $asset_remarks_string.='Tool Part Number '.@$part_number.' Not Exit. ';
                           }
                           else
                           {
                                $tool_cal_row = $tool_data[0];                                
                                $tool_main_id = $tool_cal_row['tool_id'];
                           }
                           $asset_data = $this->Common_model->get_data('asset',array('asset_number'=>trim($asset_number),'status'=>1));
                            if($asset_data)
                            {
                                $error = 1;
                                $asset_remarks_string.='Asset Number '.@$asset_number.' Already Exist. Please give other which is should be unique '; 
                            }

                            if(strlen($asset_number) < 15 || strlen($asset_number) > 17)
                            {
                                $error =1 ;
                                $asset_remarks_string.='Asset Number Should Have Minimum 15 Characters AND Maximum 17 Characters'; 
                            }

                          /* $s_no_uq = $this->Common_model->get_data('part',array('serial_number'=>@$serial_number));
                           if(count($s_no_uq) == 0){*/
                            $sno_arr[] = $serial_number; 

                            /*}
                            else{
                                $error = 1;
                                $asset_remarks_string.='Serial Number '.@$serial_number.' Already Exist. Please give other which is should be unique '; 
                            }*/
                            //echo $asset_remarks_string;exit;
                           //echo $error;exit;
                           if($error == 0)
                           {
                                $tool_cal_row = $tool_data[0];

                                $cal_type_id = $tool_cal_row['cal_type_id'];
                                $tool_main_id = $tool_cal_row['tool_id'];
                                //echo $cal_type_id;exit;
                                if($cal_type_id == 1)
                                {
                                   $cal_supplier_id = $this->Common_model->get_value('supplier',array('supplier_code'=>@$cal_supplier_code,'calibration'=>1),'supplier_id');
                                   
                                   if($cal_supplier_id=='')
                                   {
                                      $error = 1;
                                      $cal_supplier_id =NULL;
                                      $asset_remarks_string.='Calibration Supplier Code '.@$cal_supplier_code.' Not Exit. ';
                                   }
                                   //echo $calibrated_date.'--'.$cal_due_date.'<br>';//exit;
                                   //echo format_date($calibrated_date);exit;
                                   //echo date('Y-m-d',strtotime($calibrated_date));exit;
                                   if($calibrated_date =='')
                                   {
                                      $error = 1;
                                      $asset_remarks_string.='calibrated Date Not Given . ';
                                   }
                                   else 
                                   {
                                       
                                        $calibrated_date =($calibrated_date!='')?format_date($calibrated_date,'Y-m-d'):'';

                                   }
                                   if($cal_due_date=='')
                                   {
                                      $error = 1;
                                      $asset_remarks_string.='calibration Due Date Not Given . ';
                                   }
                                   else
                                   {
                                      $cal_due_date = ($cal_due_date!='')?format_date($cal_due_date,'Y-m-d'):'';
                                      //echo $cal_due_date; exit();
                                   }
                                   //echo $calibrated_date.'--'.$cal_due_date;exit;
                                }
                                else
                                {
                                    $cal_supplier_id = NULL;
                                    $calibrated_date = '';
                                    $cal_due_date = '';
                                }
                            }
                                                      
                           //echo $error;exit;
                           //echo @$cal_supplier_id;exit;
                           if($status_name == 'G' || $status_name == 'D')
                           {
                                $status_id = ($status_name =='G')?1:2;
                                $status_exist = 1;

                           }
                           else { $status_exist=''; }
                           if($status_exist=='')
                           {
                              $error = 1;
                              $asset_remarks_string.='Status  '.@$statu_name.' Not Exit. ';
                           }
                           //echo $error;exit;
                            //echo $this->db->last_query();exit;
                           
                            //echo $asset_status_exist.'pp';exit;
                           

                            if($serial_number =='')
                           {
                              $error = 1;
                              $asset_remarks_string.='Serial Number Not Exit. ';
                           }

                           if($po_number =='')
                           {
                              $error = 1;
                              $asset_remarks_string.='Po Number Not Exit. ';
                           }
                           if($earpe_number =='')
                           {
                              $error = 1;
                              $asset_remarks_string.='Earpe Number Not Exit. ';
                           }
                           if($currency == '')
                           {
                              $error = 1;
                              $asset_remarks_string.='Currency Not Exit. ';
                           }
                           else
                           {
                                $currency_id = $this->Common_model->get_value('currency',array('name'=>$currency),'currency_id');
                                if($currency_id){ }
                                else 
                                {
                                    $error = 1;
                                    $currency_id = NULL;
                                    $asset_remarks_string.= 'Currency '.$currency.' Not Exist. ';
                                }

                           }
                           if($cost =='')
                           {
                              $error = 1;
                              $asset_remarks_string.='Cost Not Exit. ';
                           }
                           if($cost_in_inr =='')
                           {
                              $error = 1;
                              $asset_remarks_string.='Currency In INR. ';
                           }
                           //echo $asset_remarks_string.'pp';exit;
                           //echo $records_status .'--'.$error;exit;
                           
                            //$asset_remarks_string ='';
                            
                            $date_of_use = format_date(@$date_of_use,'Y-m-d');

                            $asset_failed_data = array(                                    
                                'wh_code'                => @$wh_code,
                                'asset_number'           => @$asset_number,
                                'running_sno'            => @$running_sno,
                                'sub_inventory'          => @$sub_inventory,                           
                                'calibration_supplier'   => @$cal_supplier_id,
                                'cal_due_date'           => @$cal_due_date,                           
                                'date_of_use'            => @$date_of_use,
                                'asset_status'           => @$status_name,
                                'asset_remarks'          => @$remarks, 
                                'calibrated_date'        => @$calibrated_date,
                                'po_number'              => @$po_number,
                                'earpe_number'           => @$earpe_number,
                                'cost'                   => @$cost,
                                'cost_in_inr'            => @$cost_in_inr,
                                'currency'               => @$currency,
                                'upload_id'              => $upload_id                                
                            );
                                //echo '<pre>';print_r($asset_failed_data);exit;
                               // $upload_asset_id = $this->Common_model->insert_data('upload_asset',$failed_asset);  
                            $failed_part_data = array(
                                        'serial_number'       =>  @$serial_number,
                                        'part_number'         =>  @$part_number,
                                        'status'              =>  @$status_name,                                            
                                        'quantity'            =>  @$quantity,
                                        'status_remarks'      =>  @$status_remarks,
                                        'part_upload_remarks' => @$asset_remarks_string
                                        );

                               // $this->Common_model->insert_data('part',$parent_part_data);
                            //echo $asset_remarks_string;exit;
                            $failed_part_array[] = $failed_part_data; 
                            $asset_failed_array[] = $asset_failed_data;
                            //echo 
                            //echo $error;exit;
                            //echo $supplier_exist.'pp';exit;
                            if($error == 1 )
                            {
                                    $records_status =2;
                                    /*echo 'll'.$asset_remarks_string;exit;     
                                    echo 'jnkj'                      ;//exit;*/
                            }

                            // asset succes transaction to insert into asset master table 
                            else
                            {
                                //echo 'lkm';exit;
                                $tool_row = $tool_data[0];
                                $part_level_id = $tool_row['part_level_id'];
                                $tool_id = $tool_row['tool_id'];
                                $tool_main_id = $tool_id;
                                $supplier_id = $tool_row['supplier_id'];
                                //echo '<pre>';print_r($tool_row);exit;
                                $asset_level_id = $tool_row['asset_level_id'];
                                $part_level_id = $tool_row['part_level_id'];
                                if($asset_level_id == 1)  {  $qty = 1;  $asset_level = '00'; $part_level_id =1;}
                                if($asset_level_id == 2)  {  $qty =1;  $asset_level = '01'; $part_level_id = 1;}
                                if($asset_level_id == 3 ) { $qty = $quantity; $asset_level ='02'; $part_level_id = 1;}
                                
                                
                                $tool_code = $tool_row['tool_code'];
                                $modality_id = $tool_row['modality_id'];
                               
                                //echo $asset_number;exit;
                                $cal_supplier_id = ($cal_supplier_id =='')?NULL:$cal_supplier_id;
                                $status = ($status_name =='G')?1:2;

                                $asset_success_data = array(     
                                    'asset_number'       => @$asset_number,
                                    'running_sno'        => (int)@$running_sno,
                                    'remarks'            => @$remarks,
                                    'modality_id'        => @$modality_id,
                                    'cal_supplier_id'    => @$cal_supplier_id,
                                    'cal_due_date'       => @$cal_due_date,
                                    'calibrated_date'    => @$calibrated_date,  
                                    'cal_type_id'        => @$cal_type_id,
                                    'supplier_id'        => @$supplier_id,
                                    'wh_id'              => @$wh_id,
                                    'mwh_id'             => @$wh_id,
                                    'sub_inventory'      => @$sub_inventory,
                                    'date_of_use'        => @$date_of_use,
                                    'po_number'          => @$po_number,
                                    'earpe_number'       => @$earpe_number,
                                    'currency_id'        => @$currency_id,
                                    'cost'               => @$cost,
                                    'cost_in_inr'        => @$cost_in_inr,
                                    'created_by'         =>  $this->session->userdata('sso_id'),
                                    'created_time'       =>  date('Y-m-d H:i:s'),
                                    'status'             =>  1, // default 1 by end of tranction update status
                                    'asset_condition_id' => @$status,
                                    'availability_status' => 1 //default 1 by end of tranction update status

                                    );
                                //echo '<pre>';print_r($asset_success_data);exit;
                                //$asset_id = $this->Common_model->insert_data('asset',$asset_data);

                                // inserting into asset history
                                $asset_history_data = array(
                                        'wh_id'         => @$wh_id,                                        
                                        'sub_inventory' => @$sub_inventory,
                                        'from_date'     => date('Y-m-d')
                                        );
                                //$this->Common_model->insert_data('asset_history',$asset_history);
                                $success_part_data = array(
                                    'serial_number'       =>  @$serial_number,
                                    'status'              =>  @$status,
                                    'part_status'         => 1,
                                    'tool_id'             =>  @$tool_id,
                                    'quantity'            =>  @$qty,
                                    'remarks'             =>  @$status_remarks,
                                    'part_level_id'       =>  @$part_level_id,
                                    'created_by'          =>  $this->session->userdata('sso_id'),
                                    'created_time'        =>  date('Y-m-d H:i:s')                                        
                                    );
                                $success_part_array[] = @$success_part_data; 

                                /*echo '<pre>';print_r($success_part_array);
                                echo '<pre>';print_r($asset_success_data);exit;                */
                                $records_status = 1;                
                            }                  
                        } 

                        // checking what are the L0 items missed 
                        else
                        {  
                            //echo $date_of_use;exit;
                            $asset_remarks_string = '';                            
                            if(@$wh_code=='')      $asset_remarks_string.= 'Ware House Code';
                            if(@$date_of_use=='')       $asset_remarks_string.= 'Date Of USe';
                            if(@$part_number == '')          $asset_remarks_string.= 'Part Number';
                            if(@$serial_number=='')      $asset_remarks_string.= 'Serial Number ';
                            if(@$status_name=='')    $asset_remarks_string.= 'Status ';                            
                            if(@$po_number=='')    $asset_remarks_string.= 'Po Number ';
                            if(@$earpe_number=='')    $asset_remarks_string.= 'Earpe Number ';
                            if(@$currency=='')    $asset_remarks_string.= 'Currency ';
                            if(@$cost=='')    $asset_remarks_string.= 'Cost ';
                            if(@$cost_in_inr=='')    $asset_remarks_string.= 'Cost In Inr ';
                            if(@$asset_number=='')    $asset_remarks_string.= 'Asset Number ';
                            

                            $asset_remarks_string.=' Are Missed';
                            // capturing failed data to temp tables 
                            $asset_failed_data = array(                                    
                                'wh_code'               => @$wh_code,
                                'asset_number'          => @$asset_number,
                                'running_sno'           => @$running_sno,
                                'sub_inventory'         => @$sub_inventory,                           
                                'calibration_supplier'  => @$cal_supplier_code,
                                'cal_due_date'          => @$cal_due_date,                           
                                'date_of_use'           => @$date_of_use,
                                'asset_status'          => @$status_name,
                                'asset_remarks'         => @$remarks, 
                                'calibrated_date'       => @$calibrated_date,
                                'po_number'             => @$po_number,
                                'earpe_number'          => @$earpe_number,
                                'currency'              => @$currency,
                                'cost_in_inr'           => @$cost_in_inr,
                                'upload_id'             => @$upload_id                                
                            );
                                //echo '<pre>';print_r($asset_failed_data);exit;
                               // $upload_asset_id = $this->Common_model->insert_data('upload_asset',$failed_asset);  
                            $failed_part_data = array(
                                'serial_number'       =>  @$serial_number,
                                'part_number'         =>  @$part_number,
                                'status'              =>  @$status_name,                                            
                                'quantity'            =>  @$qty,
                                'status_remarks'      =>  @$status_remarks,
                                'part_upload_remarks'  =>  $asset_remarks_string
                                );                               
                            
                            $asset_failed_array[] = @$asset_failed_data;
                            $failed_part_array[] = @$failed_part_data;   
                            //echo 'kjnkj'                  ;exit;
                            $records_status =2;                           
                        } 
                        $insert++;               
                    } // check empty row end
                    // inserting L1 items
                    else
                    {

                        //echo 'kjnkj';
                        /*echo $records_status;//exit;
                        echo $error;exit;*/
                        //echo '<pre>';print_r($success_part_array).'<br>';
                        //echo $serial_number;exit;
                        $error = 0;
                        $asset_remarks_string = '';
                        if($serial_number!='')
                        {
                            /*$s_no_uq = $this->Common_model->get_data('part',array('serial_number'=>@$serial_number));
                           if(count($s_no_uq) == 0){ */       

                                if(in_array($serial_number, @$sno_arr))
                                {
                                    $error = 1;
                                    $asset_remarks_string.='Duplicate Serial Numbers Found For Same Asset '.@$serial_number.' . Please give other which is should be unique ';     
                                }

                            /*}
                            else{
                                $error = 1;
                                $asset_remarks_string.='Serial Number '.@$serial_number.' Already Exist. Please give other which is should be unique '; 
                            }*/
                        }

                        else
                        {
                            $error = 1;
                            $asset_remarks_string.= 'Serial Number Missed';
                        }
                        // pushing the serial number to check the uniqness
                        $sno_arr[] = $serial_number;

                         /*echo $error;//exit;
                        echo $asset_remarks_string;exit;*/

                        if($status_name == 'G' || $status_name == 'D')
                        {
                            $status_id = ($status_name =='G')?1:2;
                            $status_exist = 1;

                        }
                        else { $status_exist=''; }
                        if($status_exist=='')
                        {
                          $error = 1;
                          $asset_remarks_string.='Status  '.@$statu_id.' Not Exit. ';
                        }

                           //echo $error;exit;
                            //echo $this->db->last_query();exit;
                           
                            //echo $asset_status_exist.'pp';exit;
                        $tool_data = $this->Common_model->get_data('tool',array('part_number'=>$part_number));
                        //echo '<pre>'; print_r($tool_data);exit;
                        if(count($tool_data) == 0)
                        {
                          $error = 1;
                          $asset_remarks_string.='Tool Part Number '.@$part_number.' Not Exit. ';
                        }  
                        /*echo $asset_remarks_string;exit;
                        echo $records_status;//exit;
                        echo $error;exit;                      */
                         /*echo $records_status;//exit;
                        echo $error;exit;*/
                        $failed_part_data = array(
                                        'serial_number'       =>  @$serial_number,
                                        'part_number'         =>  @$part_number,
                                        'status'              =>  @$status_name,                                            
                                        'quantity'            =>  @$qty,
                                        'status_remarks'      =>  @$status_remarks,
                                        'part_upload_remarks' => @$asset_remarks_string
                                        );
                       if($error == 1)
                       {
                           $records_status = 2;
                       }
                       else
                       {
                            //echo '123';//exit;
                            $tool_data = $this->Common_model->get_data('tool',array('part_number'=>$part_number));
                            $tool_row = $tool_data[0];
                            $tool_id = $tool_row['tool_id'];
                            //echo '<pre>';print_r($tool_row);exit;
                            $asset_level_id = $tool_row['asset_level_id'];
                            
                            /*if($asset_level_id == 3)                                
                            {*/
                                $tool_childs = $this->Common_model->get_data('tool_part',array('tool_main_id'=>@$tool_main_id,'tool_sub_id'=>$tool_id,'status'=>1));
                                //echo $this->db->last_query();exit;
                                //echo '<pre>';print_r($tool_childs)
                                //print_r($tool_childs);exit;
                                if(count($tool_childs)>0)
                                    $qty = $quantity;
                                else
                                {
                                    $records_status = 2;
                                    $error = 1;
                                    $qty =$quantity;
                                    $asset_remarks_string.='This Part Number'.$part_number.' is not the part of this Tool set.Please Check ';
                                }
                                //echo $asset_remarks_string;exit;
                            //} exit;
                            /*echo '<pre>';print_r($pno_arr);
                            echo $part_number;//exit;*/
                            if(in_array(@$part_number, @$pno_arr))
                                {
                                    $error = 1;
                                    $records_status = 2;
                                    $asset_remarks_string.=' Duplicate Part Numbers Found For Same Asset '.@$part_number.' . Please give other which is should be unique In Same Kit ';     
                                }
                                
                           // echo $asset_remarks_string;exit;
                           // echo $records_status .'--'.$error;exit;
                           /* echo $qty;
                            echo $this->db->last_query();exit;*/
                            $failed_part_data = array(
                                        'serial_number'       =>  @$serial_number,
                                        'part_number'         =>  @$part_number,
                                        'status'              =>  @$status_name,                                            
                                        'quantity'            =>  @$qty,
                                        'status_remarks'      =>  @$status_remarks,
                                        'part_upload_remarks' => @$asset_remarks_string
                                        );
                            $success_part_data = array(
                                    'serial_number'       =>  @$serial_number,
                                    'status'              =>  @$status_id,                                    
                                    'tool_id'             =>  @$tool_id,
                                    'quantity'            =>  @$qty,
                                    'remarks'             =>  @$status_remarks,
                                    'part_status'         => 1,
                                    'part_level_id'       =>  2, // For Child Levels Always Part Level is   2
                                    'created_by'          =>  $this->session->userdata('sso_id'),
                                    'created_time'        =>  date('Y-m-d H:i:s')                                        
                                    );
                         
                         
                          /*echo '<pre>';print_r($success_part_array);
                          echo '<pre>';print_r($failed_part_array);exit;*/
                        }
                        $success_part_array[] = @$success_part_data; 
                         $failed_part_array[]  = @$failed_part_data;
                    }
                    $f_asset_success_data[] = $asset_success_data;
                     
                } // while end
                /*echo '<pre>';print_r($success_part_array);
                echo '<pre>';print_r($failed_part_array);exit;*/
                //echo $test;exit;
               //echo $records_status.'--'.$error;exit;
                           //echo 'lkmlkm';     exit;
                if($insert!=0)
                {
                    //echo '<pre>';print_r($success_part_array);
                   /* print_r($failed_part_array);
                    print_r($asset_failed_data);*/
                    //echo $records_status;exit;
                    //echo '<pre>';print_r($f_asset_success_data);exit;
                    //echo $asset_remarks_string;exit;
                    //exit;
                    //echo '123';exit;
                    //echo $records_status;exit;
                    //echo '<pre>';print_r($this->Common_model->get_data('upload_tool',array('upload_id'=>$upload_id))        ).'<br>';
                    $return_val = wge_bulk_upload_assets(@$asset_success_data,@$asset_failed_data,@$success_part_array,
                        @$failed_part_array,@$records_status,@$upload_id,@$asset_history_data);
                    //echo $return_val;exit;
                    if($return_val == 0)
                        $successfully_inserted++;
                    else
                        $unsuccessfully_inserted++;
                    //echo '<pre>';print_r($this->Common_model->get_data('upload_tool',array('upload_id'=>$upload_id))        );exit;
                }
                fclose($file);
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                <div class="icon"><i class="fa fa-times-circle"></i></div>
                                                <strong>Error!</strong> something went wrong! please check.
                                             </div>'); 
                    redirect(SITE_URL.'asset');  
                }
                else
                {
                    $this->db->trans_commit();
                    if(@$successfully_inserted != 0 && @$unsuccessfully_inserted !=0)
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-warning alert-white rounded">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Success!</strong> '.@$successfully_inserted.' Asset are Added. and '.$unsuccessfully_inserted.' are not added. Please Check Below For Missed Uplods !
                                     </div>');
                        redirect(SITE_URL.'asset_missed_bulkupload_asset_view/'.storm_encode(1));  
                    }
                    if(@$successfully_inserted==0)
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Error!</strong> '.$unsuccessfully_inserted.' Asset are not Uploaded. Please Check Below Missed Uploads !
                                     </div>');
                        redirect(SITE_URL.'asset_missed_bulkupload_asset_view/'.storm_encode(1));  
                    }
                    else
                    {
                        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <div class="icon"><i class="fa fa-check"></i></div>
                                        <strong>Success!</strong> '.$successfully_inserted.' Asset has been Added successful!
                                     </div>');
                    }
                    redirect(SITE_URL.'asset');  
                } 
            }
            else
            {
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> Please Upload Matched CSV File.</div>');      
                redirect(SITE_URL.'wge_bulkupload_asset'); exit();
            }

        }
    }

    public function download_missed_asset_uploads()
    { 
        #page authentication
        $task_access = page_access_check('manage_asset');

        $qry ="SELECT MAX(upload_id) as upload_id FROM upload WHERE status = 2 AND type =2 ";
        $res = $this->db->query($qry);
        $results = $res->row_array();
        $upload_id = $results['upload_id'];
        $assets = $this->Asset_m->asset_upload_details($upload_id);
        //echo '<pre>';print_r($assets);exit;
        $header = '';
        $data ='';
        $titles = array('Ware House','Sub Inventory','Tool Part Number','Serial Number','Status',
             'Status remarks','Calibration Supplier','Calibrated Date',' Calibration Due Date','Asset Remarks','Date Of Use','Quantity',
             'PO Number ','Earper Number','Cost','Currency','Cost In INR','Country','Upload Remarks');
        $data .= '<table border="1">';
        $data.='<thead>';
        $data.='<tr>';
        foreach ( $titles as $title)
        {
            $data.= '<th align="center">'.$title.'</th>';
        }
        $data.='</tr>';
        $data.='</thead>';
        $data.='<tbody>';
         

        if(count($assets)>0)
        {
            $p = 0;
           $first_upload_asset_id = $assets[0]['upload_asset_id']; 
            foreach($assets as $row)
            {
                $next_upload_asset_id = $row['upload_asset_id'];
                $data.='<tr>';

                if($p== 0) 
                { 
                    $data.='<td align="center">'.$row['wh_code'].'</td>';                   
                    $data.='<td align="center">'.$row['sub_inventory'].'</td>'; 
                }
                else
                {
                    if($first_upload_asset_id==$next_upload_asset_id)
                    {
                        $data.='<td align="center"></td>';                   
                        $data.='<td align="center"></td>'; 
                    }
                    else
                    {

                        $data.='<td align="center">'.$row['wh_code'].'</td>';                   
                        $data.='<td align="center">'.$row['sub_inventory'].'</td>'; 
                    }
                }
                $data.='<td align="center">'.$row['part_number'].'</td>';                
                $data.='<td align="center">'.$row['serial_number'].'</td>';                   
                $data.='<td align="center">'.$row['status'].'</td>';                   
                $data.='<td align="center">'.$row['status_remarks'].'</td>';   
                if($p == 0)
                {
                    $data.='<td align="center">'.$row['calibration_supplier'].'</td>';
                    $data.='<td align="center">'.$row['calibrated_date'].'</td>'; 
                    $data.='<td align="center">'.$row['cal_due_date'].'</td>';                   
                    $data.='<td align="center">'.$row['asset_remarks'].'</td>'; 
                    $data.='<td align="center">'.$row['date_of_use'].'</td>';
                    $data.='<td align="center">'.$row['quantity'].'</td>';
                    $data.='<td align="center">'.$row['po_number'].'</td>';
                    $data.='<td align="center">'.$row['earpe_number'].'</td>';
                    $data.='<td align="center">'.$row['cost'].'</td>';
                    $data.='<td align="center">'.$row['currency'].'</td>';
                    $data.='<td align="center">'.$row['cost_in_inr'].'</td>';
                }
                else
                {
                    if($first_upload_asset_id == $next_upload_asset_id)
                    {
                        $data.='<td align="center"></td>';                
                        $data.='<td align="center"></td>';                   
                        $data.='<td align="center"></td>'; 
                        $data.='<td align="center"></td>';
                        $data.='<td align="center"></td>';
                        $data.='<td align="center">'.$row['quantity'].'</td>';
                        $data.='<td align="center"></td>';                
                        $data.='<td align="center"></td>';                   
                        $data.='<td align="center"></td>'; 
                        $data.='<td align="center"></td>';
                        $data.='<td align="center"></td>';
                    } 
                    else
                    {
                        $data.='<td align="center">'.$row['calibration_supplier'].'</td>';
                        $data.='<td align="center">'.$row['calibrated_date'].'</td>';   
                        $data.='<td align="center">'.$row['cal_due_date'].'</td>';
                        $data.='<td align="center">'.$row['asset_remarks'].'</td>'; 
                        $data.='<td align="center">'.$row['date_of_use'].'</td>';
                        $data.='<td align="center">'.$row['quantity'].'</td>';
                        $data.='<td align="center">'.$row['po_number'].'</td>';
                        $data.='<td align="center">'.$row['earpe_number'].'</td>';
                        $data.='<td align="center">'.$row['cost'].'</td>';
                        $data.='<td align="center">'.$row['currency'].'</td>';
                        $data.='<td align="center">'.$row['cost_in_inr'].'</td>'; 
                    }  
                }
                $data.='<td align="center">'.$row['country_name'].'</td>'; 
                $data.='<td align="center">'.$row['part_upload_remarks'].'</td>'; 
                $data.='</tr>';   
                $p++;

                               
            }
        }
        else
        {
            $data.='<tr><td colspan="'.(count($titles)+1).'" align="center">No Results Found</td></tr>';
        }
        $data.='</tbody>';
        $data.='</table>';
        $time = date("Ymdhis");
        $xlFile='Assets'.$time.'.xls'; 
        header("Content-type: application/x-msdownload"); 
        # replace excelfile.xls with whatever you want the filename to default to
        header("Content-Disposition: attachment; filename=".$xlFile."");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $data;       
    }
    
    public function asset_download_missed_asset_uploads()
    { 
        #page authentication
        $task_access = page_access_check('manage_asset');

        $qry ="SELECT MAX(upload_id) as upload_id FROM upload WHERE status = 2 AND type =2 ";
        $res = $this->db->query($qry);
        $results = $res->row_array();
        $upload_id = $results['upload_id'];
        $assets = $this->Asset_m->asset_upload_details($upload_id);
        //echo '<pre>';print_r($assets);exit;
        $header = '';
        $data ='';
        $titles = array('Ware House','Sub Inventory','Tool Part Number','Serial Number','Status',
             'Status remarks','Calibration Supplier','Calibrated Date',' Calibration Due Date','Asset Remarks','Date Of Use','Quantity',
             'PO Number ','Earper Number','Cost','Currency','Cost In INR','Asset Number','Serial Number','Upload Remarks');
        $data .= '<table border="1">';
        $data.='<thead>';
        $data.='<tr>';
        foreach ( $titles as $title)
        {
            $data.= '<th align="center">'.$title.'</th>';
        }
        $data.='</tr>';
        $data.='</thead>';
        $data.='<tbody>';
         

        if(count($assets)>0)
        {
            $p = 0;
           $first_upload_asset_id = $assets[0]['upload_asset_id']; 
            foreach($assets as $row)
            {
                $next_upload_asset_id = $row['upload_asset_id'];
                $data.='<tr>';

                if($p== 0) 
                { 
                    $data.='<td align="center">'.$row['wh_code'].'</td>';                   
                    $data.='<td align="center">'.$row['sub_inventory'].'</td>'; 
                }
                else
                {
                    if($first_upload_asset_id==$next_upload_asset_id)
                    {
                        $data.='<td align="center"></td>';                   
                        $data.='<td align="center"></td>'; 
                    }
                    else
                    {

                        $data.='<td align="center">'.$row['wh_code'].'</td>';                   
                        $data.='<td align="center">'.$row['sub_inventory'].'</td>'; 
                    }
                }
                $data.='<td align="center">'.$row['part_number'].'</td>';                
                $data.='<td align="center">'.$row['serial_number'].'</td>';                   
                $data.='<td align="center">'.$row['status'].'</td>';                   
                $data.='<td align="center">'.$row['status_remarks'].'</td>';   
                if($p == 0)
                {
                    $data.='<td align="center">'.$row['calibration_supplier'].'</td>';                
                    $data.='<td align="center">'.$row['calibrated_date'].'</td>'; 
                    $data.='<td align="center">'.$row['cal_due_date'].'</td>';                   
                    $data.='<td align="center">'.$row['asset_remarks'].'</td>'; 
                    $data.='<td align="center">'.$row['date_of_use'].'</td>';
                    $data.='<td align="center">'.$row['quantity'].'</td>';
                    $data.='<td align="center">'.$row['po_number'].'</td>';
                    $data.='<td align="center">'.$row['earpe_number'].'</td>';
                    $data.='<td align="center">'.$row['cost'].'</td>';
                    $data.='<td align="center">'.$row['currency'].'</td>';
                    $data.='<td align="center">'.$row['cost_in_inr'].'</td>';
                    $data.='<td align="center">'.$row['asset_number'].'</td>';
                    $data.='<td align="center">'.$row['running_sno'].'</td>';
                }
                else
                {
                    if($first_upload_asset_id == $next_upload_asset_id)
                    {
                        $data.='<td align="center"></td>';                
                        $data.='<td align="center"></td>';                   
                        $data.='<td align="center"></td>'; 
                        $data.='<td align="center"></td>';
                        $data.='<td align="center"></td>';
                        $data.='<td align="center">'.$row['quantity'].'</td>';
                        $data.='<td align="center"></td>';                
                        $data.='<td align="center"></td>';                   
                        $data.='<td align="center"></td>'; 
                        $data.='<td align="center"></td>';
                        $data.='<td align="center"></td>';
                        $data.='<td align="center"></td>';
                        $data.='<td align="center"></td>';
                    } 
                    else
                    {
                        $data.='<td align="center">'.$row['calibration_supplier'].'</td>';                
                        $data.='<td align="center">'.$row['cal_due_date'].'</td>';                   
                        $data.='<td align="center">'.$row['asset_remarks'].'</td>'; 
                        $data.='<td align="center">'.$row['date_of_use'].'</td>';
                        $data.='<td align="center">'.$row['quantity'].'</td>';
                        $data.='<td align="center">'.$row['po_number'].'</td>';
                        $data.='<td align="center">'.$row['earpe_number'].'</td>';
                        $data.='<td align="center">'.$row['cost'].'</td>';
                        $data.='<td align="center">'.$row['currency'].'</td>';
                        $data.='<td align="center">'.$row['cost_in_inr'].'</td>'; 
                        $data.='<td align="center">'.$row['asset_number'].'</td>';
                        $data.='<td align="center">'.$row['running_sno'].'</td>';
                    }  
                }
                $data.='<td align="center">'.$row['part_upload_remarks'].'</td>'; 
                $data.='</tr>';   
                $p++;

                               
            }
        }
        else
        {
            $data.='<tr><td colspan="'.(count($titles)+1).'" align="center">No Results Found</td></tr>';
        }
        $data.='</tbody>';
        $data.='</table>';
        $time = date("Ymdhis");
        $xlFile='Assets'.$time.'.xls'; 
        header("Content-type: application/x-msdownload"); 
        # replace excelfile.xls with whatever you want the filename to default to
        header("Content-Disposition: attachment; filename=".$xlFile."");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $data;       
    }

    public function activate_asset($encoded_id)
    {
        $asset_id=@storm_decode($encoded_id);
        if($asset_id==''){
            redirect(SITE_URL.'asset_id');
            exit;
        }
        $where = array('asset_id' => $asset_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('asset',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Asset has been Activated successfully!
        </div>');
        redirect(SITE_URL.'asset');
    }

    // checking part number uniqueness
    public function check_part_number_availability()
    {
        $part_number = validate_string($this->input->post('part_number',TRUE));
        $asset_id = validate_number($this->input->post('asset_id',TRUE));
        $data = array('part_number'=>$part_number,'asset_id'=>$asset_id);
        $result = $this->Asset_m->check_part_number_availability($data);
        echo $result;
    }

    // checking asset code uniqueness
    public function check_asset_code_availability()
    {
        $asset_code = validate_string($this->input->post('asset_code',TRUE));
        $asset_id = validate_number($this->input->post('asset_id',TRUE));
        $data = array('asset_code'=>$asset_code,'asset_id'=>$asset_id);
        $result = $this->Asset_m->check_asset_code_availability($data);
        echo $result;
    }

    public function missed_bulkupload_asset_view()
    {
        $upload_id = @storm_decode($this->uri->segment(2));
        if($upload_id == '')
        {
        redirect(SITE_URL.'asset'); exit();
        }

        $data['nestedView']['heading']="Missed List of Asset Bulk Upload";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Missed List of Asset Bulk Upload';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'user');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Missed Asset Bulk Upload','class'=>'active','url'=>'');

        $qry ="SELECT MAX(upload_id) as upload_id FROM upload WHERE status = 2 AND type =2";
        $res = $this->db->query($qry);
        $results = $res->row_array();
        $upload_id = $results['upload_id'];
        //echo $upload_id;exit;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL.'missed_bulkupload_asset_view/'.storm_encode($upload_id).'/';
        # Total Records
        $config['total_rows'] = $this->Asset_m->missed_asset_total_num_rows($upload_id);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($data['pagination_links'] != '') {
          $data['last'] = $this->pagination->cur_page * $config['per_page'];
          if ($data['last'] > $data['total_rows']) 
          {
              $data['last'] = $data['total_rows'];
          }
          $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['missedResults'] = $this->Asset_m->missed_asset_results($current_offset, $config['per_page'], $upload_id);

        #additional Data 
        $data['upload_id'] = $upload_id; 
        $this->load->view('asset/missed_asset_upload_view',$data);
    }

    public function asset_missed_bulkupload_asset_view()
    {
        $upload_id = @storm_decode($this->uri->segment(2));
        if($upload_id == '')
        {
        redirect(SITE_URL.'asset'); exit();
        }

        $data['nestedView']['heading']="Missed List of Asset Bulk Upload";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Missed List of Asset Bulk Upload';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'user');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Missed Asset Bulk Upload','class'=>'active','url'=>'');

        $qry ="SELECT MAX(upload_id) as upload_id FROM upload WHERE status = 2 AND type =2 ";
        $res = $this->db->query($qry);
        $results = $res->row_array();
        $upload_id = $results['upload_id'];
        //echo $upload_id;exit;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL.'asset_missed_bulkupload_asset_view/'.storm_encode($upload_id).'/';
        # Total Records
        $config['total_rows'] = $this->Asset_m->missed_asset_total_num_rows($upload_id);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($data['pagination_links'] != '') {
          $data['last'] = $this->pagination->cur_page * $config['per_page'];
          if ($data['last'] > $data['total_rows']) {
              $data['last'] = $data['total_rows'];
          }
          $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['missedResults'] = $this->Asset_m->missed_asset_results($current_offset, $config['per_page'], $upload_id);

        #additional Data 
        $data['upload_id'] = $upload_id; 
        $this->load->view('asset/asset_missed_asset_upload_view',$data);
    }

    public function asset_qr()
    {
        $asset_id=@storm_decode($this->uri->segment(2));
        $page_redirect = $this->uri->segment(3);
        if($asset_id=='')
        {
            redirect(SITE_URL.'asset');
            exit;
        } 
        $data['nestedView']['heading']="Generate Asset QR";
        $data['nestedView']['cur_page'] = 'generate_asset_qr';
        $data['nestedView']['parent_page'] = 'generate_asset_qr';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Generate Asset QR';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Generate Asset QR','class'=>'active','url'=>'');

        $data['qr_lables'] = $this->Common_model->get_data('qr_lable',array('status'=>1,'lable_id>'=>1));
        $data['asset_id'] = $asset_id;
        $data['asset'] = $this->Asset_m->get_asset_info($asset_id);
        if($page_redirect == 1)
        {
            $data['cancel_url'] = SITE_URL.'asset';
        }
        else
        {
            $data['cancel_url'] = SITE_URL.'tools_inventory';
        }
        $this->load->view('asset/asset_qr',$data);
    }

    // Mahesh 05-10-2017
    public function print_qr()
    {
        //print_r($_POST); exit;
        if($this->input->post('generate_qr')!=1)
        {
            redirect(SITE_URL.'asset'); exit();
        }
        $data['nestedView']['heading']="Print Asset QR";
        $data['nestedView']['cur_page'] = 'print_asset_qr';
        $data['nestedView']['parent_page'] = 'print_asset_qr';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();
        
        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Print Asset QR';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Print Asset QR','class'=>'active','url'=>'');

        
        $data['asset_id'] = $asset_id = $this->input->post('asset_id');
        $data['asset'] = $asset = $this->Asset_m->get_asset_info($asset_id);
        $data['lable_id'] = $lable_id = $this->input->post('lable_size');
        // Get label info
        $data['label'] = $label_row = $this->Common_model->get_data_row('qr_lable',array('lable_id'=>$lable_id));
        $printer_model = $this->input->post('printer_model');
        $printer_models = get_printer_models();
        $data['printer_model'] = $printer_models[$printer_model];
        //echo '<pre>';print_r($data); echo '</pre>'; exit;
        
        $data['prn_file_name'] = $label_row['name'].'_'.$printer_model.'.prn';
        $this->load->view('asset/feed_asset_data',$data);
    }

    // Mahesh 25th June 2017 10:28 AM
    public function asset_info()
    {
        $data['nestedView']['heading']="Asset Info";
        $data['nestedView']['cur_page'] = 'asset_info';
        $data['nestedView']['parent_page'] = 'asset_info';

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Check Asset Info';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Check Asset Info','class'=>'active','url'=>'');
        $asset_number = validate_string($this->input->post('asset_number',TRUE));
        $asset_number = trim(@$asset_number);
        if($asset_number!='')
        {
            // Get Asset Details
            $parts = $this->Asset_m->get_asset_part_details($asset_number);
            if($parts)
            {
                $data['parts'] = $parts;

            }
            $data['asset_number'] = $asset_number;
            
        }
        $this->load->view('asset/read_qr',$data);
    }

    public function egm_calibration()
    {
        $asset_id = validate_number(storm_decode($this->uri->segment(2)));
        if($asset_id=='')
        {
            redirect(SITE_URL.'asset'); exit();
        }
        $st_entry = check_for_st_entry($asset_id);
        if(count($st_entry)>0)
        {
            $asset_number = $this->Common_model->get_value('asset',array('asset_id'=>$asset_id),'asset_number');
            $stn_number = $st_entry['stn_number'];
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Asset: <strong>'.$asset_number.'</strong> is involved in Stock Transfer Request: <strong>'.$stn_number.'</strong>. Please complete ST to proceed Further!.</div>'); 
            redirect(SITE_URL.'asset'); exit();
        }

        $data['nestedView']['heading']="Calibration Asset Details";
        $data['nestedView']['cur_page'] = 'manage_asset';
        $data['nestedView']['parent_page'] = 'manage_asset';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

        # include files
        $data['nestedView']['js_includes'] = array();
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/egm_calibration.js"></script>';

        $data['nestedView']['css_includes'] = array();

        # Breadcrumbs
        $data['nestedView']['breadCrumbTite'] = 'Calibration Asset Details';
        $data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Asset','class'=>'','url'=>SITE_URL.'asset');
        $data['nestedView']['breadCrumbOptions'][] = array('label'=>'Calibration Asset Details','class'=>'active','url'=>'');

        #check for calibration entry
        $ec_arr = $this->Asset_m->get_cr_status($asset_id);
        if(count($ec_arr)==0)
        {
            $match_arr = array(2);
            $data['cal_status_arr'] = $this->Asset_m->get_cal_status_list($match_arr);
        }
        else
        {
            if($ec_arr['cal_status_id']==1)
            {
                $match_arr = array(2);
                $data['cal_status_arr'] = $this->Asset_m->get_cal_status_list($match_arr);
            }
            else
            {
                $match_arr = array(3,4,5,6);
                $data['cal_status_arr'] = $this->Asset_m->get_cal_status_list($match_arr);
            }
        }

        #display partial tolerance detials
        $data['display_cr_remarks'] = get_cr_remarks($asset_id);

        # Additional data
        $tool_id = $this->Asset_m->get_top_tool_id($asset_id);
        $data['lrow'] = $this->Asset_m->asset_details_view($asset_id,$tool_id);
        $data['asset_item_list'] = $this->Asset_m->get_asset_item_list($asset_id);
        $data['task_access'] = $task_access;
        $data['documenttypeDetails'] = $this->Common_model->get_data('document_type',array('status'=>1,'workflow_type'=>1));

        $this->load->view('asset/egm_calibration',$data);
    }

    public function submit_egm_calibration()
    {
        if(validate_number($this->input->post('submit_cal',TRUE))==1)
        {
            $asset_id = validate_number(storm_decode($this->input->post('asset_id',TRUE)));
            $cal_status_id = validate_number($this->input->post('cal_status_id',TRUE));
            $remarks = validate_string($this->input->post('remarks',TRUE));
            $asset_arr = $this->Common_model->get_data_row('asset',array('asset_id'=>$asset_id));
            $country_id = $asset_arr['country_id'];

            $this->db->trans_begin();

            #insert asset documents
            $document_type = $this->input->post('document_type',TRUE);
            foreach ($document_type as $key => $value) 
            {
                if($_FILES['support_document_'.$key]['name']!='')
                {
                    $config['file_name'] = date('YmdHis').'-'.$key.'.'.get_file_extension($_FILES['support_document_'.$key]['name']);
                    $config['upload_path'] = asset_document_path();
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('support_document_'.$key);
                    $fileData = $this->upload->data();
                    $support_document = $config['file_name'];
                }
                else
                {
                    $support_document = '';
                }
                $insert_doc = array(
                    'document_type_id' => $value,
                    'name'             => $support_document,
                    'doc_name'         => $_FILES['support_document_'.$key]['name'],
                    'asset_id'         => $asset_id,
                    'created_by'       => $this->session->userdata('sso_id'),
                    'created_time'     => date('Y-m-d H:i:s')
                );
                if($value!='' && $support_document != '')
                {
                    $this->Common_model->insert_data('asset_document',$insert_doc);
                }
            }

            $ec_id = check_for_cr_entry($asset_id);
            if($ec_id == 0)//create New CR
            {
                // Dynamic CR number Generarion
                $cr_number = get_cr_number($country_id);

                if($cr_number == '')
                {
                    $f_order_number = "1-00001";
                }
                else
                {
                    $num = ltrim($cr_number,"C");
                    $result = explode("-", $num);
                    $running_no = (int)$result[1];

                    if($running_no == 99999)
                    {
                        $first_num = $result[0]+1;
                        $second_num = get_running_sno_five_digit(1);  
                    }
                    else
                    {
                        $first_num = $result[0];
                        $val = $running_no+1;
                        $second_num = get_running_sno_five_digit($val);
                    }
                    $f_order_number = $first_num.'-'.$second_num;
                }
                $order_number = 'C'.$f_order_number;
                // Insert Data into EGM Calibration
                $e_data = array(
                    'cr_number'     => $order_number,
                    'asset_id'      => $asset_id,
                    'cal_status_id' => 1,
                    'country_id'    => $country_id,
                    'created_by'    => $this->session->userdata('sso_id'),
                    'created_time'  => date('Y-m-d H:i:s'),
                    'status'        => 1,
                    'remarks'       => 'Created Prior to Due Date'
                );
                $ec_id = $this->Common_model->insert_data('egm_calibration',$e_data);
                
                // Insertdata into egm_cal_history
                $ech_data=array(
                    'ec_id'         => $ec_id,
                    'cal_status_id' => 1,
                    'start_time'    => date('Y-m-d H:i:s'),
                    'remarks'       => 'Created Prior to Due Date'
                );
                $ech_id = $this->Common_model->insert_data('egm_cal_history',$ech_data);
            }

            $cr_number = $this->Common_model->get_value('egm_calibration',array('ec_id'=>$ec_id),'cr_number');
            $cal_status_name = $this->Common_model->get_value('egm_cal_status',array('cal_status_id'=>$cal_status_id),'name');
            $a_remarks = "Asset is ".$cal_status_name.' for CR:'.$cr_number;

            if($cal_status_id == 2)#under calibration / send to vendor
            {
                $ash_arr = array(
                    'asset_id'      => $asset_id,
                    'asset_status'  => 4,
                    'old_data'      => array('asset_status_id' => $asset_arr['status']),
                    'a_remarks'     => $a_remarks,
                    'new_data'      => array('asset_status_id' => 4),
                    'cal_date'      => '',
                    'cal_due_date'  => '',
                    'ec_id'         => $ec_id,
                    'cal_status_id' => $cal_status_id,
                    'ec_remarks'    => $remarks,
                    'ec_status'     => 1
                );
                update_egm_calibration($ash_arr);
            }
            else if($cal_status_id == 3 || $cal_status_id == 5)#calibrated & partial tolerance
            {
                $cal_date = date('Y-m-d',strtotime($this->input->post('cal_date')));
                $cal_due_date = date('Y-m-d',strtotime($this->input->post('cal_due_date')));

                $old_data = array(
                    'asset_status_id' => $asset_arr['status'],
                    'cal_due_date'    => $asset_arr['cal_due_date'],
                    'calibrated_date' => $asset_arr['calibrated_date']
                );

                $new_data = array(
                    'asset_status_id' => 1,
                    'cal_due_date'    => $cal_due_date,
                    'calibrated_date' => $cal_date
                );

                $ash_arr = array(
                    'asset_id'      => $asset_id,
                    'asset_status'  => 1,
                    'old_data'      => $old_data,
                    'a_remarks'     => $a_remarks,
                    'new_data'      => $new_data,
                    'cal_date'      => $cal_date,
                    'cal_due_date'  => $cal_due_date,
                    'ec_id'         => $ec_id,
                    'cal_status_id' => $cal_status_id,
                    'ec_remarks'    => $remarks,
                    'ec_status'     => 10
                );
                update_egm_calibration($ash_arr);
            }
            else if($cal_status_id == 4)#out of tolerance
            {
                $ash_arr = array(
                    'asset_id'      => $asset_id,
                    'asset_status'  => 9,
                    'old_data'      => array('asset_status_id' => $asset_arr['status']),
                    'a_remarks'     => $a_remarks,
                    'new_data'      => array('asset_status_id' => 9),
                    'cal_date'      => '',
                    'cal_due_date'  => '',
                    'ec_id'         => $ec_id,
                    'cal_status_id' => $cal_status_id,
                    'ec_remarks'    => $remarks,
                    'ec_status'     => 10
                );
                update_egm_calibration($ash_arr);
            }
            else if($cal_status_id == 6)#obselete
            {
                $ash_arr = array(
                    'asset_id'      => $asset_id,
                    'asset_status'  => 11,
                    'old_data'      => array('asset_status_id' => $asset_arr['status']),
                    'a_remarks'     => $a_remarks,
                    'new_data'      => array('asset_status_id' => 11),
                    'cal_date'      => '',
                    'cal_due_date'  => '',
                    'ec_id'         => $ec_id,
                    'cal_status_id' => $cal_status_id,
                    'ec_remarks'    => $remarks,
                    'ec_status'     => 10
                );
                update_egm_calibration($ash_arr);
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>');
            }
            else
            {
                $this->db->trans_commit();
                $a_remarks = "Asset :<strong>".$asset_arr['asset_number']."</strong> is ".$cal_status_name.' with CR No.:<strong>'.$cr_number.'</strong>';
                $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> '.$a_remarks.' !
                </div>');
            }
        }
        redirect(SITE_URL.'asset');
    }
}