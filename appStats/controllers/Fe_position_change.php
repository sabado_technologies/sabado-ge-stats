<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';

class Fe_position_change extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Fe_position_change_m');
	}

	public function fe_position_change()
    {
        $data['nestedView']['heading']="FE Position Change Requests";
		$data['nestedView']['cur_page'] = "fe_position";
		$data['nestedView']['parent_page'] = 'fe_position';

        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);

		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'FE Position Change Requests';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>SITE_URL.'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'FE Position Change Requests','class'=>'active','url'=>'');

        # Search Functionality
        $psearch=validate_string($this->input->post('searchfeposition',TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
            'ssoid'      => validate_string($this->input->post('sso_id',TRUE)),
            'country_id' => validate_number(@$this->input->post('country_id',TRUE))
            );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {

            if($this->uri->segment(2)!='')
            {
                $searchParams=array(
                'ssoid'      => $this->session->userdata('ssoid'),
                'country_id' => $this->session->userdata('country_id')
                );
            }
            else 
            {
                $searchParams=array(
                'ssoid'      => '',
                'country_id' => ''
                );
                $this->session->set_userdata($searchParams);
            }
            
        }
        $data['search_data'] = $searchParams;
		# Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'fe_position_change/';
        # Total Records
        $config['total_rows'] = $this->Fe_position_change_m->fe_position_change_total_num_rows($searchParams,$task_access);

        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['fe_positionResults'] = $this->Fe_position_change_m->fe_position_change_list_results($current_offset, $config['per_page'], $searchParams,$task_access);

        # Additional data
        $data['countryList'] = $this->Common_model->get_data('location',array('status'=>1,'level_id'=>2,'task_access'=>$task_access));
        $data['task_access'] = $task_access;
        $fe_position_array = array('l_modal'=>3);
        $this->session->set_userdata($fe_position_array);
        
		$this->load->view('fe_position/fe_changed_position',$data);
	}

	public function approve_fe_position_change()
    {
        #page authentication
        $task_access = page_access_check('fe_position');

        if($this->input->post('submitFePosition')!='')
        {
            $fpc_id = validate_number($this->input->post('fpc_id',TRUE));
            $new_wh = validate_number($this->input->post('fe_warehouse',TRUE));

            $fpc_row = $this->Common_model->get_data_row('fe_position_change',array('fpc_id'=>$fpc_id));
            $new_fe_position = $fpc_row['new_fe_position'];
            $sso_id = $fpc_row['sso_id'];
            $user_row = $this->Common_model->get_data_row('user',array('sso_id'=>$sso_id));

            $this->db->trans_begin();
            #audit data
            $old_data = array(
                'wh_id'       => $user_row['wh_id'],
                'fe_position' => $user_row['fe_position']);

            #update user
            $update_user = array(
                'wh_id'         => $new_wh,
                'fe_position'   => $new_fe_position,
                'modified_by'   => $this->session->userdata('sso_id'),
                'modified_time' => date('Y-m-d H:i:s')
            );
            $update_user_where = array('sso_id'=> $sso_id);
            $this->Common_model->update_data('user',$update_user,$update_user_where);

            #audit data
            $new_data = array(
                'wh_id'       => $new_wh,
                'fe_position' => $new_fe_position);
            $remarks = "FE Position change Has been Approved";
            audit_data('user_master',$sso_id,'user',2,'',$new_data,array('sso_id'=>$sso_id),$old_data,$remarks,'',array(),'','',$user_row['country_id']);

            #update FE Position
            $update_fep = array(
                'approval_status' => 2,
                'modified_by'     => $this->session->userdata('sso_id'),
                'modified_time'   => date('Y-m-d H:i:s')
            );
            $update_fep_where = array('fpc_id' => $fpc_id);
            $this->Common_model->update_data('fe_position_change',$update_fep,$update_fep_where);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>
                <strong>Error!</strong> something went wrong! please check.</div>');
            }
            else
            {
                $this->db->trans_commit();
                $this->session->set_flashdata('response','<div class="col-md-12 alert alert-success alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-check"></i></div>
                <strong>Success!</strong> Warehouse & FE Location has been updated successfully for SSO :<strong> '.$user_row['sso_id'].' - '.$user_row['name'].' </strong> !
                </div>');
            } 
        }
        redirect(SITE_URL.'fe_position_change'); exit;
    }

    public function reject_fe_position()
    {
        $fpc_id = validate_number(storm_decode($this->uri->segment(2)));
        if($fpc_id == '')
        {
            redirect(SITE_URL.'fe_position_change'); exit();
        }

        #page authentication
        $task_access = page_access_check('fe_position');

        $this->db->trans_begin();
        $update_data = array(
            'approval_status' => 3,
            'modified_time'   => date('Y-m-d H:i:s'),
            'modified_by'     => $this->session->userdata('sso_id')
        );
        $update_where = array('fpc_id'=>$fpc_id);
        $this->Common_model->update_data('fe_position_change',$update_data,$update_where);
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> something went wrong! please check.</div>'); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="col-md-12 alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong>Position Change Request for FE Has been Rejected successfully !
            </div>');
        }
        redirect(SITE_URL.'fe_position_change'); exit; 
    }

    public function get_fe_warehouse()
    {
        $wh_id = validate_number($this->input->post('wh_id',TRUE));
        $country_id = validate_number($this->input->post('country_id',TRUE));
        $warehouse_list = $this->Fe_position_change_m->get_country_wise_warehouse($wh_id,$country_id);
        $qry_data='';
        if(count($warehouse_list)>0)
        {
            $qry_data.='<option value="">- New Warehouse -</option>';
            foreach($warehouse_list as $row)
            {  
                $qry_data.='<option value="'.$row['wh_id'].'">'.$row['wh_code'].' -('.$row['name'].'), '.$row['city_name'].', '.$row['state_name'].'</option>';
            }
        } 
        else 
        {
            $qry_data.='<option value="">No Data Found</option>';
        }
        echo $qry_data;
    }
} 
?>