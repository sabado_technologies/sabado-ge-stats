<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created By gowri on 15th June 5:30PM
class Eq_model extends Base_controller 
{
	public  function __construct() 
	{
        parent::__construct();
		$this->load->model('Eq_model_model');
	}
	public function eq_model()
    {
        
        $data['nestedView']['heading']="Manage Equipment Model";
		$data['nestedView']['cur_page'] = 'manage_equipment_model';
		$data['nestedView']['parent_page'] = 'manage_equipment_model';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
        $data['nestedView']['css_includes'] = array();
		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Manage Equipment Model';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Equipment Model','class'=>'active','url'=>'');

        # Search Functionality
       
        $psearch=validate_string($this->input->post('searcheq_model', TRUE));
        if($psearch!='') 
        {
            $searchParams=array(
                               'eq_model_name' => validate_string($this->input->post('eq_model_name', TRUE))
                               );
            $this->session->set_userdata($searchParams);
        } 
        else 
        {
        if($this->uri->segment(2)!='')
            {
            $searchParams=array(
                              'eq_model_name' => $this->session->userdata('eq_model_name')
                              );
            }
            else {
                $searchParams=array(
                                    'eq_model_name' => ''
                                   );
                $this->session->set_userdata($searchParams);
            }
        }
        $data['search_data'] = $searchParams;

        # Default Records Per Page - always 10
        /* pagination start */
        $config = get_paginationConfig();
        $config['base_url'] = SITE_URL . 'eq_model/';
        # Total Records
        $config['total_rows'] = $this->Eq_model_model->eq_model_total_num_rows($searchParams);
        $config['per_page'] = getDefaultPerPageRecords();
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();
        $current_offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($data['pagination_links'] != '') {
            $data['last'] = $this->pagination->cur_page * $config['per_page'];
            if ($data['last'] > $data['total_rows']) {
                $data['last'] = $data['total_rows'];
            }
            $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config['per_page']) + 1) . ' to ' . ($data['last']) . ' of ' . $data['total_rows'];
        }
        $data['sn'] = $current_offset + 1;
        /* pagination end */

        # Loading the data array to send to View
        $data['eq_modelResults'] = $this->Eq_model_model->eq_model_results($current_offset, $config['per_page'], $searchParams);
        
        # Additional data
        $data['displayResults'] = 1;
        $this->load->view('equipment_model/equipment_model_view',$data);

    }
    public function add_eq_model()
    {
        
        $data['nestedView']['heading']="Add New Equipment Model";
		$data['nestedView']['cur_page'] = 'check_manage_equipment_model';
		$data['nestedView']['parent_page'] = 'manage_equipment_model';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        $data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/eq_model.js"></script>';
		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Add New Equipment Model';
		$data['nestedView']['breadCrumbOptions'] = array( array('label'=>'Home','class'=>'','url'=>'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Manage Equipment Model ','class'=>'','url'=>SITE_URL.'eq_model');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Add New Equipment Model','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 1;
        $data['form_action'] = SITE_URL.'insert_eq_model';
        $data['displayResults'] = 0;
        $this->load->view('equipment_model/equipment_model_view',$data);
    }

    public function insert_eq_model()
    {
        #page authentication
        $task_access = page_access_check('manage_equipment_model');
        $name = validate_string($this->input->post('eq_model_name',TRUE));   
        $check_data=array('name' =>$name,'eq_model_id'=>0);

        #check unique eq_model name 
        $available = $this->Eq_model_model->is_eq_model_nameExist($check_data);
        if($available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Equipment Model : '.$name.' already exist! please check.
            </div>'); 
            redirect(SITE_URL.'eq_model'); exit();

        }   
        $this->db->trans_begin();
    	$data=array(
                    'name'          =>      $name,
                    'created_by'    =>      $this->session->userdata('sso_id'),
                    'created_time'  =>      date('Y-m-d H:i:s')
                    );
        
        $eq_model_id = $this->Common_model->insert_data('equipment_model',$data);

        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
           	
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check. </div>'); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Equipment model has been Added successfully! </div>');
                  
        }

        redirect(SITE_URL.'eq_model');  
    }

    public function edit_eq_model()
    {
        
        $eq_model_id=@storm_decode($this->uri->segment(2));
        if($eq_model_id=='')
        {
            redirect(SITE_URL.'eq_model');
            exit;
        }
        # Data Array to carry the require fields to View and Model
        $data['nestedView']['heading']="Edit Equipment Model ";
		$data['nestedView']['cur_page'] = 'check_manage_equipment_model';
		$data['nestedView']['parent_page'] = 'manage_equipment_model';
        #page authentication
        $task_access = page_access_check($data['nestedView']['parent_page']);
		# include files
		$data['nestedView']['js_includes'] = array();
		$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/jquery.parsley/parsley.js"></script>';
        //$data['nestedView']['js_includes'][] = '<script type="text/javascript" src="'.assets_url().'js/storm/eq_model.js"></script>';
		$data['nestedView']['css_includes'] = array();

		# Breadcrumbs
		$data['nestedView']['breadCrumbTite'] = 'Edit Equipment Model ';
		$data['nestedView']['breadCrumbOptions'] = array(array('label'=>'Home','class'=>'','url'=>'home'));
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>' Manage Equipment Model','class'=>'','url'=>'eq_model');
		$data['nestedView']['breadCrumbOptions'][] = array('label'=>'Edit Equipment Model','class'=>'active','url'=>'');

        # Additional data
        $data['flg'] = 2;
        $data['form_action'] = SITE_URL.'update_eq_model';
        $data['displayResults'] = 0;

        # Data
        $row = $this->Common_model->get_data('equipment_model',array('eq_model_id'=>$eq_model_id));
        $data['lrow'] = $row[0];
        $this->load->view('equipment_model/equipment_model_view',$data);
    }
    public function update_eq_model()
    {
       #page authentication
        $task_access = page_access_check('manage_equipment_model');
        $eq_model_id=validate_number(storm_decode($this->input->post('encoded_id',TRUE)));
        if($eq_model_id==''){
            redirect(SITE_URL.'eq_model');
            exit;
        }
        $eq_model_name =validate_string($this->input->post('eq_model_name',TRUE)); 
        $check_data=array('name'        => $eq_model_name,
                          'eq_model_id' => $eq_model_id);

        #check unique eq_model name 
        $available = $this->Eq_model_model->is_eq_model_nameExist($check_data);
        if($available>0)
        {
            $this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> Equipment Model : '.$name.' already exist! please check.
            </div>'); 
            redirect(SITE_URL.'eq_model'); exit();

        }   
        $this->db->trans_begin();
        $data=array(
                    'name'           => $eq_model_name,
                    'modified_by'    => $this->session->userdata('sso_id'),
                    'modified_time'  => date('Y-m-d H:i:s')
                    );
        $where = array('eq_model_id'=>$eq_model_id);      
        $this->Common_model->update_data('equipment_model',$data,$where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
           	$this->session->set_flashdata('response','<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong>Something Went Wrong! Please check..</div>'); 
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-check"></i></div>
            <strong>Success!</strong> Equipment model has been Updated successfully!</div>');  
        }

        redirect(SITE_URL.'eq_model');  
    }

    public function deactivate_eq_model($encoded_id)
    {
        #page authentication
        $task_access = page_access_check('manage_equipment_model');
        $eq_model_id=@storm_decode($encoded_id);
        if($eq_model_id==''){
            redirect(SITE_URL.'eq_model');
            exit;
        }
        $where = array('eq_model_id' => $eq_model_id);
        //deactivating user
        $data_arr = array('status' => 2);
        $this->Common_model->update_data('equipment_model',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Equipment model has been deactivated successfully!</div>');
        redirect(SITE_URL.'eq_model');

    }

     public function activate_eq_model($encoded_id)
    {
        #page authentication
        $task_access = page_access_check('manage_equipment_model');
        $eq_model_id=@storm_decode($encoded_id);
        if($eq_model_id==''){
            redirect(SITE_URL.'eq_model');
            exit;
        }
        $where = array('eq_model_id' => $eq_model_id);
        //deactivating user
        $data_arr = array('status' => 1);
        $this->Common_model->update_data('equipment_model',$data_arr, $where);

        $this->session->set_flashdata('response','<div class="alert alert-success alert-white rounded">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <div class="icon"><i class="fa fa-check"></i></div>
        <strong>Success!</strong> Equipment Model has been Activated successfully!</div>');
        redirect(SITE_URL.'eq_model');
    }
    // checking uniqueness 
    public function is_eq_model_nameExist()
    {
        $name = validate_string($this->input->post('name',TRUE));
        $eq_model_id = validate_number($this->input->post('eq_model_id',TRUE));
        $data = array('name'=>$name,'eq_model_id'=>$eq_model_id);
        $result = $this->Eq_model_model->is_eq_model_nameExist($data);
        echo $result;
    }
}