<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';
// Created by Roopa on 17th june 2017
class Ajax_ci extends Base_Controller{

    public function __construct() 
    {
        parent::__construct();
    }        
    public function ajax_get_zone_by_country_id()
    {
        $country_id = validate_number($this->input->post('country_id',TRUE));
        echo ajax_get_zone_by_country_id($country_id);
    }
    public function ajax_get_state_by_zone_id()
    {
        $zone_id = validate_number($this->input->post('zone_id',TRUE));
        echo ajax_get_state_by_zone_id($zone_id);
    }

    public function ajax_get_city_by_state_id()
    {
        $state_id = validate_number($this->input->post('state_id',TRUE));
        echo ajax_get_city_by_state_id($state_id);
    }
    public function ajax_get_area_by_city_id()
    {
        $city_id = validate_number($this->input->post('city_id',TRUE));
        echo ajax_get_area_by_city_id($city_id);
    }

    public function ajax_get_users_by_country_id()
    {
        $country_id = validate_number($this->input->post('country_id'));
        $res = ajaxUsersDropDown($country_id);
        echo $res;
    }

    public function ajax_get_countries_by_sso_id()
    {
        $country_id = validate_number($this->input->post('sso_id'));
        $res = ajaxCountryDropDown($country_id);
        echo $res;
    }
}
?>