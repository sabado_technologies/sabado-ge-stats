<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<div class="row">					
								<div class="header">
									<h4 align="center">Asset Details : <strong><?php echo $lrow['asset_number']; ?></strong></h4>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<table class="no-border">
											<tbody class="no-border-x no-border-y">
												<tr>
													<td class="data-lable"  width="30%"><strong>Modality :</strong></td>
											        <td class="data-item"><?php echo @$lrow['modality_name'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Tool Type :</strong></td>
											        <td class="data-item"><?php echo @$lrow['tool_type_name'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Ware House :</strong></td>
											        <td class="data-item"><?php echo @$lrow['warehouse_code'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Sub Inventory :</strong></td>
											        <td class="data-item"><?php echo @$lrow['sub_inventory'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Asset Type :</strong></td>
											        <td class="data-item"><?php echo @$lrow['asset_type'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Kit :</strong></td>
											        <td class="data-item"><?php if($lrow['kit']==1)
											        {
											            echo "Yes";
											        }else
											        {
											        	echo "No";
											        }
											        ?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Cost :</strong></td>
											        <td class="data-item"><?php echo indian_format_price(round(@$lrow['cost_in_inr']));?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Supplier :</strong></td>
											        <td class="data-item"><?php echo @$lrow['supplier_name'];?></td>
												</tr>
												<?php if(@$lrow['cal_type_id']==1){?>
												<tr>
													<td class="data-lable"><strong>Calibrated Date :</strong></td>
													<td class="data-item"><h4><strong><?php echo indian_format(@$lrow['calibrated_date']); ?></strong></h4></td>
												</tr>
												<?php } ?>
												<?php if(@$lrow['gps_tracker']!=''){?>
												<tr>
													<td class="data-lable"><strong>GPS TrackingID :</strong></td>
											        <td class="data-item"><?php echo @$lrow['gps_tracker'];?></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									<div class="col-md-6">
										<table class="no-border">
											<tbody class="no-border-x no-border-y">
												<tr>
													<td class="data-lable"  width="30%"><strong>Asset Level :</strong></td>
													<td class="data-item"><?php echo @$lrow['asset_level_name']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Calibration :</strong></td>
													<td class="data-item"><?php echo @$lrow['calibration']?></td>
												</tr>
												
												<tr>
													<td class="data-lable"><strong>Manufacturer :</strong></td>
													<td class="data-item"><?php echo @$lrow['manufacturer']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Po Number :</strong></td>
													<td class="data-item"><?php echo @$lrow['po']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Earpe Number :</strong></td>
													<td class="data-item"><?php echo @$lrow['earpe']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Date of use :</strong></td>
													<td class="data-item"><?php echo indian_format(@$lrow['date_of_used']); ?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Remarks :</strong></td>
											        <td class="data-item"><?php echo @$lrow['asset_remarks'];?></td>
												</tr>
												<?php if(@$lrow['cal_type_id']==1){?>
												<tr>
													<td class="data-lable"><strong>Calibration Supplier :</strong></td>
													<td class="data-item"><?php echo @$lrow['cs_name']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Calibration Due Date :</strong></td>
													<td class="data-item"><h4><strong><?php echo indian_format(@$lrow['due_date']); ?></strong></h4></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<?php if($display_cr_remarks!=''){ ?>
								<div class="col-md-12">
									<h4 align="center">Note : <strong><span style="color:red;"><?php echo $display_cr_remarks; ?></span></strong></h4>
								</div>
								<?php } ?>
								<div class="table-responsive col-md-12">
									<table class="table table-bordered hover">
										<thead>
											<tr>
												<th class="text-center"><strong>S No.</strong></th>
												<th class="text-center"><strong>Tool Number</strong></th>
												<th class="text-center"><strong>Tool Desc.</strong></th>
												<th class="text-center"><strong>Tool Level</strong></th>
												<th class="text-center"><strong>Serial Number</strong></th>
												<th class="text-center"><strong>Asset Condition</strong></th>
												<th class="text-center"><strong>Qty</strong></th>
												<th class="text-center"><strong>Tool Code</strong></th>
											</tr>
										</thead>
										<tbody>
										<?php
											if(count(@$asset_item_list)>0)
											{	$count = 1;
												foreach($asset_item_list as $ad)
												{?>	
													<tr>
														<td class="text-center"><?php echo $count++; ?></td>
														<td class="text-center"><?php echo $ad['part_number']; ?></td>
														<td class="text-center"><?php echo $ad['part_description']; ?></td>
														<td class="text-center"><?php echo $ad['part_level_name']; ?></td>
														<td class="text-center"><?php echo $ad['serial_number']; ?></td>
														<td class="text-center"><?php echo $ad['asset_condition_name']; ?></td>
														<td class="text-center"><?php echo $ad['quantity']; ?></td>
														<td class="text-center"><?php echo $ad['tool_code']; ?></td>
													</tr>
												 <?php		
												} 
											} 
											else {
												?>	
												<tr class="text-center"><td colspan="8" align="center">No Records.</td></tr>
											<?php } ?>
										</tbody>
									</table><br>
								</div>
							</div>
							<br>
							<div class="row">
								<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'submit_egm_calibration';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
									<input type="hidden" name="asset_id" value="<?php echo storm_encode($lrow['asset_id']); ?>">
									<div class="col-md-12">
										<div class="form-group">
											<label for="inputName" class="col-sm-4 control-label">CR Status <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<select class="select2 cal_status_id" name="cal_status_id" required="required">
													<option value="">- Calibration Status -</option>
													<?php 
														foreach($cal_status_arr as $key => $value) 
														{
															echo "<option value='".$value['cal_status_id']."'>".$value['name']."</option>";
														}
													?>
												</select>
											</div>
										</div>
									</div>

									<div class="calibrated_div hidden">
										<div class="col-md-12">
											<div class="form-group cal_date_div">
												<label class="col-sm-4 control-label">Calibrated Date <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<input class="form-control cal_date date"  size="16" type="text" autocomplete="off"  readonly placeholder="Calibrated Date" name="cal_date" style="cursor:hand;background-color: #ffffff">
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group cal_due_date_div">
												<label class="col-sm-4 control-label">Calibration Due Date <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<input class="form-control cal_due_date date"  size="16" type="text" autocomplete="off"  readonly placeholder="Calibration Due Date" name="cal_due_date" style="cursor:hand;background-color: #ffffff">
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<label for="inputName" class="col-sm-4 control-label">Remarks <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<textarea class="form-control" name="remarks" required placeholder="remarks"></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-offset-9 col-md-2" style="margin-bottom: 5px;">
											<a id="add_document" class="btn blue tooltips"><i class="fa fa-plus"></i> Add Document</i></a>
										</div>
										<div class="col-md-12">
											<div class="table-responsive col-md-offset-1 col-md-10 tab_hide">
												<table class="table table-bordered hover document_table">
													<thead>
														<tr>
															<th class="text-center" width="8%"><strong>S.No.</strong></th>
															<th width="30%"class="text-center"><strong>Document Type</strong></th>
															<th width="40%" class="text-center"><strong>Supported Document</strong></th>
															<th class="text-center" width="8%"><strong>Delete</strong></th>
														</tr>
													</thead>
													<tbody>
														<?php $sno = 1; ?>			
														<tr class="doc_row">
															<td class="text-center">
													            <span class="sno"><?php echo $sno; ?></span>
													        </td>
															<td>
																<select name="document_type[1]" class="form-control doc_type" > 
																	<option value="">Select Document Type</option>
																	<?php 
																		foreach($documenttypeDetails as $doc)
																		{
																			echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
																		}
																	?>
																</select>
															</td>
															<td>
													    		<input type="file" class="document" id="document" name="support_document_1">
															</td>
															
													        <td class="text-center"><a class="btn btn-danger btn-sm remove_bank_row"><i class="fa fa-trash-o"></i></a></td>
														</tr>
													</tbody>
												</table><br>
											</div>
										</div>
										<div class="col-sm-offset-5 col-sm-5">
											<div class="form-group">
												<button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure you want to Submit?')" name="submit_cal" value="1"><i class="fa fa-check"></i> Submit</button>
												<a class="btn btn-danger" href="<?php echo SITE_URL.'asset';?>"><i class="fa fa-times"></i> Cancel</a>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div><br><br>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>