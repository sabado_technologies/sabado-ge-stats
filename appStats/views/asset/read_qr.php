<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<?php
							if(@$asset_number=='')
							{
							?>
							<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'asset_info';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
								<div class="form-group">
		                            <div class="col-sm-3">
										<input type="text" autocomplete="off" name="asset_number" id="asset_number" autofocus class="form-control">
									</div>
								</div>
							</form>
							<h1 align="center">Scan QR Code to get Asset Details</h1>
							<?php
							}
							else
							{
							?>
								<h2>Asset ID : <?php echo $asset_number;?></h2>
							<?php
								if(@$parts)
								{
									?>
									<table class="table table-bordered hover">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Part Number</th>
												<th>Part Description</th>
												<th>Serial Number</th>
												<th>Quantity</th>
												<th>Level</th>
											</tr>
										</thead>
										
										<?php
										$sn = 1;
										foreach ($parts as $prow) {
											?>
											<tr>
												<td><?php echo $sn++;?></td>
												<td><?php echo $prow['part_number'];?></td>
												<td><?php echo $prow['part_description'];?></td>
												<td><?php echo $prow['serial_number'];?></td>
												<td><?php echo $prow['quantity'];?></td>
												<td><?php echo $prow['part_level'];?></td>
											</tr>
											<?php
										}
										?>
									</table>
									<?php
								}
								else
								{
									?>
										<h3 align="center">No Records Found</h3>
									<?php
								}
								?>
									<div align="center" style="margin:20px 0px;">
									<a align="center" href="<?php echo SITE_URL;?>asset_info" class="btn btn-success">Back</a>
									</div>
								<?php
							}
							?>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<style type="text/css">
    form input[type=text]{
        height:0.0001px !important; 
        color: #fff; 
        border-color: none; 
        opacity:0.0001;
    }
</style>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#asset_number').on('input',function(){
            var asset_number = $(this).val();
            if(asset_number.trim() != '')
            {
            	$('form').submit();
            }
        });

        /*$('#asset_number').on('keypress keyup keydown',function(e){
            e.preventDefault();
            return false;
        });*/
        $('body').on('click',function(e){
        	$('#asset_number').focus();
        });
    });
</script>