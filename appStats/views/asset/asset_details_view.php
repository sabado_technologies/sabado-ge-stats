<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<div class="row">							
								<div class="header">
									<h4 align="center">Asset Details : <strong><?php echo $lrow['asset_number']; ?></strong></h4>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<table class="no-border">
											<tbody class="no-border-x no-border-y">
												<tr>
													<td class="data-lable"  width="30%"><strong>Modality :</strong></td>
											        <td class="data-item"><?php echo @$lrow['modality_name'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Tool Type :</strong></td>
											        <td class="data-item"><?php echo @$lrow['tool_type_name'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Ware House :</strong></td>
											        <td class="data-item"><?php echo @$lrow['warehouse_code'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Sub Inventory :</strong></td>
											        <td class="data-item"><?php echo @$lrow['sub_inventory'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Asset Type :</strong></td>
											        <td class="data-item"><?php echo @$lrow['asset_type'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Kit :</strong></td>
											        <td class="data-item"><?php if($lrow['kit']==1)
											        {
											            echo "Yes";
											        }else
											        {
											        	echo "No";
											        }
											        ?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Cost :</strong></td>
											        <td class="data-item"><?php echo indian_format_price(round(@$lrow['cost_in_inr']));?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Supplier :</strong></td>
											        <td class="data-item"><?php echo @$lrow['supplier_name'];?></td>
												</tr>
												<?php if(@$lrow['cal_type_id']==1){?>
												<tr>
													<td class="data-lable"><strong>Calibrated Date :</strong></td>
													<td class="data-item"><h4><strong><?php echo indian_format(@$lrow['calibrated_date']); ?></strong></h4></td>
												</tr>
												<?php } ?>
												<?php if(@$lrow['gps_tracker']!=''){?>
												<tr>
													<td class="data-lable"><strong>GPS TrackingID :</strong></td>
											        <td class="data-item"><?php echo @$lrow['gps_tracker'];?></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									<div class="col-md-6">
										<table class="no-border">
											<tbody class="no-border-x no-border-y">
												<tr>
													<td class="data-lable"  width="30%"><strong>Asset Level :</strong></td>
													<td class="data-item"><?php echo @$lrow['asset_level_name']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Calibration :</strong></td>
													<td class="data-item"><?php echo @$lrow['calibration']?></td>
												</tr>
												
												<tr>
													<td class="data-lable"><strong>Manufacturer :</strong></td>
													<td class="data-item"><?php echo @$lrow['manufacturer']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Po Number :</strong></td>
													<td class="data-item"><?php echo @$lrow['po']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Earpe Number :</strong></td>
													<td class="data-item"><?php echo @$lrow['earpe']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Date of use :</strong></td>
													<td class="data-item"><?php echo indian_format(@$lrow['date_of_used']); ?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Remarks :</strong></td>
											        <td class="data-item"><?php echo @$lrow['asset_remarks'];?></td>
												</tr>
												<?php if(@$lrow['cal_type_id']==1){?>
												<tr>
													<td class="data-lable"><strong>Calibration Supplier :</strong></td>
													<td class="data-item"><?php echo @$lrow['cs_name']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Calibration Due Date :</strong></td>
													<td class="data-item"><h4><strong><?php echo indian_format(@$lrow['due_date']); ?></strong></h4></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>	
							<br>						
							<div class="row">
								<?php if($display_cr_remarks!=''){ ?>
								<div class="col-md-12">
									<h4 align="center">Note : <strong><span style="color:red;"><?php echo $display_cr_remarks; ?></span></strong></h4>
								</div>
								<?php } ?>			
								<div class="header">
									<h4 align="center"><strong>Main Component</strong></h4>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<table class="no-border">
											<tbody class="no-border-x no-border-y">
												<tr>
													<td class="data-lable" width="23%"><strong>Serial Number :</strong></td>
											        <td class="data-item"><?php echo @$lrow['serial_number'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Tool Number :</strong></td>
											        <td class="data-item"><?php echo @$lrow['part_number'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Tool Description :</strong></td>
											        <td class="data-item"><?php echo @$lrow['part_description'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Tool Code :</strong></td>
											        <td class="data-item"><?php echo @$lrow['tool_code'];?></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="col-md-6">
										<table class="no-border">
											<tbody class="no-border-x no-border-y">
												<tr>
													<td class="data-lable"  width="23%"><strong>Quantity :</strong></td>
													<td class="data-item"><?php echo @$lrow['quantity'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Status :</strong></td>
													<td class="data-item"><?php echo (@$defective_asset_id != '')?"Missed":@$lrow['asset_condition_name']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Remarks :</strong></td>
											        <td class="data-item"><?php echo @$lrow['part_remarks'];?></td>
												</tr>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<br>
							<?php if(count(@$subComponent)>0)
							{ ?>
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Sub Components</strong></h4>
								</div>
								<?php foreach ($subComponent as $sub) 
								{ ?>
									<div class="col-md-12">
										<div class="col-md-6">
											<table class="no-border">
												<tbody class="no-border-x no-border-y">
													<tr>
														<td class="data-lable" width="23%"><strong>Serial Number :</strong></td>
												        <td class="data-item"><?php echo @$sub['serial_number'];?></td>
													</tr>
													<tr>
														<td class="data-lable"><strong>Tool Number :</strong></td>
												        <td class="data-item"><?php echo @$sub['part_number'];?></td>
													</tr>
													<tr>
														<td class="data-lable"><strong>Tool Description :</strong></td>
												        <td class="data-item"><?php echo @$sub['part_description'];?></td>
													</tr>
													<tr>
														<td class="data-lable"><strong>Tool Code :</strong></td>
												        <td class="data-item"><?php echo @$sub['tool_code'];?></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="col-md-6">
											<table class="no-border">
												<tbody class="no-border-x no-border-y">
													<tr>
														<td class="data-lable"  width="23%"><strong>Quantity :</strong></td>
														<td class="data-item"><?php echo @$sub['quantity'];?></td>
													</tr>
													<tr>
														<td class="data-lable"><strong>Status :</strong></td>
														<td class="data-item"><?php echo @$sub['asset_condition_name']?></td>
													</tr>
													<tr>
														<td class="data-lable"><strong>Remarks :</strong></td>
												        <td class="data-item"><?php echo @$sub['part_remarks'];?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="col-md-12">
										<br>
									</div>
								<?php } ?>
							</div>
							<?php } ?>
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Attached Documents</strong></h4>
								</div>
								<br>
								<div class="table-responsive col-md-offset-1 col-md-10">
									<table class="table table-bordered hover">
										<thead>
											<tr>
												<th class="text-center"><strong>S No.</strong></th>
												<th width="18%" class="text-center"><strong>Attached Date</strong></th>
												<th class="text-center"><strong>Document Type</strong></th>
												<th width="30%" class="text-center"><strong>Supported Document</strong></th>
												
											</tr>
										</thead>
										<tbody>
										<?php
											if(count(@$attach_document)>0)
											{	$count = 1;
												foreach($attach_document as $ad)
												{?>	
													<tr>
														<td class="text-center"><?php echo $count++; ?></td>
														<td class="text-center"><?php echo $ad['created_time']; ?></td>
														<td class="text-center">
																<?php 
																	foreach($documenttypeDetails as $doc)
																	{
																		if($doc['document_type_id']==@$ad['document_type_id'])
																		{
																			echo $doc['name']; 
																		}
																	}
																?>
														</td>
														<td>
					                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
														</td>
													</tr>
												 <?php		
												} 
											} 
											else {
												?>	
												<tr class="text-center"><td colspan="4" align="center">No Attached Docs.</td></tr>
											<?php } ?>
										</tbody>
									</table><br>
								</div>
								<?php if(@$defective_asset_id != ''){?>
									<form method="post" action="<?php echo SITE_URL;?>submit_missed_asset"   parsley-validate novalidate class="form-horizontal">
										<input type="hidden" name="asset_id" value="<?php echo @$asset_id;?>">
										<input type="hidden" name="defective_asset_id" value="<?php echo @$defective_asset_id;?>">
										<div class="col-md-12">
									    	<div class="form-group">
										    	<label class="col-sm-4 control-label">Remarks: <span class="req-fld">*</span></label>
				                    			<div class="col-sm-5">
													<textarea required class="form-control" name="remarks1"></textarea>
												</div>
									    	</div>
									    </div>
									    <div class="form-group">
											<div class="col-sm-offset-5 col-sm-5"><br>
												<button class="btn btn-primary" type="submit" value="1" onclick="return confirm('Are you sure you want to Submit?')" name="submit_supplier"><i class="fa fa-check"></i> Submit</button>
											    <a class="btn btn-danger" href="<?php echo SITE_URL;?>missed_asset"><i class="fa fa-times"></i> Cancel</a>
											</div>
										</div>									     
									</form>
								<?php }
								else
								{ ?>
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>
										<a class="btn btn-primary" href="<?php echo SITE_URL.'asset'.$current_offset;?>"><i class="fa fa-reply"></i> Back</a>
									</div>
								</div>
								<?php } ?>
								
							</div>
							<br>


						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>