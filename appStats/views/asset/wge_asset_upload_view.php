<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:55%; z-index:2; opacity:0"><img src="<?php echo SITE_URL1;?>assets/images/ajax-loading-img.gif" /></div>
<div class="cl-mcont" id="orderCartContainer">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        	<?php echo $this->session->flashdata('response'); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
	                    <div class="content">
	                    	<form class="form-horizontal" id="bulkUploadFrm" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data" >
	                        	<div class="form-group">
	                                <label for="inputName" class="col-sm-3 control-label">Upload CSV File</label>
	                                <div class="col-sm-6">
	                                 	<input type="file" name="uploadCsv" id="uploadCsv" accept=".csv" >
	                                    <p><small>Note: Upload Data In Specified CSV Format Only</small></p>
									</div>
	                            </div>
	                            <div class="form-group">
	                                <div class="col-sm-offset-3 col-sm-9">
	                                    <button class="btn btn-primary upload_asset" type="submit"  value ="1" name="bulkUpload"><i class="fa fa-cloud-upload"></i> Upload Data</button>
	                                </div>
	                            </div>
	                        </form>
	                        <div class="row">
	                         	<div class="col-sm12  col-md-12">
	                            	<div class="block-flat1">
	                                    <div class="header">							
	                                        <h4><strong>Instructions</strong></h4>
	                                    </div>
	                                    <div class="content">
	                                    	<div class="col-sm-12">
	                                        	<div class="col-sm-3">
	                                            	<h5><strong>Bulk Upload Template</strong></h5>
	                                                <p>
														<a href="<?php echo SITE_URL1;?>samples/wge_sample_asset_template.csv" class="btn btn-primary"><i class="fa fa-wrench"></i> Asset Template</a>
	                                                </p>
	                                                <?php if(@$failed_upload==1) { ?>
	                                                <p>
														<a href="<?php echo SITE_URL1;?>download_missed_asset_uploads" class="btn btn-danger">
														<i class="fa fa-gavel"></i> Missed Upload </a>
	                                                </p> <?php }?>

		                                            	<h5><strong>Status </strong></h5>
		                                                <div class="table-responsive">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th align="center"><strong>Status Name</strong></th>
																		<th align="center"><strong>Status</strong></th>																	
																	</tr>
																</thead>
																<tbody>
																	<?php
																	foreach(asset_status_array() as $ca)
																	{
																		?>
																		<tr>
																			<td align="center"><?php echo $ca['status'];?></td>
																			<td align="center"><?php echo $ca['name'];?></td>
																			
																		</tr>
																		<?php
																	}
																	?>
																</tbody>
															</table>
		                                                </div>
		                                            
	                                            </div>
	                                            <div class="col-sm-4">
	                                            	<h5><strong>Warehouse Info </strong></h5>
	                                                <div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th align="center"><strong>Warehouse Code</strong></th>	
																	<th align="center"><strong>Warehouse Name</strong></th>
																																	
																</tr>
															</thead>
															<tbody>
																<?php
																foreach($branch as $branchrow)
																{
																	?>
																	<tr>
																		<td align="center"><?php echo $branchrow['wh_code'];?></td>
																		<td align="center"><?php echo $branchrow['name'];?></td>
																		
																		
																	</tr>
																	<?php
																}
																?>
															</tbody>
														</table>
	                                                </div>	
	                                                

	                                            </div>	
	                                            <div class="col-sm-4">
	                                            	<h5><strong>Calibration Supplier </strong></h5>
	                                                <div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th align="center"><strong>Calibration Supplier Code</strong></th>
																	<th align="center"><strong>Name</strong></th>																	
																</tr>
															</thead>
															<tbody>
																<?php
																foreach($cal_supplier as $csup)
																{
																	?>
																	<tr>
																		<td align="center"><?php echo $csup['supplier_code'];?></td>
																		<td align="center"><?php echo $csup['name'];?></td>
																		
																	</tr>
																	<?php
																}
																?>
															</tbody>
														</table>
	                                                </div>
	                                            </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>		    
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
$('.upload_asset').on('click',function(){
  $("#orderCartContainer").css("opacity",0.5);
  $("#loaderID").css("opacity",1);
});
</script>
