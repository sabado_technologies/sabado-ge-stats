<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:58%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>
<div class="cl-mcont">
	
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content" id="response">
							
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(".cl-mcont").css("opacity",0.5);
	$("#loaderID").css("opacity",1);
	var part_number = '<?php echo $asset['part_number'];?>';
	var part_description = '<?php echo $asset['part_description'];?>';
	var asset_number = '<?php echo $asset['asset_number'];?>';
	var serial_number = '<?php echo $asset['serial_number'];?>';
	var sso_id = '<?php echo $_SESSION['sso_id']?>';
	var prn_file_name = '<?php echo $prn_file_name?>';
	var lable_id = '<?php echo $lable_id?>';
	var printer_name = '<?php echo $printer_model['printer_name']?>';

	var feed_url = 'http://localhost/bartender/feed.php';
	var data = 'part_number='+part_number+'&part_description='+part_description+'&asset_number='+asset_number+'&sso_id='+sso_id+'&prn_file_name='+prn_file_name+'&printer_name='+printer_name+'&serial_number='+serial_number+'&lable_id='+lable_id;
	$.ajax({
      type:"POST",
      url:feed_url,
      data:data,
      cache:false,
      success:function(response){
        //alert(response);
        $(".cl-mcont").css("opacity",1);
		$("#loaderID").css("opacity",0);
		res = $.trim(response);
        if(res=='1 file(s) copied.')
        {
        	$('#response').html('<h1 align="center"><i class="fa fa-check-circle" style="color:#4caf50"></i> Success</h1>');
        }
        else if(response=='2')
        {
        	$('#response').html('<h1 align="center"><i class="fa fa-times-circle" style="color:#e53935"></i> Error: PRN File Not Exists</h1>');
        }
        else
        {
        	//$('#response').html('<h1 align="center"><i class="fa fa-times-circle" style="color:#e53935"></i> Error: Something went wrong</h1>');
        	$('#response').html('<h1 align="center"><i class="fa fa-check-circle" style="color:#4caf50"></i> '+response+'</h1>');
        }
      },
      error: function(xhr, status, error) {
		  //$('#response').html(xhr.responseText);
		  $(".cl-mcont").css("opacity",1);
		  $("#loaderID").css("opacity",0);
		  if(error=='Not Found')
		  {
		  	$('#response').html('<h1 align="center"><i class="fa fa-times-circle" style="color:#e53935"></i> Error: Bartender Integration Not Found</h1>');
		  }
		}
    });
</script>