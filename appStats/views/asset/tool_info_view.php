<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			<?php
			if(isset($flg))
			{
			    ?>
			    <div class="row"> 
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<form class="form-horizontal" role="form"  action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
									<input type="hidden" name="tool_id" id="tool_id" value="<?php echo @$lrow['tool_id'];?>">	                           

									<div class="form-group">
										<label for="inputName" class="col-sm-3 control-label">Tool Part Number <span class="req-fld">*</span></label>
											<div class="col-sm-6">
												<input type="text" autocomplete="off" required class="form-control" placeholder="Tool Part Number" name="part_number" id="part_number" value="<?php echo @$lrow['part_number']; ?>">
											</div>
									</div>
									<div class="form-group">
										<label for="inputName" class="col-sm-3 control-label">Tool Code <span class="req-fld">*</span></label>
											<div class="col-sm-6">
												<input type="text" autocomplete="off" required class="form-control" placeholder="Tool Code" name="tool_code" id="tool_code" value="<?php echo @$lrow['tool_code']; ?>">
											</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Description</label>
										<div class="col-sm-6">
											<textarea class="form-control" id="description" name="part_description"><?php echo @$lrow['part_description'];  ?></textarea>
										</div>
									</div>
									
									<div class="form-group">
										<label for="inputeq_model" class="col-sm-3 control-label">Equipment Model <span class="req-fld">*</span></label>
			                            <div class="col-sm-6">
											<?php echo form_dropdown('eq_model[]', $eq_model, @$eq_model_selected,'class="select2  " required multiple'); ?>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-4 col-sm-5">
											<button class="btn btn-primary" type="submit" name="submit_tool"><i class="fa fa-check"></i> Submit</button>
											<a class="btn btn-danger" href="<?php echo SITE_URL;?>tool"><i class="fa fa-times"></i> Cancel</a>
										</div>
									</div>
								</form>
							</div>
						</div>				
					</div>
				</div>
				<?php 
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
			<div class="block-flat">
				<table class="table table-bordered"></table>
				<div class="content">
					<div class="row">
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>tool_info">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<div class="col-sm-12">
											<label class="col-sm-1 control-label">Modality</label>
				                            <div class="col-sm-2">
												<?php echo form_dropdown('eq_model_id', $eq_models, @$searchParams['eq_model_id'],'class="select2 "'); ?>
											</div>
											
											<label class="col-sm-2 control-label">Part Number</label>
				                            <div class="col-sm-2">
												<input type="text" autocomplete="off" name="part_number" placeholder="Part Number" value="<?php echo @$searchParams['part_number'];?>"  class="form-control" maxlength="100">
											</div>
											<label class="col-sm-2 control-label">Description</label>
											<div class="col-sm-2">
		                                        <input type="text" autocomplete="off" name="part_description" placeholder="Part Description" value="<?php echo @$searchParams['part_description'];?>" id="location" class="form-control" maxlength="100">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<div class="col-sm-12">
											
											<label class="col-sm-1 control-label">Tool Code</label>
											<div class="col-sm-2">
												<input type="text" autocomplete="off" name="tool_code" placeholder="Tool Code" value="<?php echo @$searchParams['tool_code'];?>" id="serial_number" class="form-control"  maxlength="80">
											</div>
											<div class="col-sm-4" align="right">
												<button type="submit" name="searchtool" value="1" class="btn btn-success" title="Search"><i class="fa fa-search"></i> </button>
												<a href="<?php echo SITE_URL.'tool';?>" title="Refresh" class="btn btn-success" title="Refresh"><i class="fa fa-refresh"></i></a>
												<a title="Add New" href="<?php echo SITE_URL;?>add_tool" class="btn btn-success"><i class="fa fa-plus"></i> </a>
												<a title="Bulk Upload" href="<?php echo SITE_URL;?>bulkupload_tool" class="btn btn-success"><i class="fa fa-cloud-upload"></i> </a>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</form>
					</div><br>
					<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Part Number</strong></th>
								<th class="text-center"><strong>Description</strong></th>
								<th class="text-center"><strong>Tool Code</strong></th>
								
								<!-- <th class="text-center"><strong>Equipment Model</strong></th> -->
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							$i = 1;
							if(count($toolResults)>0)
							{
								foreach($toolResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$row['tool_id'];?></td>
										<td class="text-center"><?php echo @$row['part_number'];?></td>
										<td class="text-center"><?php echo @$row['part_description'];?></td>
										<td class="text-center"><?php echo @$row['tool_code'];?></td>
										
										<!-- <td class="text-center"><?php echo @$row['eq_model'];?></td>										 -->
										<td class="text-center">
										<?php if($row['status']==1) { ?>
                                        <a class="btn btn-default" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_tool/'.storm_encode($row['tool_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php  } else { ?> <a class="btn btn-default" title="to edit please activate" readonly style="padding:3px 3px;" href="#"><i class="fa fa-pencil"></i></a>
                                          <?php  }
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_tool/'.storm_encode($row['tool_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_tool/'.storm_encode($row['tool_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
                                    </td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			<?php 
			} ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	check_part_number_availability();
	check_tool_code_availability();
	
</script>