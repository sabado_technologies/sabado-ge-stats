<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'print_qr';?>"  parsley-validate novalidate method="post">
								<input type="hidden" name="asset_id" id="asset_id" value="<?php echo @$asset_id;?>">                    

								
								<div class="form-group">
									<label for="inputeq_model" class="col-sm-3 control-label">Lable Size <span class="req-fld">*</span></label>
		                            <div class="col-sm-3">
										<select class="form-control" name="lable_size">
											<?php
											foreach($qr_lables as $lrow)
											{
												echo '<option value="'.$lrow['lable_id'].'">'.$lrow['name'].'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputeq_model" class="col-sm-3 control-label">Printer Model <span class="req-fld">*</span></label>
		                            <div class="col-sm-3">
		                            	<?php $printer_models = get_printer_models(); ?>
										<select class="form-control" name="printer_model">
											<?php
											foreach($printer_models as $prow)
											{
												echo '<option value="'.$prow['id'].'">'.$prow['name'].'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-3">
										<button class="btn btn-primary" type="submit" name="generate_qr" value="1"><i class="fa fa-check"></i> Generate QR</button>
										<a class="btn btn-danger" href="<?php echo $cancel_url;?>"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
							</form>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	check_part_number_availability();
	check_tool_code_availability();
	
</script>