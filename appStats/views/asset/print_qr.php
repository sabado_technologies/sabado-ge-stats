<html>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo assets_url()?>css/storm/print.css">
		
	</head>
	<body>
	<?php
	
	switch($lable_size)
	{
		case 1:
	?>
		<style type="text/css">
		table {width:38mm; height:10mm; font-family: 'Open Sans', sans-serif; position: fixed; top:0px; left: 0px; }
		td{ font-size:4px; font-weight: bold;}
		.lable_head{ width: 7mm !important;}
		.logo { width:6px; height: 6px;}
		.qr_img { width: 22px; height: 22px;}
		@page {
		  size: 38mm 10mm;
		  margin: 0mm;
		}
		@media print
		{
			html, body {
		    width: 38mm;
		    height: 10mm;
		    margin:0mm;
		  }
		}
		</style>
		<!-- <h6 class="no_print">38mmX10mm</h6>
		<table>
			<tr>
				<td class="lable_head" align="left">Desp</td><td align="left">XD MAPPING TOOL</td>
				<td rowspan="3" align="center" valign="top">
					<img src="<?php echo assets_url();?>images/ge_logos/ge1.png" class="logo"><br>
					<img src="<?php echo SITE_URL?>tes1.png" class="qr_img" />
				</td></tr>
			<tr><td class="lable_head" align="left">Part No</td><td align="left">46-195763P1</td></tr>
			<tr><td class="lable_head" align="left">Asset No</td><td align="left">WGETEST0123456789</td></tr>
		</table> -->
		<table>
			<tr><td colspan="3" style="border-bottom:1px solid;" align="left">
			<img src="<?php echo assets_url();?>images/ge_logos/ge-wb1.png" class="logo"> GE Health Care</td></tr>
			<tr>
				<td class="lable_head" align="left">Tool Desc :</td><td align="left">XD MAPPING TOOL</td>
				<td rowspan="3" align="left" valign="top">
					<img src="<?php echo SITE_URL?>tes1.png" class="qr_img" />
				</td></tr>
			<tr><td class="lable_head" align="left">Tool No :</td><td align="left">46-195763P1</td></tr>
			<tr><td class="lable_head" align="left">Asset No :</td><td align="left">WGETEST0123456789</td></tr>
		</table>
	<?php
		break;
		case 2:
			$width = $lable['width'].'mm'; $height = $lable['height'].'mm';
	?>
		<style type="text/css">
		table {width:<?php echo $width;?>; height:<?php echo $height;?>; position: fixed; top:0px; left: 0px; 
			font-family: 'Open Sans', sans-serif;}
		td{ font-size:9px; font-weight: bold; padding: 0px; margin: 0px;}
		.lable_head{ width: 13mm !important;}
		.logo { width:11px; height: 11px;}
		@page {
		  size: A8;
		  margin: 0mm;
		}
		@media print
		{
			html, body {
		    width:<?php echo $width;?>; height:<?php echo $height;?>;
		    padding: 0px; margin: 0px;
		    margin: 0mm;
		    position: fixed; top: 0px;
		  }
		  
		}
		</style>
		<!-- <h6 class="no_print">50mmX15mm</h6> -->
		<table>
			<tr><td colspan="3" style="border-bottom:1px solid;" align="left" valign="center"><img src="<?php echo assets_url();?>images/ge_logos/ge-wb1.png" class="logo"> GE Health Care</td></tr>
			<tr>
				<td class="lable_head" align="left">Tool Desc :</td><td align="left"><?php echo $asset['part_description']?></td>
				<td rowspan="3" align="center" valign="bottom">
					<img src="<?php echo qr_temp_url().$asset['asset_number'].'.png'?>" class="qr_img" />
				</td></tr>
			<tr><td class="lable_head" align="left">Tool No :</td><td align="left"><?php echo $asset['part_number']?></td></tr>
			<tr><td class="lable_head" align="left">Asset ID :</td><td align="left"><?php echo $asset['asset_number']?></td></tr>
		</table>
	<?php
		break;
		case 3:
			$width = $lable['width'].'mm'; $height = $lable['height'].'mm';
	?>
		
		<style type="text/css">
		table {width:<?php echo $width;?>; height:<?php echo $height;?>; position: fixed; top:0px; left: 0px; 
			font-family: 'Open Sans', sans-serif;}
		td{ font-size:12px; font-weight: bold; padding: 0px; margin: 0px;}
		.lable_head{ width: 18mm !important;}
		.logo { width:25px; height: 25px;}
		@page {
		  size: A8;
		  margin: 0mm;
		}
		@media print
		{
			html, body {
		    width:<?php echo $width;?>; height:<?php echo $height;?>;
		    padding: 0px; margin: 0px;
		    margin: 0mm;
		    position: fixed; top: 0px;
		  }
		  
		}
		</style>
		<table>
			<tr><td colspan="3" style="border-bottom:1px solid;" align="left" valign="center"><img src="<?php echo assets_url();?>images/ge_logos/ge-wb1.png" class="logo"> GE Health Care</td></tr>
			<tr>
				<td class="lable_head" align="left">Tool Desc :</td><td align="left" colspan="2"><?php echo $asset['part_description']?></td>
				</tr>
			<tr><td class="lable_head" align="left">Tool No :</td><td align="left"><?php echo $asset['part_number']?></td>
				<td rowspan="2" align="center" valign="bottom">
					<img src="<?php echo qr_temp_url().$asset['asset_number'].'.png'?>" class="qr_img" />
				</td>
			</tr>
			<tr><td class="lable_head" align="left">Asset ID :</td><td align="left"><?php echo $asset['asset_number']?></td></tr>
		</table>
	<?php
		break;
		case 4:
			$width = $lable['height'].'mm'; $height = $lable['width'].'mm';
			$rotation = $lable['rotation'];
	?>
		<style type="text/css">
		table {width:<?php echo $width;?>; height:<?php echo $height;?>; position: fixed; top:0px; left: 0px; 
			font-family: 'Open Sans', sans-serif;}
		td{ font-size:20px; font-weight: bold; padding: 0px; margin: 0px;}
		.lable_head{ width: 30mm !important;}
		.logo { width:35px; height: 35px;}
		@page {
		  size: A6;
		  margin: 0mm;
		}
		@media print
		{
			html, body {
		    width:<?php echo $width;?>; height:<?php echo $height;?>;
		    padding: 0px; margin: 0px;
		    margin: 0mm;
		    position: fixed; top: 0px;
		  }
		  table { 
				transform: rotate(<?php echo $rotation;?>deg);
				-webkit-transform: rotate(<?php echo $rotation;?>deg); 
				-moz-transform: rotate(<?php echo $rotation;?>deg); 
				-ms-transform: rotate(<?php echo $rotation;?>deg); 
				-o-transform: rotate(<?php echo $rotation;?>deg);
				margin-top: 130px;
			}
		  
		}
		</style>
		<table>
			<tr><td colspan="3" style="border-bottom:1px solid;" align="left" valign="center"><img src="<?php echo assets_url();?>images/ge_logos/ge-wb1.png" class="logo"> GE Health Care</td></tr>
			<tr>
				<td class="lable_head" align="left">Tool Desc :</td><td align="left" colspan="2"><?php echo $asset['part_description']?></td>
				</tr>
			<tr><td class="lable_head" align="left">Tool No :</td><td align="left"><?php echo $asset['part_number']?></td>
				<td rowspan="2" align="center" valign="top">
					<img src="<?php echo qr_temp_url().$asset['asset_number'].'.png'?>" class="qr_img" />
				</td>
			</tr>
			<tr><td class="lable_head" align="left">Asset ID :</td><td align="left"><?php echo $asset['asset_number']?></td></tr>
		</table>
	<?php
		break;
		case 5:
			$width = $lable['width'].'mm'; $height = $lable['height'].'mm';
	?>
		<style type="text/css">
		table {width:<?php echo $width;?>; height:<?php echo $height;?>; position: fixed; top:0px; left: 0px; 
			font-family: 'Open Sans', sans-serif;}
		td{ font-size:15px; font-weight: bold; padding: 0px; margin: 0px;}
		.lable_head{ width: 22mm !important;}
		.logo { width:25px; height: 25px;}
		@page {
		  size: A6;
		  margin: 0mm;
		}
		@media print
		{
			html, body {
		    width:<?php echo $width;?>; height:<?php echo $height;?>;
		    padding: 0px; margin: 0px;
		    margin: 0mm;
		    position: fixed; top: 0px;
		  }
		  
		}
		</style>
		<table>
			<tr><td colspan="3" style="border-bottom:1px solid;" align="left" valign="center"><img src="<?php echo assets_url();?>images/ge_logos/ge-wb1.png" class="logo"> GE Health Care</td></tr>
			<tr>
				<td class="lable_head" align="left">Tool Desc :</td><td align="left" colspan="2"><?php echo $asset['part_description']?></td>
				</tr>
			<tr><td class="lable_head" align="left">Tool No :</td><td align="left"><?php echo $asset['part_number']?></td>
				<td rowspan="2" align="center" valign="top">
					<img src="<?php echo qr_temp_url().$asset['asset_number'].'.png'?>" class="qr_img" />
				</td>
			</tr>
			<tr><td class="lable_head" align="left">Asset ID :</td><td align="left"><?php echo $asset['asset_number']?></td></tr>
		</table>
	<?php
		break;
		case 6:
			$width = $lable['height'].'mm'; $height = $lable['width'].'mm';
			$rotation = $lable['rotation'];
	?>
		<style type="text/css">
		table {width:<?php echo $width;?>; height:<?php echo $height;?>; position: fixed; top:0px; left: 0px; 
			font-family: 'Open Sans', sans-serif;}
		td{ font-size:35px; font-weight: bold; padding: 0px; margin: 0px;}
		.lable_head{ width: 50mm !important;}
		.logo { width:50px; height: 50px;}
		@page {
		  size: A6;
		  margin: 0mm;
		}
		@media print
		{
			html, body {
		    width:<?php echo $width;?>; height:<?php echo $height;?>;
		    padding: 0px; margin: 0px;
		    margin: 0mm;
		    position: fixed; top: 0px;
		  }
		  table { 
				transform: rotate(<?php echo $rotation;?>deg);
				-webkit-transform: rotate(<?php echo $rotation;?>deg); 
				-moz-transform: rotate(<?php echo $rotation;?>deg); 
				-ms-transform: rotate(<?php echo $rotation;?>deg); 
				-o-transform: rotate(<?php echo $rotation;?>deg);
				margin-top: 230px;
			}
		  
		}
		</style>
		<table>
			<tr><td colspan="3" style="border-bottom:1px solid;" align="left" valign="center"><img src="<?php echo assets_url();?>images/ge_logos/ge-wb1.png" class="logo"> GE Health Care</td></tr>
			<tr>
				<td class="lable_head" align="left">Tool Desc :</td><td align="left" colspan="2"><?php echo $asset['part_description']?></td>
				</tr>
			<tr><td class="lable_head" align="left">Tool No :</td><td align="left"><?php echo $asset['part_number']?></td>
				<td rowspan="2" align="center" valign="top">
					<img src="<?php echo qr_temp_url().$asset['asset_number'].'.png'?>" class="qr_img" />
				</td>
			</tr>
			<tr><td class="lable_head" align="left">Asset ID :</td><td align="left"><?php echo $asset['asset_number']?></td></tr>
		</table>
		<!-- <style type="text/css">
		table {width:<?php echo $width;?>; height:<?php echo $height;?>;}
		td{ font-size:35px; }
		.lable_head{ width: 40mm !important;}
		.logo { margin: 20px 0px;}
		
		@page {
		  size: A6;
		  margin: 0cm auto;
		}
		@media print
		{
			html, body {
		    width:<?php echo $width;?>; height:<?php echo $height;?>;
		    margin-top: 30px;
		  }
		  table { 
				transform: rotate(<?php echo $rotation;?>deg);
				-webkit-transform: rotate(<?php echo $rotation;?>deg); 
				-moz-transform: rotate(<?php echo $rotation;?>deg); 
				-ms-transform: rotate(<?php echo $rotation;?>deg); 
				-o-transform: rotate(<?php echo $rotation;?>deg);
				margin-top: 230px;
			}
		}
		</style>
		<h4 class="no_print">100mmX250mm</h4>
		<table>
			<tr>
				<td class="lable_head" align="left">Desp</td><td align="left">XD MAPPING TOOL</td>
				<td rowspan="3" align="center" valign="top">
					<img src="<?php echo assets_url();?>images/ge_logos/ge3.png" class="logo"><br>
					<img src="<?php echo SITE_URL?>tes7.png" class="qr_img" />
				</td></tr>
			<tr><td class="lable_head" align="left">Part No</td><td align="left">46-195763P1</td></tr>
			<tr><td class="lable_head" align="left">Asset No</td><td align="left">WGETEST0123456789</td></tr>
		</table> -->
	<?php
		break;
	}
	?>
	</body>
</html>