<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			<?php
			if(isset($search_by_part))
			{ ?>
				<div class="row"> 
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">

								<form class="form-horizontal" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
									<div class="row">
										<?php
										if($task_access==3)
										{
											?>
											<div class="form-group">
												<label for="inputeq_model" class="col-sm-4 control-label">Country <span class="req-fld">*</span></label>
												<div class="col-md-5">
													<select class="country_id select2"  name="country_id" required >
														 <?php
														foreach($countryList as $country)
														{
															$selected = ($country['location_id']==@$selected_country)?'selected="selected"':'';
															echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
														}
														?>
													</select>
												</div>
											</div> <?php
										}
										else
										{ ?>
											<input type="hidden" name="country_id" class="country_id" value="<?php echo $selected_country; ?>">
										<?php }
										?>
										<div class="form-group">
											<label for="inputeq_model" class="col-sm-4 control-label">Tool <span class="req-fld">*</span></label>
											<div class="col-md-5">
												<select name="tool_id" required class="tool_list" style="width:100%"> 
													<option value="">- Select Tool Number/Description -</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-offset-5 col-sm-5">
												<button class="btn btn-primary" type="submit" value="1" name="submitsearch"><i class="fa fa-check"></i> Submit</button>
												<a class="btn btn-danger" href="<?php echo SITE_URL;?>asset"><i class="fa fa-times"></i> Cancel</a>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php 
			}
			else if(isset($flg))
			{
			    ?>
			    <form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				    <input type="hidden" name="encoded_id" value="<?php echo @storm_encode($trow['tool_id']); ?>">
				    <div class="row"> 
				    <input type="hidden" name="asset_id" value="<?php echo @storm_encode($assetDetails['asset_id']);?>"> 
				    <input type="hidden" name="a_id" id="asset_id" value="<?php echo @$assetDetails['asset_id'];?>"> 
				    <input type="hidden" name="country_id" value="<?php echo $country_id;?>">
				    <input type="hidden" name="count_c" id="count_c" value="<?php echo @$count_c;?>"> 
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">	
									<div class="col-md-12">
										<div class="header">
										<h5  style="float: left;color: #3380FF;margin-top:5px;"><strong>Country : <?php echo $country_name; ?></strong></h5>
										<h4 align="center" style="padding-right: 113px;margin-bottom: 5px;"><strong>Tools Details</strong></h4>
									</div>
									</div>
									<div class="col-md-6">
										<table class="no-border">
											<tbody class="no-border-x no-border-y">
												<tr>
													<td class="data-lable"><strong>Tool Number:</strong></td>
											        <td class="data-item"><?php echo @$trow['part_number'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Asset Level:</strong></td>
											        <td class="data-item"><?php echo @$trow['asset_level_name'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Tool Type:</strong></td>
											        <td class="data-item"><?php echo @$trow['tool_type_name'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Supplier:</strong></td>
											        <td class="data-item"><?php echo @$trow['supplier_name'];?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Kit:</strong></td>
											        <td class="data-item"><?php if($trow['kit']==1)
											        {
											            echo "Yes";
											        }else
											        {
											        	echo "No";
											        }
											        ?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Manufacturer:</strong></td>
											        <td class="data-item"><?php echo @$trow['manufacturer'];?></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="col-md-6">
										<table class="no-border">
											<tbody class="no-border-x no-border-y">
												<tr>
													<td class="data-lable"><strong>Tool Code:</strong></td>
													<td class="data-item"><?php echo @$trow['tool_code']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Tool Description:</strong></td>
													<td class="data-item"><?php echo @$trow['part_description']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Modality:</strong></td>
													<td class="data-item"><?php echo @$trow['modality_name']?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Calibration:</strong></td>
													<input type="hidden" name="cal_type_id" value="<?php echo $trow['cal_type_id'];?>">
													<td class="data-item"><?php if($trow['cal_type_id']==1)
											        {
											            echo "Required";
											        }else
											        {
											        	echo "Not Required";
											        }
											        ?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Asset Type</strong></td>
													<td class="data-item"><?php if($trow['asset_type_id']==1)
											        {
											            echo "Assembly";
											        }else
											        {
											        	echo "Component";
											        }
											        ?></td>
												</tr>
												<tr>
													<td class="data-lable"><strong>Equipment Model</strong></td>
													<td class="data-item"><?php echo @$eqrow['eq_model']?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>	
								<br>
								<div class="row">
									<div class="header" style="margin-bottom: 15px;">
										<h4 align="center"><strong>Asset <?php echo @$assetDetails['asset_number']; ?> Details</strong></h4>			
									</div>
								
									<input type="hidden" name="tool_id" id="tool_id" value="<?php echo @$lrow['tool_id'];?>">
									<input type="hidden" name="asset_id"  value="<?php echo @storm_encode($assetDetails['asset_id']);?>">                      

									<!-- Main Asset-->
									
									<div class="form-group">
										<div class="col-md-12">
											<label for="inputeq_model" class="col-sm-2 control-label"> GPS Tracking UID </label>
				                            <div class="col-sm-4">

												<select class="select2" name="gps_tool_id">
		                                            <option value="">- GPS Tracking UID -</option>
		                                            <?php
		                                              foreach($gps_tool_list as $row)
		                                              {
		                                              	$selected = ($row['gps_tool_id']==@$assetDetails['gps_tool_id'])?'selected="selected"':'';
		                                              	   
		                                                  echo '<option value="'.@$row['gps_tool_id'].'"  '.$selected.'>'.@$row['uid_number'].' - '.@$row['name'].'</option>';
		                                              }
		                                              ?>
		                                        </select> 
											</div>
											<?php if($trow['cal_type_id']==1){ ?>
											<label for="inputeq_model" class="col-sm-2 control-label"> Calibration Supplier <span class="req-fld">*</span></label>
				                            <div class="col-sm-4">

												<select class="form-control" required name="cal_supplier_id"  >
		                                            <option value="">Select Calibration Supplier</option>
		                                            <?php
		                                              foreach($cal_supplier_details as $row)
		                                              {
		                                              	   if(@$flg == 1){
		                                                  		$selected = ($row['supplier_id']==@$trow['cal_supplier_id'])?'selected="selected"':'';
		                                              	   }
		                                              	   else
		                                              	   {
		                                              	   		$selected = ($row['supplier_id']==@$assetDetails['cal_supplier_id'])?'selected="selected"':'';
		                                              	   }
		                                                  echo '<option value="'.@$row['supplier_id'].'"  '.$selected.'>'.@$row['name'].'</option>';
		                                              }
		                                              ?>
		                                        </select> 
											</div>
											<?php } ?>
											
										</div>
									</div>
									<?php 
									if($trow['cal_type_id']==1)
									{
										if($flg == 1)
										{
											$rc_asset_id = 0;
										} 
										else
										{
											$rc_asset_id = check_for_rc_entry($asset_id);

										} ?>
										<div class="form-group">
											<div class="col-md-12">							
												<label for="inputName" class="col-sm-2 control-label">Calibrated Date <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<?php if($rc_asset_id == 0){ ?>
													<input class="form-control date" required size="16" type="text" autocomplete="off"  value="<?php if(@$assetDetails['calibrated_date']!='') {echo indian_format(@$assetDetails['calibrated_date']);} ?>" readonly placeholder="Calibrated Date" name="calibrated_date" style="cursor:hand;background-color: #ffffff">	
													<?php }else { 

														if(@$assetDetails['calibrated_date']!='') { $cal_date = indian_format(@$assetDetails['calibrated_date']);}else{ $cal_date = "--";} ?>
														<b><p style="margin-bottom:0px;margin-top:9px;"><?php echo $cal_date; ?></p></b>
														<input type="hidden" name="calibrated_date" value="<?php if(@$assetDetails['calibrated_date']!='') {echo indian_format(@$assetDetails['calibrated_date']);} ?>">
													<?php } ?>
												</div>	
												<label for="inputName" class="col-sm-2 control-label">Calibration Due Date <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<?php if($rc_asset_id == 0){ ?>
													<input class="form-control date" required size="16" type="text" autocomplete="off"  value="<?php if(@$assetDetails['cal_due_date']!='') { echo indian_format(@$assetDetails['cal_due_date']); } ?>" readonly placeholder="Calibration Due Date" name="calibration_due_date" style="cursor:hand;background-color: #ffffff">	
													<?php }else { 
														if(@$assetDetails['cal_due_date']!='') { $cal_due_date = indian_format(@$assetDetails['cal_due_date']); }else{ $cal_due_date = "--"; } ?>
														<b><p style="margin-bottom:0px;margin-top:9px;"><?php echo $cal_due_date; ?></p></b>
														<input type="hidden" name="calibration_due_date" value="<?php if(@$assetDetails['cal_due_date']!='') { echo indian_format(@$assetDetails['cal_due_date']); } ?>">
													<?php } ?>
												</div>	
												
											</div>
										</div><?php 
									} ?>	
									<div class="form-group">
										<div class="col-md-12">
											<label for="inputeq_model" class="col-sm-2 control-label"> Warehouse <span class="req-fld">*</span></label>
				                            <div class="col-sm-4">
				                            	<?php if($flg == 1){ ?>
												<select class="select2" required name="wh_id">
		                                            <option value="">Select Warehouse</option>
		                                            <?php
		                                              	foreach($wh_details as $row)
		                                              	{
		                                                  	echo '<option value="'.@$row['wh_id'].'">'.@$row['wh_code'].' -('.$row['name'].')</option>';
		                                              	}
		                                              ?>
		                                        </select> 
		                                        <?php } 
		                                        else if($flg == 2 && $assetDetails['status']==1){?>
		                                        <select class="select2" required name="wh_id">
		                                            <option value="">Select Warehouse</option>
		                                            <?php
		                                              	foreach($wh_details as $row)
		                                              	{
		                                              		$selected = '';
		                                              		if(@$row['wh_id']==@$assetDetails['wh_id'])
		                                              		{
		                                              			$selected="selected";
		                                              		}
		                                                  	echo '<option value="'.@$row['wh_id'].'"  '.$selected.'>'.@$row['wh_code'].' -('.$row['name'].')</option>';
		                                              	}
		                                              ?>
		                                        </select> 
		                                        <?php } else{?>
		                                        <input type="hidden" name="wh_id" value="<?php echo $assetDetails['wh_id']; ?>">

		                                        <?php 
		                                    		foreach($wh_details as $row)
	                                              	{
	                                                  	$selected = '';
	                                              		if(@$row['wh_id']==@$assetDetails['wh_id'])
	                                              		{
	                                              			$selected="selected";
	                                              		}
	                                                  	if($selected === 'selected')
	                                                  	{
	                                                  		echo '<b><p style="margin-bottom:0px;margin-top:9px;">'.@$row['wh_code'].' -('.$row['name'].')</p></b>';
	                                                  	}
	                                              	}
	                                            } ?>
											</div>
											<label for="inputName" class="col-sm-2 control-label">Pallet Location </label>
											<div class="col-sm-4">
												<input type="text" autocomplete="off"  class="form-control" placeholder="Pallet Location " name="sub_inventory" id="" value="<?php echo @$assetDetails['sub_inventory']; ?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Actual Cost <span class="req-fld">*</span></label>
											<div class="col-sm-3" style="padding-right: 0px !important;">
												<input type="text" autocomplete="off" required class="form-control only-numbers" placeholder="Actual Cost" name="cost" value="<?php echo @$assetDetails['cost']; ?>">
											</div>
											<div class="col-sm-1" style="padding-left: 0px !important;">
												<select class="form-control"  name="currency_id">
		                                            <?php
		                                              foreach($currencyList as $cur)
		                                              {
		                                              $selected = ($cur['currency_id']==@$assetDetails['currency_id'])?'selected="selected"':'';
		                                              echo '<option value="'.@$cur['currency_id'].'"  '.$selected.'>'.@$cur['name'].'</option>';
		                                              }
		                                              ?>
		                                        </select>
											</div>

											<label for="inputName" class="col-sm-2 control-label">EARPE  Number <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<input type="text" autocomplete="off" required class="form-control" placeholder="EARPE Number" name="earpe_number" value="<?php echo @$assetDetails['earpe_number']; ?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Cost in <?php echo $currency_name;?> <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<input type="text" autocomplete="off" required class="form-control only-numbers" placeholder="Cost in <?php echo $currency_name;?>" name="cost_in_inr" value="<?php echo @$assetDetails['cost_in_inr']; ?>">
											</div>
											<label for="inputName" class="col-sm-2 control-label">PO Number <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<input type="text" autocomplete="off" required class="form-control" placeholder="PO Number" name="po_number" value="<?php echo @$assetDetails['po_number']; ?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											
											<label for="inputName" class="col-sm-2 control-label">Date Of Use <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<input class="form-control date" required size="16" type="text" autocomplete="off" value="<?php if(@$assetDetails['date_of_use']!='') { echo indian_format(@$assetDetails['date_of_use']); }?>" readonly placeholder="Date Of Use" name="date_of_use" style="cursor:hand;background-color: #ffffff">								
											</div>
											<label for="inputName" class="col-sm-2 control-label">Remarks </label>
											<div class="col-sm-4">
												<textarea name="remarks" class="form-control"><?php echo @$assetDetails['remarks']; ?></textarea>
											</div>
										</div>
									</div>
								</div><br>
							</div>
						</div>				
					</div>
				</div><br>
				<?php $sno = 1; ?>
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="header" style="margin-bottom: 15px;">
								<h4 align="center"><strong>Main Component</strong></h4>			
							</div>
                        	<div class="row">
                        		<div class="form-group">
	                        		<div class="col-md-12">
	                        			<label class="col-sm-1 control-label auto_inc"><strong><?php echo $sno++.')';?></strong></label>
										<label class="col-sm-1 control-label">Tool :</label>
										<div class="col-sm-4">
											<p class="form-control-static" style="margin-top: 3px;"><strong><?php echo $mainComponent['part_number'].' - '.$mainComponent['part_description']; ?></strong></p>
										</div>
	                        			<label class="col-sm-2 control-label">Serial Number <span class="req-fld">*</span></label>
	                        			<div class="col-sm-3 serial_class">
											<input type="text" autocomplete="off" 
											class="form-control serial_number main_serial" 
											placeholder="Serial Number" 
											data-tool_id="<?php echo $mainComponent['tool_id']; ?>" data-tool_part_id="<?php echo @$m_part_id; ?>"
											data-country_id="<?php echo @$country_id; ?>" 
											name="m_serial_number" 
											value="<?php echo @$toolDetails['serial_number']; ?>">
										</div>
									</div>
                        		</div>
                        		<div class="form-group">
	                        		<div class="col-md-12">
	                        			<?php if($trow['asset_level_id']==3){?>
											<label class="col-sm-2 control-label">Quantity <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<input type="text" autocomplete="off" required class="form-control only-numbers" name="m_quantity" maxlength="3" value="<?php echo @$toolDetails['quantity']; ?>">
											</div>
										<?php } else
										{?>
											<label class="col-sm-2 control-label">Quantity :</label>
											<div class="col-sm-4">
												<p class="form-control-static" style="margin-top: 3px;"><strong>1</strong></p>
											</div>
										<?php } ?>

										<label class="col-sm-2 control-label">Status <span class="req-fld">*</span></label>
									<div class="col-sm-3">
										<select name="m_asset_condition_id" class="form-control main_status">
											<?php 
												foreach($status_details as $status)
												{
													$selected = ($status['asset_condition_id']==@$toolDetails['status'])?'selected="selected"':'';
													echo '<option value="'.$status['asset_condition_id'].'" '.$selected.'>'.$status['name'].'</option>';
												}
											?>
										</select>
									</div>										
									</div>
                        		</div>
								<div class="form-group main_remark <?php if(@$toolDetails['status']==1 || @$flg == 1) { echo 'hidden'; } ?>">
								<div class="col-md-12">
										<label class="col-sm-2 control-label">Remarks <span class="req-fld">*</span></label>
										<div class="col-sm-3">
											<textarea class="form-control parent_part_remarks"  name="m_remarks"><?php echo @$toolDetails['remarks'];  ?></textarea>
										</div>
								</div>	
								</div><br>
								<?php if(count($subComponent)>0)
								{ ?>

									<div class="header" style="margin: 15px;">
										<h4 align="center"><strong>Sub Components</strong></h4>			
									</div><?php
									foreach($subComponent as $row) 
									{ ?>
										<div class="asset_parent sub_div" <?php if(@$flg==2 && @$subComp[$row['tool_id']]['serial_number']=="") {?>  style="background-color: #ffe7c9 !important; padding-top: 10px;" <?php } ?>>
			                        		<div class="form-group">
				                        		<div class="col-md-12">
				                        			<label class="col-sm-1 control-label auto_inc"><strong><?php echo $sno++.')';?></strong></label>
													<label class="col-sm-1 control-label">Tool :</label>
													<div class="col-sm-4">
														<p class="form-control-static" style="margin-top: 3px;"><strong><?php echo $row['part_number'].' - '.$row['part_description']; ?></strong></p>
													</div>
				                        			<label class="col-sm-2 control-label">Serial Number <span class="req-fld">*</span></label>
				                        			<div class="col-sm-3 serial_class">
				                        				
														<input type="text" autocomplete="off" class="form-control serial_number" placeholder="Serial Number" data-tool_id="<?php echo $row['tool_id']; ?>" name="c_serial_number[<?php echo $row['tool_id']; ?>]" value="<?php echo @$subComp[$row['tool_id']]['serial_number']; ?>">
													</div>
													<div class="col-sm-1">
														<a style="padding:5px 15px;" data-toggle="tooltip" title="Remove"  class="btn btn-danger btn-sm delete_asset"> <i class="fa fa-trash-o"></i></a>
													</div>
												</div>
			                        		</div>
			                        		<div class="form-group">
				                        		<div class="col-md-12">
				                        			<label class="col-sm-2 control-label">Quantity :</label>
													<div class="col-sm-4">
														<p class="form-control-static" style="margin-top: 3px;"><strong><?php echo $row['quantity'] ?></strong></p>
													</div>

													<label class="col-sm-2 control-label">Status <span class="req-fld">*</span></label>
													<div class="col-sm-3">
														<select name="c_status[<?php echo $row['tool_id']; ?>]" class="form-control parent_asset_status_id" > 
															
															<?php 
																foreach($status_details as $status)
																{
																	$selected = ($status['asset_condition_id']==@$subComp[$row['tool_id']]['status'])?'selected="selected"':'';
																	echo '<option value="'.$status['asset_condition_id'].'" '.$selected.'>'.$status['name'].'</option>';
																}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													
													<div class="parent_part_remarks_div <?php if(@$flg == 1) { echo "hidden"; }
													else if(@$flg==2 && @$subComp[$row['tool_id']]['status']==1) { echo "hidden"; }
													else if(@$flg==2 && @$subComp[$row['tool_id']]['status']=="") { echo "hidden"; }?>">
														<label class="col-sm-2 control-label">Remarks <span class="req-fld">*</span></label>
														<div class="col-sm-3">
															<textarea class="form-control parent_part_remarks"  name="c_remarks[<?php echo $row['tool_id']; ?>]"><?php echo @$subComp[$row['tool_id']]['remarks'];  ?></textarea>
														</div>
													</div>
												</div>	
											</div><hr>
										</div>
								<?php } 
								} ?>
						</div>
						<div class="row">
							<div class="col-md-offset-10 col-md-2">
								<a id="add_document" class="btn blue tooltips"><i class="fa fa-plus"></i> Add Document</i></a>
							</div>
                    		<div class="col-md-12">
	                        	<div class="table-responsive col-md-offset-1 col-md-9 tab_hide">
									<table class="table table-bordered hover document_table">
										<thead>
											<tr>
												<th class="text-center" width="8%"><strong>S.No.</strong></th>
												<th class="text-center"><strong>Document Type</strong></th>
												<th width="30%" class="text-center"><strong>Supported Document</strong></th>
												<th width="8%" class="text-center"><strong>Delete</strong></th>
											</tr>
										</thead>
										<tbody>
										<?php $sno = 1;
												if(count(@$attach_document)>0)
											{	
												foreach($attach_document as $ad)
												{?>	
													<tr class="attach">
														<input type="hidden" name="doc_id" class="asset_doc_id" value="<?php echo $ad['asset_doc_id'];?>">
														<td>
	                                                        <span class="sno"><?php echo $sno++; ?></span>
	                                                    </td>
														<td align="center">
																<?php 
																	foreach($documenttypeDetails as $doc)
																	{
																		if($doc['document_type_id']==@$ad['document_type_id'])
																		{
																			echo $doc['name'].' - '.$ad['created_time']; 
																		}
																	}
																?>
														</td>
														<td>
					                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
														</td>
														<td>
														<?php if($ad['status']==1)
														{ ?>
															<a class="btn link btn-danger btn-sm deactivate" > <i class="fa fa-trash-o"></i></a>
												        
														<?php } 
														else
															{?>
															<a class="btn link btn-info btn-sm activate" > <i class="fa fa-check"></i></a>
														<?php } ?>
												        </td>
													</tr>
												 <?php		
												} 
											} ?>			
											<tr class="doc_row">
												<td>
                                                    <span class="sno"><?php echo $sno; ?></span>
                                                </td>
												<td>
													<select name="document_type[1]" class="form-control doc_type" > 
														<option value="">Select Document Type</option>
														<?php 
															foreach($documenttypeDetails as $doc)
															{
																echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
															}
														?>
													</select>
												</td>
												<td>
			                                		<input type="file" class="document" id="document" name="support_document_1">
												</td>
												
										        <td><a class="btn btn-danger btn-sm remove_bank_row" <?php if($flg == 2){ ?> style='display:none'; <?php }?> > <i class="fa fa-trash-o"></i></a></td>
											</tr>
										</tbody>
									</table><br>
								</div>
                    		</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-5">
								<button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure you want to Submit?')" name="submit_tool" value="1"><i class="fa fa-check"></i> Submit</button>
								<?php if($flg == 1){ ?>
								<button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure you want to Submit And Print?')" value="1" name="generate_submit_tool"><i class="fa fa-qrcode"></i>  Submit & Generate QR</button><?php } ?>
								<a class="btn btn-danger" href="<?php echo SITE_URL;?>asset"><i class="fa fa-times"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div><br>

				</form>
				<?php 
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>asset">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-3">
	                                    <input type="text" autocomplete="off" name="asset_part_no" placeholder="Tool Number" value="<?php echo @$searchParams['asset_part_no'];?>"  class="form-control">
									</div>
		                            <div class="col-sm-3">
	                                    <input type="text" autocomplete="off" name="asset_part_desc" placeholder="Tool Description" value="<?php echo @$searchParams['asset_part_desc'];?>"  class="form-control">
									</div>
									<div class="col-sm-3">
	                                    <input type="text" autocomplete="off" name="a_asset_no" placeholder="Asset Number" value="<?php echo @$searchParams['a_asset_no'];?>" class="form-control">
									</div>
									<div class="col-sm-3">
									    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$searchParams['serial_no'];?>" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-3">
										<select name="asset_modality_arr[]" multiple="multiple" class="select2-container select2-container-multi select2 modality-multiple" style=" width: 100%;"  > 
											<?php 
												$selected='';
												foreach($modality_details as $mod)
												{
													$modality_arr = array(0);
													if(@$searchParams['asset_modality_arr']!='')
													{
														$modality_arr = @$searchParams['asset_modality_arr'];
													}
													if(in_array(@$mod['modality_id'], $modality_arr))
													{
														$selected = 'selected';
													}
													else
													{
														$selected='';
													}
													
													echo '<option value="'.$mod['modality_id'].'" '.$selected.'>'.$mod['name'].'</option>';
												}
											?>
										</select>
									</div>
									
		                            <div class="col-sm-3">
										<select name="asset_wh_arr[]" class="select2-container select2-container-multi select2 inventory-multiple" style=" width: 100%;" multiple="multiple" > 
											<?php 
												$selected ='';
												foreach($wh_details as $wh)
												{
													$wh_arr = array(0);
													if(@$searchParams['asset_wh_arr']!='')
													{
														$wh_arr = @$searchParams['asset_wh_arr'];
													}
													if(in_array($wh['wh_id'], @$wh_arr))
													{
														$selected = 'selected';
													}
													else
													{
														$selected='';
													}
													
													echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_code'].' -('.$wh['name'].')</option>';
												}
											?>
										</select>
									</div>
		                            <div class="col-sm-3">
										<select name="asset_status_arr[]" multiple="multiple" class="select2-container select2-container-multi select2 status-multiple" style=" width: 100%;"> 
											<?php 
												$selected ='';
												foreach($status_data as $status)
												{
													$status_arr = array(0);
													if(@$searchParams['asset_status_arr']!='')
													{
														$status_arr = @$searchParams['asset_status_arr'];
													}
													if(in_array(@$status['asset_status_id'], @$status_arr))
													{
														$selected = 'selected';
													}
													else
													{
														$selected='';
													}
													
													echo '<option value="'.$status['asset_status_id'].'" '.$selected.'>'.$status['name'].'</option>';
												}
											?>
										</select>
									</div>
									<div class="col-sm-3">
										<select name="cal_type_id" class="select2" style=" width: 100%;">
											<option value="">- Calibration Type -</option>
											<option value="1" <?php if(@$searchParams['cal_type_id']!='' && @$searchParams['cal_type_id']==1){ echo "selected";} ?>>Calibration Asset</option>
											<option value="2" <?php if(@$searchParams['cal_type_id']!='' && @$searchParams['cal_type_id']==2){ echo "selected";} ?>>Non Calibration Asset</option>
										</select>
									</div>
								</div>	
							</div>
						</div>
						<?php
						if($task_access==3 && $this->session->userdata('header_country_id')=='')
						{?>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<div class="col-sm-3">
											<select name="asset_country_id" class="select2" > 
												<option value="">- Select Country -</option>
												<?php 
													foreach($countryList as $row)
													{
														$selected = ($row['location_id']==@$searchParams['asset_country_id'])?'selected="selected"':'';
														echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
													}
												?>
											</select>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
						<div class="row">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
							<div class="col-sm-12">
								<div class="col-sm-6 col-md-6" align="left">							
									<a href="<?php echo SITE_URL.'search_by_part'; ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
									<a href="<?php echo SITE_URL.'wge_bulkupload_asset'; ?>" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Upload in Bulk</a>
									
								</div>	
								<div class="col-sm-6 col-md-6" align="right">							
									<button type="submit" name="searchasset" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'asset'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a> 
								</div>		
							</div>
						</div>
					</form>
					<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.No</strong></th>
								<th class="text-center"><strong>Tool No.</strong></th>
								<th class="text-center"><strong>Tool Description</strong></th>
								<th class="text-center"><strong>Asset Number</strong></th>
								<th class="text-center"><strong>Serial Number</strong></th>
								<th class="text-center"><strong>Inventory</strong></th>
								<th class="text-center"><strong>Modality</strong></th>
								<th class="text-center"><strong>Tool Type</strong></th>
								<th class="text-center"><strong>Tool Availability</strong></th>
								<th class="text-center"><strong>Asset Status</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center" width="12%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($assetResults)>0)
							{
								foreach($assetResults as $row)
								{
									$position=get_asset_position_with_flag($row['asset_id']);
                                                    $text=$position[0];
                                                    $flag=$position[1];?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['part_number'];?></td>
										<td class="text-center"><?php echo @$row['part_description'];?></td>
										<td class="text-center"><?php echo @$row['asset_number'];?></td>
										<td class="text-center"><?php echo @$row['serial_number'];?></td>
										<td class="text-center"><?php echo @$row['wh_code'];?></td>
										<td class="text-center"><?php echo @$row['modality_name'];?></td>					
										<td class="text-center"><?php echo @$row['tool_type_name'].' '.'Tool';?></td>
										<td class="text-center"><?php echo get_asset_position($row['asset_id']);?></td>						
										<td class="text-center"><?php echo ($row['approval_status'] ==1)?get_da_status($row['asset_id'],1):@$row['status_name'];?></td>
										<td class="text-center"> <?php echo $row['location'];?></td>
										<td class="text-center">
											<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'assetview_details/'.storm_encode($row['asset_id']);?>"><i class="fa fa-eye"></i></a>	

                                        	<a class="btn btn-warning" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Update"  href="<?php echo SITE_URL.'edit_asset/'.storm_encode($row['asset_id']);?>"><i class="fa fa-pencil"></i></a>

                                       		<a class="btn btn-success" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Print QR"  href="<?php echo SITE_URL.'asset_qr/'.storm_encode($row['asset_id']).'/1';?>"><i class="fa fa-print"></i></a>
                                       		<?php if($flag==1 && $row['status'] ==1 && $row['approval_status']==0){ ?>
                                       		
                                        		<a class="btn btn-danger" style="padding:3px 3px;margin-top: 5px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Scrap Asset"  href="<?php echo SITE_URL.'raise_scrap_asset/'.storm_encode($row['asset_id']);?>" onclick="return confirm('Are you sure you want to Proceed for Scrap?')" ><i class="fa fa-trash-o"></i></a><?php 
                                        	} ?>
                                        	<?php 
                                    		$astatus = $row['status'];
                                    		$s_array = array(2,5,6,7,9,11);
                                    		if(in_array($astatus, $s_array) && $row['approval_status']==0){ ?>
                                				<a class="btn btn-blue" style="padding:3px 3px;margin-top: 5px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Change Status to 'AT WH'"  href="<?php echo SITE_URL.'change_status_to_at_wh/'.storm_encode($row['asset_id']);?>" onclick="return confirm('Are you sure you want to Release asset and change status to AT WH?')" ><i class="fa fa-chain-broken"></i></a><?php 
                                			} ?>

                                			<?php 
                                			$cal_status = array(1,4,12);
                                			if($row['region_id']==2 && $row['cal_type_id']==1 && in_array($astatus, $cal_status) && $flag == 1 && $row['approval_status']==0){ ?>
	                                    		<a class="btn btn-info" style="padding:3px 3px;margin-top: 5px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Calibration"  href="<?php echo SITE_URL.'egm_calibration/'.storm_encode($row['asset_id']);?>"><i class="fa fa-compass"></i></a>
	                                    	<?php }  
												$data = 
												array('primary_key' => storm_encode(@$row['asset_id']),
											          'table_name'  => 'asset',
											          'work_flow'   => 'asset_master',
											          'main_table'  => 'asset',
											          'main_key'    => $row['asset_id']);
												$send_data = implode('~',$data); 
											?>
											<a class="btn btn-default" 
												style="padding:3px 3px;margin-top: 5px;"
												href="<?php echo SITE_URL.'get_audit_page/'.$send_data;?>"
												target="_blank"
												data-toggle="tooltip" title="Audit Trail"><i class="fa fa-book"></i>
											</a>
                                    	</td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="12" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			<?php 
			} ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<?php if(isset($flg)) { ?>
<script type="text/javascript">
	add_component_new_row();
	check_serial_availablity();
	deactivate_attached_record();

</script>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select3').select2();
		var country_id = $('.country_id :selected').val();
		if(country_id  == undefined)
		{
			var country_id = $('.country_id').val();
		}
		select2Ajax('tool_list', 'get_asset_master_tools', country_id, 3);
		$(".modality-multiple").select2({
		    placeholder: "- Select Modality -"
		});
		$(".status-multiple").select2({
		    placeholder: "- Select Status -"
		});
		$(".inventory-multiple").select2({
		    placeholder: "- Select Inventory -"
		});
	});
	$('.country_id').on('change',function(){
	  var country_id = $(this).find(':selected').val();
	  select2Ajax('tool_list', 'get_asset_master_tools', country_id, 3);
	});

</script>