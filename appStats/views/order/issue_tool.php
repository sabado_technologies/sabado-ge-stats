<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:50%; z-index:2; opacity:0"><img src="<?php echo SITE_URL1;?>assets/images/ajax-loading-img.gif" /></div>
	<div class="cl-mcont " id="orderCartContainer">
	    <div class="row"> 
	        <div class="col-sm-12 col-md-12">
			    <?php echo $this->session->flashdata('response'); ?>			
				<div class="block-flat">
					<div class="content">
					    	<form class="form-horizontal" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
								<input class="form-control"  type="hidden" value="<?php echo @$order_info['tool_order_id'];?>"  name="tool_order_id" id="tool_order_id">
								<input type="hidden" name="count_c" id="count_c" value="<?php echo @$count_c;?>"> 
								<input type="hidden" name="current_stage_id"  value="<?php echo @$order_info['current_stage_id'];?>"> 
								<input type="hidden"  name ="sso_id" class="jquery_role_id transactionUser"  value="<?php echo @$_SESSION['transactionUser'];?>"> 
								<input type="hidden"  name ="transactionCountry" class="transactionCountry"  value="<?php echo @$_SESSION['transactionCountry'];?>"> 
								<input type="hidden" value="raise_order" class="transactionPage">
								<input type="hidden"  name ="final_sso_id" class="jquery_role_id"  value="<?php echo @$sso_id;?>"> 
								<input type="hidden"  name ="wh_id" class=""  value="<?php echo @$order_info['wh_id'];?>"> 
								<input type="hidden"  name ="conditionRole" class=""  value="<?php echo @$conditionRole;?>">
								

								<?php
								if(count($fe_tool))
								{
									foreach($fe_tool as $fe)
									{?>
										<input type="hidden" value="<?php echo $fe['tool_id'];?>" data-tid="<?php echo $fe['tool_id'];?>" class="fe_tool_arr"><?php
									}
								} ?>
								<div class="row">
									<div class="col-sm-9">
										<h5 style="margin-left: 20px;">Transaction Country : <?php $country = $this->Common_model->get_value('location',array('location_id'=>$_SESSION['transactionCountry']),'name');
										echo "<strong>".$country ."</strong>";
										 ?></h5>
									</div><?php
									if($flg == 1)
									{ ?>
										<div class="col-sm-3" align="right">
											<a href="<?php echo SITE_URL.'raise_order/0'?>" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Add More to cart</a>
										</div><?php 
									}
									else 
									{  ?>
										<div class="col-sm-3" align="right">
											<a href="<?php echo SITE_URL.'raise_order/'.storm_encode($order_info['tool_order_id']).'/0';?>" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Add More to cart</a>
										</div><?php 
									} ?>
								</div>
									
								<div class="table-responsive" style="margin-top: 10px;">
									<table class="table table-bordered hover" id="mytable">
										<thead>
											<tr>
												<th class="text-center"><strong>SNO</strong></th>
												<th class="text-center"><strong>Tool Number</strong></th>
												<th class="text-center"><strong>Tool Description</strong></th>								
												<th class="text-center"><strong>Qty</strong></th>
												<th class="text-center"><strong>Action</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php $i= 1;
											if(count(@$tools) >0)
											{
												foreach($tools as $row)
												{
													
													?>
													<tr class="toolSelectedRow">
														<td class="text-center"><?php echo $i++;?></td>									
														<td class="text-center"><?php echo @$row['part_number'];?></td>
														<td class="text-center"><?php echo @$row['part_description'];?></td>												
														<td class="text-center">

														<input  type="number" autocomplete="off" name="quantity[<?php echo @$row['tool_id'];?>]" required min= "1" 
															class="only-numbers text-center toolQuantity" value="<?php echo @$_SESSION['tool_id'][$row['tool_id']]; ?>" >
															</td>			
														<input type="hidden" class="toolIdCls" value="<?php echo @$row['tool_id'];?>">							


													
															
														<td class="text-center"><a class="btn btn-danger btn-xs removeRow fe_tools" 
															data-cid="<?php echo $row['tool_id']; ?>" data-orderid="<?php echo @$row['tool_order_id']; ?>"
															data-availableqty="<?php echo @$row['available_quantity']; ?>"
															data-quantity="<?php echo @$row['quantity']; ?>" 
															data-orderedtool="<?php echo @$row['ordered_tool_id']; ?>"
															data-currentstage="<?php echo @$order_info['current_stage_id']; ?>"   href="#"  >
														<i class="fa fa-times"></i></a></td> <!--<?php if(count($_SESSION['tool_id']) == 1 || count($tools) == 1){?> style='display:none'; <?php } ?>-->
													</tr>

											<?php	} 
											}
											else {
											?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
											<?php } ?>								
										</tbody>
									</table>
								</div><br>

								<!-- checking Onbehalf Fe Role -->
								<?php 									
									if($conditionRole == 5 || $conditionRole == 6 || $conditionAccess == 2 || $conditionAccess ==3)
										$new_wh_div_flg = 1;						
								?>
								<div class="row">
									<div class="form-group new_wh_div <?php if(!isset($new_wh_div_flg)){ echo "hidden"; }?>">
										<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Ship From Wh <span class="req-fld">*</span></label>
				                           	<div class="col-sm-4">
				                           		<select name="wh_id" class="select2"> 
												<?php 
												foreach($ship_by_wh_data as $st)
												{
													if($flg == 1)
													{
														if(checkAsean($_SESSION['transactionCountry'],1))
															$defaultWhId = getAseanWarehouse($_SESSION['transactionCountry'],1);
														else
															$defaultWhId = $this->Common_model->get_value('user',array('sso_id'=>$_SESSION['transactionUser']),'wh_id');
													}
													else
													{
														$defaultWhId = $order_info['wh_id'];
													}
													$selected = ($st['wh_id']==$defaultWhId)?'selected="selected"':'';
													echo '<option value="'.$st['wh_id'].'" '.$selected.'>'.$st['wh_code'].'- ( '.$st['name'].' )'.'</option>';
												}?>
												</select>
				                            </div>                               
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Select Type <span class="req-fld">*</span></label>
				                           	<div class="col-sm-4 custom_icheck">
				                           		<label class="radio-inline"> 
				                                    <div class="iradio_square-blue <?php if(@$order_info['fe_check']==0 || $flg == 1) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                        <input type="radio" class="fe_check" value="0" name="fe_check" <?php if(@$order_info['fe_check']==0 || $flg == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
				                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                    </div> 
				                                    Request From WH
				                                </label>
				                                <label class="radio-inline"> 
				                                    <div class="iradio_square-blue <?php if(@$order_info['fe_check']==1 ) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                        <input type="radio" class="fe_check" value="1" name="fe_check" <?php if(@$order_info['fe_check']==1 ) { echo "checked"; }?> style="position: absolute; opacity: 0;">
				                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                    </div> 
				                                    Transfer From FE
				                                </label>
				                            </div>
				                            <div class="fe1_order_number_div <?php if(@$order_info['fe_check']== 0 || $flg ==1) { echo "hidden";}?>">
					                            <label for="inputName" class="col-sm-2 control-label">FE1 Order Number <span class="req-fld">*</span></label>
					                             <div class="col-sm-4">
													<input class="form-control"  size="16" type="text" autocomplete="off"  value="<?php echo @$order_info['fe1_order_number']; ?>"  placeholder="FE1 Order Number" name="fe1_order_number" id="fe1_order_number">
													<span>
													<!-- <a target="_blank" href="<?php echo SITE_URL;?>toolsAvailabilityWithFE"></i> Check Tool Availability with FE</a> -->
													<a class="md-trigger check_with_sso" data-toggle="modal" data-target="#form-primary" href="#" >Check With SSO</a>
													</span>
												</div>
											</div>                                
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Service Type <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<select name="service_type_id" required class="select2 service_type_id" > 
												<option value="">Service Type</option>
												<?php 
													foreach($service_type as $st)
													{
														$selected = ($st['service_type_id']==@$order_info['service_type_id'])?'selected="selected"':'';
														echo '<option value="'.$st['service_type_id'].'" '.$selected.'>'.$st['name'].'</option>';
													}
												?>
											</select>
											</div>
											<div class=" service_type_remarks_div <?php if(@$order_info['service_type_id']!=5 || @$flg ==1) { echo "hidden";}?>">
												<label for="inputName" class="col-sm-2 control-label"> Others Remarks <span class="req-fld">*</span></label>
													<div class="col-sm-4">
														<textarea name="service_type_remarks" class="form-control service_type_remarks_value"><?php echo @$order_info['service_type_remarks']; ?></textarea>
													</div>
											</div> 
										</div>
									</div>
									<div class="form-group site_readiness_div <?php if(@$order_info['service_type_id']!=1 || $flg ==1) { echo "hidden";}?>">
										<div class="col-md-12 custom-icheck">
											<label for="inputName" class="col-sm-2 control-label">Site Readiness <span class="req-fld">*</span></label>
				                           	<div class="col-sm-4 custom_icheck">
				                                <label class="radio-inline"> 
				                                    <div class="iradio_square-blue <?php if(@$order_info['site_readiness']==1 || $flg == 1) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                        <input type="radio" class="site_readiness" value="1" name="site_readiness" <?php if(@$order_info['site_readiness']==1 || $flg == 1) { echo "checked"; }?> style="position: absolute; opacity: 0;">
				                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                    </div> 
				                                    Yes
				                                </label>
				                                <label class="radio-inline"> 
				                                    <div class="iradio_square-blue <?php if(@$order_info['site_readiness']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                        <input type="radio" class="site_readiness" value="2" name="site_readiness" <?php if(@$order_info['site_readiness']==2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
				                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                    </div> 
				                                    No
				                                </label>
				                            </div>
				                            <div class="site_remarks <?php if(@$order_info['site_readiness']==1 || $flg ==1) { echo "hidden";}?> ">
				                            	<label for="inputName" class="col-sm-2 control-label">  Remarks <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea name="site_remarks" class="form-control site_remarks_value"><?php echo @$order_info['site_remarks']; ?></textarea>
												</div>
				                            </div>                               
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 ">	
											<?php 
											$country_name_sr = $this->Common_model->get_value('location',array('location_id'=>@$_SESSION['transactionCountry']),'name');
											$sr_display = 'Siebel SR Number';
											//echo $country_name_sr;exit;
											if($country_name_sr == 'Egypt' || $country_name_sr =='Algeria' || $country_name_sr == 'Turkey' || $country_name_sr == 'Saudi Arabia')
											{
												$sr_display = 'Job number';
											} ?>									
				                            <label for="inputName" class="col-sm-2 control-label"><?php echo @$sr_display;?> <span class="req-fld">*</span></label>
				                            <div class="col-sm-4">
												<input class="form-control" required size="16" type="text" autocomplete="off"  value="<?php echo @$order_info['siebel_number']; ?>"  placeholder="<?php echo @$sr_display;?>" name="srn_number" id="">
											</div> 
											<label for="inputName" class="col-sm-2 control-label">Request Date <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<input class="form-control" required size="16" type="text" autocomplete="off"  id ="deploy_date" value="<?php  if(@$flg == 2){  echo indian_format(@$order_info['deploy_date']); }else { echo date('d-m-Y'); }  ?>" readonly placeholder="Date" name="deploy_date">
												
											</div>                              
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Need Date <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<input class="form-control need_date" required size="16" type="text" autocomplete="off"  value="<?php if(@$flg ==2) echo indian_format(@$order_info['request_date']); ?>" readonly placeholder="Date" name="request_date" id="dateFrom" style="cursor:hand;background-color: #ffffff">
											</div>

											<label for="inputName" class="col-sm-2 control-label">Return Date <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<input class="form-control" required size="16" type="text" autocomplete="off"  value="<?php if(@$flg == 2) echo indian_format(@$order_info['return_date']); ?>" readonly placeholder="Date" name="return_date" id="dateTo" style="cursor:hand;background-color: #ffffff">
											</div>
										</div>
									</div>
									<?php 
										if(getTaskPreference("tool_order_notes",$_SESSION['transactionCountry'])) { ?>
										<div class="form-group ">
											<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Notes <span class="req-fld"></span> </label>
												<div class="col-sm-4">
													<input type="text" autocomplete="off" class="form-control" value="<?php echo @$order_info['notes'];?>"  name="notes" >
												</div>
											</div>
										</div>
									<?php } ?>
									<div class="form-group">
										<div class="col-md-12 custom-icheck">
											<label for="inputName" class="col-sm-2 control-label">Delivery Point <span class="req-fld">*</span></label>
				                           	<div class="col-sm-5 custom_icheck">
				                                <label class="radio-inline"> 
				                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==1 || $flg == 1) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                        <input type="radio" class="delivery_type" value="1" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==1 || $flg == 1) { echo "checked"; }?> style="position: absolute; opacity: 0;">
				                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                    </div> 
				                                    Customer
				                                </label>
				                                <label class="radio-inline"> 
				                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                        <input type="radio" class="delivery_type" value="2" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
				                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                    </div> 
				                                    Pickup From WH
				                                </label>
				                                <label class="radio-inline"> 
				                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==3 ) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                        <input type="radio" class="delivery_type" value="3" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==3 ) { echo "checked"; }?> style="position: absolute; opacity: 0;">
				                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                    </div> 
				                                    Others
				                                </label>
				                            </div>
				                            
					                                                    
										</div>
									</div>
									
									<div class="customerRow <?php if(@$order_info['order_delivery_type_id']==2 || @$order_info['order_delivery_type_id'] == 3){ echo "hidden";}?>">
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-2 control-label">System ID <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<?php 
													$region_id = $this->Common_model->get_value('location',array('location_id'=>@$_SESSION['transactionCountry']),'region_id');
													$asean_region_id = get_asean_region_id();
													$display_name = 'System ID';
													if($region_id == $asean_region_id)
													{
														$display_name = 'System ID (DI & US) / Serial Number (LCS)';
													} ?>
													<input type="text" autocomplete="off"  class="form-control" id="system_id" placeholder="<?php echo @$display_name; ?>" name="system_id" value="<?php if(@$order_info['order_delivery_type_id'] == 1){ echo @$order_info['system_id'];} ?>">
													<input type="hidden" name="install_base_id" id="install_base_id" value="<?php 
													if(@$order_info['order_delivery_type_id'] == 1){ echo @$order_info['install_base_id'];} ?>">
												</div>
												<label for="inputName" class="col-sm-2 control-label">Site ID <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<input type="text" autocomplete="off"  class="form-control" id="site_id"  readonly placeholder="Site ID" name="site_id" value="<?php if(@$order_info['order_delivery_type_id'] == 1){ echo @$order_info['site_id']; } ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12 custom-icheck">
												<label for="inputName" class="col-sm-2 control-label">Change Address <span class="req-fld">*</span></label>
						                       	<div class="col-sm-4 custom_icheck">
						                            <label class="radio-inline"> 
						                                <div class="iradio_square-blue <?php if(@$order_info['check_address']==1 ) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
						                                    <input type="radio" class="change_address" value="1" name="check_address" <?php if(@$order_info['check_address']==1 ) { echo "checked"; }?> style="position: absolute; opacity: 0;">
						                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
						                                </div> 
						                                Yes
						                            </label>
						                            <label class="radio-inline"> 
						                                <div class="iradio_square-blue <?php if(@$order_info['check_address']==0 || $flg == 1 ) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
						                                    <input type="radio" class="change_address" value="0" name="check_address" <?php if(@$order_info['check_address']== 0 || $flg == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
						                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
						                                </div> 
						                                No
						                            </label>
						                        </div> 
						                        <div class="address_check <?php if(@$order_info['check_address']==1){ echo ""; }else{ echo "hidden"; }?>">
						                        	<label for="inputName" class="col-sm-2 control-label"> Address Remarks <span class="req-fld">*</span> </label>
													<div class="col-sm-4">
														<textarea name="address_remarks" class="form-control address_value"><?php echo @$order_info['remarks']; ?></textarea>
													</div>
						                        </div>
						                                                      
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Customer Name <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<input type="text" autocomplete="off" class="form-control"  <?php if(@$order_info['check_address'] == 0){ echo 'disabled';}?> value="<?php if (@$order_info['order_delivery_type_id'] ==1){echo @$c_name;}?>" id="c_name" name="c_name" >
												</div>
												<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control"  id="ct_address_1" name="ct_address_1" <?php if(@$order_info['check_address'] == 0){ echo 'disabled';}?> ><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address1'];} ?>
													</textarea>
												</div>

												
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Address2  <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control"  id="ct_address_2" name="ct_address_2" <?php if(@$order_info['check_address'] == 0){ echo 'disabled';}?>><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address2'];} ?>
													</textarea>
												</div>
												<label for="inputName" class="col-sm-2 control-label">City <span class="req-fld">*</span> </label>
												<div class="col-sm-4">
													<textarea class="form-control"  id="ct_address_3" name="ct_address_3" <?php if(@$order_info['check_address'] == 0){ echo 'disabled';}?> ><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address3'];} ?>
													</textarea>
												</div>

												
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">State <span class="req-fld">*</span> </label>
												<div class="col-sm-4">
													<textarea class="form-control"  id="ct_address_4"  name="ct_address_4" <?php if(@$order_info['check_address'] == 0){ echo 'disabled';}?> ><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address4'];} ?>
													</textarea>
												</div>
												<label for="inputName" class="col-sm-2 control-label">Pin Code <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<input type="number" autocomplete="off" class="form-control" minlength="3" maxlength="6" <?php if(@$order_info['check_address'] == 0){ echo 'disabled';}?> value="<?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['pin_code'];}?>" id="ct_pin_code" name="ct_pin_code" >
												</div>
												
											</div>
										</div>										
										<div class="row">
			                                <div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
			                                    <a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a>
			                                </div>
											<div class="col-md-12">
											    <div class="table-responsive col-md-offset-2 col-md-8  tab_hide">
											        <table class="table hover document_table">
											            <thead>
											                <tr>
											                    <th class="text-center" width="8%"><strong>Sno</strong></th>
											                    <th class="text-center" width="40%" ><strong>Document Type</strong></th>
											                    <th class="text-center" width="40%" ><strong>Supported Document</strong></th>
											                    <th class="text-center" width="8%" ><strong>Delete</strong></th>
											                </tr>
											            </thead>
											            <tbody>
											            <?php $count = 1;
											                if(count(@$docs)>0)
											                {   
											                    foreach($docs as $doc)
											                    {   ?> 
											                        <tr class="attach">
											                        	<input type="hidden" class="asset_doc_id" value="<?php echo $doc['order_doc_id']; ?>">
											                            <td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
											                            <td class="text-center">
											                                <?php 
											                                    foreach($documenttypeDetails as $docs)
											                                    {
											                                        if($docs['document_type_id']==@$doc['document_type_id'])
											                                        {
											                                            echo $docs['name'].' - ('.$doc['created_time'].')'; 
											                                        }
											                                    }
											                                ?>
											                            </td>
											                            <td>
											                                <a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo SITE_URL.''.$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
											                            </td>
											                            <td>
											                            <?php if($doc['status']==1)
											                            { ?>
											                                <a class="btn link btn-danger btn-sm deactivate" > <i class="fa fa-trash-o"></i></a>
											                            
											                            <?php } 
											                            else
											                                {?>
											                                <a class="btn link btn-info btn-sm activate" > <i class="fa fa-check"></i></a>
											                            <?php } ?>
											                            </td>
											                        </tr>
											                     <?php      
											                    } 
											                } ?>            
											                <tr class="doc_row">
											                    <td class="text-center">
											                        <span class="sno"><?php echo $count; ?></span>
											                    </td>
											                    <td>
											                        <select name="document_type[1]" class="form-control doc_type" > 
											                            <option value="">- Select Document Type -</option>
											                            <?php 
											                                foreach($documenttypeDetails as $doc)
											                                {
											                                    echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
											                                }
											                            ?>
											                        </select>
											                    </td>
											                    <td>
											                        <input type="file" id="document" name="support_document_1" class="document">
											                    </td>
											                    
											                    <td><a <?php if(@$flg == 2){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
											                </tr>
											            </tbody>
											        </table>
											    </div>
											</div>
			                            </div>
									</div> <!-- end of customer div-->
									<div class="warehouseRow <?php if(@$order_info['order_delivery_type_id']== 1 || @$order_info['order_delivery_type_id']==3 || $flg==1){ echo "hidden";};?>" > <!-- Start of warehouse Div-->
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-2 control-label">Warehouse <span class="req-fld">*</span></label>
												<div class="col-sm-4">											   
													<select name="fe_to_wh_id" class="form-control wh_id_cls  " > 
													<option value="">Select Warehouse</option>
													<?php 
														foreach($wh_data as $st)
														{
															if(@$order_info['order_delivery_type_id'] == 2)
																$selectedWhId = @$order_info['to_wh_id'];
															else
																$selectedWhId = $selected_ordered_to_wh;
															$selected = ($st['wh_id']==$selectedWhId)?'selected="selected"':'';
															echo '<option value="'.$st['wh_id'].'" '.$selected.'>'.$st['wh_code'].'- ( '.$st['name'].' )'.'</option>';
														}
													?>
													</select>
														
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control"  id="wh_address_1" name="wh_address_1" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address1']; } else{ echo @$order_info['address1']; }  ?></textarea>
												</div>

												<label for="inputName" class="col-sm-2 control-label">Address2 <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control"  id="wh_address_2" name="wh_address_2" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address2']; }  else{ echo @$order_info['address2']; }  ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-2 control-label">City <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control"  id="wh_address_3" name="wh_address_3" readonly ><?php if($_SESSION['role_id'] == 2){  echo @$wh_data['address3']; } else{ echo @$order_info['address3']; }  ?></textarea>
												</div>

												<label for="inputName" class="col-sm-2 control-label">State <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control"  id="wh_address_4"  name="wh_address_4" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address4']; }  else{ echo @$order_info['address4']; }  ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-2 control-label">Pin Code <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<input type="number" autocomplete="off" class="form-control" minlength="3" maxlength="6" id="wh_pin_code"  name="wh_pin_code" readonly value="<?php if($_SESSION['role_id']==2){echo @$wh_data['pin_code']; }  else{ echo @$order_info['pin_code']; } ?>">
												</div>
											</div>
										</div>
									</div> <!-- end of warehouse -->
									<div class="othersRow <?php if(@$order_info['order_delivery_type_id'] == 1 || @$order_info['order_delivery_type_id'] == 2 || $flg ==1){ echo "hidden";} ?>" >
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control" id="address_1"  name="address_1" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address1'];}  ?></textarea>
												</div>

												<label for="inputName" class="col-sm-2 control-label">Address2 <span class="req-fld">*</span> </label>
												<div class="col-sm-4">
													<textarea class="form-control"   name="address_2" id="address_2" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address2'];}  ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-2 control-label">City <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control"  name="address_3" id ="address_3" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address3'];}  ?></textarea>
												</div>

												<label for="inputName" class="col-sm-2 control-label">State <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control"   name="address_4" id="address_4"><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address4'];} ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-2 control-label">Pin Code <span class="req-fld">*</span> </label>
												<div class="col-sm-4">
													<input type="text" autocomplete="off" class="form-control" minlength="3" maxlength="6"  id="pin_code" name="pin_code" value="<?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['pin_code'];}  ?>">
												</div>
											</div>
										</div>
									</div>
									
									
									<div class="form-group">
										<div class="col-sm-offset-5 col-sm-5">
											<button class="btn btn-success" type="submit" value="1"  name="submitorder" onclick="return confirm('Are you sure you want to Submit ?')" id="submitOrder"><i class="fa fa-check"></i> Submit</button>
											<a class="btn btn-danger" href="<?php echo $cancel_url;?>"><i class="fa fa-times"></i> Cancel</a>
										</div>
									</div>
								</div>
							</form>
					</div>	             	          		
				</div>
			</div>		
		</div>
	</div>
</div>

<!-- Modal popup -->
<div class="modal fade colored-header" id="form-primary">
	<div class="modal-dialog" style="width:85%">
		<div class="md-content">
			<div class="modal-header">
	        	<h3>Check tools availability with other FE</h3>
	    	</div>
	    	<div class="modal-body form">
	    		<form role="form" class="form-horizontal" id="fe_owned_tools_frm" method="post" action="#">
	    			<input type="hidden" name="current_offset" id="current_offset" value="">
	    			<input type="hidden" name="submit_button" id="submit_button" value="1">
	    		<div class="row">
                    <div class="col-sm-12 form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <select name="ser_sso_id" class="select2 sso_data" style="width:100%"><option value="">-SSO-</option>
                        </select>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" class="form-control">
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <div class="col-sm-offset-2 col-sm-3">
                            <input type="text" autocomplete="off" name="part_number" placeholder="Tool Number" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" autocomplete="off" name="part_description" placeholder="Tool Description" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <button type="button" name="fe_owned_search" id="fe_owned_search" value="1" class="btn btn-success"><i class="fa fa-search"></i> </button>
                            <button type="button" name="reset_search" id="reset_search" value="1" class="btn btn-success"><i class="fa fa-refresh"></i> </button>
                        </div>
                    </div>
                </div>
                </form>
                <div id="feSearchResults" align="center"><h4>Search FE Orders Here</h4></div>
	    	</div>
	    	<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-flat md-close" disabled="" id="confirm_fe1_order">Confirm</button>
				<button type="button" class="btn btn-info btn-flat md-close" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<div class="md-overlay"></div>
<div id="loaderID" style="position:fixed; top:50%; left:58%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>

<script type="text/javascript">		
		get_warehouse_address();
		get_warehouses_dropdown_by_sso();
</script>
	 
	

			
		
