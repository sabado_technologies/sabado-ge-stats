<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			 
		<?php
		if(isset($flg))
		{
         if($flg == 1)
            { ?>
            <div class="row"> 
                <div class="col-sm-12 col-md-12">
                    <div class="block-flat">
                        <div class="content">
                            <div class="col-sm-12 col-md-12">
                                <div class="row">                           
                                    <div class="header">
                                                <h5 align="center"><strong>Pickup Details</strong></h5>
                                            </div>
                                    <div class="col-md-6">
                                        <table class="no-border">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <td class="data-lable"><strong>Return Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['return_number'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong><u>From Address :</u></strong></td>
                                                    <td class="data-item"></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>SSO Detail :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['sso_id'].' - '.$rrow['user_name']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address1 :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address1']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address2 :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address2']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address3']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>State :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address4']?></td>
                                                </tr>
                                                <!-- <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['location_name']?></td>
                                                </tr> -->
                                                <tr>
                                                    <td class="data-lable"><strong>Pin Code :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['zip_code']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong><u>Courier Details :</u></strong></td>
                                                    <td class="data-item"></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Courier Name :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['courier_name'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Contact Person :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['contact_person'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Vehicle Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['vehicle_number'];?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="no-border">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <td class="data-lable"><strong>Expected Arrival Date :</strong></td>
                                                    <td class="data-item"><?php echo indian_format(@$rrow['expected_arrival_date']);?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong><u>To Address :</u></strong></td>
                                                    <td class="data-item"></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>C/o :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['to_name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address1 :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['address1']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address2 :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['address2']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['address3']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>State :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['address4']?></td>
                                                </tr>
                                                <!-- <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['location_name']?></td>
                                                </tr> -->
                                                <tr>
                                                    <td class="data-lable"><strong>Pin Code :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['pin_code']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable">.</td>
                                                    <td class="data-item"> </td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Courier Type :</strong></td>
                                                    <td class="data-item"><?php if(@$rrow['courier_type']==1)
                                                            {
                                                                echo "By Courier";
                                                            }
                                                            else if(@$rrow['courier_type']==2)
                                                            {
                                                                echo "By Hand";
                                                            }
                                                            else if(@$rrow['courier_type']==3)
                                                            {
                                                                echo "By Determined";
                                                            }
                                                            ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Docket/AWB number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['courier_number'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Phone Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['phone_number'];?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12"><br>
                                <div class="row">                           
                                    <div class="header">
                                        <h5 align="center"><strong>Acknowledged Tools</strong></h5>
                                    </div>
                                </div>
                                <div class="table-responsive" style="margin-top: 10px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="text-center"><strong>S.No</strong></th>
                                                <th class="text-center"><strong>Tool Number</strong></th>
                                                <th class="text-center"><strong>Tool Description</strong></th>
                                                <th class="text-center"><strong>Tool Level</strong></th>
                                                <th class="text-center"><strong>Asset Number</strong></th>
                                                <th class="text-center"><strong>Status</strong></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count(@$asset_list)>0)
                                            {   $sn = 1;
                                                foreach(@$asset_list as $row)
                                                { 
                                                ?>
                                                    <tr>
                                                        <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                                        <td class="text-center"><?php echo $sn++; ?></td>
                                                        <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                        <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                        <td class="text-center"><?php echo 'L0' ?></td>
                                                        <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                        <td class="text-center">
                                                        <?php if($asset_val[$row['asset_id']]['asset_status'] == 3)
                                                        {
                                                            echo "Not Received"; 
                                                        }
                                                        else
                                                        {
                                                            echo "Received";
                                                        }?>
                                                        </td>
                                                    </tr>
                                                <?php if(count(@$row['components'])>0)
                                                    {  ?>
                                                        <tr class="details">
                                                        <td colspan="7">
                                                        <table class="table">
                                                        	<thead>
					                                            <tr>
					                                                
					                                                <th class="text-center"><strong>Serial Number</strong></th>
					                                                <th class="text-center"><strong>Tool Number</strong></th>
					                                                <th class="text-center"><strong>Tool Description</strong></th>
					                                                <th class="text-center"><strong>Tool Level</strong></th>
					                                                <th class="text-center"><strong>Quantity</strong></th>
					                                                <th class="text-center"><strong>Tool Health</strong></th>
					                                                <th class="text-center"><strong>Remarks</strong></th>
					                                            </tr>
					                                        </thead>
                                                            
                                                        
                                                            <tbody>
                                                       <?php foreach(@$row['components'] as $value)
                                                        { 
                                                        ?>
                                                            <tr class="asset_row">
                                                                <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_level']; ?></td>
                                                                <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                                <td class="text-center">
                                                                    <?php if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 1)
                                                                    {
                                                                        echo "Good";
                                                                    }
                                                                    else if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 2)
                                                                    {
                                                                        echo "Defective";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "Missing";
                                                                    } ?>
                                                                </td>
                                                                <td class="text-center">
                                                                   <?php if($asset_val[$row['asset_id']][$value['part_id']]['remarks'] != '') { echo $asset_val[$row['asset_id']][$value['part_id']]['remarks']; }
                                                                    else{
                                                                        echo "No remarks";
                                                                        } ?>
                                                                </td>
                                                            </tr>
                                                        <?php     
                                                        } ?>

                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr><?php
                                                    }
                                                    else
                                                    { ?>
                                                        <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                    <?php 
                                                    }
                                                }
                                            
                                            } else {?>
                                                <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                    <?php   } ?>
                                        </tbody>
                                    </table>
                                </div><br>
                            </div> 
                            <div class="row">                           
                                <div class="header">
                                    <h5 align="center"><strong>Attached Documents</strong></h5>
                                </div>
                                <br>
                                <div class="table-responsive col-md-offset-1 col-md-10">
                                    <table class="table hover">
                                        <thead>
                                            <tr>
                                                <th>S No.</th>
                                                <th width="18%" class="text-center"><strong>Attached Date</strong></th>
                                                <th class="text-center"><strong>Document Type</strong></th>
                                                <th width="30%" class="text-center"><strong>Supported Document</strong></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            if(count(@$attach_document)>0)
                                            {   $count = 1;
                                                foreach($attach_document as $ad)
                                                {?> 
                                                    <tr>
                                                        <td><?php echo $count++; ?></td>
                                                        <td><?php echo $ad['created_time']; ?></td>
                                                        <td align="center">
                                                                <?php 
                                                                    foreach($documenttypeDetails as $doc)
                                                                    {
                                                                        if($doc['document_type_id']==@$ad['document_type_id'])
                                                                        {
                                                                            echo $doc['name']; 
                                                                        }
                                                                    }
                                                                ?>
                                                        </td>
                                                        <td>
                                                            <a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
                                                        </td>
                                                    </tr>
                                                 <?php      
                                                } 
                                            } 
                                            else {
                                                ?>  
                                                <tr><td colspan="4" align="center">No Attached Docs.</td></tr>
                                            <?php } ?>
                                        </tbody>
                                    </table><br>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-5">
                                        <a class="btn btn-primary" href="<?php echo SITE_URL;?>closed_order"><i class="fa fa-reply"></i> Back</a>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                    </div>              
                </div>
            </div>
	    <?php 
            }
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>closed_order">
						
						<div class="row">
							<div class="col-sm-12 form-group">
								<!-- <label class="col-sm-2 control-label">Order Type</label> -->
                                <div class="col-sm-3">
                                    <select name="order_delivery_type_id" class="select2" > 
                                        <option value="">- Order Type -</option>
                                        <?php 
                                            foreach($order_type as $ot)
                                            {
                                                $selected = ($ot['order_delivery_type_id']==@$searchParams['order_delivery_type_id'])?'selected="selected"':'';
                                                echo '<option value="'.$ot['order_delivery_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <!-- <label class="col-sm-2 control-label">Closed Status</label> -->
                                <div class="col-sm-3">
                                    <select name="status" class="select2" >                                     
                                        <?php 
                                            foreach(closed_order_status() as $sta)
                                            {
                                                $selected = ($sta['status']==@$searchParams['status'])?'selected="selected"':'';
                                                echo '<option value="'.$sta['status'].'" '.$selected.'>'.$sta['name'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <?php if($task_access==2 || $task_access==3)
                                    {
                                        ?>
                                    <div class="col-sm-3">
                                        <?php 
                                        $d['name']        = 'closed_fe_sso_id';
                                        $d['search_data'] = @$searchParams['closed_fe_sso_id'];
                                        $this->load->view('sso_dropdown_list',$d); 
                                        ?>
                                    </div>  
                                    <?php } ?> 
                                    <?php if( ($task_access == 3 && $_SESSION['header_country_id']=='') || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']=='') ){ ?>   
                                        <div class="col-sm-3">
                                            <select name="co_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
                                                <?php
                                                foreach($country_list as $row)
                                                {
                                                    $selected = ($row['location_id']==@$searchParams['co_country_id'])?'selected="selected"':'';
                                                    echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                                }?>
                                            </select>
                                        </div>

                                         <?php
                                    }?> 
                                      
								
							</div>	
						</div>
						<div class="row">
                            <div class="col-sm-12 form-group">
                                <!-- <label class="col-sm-2 control-label">Order Number</label> -->
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['order_number'];?>"  class="form-control">
                                </div>
                                <!-- <label class="col-sm-2 control-label">Request Date</label> -->
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="deploy_date" id="dateFrom" readonly placeholder="Request Date" value="<?php echo @$searchParams['deploy_date'];?>"  class="form-control" style="cursor:hand;background-color: #ffffff">
                                </div>
                                
                                <!-- <label class="col-sm-2 control-label">Return Date</label> -->
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="return_date" id="dateTo" readonly placeholder="Return Date To" value="<?php echo @$searchParams['return_date'];?>"  class="form-control" style="cursor:hand;background-color: #ffffff">
                                </div>     
                                <div class="col-sm-3">                         
                                    <button type="submit" name="closed_order" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
                                    <a href="<?php echo SITE_URL.'closed_order'; ?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>
                                    <button type="submit" name="downloadClosedOrders" onclick="return confirm('Are you sure you want to download?')" data-toggle="tooltip" title="Download" value="1" formaction="<?php echo SITE_URL.'downloadClosedOrders';?>" class="btn btn-success"><i class="fa fa-cloud-download"></i></button>  
                                </div>
                            </div>
                        </div>
					</form>
					<div class="header"></div>
				 	<div class="table-responsive" style="margin-top: 10px;">
				 		<?php if(@$searchParams['status'] == 10 ) {?> 
	                        <table class="table">
	                            <thead>
	                                <tr>
	                                    <th></th>
	                                    <th class="text-center" width="1%"><strong>S.No</strong></th>
	                                    <th class="text-center" width="5%"><strong>Order Number</strong></th>
	                                    <th class="text-center" width="5%"><strong>Order Type </strong></th>
                                        <th class="text-center" ><strong>SSO </strong></th>
	                                    <th class="text-center" width="9%"><strong>Request Date</strong></th>
	                                    <th class="text-center" width="9%"><strong>Return Date</strong></th>
                                        <th class="text-center" width="20%"><strong>Shipped Date</strong></th>
	                                    <th class="text-center" width="9%"><strong>Closed Date</strong></th>
                                        <th class="text-center" width="5%"><strong>Status</strong></th>
                                        <th class="text-center" width="9%"><strong>Country</strong></th>
                                        <th class="text-center" width="9%"><strong>Actions</strong></th>
	                                    
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <?php 
	                                if(count(@$orderResults)>0)
	                                {   
	                                    foreach(@$orderResults as $row)
	                                    { 
	                                    ?>
	                                        <tr>
	                                            <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand" ></td>
	                                            <td class="text-center"><?php echo $sn++; ?></td>
	                                            <td class="text-center"><?php echo $row['order_number']; ?></td>
	                                            <td class="text-center"><?php echo $row['order_type']; ?></td>
                                                <td class="text-center"><?php echo $row['sso']; ?></td>
	                                            <td class="text-center"><?php echo ($row['deploy_date']=='')?"NA":indian_format($row['deploy_date']); ?></td>
	                                            <td class="text-center"><?php echo ($row['return_date']=='')?"NA":indian_format($row['return_date']); ?></td>                                                
	                                            <td class="text-center"><?php 
                                                        echo getFinalShipDate($row);
                                                ?>    
                                                </td>
                                                <td class="text-center"><?php echo ($row['closed_date']=='')?"NA":indian_format($row['closed_date']); ?></td>
	                                            <td class="text-center"><?php echo ($row['final_status']==10)?'Closed':'Cancelled'; ?></td>
                                                <td class="text-center"><?php echo $row['countryName'];?></td>
	                                            <td class="text-center">
                                                    <a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(1);?>"><i class="fa fa-eye"></i></a>
                                                    <?php 
                                                        $data = 
                                                        array('primary_key' => storm_encode(0),
                                                              'table_name'  => '',
                                                              'work_flow'   => '',
                                                              'main_table'  => 'tool_order',
                                                              'main_key'    => $row['tool_order_id']);
                                                        $send_data = implode('~',$data); 
                                                    ?>
                                                    <a class="btn btn-primary" 
                                                        style="padding:3px 3px;"
                                                        href="<?php echo SITE_URL.'get_audit_page/'.$send_data;?>"
                                                        target="_blank"
                                                        data-toggle="tooltip" title="Audit Trail"><i class="fa fa-book"></i>
                                                    </a>
                                                </td>
	                                        </tr>
	                                        <?php if(count(@$final_results[$row['tool_order_id']])>0)
	                                        {  ?>
	                                        <tr class="details">
	                                            <td></td>
	                                                <td  colspan="11"> 
	                                                    <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
	                                                        <thead>
	                                                            <th class="text-center">Return Number</th>	                                                            
	                                                            <th class="text-center">Pickup Point</th>
	                                                            <th class="text-center">Received Date </th>
	                                                            <th class="text-center">Return Type</th>
	                                                            <th class="text-center">Action</th>
	                                                        </thead>
	                                                        <tbody>
	                                                   <?php foreach(@$final_results[$row['tool_order_id']] as $value)
	                                                    { 
	                                                    ?>
	                                                        <tr class="asset_row">
	                                                            <td class="text-center"><?php echo $value['return_number']; ?></td>	                                                            
	                                                            <td class="text-center"><?php echo $value['location_name']; ?></td>
	                                                            <td class="text-center"><?php echo indian_format($value['received_date']); ?></td>
	                                                            <td class="text-center"><?php echo $value['return_type_name']; ?></td>
	                                                           <td class="text-center"> <a class="btn btn-default" data-toggle="tooltip" title="View Closed FE Returns" style="padding:3px 3px;" href="<?php echo SITE_URL.'viewFeClosedReturnDetails/'.storm_encode($value['return_order_id']);?>"><i class="fa fa-eye"></i></a></td>
	                                                        </tr>
	                                                    <?php     
	                                                    } ?>
	                                                        </tbody>
	                                                    </table>
	                                            </td>
	                                        </tr><?php
	                                        }
	                                        else
	                                        { ?>
	                                            <tr class="details"><td colspan="11" align="center"><span class="label label-primary">No Retunrns Found</span></td></tr>
	                                        <?php 
	                                        }
	                                    }
	                                
	                                } else {?>
	                                    <tr><td colspan="12" align="center"><span class="label label-primary">No Records Found</span></td></tr>
	                        <?php   } ?>
	                            </tbody>
	                        </table>
	                     <?php } else { ?>
	                     <table class="table">
                            <thead>
                                <tr>                                    
                                    <th class="text-center"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>Order Number</strong></th>
                                    <th class="text-center"><strong>Order Type </strong></th>
                                    <th class="text-center"><strong>Request Date</strong></th>
                                    <th class="text-center"><strong>Need Date</strong></th>
                                    <th class="text-center"><strong>Return Date</strong></th>
                                    <th class="text-center"><strong>Cancelled Date</strong></th>
                                    <th class="text-center"><strong>Status</strong></th>
                                    <th class="text-center" width="10%"><strong>View</strong></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count(@$orderResults)>0)
                                {   $sn = 1;
                                    foreach(@$orderResults as $row)
                                    { 
                                    ?>
                                        <tr>
                                            
                                            <td class="text-center"><?php echo $sn++; ?></td>
                                            <td class="text-center"><?php echo $row['order_number']; ?></td>
                                            <td class="text-center"><?php echo $row['order_type']; ?></td>
                                            <td class="text-center"><?php echo indian_format($row['deploy_date']); ?></td>
                                            <td class="text-center"><?php echo indian_format($row['request_date']); ?></td>
                                            <td class="text-center"><?php echo indian_format($row['return_date']); ?></td>
                                            <td class="text-center"><?php echo indian_format($row['closed_date']); ?></td>
                                            <td class="text-center"><?php echo ($row['final_status']==10)?'Closed':'Cancelled'; ?></td>
                                            <td class="text-center">
                                                <a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(1);?>"><i class="fa fa-eye"></i></a>
                                                <?php 
                                                $data = 
                                                array('primary_key' => storm_encode(0),
                                                      'table_name'  => '',
                                                      'work_flow'   => '',
                                                      'main_table'  => 'tool_order',
                                                      'main_key'    => $row['tool_order_id']);
                                                $send_data = implode('~',$data); 
                                                ?>
                                                <a class="btn btn-primary" 
                                                    style="padding:3px 3px;"
                                                    href="<?php echo SITE_URL.'get_audit_page/'.$send_data;?>"
                                                    target="_blank"
                                                    data-toggle="tooltip" title="Audit Trail"><i class="fa fa-book"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php 
                                    }
                                
                                } else {?>
                                    <tr><td colspan="9" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                        <?php   } ?>
                            </tbody>
                        </table>

	                     <?php  } ?>

                    </div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
		<?php } ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	/***TABLE COLLAPSE ON 12-Nov-2015***/
	$('.details').hide();
	$(document).on('click',".toggle-details",function () { 
	  var row=$(this).closest('tr');
	  var next=row.next();
	  $('.details').not(next).hide();
	  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');

	  next.toggle();
	  if (next.is(':hidden')) {
	    $(this).attr('src',ASSET_URL+'images/plus.png');
	    $(this).attr('title','Expand');
	  } else {
	    $(this).attr('src',ASSET_URL+'images/minus.png');
	    $(this).attr('title','Collapse');
	  }
	});
</script>
