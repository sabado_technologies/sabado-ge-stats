<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>		
            <div class="block-flat">
                <div class="content">
                    <div class="col-sm-12 col-md-12">
                        <div class="row">                           
                            <div class="row" style="padding: 10px;">                            
                                <div class="header">
                                    <h4 align="center"><strong>Transaction Info</strong></h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                       <?php if($trow['order_type'] == 1){?>
                                        <tr>
                                            <td class="data-lable"><strong>STN Number:</strong></td>
                                            <td class="data-item"><?php echo @$trow['stn_number'];?></td>

                                        </tr>
                                        <?php } else { ?>
                                        <tr>
                                            <td class="data-lable"><strong>Order Number:</strong></td>
                                            <td class="data-item"><?php echo @$trow['order_number'];?></td>

                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td class="data-lable"><strong>Request Date:</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$trow['deploy_date']);?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address1:</strong></td>
                                            <td class="data-item"><?php echo @$trow['address1'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City:</strong></td>
                                            <td class="data-item"><?php echo @$trow['address3'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Pin Code:</strong></td>
                                            <td class="data-item"><?php echo @$trow['pin_code'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Courier Name:</strong></td>
                                            <td class="data-item"><?php echo @$drow['courier_name'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Contact Person:</strong></td>
                                            <td class="data-item"><?php echo @$drow['contact_person'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Expected Delivery Date:</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$drow['expected_delivery_date']);?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>SSO Detail:</strong></td>
                                            <td class="data-item"><?php echo @$trow['sso']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Return Date:</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$trow['return_date'])?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address2:</strong></td>
                                            <td class="data-item"><?php echo @$trow['address2'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>State:</strong></td>
                                            <td class="data-item"><?php echo @$trow['address4'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Courier Type:</strong></td>
                                            <td class="data-item"><?php if(@$drow['courier_type']==1)
                                                    {
                                                        echo "By Courier";
                                                    }
                                                    else if(@$drow['courier_type']==2)
                                                    {
                                                        echo "By Hand";
                                                    }
                                                    else if(@$drow['courier_type']==3)
                                                    {
                                                        echo "By Dedicated";
                                                    }
                                                    ?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Docket/AWB number:</strong></td>
                                            <td class="data-item"><?php echo @$drow['courier_number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Phone Number:</strong></td>
                                            <td class="data-item"><?php echo @$drow['phone_number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Remarks:</strong></td>
                                            <td class="data-item"><?php echo @$drow['remarks'];?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div><br>
                    </div>

                    
                </div>
            </div>
            <div class="block-flat">
                <div class="content">
                    <form method="post" action="<?php echo SITE_URL.'insert_st_fe_received_order';?>" parsley-validate novalidate class="form-horizontal" enctype="multipart/form-data">                    
                        <div class="col-sm-12 col-md-12"><br>
                            <div class="row">                           
                                <div class="header">
                                    <h5 align="center"><strong>Acknowledge Requested Tools</strong></h5>
                                </div>
                            </div>
                            <div class="table-responsive" style="margin-top: 10px;">
                                
                                <table class="table">
                                    <thead>
                                        <th class="text-center"> Asset Number </th>
                                        <th class="text-center">Part Number</th>
                                        <th class="text-center">Part Description</th>
                                        <th class="text-center">Serial Number</th>
                                        <th class="text-center">Part Level </th>                                        
                                    </thead>
                                    <tbody>
                                   <?php 
                                    if(count(@$asset_data)){

                                   foreach(@$asset_data as $value)
                                    { 
                                    ?>
                                        <tr class="asset_row">
                                            <td class="text-center"><?php echo $value['asset_number']; ?></td>
                                            <td class="text-center"><?php echo $value['part_number']; ?></td>
                                            <td class="text-center"><?php echo $value['part_description']; ?></td>
                                            <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                            <td class="text-center"><?php echo $value['part_level']; ?></td>
                                            
                                            
                                        </tr>
                                    <?php     
                                    } 
                                    } else {?>
                                    <tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                        <?php   } ?>
                                        </tbody>
                                </table>
                                             
                             </div>
                        </div>     
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-6"><br>  
                                <?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>   
                                <a class="btn btn-primary" href="<?php echo SITE_URL.'closed_receive_order'.$current_offset;?>"><i class="fa fa-reply"></i> Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>	    
        </div>
    </div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>