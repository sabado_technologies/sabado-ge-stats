<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:50%; z-index:2; opacity:0"><img src="<?php echo SITE_URL1;?>assets/images/ajax-loading-img.gif" /></div>
	<div class="cl-mcont">
	    <div class="row"> 
	        <div class="col-sm-12 col-md-12">
			    <?php echo $this->session->flashdata('response'); ?>			
				<div class="block-flat">
					<div class="content">
						<form role="form" class="form-horizontal" parsley-validate novalidate method="post" action="<?php echo SITE_URL;?>insert_fe_owned_order">
							<input type="hidden" value="<?php echo @$owned_assets[0]['order_status_id'];?>" name="order_status_id">
							<input type="hidden" value="<?php echo @$tool_order_id;?>" name="tool_order_id">
							 <input type="hidden" value="<?php echo $fe1_order_number;?>" name="fe1_order_number">
							<input type="hidden" value="<?php echo $_SESSION['role_id'];?>" class="role_id">
							<input type="hidden" value="<?php echo count(@$owned_assets);?>" name="owned_assets_count">

							<div class="table-responsive" style="margin-top: 10px;">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        	<th><input type="checkbox" id="checkAll" name="checkAll"></th>
                                            <th></th>
                                            <th class="text-center"><strong>S.No</strong></th>
                                            <th class="text-center"><strong>Tool Number</strong></th>
                                            <th class="text-center"><strong>Tool Description</strong></th>
                                            <th class="text-center"><strong>Asset Number</strong></th>
                                            <th class="text-center"><strong>Return Status</strong></th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if(count(@$return_parts)>0)
                                        {   $sn = 1;
                                            foreach(@$return_parts as $oah_id => $row)
                                            { 
                                            ?>
                                                <tr>
                                                	<td><input type="checkbox" class="chkAll openOrder " name="oah_id[<?php echo $row['oah_id'];?>]" value="<?php echo $row['ordered_asset_id'];?>"></td>
                                                    <input type="hidden" name="assetAndOrderedAsset[<?php echo $row['asset_id'];?>]" value="<?php echo $row['ordered_asset_id'];?>" >   
                                                    
                                                    <td class="center "><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand"></td>
                                                    <td class="text-center"><?php echo $sn++; ?></td>
                                                    <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                    <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                    <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                    <td class="text-center ">                                                                
                                                        <div class="col-sm-12 custom_icheck">
                                                        <label class="radio-inline"> 
                                                            <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                <input type="radio" class="asset_status" value="1" name="oah_condition_id[<?php echo $row['ordered_asset_id']; ?>]" checked style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                            </div> 
                                                            Return 
                                                        </label>
                                                        <label class="radio-inline"> 
                                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                <input type="radio" class="asset_status" value="2" name="oah_condition_id[<?php echo $row['ordered_asset_id']; ?>]" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                            </div> 
                                                            Missed
                                                        </label>
                                                        
                                                        </div>
                                                    </td>
                                                    
                                                </tr>
                                            <?php if(count(@$row['health_data'])>0)
                                                {  ?>
                                                    <tr class="details">
                                                        <td  colspan="7">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center"><strong>Serial Number</strong></th>                                                                
                                                                <th class="text-center"><strong>Tool Number</strong></th>                                                                
                                                                <th class="text-center"><strong>Tool Description</strong></th>
                                                                <th class="text-center"><strong>Tool Level</strong></th>
                                                                <th class="text-center"><strong>Quantity</strong></th>
                                                                <th class="text-center"><strong>Tool Health</strong></th>
                                                                <th class="text-center"><strong>Remarks</strong></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                   <?php foreach(@$row['health_data'] as $value)
                                                    {  ?>
                                                        <tr class="asset_row">
                                                            <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_level_name']; ?></td>
                                                            <td class="text-center"><?php echo $value['quantity']; ?></td>                                                                    
                                                            <input type="hidden" name="oah_oa_health_id_part_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][]" value="<?php echo $value['part_id'];?>"  >                       
                                                            <td width="32%" class="">
                                                                <div class="custom_icheck">
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue <?php if($value['asset_condition_id'] == 1) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_condition_id" value="1" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id'] == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Good
                                                                </label>
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue <?php if(@$value['asset_condition_id'] == 2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_condition_id" value="2" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id']== 2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Defective
                                                                </label>
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue <?php if(@$value['asset_condition_id'] == 3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_condition_id" value="3" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id'] == 3) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Missing
                                                                </label>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="textbox">
                                                                    <textarea class="form-control textarea" name="remarks[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]"><?php if($value['remarks'] != '') { echo $value['remarks']; } ?></textarea>  
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php     
                                                    } ?> 

                                                        </tbody>
                                                    </table>
                                                    </td>
                                                </tr><?php
                                                }
                                                else
                                                { ?>
                                                    <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                <?php 
                                                }
                                            }
                                        
                                        } else { ?>
                                            <tr><td colspan="7" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                <?php   } ?>
                                    </tbody>
                                </table>
                            </div><br>
							
							<div class="row">
								<h4> &nbsp;&nbsp;&nbsp;&nbsp;<b> Return From</b></h4>
								
							</div>
							<div class="form-group">
								<div class="col-md-12 custom-icheck">
									<label for="inputName" class="col-sm-2 control-label">Pickup Point <span class="req-fld">*</span></label>
		                           	<div class="col-sm-5 custom_icheck">
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==1 || $flg == 1) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio"  class="delivery_type" value="1" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==1 || $flg == 1) { echo "checked"; }?> style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    Customer
		                                </label>
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" class="delivery_type" value="2" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    Warehouse
		                                </label>
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==3 ) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" class="delivery_type" value="3" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==3 ) { echo "checked"; }?> style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    Others
		                                </label>
		                            </div>
		                            <div class="zonal_wh_row <?php if($_SESSION['role_id']==2 ){ echo "hidden";}?>">
		                            	<label for="inputName" class="col-sm-1 control-label">Shipby <span class="req-fld">*</span></label>
											<div class="col-sm-4">
											   
													<select name="zonal_wh_id" class="form-control zonal_wh_id " > 
													<option value="">Select Warehouse</option>
													<?php 
														foreach($wh_data as $st)
														{
															$selected = ($st['wh_id']==@$wh_id)?'selected="selected"':'';
															echo '<option value="'.$st['wh_id'].'" '.$selected.'>'.$st['wh_code'].'-('. $st['name'].' )'.'</option>';
														}
													?>
													</select>
													
										</div>
		                            </div>
			                            
			                                                    
								</div>
							</div>

							<div class="customerRow <?php if(@$order_info['order_delivery_type_id']==2 || @$order_info['order_delivery_type_id'] == 3){ echo "hidden";}?>">
								<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">System ID <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<input type="text" autocomplete="off"  class="form-control" id="system_id" placeholder="System ID" name="system_id" value="<?php if(@$order_info['order_delivery_type_id'] == 1){ echo @$order_info['system_id'];} ?>">
										</div>
										<label for="inputName" class="col-sm-2 control-label">Site ID <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<input type="text" autocomplete="off"  class="form-control" id="site_id"  placeholder="Site ID" name="site_id" value="<?php if(@$order_info['order_delivery_type_id'] == 1){ echo @$order_info['site_id']; } ?>">
										</div>

										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12 custom-icheck">
										<label for="inputName" class="col-sm-2 control-label">Change Address <span class="req-fld">*</span></label>
				                       	<div class="col-sm-4 custom_icheck">
				                            <label class="radio-inline"> 
				                                <div class="iradio_square-blue <?php if(@$order_info['check_address']==1 ) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                    <input type="radio" class="change_address" value="1" name="check_address" <?php if(@$order_info['check_address']==1 ) { echo "checked"; }?> style="position: absolute; opacity: 0;">
				                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                </div> 
				                                Yes
				                            </label>
				                            <label class="radio-inline"> 
				                                <div class="iradio_square-blue <?php if(@$order_info['check_address']==0 || $flg == 1 ) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
				                                    <input type="radio" class="change_address" value="0" name="check_address" <?php if(@$order_info['check_address']== 0 || $flg == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
				                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
				                                </div> 
				                                No
				                            </label>
				                        </div> 
				                        <div class="address_check hidden">
				                        	<label for="inputName" class="col-sm-2 control-label"> Address Remarks <span class="req-fld">*</span>  </label>
											<div class="col-sm-4">
												<textarea name="address_remarks" class="form-control address_value"><?php echo @$order_info['remarks']; ?></textarea>
											</div>
				                        </div>
				                                                      
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-12">
                                        <label for="inputName" class="col-sm-2 control-label">Customer Name <span class="req-fld">*</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" autocomplete="off" class="form-control"  disabled value="<?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['pin_code'];}?>" id="c_name" name="c_name" >
                                        </div>
										<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<textarea class="form-control"  id="ct_address_1" name="ct_address_1" disabled><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address1'];} ?>
											</textarea>
										</div>

										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
                                        <label for="inputName" class="col-sm-2 control-label">Address2 <span class="req-fld">*</span></label>
                                        <div class="col-sm-4">
                                            <textarea class="form-control"  id="ct_address_2" name="ct_address_2" disabled><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address2'];} ?>
                                            </textarea>
                                        </div>
										<label for="inputName" class="col-sm-2 control-label">City <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<textarea class="form-control"  id="ct_address_3" name="ct_address_3" disabled><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address3'];} ?>
											</textarea>
										</div>

										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
                                        <label for="inputName" class="col-sm-2 control-label">State <span class="req-fld">*</span> </label>
                                        <div class="col-sm-4">
                                            <textarea class="form-control"  id="ct_address_4"  name="ct_address_4" disabled><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address4'];} ?>
                                            </textarea>
                                        </div>
										<label for="inputName" class="col-sm-2 control-label">Pin Code <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<input type="text" autocomplete="off" class="form-control" minlength="3" maxlength="6" disabled value="<?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['pin_code'];}?>" id="ct_pin_code" name="ct_pin_code" >
										</div>
									</div>
								</div>
								
							</div> <!-- end of customer div-->
							<div class="warehouseRow <?php if(@$order_info['order_delivery_type_id']== 1 || @$order_info['order_delivery_type_id']==3 || $flg==1){ echo "hidden";};?>" > <!-- Start of warehouse Div-->
								<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">Warehouse <span class="req-fld">*</span></label>
										<div class="col-sm-4">
										   
												<select name="from_wh_id" class="form-control wh_id_cls  " > 
												<option value="">Select Warehouse</option>
												<?php 
													foreach($wh_data as $st)
													{
														$selected = ($st['wh_id']==@$wh_id)?'selected="selected"':'';
														echo '<option value="'.$st['wh_id'].'" '.$selected.'>'.$st['wh_code'].'-('. $st['name'].' )'.'</option>';
													}
												?>
												</select>
												
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<textarea class="form-control"  id="wh_address_1" name="wh_address_1" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address1']; } else{ echo @$order_info['address1']; }  ?></textarea>
										</div>

										<label for="inputName" class="col-sm-2 control-label">Address2 <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<textarea class="form-control"  id="wh_address_2" name="wh_address_2" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address2']; }  else{ echo @$order_info['address2']; }  ?></textarea>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">City <span class="req-fld">*</span> </label>
										<div class="col-sm-4">
											<textarea class="form-control"  id="wh_address_3" name="wh_address_3" readonly ><?php if($_SESSION['role_id'] == 2){  echo @$wh_data['address3']; } else{ echo @$order_info['address3']; }  ?></textarea>
										</div>

										<label for="inputName" class="col-sm-2 control-label">State <span class="req-fld">*</span> </label>
										<div class="col-sm-4">
											<textarea class="form-control"  id="wh_address_4"  name="wh_address_4" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address4']; }  else{ echo @$order_info['address4']; }  ?></textarea>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">Pin Code <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<input type="text" autocomplete="off" class="form-control"  id="wh_pin_code" minlength="3" maxlength="6" name="wh_pin_code" readonly value="<?php if($_SESSION['role_id']==2){echo @$wh_data['pin_code']; }  else{ echo @$order_info['pin_code']; } ?>">
										</div>
									</div>
								</div>
							</div> <!-- end of warehouse -->
							<div class="othersRow <?php if(@$order_info['order_delivery_type_id'] == 1 || @$order_info['order_delivery_type_id'] == 2 || $flg ==1){ echo "hidden";} ?>" >
								<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<textarea class="form-control" id="others_address1" id="address_1"  name="address_1" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address1'];}  ?></textarea>
										</div>

										<label for="inputName" class="col-sm-2 control-label">Address2 <span class="req-fld">*</span> </label>
										<div class="col-sm-4">
											<textarea class="form-control"  id="address_2" name="address_2" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address2'];}  ?></textarea>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">City <span class="req-fld">*</span>  </label>
										<div class="col-sm-4">
											<textarea class="form-control" id="address_3" name="address_3" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address3'];}  ?></textarea>
										</div>

										<label for="inputName" class="col-sm-2 control-label">State <span class="req-fld">*</span> </label>
										<div class="col-sm-4">
											<textarea class="form-control"  id="address_4" name="address_4" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address4'];} ?></textarea>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">Pin Code <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<input type="text" autocomplete="off" class="form-control"  minlength="3" maxlength="6" id="pin_code" name="pin_code" value="<?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['pin_code'];}  ?>">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<h4> &nbsp;&nbsp;&nbsp;&nbsp;<b> Return To</b></h4>
								
							</div>
							<div class="form-group">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-2 control-label">Return Type <span class="req-fld">*</span></label>
											<div class="col-sm-4">									   
												<select name="return_type_id" required class="form-control return_cls " > 
												<option value="">Select Return Type</option>
												<?php 
													foreach($return_type as $rt)
													{
														$selected = ($rt['return_type_id']==@$whlk_id)?'selected="selected"':'';
														echo '<option value="'.$rt['return_type_id'].'" '.$selected.'>'.$rt['name'].'</option>';
													}
												?>
												</select>										
											</div>
										<div class="wh_div_cls">
											<label for="inputName" class="col-sm-2 control-label">Return To WH <span class="req-fld">*</span></label>
											<div class="col-sm-4  ">									   
													<select name="to_wh_id" class="form-control r_wh_cls" > 
													<option value="">Select Warehouse</option>
													<?php 
														foreach($all_wh_data as $st2)
														{
															$selected = ($st2['wh_id']==@$wh_id)?'selected="selected"':'';
															echo '<option value="'.$st2['wh_id'].'" '.$selected.'>'.$st2['wh_code'].'- ('.$st2['name'].' )'.'</option>';
														}
													?>
													</select>											
											</div>	
										</div>
										<div class="fe2_order hidden">
											<label for="inputName" class="col-sm-2 control-label">FE2 Order Number <span class="req-fld">*</span></label>
											<div class="col-sm-4">									   
												<input type="text" autocomplete="off" class="form-control"   value=" " id="order_number" name="order_number">										
												<span>
													<!-- <a target="_blank" href="<?php echo SITE_URL;?>toolsAvailabilityWithFE"></i> Check Tool Availability with FE</a> -->
													<a class="md-trigger" data-toggle="modal" data-target="#form-primary" href="#" >Check with SSO</a>
												</span>
											</div>
										</div>				
									</div>

							</div>
							<!-- <div class="form-group">
								<div class="col-md-12">
									
								</div>
							</div> -->
							<br>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-5">
									<button class="btn btn-success" type="submit" value="1" onclick="return confirm('Are you sure you want to Submit ?')" name="submit_pickup" disabled id="submitOrder"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo @$cancel_form_action;?>"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>	  
			            </form>        		
					</div>
				</div>	
				
			</div>
		</div>
	</div>
</div>
<!-- Modal popup -->
<div class="modal fade colored-header" id="form-primary">
	<div class="modal-dialog">
		<div class="md-content">
			<div class="modal-header">
	        	<h3>Check tools Needed By FE</h3>
	    	</div>
	    	<div class="modal-body form">
	    		<form role="form" class="form-horizontal" id="fe_owned_tools_frm" method="post" action="#">
	    			<input type="hidden" name="current_offset" id="current_offset">
                    <input type="hidden" name="submit_button" id="submit_button" value="1">
	    		<div class="row">
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label">Order Number</label>
                        <div class="col-sm-2">
                            <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" class="form-control">
                        </div>
                        <label class="col-sm-1 control-label">SSO</label>
                        <div class="col-sm-3">
                            <select name="ser_sso_id" class="select2" > 
                            <option value="">-SSO-</option>
                            <?php 
                                foreach(get_fe_users() as $ot)
                                {
                                    
                                    echo '<option value="'.$ot['sso_id'].'" '.$selected.'>'.$ot['name'].'</option>';
                                }
                            ?>
                        </select>
                        </div>
                        <label class="col-sm-2 control-label">Asset Number </label>
                        <div class="col-sm-2">
                            <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" class="form-control">
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label">Tool Number</label>
                        <div class="col-sm-2">
                            <input type="text" autocomplete="off" name="part_number" placeholder="Tool Number" class="form-control">
                        </div>
                        <label class="col-sm-1 control-label">Description</label>
                        <div class="col-sm-3">
                            <input type="text" autocomplete="off" name="part_description" placeholder="Tool Description" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <button type="button" name="fe_owned_search" id="fe_owned_search" value="1" class="btn btn-success"><i class="fa fa-search"></i> </button>
                            <button type="button" name="reset_search" id="reset_search" value="1" class="btn btn-success"><i class="fa fa-refresh"></i> </button>
                        </div>
                    </div>
                </div>
                </form>
                <div id="feSearchResults">
	                <h2 align="center">Search FE Orders here</h2>
                </div>
	    	</div>
	    	<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-flat md-close" disabled="" id="confirm_fe1_order">Confirm</button>
				<button type="button" class="btn btn-info btn-flat md-close" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<div class="md-overlay"></div>
<div id="loaderID" style="position:fixed; top:50%; left:58%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>

<script type="text/javascript">		
		get_warehouse_address();
		
</script>
