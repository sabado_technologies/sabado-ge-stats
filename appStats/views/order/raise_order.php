<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
						<?php 
						if($tool_order_id!='')
						{
							$url = SITE_URL.'raise_order/'.storm_encode($tool_order_id);
						}
						else
						{
							$url = SITE_URL.'raise_order';
						}?>
						<form role="form" class="form-horizontal" method="post" action="<?php echo $url;?>">
							<input type="hidden" class="transactionUser" name="onbehalf_fe" value="<?php echo $_SESSION['transactionUser'];?>">
							<input type="hidden" name="country_id" class="transactionCountry" value="<?php echo $_SESSION['transactionCountry'];?>">							
							<input type="hidden" name="tool_order_id" value="<?php echo @$tool_order_id;?>">
							<div class="row">
								<div class="col-sm-12">									
									<!-- <label class="col-sm-2 control-label">Tool Number</label> -->
		                            <div class="col-sm-3">
										<input type="text" autocomplete="off" name="part_number" placeholder="Tool Number" value="<?php echo @$searchParams['part_number'];?>"  class="form-control" maxlength="100">
									</div>
									<!-- <label class="col-sm-2 control-label">Tool Description</label> -->
									<div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="part_description" placeholder="Tool Description" value="<?php echo @$searchParams['part_description'];?>" id="location" class="form-control" maxlength="100">
									</div>
									<!-- <label class="col-sm-2 control-label">Modality</label> -->
										<div class="col-sm-3">
											<select name="modality_id" class="select2" > 
												<option value="">Modality</option>
												<?php 
													foreach($modality_details as $mod)
													{
														$selected = ($mod['modality_id']==@$searchParams['modality_id'])?'selected="selected"':'';
														echo '<option value="'.$mod['modality_id'].'" '.$selected.'>'.$mod['name'].'</option>';
													}
												?>
											</select>
										</div>
									
									<div class="col-sm-3 col-md-3">

										<button type="submit" name="search" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Search"><i class="fa fa-search"></i></button>
										<button type="submit" name="reset" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Reset Search Filters"><i class="fa fa-reply"></i></button>
										<button type="submit" formaction="<?php echo $url; ?>" name="refresh" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>

									</div>
								</div>
								<div class="col-sm-12 " style="margin-top: 5px;">
									<?php if($conditionAccess > 1 || $conditionRole==5 || $conditionRole ==6){ ?>
									<!-- <label class="col-sm-2 control-label">Warhouse</label> -->
									<div class="col-sm-3">
										<select name="ro_wh_id" class="select2" > 
											<option value="">Warehouse</option>
											<?php 
												foreach($warehouseData as $wh)
												{
													$selected = ($wh['wh_id']==@$searchParams['ro_wh_id'])?'selected="selected"':'';
													echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_code'].'-('.$wh['name'].')'.'</option>';
												}
											?>
										</select>
									</div>
									<?php } ?>
								</div>
							</div>
						</form>

						<?php 
						if($tool_order_id!='')
						{
							$url = storm_encode($tool_order_id).'/'.$current_segment;
						}
						else
						{
							$url = $current_segment;
						}?>
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'raise_order/'.$url;?>">
							<input type="hidden" class="transactionUser" name="onbehalf_fe" value="<?php echo $_SESSION['transactionUser'];?>">
							<input type="hidden" name="country_id" value="<?php echo $_SESSION['transactionCountry'];?>">							
							<input type="hidden" name="tool_order_id" value="<?php echo @$tool_order_id;?>">
							<div class="row">
								<div class="header"></div>
								<div class="content">
									<div class="row" style="margin-bottom:3px;">
										
										<div class="col-sm-4" >
											<p style="margin-left: 15px !important;">Transaction Country <?php 
												$country = $this->Common_model->get_value('location',array('location_id'=>$_SESSION['transactionCountry']),'name');
												echo ": <strong>".$country."</strong>"; ?></p>
										</div>
										<div class="col-sm-3">
										<?php   
											if(count(@$_SESSION['tool_id']) > 0)
											{ ?>
												<label class=" col-sm-12 alert alert-info" align="center" style="padding-top: inherit; padding-bottom: inherit;margin-bottom:2px;">
												<?php 
												echo count(@$_SESSION['tool_id']);
												if(count(@$_SESSION['tool_id']) == 1)
												{ echo ' Tool <strong>'; } 
												else
												{ echo ' Tools <strong>';}
											?> Added to cart</strong></label>
											<?php
											}?>
										</div>
										<div class="col-sm-1"></div>
										<div class="col-sm-2" align="right">
											<button type="submit" id="add" name="add" value="1" class="btn btn-success" disabled><i class="fa fa-shopping-cart"></i> Add to Cart</button>
										</div>
										<?php 
										if($tool_order_id!='')
										{
											$proceed = SITE_URL.'issue_tool/'.storm_encode($tool_order_id);
										}
										else
										{
											$proceed = SITE_URL.'issue_tool';
										}
										?>
										<div class="col-sm-1">
											<a href="<?php echo $proceed; ?>" id="issue" name="issue" class="btn btn-success"
											<?php 
												if(!isset($_SESSION['tool_id']))
												{ ?>
												disabled
										  <?php } 
											else if(count(@$_SESSION['tool_id']) == 0)
												{ ?>
												disabled
											<?php } ?>
											><i class="fa fa-mail-forward"></i> Proceed</a>
										</div>
									</div>
								</div>									
							</div>
						
							<div class="header"></div>
							<div class="table-responsive">
								<table class="table table-bordered hover">
									<thead>
										<tr>
											
											<th class="text-center" width="4%"><strong></strong></th>
											<th class="text-center"><strong>Tool Number</strong></th>
											<th class="text-center" width="30%"><strong> Tool Description</strong></th>	
											<th class="text-center"><strong>Modality</strong></th>
											<th class="text-center" width="25%"><strong>Avail Qty in WH Under You</strong></th>								
											<th class="text-center"><strong>Qty</strong></th>
										</tr>
									</thead>
									<tbody>
									<?php
										$i = 1;
										if(count($toolResults)>0)
										{
											foreach($toolResults as $row)
											{
												$isCarted = false;
												if(isset($_SESSION['tool_id'][$row['tool_id']])){$isCarted = true;}
												$chk_st = ($isCarted)?'checked="checked"':'';
												$disable_st = ($isCarted)?'':'disabled="disabled"';
												$comp_qty = ($isCarted)?@$_SESSION['tool_id'][$row['tool_id']]:'';
												?>
												<tr class="toolRow">
													
													<td class="text-center"><input type="checkbox" <?php echo $chk_st; ?> name="tool_id[]" value="<?php echo @$row['tool_id']; ?>" class="icheck"></td>
													<td class="text-center"><?php echo $row['part_number'];?></td>
													<td class="text-center"><?php echo $row['part_description'];?></td>
													<td class="text-center"><?php echo $row['modality_name'];?></td>
													<!-- <td class="text-center"></td> -->
													<td class="text-center avail_qty" >
													<?php echo $row['asset_count'];?> &nbsp;&nbsp;&nbsp;
														<a class="btn-sm btn-primary btn-flat md-trigger inventory_qty" data-toggle="modal" data-target="#form-primary" href="#"  data-tool_id=<?php echo $row['tool_id']; ?>>Check Avail Qty in Other Inventory</a>
														</td>
													<td class="text-center">
														<input type="number" autocomplete="off" name="quantity[]" required min= "1" class="only-numbers toolQuantity text-center" value="<?php echo @$comp_qty; ?>" <?php echo $disable_st; ?> >									</td>										
													
			                                    	</td>
												</tr>
												 <?php 
												 	
											}
										} else {
										?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
								<?php 	} ?>
									</tbody>
								</table>
							</div>
						</form>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
		</div>
	</div>
</div>
<?php
if(count($toolResults)>0)
{
	?>
	<div class="modal fade colored-header" id="form-primary">
		<div class="modal-dialog">
			<div class="md-content">
				<div class="modal-header">
		        	<h3>Quantity In Inventory</h3>
		        		<button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">×</button>
		    	</div>
		    	<div class="modal-body form">
		    		<div class="row view_order">
		    			<div class="formContentBlock">
		    			</div>
		    		</div>
		    	</div>
		    	<div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Cancel</button>
              </div>
		    	
			</div>
		</div>
	</div> <?php
}?>
<div class="md-overlay"></div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	/*add_component_new_row();
	get_asset_level_id();
	check_tool_code_availability();
	check_part_number_availability();*/
	$(document).on('click','.inventory_qty',function(){
    var tool_id= $(this).data("tool_id");
    var transactionUser = $('.transactionUser').val();
    var transactionCountry = $('.transactionCountry').val();
    console.log(tool_id+'--'+transactionUser);
    if(tool_id!= '')
    {
      $.ajax({
      type:"POST",
	  url:SITE_URL+'get_inventory_tools_availability',
	  data:{tool_id:tool_id,transactionUser:transactionUser,transactionCountry:transactionCountry},
	  cache:false,
      success:function(html){
      	
        if(html !='')
        {
        	$('.view_order').html(html);
        }
      }
    });
    }

  });
</script>