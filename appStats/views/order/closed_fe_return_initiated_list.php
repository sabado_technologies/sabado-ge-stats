<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>closed_fe_return_initiated_list">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                             <!-- <label class="col-sm-2 control-label">Return  Type</label> -->
                            <div class="col-sm-3">
                                <select name="return_type_id" class="select2" > 
                                    <option value="">-Return Type-</option>
                                    <?php 
                                        foreach($return_type_arr as $ot)
                                        {
                                            $selected = ($ot['return_type_id']==@$searchParams['ric_return_type_id'])?'selected="selected"':'';
                                            echo '<option value="'.$ot['return_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                            <!-- <label class="col-sm-2 control-label">Order Number</label> -->
                            <div class="col-sm-3">
                                <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['ric_order_number'];?>" class="form-control">
                            </div>
                            <!-- <label class="col-sm-2 control-label">Return Number </label> -->
                            <div class="col-sm-3">
                                <input type="text" name="return_number" autocomplete="off" placeholder="Return Number" value="<?php echo @$searchParams['ric_return_number'];?>" class="form-control">
                            </div>
                            <div class="col-sm-3">    
                                    <button type="submit" name="closed_fe_return_initiated_list" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                    <a href="<?php echo SITE_URL.'closed_fe_return_initiated_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <?php if($task_access==2 || $task_access==3)
                                {
                                    ?>
                                    <div class="col-sm-3">
                                        <?php 
                                        $d['name']        = 'ric_sso_id';
                                        $d['search_data'] = @$searchParams['ric_sso_id'];
                                        $this->load->view('sso_dropdown_list',$d); 
                                        ?>
                                    </div> <?php
                                } ?>
                           
                            <?php if( ($task_access == 3 && $_SESSION['header_country_id']=='') || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']=='') ){ ?>
                                    <div class="col-sm-3">
                                        <select name="ric_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
                                            <?php
                                            foreach($country_list as $row)
                                            {
                                                $selected = ($row['location_id']==@$searchParams['ric_country_id'])?'selected="selected"':'';
                                                echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                            }?>
                                        </select>
                                    </div>

                                     <?php
                                }?> 
                                
                        </div>
                    </div>
				</form>
				<div class="col-sm-12 col-md-12">
                    <div class="table-responsive" >
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>Return Number </strong></th>
                                    <th class="text-center"><strong>Order Number</strong></th>
                                    <th class="text-center"><strong>To Order</strong></th>
                                    <th class="text-center"><strong>From SSO</strong></th>
                                    <th class="text-center"><strong>To SSO</strong></th>                                    
                                    <th class="text-center"><strong>Return Type</strong></th>                                                                        
                                    <th class="text-center"><strong>Coutnry</strong></th>
                                    <th class="text-center"><strong>Status</strong></th>
                                    <th class="text-center"><strong>Action </strong></th> 

                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count(@$orderResults)>0)
                                {   $sn = 1;
                                    foreach(@$orderResults as $row)
                                    { 
                                    ?>
                                        <tr>
                                            <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                            <td class="text-center"><?php echo $sn++; ?></td>
                                            <td class="text-center"><?php echo $row['return_number']; ?></td>
                                            <td class="text-center"><?php echo $row['fe1_order_number']; ?></td>
                                            <td class="text-center"><?php echo ($row['to_order']=='')?'NA':$row['to_order']; ?></td>
                                            <td class="text-center"><?php echo $row['sso']; ?></td>
                                            <td class="text-center"><?php echo ($row['to_sso']=='')?'NA':$row['to_sso']; ?></td>                                            
                                            <td class="text-center"><?php echo $row['return_type']; ?></td>  
                                            <td class="text-center"><?php echo $row['countryName']; ?></td> 
                                            <td class="text-center"><?php                                                 
                                                    $arr = array('tool_order_id'=>@$row['from_tool_order_id'],'rto_id'=>@$row['rto_id']);                                                
                                                    $return_stage_data = get_transaction_latest_record($arr);                                                
                                                    $return_stage = @$return_stage_data['current_stage_id'];
                                                    echo getReturnOrderStatus($row,$return_stage);
                                             ?></td>
                                           <td class="text-center"> <a class="btn btn-default" data-toggle="tooltip" title="View Return  Details" style="padding:3px 3px;" href="<?php echo SITE_URL.'viewClosedReturnDetials/'.storm_encode($row['return_order_id']);?>"><i class="fa fa-eye"></i></a>                                     
                                        </tr>
                                        <?php if(count(@$asset_data[$row['order_status_id']])>0)
                                        {  ?>
                                        <tr class="details">
                                            <td></td>
                                                <td  colspan="10"> 
                                                    <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                                        <thead>
                                                            <th class="text-center">Asset Number </th>
                                                            <th class="text-center">Part Number</th>
                                                            <th class="text-center">Part Description</th>
                                                            <th class="text-center">Serial Number</th>
                                                            <th class="text-center">Part Level </th>
                                                            <th class="text-center">Quantity</th>
                                                        </thead>
                                                        <tbody>
                                                   <?php foreach(@$asset_data[$row['order_status_id']] as $value)
                                                    { 
                                                    ?>
                                                        <tr class="asset_row">
                                                            <td class="text-center"><?php echo $value['asset_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                            <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_level']; ?></td>
                                                            <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                            
                                                        </tr>
                                                    <?php     
                                                    } ?>
                                                        </tbody>
                                                    </table>
                                            </td>
                                        </tr><?php
                                        }
                                        else
                                        { ?>
                                            <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                        <?php 
                                        }
                                    }
                                
                                } else {?>
                                    <tr><td colspan="11" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                        <?php   } ?>
                            </tbody>
                        </table>
                    </div>
                </div> 
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>