<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:50%; z-index:2; opacity:0"><img src="<?php echo SITE_URL1;?>assets/images/ajax-loading-img.gif" /></div>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">          
		    <?php echo $this->session->flashdata('response'); ?>			
			<div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" id="form-primary" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
								<?php if( ($task_access ==3 && $_SESSION['header_country_id'] =='')  ) {?>
								<div class="row">
									<label class="col-sm-4 control-label">User Country<span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<select class="select2 country transactionCountry" required name="country_id" style="width:100%">
											 <?php
											foreach($countriesData as $country)
											{
												$selected = ($country['location_id']==$this->session->userdata('s_country_id'))?'selected="selected"':'';
												echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
											}
											?>
										</select>
									</div>
								</div>
								<?php } else{ 

										$c_id = (@$_SESSION['header_country_id'] =='')?$_SESSION['s_country_id']:$_SESSION['header_country_id'];

									?>
									<input type="hidden"  class="transactionCountry" name="country_id"  value="<?php echo $c_id ?>" >
								<?php } ?>
								<div class="form-group <?php if($task_access == 1) { echo "hidden"; } ?>">
									<label for="inputeq_model" class="col-sm-4 control-label">On behalf of <span class="req-fld">*</span></label>
									<div class="col-md-5">
									<select name="onbehalf_fe" <?php if($task_access != 1) { echo "required"; } ?> class="onbehalf_fe" style="width:100%"> 
										<option value="">- Select SSO -</option>
									</select>
									</div>
								</div>
								<div class="form-group transactionCountryDiv <?php if($task_access !=1){ echo "hidden"; }  ?> ">
									<label for="inputeq_model" class="col-sm-4 control-label">Tool Order Transaction Country <span class="req-fld">*</span></label>
									<div class="col-md-5">
									<select name="userTransactionCountry" <?php if($task_access == 1) { echo "required"; } ?> class="select3 userTransactionCountry" style="width:100%;"> 
										<option value="">- Select Transaction Country -</option>	
										<?php 
											if($task_access == 1)
											{
												foreach(get_user_countries($_SESSION['sso_id']) as $row)
												{
													echo '<option value="'.$row['location_id'].'">'.$row['name'].'</option>';
												}
											}
										
										?>
										
									</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary" type="submit" value="1" name="submitsearch"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL;?>"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
								
							</form>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">

$(document).ready(function(){
	var submitCheck = 0;
	$('.select3').select2();
	var country_id = $('.country :selected').val();
	if(country_id  == undefined)
	{
		var country_id = $('.transactionCountry').val();
	}
	select2Ajax('onbehalf_fe', 'onbehalf_fe_ajax', country_id, 2);
});

$(document).on('change','.country',function(){
	var country_id = $(this).val();
	select2Ajax('onbehalf_fe', 'onbehalf_fe_ajax', country_id, 2);
	$('.transactionCountryDiv').addClass('hidden');
});

$(document).on('change','.onbehalf_fe',function(){
	var sso_id=$(this).val();
	$("button[name='submitsearch']").prop("disabled",true);
	$.ajax({
		type:"POST",
		url:SITE_URL+'ajax_get_countries_by_sso_id',
		data:{sso_id:sso_id},
		cache:false,
		success:function(html){
			$("button[name='submitsearch']").prop("disabled",false);
			$("#form-primary").css("opacity",1);
    		$("#loaderID").css("opacity",0);
			if(html !=0)
			{
				$('.transactionCountryDiv').removeClass("hidden");
				$('.userTransactionCountry').html(html);
				$('.select3').select2('destroy'); 
                $('.select3').select2();
				submitCheck = 1;
			}
			else
			{
				$('.transactionCountryDiv').addClass('hidden');
				submitCheck = 0;
			}				
		}
	});
});

$(document).on('change','.userTransactionCountry',function(){
	var userTransactionCountry = $('.userTransactionCountry').find(':selected').val();
	if(userTransactionCountry == '')
	{
		submitCheck = 0;	
	}
	else
	{
		submitCheck = 1;
		$(".transactionCountry").val(userTransactionCountry);	
	}
});

$("form").submit(function( e ) {
  	if(submitCheck == 1){
	  	var userTransactionCountryVal = $('.userTransactionCountry').find('option:selected').val();
	  	if(userTransactionCountryVal =='' ){
	  	 	alert("Please Choose the Transaction Country");
	  	 	return false;
	  	}else{
	  	 	var userTransactionCountry = $('.userTransactionCountry').find(':selected').val();	
	  	 	$(".transactionCountry").val(userTransactionCountry);
	  	}
  	}
});

</script>