<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
        if(@$flg == 1){?>
            <div class="block-flat">
                <div class="content">
                    <div class="col-sm-12 col-md-12">
                        <div class="row">                           
                            <div class="header">
                                        <h5 align="center"><strong>Pickup Details</strong></h5>
                                    </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>Return Number :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['return_number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong><u>From Address :</u></strong></td>
                                            <td class="data-item"></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>SSO Detail :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['sso_id'].' -  '.$rrow['user_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address1 :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address1']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address2 :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address2']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address3']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>State :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address4']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['location_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Pin Code :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['zip_code']?></td>
                                        </tr>
                                        <?php if($_SESSION['role_id'] ==5 ||$_SESSION['role_id'] ==6 ) { ?>
                                        <tr>
                                            <td class="data-lable"><strong>Ship By :</strong></td>
                                            <td class="data-item"><?php echo @getWhCodeAndName($rrow['wh_id'])?></td>
                                        </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>Expected Arrival Date :</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$rrow['expected_arrival_date']);?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong><u>To Address :</u></strong></td>
                                            <td class="data-item"></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>C/o :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['to_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address1 :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address1']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address2 :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address2']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address3']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>State :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address4']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['location_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Pin Code :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['pin_code']?></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12"><br>
                        <div class="row">                           
                            <div class="header">
                                <h5 align="center"><strong>Pickup Requested Tools</strong></h5>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table hover">
                                <thead>
                                    <tr>
                                        <th class="text-center"><strong>S.No</strong></th>
                                        <th class="text-center"><strong>Tool Number</strong></th>
                                        <th class="text-center"><strong>Tool Description</strong></th>
                                        <th class="text-center"><strong>Asset Number</strong></th>
                                        <th class="text-center"><strong>Serial Number</strong></th>
                                        <th class="text-center"><strong>Quantity</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count(@$asset_list)>0)
                                    {   $sn = 1;
                                        foreach(@$asset_list as $row)
                                        {
                                        ?>
                                            <tr>
                                                <td class="text-center"><?php echo $sn++; ?></td>
                                                <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                <td class="text-center"><?php echo $row['serial_number']; ?></td>
                                                <td class="text-center"><?php echo $row['t_quantity']; ?></td>
                                                
                                            </tr>
                                <?php   }
                                    } else {?>
                                        <tr><td colspan="5" align="center">No Records Found.</td></tr>
                            <?php   } ?>
                                </tbody>
                            </table>
                        </div><br>
                    </div>
                    <div class="row">                           
                        <div class="header">
                            <h5 align="center"><strong>Attached Documents</strong></h5>
                        </div>
                        <br>
                        <div class="table-responsive col-md-offset-1 col-md-10">
                            <table class="table hover">
                                <thead>
                                    <tr>
                                        <th>S No.</th>
                                        <th width="18%" class="text-center"><strong>Attached Date</strong></th>
                                        <th class="text-center"><strong>Document Type</strong></th>
                                        <th width="30%" class="text-center"><strong>Supported Document</strong></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(count(@$attach_document)>0)
                                    {   $count = 1;
                                        foreach($attach_document as $ad)
                                        {?> 
                                            <tr>
                                                <td><?php echo $count++; ?></td>
                                                <td><?php echo $ad['created_time']; ?></td>
                                                <td align="center">
                                                        <?php 
                                                            foreach($documenttypeDetails as $doc)
                                                            {
                                                                if($doc['document_type_id']==@$ad['document_type_id'])
                                                                {
                                                                    echo $doc['name']; 
                                                                }
                                                            }
                                                        ?>
                                                </td>
                                                <td>
                                                    <a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
                                                </td>
                                            </tr>
                                         <?php      
                                        } 
                                    } 
                                    else {
                                        ?>  
                                        <tr><td colspan="4" align="center">No Attached Docs.</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table><br>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-5">
                                <?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>
                                <a class="btn btn-primary" href="<?php echo $cancel_form_action.$current_offset;?>"><i class="fa fa-reply"></i> Back</a>
                            </div>
                        </div>
                    </div><br>
                </div>
            </div>

        <?php }
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>open_fe_return_initiated_list">
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <!-- <label class="col-sm-2 control-label">Return  Type</label> -->
                                <div class="col-sm-3">
                                    <select name="return_type_id" class="select2"> 
                                        <option value="">-Return Type-</option>
                                        <?php 
                                            foreach($return_type_arr as $ot)
                                            {
                                                $selected = ($ot['return_type_id']==@$searchParams['ri_return_type_id'])?'selected="selected"':'';
                                                echo '<option value="'.$ot['return_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <!-- <label class="col-sm-2 control-label">Order Number</label> -->
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['ri_order_number'];?>" class="form-control">
                                </div>
                                <!-- <label class="col-sm-2 control-label">Return Number </label> -->
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="return_number" placeholder="Return Number" value="<?php echo @$searchParams['ri_return_number'];?>" class="form-control">
                                </div>
                                <div class="col-sm-3">    
                                        <button type="submit" name="open_fe_return_initiated_list" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                    <a href="<?php echo SITE_URL.'open_fe_return_initiated_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <?php if($task_access==2 || $task_access==3)
                                    {
                                        ?>
                                        <div class="col-sm-3">
                                            <?php 
                                            $d['name']        = 'ri_sso_id';
                                            $d['search_data'] = @$searchParams['ri_sso_id'];
                                            $this->load->view('sso_dropdown_list',$d); 
                                            ?>
                                        </div> <?php
                                    } ?>
                                
                                <?php if( ($task_access == 3 && $_SESSION['header_country_id']=='') || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']=='') ){ 
                                        ?>
                                        <div class="col-sm-3">
                                            <select name="ri_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
                                                <?php
                                                foreach($country_list as $row)
                                                {
                                                    $selected = ($row['location_id']==@$searchParams['ri_country_id'])?'selected="selected"':'';
                                                    echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                                }?>
                                            </select>
                                        </div>

                                         <?php
                                    }?> 
                            </div>
                        </div>
					</form>
				<div class="col-sm-12 col-md-12">
                    <div class="table-responsive" >
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%"></th>
                                    <th class="text-center" width="4%"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>Order No.</strong></th>
                                    <th class="text-center"><strong>To Order</strong></th>
                                    <th class="text-center"><strong>SSO</strong></th>
                                    <th class="text-center"><strong>To SSO</strong></th>
                                    <th class="text-center" ><strong>Return No. </strong></th>
                                    <th class="text-center"><strong>Return Type</strong></th> 
                                    <th class="text-center"><strong>Country</strong></th>                               
                                    <th class="text-center" width="20%"><strong>Status</strong></th>   
                                    <th class="text-center" width="9%"><strong>Actions</strong></th> 
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count(@$orderResults)>0)
                                {  
                                    foreach(@$orderResults as $row)
                                    { 
                                    ?>
                                        <tr>
                                            <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                            <td class="text-center"><?php echo $sn++; ?></td>
                                            <td class="text-center"><?php echo $row['fe1_order_number']; ?></td>
                                            <td class="text-center"><?php echo (@$row['to_order']=='')?'NA':$row['to_order']; ?></td>
                                            <td class="text-center"><?php echo $row['sso']; ?></td>
                                            <td class="text-center"><?php echo (@$row['to_sso']=='')?'NA':$row['to_sso']; ?></td>
                                            <td class="text-center"><?php echo $row['return_number']; ?></td>
                                            <td class="text-center"><?php echo $row['return_type']; ?></td>  
                                            <td class="text-center"><?php echo $row['countryName']; ?></td>  
                                            <td class="text-center"><?php 
                                            $arr = array('tool_order_id'=>@$row['from_tool_order_id'],'rto_id'=>@$row['rto_id']);                                                
                                            $return_stage_data = get_transaction_latest_record($arr);   
                                            $return_stage = @$return_stage_data['current_stage_id'];
                                            echo getReturnOrderStatus($row,$return_stage);

                                            ?></td> 
                                            <td class="text-center"> 
                                                <a class="btn btn-default" data-toggle="tooltip" title="View Return  Details" style="padding:3px 3px;" href="<?php echo SITE_URL.'viewOpenReturnDetials/'.storm_encode($row['return_order_id']).'/'.storm_encode(1);?>"><i class="fa fa-eye"></i></a>   
                                                <?php   
                                                if( $row['return_approval'] < 2 ) { ?>
                                                <a class="btn btn-danger" data-container="body" data-placement="top"  data-toggle="tooltip" title="Cancel Return Request" style="padding:3px 3px;" href="<?php echo SITE_URL.'cancel_return_request/'.storm_encode($row['return_order_id']);?>"><i class="fa fa-trash-o"></i></a>
                                                <?php }  ?>
                                                
                                            </td>                                  
                                            
                                        </tr>
                                        <?php if(count(@$asset_data[$row['order_status_id']])>0)
                                        {  ?>
                                        <tr class="details">
                                                <td  colspan="11"> 
                                                    <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                                        <thead>
                                                            <th class="text-center"> Asset Number </th>
                                                            <th class="text-center">Tool Number</th>
                                                            <th class="text-center">Tool Description</th>
                                                            <th class="text-center">Serial Number</th>
                                                            <th class="text-center">Tool Level </th>
                                                            <th class="text-center">Quantity</th>
                                                        </thead>
                                                        <tbody>
                                                   <?php foreach(@$asset_data[$row['order_status_id']] as $value)
                                                    { 
                                                    ?>
                                                        <tr class="asset_row">
                                                            <td class="text-center"><?php echo $value['asset_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                            <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_level']; ?></td>
                                                            <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                            
                                                        </tr>
                                                    <?php     
                                                    } ?>
                                                        </tbody>
                                                    </table>
                                            </td>
                                        </tr><?php
                                        }
                                        else
                                        { ?>
                                            <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                        <?php 
                                        }
                                    }
                                
                                } else {?>
                                    <tr><td colspan="11" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                        <?php   } ?>
                            </tbody>
                        </table>
                    </div>
                </div> 
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>