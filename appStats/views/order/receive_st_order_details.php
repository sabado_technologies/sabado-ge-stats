<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>		
            <div class="block-flat">
                <div class="content">
                    <div class="col-sm-12 col-md-12">
                        <div class="row">                           
                            <div class="row" style="padding: 10px;">                            
                                <div class="header">
                                    <h4 align="center"><strong>Stock Transfer Info</strong></h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>STN Number:</strong></td>
                                            <td class="data-item"><?php echo @$trow['stn_number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Request Date:</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$trow['deploy_date']);?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address1:</strong></td>
                                            <td class="data-item"><?php echo @$trow['address1'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City:</strong></td>
                                            <td class="data-item"><?php echo @$trow['address3'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Pin Code:</strong></td>
                                            <td class="data-item"><?php echo @$trow['pin_code'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Courier Name:</strong></td>
                                            <td class="data-item"><?php echo @$drow['courier_name'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Contact Person:</strong></td>
                                            <td class="data-item"><?php echo @$drow['contact_person'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Expected Delivery Date:</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$drow['expected_delivery_date']);?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>SSO Detail:</strong></td>
                                            <td class="data-item"><?php echo @$sso_name;?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Return Date:</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$trow['return_date'])?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address2:</strong></td>
                                            <td class="data-item"><?php echo @$trow['address2'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>State:</strong></td>
                                            <td class="data-item"><?php echo @$trow['address4'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Courier Type:</strong></td>
                                            <td class="data-item"><?php if(@$drow['courier_type']==1)
                                                {
                                                    echo "By Courier";
                                                }
                                                else if(@$drow['courier_type']==2)
                                                {
                                                    echo "By Hand";
                                                }
                                                else if(@$drow['courier_type']==3)
                                                {
                                                    echo "By Dedicated";
                                                }
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Docket/AWB number:</strong></td>
                                            <td class="data-item"><?php echo @$drow['courier_number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Phone Number:</strong></td>
                                            <td class="data-item"><?php echo @$drow['phone_number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Remarks:</strong></td>
                                            <td class="data-item"><?php echo @$drow['remarks'];?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div><br>
                    </div>
                </div>
            </div>
            <div class="block-flat">
                <div class="content">
                    <form method="post" action="<?php echo SITE_URL.'insert_st_fe_received_order';?>" parsley-validate novalidate class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" name="order_delivery_id" value="<?php echo $return_info['order_delivery_id']; ?>"> 
                    <input type="hidden" name="tool_order_id" value="<?php echo $tool_order_id; ?>"> 
                    <input type="hidden" name="stn_number" value="<?php echo @$trow['stn_number']; ?>"> 
                    <input type="hidden" value="<?php echo @$return_assets[0]['order_status_id'];?>" name="order_status_id" >
                        <div class="col-sm-12 col-md-12"><br>
                            <div class="row">                           
                                <div class="header">
                                    <h5 align="center"><strong>Acknowledge Requested Tools</strong></h5>
                                </div>
                            </div>
                            <div class="table-responsive" style="margin-top: 10px;">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-center"><strong>S.No</strong></th>
                                            <th class="text-center"><strong>Tool Number</strong></th>
                                            <th class="text-center"><strong>Tool Description</strong></th>
                                            <th class="text-center"><strong>Asset Number</strong></th>
                                            <th class="text-center"><strong>Acknowledge Status</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if(count(@$return_parts)>0)
                                        {   $sn = 1;
                                            foreach(@$return_parts as $oah_id => $row)
                                            { 
                                            ?>
                                                <tr>
                                                    <input type="hidden" name="assetAndOrderedAsset[<?php echo $row['asset_id'];?>]" value="<?php echo $row['ordered_asset_id'];?>" >   
                                                    <input type="hidden" name="assetAndOrderedTool[<?php echo $row['asset_id'];?>]" value="<?php echo $row['ordered_tool_id'];?>" >   
                                                    <input type="hidden" name="oah_id[<?php echo $oah_id;?>]" value="<?php echo $row['ordered_asset_id'];?>" >
                                                    <td class="center "><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand"></td>
                                                    <td class="text-center"><?php echo $sn++; ?></td>
                                                    <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                    <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                    <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                    <td class="text-center ">                                                                
                                                        <div class="col-sm-12 custom_icheck">
                                                        <label class="radio-inline"> 
                                                            <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                <input type="radio" class="asset_status" value="1" name="oah_condition_id[<?php echo $row['ordered_asset_id']; ?>]" checked style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                            </div> 
                                                            Received
                                                        </label>
                                                        <label class="radio-inline"> 
                                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                <input type="radio" class="asset_status" value="2" name="oah_condition_id[<?php echo $row['ordered_asset_id']; ?>]" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                            </div> 
                                                            Not Received
                                                        </label>                                                        
                                                        </div>
                                                    </td>                                                    
                                                </tr>
                                            <?php if(count(@$row['health_data'])>0)
                                                {  ?>
                                                    <tr class="details">
                                                        <td  colspan="6">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center"><strong>Serial Number</strong></th>                                                                
                                                                <th class="text-center"><strong>Tool Number</strong></th>                                                                
                                                                <th class="text-center"><strong>Tool Description</strong></th>
                                                                <th class="text-center"><strong>Tool Level</strong></th>
                                                                <th class="text-center"><strong>Quantity</strong></th>
                                                                <th class="text-center"><strong>Tool Health</strong></th>
                                                                <th class="text-center"><strong>Remarks</strong></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                   <?php foreach(@$row['health_data'] as $value)
                                                    {  ?>
                                                        <tr class="asset_row">
                                                            <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_level_name']; ?></td>
                                                            <td class="text-center"><?php echo $value['quantity']; ?></td>                                                                    
                                                            <input type="hidden" name="oah_oa_health_id_part_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][]" value="<?php echo $value['part_id'];?>"  >                       
                                                            <td width="32%" class="">
                                                                <div class="custom_icheck">
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue <?php if($value['asset_condition_id'] == 1) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_condition_id" value="1" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id'] == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                        <input type="hidden" class=""  name="old_oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['asset_condition_id'];?>">
                                                                        <input type="hidden" class=""  name="oa_health_id_tool_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['p_tool_id'];?>">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Good
                                                                </label>
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue <?php if(@$value['asset_condition_id'] == 2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_condition_id" value="2" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id']== 2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                        <input type="hidden" class=""  name="old_oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['asset_condition_id'];?>">
                                                                        <input type="hidden" class=""  name="oa_health_id_tool_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['p_tool_id'];?>">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Defective
                                                                </label>
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue <?php if(@$value['asset_condition_id'] == 3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_condition_id" value="3" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id'] == 3) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                        <input type="hidden" class=""  name="old_oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['asset_condition_id'];?>">
                                                                        <input type="hidden" class=""  name="oa_health_id_tool_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['p_tool_id'];?>">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Missing
                                                                </label>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="textbox">
                                                                    <textarea class="form-control textarea" name="remarks[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]"><?php if($value['remarks'] != '') { echo $value['remarks']; } ?></textarea>  
                                                                    <input  type= "hidden"  name="old_remarks[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['remarks'];?>" > 
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php     
                                                    } ?> 
                                                    </tbody>
                                                    </table>
                                                    </td>
                                                </tr><?php
                                                }
                                                else
                                                { ?>
                                                    <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                <?php 
                                                }
                                            }                                        
                                        } else { ?>
                                            <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                <?php   } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>     
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-6"><br>
                                <button class="btn btn-primary" onclick="return confirm('Are you sure, that you checked all L1 items?')"  type="submit" value="1" name="approve"><i class="fa fa-check"></i> Submit</button>
                                <a class="btn btn-danger" href="<?php echo SITE_URL;?>receive_order"><i class="fa fa-times"></i> Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>	    
        </div>
    </div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>