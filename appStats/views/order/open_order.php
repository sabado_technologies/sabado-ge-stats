<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>open_order">
						
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<select name="order_delivery_type_id" class="select2"> 
										<option value="">- Order Type -</option>
										<?php 
											foreach($order_type as $ot)
											{
												$selected = ($ot['order_delivery_type_id']==@$searchParams['order_delivery_type_id'])?'selected="selected"':'';
												echo '<option value="'.$ot['order_delivery_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
											}
										?>
									</select>
								</div>
	                            <div class="col-sm-3">
									<select name="current_stage_id" class="select2"> 
										<option value="">- Order Status -</option>
										<?php 
											foreach($current_stage as $st)
											{
												$selected = ($st['current_stage_id']==@$searchParams['current_stage_id'])?'selected="selected"':'';
												echo '<option value="'.$st['current_stage_id'].'" '.$selected.'>'.$st['name'].'</option>';
											}
										?>
									</select>
								</div>
								<!-- <label class="col-sm-2 control-label <?php if($_SESSION['role_id']== 2 || $_SESSION['role_id']== 5 || $_SESSION['role_id'] ==6) echo "hidden";?>">SSO</label> -->
	                            <div class="col-sm-3 <?php if($task_access==1) echo "hidden";?>">
									<?php 
									$d['name']        = 'search_fe_sso_id';
									$d['search_data'] = @$searchParams['search_fe_sso_id'];
									$this->load->view('sso_dropdown_list',$d); 
									?>
								</div>
								<?php if( ($task_access == 3 && $_SESSION['header_country_id']=='') || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']=='') ){ ?>
								<div class="col-sm-3">
									<select name="ro_country_id" class="select2"> 
										<option value="">- Select Country -</option>
										<?php 
											foreach($countriesData as $con)
											{
												$selected = ($con['location_id']==@$searchParams['ro_country_id'])?'selected="selected"':'';
												echo '<option value="'.$con['location_id'].'" '.$selected.'>'.$con['name'].'</option>';
											}
										?>
									</select>
								</div>
								<?php } ?>					
							</div>
						</div>	
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['order_number'];?>"  class="form-control">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="request_date" readonly placeholder="Request Date" value="<?php echo @$searchParams['request_date'];?>"  id = "dateFrom"class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="return_date" readonly placeholder="Return Date To" value="<?php echo @$searchParams['return_date'];?>" id="dateTo"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								
								<div class="col-sm-3">													
									<button type="submit" name="open_order" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Search"><i class="fa fa-search"></i> </button>
									<a href="<?php echo SITE_URL.'open_order'; ?>" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i> </a>
									<button type="submit" onclick="return confirm('Are you sure you want to download?')" name="downloadOpenOrders" value="1" formaction="<?php echo SITE_URL.'downloadOpenOrders';?>" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Download"><i class="fa fa-cloud-download"></i> </button>  
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover" id="openOrderTable">
						<thead>
							<tr>
								<th class="text-center" width="1%"><strong>S.No</strong></th>
								<th class="text-center" width="5%"><strong>Order Number</strong></th>
								<th class="text-center"><strong>Requested By</strong></th>
								<th class="text-center" width="7%"><strong>Order Type</strong></th>
								<th class="text-center" width="9%"><strong>Request Date</strong></th>
								<th class="text-center" width="9%"><strong>Need Date</strong></th>
								<th class="text-center" width="9%"><strong>Return Date</strong></th>
								<th class="text-center" width="7%"><strong>Country</strong></th>
								<th class="text-center" width="25%"><strong>Order Status</strong></th>
								<th class="text-center" width="10%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['order_number'];?></td>										
										<td class="text-center"><?php echo @$row['sso'];?></td>
										
										<td class="text-center"><?php echo @$row['order_type'];?></td>
										<td class="text-center"><?php echo indian_format(@$row['deploy_date']);?></td>
										<td class="text-center"><?php echo indian_format(@$row['request_date']);?></td>
										<td class="text-center"><?php echo indian_format(@$row['return_date']);?></td>
										<td class="text-center"><?php echo @$row['countryName'];?></td>
										<td class="text-center">
											<?php 
												echo getOpenOrderStatus($row);
											?>
										</td>										
										<td class="text-center">
											<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-eye"></i></a>
											<?php 
												$data = 
												array('primary_key' => storm_encode(0),
											          'table_name'  => '',
											          'work_flow'   => '',
											          'main_table'  => 'tool_order',
											          'main_key'    => $row['tool_order_id']);
												$send_data = implode('~',$data); 
											?>
											<a class="btn btn-primary" 
												style="padding:3px 3px;"
												href="<?php echo SITE_URL.'get_audit_page/'.$send_data;?>"
												target="_blank"
												data-toggle="tooltip" title="Audit Trail"><i class="fa fa-book"></i>
											</a>
											<br>
											<?php  
										    if($row['current_stage_id'] == 4 || $row['current_stage_id'] == 5)
										    {
										    	#check that order is involved in any stock transfer
										      	if(checkSTorderExist($row['tool_order_id'])== 0 )
										      	{ ?>										
										        	<a class="btn btn-warning" style="padding:3px 3px;margin-top: 5px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Update Order"  href="<?php echo SITE_URL.'edit_order/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>

										        	<a class="btn btn-danger" style="padding:3px 3px;margin-top: 5px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Cancel Order"  onclick="return confirm('Are you sure you want to Cancel Requested Tool Order?')" href="<?php echo SITE_URL.'cancel_order/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-trash-o"></i></a><?php 
										        } 
										        else 
										        {  
										        	if($row['current_stage_id'] == 4 && ($task_access == 2 || $task_access == 3))
										        	{ ?>
														<a class="btn btn-danger " data-toolOrderId="<?php echo $row['tool_order_id']; ?>" style="padding:3px 3px;margin-top: 5px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Closed the Shipment Process as Partially Fullfilled"  onclick="return confirm('Are you sure you want to Close the Shipment Process with Partial movement of Tool Order?')" href="<?php echo SITE_URL.'closeShipmentAsPS/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-shopping-cart"></i></a><?php 
													} 
												}
										    }?>
										</td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="10" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
