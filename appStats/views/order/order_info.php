<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">							
							<div class="header">
								<h5 align="center"><strong>Order Details</strong></h5>
							</div>
							<div class="row">
							<div class="col-md-6">
								<table class="no-border">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="data-lable"><strong>Order Number :</strong></td>
									        <td class="data-item"><?php echo ($order_info['order_type'] == 1)?$order_info['stn_number']:@$order_info['order_number'];?></td>
										</tr>
										
										<tr>
											<td class="data-lable"><strong>Ship To Address:</strong></td>
											<td class="data-lable"><strong></strong></td>
									        
										</tr>
										<tr>
											<td class="data-lable"><strong>Address1 :</strong></td>
									        <td class="data-item"><?php echo @$order_info['address1'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>City :</strong></td>
									        <td class="data-item"><?php echo @$order_info['address3'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Pin Code :</strong></td>
									        <td class="data-item"><?php echo @$order_info['pin_code'];?></td>
										</tr>
										
										
									</tbody>
								</table>
							</div>
							<div class="col-md-6">
								<table class="no-border">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="data-lable"><strong>Requested By :</strong></td>
											<td class="data-item"><?php echo @$order_info['sso'];?></td>
										</tr>
										<?php 
										if($order_info['order_delivery_type_id'] == 1) 
										{ ?>
										<tr>
											<td class="data-lable"><strong>Ship To Customer</strong></td>
											<td class="data-item"><?php echo @$order_info['shipToCustomer'];?></td>									        
										</tr><?php 
										}
										else
										{
											if($order_info['order_delivery_type_id']==2)
											{ ?>
												<tr>
													<td class="data-lable"><strong>Ship To Warehouse</strong></td>
													<td class="data-item"><?php echo @$order_info['shipToWh'];?></td>									        
												</tr><?php 
											}
											else
											{ ?>
												<tr>
													<td class="data-lable"><strong>&nbsp;</strong></td>
													<td class="data-item"><strong></strong></td>									        
												</tr><?php
											}
										}?>
										<tr>
											<td class="data-lable"><strong>Address2 :</strong></td>
									        <td class="data-item"><?php echo @$order_info['address2'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>State :</strong></td>
									        <td class="data-item"><?php echo @$order_info['address4'];?></td>
										</tr>
										<?php 
										if($order_info['order_delivery_type_id'] == 1) 
										{ ?>
										<tr>
											<td class="data-lable"><strong>System Id</strong></td>
											<td class="data-item"><?php echo @$order_info['system_id'];?></td>									        
										</tr><?php 
										}
										else
										{ ?>
											<tr>
												<td class="data-lable"><strong>&nbsp;</strong></td>
												<td class="data-item"><strong></strong></td>									        
											</tr><?php											
										}?>
									</tbody>
								</table>
							</div>
							</div><br>
							
							
							<div class="header">
								<h5 align="center"><strong>Ordered Tools</strong></h5>
							</div>
							
							
							
							
							<div class="table-responsive"> 
								<table class="table tabb">
									<thead>
										<tr>
											<th class="text-center"><strong>S No.</strong></th>
			                                <th class="text-center"><strong>Tool Number</strong></th>
			                                <th class="text-center"><strong>Tool Code</strong></th>
			                                <th class="text-center" width="30%"><strong>Tool Description</strong></th>
			                                <th class="text-center"><strong>Ordered Qty</strong></th>
			                                <th class="text-center"><strong>Available Qty</strong></th>
			                              	
										</tr>
									</thead>
									<tbody>
										<?php 
										if(count(@$tools)>0)
										{	$sn = 1;
											foreach(@$tools as $row)
											{
											?>
												<tr class="asset_row">
													<td class="text-center"><?php echo $sn++; ?></td>
													<td class="text-center"><?php echo $row['part_number']; ?></td>
													<td class="text-center"><?php echo $row['tool_code']; ?></td>
													<td class="text-center"><?php echo $row['part_description']; ?></td>
													<td class="text-center"><?php echo $row['quantity']; ?></td>
													<td class="text-center"><?php echo $row['available_quantity']; ?></td>
													
												</tr>
									<?php   }
										} else {?>
											<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
			                    <?php 	} ?>
									</tbody>
								</table>
			                </div><br>

			                <div class="header">
								<h5 align="center"><strong>Documents Details</strong></h5>
							</div>					
							
							<div class="table-responsive"> 
								<table class="table tabb">
									<thead>
										<tr>
											<th class="text-center"><strong>S No.</strong></th>
			                                <th class="text-center"><strong>Document Type</strong></th>
			                                <th class="text-center"><strong>Name</strong></th>
			                                <th class="text-center" width="30%"><strong>Download</strong></th>                           	
										</tr>
									</thead>
									<tbody>
										<?php 
										if(count(@$attach_document)>0)
										{	$sn = 1;
											foreach(@$attach_document as $row)
											{
											?>
												<tr class="asset_row">
													<td class="text-center"><?php echo $sn++; ?></td>
													<td class="text-center"><?php echo $this->Common_model->get_value('document_type',array('document_type_id'=>$row['document_type_id']),'name'); ?></td>
													<td class="text-center"><?php echo $row['doc_name']; ?></td>
													<td><a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo SITE_URL1.order_document_path().$row['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $row['name']; ?></td>
													
												</tr>
									<?php   }
										} else {?>
											<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
			                    <?php 	} ?>
									</tbody>
								</table>
			                </div>
			                <div class="form-group">
								<div class="col-sm-offset-5 col-sm-5">
										<?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>
									<a class="btn btn-primary" href="<?php echo $form_action.$current_offset;?>"><i class="fa fa-reply"></i> Back</a>
								</div>
							</div>
							<br>
			                <br>
			                
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<style type="text/css">
    /*form input[type=text]{
        height:0.0001px !important; 
        color: #fff; 
        border-color: none; 
        opacity:0.0001;
    }*/
</style>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#asset_number').on('input',function(){
            var asset_number = $(this).val();
            if(asset_number.trim() != '')
            {
            	$('form').submit();
            }
        });
        $('body').on('click',function(e){
        	$('#asset_number').focus();
        });
    });
</script>