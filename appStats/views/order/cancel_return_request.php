<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
            <?php echo $this->session->flashdata('response'); ?>
    		<div class="block-flat">
                <div class="content">
                    <div class="col-sm-12 col-md-12">
                        <div class="row">                           
                            <div class="header">
                                        <h5 align="center"><strong>Pickup Details</strong></h5>
                                    </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>Return Number :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['return_number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong><u>From Address :</u></strong></td>
                                            <td class="data-item"></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>SSO Detail :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['sso_id'].' -  '.$rrow['user_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address1 :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address1']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address2 :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address2']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address3']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>State :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address4']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['location_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Pin Code :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['zip_code']?></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>Expected Arrival Date :</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$rrow['expected_arrival_date']);?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong><u>To Address :</u></strong></td>
                                            <td class="data-item"></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>C/o :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['to_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address1 :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address1']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address2 :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address2']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address3']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>State :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address4']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['location_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Pin Code :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['pin_code']?></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12"><br>
                        <div class="row">                           
                            <div class="header">
                                <h5 align="center"><strong>Pickup Requested Tools</strong></h5>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table hover">
                                <thead>
                                    <tr>
                                        <th class="text-center"><strong>S.No</strong></th>
                                        <th class="text-center"><strong>Tool Number</strong></th>
                                        <th class="text-center"><strong>Tool Description</strong></th>
                                        <th class="text-center"><strong>Asset</strong></th>
                                        <th class="text-center"><strong>Quantity</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count(@$asset_list)>0)
                                    {   $sn = 1;
                                        foreach(@$asset_list as $row)
                                        {
                                        ?>
                                            <tr>
                                                <td class="text-center"><?php echo $sn++; ?></td>
                                                <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                <td class="text-center"><?php echo $row['t_quantity']; ?></td>
                                                
                                            </tr>
                                <?php   }
                                    } else {?>
                                        <tr><td colspan="5" align="center">No Records Found.</td></tr>
                            <?php   } ?>
                                </tbody>
                            </table>
                        </div><br>
                    </div>
                    <div class="row">                           
                        <div class="header">
                            <h5 align="center"><strong>Attached Documents</strong></h5>
                        </div>
                        <br>
                        <div class="table-responsive col-md-offset-1 col-md-10">
                            <table class="table hover">
                                <thead>
                                    <tr>
                                        <th>S No.</th>
                                        <th width="18%" class="text-center"><strong>Attached Date</strong></th>
                                        <th class="text-center"><strong>Document Type</strong></th>
                                        <th width="30%" class="text-center"><strong>Supported Document</strong></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(count(@$attach_document)>0)
                                    {   $count = 1;
                                        foreach($attach_document as $ad)
                                        {?> 
                                            <tr>
                                                <td><?php echo $count++; ?></td>
                                                <td><?php echo $ad['created_time']; ?></td>
                                                <td align="center">
                                                        <?php 
                                                            foreach($documenttypeDetails as $doc)
                                                            {
                                                                if($doc['document_type_id']==@$ad['document_type_id'])
                                                                {
                                                                    echo $doc['name']; 
                                                                }
                                                            }
                                                        ?>
                                                </td>
                                                <td>
                                                    <a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
                                                </td>
                                            </tr>
                                         <?php      
                                        } 
                                    } 
                                    else {
                                        ?>  
                                        <tr><td colspan="4" align="center">No Attached Docs.</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table><br>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-5">
                                <a class="btn btn-danger" href="<?php echo SITE_URL;?>submit_cancel_return_request/<?php echo storm_encode($rrow['return_order_id']); ?>" onclick="return confirm('Are you sure you want to Cancel Return Request?')"><i class="fa fa-times"></i> Cancel Return Request </a>
                                <a class="btn btn-primary" href="<?php echo SITE_URL.'open_fe_return_initiated_list';?>"><i class="fa fa-reply"></i> Back</a>
                            </div>
                        </div>
                    </div><br>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>