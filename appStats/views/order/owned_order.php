<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>raise_pickup">
						<div class="row">
							<div class="col-sm-12">
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['order_number'];?>"  class="form-control">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" id="dateFrom" name="deploy_date" placeholder="Request Date" value="<?php echo @$searchParams['deploy_date'];?>"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" id ="dateTo" name="return_date" placeholder="Return Date To" value="<?php echo @$searchParams['return_date'];?>"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">							
									<button type="submit" name="raise_pickup" value="1" class="btn btn-success"><i class="fa fa-search"></i> </button>
									<a href="<?php echo SITE_URL.'raise_pickup'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> </a>
									<button type="submit" onclick="return confirm('Are you sure you want to download?')" name="downloadReturnOrders" value="1" formaction="<?php echo SITE_URL.'downloadReturnOrders';?>" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Download"><i class="fa fa-cloud-download"></i> </button>  
								</div>
							</div>					
							<div class="col-sm-12" style="margin-top:10px">
								<div class="col-sm-3">
									<select name="order_delivery_type_id" class="select2"> 
										<option value="">-Order Type-</option>
										<?php 
											foreach($order_type as $ot)
											{
												$selected = ($ot['order_delivery_type_id']==@$searchParams['order_delivery_type_id'])?'selected="selected"':'';
												echo '<option value="'.$ot['order_delivery_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
											}
										?>
									</select>
								</div>
								<?php if($task_access==2 || $task_access==3)
									{
										?>
										<div class="col-sm-3">
											<?php 
                                            $d['name']        = 'rt_sso_id';
                                            $d['search_data'] = @$searchParams['rt_sso_id'];
                                            $this->load->view('sso_dropdown_list',$d); 
                                            ?>
										</div> <?php
									} ?>								
								 <?php
									if( ($task_access==3 && @$_SESSION['header_country_id']=='') || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']=='') ) 
									{
										?>
										<div class="col-sm-3">
											<select name="rt_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
												<?php
												foreach($country_list as $row)
												{
													$selected = ($row['location_id']==@$searchParams['rt_country_id'])?'selected="selected"':'';
													echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
												}?>
											</select>
										</div>

										 <?php
									}?>	
								</div>
							</div>	
						</div>
					</form>
					<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th> </th>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Order Number</strong></th>
								<th class="text-center"><strong>SSO</strong></th>
								<th class="text-center"><strong>Order Type</strong></th>
								<th class="text-center"><strong>Request Date</strong></th>
								<th class="text-center"><strong>Return Date</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center"><strong>Order Status</strong></th>																
								<th class="text-center" width="15%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{ ?>
									<tr>
										<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand" ></td>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['order_number'];?></td>										
										<td class="text-center"><?php echo @$row['sso'];?></td>
										<td class="text-center"><?php echo @$row['order_type'];?></td>
										<td class="text-center"><?php echo indian_format(@$row['request_date']);?></td>
										<td class="text-center"><?php echo indian_format(@$row['return_date']);?></td>
										<td class="text-center"><?php echo @$row['countryName'];?></td>
										<td class="text-center"><?php echo @$row['current_stage_name'];?></td>
										<td class="text-center">
											<?php if(@$row['days_approval'] == 1)
												 { ?>
													At Admin For Return Date Extension Approval
											<?php }
											 else
											 	{   
											 		if(count(@$row['owned_tools_list'])>0)
											 		{ ?>
	                                	        	
	                                	        		<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Initiate Return "  href="<?php echo SITE_URL.'owned_order_details/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>

	                                	        		<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Extend Return Date"  href="<?php echo SITE_URL.'raiseDaysExtensionRequest/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-calendar"></i></a>
	                                	        		<?php 
	                                	        	}
	                                	        	else
	                                	        	{
	                                	        		echo "NA"; 
	                                	        	}
	                                	        } ?>                                       	
                                    	</td>
									</tr>
									<?php if(count(@$row['owned_tools_list'])>0)
                                            {  ?>
                                            <tr class="details">
                                                    <td  colspan="10"> 
                                                        <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                                            <thead>
                                                            	<th class="text-center">Asset Number</th>
                                                                <th class="text-center">Tool Number</th>
                                                                <th class="text-center">Tool Description</th>
                                                                <th class="text-center">Serial Number</th>
                                                                <th class="text-center">Tool Type </th>
                                                                
                                                            </thead>
                                                            <tbody>
                                                       <?php foreach(@$row['owned_tools_list'] as $value)
                                                        { 
                                                        ?>
                                                            <tr class="asset_row">
                                                            	 <td class="text-center"><?php echo $value['asset_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                                <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['tool_type']; ?></td>
                                                            </tr>
                                                        <?php     
                                                        } ?>
                                                            </tbody>
                                                        </table>
                                                </td>
                                            </tr><?php
                                            }
                                            else
                                            { ?>
                                                <tr class="details"><td colspan="10" align="center"><span class="label label-primary">Waiting For FE to FE Transfer Approval</span></td></tr>
                                            <?php 
                                            }
								}
							} else {
							?>	<tr><td colspan="10" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
