<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>closed_receive_order">
						<div class="row">
												
							<div class="col-sm-12 form-group">
								<!-- <label class="col-sm-2 control-label">Order Type</label> -->
								<div class="col-sm-3">
									<select name="order_delivery_type_id" class="select2" > 
										<option value="">-Order Type-</option>
										<?php 
											foreach($order_type as $ot)
											{
												$selected = ($ot['order_delivery_type_id']==@$searchParams['order_delivery_type_id'])?'selected="selected"':'';
												echo '<option value="'.$ot['order_delivery_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
											}
										?>
									</select>
								</div>	
								<!-- <label class="col-sm-3 control-label">Ack From</label> -->
								<div class="col-sm-3">
									<select name="ack_from" class="select2"> 
										<option value="">-Ack From-</option>
										<?php 
											foreach(ack_from() as $ot)
											{
												$selected = ($ot['id']==@$searchParams['ack_from'])?'selected="selected"':'';
												echo '<option value="'.$ot['id'].'" '.$selected.'>'.$ot['name'].'</option>';
											}
										?>
									</select>
								</div>
								<?php if($task_access==2 || $task_access==3)
									{
										?>
										<div class="col-sm-3">
											<?php 
	                                        $d['name']        = 'crec_sso_id';
	                                        $d['search_data'] = @$searchParams['rec_sso_id'];
	                                        $this->load->view('sso_dropdown_list',$d); 
	                                        ?>
										</div> <?php
									} ?>		
								<?php
										if( ($task_access==3 && @$_SESSION['header_country_id']=='' ) || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']==''))
										{
											?>
											<div class="col-sm-3">
												<select name="crec_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
													<?php
													foreach($country_list as $row)
													{
														$selected = ($row['location_id']==@$searchParams['crec_country_id'])?'selected="selected"':'';
														echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
													}?>
												</select>
											</div>

											 <?php
										}?>			
								</div>
								<div class="col-sm-12 form-group">
								<!-- <label class="col-sm-2 control-label">Order Number</label> -->
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['order_number'];?>"  class="form-control">
								</div>
								<!-- <label class="col-sm-2 control-label">Request Date</label> -->
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" readonly id="dateFrom" name="deploy_date" placeholder="Request Date" value="<?php echo @$searchParams['deploy_date'];?>"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								
								<!-- <label class="col-sm-2 control-label">Return Date</label> -->
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" id="dateTo" readonly name="return_date" placeholder="Return Date To" value="<?php echo @$searchParams['return_date'];?>"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3 col-md-3">							
									<button type="submit" name="receive_order" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'closed_receive_order'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
								</div>			
							</div>	
						</div>	
					</form>
				</div>
					
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th></th>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Order Number</strong></th>
								<th class="text-center"><strong>SSO</strong></th>
								<th class="text-center"><strong>Order Type</strong></th>
								<th class="text-center"><strong>Request Date</strong></th>
								<th class="text-center"><strong>Return Date</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center"><strong>Order Status</strong></th>																
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{?>
									<tr>
										<td class="text-center ">
											<?php if(count(@$st_data[$row['tool_order_id']])>0) { ?>
											<img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details">
											<?php } ?>
											</td>
											<td class="text-center"><?php echo @$sn++;?></td>
											<td class="text-center"><?php echo @$row['order_number'];?></td>										
											<td class="text-center"><?php echo @$row['sso'];?></td>	
											<td class="text-center"><?php echo @$row['order_type'];?></td>
											<td class="text-center"><?php echo indian_format(@$row['deploy_date']);?></td>
											<td class="text-center"><?php echo indian_format(@$row['return_date']);?></td>
											<td class="text-center"><?php echo @$row['countryName'];?></td>
											<td class="text-center">
												<?php if(count(@$st_data[$row['tool_order_id']])>0 && $row['current_stage_id'] == 7) { echo 'At FE' ?>
												
												<?php } else{ 
												 echo @$row['cs_name'];
												}
												 ?>
											</td>			
											<td class="text-center">	
											<?php if(count(@$st_data[$row['tool_order_id']])>0) { ?>
												<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(12);?>"><i class="fa fa-eye"></i></a>									
											<?php }else{ ?>
												<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'closed_receive_st_fe_order_details/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-eye"></i></a>									
	                                        <?php } ?>
	                                        
	                                    </td>
									</tr>
									<?php if(count(@$st_data[$row['tool_order_id']])>0)
                                        {  ?>
                                        <tr class="details">
                                        	<td></td>
                                            <td colspan="9"> 
                                                <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                                        <thead>
                                                            <th class="text-center">ST Number</th>
                                                            <th class="text-center">Shipment From</th>
                                                            <th class="text-center">Order Status</th>
                                                            <th class="text-center">Actions</th>

                                                        </thead>
                                                        <tbody>
                                                   <?php foreach(@$st_data[$row['tool_order_id']] as $value)
                                                    { 
                                                    ?>
                                                        <tr class="asset_row">
                                                            <td class="text-center"><?php echo $value['stn_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['wh']; ?></td>
                                                            <td class="text-center"><?php 
                                                            	echo stockTransferStatus($value,2);
                                                            
                                                             ?></td>  
                                                              <td class="text-center">
                                                            
                                                            <!-- Closed STNs -->
                                                            <?php if($value['current_stage_id'] == 3 && $value['status']!=4){ ?>
                                                               <a class="btn btn-default" style="padding:3px 3px;"  data-contain<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'closed_receive_st_fe_order_details/'.storm_encode($value['tool_order_id']);?>"><i class="fa fa-eye"></i></a>									
                                                            <?php } else { ?>
                                                             <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View  Details"  href="<?php echo SITE_URL.'transfer_order_info/'.storm_encode($value['tool_order_id']).'/'.storm_encode(3);?>"><i class="fa fa-eye"></i></a>                                                  
                                                            <?php } ?>     
                                                           
                                                             </td>                                                           
                                                        </tr>
                                                    <?php     
                                                    } ?>
                                                        </tbody>
                                                    </table>
                                            </td>
                                        </tr><?php
                                        } ?>
						<?php	}
							} else {
							?>	<tr><td colspan="10" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
					<div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>
				</div>
	                	          		
		</div>
	</div>		
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$('.details').hide();
	$(document).on('click',".toggle-details",function () { 
	  var row=$(this).closest('tr');
	  var next=row.next();
	  $('.details').not(next).hide();
	  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
	  next.toggle();
	  if (next.is(':hidden')) {
	    $(this).attr('src',ASSET_URL+'images/plus.png');
	    $(this).attr('title','Expand');
	  } else {
	    $(this).attr('src',ASSET_URL+'images/minus.png');
	    $(this).attr('title','Collapse');
	  }
	});
</script>
