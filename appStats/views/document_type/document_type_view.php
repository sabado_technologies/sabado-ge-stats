<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form method="post" action="<?php echo $form_action;?>" parsley-validate novalidate class="form-horizontal">
						<?php
                        if($flg==2){
                            ?>
                            <input type="hidden" name="encoded_id" value="<?php echo storm_encode($row['document_type_id']);?>">
                            <?php
                        }
                        ?>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
									<label for="inputName" class=" col-sm-4 control-label">Workflow <span class="req-fld">*</span></label>
										<div class="col-sm-5">
											<select class="form-control" required name="workflow_type">
												<option value="">- Workflow Type -</option>
												<option value="1" <?php if(@$row['workflow_type']==1){ echo "selected";}?>>Asset Master</option>
												<option value="2" <?php if(@$row['workflow_type']==2){ echo "selected";}?>>Tools Order</option>
												<option value="3" <?php if(@$row['workflow_type']==3){ echo "selected";}?>>Stock Transfer</option>
												<option value="4" <?php if(@$row['workflow_type']==4){ echo "selected";}?>>Calibration</option>
												<option value="5" <?php if(@$row['workflow_type']==5){ echo "selected";}?>>Repair</option>
												<option value="6" <?php if(@$row['workflow_type']==6){ echo "selected";}?>>Scrap</option>
											</select>
										</div>
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
									<label for="inputName" class=" col-sm-4 control-label">Name <span class="req-fld">*</span></label>
										<div class="col-sm-5">
											<input type="text" autocomplete="off" required class="form-control" placeholder="Document Type Name" name="name" value="<?php echo @$row['name'];?>">
										</div>
                                    </div>
                                </div>
                            </div>								
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-6">
									<button class="btn btn-primary" type="submit" value="1" name="submit_branch"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>document_type"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>document_type">
						<div class="col-sm-12">
							<label class="col-sm-2 control-label">Workflow Type</label>
							<div class="col-sm-2">
								<select class="form-control" required name="workflow_type">
									<option value="">- Workflow Type -</option>
									<option value="1" <?php if(@$search_data['workflow_type']==1){ echo "selected";}?>>Asset Master</option>
									<option value="2" <?php if(@$search_data['workflow_type']==2){ echo "selected";}?>>Tools Order</option>
									<option value="3" <?php if(@$search_data['workflow_type']==3){ echo "selected";}?>>Stock Transfer</option>
									<option value="4" <?php if(@$search_data['workflow_type']==4){ echo "selected";}?>>Calibration</option>
									<option value="5" <?php if(@$search_data['workflow_type']==5){ echo "selected";}?>>Repair</option>
									<option value="6" <?php if(@$search_data['workflow_type']==6){ echo "selected";}?>>Scrap</option>
								</select>
							</div>
							<label class="col-sm-2 control-label">Document Type</label>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="doc_name" value="<?php echo @$search_data['name'];?>" class="form-control">
							</div>
							<div class="col-sm-3">
								<button type="submit" name="searchdocument" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
								<a href="<?php echo SITE_URL.'document_type';?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>                                
                                <a data-toggle="tooltip" title="Add New" href="<?php echo SITE_URL.'add_document_type';?>" class="btn btn-success"><i class="fa fa-plus"></i></a>
								<button type="submit" data-toggle="tooltip" title="Download" name="download_document_type" value="1" formaction="<?php echo SITE_URL.'download_document_type';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i></button>  
							</div>
						</div>
					</form>
				</div><br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                            	<th class="text-center"><strong>Workflow Type</strong></th>
                                <th class="text-center"><strong>Document Type</strong></th>
                                <th class="text-center"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($document_results)>0)
							{
								foreach($document_results as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php 
										if($row['workflow_type']==1)
										{
											echo "Asset Master";
										}
										else if($row['workflow_type']==2)
										{
											echo "Tools Order";
										}
										else if($row['workflow_type']==3)
										{
											echo "Stock Transfer";
										}
										else if($row['workflow_type']==4)
										{
											echo "Calibration";
										}
										else if($row['workflow_type']==5)
										{
											echo "Repair";
										}
										else if($row['workflow_type']==6)
										{
											echo "Scrap";
										}?></td>
										<td class="text-center"><?php echo $row['name']; ?></td>
										<td class="text-center">
										<?php
										if($row['document_type_id']==1 || $row['document_type_id']==2)
											{$disabled='disabled';}
										else
										{
											$disabled='';
										}
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-default" <?php echo $disabled?> data-toggle="tooltip" title="Edit" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_document_type/'.storm_encode($row['document_type_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php
                                        }else{?>
                                        <a class="btn btn-default" <?php echo $disabled?> readonly title="activate Document Type to edit" style="padding:3px 3px;" href="#"><i class="fa fa-pencil"></i></a>
                                       <?php }?>
                                        <?php
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" <?php echo $disabled?> data-toggle="tooltip" title="Deactivate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_document_type/'.storm_encode($row['document_type_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" <?php echo $disabled?> data-toggle="tooltip" title="activate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_document_type/'.storm_encode($row['document_type_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="3" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	is_documenttypeExist();
</script>