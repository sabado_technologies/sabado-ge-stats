<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<?php
			if($flg==2)
			{
				?>
				<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
			    	<input type="hidden" name="rc_asset_id"  value="<?php echo storm_encode(@$rc_asset_id);?>">
			    	<input type="hidden" name="rc_order_id"  value="<?php echo storm_encode(@$rc_order_id);?>">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="block-flat">
								<div class="content">
									<div class="table-responsive">
										<table class="table table-bordered hover" id="mytable">
											<thead>
												<tr>
													<th class="text-center"><strong></strong></th>
													<th class="text-center"><strong>CR Number</strong></th>
													<th class="text-center"><strong>Tool Number</strong></th>
													<th class="text-center"><strong>Tool Description</strong></th>
													<th class="text-center"><strong>Tool Type</strong></th>
													<th class="text-center"><strong>Asset Number</strong></th>
													<th class="text-center"><strong>Serial Number</strong></th>
												</tr>
											</thead>
											<tbody>
												<?php
												if(count($asset_details)>0)
												{
													
													?>
													<tr class="asset_selected_row text-center">
														<td class="center "><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'></td>
														<td class="text-center"><?php echo $asset_details['rc_number'];?>
															
														</td>
														<td class="text-center"><?php echo $asset_details['part_number'];?></td>
														<td class="text-center"><?php echo $asset_details['part_description'];?></td>
														<td class="text-center"><?php echo $asset_details['tool_type'];?></td>
														<td class="text-center"><?php echo $asset_details['asset_number'];?>
														</td>
														<td class="text-center"><?php echo $asset_details['serial_number'];?>
														</td>
													</tr> 
													<tr class="details">
														<?php
														if(count($asset_details['asset_health_list'])>0)
														{
															?>
															<td colspan="7">
																<table class="table">
																	<thead>
																		<th class="text-center"><strong>Serial Number</strong></th>
																		<th class="text-center"><strong>Tool Number</strong></th>
																		<th class="text-center"><strong>Tool Description</strong></th>
																		<th class="text-center"><strong>Tool Level</strong></th>
																		<th class="text-center"><strong>Qty</strong></th>
																		<th class="text-center"><strong>Tool Health</strong></th>
																	</thead>
																	<tbody>
																		<?php
																		foreach($asset_details['asset_health_list'] as $value)
																		{
																			?>
																			<tr class="asset_row">
																				<td align="center">
																					<?php echo $value['serial_number'];?>
																				</td>
																				<td align="center">
																					<?php echo $value['part_number'];?>
																				</td>
																				<td align="center">
																					<?php echo $value['part_description'];?>
																				</td>
																				<td align="center">
																					<?php echo $value['part_level_name'];?>
																				</td>
																				<td align="center">
																					<?php echo $value['quantity'];?>
																				</td>
																				<td align="center">
																					<?php 
																					if($selected_asset_health[$value['part_id']]['asset_condition_id'] == 1)
																						echo 'Good';
																					if($selected_asset_health[$value['part_id']]['asset_condition_id'] == 2)
																						echo 'Defected';
																					if($selected_asset_health[$value['part_id']]['asset_condition_id'] == 3)
																						echo 'Missing';
																					?>
																				</td>
																			</tr> <?php
																		}?>
																	</tbody>
																</table>
															</td> <?php
														} 
														else
														{ ?>
															<td colspan="6" align="center"><span class="label label-primary">No Open Calibration Records</span></td> <?php
														}?>
													</tr><?php
												}
												else 
												{
												?>	<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
												<?php 	} ?>
											</tbody>
										</table>
									</div><br>
											
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-4 control-label">Status :</label>
												<div class="custom_icheck col-sm-5">
													<label class="radio-inline">
														<div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
											                <input type="radio" class="check_qa_status" value="1" name="qa_status" checked style="position: absolute; opacity: 0;">
											                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
											            </div> Approve
											        </label>
											        <label class="radio-inline">
														<div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
											                <input type="radio" class="check_qa_status" value="2" name="qa_status" style="position: absolute; opacity: 0;">
											                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
											            </div> Reject
											        </label>
											    </div>
											</div>
										</div>
										<div class="form-group hidden qa_reason">
											<div class="col-md-12">
												<label for="inputName" class="col-sm-4 control-label">Reason :</label>
											   	<div class="custom_icheck col-sm-5">
											   	<?php $i = 0;
												foreach($qa_status as $row)
												{?>
											        <label class="radio-inline"> 
											            <div class="iradio_square-blue <?php if($i==0) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
											                <input type="radio" value="<?php echo $row['qa_status_id']; ?>" <?php if($i==0) { echo "checked"; } $i++; ?> name="qa_reason"  style="position: absolute; opacity: 0;">
											                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
											            </div> <?php echo $row['name'];?></label><br>
											    <?php } ?>
											    </div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
												<label class="col-sm-4 control-label">Remarks :<span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<textarea class="form-control" name="remarks" required></textarea>
												</div>
											</div>
										</div>
									</div>
			                        	<div class="row">
											<div class="form-group">
												<div class="col-md-12">
											    	<div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
														<a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a><br>
													</div>
													<div class="table-responsive col-md-offset-1 col-md-9  tab_hide">
														<table class="table hover document_table">
															<thead>
																<tr>
																	<th  width="8%" class="text-center"><strong>Sno</strong></th>
																	<th width="40%" class="text-center"><strong>Document Type</strong></th>
																	<th width="40%" class="text-center"><strong>Supported Document</strong></th>
																	<th width="8%"  class="text-center"><strong>Delete</strong></th>
																</tr>
															</thead>
																<tbody>
																<?php $count = 1;
																	if(count(@$main_doc)>0)
																	{	
																		foreach($main_doc as $doc)
																			{?>	
																				<tr class="attach">
																					<input type="hidden" name="doc_id" class="doc_id" value="<?php echo $doc['rcd_id'];?>">
																					<td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
																					<td class="text-center">
																						<?php 
																							foreach($documenttypeDetails as $docs)
																							{
																								if($docs['document_type_id']==@$doc['document_type_id'])
																								{
																									echo $docs['name'].' - ('.date('d-m-Y H:i:s',strtotime($doc['created_time'])).')'; 
																								}
																							}
																						?>
																					</td>
																					<td>
																                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
																					</td>
																					<td class="text-center">
																						<a class="btn link btn-danger btn-sm" disabled  > <i class="fa fa-trash-o"></i></a>
																			        </td>
																				</tr>
																			 <?php		
																			}
																		}
																		if(count($attach_document)>0)
																		{
																			foreach($attach_document as $doc)
																			{?>	
																				<tr class="attach">
																					<td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
																					<td class="text-center">
																						<?php 
																							foreach($documenttypeDetails as $docs)
																							{
																								if($docs['document_type_id']==@$doc['document_type_id'])
																								{
																									echo $docs['name'].' - ('.date('d-m-Y H:i:s',strtotime($doc['created_time'])).')'; 
																								}
																							}
																						?>
																					</td>
																					<td>
																                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
																					</td>
																					<td class="text-center">
																						<a class="btn link btn-danger btn-sm" disabled> <i class="fa fa-trash-o"></i></a>
																	        		</td>
																				</tr>
																			 <?php		
																			}
																		}?>			
																		<tr class="doc_row">
																			<td class="text-center">
																				<span class="sno"><?php echo $count; ?></span>
																			</td>
																			<td class="text-center">
																				<select name="document_type[1]" class="form-control doc_type" > 
																					<option value="">Select Document Type</option>
																					<?php 
																						foreach($documenttypeDetails as $doc)
																						{
																							echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
																						}
																					?>
																				</select>
																			</td>
																			<td>
																	    		<input type="file" id="document" name="support_document_1" class="document">
																			</td>
																			
																	        <td class="text-center"><a <?php if(@$flg == 2){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
																		</tr>
																	</tbody>
														</table><br>
													</div>
												</div>
											</div>
				                    		<div class="form-group">
												<div class="col-sm-offset-5 col-sm-5">
													<button class="btn btn-primary"onclick="return confirm('Are you sure you want to Submit?')" value="1"  type="submit" name="submit"><i class="fa fa-check"></i> Submit</button>
													<a class="btn btn-danger" href="<?php echo SITE_URL;?>qc_calibration"><i class="fa fa-times"></i> Cancel</a>
													
												</div>
											</div>
										</div>
									
								</div>
							</div>
						</div>
					</div>
				</form> <?php
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
				<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>qc_calibration"  parsley-validate novalidate method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="block-flat">
								<div class="content">
									<div class="row">
										<div class="col-sm-12 form-group">
											<!-- <label class="col-sm-1 control-label">CRB No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="rcb_number" placeholder="CRB Number" value="<?php echo @$search_data['rcb_number'];?>"  class="form-control" maxlength="100">
											</div>
											<!-- <label class="col-sm-1 control-label">CR No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="rc_number" placeholder="CR Number" value="<?php echo @$search_data['rc_number'];?>"  class="form-control" maxlength="100">
											</div>
											<!-- <label class="col-sm-1 control-label">Asset No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>"  class="form-control" maxlength="100">
											</div>
											<div class=" col-sm-3">							
												<button type="submit" name="searchcalibration" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
												<a href="<?php echo SITE_URL.'qc_calibration'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
											</div>
										</div>
										<div class="col-sm-12 form-group">
											<div class="col-sm-3">
											    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
											</div>
											<?php
											if($task_access==2 || $task_access==3)
											{
												?>
												<!-- <label class="col-sm-1 control-label">Warehouse</label> -->
												<div class="col-sm-3">
													<select name="wh_id" class=" main_status supplier_id select2" > 
														<option value="">- Warehouse -</option>
														<?php
														foreach($warehouse as $row)
														{
															$selected = ($row['wh_id']==@$search_data['whc_id'])?'selected="selected"':'';
															echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
														}?>
													</select>
												</div> <?php

											}
											if($task_access == 3 && @$_SESSION['header_country_id']=='')
											{
												?>
												<!-- <label class="col-sm-1 control-label">Country</label> -->
												<div class="col-sm-3">
													<select name="country_id" class="main_status select2" >    
													<option value="">- Country -</option>
														<?php
														foreach($country_list as $row)
														{
															$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
															echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
														}?>
													</select>
												</div> <?php
											}?>
										</div>
									</div>
									<div class="row">
										<div class="header"></div>
										<table class="table table-bordered" ></table>
									</div>
									<div class="header"></div>
									<div class="table-responsive">
										<table class="table table-bordered hover">
											<thead>
												<tr >
													<th class="text-center"><strong></strong></th>
													<th class="text-center"><strong>S.NO</strong></th>
													<th class="text-center"><strong>CRB Number</strong></th>
													<th class="text-center"><strong>Warehouse</strong></th>
													<th class="text-center"><strong>Country</strong></th>
													<th class="text-center"><strong>Exp Delivery Date</strong></th>
													<th class="text-center"><strong>Exp Return Date</strong></th>
													<th class="text-center"><strong>Actions</strong></th>
												</tr>
											</thead>
											<tbody>
												<?php
												if(count($calibration_results)>0)
												{
													foreach($calibration_results as $row)
													{
														?>
														<tr>
															<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'></td>
															<td class="text-center"><?php echo $sn++;?></td>
															<td class="text-center"><?php echo $row['rcb_number'];?></td>
															<td class="text-center"><?php echo $row['wh_code'].'-'.'('.$row['name'].')';?></td>
															<td class="text-center"><?php echo $row['country'];?></td>
															<td class="text-center"><?php if($row['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$row['expected_delivery_date']));} else {echo '';}?></td>
															<td class="text-center"><?php if($row['expected_return_date']!=''){ echo date('d-m-Y',strtotime(@$row['expected_return_date']));} else {echo '';}?></td>
															<td class="text-center">
																<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View"  href="<?php echo SITE_URL.'view_qc_calibration_list/'.storm_encode($row['rc_order_id']);?>"><i class="fa fa-eye"></i></a>
																<?php
																if(@$row['status']==1)
																{
																	?>
																<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Update Calibration Details" href="<?php echo SITE_URL.'edit_admin_calibration/'.storm_encode($row['rc_order_id']);?>"><i class="fa fa-pencil"></i></a>
																<a class="btn btn-danger" style="padding:3px 3px;" data-container="body" data-placement="top" onclick="return confirm('Are you sure you want to Cancel?')"  data-toggle="tooltip" title="Remove Assets" href="<?php echo SITE_URL.'remove_admin_calibration/'.storm_encode($row['rc_order_id']);?>"><i class="fa fa-trash-o"></i></a> <?php
																} ?>											
															</td>
														</tr> 
														<?php
														if(count($row['asset_list'])>0)
														{
															?>
															<tr class="details">
	                                                            <td  colspan="9">
			                                                        <table class="table">
			                                                        	<thead>
			                                                        		<th class="text-center"><strong>CR Number</strong></th>
			                                                        		<th class="text-center"><strong>Tool Number</strong></th>
			                                                        		<th class="text-center"><strong>Tool Description</strong></th>
			                                                        		<th class="text-center"><strong>Asset Number</strong></th>
			                                                        		<th class="text-center"><strong>Serial Number</strong></th>
			                                                        		<th class="text-center"><strong>Calibration Date</strong></th>
			                                                        		<th class="text-center"><strong>Cal Due Date</strong></th>
			                                                        		<th class="text-center"><strong>CR Status</strong></th>
			                                                        		<th class="text-center"><strong>Actions</strong></th>
			                                                        	</thead>
			                                                            <tbody>
			                                                       <?php foreach(@$row['asset_list'] as $value)
			                                                        { 
			                                                        ?>
			                                                            <tr class="asset_row">
			                                                                <td align="center"><?php echo $value['rc_number']; ?></td>
			                                                                <td align="center"><?php echo $value['part_number']; ?></td>
			                                                                <td align="center"><?php echo $value['part_description']; ?></td>
			                                                                <td align="center"><?php echo $value['asset_number']; ?></td>
			                                                                <td align="center"><?php echo $value['serial_number']; ?></td>
			                                                                <td align="center"><?php echo indian_format($value['calibrated_date']); ?></td>
			                                                                <td align="center"><?php echo indian_format($value['cal_due_date']); ?></td>
			                                                                <td align="center"><?php echo $value['current_stage']; ?></td>
			                                                                <?php
			                                                                if($value['current_stage_id']==15)
			                                                                {
			                                                                	?>
			                                                                	<td align="center"><a data-container="body" data-placement="top"  data-toggle="tooltip" title="Update QA Calibration Details" class="btn btn-default" data-modal="form-primary" style="padding:3px 3px;" href="<?php echo SITE_URL.'open_qc_calibration_list/'.storm_encode($value['rc_asset_id']);?>"><i class="fa fa-pencil"></i>
			                                                                	</a></td> <?php
			                                                                }?>
			                                                            </tr>
			                                                        <?php     
			                                                        } ?>

			                                                            </tbody>
			                                                        </table>
			                                                    </td>
	                                                    	</tr><?php
														}
														else
														{
															?>
															<tr><td colspan="8" align="center"><span class="label label-primary">No Open Calibration Records</span></td></tr> <?php
														}
													}
												}
												else 
												{
												?>	<tr><td colspan="9" align="center"><span class="label label-primary">No Open Calibration Records</span></td></tr>
										<?php 	} ?>
											</tbody>
										</table>
									</div>
									<div class="row">
					                	<div class="col-sm-12">
						                    <div class="pull-left">
						                        <div class="dataTables_info" role="status" aria-live="polite">
						                            <?php echo @$pagermessage; ?>
						                        </div>
						                    </div>
						                    <div class="pull-right">
						                        <div class="dataTables_paginate paging_bootstrap_full_number">
						                            <?php echo @$pagination_links; ?>
						                        </div>
						                    </div>
					                	</div> 
					                </div>
								</div>
							</div>
						</div>
					</div>
				</form> <?php
		    }?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>