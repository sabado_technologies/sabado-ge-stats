<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" target="_blank" action="<?php echo SITE_URL;?>update_cal_print_details"  parsley-validate novalidate method="post">
							<input type="hidden" name="rc_order_id" value="<?php echo storm_encode($rc_order_id); ?>">
								<div class="form-group">
									<label for="inputName" class="col-sm-5 control-label">CRB Number :</label>
										<div class="col-sm-5">
											<p class="form-control-static"><h4><strong><?php echo $crow['rcb_number']; ?></strong></h4></p>
										</div>
								</div>
								<div class="form-group">
									<label for="inputName" class="col-sm-5 control-label">Currently Print is:</label>
										<div class="col-sm-5">
											<p class="form-control-static" style="padding-top: 10px;"><h4><strong>
											<?php 
											if($print_list['print_type']==1)
											{
												echo 'Invoice :'.$print_list['format_number'];
											}
											else
											{
												echo 'Delivery Challan :'.$print_list['format_number'];
											}	?></strong></h4></p>
										</div>
								</div>

								<div class="form-group">
									<label for="inputName" class="col-sm-5 control-label">Generated Date <span class="req-fld">*</span></label>
									<div class="col-sm-2">
										<input type="text" autocomplete="off" readonly class="form-control expected_date" data-min-view="2" name="print_date" value="<?php echo indian_format($print_list['created_time']); ?>" style="cursor:hand;background-color: #ffffff">
									</div>
								</div>
                        
						<div class="form-group no-padding"><hr>
									<div class="col-sm-7">
										<h4 class="hthin"><u>Billed From</u></h4>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-4 control-label">Billed From <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<select class="select2 billed_to" name="billed_to" required>
												<option value="">- Billed From -</option>
												<?php
												foreach($billed_to_list as $bill)
												{
													$selected = ($bill['wh_id']==@$wh_ship['billed_to'])?'selected="selected"':'';
													echo '<option value="'.$bill['wh_id'].'" '.$selected.'>'.$bill['wh_code'].' -('.$bill['name'].')</option>';
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="row"><hr style="margin-left: 18px;">
									<div class="col-sm-6">
										<div class="form-group no-padding">
											<div class="col-sm-7">
												<h4 class="hthin"><u>Shipment From</u></h4>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="add1" readonly><?php echo $billed_from['address1']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="add2" readonly><?php echo $billed_from['address2']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="add3" readonly><?php echo $billed_from['address3']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="add4" readonly><?php echo $billed_from['address4']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control" name="pin" placeholder="Pin Code" readonly value="<?php echo @$billed_from['pin_code']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">GST Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control" placeholder="GST Number" name="gst" readonly value="<?php echo @$billed_from['gst_number']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">PAN Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control" name="pan" readonly placeholder="PAN Number" value="<?php echo @$billed_from['pan_number']; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group no-padding">
											<div class="col-sm-7">
												<h4 class="hthin"><u>Shipped To</u></h4>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control add1" name="address1" required><?php echo $crow['address1']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control add2" name="address2" required><?php echo $crow['address2']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control add3" name="address3" required><?php echo $crow['address3']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control add4" name="address4" required><?php echo $crow['address4']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control pin_code" name="pin_code" placeholder="Pin Code" required value="<?php echo @$crow['pin_code']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">GST Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control gst_number" placeholder="GST Number" name="gst_number" required value="<?php echo @$crow['gst_number']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">PAN Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control pan_number" name="pan_number" required placeholder="PAN Number" value="<?php echo @$crow['pan_number']; ?>">
												</div>
											</div>
										</div>
									</div>
								</div>

                        <div class="form-group no-padding"><hr>
							<div class="col-sm-7">
								<h4 class="hthin"><u>Courier Info</u></h4>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 custom-icheck">
									<label for="inputName" class="col-sm-4 control-label">Mode Of Transport <span class="req-fld">*</span></label>
	                            	<div class="col-sm-5 custom_icheck">
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$wh_ship['courier_type']==1) { echo "checked"; } ?>" style=" position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" class="courier_type" value="1" name="courier_type" <?php if(@$wh_ship['courier_type']==1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Courier
		                                </label>
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$wh_ship['courier_type']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" <?php if(@$wh_ship['courier_type']==2) { echo "checked"; } ?> class="courier_type" value="2" name="courier_type" style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Hand
		                                </label>

		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$wh_ship['courier_type']==3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" <?php if(@$wh_ship['courier_type']==3) { echo "checked"; } ?> class="courier_type" value="3" name="courier_type" style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Dedicated
		                                </label>
	                            	</div>
								</div>
							</div>
							<div class="form-group courier_div <?php if(@$wh_ship['courier_type']==2 || @$wh_ship['courier_type']==3) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Courier Name <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control courier_name" placeholder="Courier Name" name="courier_name" value="<?php echo @$wh_ship['courier_name']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group docket_div <?php if(@$wh_ship['courier_type']==2 || @$wh_ship['courier_type']==3) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Docket/AWB No. <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control docket_num" placeholder="Docket Number" name="courier_number" value="<?php echo @$wh_ship['courier_number']; ?>">
									</div>
								</div>
							</div>

							<div class="form-group vehicle_div <?php if(@$wh_ship['courier_type']==1 || @$wh_ship['courier_type']==2) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Vehicle Number <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control vehicle_num" placeholder="Vehicle Number" name="vehicle_number" value="<?php echo @$wh_ship['vehicle_number']; ?>">
									</div>
								</div>
							</div>

							<div class="form-group contact_div <?php if(@$wh_ship['courier_type']==1 ) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Contact Person <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control contact_person" placeholder="Contact Person" name="contact_person" value="<?php echo @$wh_ship['contact_person']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group phone_div <?php if(@$wh_ship['courier_type']==1) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Phone Number <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control phone_number" placeholder="Phone Number" name="phone_number" value="<?php echo @$wh_ship['phone_number']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group expected_date_row">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Shipment Date <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input class="form-control  expected_date"  data-min-view="2" data-date-format="dd-mm-yyyy" size="16" type="text" autocomplete="off" placeholder="DD-MM-YYYY"  name="shipment_date" readonly value="<?php echo indian_format(@$wh_ship['shipment_date']); ?>" style="cursor:hand;background-color: #ffffff">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">E-Way Bill No.</label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control" placeholder="E-Way Bill Number" name="remarks" value="<?php echo @$wh_ship['remarks']; ?>">
									</div>
								</div>
							</div>
                        </div>
								<div class="form-group"><br>
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary" type="submit" name="submit" value="1" onclick="return confirm('Are you sure you want to Update Print Details?')"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL;?>closed_cal_delivery_list"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
							</form>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>