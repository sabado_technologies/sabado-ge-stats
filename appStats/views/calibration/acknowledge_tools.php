<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
						<input type="hidden" name="rc_order_id" value="<?php echo storm_encode(@$rc_order_id)?>">
						<input type="hidden" name="rc_asset_id" value="<?php echo storm_encode(@$rc_asset_id)?>">
						<input type="hidden" name="add_wh_id" value="<?php echo storm_encode(@$add_wh_id)?>">
						<div class="block-flat">
							<div class="content">
								<div class="header">
									<h5 align="center"><strong>Order Details</strong></h5>
								</div>
								<div class="row">
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>CR Number :</strong></td>
										        <td class="data-item"><?php echo @$rc_number;?></td>
											</tr>
											
											<tr>
												<td class="data-lable"><strong>Exp Delivery Date :</strong></td>
										        <td class="data-item"><?php if($order_details['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$order_details['expected_delivery_date']));} else {echo '';} ?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Supplier :</strong></td>
										        <td class="data-item"><?php echo @$supplier['supplier_code'].' - ('.@$supplier['name'].')';?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											
											<tr>
												<td class="data-lable"><strong>Asset Adding into  Warehouse :</strong></td>
												        <td class="data-item"><!-- <?php echo @$warehouse['wh_code'].' - '.'('.$warehouse['name'].')'?> -->
												        	<?php echo $wh_name; ?>
												        </td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Exp Return Date :</strong></td>
										        <td class="data-item"><?php if($order_details['expected_return_date']!=''){ echo date('d-m-Y',strtotime(@$order_details['expected_return_date']));} else {echo '';} ?></td>
											</tr>
										</tbody>
									</table>
								</div>
								</div><br>
								<form class="form-horizontal"  role="form" action="<?php echo SITE_URL.'insert_fe_delivery_asset';?>" method="post"> 
								<div class="table-responsive"> 
									<table class="table tabb">
										<thead>
											<tr>
												<th class="text-center"><strong>Serial Number</strong></th>
				                                <th class="text-center"><strong>Tool Number</strong></th>
				                                <th class="text-center" width="30%"><strong>Tool Description</strong></th>
				                                <th class="text-center"><strong>Tool Level</strong></th>
				                                <th class="text-center"><strong>Qty</strong></th>
				                              	<th class="text-center" width="32%"><strong>Tool Health</strong></th>
				                              	<th class="text-center" ><strong>Remarks</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(count(@$asset_details)>0)
											{	
												foreach(@$asset_details as $row)
												{
												?>
													<tr class="asset_row">
														<td class="text-center"><?php echo $row['serial_number']; ?></td>
														<td class="text-center"><?php echo $row['part_number']; ?></td>
														<td class="text-center"><?php echo $row['part_description']; ?></td>
														
														<td class="text-center"><?php echo $row['part_level_name']; ?></td>
														<td class="text-center"><?php echo $row['quantity']; ?></td>
														<td class="text-center">
														 <div class="col-sm-12 custom_icheck">
						                                    <label class="radio-inline"> 
						                                        <div class="iradio_square-blue <?php if(@$selected_asset_health[$row['part_id']]['asset_condition_id']==1) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
						                                            <input type="radio" class="asset_condition_id" value="1" name="asset_condition_id[<?php echo $row['part_id']; ?>]" <?php if(@$selected_asset_health[$row['part_id']]['asset_condition_id']==1) { echo "checked"; }?> style="position: absolute; opacity: 0;">
						                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
						                                        </div> 
						                                       	Good
						                                    </label>
						                                    <label class="radio-inline"> 
						                                        <div class="iradio_square-blue <?php if(@$selected_asset_health[$row['part_id']]['asset_condition_id']==2) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
						                                            <input type="radio" class="asset_condition_id" value="2" name="asset_condition_id[<?php echo $row['part_id']; ?>]" <?php if(@$selected_asset_health[$row['part_id']]['asset_condition_id']==2) { echo "checked"; }?> style="position: absolute; opacity: 0;">
						                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
						                                        </div> 
						                                        Defective
						                                    </label>
						                                    <label class="radio-inline"> 
						                                        <div class="iradio_square-blue <?php if(@$selected_asset_health[$row['part_id']]['asset_condition_id']==3) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
						                                            <input type="radio" class="asset_condition_id" value="3" name="asset_condition_id[<?php echo $row['part_id']; ?>]" <?php if(@$selected_asset_health[$row['part_id']]['asset_condition_id']==3) { echo "checked"; }?> style="position: absolute; opacity: 0;">
						                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
						                                        </div> 
						                                        Missing
						                                    </label>
														</div>
														</td>
														<td>
															<div class="textbox">
						                                   		<textarea class="form-control textarea" name="remarks[<?php echo $row['part_id']; ?>]"><?php if(@$selected_asset_health[$row['part_id']]['remarks'] != '') { echo @$selected_asset_health[$row['part_id']]['remarks']; }?></textarea>
						                                   	</div>
														</td>
													</tr>
										<?php   }
											} else {?>
												<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
				                    <?php 	} ?>
										</tbody>
									</table>
				                </div><br>
				                <div class="form-group">
									<div class=" col-md-offset-2 col-md-8">
										<label for="inputName" class="col-sm-4 control-label">Pallet Location <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<input type="text" autocomplete="off" required class="form-control" placeholder="Pallet Location" name="sub_inventory">
										</div>
									</div>
								</div>
				                <div class="form-group">
	                        		<div class="col-md-12">
			                        	<div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
											<a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a><br>
										</div>
										<div class="table-responsive col-md-offset-1 col-md-9 tab_hide">
											<table class="table hover document_table">
												<thead>
													<tr>
														<th  width="8%"><strong>Sno</strong></th>
														<th width="40%" ><strong>Document Type</strong></th>
														<th width="40%" ><strong>Supported Document</strong></th>
														<th width="8%" ><strong>Delete</strong></th>
													</tr>
												</thead>
													<tbody>
													<?php $count = 1;
			if(count($attach_document)>0)
			{
				foreach($attach_document as $doc)
				{?>	
					<tr class="attach">
						<td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
						<td class="text-center">
							<?php 
								foreach($documenttypeDetails as $docs)
								{
									if($docs['document_type_id']==@$doc['document_type_id'])
									{
										echo $docs['name'].' - ('.date('d-m-Y H:i:s',strtotime($doc['created_time'])).')'; 
									}
								}
							?>
						</td>
						<td>
	                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
						</td>
						<td class="text-center">
							<a class="btn link btn-danger btn-sm" disabled> <i class="fa fa-trash-o"></i></a>
		        		</td>
					</tr>
				 <?php		
				}
			}?>			
			<tr class="doc_row">
				<td class="text-center">
					<span class="sno"><?php echo $count; ?></span>
				</td>
				<td class="text-center">
					<select name="document_type[1]" class="form-control doc_type" > 
						<option value="">Select Document Type</option>
						<?php 
							foreach($documenttypeDetails as $doc)
							{
								echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
							}
						?>
					</select>
				</td>
				<td>
		    		<input type="file" id="document" name="support_document_1" class="document">
				</td>
				
		        <td><a <?php if(count($attach_document)>0){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
			</tr>
														</tbody>
											</table><br>
										</div>
									</div>
		                    	</div>
				                <div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary submit_btn" type="submit" onclick="return confirm('Are you sure you want to Submit?')" value="1" name="submit_fe"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL.'vendor_calibration'; ?>"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
				                </form><br>
							</div>
						</div>	
					</form>			
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>