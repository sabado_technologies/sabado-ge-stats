<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>closed_calibration"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12 form-group">
										<!-- <label class="col-sm-1 control-label">CRB No</label> -->
			                            <div class="col-sm-3">
											<input type="text" autocomplete="off" name="rcb_number" placeholder="CRB Number" value="<?php echo @$search_data['rcb_number'];?>"  class="form-control" maxlength="100">
										</div>
										
										<!-- <label class="col-sm-1 control-label">CR No</label> -->
			                            <div class="col-sm-3">
											<input type="text" autocomplete="off" name="rc_number" placeholder="CR Number" value="<?php echo @$search_data['rc_number'];?>"  class="form-control" maxlength="100">
										</div>
										<!-- <label class="col-sm-1 control-label">Asset No</label> -->
			                            <div class="col-sm-3">
											<input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>"  class="form-control" maxlength="100">
										</div>
										<div class=" col-sm-3">							
											<button type="submit" name="searchcalibration" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
											<a href="<?php echo SITE_URL.'closed_calibration'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
										</div>
									</div>
									<div class="col-sm-12 form-group">
										<div class="col-sm-3">
										    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
										</div>
										<?php
										if($task_access==2 || $task_access==3)
										{
											?>
											<!-- <label class="col-sm-1 control-label">Warehouse</label> -->
											<div class="col-sm-3">
												<select name="wh_id" class=" main_status supplier_id select2" > 
													<option value="">- Warehouse -</option>
													<?php
													foreach($warehouse as $row)
													{
														$selected = ($row['wh_id']==@$search_data['whc_id'])?'selected="selected"':'';
														echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
													}?>
												</select>
											</div> <?php

										}
										if($task_access == 3 && @$_SESSION['header_country_id']=='')
										{
											?>
											<!-- <label class="col-sm-1 control-label">Country</label> -->
											<div class="col-sm-3">
												<select name="country_id" class="main_status select2" >    <option value="">- Country -</option>
													<?php
													foreach($country_list as $row)
													{
														$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
														echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
													}?>
												</select>
											</div> <?php
										}?>
									</div>
								</div>
								<div class="row">
									<div class="header"></div>
									<table class="table table-bordered" ></table>
								</div>
								<div class="header"></div>
								<div class="table-responsive">
									<table class="table table-bordered hover">
										<thead>
											<tr >
												<th class="text-center"><strong></strong></th>
												<th class="text-center"><strong>S.NO</strong></th>
												<th class="text-center"><strong>CRB Number</strong></th>
												<th class="text-center"><strong>Warehouse</strong></th>
												<th class="text-center"><strong>Country</strong></th>
												<th class="text-center"><strong>Exp Delivery Date</strong></th>
												<th class="text-center"><strong>Exp Return Date</strong></th>
												<th class="text-center"><strong>Actions</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php
											if(count($calibration_results)>0)
											{
												foreach($calibration_results as $row)
												{
													?>
													<tr>
														<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'></td>
														<td class="text-center"><?php echo $sn++;?></td>
														<td class="text-center"><?php echo $row['rcb_number'];?></td>
														<td class="text-center"><?php echo $row['wh_code'].'-'.'('.$row['name'].')';?></td>
														<td class="text-center"><?php echo $row['country'];?></td>
														<td class="text-center"><?php if($row['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$row['expected_delivery_date']));} else {echo '';}?></td>
														<td class="text-center"><?php if($row['expected_return_date']!=''){ echo date('d-m-Y',strtotime(@$row['expected_return_date']));} else {echo '';}?></td>
														<td class="text-center">
															<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View"  href="<?php echo SITE_URL.'closed_calibration_view/'.storm_encode($row['rc_order_id']);?>"><i class="fa fa-eye"></i></a>										
														</td>
													</tr> 
													<tr class="details">
                                                        <?php
														if(count($row['asset_list'])>0)
														{?>    
															<td  colspan="8">
		                                                        <table class="table">
		                                                        	<thead>
		                                                        		<th class="text-center"><strong>CR Number</strong></th>
		                                                        		<th class="text-center"><strong>Tool Number</strong></th>
		                                                        		<th class="text-center"><strong>Tool Description</strong></th>
		                                                        		<th class="text-center"><strong>Asset Number</strong></th>
		                                                        		<th class="text-center"><strong>Serial Number</strong></th>
		                                                        		<th class="text-center"><strong>CR Status</strong></th>
		                                                        		<th class="text-center"><strong>Remarks</strong></th>
		                                                        	</thead>
		                                                            <tbody>
		                                                       <?php foreach(@$row['asset_list'] as $value)
		                                                        { 
		                                                        ?>
		                                                            <tr class="asset_row">
		                                                                <td align="center"><?php echo $value['rc_number']; ?></td>
		                                                                <td align="center"><?php echo $value['part_number']; ?></td>
		                                                                <td align="center"><?php echo $value['part_description']; ?></td>
		                                                                <td align="center"><?php echo $value['asset_number']; ?></td>
		                                                                <td align="center"><?php echo $value['serial_number']; ?></td>
		                                                                <td align="center"><?php echo $value['current_stage']; ?></td>
		                                                                <td align="center"><?php echo $value['remarks']; ?></td>
		                                                                
		                                                            </tr>
		                                                        <?php     
		                                                        } ?>

		                                                            </tbody>
		                                                        </table>
		                                                    </td> <?php
		                                                }
		                                                else
		                                                {
		                                                	?><td colspan="8" align="center"><span class="label label-primary">No closed Calibration Records</span></td> <?php
		                                                }?>
                                                    </tr> <?php
												}
											}
											else 
											{
											?>	<tr><td colspan="9" align="center"><span class="label label-primary">No closed Calibration Records</span></td></tr>
									<?php 	} ?>
										</tbody>
									</table>
								</div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>