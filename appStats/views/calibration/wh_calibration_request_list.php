<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'wh_calibration';?>">
					<div class="row">
						<div class="col-sm-12 form-group">
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="crb_number" placeholder="CRB Number" value="<?php echo @$search_data['crb_number'];?>" class="form-control">
							</div>
							<div class="col-sm-3">
								<select name="supplier_id" class="select2" > 
									<option value="">- Supplier -</option>
									<?php 
										foreach($supplier_list as $row)
										{
											$selected = ($row['supplier_id']==@$search_data['supplier_id'])?'selected="selected"':'';
											echo '<option value="'.$row['supplier_id'].'" '.$selected.'>'.$row['supplier_code'].'-('.$row['name'].')</option>';
										}
									?>
								</select>
							</div>
							<?php
							if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray']))
							{
								?>
								<div class="col-sm-3">
									<select name="wh_id" class=" main_status supplier_id select2" > 
										<option value="">- From Warehouse -</option>
										<?php
										foreach($warehouse as $row)
										{
											$selected = ($row['wh_id']==@$search_data['whc_id'])?'selected="selected"':'';
											echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
										}?>
									</select>
								</div> <?php

							}
							if($task_access == 3 && @$_SESSION['header_country_id']=='')
							{
								?>
								<div class="col-sm-3">
									<select name="country_id" class="main_status select2" >    <option value="">- Country -</option>
										<?php
										foreach($country_list as $row)
										{
											$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
											echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
										}?>
									</select>
								</div> <?php
							}?>
						</div>
						<div class="col-sm-12 form-group">
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="rc_number" placeholder="CR Number" value="<?php echo @$search_data['rc_number'];?>"  class="form-control" maxlength="100">
							</div>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>"  class="form-control" maxlength="100">
							</div>
							
							
							<div class="col-sm-3">
								<input class="form-control date" required size="16" type="text" autocomplete="off"  value="<?php if($search_data['ship_by_date']!=''){ 
									$date = strtotime("-2 days", strtotime($search_data['ship_by_date']));
    								$ship_by_date =  date("d-m-Y", $date); echo $ship_by_date;
    								} ?>" readonly placeholder="Ship By Date" name="ship_by_date" style="cursor:hand;background-color: #ffffff">	
							</div>
							<div class="col-sm-3" align="center">							
								<button type="submit" name="searchcalibration" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
								<a href="<?php echo SITE_URL.'wh_calibration'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
							</div>
						</div>
						<div class="col-sm-12 form-group">
							<div class="col-sm-3">
							    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="header"></div>
					</div>
				</form>
				<div class="table-responsive" style="margin-top: 10px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>CRB Number</strong></th>
                                <th class="text-center"><strong>Supplier</strong></th>
                                <th class="text-center"><strong>Ship By Date</strong></th>
                                <th class="text-center"><strong>From wh</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center"><strong>Action</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(count(@$calibration_results)>0)
                            {   
                                foreach(@$calibration_results as $row)
                                { 
                                ?>
                                    <tr>
                                        <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                        <td class="text-center"><?php echo $sn++; ?></td>
                                        <td class="text-center"><?php echo $row['rcb_number']; ?></td>
                                        <td class="text-center"><?php echo $row['supplier_code'].' -('.$row['supplier_name'].')'; ?></td>
                                        <td class="text-center"><?php echo indian_format($row['ship_by_date']); ?></td>
                                        <td class="text-center"><?php echo $row['wh_name']; ?></td>
                                        <td class="text-center"><?php echo $row['country']; ?></td>
                                        <td class="text-center">        
                                        	<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Proceed For Shipment"  href="<?php echo SITE_URL.'wh_calibration_wizard/'.storm_encode($row['rc_order_id']);?>"><i class="fa fa-pencil"></i></a>
                                        	<?php if($row['check_scanned_asset']>0)
	                                        { ?>
	                                        	<a class="btn btn-warning" style="padding:3px 3px;"  data-container="body" data-placement="top" data-toggle="tooltip" title="Unlink Scanned Assets"  href="<?php echo SITE_URL.'unlink_cal_scanned_asset/'.storm_encode($row['rc_order_id']);?>" onclick="return confirm('Are you sure you want to Unlink Scanned Assets?')"><i class="fa fa-chain-broken"></i></a>
	                                        <?php }?>
                                        </td>
                                    </tr>
                                <?php if(count(@$row['cal_asset'])>0)
                                    {  $slno = 1; ?>
                                    <tr class="details">
                                        <td  colspan="8">
	                                        <table class="table">
	                                        	<thead>
						                            <tr>
						                                <th class="text-center"><strong>S.No</strong></th>
						                                <th class="text-center"><strong>CR Number</strong></th>
						                                <th class="text-center"><strong>Tool Number</strong></th>
						                                <th class="text-center"><strong>Tool Description</strong></th>
						                                <th class="text-center"><strong>Asset Number</strong></th>
						                                <th class="text-center"><strong>Serial Number</strong></th>
						                                <th class="text-center"><strong>Tool Availability</strong></th>
						                            </tr>
						                        </thead>
	                                            <tbody>
	                                        <?php foreach(@$row['cal_asset'] as $value)
	                                        { 
	                                        ?>
	                                            <tr class="asset_row">
	                                                <td class="text-center"><?php echo $slno++; ?></td>
	                                                <td class="text-center"><?php echo $value['rc_number']; ?></td>
	                                                <td class="text-center"><?php echo $value['part_number']; ?></td>
	                                                <td class="text-center"><?php echo $value['part_description']; ?></td>
	                                                <td class="text-center"><?php echo $value['asset_number']; ?></td>
	                                                <td class="text-center"><?php echo $value['serial_number']; ?></td>
	                                                <td class="text-center"><?php echo get_asset_position($value['asset_id']); ?></td>
	                                            </tr>
	                                        <?php     
	                                        } ?>
												</tbody>
	                                        </table>
                                        </td>
                                    </tr><?php
                                    }
                                    else
                                    { ?>
                                        <tr><td colspan="6" align="center"><span class="label label-primary">- No Calibration Requests -</span></td></tr>
                                    <?php 
                                    }
                                }
                            
                            } else {?>
                                <tr><td colspan="8" align="center"><span class="label label-primary">- No Calibration Shipment Requests -</span></td></tr>
                    <?php   } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>