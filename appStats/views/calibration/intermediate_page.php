<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
							<div class="row">
	                        	<div class="col-md-12">
	                        		<h3 align="center">Select Warehouse, To add Asset into Inventory</h3>
	                        		<br>
	                        		<div class="form-group">
										<label for="inputName" class="col-sm-4 control-label">Warehouse <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<select class="select2" name="warehouse_id" required>
													<option value="">- Select Warehouse -</option>
													 <?php
													 if(count($warehouse_list)>0)
													 {
													 	foreach($warehouse_list as $wh)
														{
															echo '<option value="'.$wh['wh_id'].'">'.$wh['wh_code'].' -('.$wh['name'].')</option>';
														}
													 }
													?>
												</select>
											</div>
									</div>
	                        	</div>
	                        </div>
	                        <div class="form-group">
								<div class="col-sm-offset-4 col-sm-5">
									<button class="btn btn-primary" type="submit" value="1" name="SubmitWarehouse"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo @$cancel; ?>"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>