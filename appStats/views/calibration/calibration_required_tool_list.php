<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>calibration_required_tools"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12">
										<!-- <label class="col-sm-2 control-label">order Number</label> -->
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="order_no" placeholder="Order Number" value="<?php echo @$search_data['order'];?>" id="location" class="form-control" maxlength="100">
										</div>
										<!-- <label class="col-sm-2 control-label">Order Delivery Type</label> -->
										<div class="col-sm-3">
											<select name="order_type"  class="form-control supplier_id" > 
												<option value="">Order Delivery Type</option>
												<?php
												foreach($order_delivery_type as $row)
												{
													$selected="";
													if($row['order_delivery_type_id']==$search_data['order_type']){
														$selected="selected";
													}
													echo '<option value="'.$row['order_delivery_type_id'].'" '.$selected.'>'.$row['name'].'</option>';
												}?>
											</select>
										</div>
										<?php if($task_access==2 || $task_access==3)
										{
											?>
											<div class="col-sm-3">
												<select name="crt_sso_id" class="select2 main_status supplier_id" > 
													<option value="">- Select User -</option>
													<?php
													foreach($users as $row)
													{
														$selected = ($row['sso_id']==@$search_data['crt_sso_id'])?'selected="selected"':'';
														echo '<option value="'.$row['sso_id'].'" '.$selected.'>'.$row['name'].'</option>';
													}?>
												</select>
											</div> <?php
										} ?>
										<?php
										if( ($task_access==3 && @$_SESSION['header_country_id']=='' ) || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']=='') ) 
										{
											?>
											<div class="col-sm-3">
												<select name="crt_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
													<?php
													foreach($country_list as $row)
													{
														$selected = ($row['location_id']==@$search_data['crt_country_id'])?'selected="selected"':'';
														echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
													}?>
												</select>
											</div>

											 <?php
										}?>
									</div>
									<!-- <div class="col-sm-12">
										<label class="col-sm-2 control-label">Tool Description</label>
										<div class="col-sm-3">
											<input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Description" value="<?php echo @$search_data['tool_desc'];?>" id="serial_number" class="form-control"  maxlength="80">
										</div>
										<label class="col-sm-2 control-label">Asset Number</label>
										<div class="col-sm-3">
		                                    <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
										</div>
									</div> -->
								</div>
								<div class="row">
									<div class="header"></div>
									<table class="table table-bordered" ></table>
									<div class="col-sm-12">
										<div class="col-sm-4 col-md-9"></div>
										<div class="col-sm-3 col-md-3">							
											<button type="submit" name="searchcalibration" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
											<a href="<?php echo SITE_URL.'calibration_required_tools'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
										</div>
									</div>
								</div>
								<div class="ttable-responsive" style="margin-top: 10px;">
									<table class="table">
										<thead>
											<tr>
												<th width="3%"></th>
												<th class="text-center"><strong>S.NO</strong></th>
												<th class="text-center"><strong>Order Number</strong></th>
												<th class="text-center"><strong>Request Date</strong></th>
												<th class="text-center"><strong>Return Date</strong></th>
												<th class="text-center"><strong>SSO</strong></th>
												<th class="text-center"><strong>Country</strong></th>
												<!-- <th class="text-center"><strong>Actions</strong></th> -->
											</tr>
										</thead>
										<tbody>
											<?php
											if(count(@$calibration_results)>0)
											{
												foreach(@$calibration_results as $row)
												{
													?>
													<tr>
														<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
														<td class="text-center"><?php echo $sn++;?></td>
														<td class="text-center"><?php echo $row['order_number'];?></td>
														<td class="text-center"><?php echo indian_format($row['request_date']);?></td>
														<td class="text-center"><?php echo indian_format($row['return_date']);?></td>
														<td class="text-center"><?php echo $row['sso'];?></td>
														<td class="text-center"><?php echo $row['countryName'];?></td>
														<!-- <td class="text-center">
															<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Raise Pickup"  href="<?php echo SITE_URL.'raisePickupForCalibration/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>
														</td> -->
													</tr> 
													
														<tr class="details">
															<td  colspan="8">
																<table class="table">
																	<thead>
		                                                        		<th class="text-center">Asset Number</th>
		                                                        		<th class="text-center">Part Number</th>
		                                                        		<th class="text-center">Part Description</th>
		                                                        		<th class="text-center">Serial Number</th>
		                                                        		<th class="text-center">calibration Due Date</th>
		                                                        		
		                                                        	</thead>
		                                                        	<tbody>
		                                                        		<?php foreach(@$row['tool_details'] as $value)
		                                                        		{
		                                                        				?>
		                                                        				<tr class="asset_row">
			                                                        				<td class="text-center"><?php echo $value['asset_number']; ?></td>
	                                                                				<td class="text-center"><?php echo $value['part_number']; ?></td>
	                                                                				<td class="text-center"><?php echo $value['part_description']; ?></td>
	                                                                				<td class="text-center"><?php echo $value['serial_number']; ?></td>
	                                                                				<td class="text-center"><?php echo indian_format($value['cal_due_date']); ?></td>
			                                                        			</tr> <?php
		                                                        									                                                    
								                                        } ?>
		                                                        	</tbody>
																</table>
															</td>
														</tr> <?php
													
												}
											}
											else 
											{
											?>	<tr><td colspan="7" align="center"><span class="label label-primary">No Records</span></td></tr>
											<?php 	} ?>
										</tbody>
									</table>
								</div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
