<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<div class="row">
								<div class="header">
									<h4 align="center"><strong>Asset Details</strong></h4>
								</div>
								<br>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered hover">
									<thead>
										<tr>
											<th class="text-center"><strong>CR Number</strong></th>
											<th class="text-center"><strong>Tool Number</strong></th>
											<th class="text-center"><strong>Tool Description</strong></th>
											<th class="text-center"><strong>Asset Number</strong></th>
											<th class="text-center"><strong>Serial Number</strong></th>
										</tr>
									</thead>
									<tbody>
										<?php
										if(count($asset_details)>0)
										{
											foreach($asset_details as $row)
											{
												?>
												<tr>
													<td class="text-center"><?php echo $row['rc_number'];?>
														<input type="hidden" name="rc_asset_id[]" value="<?php echo $row['rc_asset_id']?>">
														<input type="hidden" name="wh_id[]" value="<?php echo $row['wh_id']?>">
													</td>
													<td class="text-center"><?php echo $row['part_number'];?></td>
													<td class="text-center"><?php echo $row['part_description'];?></td>
													<td class="text-center"><?php echo $row['asset_number'];?></td>
													<td class="text-center"><?php echo $row['serial_number'];?></td>
												</tr> <?php
											}
										}
										else 
										{
										?>	<tr><td colspan="4" align="center"><span class="label label-primary">No Records</span></td></tr>
										<?php 	} ?>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="header">
									<h4 align="center"><strong>Supplier Details</strong></h4>
								</div>							
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>CRB Number :</strong></td>
										        <td class="data-item"><?php echo @$cal_row['rcb_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Supplier :</strong></td>
										        <td class="data-item"><?php echo @$supplier['supplier_code'].' - ('.@$supplier['name'].')';?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Expeced Delivery Date :</strong></td>
										        <td class="data-item"><?php if($cal_row['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$cal_row['expected_delivery_date']));} else {echo '';} ?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Contact Person :</strong></td>
												<td class="data-item"><?php echo @$cal_row['contact_person'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address1 :</strong></td>
												<td class="data-item"><?php echo @$cal_row['address1']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address3 :</strong></td>
												<td class="data-item"><?php echo @$cal_row['address3']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Pincode :</strong></td>
												<td class="data-item"><?php echo @$cal_row['pin_code'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											
											<tr>
												<td class="data-lable"><strong>Warehouse :</strong></td>
												<td class="data-item"><?php echo @$warehouse['wh_code'].' - '.'('.$warehouse['name'].')'?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Expeced Return Date :</strong></td>
										        <td class="data-item"><?php if($cal_row['expected_return_date']!=''){ echo date('d-m-Y',strtotime(@$cal_row['expected_return_date']));} else {echo '';} ?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Phone Number :</strong></td>
												<td class="data-item"><?php echo @$cal_row['phone_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address2 :</strong></td>
										        <td class="data-item"><?php echo @$cal_row['address2'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address4 :</strong></td>
												<td class="data-item"><?php echo @$cal_row['address4']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Remarks :</strong></td>
												<td class="data-item"><?php echo @$cal_row['remarks'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<br>
							
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Attached Documents</strong></h4>
								</div>
								<br>
								<div class="table-responsive col-md-offset-1 col-md-10">
									<table class="table table-bordered hover">
										<thead>
											<tr>
												<th class="text-center"><strong>SNo.</strong></th>
												<th width="18%" class="text-center"><strong>Attached Date</strong></th>
												<th class="text-center"><strong>Document Type</strong></th>
												<th width="30%" class="text-center"><strong>Supported Document</strong></th>
											</tr>
										</thead>
										<tbody>
										<?php $count = 1;
											if(count(@$main_doc)>0)
											{	
												foreach($main_doc as $ad)
												{?>	
													<tr>
														<td class="text-center"><?php echo $count++; ?></td>
														<td class="text-center"><?php echo date('d-m-Y H:i:s',strtotime($ad['created_time'])) ?></td>
														<td class="text-center">
																<?php 
																	foreach($documenttypeDetails as $doc)
																	{
																		if($doc['document_type_id']==@$ad['document_type_id'])
																		{
																			echo $doc['name']; 
																		}
																	}
																?>
														</td>
														<td class="text-center">
					                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
														</td>
													</tr>
												 <?php		
												} 
											}
											if(count($main_doc)==0){
												?>	
												<tr><td colspan="5" class="text-center">No Attached Docs.</td></tr>
											<?php } ?>
										</tbody>
									</table><br>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>
										<a class="btn btn-primary" href="<?php echo SITE_URL.'closed_cal_delivery_list'.$current_offset;?>"><i class="fa fa-reply"></i> Back</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>