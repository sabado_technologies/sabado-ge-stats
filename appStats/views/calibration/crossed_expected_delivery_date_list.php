<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>crossed_expected_delivery_date"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12">
										<label class="col-sm-2 control-label">Order Number</label>
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="order_no" placeholder="Order Number" value="<?php echo @$search_data['tool_no'];?>" id="location" class="form-control" maxlength="100">
										</div>
										<label class="col-sm-2 control-label">Order Delivery Type</label>
										<div class="col-sm-3">
											<select name="order_type"  class="form-control supplier_id" > 
												<option value="">Order Delivery Type</option>
												<?php
												foreach($order_delivery_type as $row)
												{
													$selected="";
													if($row['order_delivery_type_id']==$search_data['order_type']){
														$selected="selected";
													}
													echo '<option value="'.$row['order_delivery_type_id'].'" '.$selected.'>'.$row['name'].'</option>';
												}?>
											</select>
										</div>
									</div>
									<!-- <div class="col-sm-12">
										<label class="col-sm-2 control-label">Tool Description</label>
										<div class="col-sm-3">
											<input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Description" value="<?php echo @$search_data['tool_desc'];?>" id="serial_number" class="form-control"  maxlength="80">
										</div>
										<label class="col-sm-2 control-label">Asset Number</label>
										<div class="col-sm-3">
		                                    <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
										</div>
									</div> -->
								</div>
								<div class="row">
									<div class="header"></div>
									<table class="table table-bordered" ></table>
									<div class="col-sm-12">
										<div class="col-sm-4 col-md-9"></div>
										<div class="col-sm-3 col-md-3">							
											<button type="submit" name="searchcalibration" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
											<a href="<?php echo SITE_URL.'crossed_expected_delivery_date'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
										</div>
									</div>
								</div>
								<div class="ttable-responsive" style="margin-top: 10px;">
									<table class="table">
										<thead>
											<tr>
												<th width="10%"></th>
												<th class="text-center"><strong>S.NO</strong></th>
												<th class="text-center"><strong>Order Number</strong></th>
												<th class="text-center"><strong>Request Date</strong></th>
												<th class="text-center"><strong>Return Date</strong></th>
												<th class="text-center"><strong>Expected Delivery Date</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php
											if(count(@$calibration_results)>0)
											{
												foreach(@$calibration_results as $row)
												{
													?>
													<tr>
														<td class="center "><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
														<td class="text-center"><?php echo $sn++;?></td>
														<td class="text-center"><?php echo $row['order_number'];?></td>
														<td class="text-center"><?php echo $row['request_date'];?></td>
														<td class="text-center"><?php echo $row['return_date'];?></td>
														<td class="text-center"><?php echo $row['exp_d_date'];?></td>
														<!-- <td class="text-center">
															<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Enter Caibration Details"  href="<?php echo SITE_URL.'calibration_asset_details/'.storm_encode($row['asset_id']).'/'.storm_encode($row['calibration_id']);?>"><i class="fa fa-pencil"></i></a>
														</td> -->
													</tr> 
													<?php if(count(@$row['expected_delivery_date'])>0)
													{
														?>
														<tr class="details">
															<td  colspan="8">
																<table class="table">
																	<thead>
		                                                        		<th>Asset Number</th>
		                                                        		<th>Part Number</th>
		                                                        		<th>Part Description</th>
		                                                        		<th>Serial Number</th>
		                                                        		<th>Part Level</th>
		                                                        	</thead>
		                                                        	<tbody>
		                                                        		<?php foreach(@$row['expected_delivery_date'] as $value)
		                                                        		{
		                                                        			?>
		                                                        				<tr class="asset_row">
			                                                        				<td ><?php echo $value['asset_number']; ?></td>
	                                                                				<td ><?php echo $value['part_number']; ?></td>
	                                                                				<td ><?php echo $value['part_description']; ?></td>
	                                                                				<td ><?php echo $value['serial_number']; ?></td>
	                                                                				<td ><?php echo $value['part_level']; ?></td>
			                                                        			</tr> <?php
		                                                        			
								                                        } ?>
		                                                        	</tbody>
																</table>
															</td>
														</tr> <?php
													}
													else
                                                    { ?>
                                                        <tr><td colspan="7" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                    <?php 
                                                    }
												}
											}
											else 
											{
											?>	<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
											<?php 	} ?>
										</tbody>
									</table>
								</div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
