<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>missed_asset"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12 form-group">
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="tool_no" placeholder="Tool Number" value="<?php echo @$search_data['ma_tool_no'];?>" id="location" class="form-control" maxlength="100">
										</div>
										<div class="col-sm-3">
											<input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Description" value="<?php echo @$search_data['ma_tool_desc'];?>" id="serial_number" class="form-control"  maxlength="80">
										</div>
                                        <div class="col-sm-3">
                                            <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['ma_asset_number'];?>" class="form-control">
                                        </div>
                                        <div class="col-sm-3 col-md-3">                         
                                            <button type="submit" name="searchtools" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                            <a href="<?php echo SITE_URL.'missed_asset'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                        </div>
									</div>
									<div class="col-sm-12 form-group">
                                        <div class="col-sm-3">
                                            <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
                                        </div>
										<?php if($task_access==2 || $task_access==3)
                                        {
                                            ?>
                                            <div class="col-sm-3">
                                                <select name="ma_wh_id" class="select2"> 
                                                    <option value="">- Select Warehouse -</option>
                                                    <?php
                                                    foreach($warehouse as $row)
                                                    {
                                                        $selected = ($row['wh_id']==@$search_data['ma_wh_id'])?'selected="selected"':'';
                                                        echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
                                                    }?>
                                                </select>
                                            </div> <?php
                                        } 
                                        if($task_access==3 && @$_SESSION['header_country_id']=='')
                                        {
                                            ?>
                                            <div class="col-sm-3">
                                                <select name="country_id" class="main_status select2" >    <option value="">- Select Country -</option>
                                                    <?php
                                                    foreach($country_list as $row)
                                                    {
                                                        $selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
                                                        echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                                    }?>
                                                </select>
                                            </div>

                                             <?php
                                        }?>
									</div>
								</div>
								<div class="row">
									<div class="header"></div>
									<table class="table table-bordered" ></table>
								</div>
								<div class="table-responsive" style="margin-top: 10px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="text-center"><strong>S.No</strong></th>
                                                <th class="text-center"><strong>Tool Number</strong></th>
                                                <th class="text-center"><strong>Tool Description</strong></th>
                                                <th class="text-center"><strong>Asset Number</strong></th>
                                                <th class="text-center"><strong>Serial Number</strong></th>
                                                <th class="text-center"><strong>Warehouse</strong></th>
                                                <th class="text-center"><strong>Country</strong></th>
                                                <th class="text-center"><strong>Action</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count(@$repair_results)>0)
                                            {   
                                                foreach(@$repair_results as $row)
                                                { 
                                                ?>
                                                    <tr>
                                                        <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                                        <td class="text-center"><?php echo $sn++; ?></td>
                                                        <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                        <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                        <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                        <td class="text-center"><?php echo $row['serial_number']; ?></td>
                                                        <td class="text-center"><?php echo $row['wh_code'].' -('.$row['warehouse'].' )'; ?></td>
                                                        <td class="text-center"><?php echo $row['country_name']; ?></td>
                                                        <td class="text-center">
                                                        	<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Action" href="<?php echo SITE_URL.'assetview_details/'.storm_encode($row['asset_id']).'/'.storm_encode($row['defective_asset_id']);?>"><i class="fa fa-pencil"></i></a>      
                                                        </td>
                                                    </tr>
                                                <?php if(count(@$row['part_list'])>0)
                                                    {  ?>
                                                        <tr class="details">
                                                            <td  colspan="9">
                                                        <table class="table">
                                                        	<thead>
                                                        		<th class="text-center"><strong>Serial Number</strong></th>
                                                        		<th class="text-center"><strong>Tool Number</strong></th>
                                                        		<th class="text-center"><strong>Tool Description</strong></th>
                                                        		<th class="text-center"><strong>Qty</strong></th>
                                                        		<th class="text-center"><strong>Tool Level</strong></th>
                                                        		<th class="text-center"><strong> Tool Health</strong></th>
                                                        		<th class="text-center"><strong>Remarks</strong></th>
                                                        	</thead>
                                                            <tbody>
                                                       <?php foreach(@$row['part_list'] as $value)
                                                        { 
                                                        ?>
                                                            <tr class="asset_row">
                                                                <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                                <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_level']; ?></td>
                                                                <td class="text-center"><?php echo $value['asset_condition']; ?>
                                                                </td>
                                                                <td class="text-center">
                                                                   <?php if($value['remarks'] != ''){
                                                                   	echo $value['remarks']; } else {
                                                                   		echo "No Remarks"; }?>
                                                                </td>
                                                            </tr>
                                                        <?php     
                                                        } ?>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr><?php
                                                    }
                                                    else
                                                    { ?>
                                                        <tr><td colspan="8" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                    <?php 
                                                    }
                                                }
                                            
                                            } else {?>
                                                <tr><td colspan="9" align="center"><span class="label label-primary">No Missed Assets Records</span></td></tr>
                                    <?php   } ?>
                                        </tbody>
                                    </table>
                                </div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>