<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>defective_asset"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12 form-group">
                                        <div class="col-sm-3">
                                            <input type="text" autocomplete="off" name="tool_no" placeholder="Tool Number" value="<?php echo @$search_data['tool_no'];?>" id="location" class="form-control" maxlength="100">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Description" value="<?php echo @$search_data['tool_desc'];?>" id="serial_number" class="form-control"  maxlength="80">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
                                        </div>
                                        <div class="col-sm-3 col-md-3">     
                                            <button type="submit" name="searchtools" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                            <a href="<?php echo SITE_URL.'defective_asset'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                        </div>
                                    </div>
									<div class="col-sm-12 form-group">
                                        <div class="col-sm-3">
                                            <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
                                        </div>
                                        <?php if($task_access == 2 || $task_access==3){?>
                                            <div class="col-sm-3 ">
                                                <select class="select2" name="wh_id" >
                                                    <option value="">- Warehouse -</option>
                                                     <?php
                                                    foreach($whList as $wh)
                                                    {
                                                        $selected = ($wh['wh_id']==$search_data['whid'])?'selected="selected"':'';
                                                        echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_code'].' -('.$wh['name'].')</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <?php } 
                                            if($task_access == 3 && $_SESSION['header_country_id']==''){?>
                                            <div class="col-sm-3 ">
                                                <select class="select2" name="country_id" >
                                                    <option value="">- Country -</option>
                                                     <?php
                                                    foreach($countryList as $country)
                                                    {
                                                        $selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
                                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <?php } ?>
                                    </div>
									</div>
								</div>
								<div class="row">
									<div class="header"></div>
								</div>
								<div class="table-responsive" style="margin-top: 10px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="text-center"><strong>S.No</strong></th>
                                                <th class="text-center"><strong>Tool Number</strong></th>
                                                <th class="text-center" ><strong>Tool Description</strong></th>
                                                <th class="text-center"><strong>Asset Number</strong></th>
                                                <th class="text-center"><strong>Serial Number</strong></th>
                                                <th class="text-center"><strong>Warehouse</strong></th>
                                                <th class="text-center"><strong>Tool Availability</strong></th>
                                                <th class="text-center"><strong>Country</strong></th>
                                                <th class="text-center" width="19%"><strong>Action</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count(@$repair_results)>0)
                                            {   
                                                foreach(@$repair_results as $row)
                                                { 
                                                    $position=get_asset_position_with_flag($row['asset_id']);
                                                    $text=$position[0];
                                                    $flag=$position[1];
                                                    // Check Whether RC Number Has Raised Or Not
                                                    $rc_check=check_for_rc_entry($row['asset_id']);
                                                ?>
                                                
                                                    <tr>
                                                        <input type="hidden" name="" value="<?php echo $flag;?>" class="asset_position">

                                                        <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'></td>
                                                        <td class="text-center"><?php echo $sn++; ?></td>
                                                        <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                        <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                        <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                        <td class="text-center"><?php echo $row['serial_number']; ?></td>
                                                        <td class="text-center"><?php echo $row['wh_code'].' -('.$row['warehouse'].' )'; ?></td>
                                                        <td class="text-center"><?php echo @$text; ?>
                                                        <td class="text-center"><?php echo $row['country_name']; ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?php if(@$flag==1 && @$rc_check == 0 && $row['check_missed']==0){?>
                                                        	<a class="btn btn-default replacement" 
                                                            style="padding:3px 3px;"  
                                                            data-container="body" 
                                                            data-placement="top" 
                                                            onclick="return confirm('Are you sure you want to Proceed for Replacement?')"  data-toggle="tooltip" 
                                                            title="Replacement"  
                                                            href="<?php echo SITE_URL.'replace_tool/'.storm_encode($row['defective_asset_id']);?>"><i class="fa fa-exchange"></i></a> 

                                                        	<a class="btn btn-info repair" 
                                                            style="padding:3px 3px;"  
                                                            data-container="body" 
                                                            data-placement="top" 
                                                            onclick="return confirm('Are you sure you want to Proceed for Repair?')" 
                                                            data-wh-id="<?php echo $row['wh_id']; ?>"  data-flag="<?php echo @$flag; ?>" 
                                                            data-toggle="tooltip" 
                                                            title="Repair"  
                                                            href="<?php echo SITE_URL.'generate_repair_process/'.storm_encode($row['defective_asset_id']);?>">
                                                            <i class="fa fa-wrench"></i></a> 

                                                            
                                                        	<a 
                                                            class="btn btn-warning scrap"   
                                                            style="padding:3px 3px;"  
                                                            data-container="body" 
                                                            data-placement="top" 
                                                            onclick="return confirm('Are you sure you want to Proceed for Scrap?')" 
                                                            data-wh-id="<?php echo $row['wh_id']; ?>" data-flag="<?php echo @$flag; ?>" 
                                                            data-toggle="tooltip" 
                                                            title="Scrap"  
                                                            href="<?php echo SITE_URL.'raise_scrap/'.storm_encode($row['defective_asset_id']);?>"><i class="fa fa-unlink"></i></a>

                                                            <?php if(@$row['cal_type_id']==1 && $row['region_id']!=2)
                                                            { ?>
                                                            <a 
                                                            class="btn btn-success calibration"   
                                                            style="padding:3px 3px;"  
                                                            data-container="body" 
                                                            data-placement="top" 
                                                            onclick="return confirm('Are you sure you want to Proceed for Calibration?')" data-wh-id="<?php echo $row['wh_id']; ?>"
                                                            data-flag="<?php echo @$flag; ?>" 
                                                            data-toggle="tooltip"
                                                            title="Re-Calibration"  
                                                            href="<?php echo SITE_URL.'generate_calibration_process/'.storm_encode($row['defective_asset_id']);?>"><i class="fa fa-compass"></i></a>
                                                            <?php }
                                                            } 
                                                            ?> 

                                                            <a 
                                                            class="btn btn-danger" 
                                                            style="padding:3px 3px;" 
                                                            data-container="body" 
                                                            data-placement="top" 
                                                            onclick="return confirm('Are you sure you want to Cancel the Defective Request?')"  data-toggle="tooltip" 
                                                            title="Cancel Defective Request"  
                                                            href="<?php echo SITE_URL.'cancel_defective_asset/'.storm_encode($row['defective_asset_id']);?>">
                                                            <i class="fa fa-trash-o"></i></a>

                                                        </td>
                                                    </tr>
                                                <?php if(count(@$row['part_list'])>0)
                                                    {  ?>
                                                        <tr class="details">
                                                            <td  colspan="10">
                                                        <table class="table">
                                                        	<thead>
                                                        		<th class="text-center">Serial No.</th>
                                                        		<th class="text-center">Tool No.</th>
                                                        		<th class="text-center">Description</th>
                                                        		<th class="text-center">Qty</th>
                                                        		<th class="text-center">Level</th>
                                                        		<th class="text-center">Health</th>
                                                        		<th class="text-center">Remarks</th>
                                                        	</thead>
                                                            <tbody>
                                                       <?php foreach(@$row['part_list'] as $value)
                                                        { 
                                                        ?>
                                                            <tr class="asset_row">
                                                                <td align="center" ><?php echo $value['serial_number']; ?></td>
                                                                <td align="center"><?php echo $value['part_number']; ?></td>
                                                                <td align="center"><?php echo $value['part_description']; ?></td>
                                                                <td align="center"><?php echo $value['quantity']; ?></td>
                                                                <td align="center"><?php echo $value['part_level']; ?></td>
                                                                <td align="center"><?php echo $value['asset_condition']; ?>
                                                                </td>
                                                                <td align="center">
                                                                   <?php if($value['remarks'] != ''){
                                                                   	echo $value['remarks']; } else {
                                                                   		echo "--"; }?>
                                                                </td>
                                                            </tr>
                                                        <?php     
                                                        } ?>

                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr><?php
                                                    }
                                                    else
                                                    { ?>
                                                        <tr><td colspan="8" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                    <?php 
                                                    }
                                                }
                                            
                                            } else {?>
                                                <tr><td colspan="10" align="center"><span class="label label-primary">No Defective Assets</span></td></tr>
                                    <?php   } ?>
                                        </tbody>
                                    </table>
                                </div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>