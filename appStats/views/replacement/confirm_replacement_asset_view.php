<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <div class="row"> 
		    	<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'submit_replacement';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
		    		<input type="hidden" name="asset_id" value="<?php echo storm_encode($asset_id);?>">
		    		<input type="hidden" name="defective_asset_id" value="<?php echo storm_encode($defective_asset_id);?>">
		    		<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">							
									<div class="header">
										<h4 align="center"><strong>Sub Component</strong></h4>
									</div>
									<div class="asset_parent">
										<div class="form-group">
			                        		<div class="col-md-12">
												<label class="col-sm-2 control-label">Part :</label>
												<div class="col-sm-4">
													<p class="form-control-static" style="margin-top: 4px;"><strong><?php echo $sub_comp['part_number'].' - '.$sub_comp['part_description']; ?></strong></p>
													<input type="hidden" name="old_part_id" value="<?php echo $sub_comp['part_id'];?>">
												</div>
			                        			<label class="col-sm-2 control-label">Old Serial Number :</label>
			                        			<div class="col-sm-4">
													<p class="form-control-static" style="margin-top: 4px;"><strong><?php echo @$sub_comp['serial_number']; ?></strong></p>
												</div>
											</div>
		                        		</div>
		                        		<div class="form-group">
			                        		<div class="col-md-12">
			                        			<label class="col-sm-2 control-label" >Quantity :</label>
												<div class="col-sm-4">
													<p class="form-control-static" style="margin-top: 4px;"><strong><?php echo $sub_comp['quantity'] ?></strong></p>
												</div>

												<label class=" col-sm-2 control-label">Old Status :</label>
												<div class="col-sm-3">
													<p class="form-control-static" style="margin-top: 4px;"><strong><?php if($sub_comp['asset_condition_id']==1){echo "Good";} elseif($sub_comp['asset_condition_id']==2){echo "Defective";}elseif($sub_comp['asset_condition_id']==3){echo "Missing";} else {echo " ";}?></strong></p>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
												<label class="col-sm-2 control-label"> New Serial Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<select name="new_part_id" required class="select2"> 
														<option value="">- Serial Number -</option>
														<?php 
															foreach($serial_number as $row)
															{
																echo '<option value="'.$row['part_id'].'">Serial : '.$row['serial_number'].'&nbsp;&nbsp;&nbsp; Asset : '.$row['asset_number'].'&nbsp;&nbsp;&nbsp; Wh : '.$row['wh_name'].'</option>';
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12">
												<label class="col-sm-2 control-label">Remarks <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="reason" required></textarea>
												</div>
											</div>
										</div>





									</div><br>
								</div>
								
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary" value="1" onclick="return confirm('Are you sure you want to Submit?')" type="submit" name="submit_tool"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL;?>defective_asset"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div><br>
							</div>
						</div>				
					</div>
		    	</form>
				
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>