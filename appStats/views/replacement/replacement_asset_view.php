<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <div class="row"> 
		    	<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<div class="header">
									<h5 align="center">Asset Number : <strong><?php echo $lrow['asset_number'];?></strong></h5>
								</div>						
							<div class="row">							
								<div class="header">
									<h5 align="center"><strong>Main Component</strong></h5>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Serial Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['serial_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Part Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['part_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Part Description :</strong></td>
										        <td class="data-item"><?php echo @$lrow['part_description'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Tool Code :</strong></td>
										        <td class="data-item"><?php echo @$lrow['tool_code'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Cost :</strong></td>
												<td class="data-item"><?php echo @$lrow['cost']?></td>
											</tr>
											
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Quantity :</strong></td>
												<td class="data-item"><?php echo @$lrow['quantity'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Status :</strong></td>
												<td class="data-item"><?php 
													if(@$m_status==1) 
													{
														echo "Good";
													}
													else if(@$m_status==2) 
													{
														echo "Defective";
													}
													else if($m_status == 3)
													{
														echo "Missing";
													}
													?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Length :</strong></td>
												<td class="data-item"><?php echo @round($lrow['length'],2);?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Breadth :</strong></td>
												<td class="data-item"><?php echo @round($lrow['breadth'],2);?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Height :</strong></td>
												<td class="data-item"><?php echo @round($lrow['height'],2);?></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
							<br>
							<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'confirm_replacement_tool';?>"  parsley-validate novalidate method="post">
		    					<input type="hidden" name="asset_id" value="<?php echo storm_encode($asset_id);?>">
		    					<input type="hidden" name="defective_asset_id" value="<?php echo storm_encode($defective_asset_id);?>">
							<?php if(count(@$subComponent)>0)
							{ ?>
							<div class="row">
								<h5 align="center"><strong>Sub Components</strong></h5>
								<div class="table-responsive">
									<table>
										<thead>
											<th width="5%" class="text-center"></th>
											<th class="text-center"><strong>Serial No. </strong></th>
											<th class="text-center"><strong>Part No. </strong></th>
											<th class="text-center"><strong>Description </strong></th>
											<th class="text-center"><strong>Qty </strong></th>
											<th class="text-center"><strong>Asset Status </strong></th>
										</thead>
										<tbody>
											<?php 
											foreach ($subComponent as $row) 
											{ ?>
												<tr>
													<td class="text-center">
														<?php 
					                        			if(($row['asset_condition_id'])==2)
					                        			{
					                        				?>
					                        				<div class="radio custom_icheck" style="padding-left: 0px; padding-top: 0px;">
							                        			<label class="control-label">
																	<div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
																		<input type="radio" class="icheck" value="<?php echo $row['tool_id'];?>" required style="position: absolute;opacity: 0;"  name="tool_id" >
																		<ins class="iCheck-helper"></ins>
																	</div>
																</label>
															</div> <?php
					                        			}
					                        			else
					                        			{	?>
					                        			-NA- <?php
					                        			}
					                        			?>
													</td>
													<td class="text-center"><?php echo $row['serial_number']; ?></td>
													<td class="text-center"><?php echo $row['part_number']; ?></td>
													<td class="text-center"><?php echo $row['part_description']; ?></td>
													<td class="text-center"><?php echo $row['quantity']; ?></td>
													<td class="text-center"><?php 
														if($row['asset_condition'] != "")
														{
															echo $row['asset_condition'];
														} 
														else
														{
															echo "Good";
														}?>
													</td>
												</tr>
									<?php
									} ?>
										</tbody>
									</table>
								</div>
							</div>
							<?php } ?>
							<br>
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-5">
									<?php if(count(@$subComponent)>0)
									{ ?>
									<button class="btn btn-primary" value="1" type="submit" name="submit_tool"><i class="fa fa-check"></i> Submit</button>
									<?php } ?>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>defective_asset"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
							</form>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>