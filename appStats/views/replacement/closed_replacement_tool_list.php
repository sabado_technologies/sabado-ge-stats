<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>closed_replacement_tool"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12 form-group">
										<!-- <label class="col-sm-2 control-label">Tool Number</label> -->
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="tool_no" placeholder="Tool Number" value="<?php echo @$search_data['tool_no'];?>" id="location" class="form-control" maxlength="100">
										</div>
										<!-- <label class="col-sm-2 control-label">Tool Description</label> -->
										<div class="col-sm-3">
											<input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Description" value="<?php echo @$search_data['tool_desc'];?>" id="serial_number" class="form-control"  maxlength="80">
										</div>
										<!-- <label class="col-sm-2 control-label">Asset Number</label> -->
										<div class="col-sm-3">
		                                    <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
										</div>
										<div class="col-sm-3 col-md-3">							
											<button type="submit" name="searchtools" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
											<a href="<?php echo SITE_URL.'closed_replacement_tool'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
										</div>
									</div>
									<div class="col-sm-12 form-group">
										
										<!-- <label class="col-sm-2 control-label">Serial Number</label> -->
										<div class="col-sm-3">
		                                    <input type="text" autocomplete="off" name="serial_number" placeholder="serial Number" value="<?php echo @$search_data['serial_number'];?>" class="form-control">
										</div>
										<?php 
                                            if($task_access == 3 && $_SESSION['header_country_id']==''){?>
                                            <div class="col-sm-3 ">
                                                <select class="select2" name="country_id" >
                                                    <option value="">- Country -</option>
                                                     <?php
                                                    foreach($countryList as $country)
                                                    {
                                                        $selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
                                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <?php } ?>
									</div>
								</div>
								<div class="row">
									<div class="header"></div>
									<table class="table table-bordered" ></table>
								</div>
								<div class="table-responsive" style="margin-top: 10px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="text-center"><strong>S.No</strong></th>
                                                <th class="text-center"><strong>Asset Number</strong></th>
                                                <th class="text-center"><strong>Changes happened At</strong></th>
                                                <th class="text-center"><strong>Reason</strong></th>
                                                <th class="text-center"><strong>Country</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count(@$closed_results)>0)
                                            {   
                                                foreach(@$closed_results as $row)
                                                { 
                                                ?>
                                                    <tr>
                                                        <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                                        <td class="text-center"><?php echo $sn++; ?></td>
                                                        <td class="text-center"><?php echo $row['old_asset_number']; ?></td>
                                                        <td class="text-center"><?php echo $row['created_time']; ?></td>
                                                        <td class="text-center">
                                                        	<?php echo $row['reason']; ?>
                                                        </td>
                                                        <td class="text-center">
                                                        	<?php echo $row['country_name']; ?>
                                                        </td>
                                                    </tr>
                                                        <tr class="details">
                                                        <td  colspan="6">
                                                        <table class="table">
                                                           <thead>
                                                           		<th class="text-center">Tool Number</th>
                                                           		<th class="text-center">Tool Description</th>
                                                           		<th class="text-center">Old Serial Number</th>
                                                           		<th class="text-center">Replaced From Asset</th>
                                                           		<th class="text-center">New Serial Number</th>
                                                          </thead>
                                                            <tbody>
	                                                            <tr class="asset_row">
	                                                                <td class="text-center"><?php echo $row['part_number']; ?></td>
	                                                                <td class="text-center"><?php echo $row['part_description']; ?></td>
	                                                                <td class="text-center"><?php echo $row['old_serial_number']; ?></td>
	                                                                <td class="text-center"><?php echo $row['new_asset_number']; ?></td>
	                                                                <td class="text-center"><?php echo $row['new_serial_number']; ?></td>
	                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            
                                            } else {?>
                                                <tr><td colspan="6" align="center"><span class="label label-primary">No Replacement Records</span></td></tr>
                                    <?php   } ?>
                                        </tbody>
                                    </table>
                                </div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>