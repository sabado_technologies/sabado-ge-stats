<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
         if($flg == 1)
            { ?>
            <div class="row"> 
                <div class="col-sm-12 col-md-12">
                    <div class="block-flat">
                        <div class="content">
                            <div class="col-sm-12 col-md-12">
                                <div class="row">                           
                                    <div class="header">
                                                <h5 align="center"><strong>Pickup Details</strong></h5>
                                            </div>
                                    <div class="col-md-6">
                                        <table class="no-border">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <td class="data-lable"><strong>Return Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['return_number'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong><u>From Address :</u></strong></td>
                                                    <td class="data-item"></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>SSO Detail :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['sso_id'].' - '.$rrow['user_name']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address1 :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address1']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address2 :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address2']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address3']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>State :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address4']?></td>
                                                </tr>
                                                <!-- <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['location_name']?></td>
                                                </tr> -->
                                                <tr>
                                                    <td class="data-lable"><strong>Pin Code :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['zip_code']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong><u>Courier Details :</u></strong></td>
                                                    <td class="data-item"></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Courier Name :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['courier_name'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Contact Person :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['contact_person'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Vehicle Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['vehicle_number'];?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="no-border">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <td class="data-lable"><strong>Expected Arrival Date :</strong></td>
                                                    <td class="data-item"><?php if($rrow['expected_arrival_date']!=""){
                                                        echo indian_format(@$rrow['expected_arrival_date']);
                                                        } ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong><u>To Address :</u></strong></td>
                                                    <td class="data-item"></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>C/o :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['to_name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address1 :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['address1']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address2 :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['address2']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['address3']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>State :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['address4']?></td>
                                                </tr>
                                                <!-- <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['location_name']?></td>
                                                </tr> -->
                                                <tr>
                                                    <td class="data-lable"><strong>Pin Code :</strong></td>
                                                    <td class="data-item"><?php echo @$trrow['pin_code']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable">.</td>
                                                    <td class="data-item"> </td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Courier Type :</strong></td>
                                                    <td class="data-item"><?php if(@$rrow['courier_type']==1)
                                                            {
                                                                echo "By Courier";
                                                            }
                                                            else if(@$rrow['courier_type']==2)
                                                            {
                                                                echo "By Hand";
                                                            }
                                                            else if(@$rrow['courier_type']==3)
                                                            {
                                                                echo "By Determined";
                                                            }
                                                            ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Docket/AWB number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['courier_number'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Phone Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['phone_number'];?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12"><br>
                                <div class="row">                           
                                    <div class="header">
                                        <h5 align="center"><strong>Acknowledged Tools</strong></h5>
                                    </div>
                                </div>
                                <div class="table-responsive" style="margin-top: 10px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="text-center"><strong>S.No</strong></th>
                                                <th class="text-center"><strong>Tool Number</strong></th>
                                                <th class="text-center"><strong>Tool Description</strong></th>
                                                <th class="text-center"><strong>Asset Number</strong></th>
                                                <th class="text-center"><strong>Status</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count(@$asset_list)>0)
                                            {   $sn = 1;
                                                foreach(@$asset_list as $row)
                                                { 
                                                ?>
                                                    <tr>
                                                        <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                                        <td class="text-center"><?php echo $sn++; ?></td>
                                                        <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                        <td class="text-center" ><?php echo $row['part_description']; ?></td>
                                                        <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                        <td class="text-center">
                                                        <?php if($asset_val[$row['asset_id']]['asset_status'] == 3)
                                                        {
                                                            echo "Not Received"; 
                                                        }
                                                        else
                                                        {
                                                            echo "Received";
                                                        }?>
                                                        </td>
                                                    </tr>
                                                <?php if(count(@$row['components'])>0)
                                                    {  ?>
                                                        <tr class="details">
                                                            <td  colspan="6">
                                                        <table class="table">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center"><strong>Serial</strong></th>
                                                                <th class="text-center"><strong>Tool Number</strong></th>
                                                                <th class="text-center"><strong>Tool Description</strong></th>
                                                                <th class="text-center"><strong>Part Level</strong></th>
                                                                <th class="text-center"><strong>Qty</strong></th>
                                                                <th class="text-center"><strong>Asset Health</strong></th>
                                                                <th class="text-center"><strong>Remarks</strong></th>
                                                            </tr>
                                                        </thead>
                                                            <tbody>
                                                       <?php foreach(@$row['components'] as $value)
                                                        { 
                                                        ?>
                                                            <tr class="asset_row">
                                                                <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                <td class="text-center" ><?php echo $value['part_description']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_level']; ?></td>
                                                                <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                                <td class="text-center">
                                                                    <?php if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 1)
                                                                    {
                                                                        echo "Good";
                                                                    }
                                                                    else if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 2)
                                                                    {
                                                                        echo "Defective";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "Missing";
                                                                    } ?>
                                                                </td>
                                                                <td class="text-center">
                                                                   <?php if($asset_val[$row['asset_id']][$value['part_id']]['remarks'] != '') { echo $asset_val[$row['asset_id']][$value['part_id']]['remarks']; }
                                                                    else{
                                                                        echo "No remarks";
                                                                        } ?>
                                                                </td>
                                                            </tr>
                                                        <?php     
                                                        } ?>

                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr><?php
                                                    }
                                                    else
                                                    { ?>
                                                        <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                    <?php 
                                                    }
                                                }
                                            
                                            } else {?>
                                                <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                    <?php   } ?>
                                        </tbody>
                                    </table>
                                </div><br>
                            </div> 
                            <div class="row">                           
                                <div class="header">
                                    <h5 align="center"><strong>Attached Documents</strong></h5>
                                </div>
                                <br>
                                <div class="table-responsive col-md-offset-1 col-md-10">
                                    <table class="table hover">
                                        <thead>
                                            <tr>
                                                <th>S No.</th>
                                                <th width="18%" class="text-center"><strong>Attached Date</strong></th>
                                                <th class="text-center"><strong>Document Type</strong></th>
                                                <th width="30%" class="text-center"><strong>Supported Document</strong></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            if(count(@$attach_document)>0)
                                            {   $count = 1;
                                                foreach($attach_document as $ad)
                                                {?> 
                                                    <tr>
                                                        <td><?php echo $count++; ?></td>
                                                        <td><?php echo date('d-m-Y H:i:s',strtotime($ad['created_time'])); ?></td>
                                                        <td align="center">
                                                                <?php 
                                                                    foreach($documenttypeDetails as $doc)
                                                                    {
                                                                        if($doc['document_type_id']==@$ad['document_type_id'])
                                                                        {
                                                                            echo $doc['name']; 
                                                                        }
                                                                    }
                                                                ?>
                                                        </td>
                                                        <td>
                                                            <a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
                                                        </td>
                                                    </tr>
                                                 <?php      
                                                } 
                                            } 
                                            else {
                                                ?>  
                                                <tr><td colspan="4" align="center">No Attached Docs.</td></tr>
                                            <?php } ?>
                                        </tbody>
                                    </table><br>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-5">
                                        <?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>
                                        <a class="btn btn-primary" href="<?php echo SITE_URL.'closed_fe_returns'.$current_offset;?>"><i class="fa fa-reply"></i> Back</a>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                    </div>              
                </div>
            </div>
	    <?php 
            }
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'closed_fe_returns';?>">
                        <div class="col-sm-12 form-group">
                            <div class="col-sm-3">
                                <input type="text" autocomplete="off" name="return_number" placeholder="Return Number" value="<?php echo @$search_data['return_number'];?>" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <?php 
                                $d['name']        = 'ssoid';
                                $d['search_data'] = @$search_data['ssoid'];
                                $this->load->view('sso_dropdown_list',$d); 
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <input class="form-control date" required size="16" type="text" autocomplete="off"
                                value="<?php if(@$search_data['date']!=''){echo date('d-m-Y',strtotime(@$search_data['date']));} ?>" readonly placeholder="Received Date" name="date" style="cursor:hand;background-color: #ffffff">  
                            </div>
                            <div class="col-sm-3">    
                                <button type="submit" name="searchpickup" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                <a href="<?php echo SITE_URL.'closed_fe_returns'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                            </div> 
                        </div>
                        <div class="col-sm-12 form-group">
                            
                            <div class="col-sm-3">
                                <input type="text" autocomplete="off" name="tool_order_number" placeholder="Order Number" value="<?php echo @$search_data['tool_order_number'];?>" class="form-control">
                            </div>
                            <?php
                            if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray']))
                            {
                                ?>
                                <div class="col-sm-3">
                                    <select name="wh_id" class=" main_status supplier_id select2" > 
                                        <option value="">- To Warehouse -</option>
                                        <?php
                                        foreach($warehouse as $row)
                                        {
                                            $selected = ($row['wh_id']==@$search_data['whc_id'])?'selected="selected"':'';
                                            echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
                                        }?>
                                    </select>
                                </div> <?php

                            }
                            if($task_access == 3 && @$_SESSION['header_country_id']=='')
                            {
                                ?>
                                <div class="col-sm-3">
                                    <select name="country_id" class="main_status select2" >    
                                        <option value="">- Country -</option>
                                        <?php
                                        foreach($country_list as $row)
                                        {
                                            $selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
                                            echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                        }?>
                                    </select>
                                </div> <?php
                            } ?>
                        </div>  
					</form>
				</div>
				<div class="table-responsive" style="margin-top: 6px;">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Return Number</strong></th>
                                <th class="text-center"><strong>Owned By</strong></th>
                                <th class="text-center"><strong>Order Number</strong></th>
                                <th class="text-center"><strong>To Warehouse</strong></th>
                                <th class="text-center"><strong>Pickup Point</strong></th>
                                <th class="text-center"><strong>Received Date</strong></th>
                                <th class="text-center"><strong>Return Type</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($closed_fe_returns_Results)>0)
							{
								foreach($closed_fe_returns_Results as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo $row['return_number']; ?></td>
                                        <td class="text-center"><?php echo $row['sso_name']; ?></td>
                                        <td class="text-center"><?php echo $row['order_number']; ?></td>
                                        <td class="text-center"><?php echo $row['wh_name']; ?></td>
                                        <td class="text-center"><?php echo $row['location_name']; ?></td>   
                                        
										<td class="text-center"><?php echo indian_format($row['received_date']); ?></td>
                                        <td class="text-center"><?php echo $row['return_type_name']; ?></td>
                                        <td class="text-center"><?php echo $row['country']; ?></td>
										<td class="text-center">
                                        <a class="btn btn-default" data-toggle="tooltip" title="View Closed Acknowledged FE Returns" style="padding:3px 3px;" href="<?php echo SITE_URL.'view_closed_fe_returns/'.storm_encode($row['return_order_id']);?>"><i class="fa fa-eye"></i></a>
                                        </td>
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="10" align="center"><span class="label label-primary">No Closed FE Returns</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>