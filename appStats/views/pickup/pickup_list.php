<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
        <div class="row wizard-row">
            <div class="col-md-12 fuelux">
                <div class="block-wizard">
                    <div id="wizard1" class="wizard wizard-ux">
                        <ul class="steps">
                            <li data-target="#step1" class="active">Pickup Tool Details<span class="chevron"></span></li>
                            <li data-target="#step2">Shipment Details<span class="chevron"></span></li>
                            <li data-target="#step3">Documentation<span class="chevron"></span></li>
                        </ul>
                        <div class="actions">
                            <button type="button" class="btn btn-xs btn-prev btn-default"> <i class="icon-arrow-left"></i>Prev</button>
                            <button type="button" class="btn btn-xs btn-next btn-default forward_btn" data-last="Finish">Next<i class="icon-arrow-right"></i></button>
                        </div>
                    </div>
                    <div class="step-content">
                        <form class="form-horizontal" target="_blank" id="fe_deliveryForm" method="post" action="<?php echo $form_action;?>" data-parsley-namespace="data-parsley-" data-parsley-validate novalidate enctype="multipart/form-data">
                        <input type="hidden" name="return_order_id" value="<?php echo storm_encode($rrow['return_order_id']); ?>">  
                        <input type="hidden" name="tool_order_id" value="<?php echo storm_encode($rrow['tool_order_id']); ?>">
                        <input type="hidden" name="rto_id" value="<?php echo storm_encode($rrow['rto_id']); ?>">
                            <div class="step-pane active" id="step1">
                                <div class="row">                           
                                    <div class="col-sm-12" style="margin-left: 10px;">
                                        <h4 class="hthin"><u>Return Tools Info</u></h4>
                                    </div>
                                    <div class="form-group" style="margin-left: 90px;">
                                        <label for="inputName" class="col-sm-2 control-label">For SSO :</label>
                                        <div class="col-sm-4">
                                            <p class="form-control-static" style="margin-top: 3px;"><strong><?php echo @$rrow['sso_id'].' - '.$rrow['user_name']?></strong></p>
                                        </div>
                                        <label for="inputName" class="col-sm-2 control-label">Return Number :</label>
                                        <div class="col-sm-4">
                                            <p class="form-control-static" style="margin-top: 3px;"><strong><?php echo @$rrow['return_number'];?></strong></p>
                                        </div>
                                    </div>
                                    <?php if(getTaskPreference("tool_return_notes",@$rrow['country_id'])) { ?>
                                    <div class="form-group" style="margin-left: 90px;">
                                        <label for="inputName" class="col-sm-2 control-label">Return Order Notes :</label>
                                        <div class="col-sm-4">
                                            <p class="form-control-static" style="margin-top: 3px;"><strong><?php echo (@$rrow['return_order_notes']=="")?"NA":@$rrow['return_order_notes'];?></strong></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                    <div class="row"><br>                         
                        <div class="col-sm-12" style="margin-left: 10px;">
                                        <h4 class="hthin"><u>Pickup Requested tools</u></h4>
                                    </div>
                        <div class="col-sm-12 col-md-12" style="margin-left: 6px;">
                            <div class="table-responsive" style="margin-top: 10px;">
                                <table class="table hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><strong>S.No</strong></th>
                                            <th class="text-center"><strong>Tool Number</strong></th>
                                            <th class="text-center"><strong>Tool Description</strong></th>
                                            <th class="text-center"><strong>Asset Number</strong></th>
                                            <th class="text-center"><strong>Serial Number</strong></th>
                                            <th class="text-center"><strong>Quantity</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if(count(@$asset_list)>0)
                                        {   $sn = 1;
                                            foreach(@$asset_list as $row)
                                            {
                                            ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $sn++; ?></td>
                                                    <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                    <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                    <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                    <td class="text-center"><?php echo $row['serial_number']; ?></td>
                                                    <td class="text-center"><?php echo $row['t_quantity']; ?></td>
                                                </tr>
                                    <?php   }
                                        } else {?>
                                            <tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                <?php   } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div>
                               
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-6"><br>
                                            <a class="btn btn-danger" href="<?php echo SITE_URL;?>pickup_list"><i class="fa fa-times"></i> Cancel</a>
                                        <button data-wizard="#wizard1" class="btn btn-primary wizard-next page1_data" value="1">Next Step <i class="fa fa-caret-right"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="step-pane" id="step2">
                                <div class="form-group no-padding">
                                    <div class="col-sm-7">
                                        <h4 class="hthin"><u>Billed From</u></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputName" class="col-sm-4 control-label">Billed From <span class="req-fld">*</span></label>
                                        <div class="col-sm-4">
                                            <select class="select2 billed_to" name="billed_to" required>
                                                <option value="">- Billed From -</option>
                                                <?php
                                                foreach($billed_to_list as $bill)
                                                {
                                                    echo '<option value="'.$bill['wh_id'].'">'.$bill['wh_code'].' -('.$bill['name'].')</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row"><hr style="margin-left: 18px;">
                                    <div class="col-sm-6">
                                        <div class="form-group no-padding">
                                            <div class="col-sm-7" style="margin-left:10px;">
                                                <h4 class="hthin"><u>Pickup From</u></h4>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control add1" name="add1" required><?php echo @$rrow['address1']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control add2" name="add2" required><?php echo @$rrow['address2']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control add3" name="add3" required><?php echo @$rrow['address3']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control add4" name="add4" required><?php echo @$rrow['address4']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <input type="text" autocomplete="off" class="form-control pin_code" name="pin1" placeholder="Pin Code" required value="<?php echo @$rrow['zip_code']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">GST Number <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control gst_number" placeholder="GST Number" name="gst1" required value="<?php if(@$rrow['gst_number']!=''){ echo  @$rrow['gst_number'];} else if(@$country_id!=2){ echo "NA";} ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">PAN Number <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pan_number" name="pan1" required placeholder="PAN Number" value="<?php if(@$rrow['pan_number']!=''){ echo  @$rrow['pan_number'];} else if(@$country_id!=2){ echo "NA";} ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group no-padding">
                                            <div class="col-sm-7">
                                                <h4 class="hthin"><u>Shipped To</u></h4>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control add11" name="address1" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?>><?php echo $trrow['address1']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control add22" name="address2" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?>><?php echo $trrow['address2']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control add33" name="address3" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?>><?php echo $trrow['address3']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control add44" name="address4" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?>><?php echo $trrow['address4']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pin_code1" name="pin_code" placeholder="Pin Code" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?> value="<?php echo @$trrow['pin_code']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">GST Number <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control gst_number1" placeholder="GST Number" name="gst_number" <?php if(@$trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?> value="<?php if(@$trrow['gst_number']!=''){ echo  @$trrow['gst_number'];} else if(@$country_id!=2){ echo "NA";} ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">PAN Number <span class="req-fld">*</span></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pan_number1" name="pan_number" <?php if(@$trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?> placeholder="PAN Number" value="<?php if(@$trrow['pan_number']!=''){ echo  @$trrow['pan_number'];} else if(@$country_id!=2){ echo "NA";} ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group no-padding"><hr>
                                    <div class="col-sm-7">
                                        <h4 class="hthin"><u>Courier Info</u></h4>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12 custom-icheck">
                                                <label for="inputName" class="col-sm-4 control-label">Mode Of Transport <span class="req-fld">*</span></label>
                                                <div class="col-sm-5 custom_icheck">
                                                    <label class="radio-inline"> 
                                                        <div class="iradio_square-blue <?php if(@$crow['courier_type']==1 || $flg == 1) { echo "checked"; } ?>" style=" position: relative;" aria-checked="true" aria-disabled="false">
                                                            <input type="radio" class="courier_type" value="1" name="courier_type" <?php if(@$crow['courier_type']==1 || $flg == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                        </div> 
                                                        By Courier
                                                    </label>
                                                    <label class="radio-inline"> 
                                                        <div class="iradio_square-blue <?php if(@$crow['courier_type']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                            <input type="radio" <?php if(@$crow['courier_type']==2) { echo "checked"; } ?> class="courier_type" value="2" name="courier_type" style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                        </div> 
                                                        By Hand
                                                    </label>
                                                    <label class="radio-inline"> 
                                                        <div class="iradio_square-blue <?php if(@$crow['courier_type']==3) { echo "checked"; } ?>" style=" position: relative;" aria-checked="true" aria-disabled="false">
                                                            <input type="radio" class="courier_type" value="3" name="courier_type" <?php if(@$crow['courier_type']==3) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                        </div> 
                                                        By Dedicated
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group courier_div">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Courier Name <span class="req-fld">*</span></label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control courier_name" placeholder="Courier Name" name="courier_name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group docket_div ">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Docket/AWB No. <span class="req-fld">*</span></label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control docket_num" placeholder="Docket Number" name="courier_number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group vehicle_div hidden">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Vehicle Number <span class="req-fld">*</span></label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control vehicle_num" placeholder="Vehicle Number" name="vehicle_number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group contact_div hidden">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Contact Person <span class="req-fld">*</span></label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control contact_person" placeholder="Contact Person" name="contact_person">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group phone_div hidden">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Phone Number <span class="req-fld">*</span></label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control phone_number" placeholder="Phone Number" name="phone_number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group expected_date_row">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">Expected Arrival Date <span class="req-fld">*</span></label>
                                                <div class="col-sm-5">
                                                    <input class="form-control  expected_date" data-min-view="2" data-date-format="dd-mm-yyyy" size="16" type="text" placeholder="DD-MM-YYYY"  name="expected_arrival_date" readonly style="cursor:hand;background-color: #ffffff">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputName" class="col-sm-4 control-label">E-Way Bill No.</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" placeholder="E-Way Bill Number" name="remarks">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-6"><br>
                                        <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Previous</button>
                                        <button class="btn btn-primary wizard-next page2_data" value="2">Next Step <i class="fa fa-caret-right"></i></button>
                                    </div>
                                </div><br><br><br>
                            </div>
                            <div class="step-pane" id="step3">
                                <div class="form-group no-padding">
                                    <div class="col-sm-7">
                                        <h4 class="hthin"><u>Attach Documents</u></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="inputName" class="col-sm-5 control-label">Transaction Type <span class="req-fld">*</span></label>
                                            <div class="col-sm-3">
                                                <select class="select2 transaction_type" required name="print_type">
                                                    <option value="">- Transaction Type -</option>
                                                    <option value="3">With In State</option>
                                                    <option value="4">Out Of State</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
                                        <a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive col-md-offset-2 col-md-8  tab_hide">
                                            <table class="table hover document_table">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="8%"><strong>Sno</strong></th>
                                                        <th class="text-center" width="40%" ><strong>Document Type</strong></th>
                                                        <th class="text-center" width="40%" ><strong>Supported Document</strong></th>
                                                        <th class="text-center" width="8%" ><strong>Delete</strong></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $count = 1;
                                                    if(count(@$docs)>0)
                                                    {   
                                                        foreach($docs as $doc)
                                                        {?> 
                                                            <tr class="attach">
                                                                <input type="hidden" name="doc_id" class="doc_id" value="<?php echo $doc['order_delivery_doc_id'];?>">
                                                                <td class="text-center"align="center"><span class="sno"><?php echo $count++; ?></span></td>
                                                                <td class="text-center" align="center">
                                                                    <?php 
                                                                        foreach($documenttypeDetails as $docs)
                                                                        {
                                                                            if($docs['document_type_id']==@$doc['document_type_id'])
                                                                            {
                                                                                echo $docs['name'].' - ('.$doc['created_time'].')'; 
                                                                            }
                                                                        }
                                                                    ?>
                                                                </td>
                                                                <td class="text-center">
                                                                    <a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
                                                                </td>
                                                                <td class="text-center">
                                                                <?php if($doc['status']==1)
                                                                { ?>
                                                                    <a class="btn link btn-danger btn-sm deactivate" > <i class="fa fa-trash-o"></i></a>
                                                                
                                                                <?php } 
                                                                else
                                                                    {?>
                                                                    <a class="btn link btn-info btn-sm activate" > <i class="fa fa-check"></i></a>
                                                                <?php } ?>
                                                                </td>
                                                            </tr>
                                                         <?php      
                                                        } 
                                                    } ?>            
                                                    <tr class="doc_row">
                                                        <td class="text-center">
                                                            <span class="sno"><?php echo $count; ?></span>
                                                        </td>
                                                        <td>
                                                            <select name="document_type[1]" class="form-control doc_type" > 
                                                                <option value="">Select Document Type</option>
                                                                <?php 
                                                                    foreach($documenttypeDetails as $doc)
                                                                    {
                                                                        echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td >
                                                            <input type="file" id="document" name="support_document_1" class="document">
                                                        </td>
                                                        
                                                        <td class="text-center"><a <?php if(@$flg == 2){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-6">
                                        <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Previous</button>
                                        <button type="submit" onclick="return confirm('Are you sure you want to Submit?')"  class="btn btn-primary submit_btn"><i class="fa fa-check"></i> Submit</button>
                                        <button type="submit" onclick="return confirm('Are you sure you want to Generate Invoice?')"  class="btn btn-primary invoice_btn hidden"><i class="fa fa-check"></i> Generate Invoice</button>
                                        <button type="submit" onclick="return confirm('Are you sure you want to Generate Delivery Challan?')"  class="btn btn-primary dc_btn hidden"><i class="fa fa-check"></i> Generate Delivery Challan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'pickup_list';?>">
                        <div class="col-sm-12 form-group">
                            <!-- <label class="col-sm-2 control-label">Return Number</label> -->
                            <div class="col-sm-3">
                                <input type="text" name="return_number" placeholder="Return Number" value="<?php echo @$search_data['return_number'];?>" class="form-control">
                            </div>
                            <!-- <label class="col-sm-2 control-label">SSO ID</label> -->
                            <div class="col-sm-3">
                                <?php 
                                $d['name']        = 'ssoid';
                                $d['search_data'] = @$search_data['ssoid'];
                                $this->load->view('sso_dropdown_list',$d); 
                                ?>
                            </div>
                            <!-- <label class="col-sm-2 control-label">Return Requested Date</label> -->
                            <div class="col-sm-3">
                                <input class="form-control date" required size="16" type="text"
                                value="<?php echo @$search_data['date']; ?>" readonly placeholder="Return Requested Date" name="date" style="cursor:hand;background-color: #ffffff">  
                            </div>
                            <div class="col-sm-3 col-md-3">    
                                <button type="submit" name="searchpickup" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                <a href="<?php echo SITE_URL.'pickup_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group">
                            <!-- <label class="col-sm-2 control-label">Order Number</label> -->
                            <div class="col-sm-3">
                                <input type="text" name="tool_order_number" placeholder="Order Number" value="<?php echo @$search_data['tool_order_number'];?>" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <select name="return_type_id" class="select2">    
                                    <option value="">- Return Type -</option>
                                    <?php
                                    foreach($return_type as $row)
                                    {
                                        $selected = ($row['return_type_id']==@$search_data['return_type_id'])?'selected="selected"':'';
                                        echo '<option value="'.$row['return_type_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                    }?>
                                </select>
                            </div>
                            <?php
                            if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray']))
                            {
                                ?>
                                <div class="col-sm-3">
                                    <select name="whid" class=" main_status supplier_id select2" > 
                                        <option value="">- Pickup By Warehouse -</option>
                                        <?php
                                        foreach($warehouse as $row)
                                        {
                                            $selected = ($row['wh_id']==@$search_data['whid'])?'selected="selected"':'';
                                            echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
                                        }?>
                                    </select>
                                </div> <?php

                            }
                            if($task_access == 3 && @$_SESSION['header_country_id']=='')
                            {
                                ?>
                                <div class="col-sm-3">
                                    <select name="country_id" class="main_status select2" >    
                                        <option value="">- Country -</option>
                                        <?php
                                        foreach($country_list as $row)
                                        {
                                            $selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
                                            echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                        }?>
                                    </select>
                                </div> <?php
                            }?>
                            
                        </div>
					</form>
				</div>
				<div class="table-responsive" style="margin-top: 10px;">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Return No.</strong></th>
                                <th class="text-center"><strong>Owned By</strong></th>
                                <th class="text-center"><strong>Order No.</strong></th>
                                <th class="text-center"><strong>Pickup By</strong></th>
                                <th class="text-center"><strong>Pickup Point</strong></th>
                                <th class="text-center"><strong>Return Requested Date</strong></th>
                                <th class="text-center"><strong>Return Type</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($pickup_Results)>0)
							{
								foreach($pickup_Results as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo $row['return_number']; ?></td>
                                        <td class="text-center"><?php echo $row['sso_name']; ?></td>
                                        <td class="text-center"><?php echo $row['order_number']; ?></td>
                                        <td class="text-center"><?php echo $row['wh_name']; ?></td>
                                        <td class="text-center"><?php echo $row['location_name']; ?></td>   
										<td class="text-center"><?php echo indian_format($row['created_time']); ?></td>
                                        <td class="text-center"><?php echo $row['return_type_name']; ?></td>
                                        <td class="text-center"><?php echo $row['country_name']; ?></td>
										<td class="text-center">
                                        <a class="btn btn-default" data-toggle="tooltip" title="Give Pickup Details" style="padding:3px 3px;" href="<?php echo SITE_URL.'view_pickup_details/'.storm_encode($row['return_order_id']);?>"><i class="fa fa-pencil"></i></a>
                                        </td>
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="10" align="center"><span class="label label-primary">No Pickup Deliveries</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>