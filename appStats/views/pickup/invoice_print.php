<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <title>Invoice Print</title>
  <link rel="shortcut icon" href="<?php echo assets_url(); ?>images/favicon_1.png" />

  <style type="text/css">
    
    body,table,td {
    margin: 0;
    padding: 0;
    font-size: 12px;
    font-family: calibri;
    }
     th, td {
        border: 1px solid black;
    }
    
    .page {
    width: 21cm;
    padding: 0cm;
    margin: 1cm auto;
    background: white;
      }
      @page {
        size: A4;
        margin: 0cm auto;
      }
      @media print 
      {
        html, body 
        {
          width: 21cm;
          height: 29.70cm;
        }
        .no-print, .no-print *
          {
              display: none !important;
          }
      }
      table {
          border-collapse: collapse;
      }
    </style>
  </head>
</head>

<body class="page">
  <div style="text-align:center" class="no-print">
    <button class="button no-print"  onclick="print_srn()" style="background-color:#3598dc;margin-top: 20px;">Print</button>
    <button class="button no-print"  onclick="close_tab()" style="background-color:#d0d0d0;margin-top: 20px;margin-left: 10px;">Close</button> 
    <h3>Please Use <span style="color: red;">Google Chrome</span> For Better Quality Print</h3>
  </div>
<table cellspacing="0" border="0" width="100%" style="border-bottom: none;">
  <tr>
    <td colspan=8 align="center">
            <img src='<?php echo assets_url()."images/wipro.png"?>' align="left" style="max-width:10%; padding:3px; margin:0px;"/>
            <img src='<?php echo assets_url()."images/ge.png"?>' align="left" style="max-width:10%; padding:3px; margin:0px;"/>
            <h3><br>TAX INVOICE (INTERNAL STOCK TRANSFER)<br>WIPRO GE HEALTHCARE PRIVATE LIMITED | WWW.WIPROGE.COM<br>Regd Office : NO 4 Kadugodi Industrial Area, Sadaramangala, Bangalore - 560067<br>Tel : 91-80-4180 1000/1224/1225  Fax : 91-80-2845 2924 </h3>
    </td>
    </tr>
  <tr>
    <td colspan=4 align="center"><b>Goods Shipped from</b></td>
    <td colspan=2  align="center"><b>Invoice Number</b></td>
    <td colspan=1  align="center"><b>Invoice date</b></td>
    <td colspan=1  align="center"><b>Total Value</b></td>
  </tr>
  <tr>
    <td colspan=2  align="center" width="25%"><b>Billed from Address</b></td>
    <td colspan=2  align="center" width="25%"><b>Dispatched from Address</b></td>
    <td colspan=2  align="center"><?php echo @$from_address['format_number']; ?></td>
    <td colspan=1  align="center"><?php echo date('d-m-Y',strtotime(@$from_address['created_time']));?></td>
    <td colspan=1  align="center"><?php echo indian_format_price(round(@$total_amount)); ?></td>
    </tr>
  <tr>
      <td colspan=2 rowspan=3 align="left" style="padding-left: 5px;">
      WIPRO GE HEALTHCARE PVT LTD<BR>C/O  <?php echo @$from_address['address1'].',<br>'.@$from_address['address2'].',<br>'.@$from_address['address3'].',<br>'.@$from_address['address4'].',<br>'.@$from_address['pin_code']; ?>
    </td>
    <td colspan=2 rowspan=3 align="left" style="padding-left: 5px;">
      WIPRO GE HEALTHCARE PVT LTD<BR>C/O  <?php echo @$from_address['address1'].',<br>'.@$from_address['address2'].',<br>'.@$from_address['address3'].',<br>'.@$from_address['address4'].',<br>'.@$from_address['pin_code']; ?>
    </td>
    <td align="center" colspan="2">Payment Terms</td>
    <td  colspan=2 align="center"><br></td>
  </tr>
  <tr>
    <td colspan=2 align="center">Tax Remarks ( If Any)</td>
    <td colspan=2 align="center"><br></td>
  </tr>
  <tr>
    <td colspan=2 align="center">PO Reference</td>
    <td colspan=2 align="center"><br></td>
    </tr>
  <tr>
    <td colspan=2 align="left">PAN NO :</td>
    <td colspan=2 align="center"><?php echo @$from_address['pan_number']; ?></td>
    <td colspan=1 align="left">Sales Order NO :</td>
    <td colspan=1 align="center" width="7%"></td>
    <td colspan=1 align="center">Customer Code</td>
    <td colspan=1 align="center"><br></td>
  </tr>
  <tr>
    <td colspan=2 align="left">GOLD CARD NO :</td>
    <td colspan=2 align="center"><br></td>
    <td colspan=1 align="left">License Validity NO :</td>
    <td colspan=1 align="center"><br></td>
    <td colspan=1 align="center">License Validity Date</td>
    <td colspan=1 align="center"><br></td>
    </tr>
  <tr>
    <td colspan=2 align="left">CIN NO :</td>
    <td colspan=2 align="center"></td>
    <td colspan=4 align="left"><br></td>
  </tr>
  <tr>
    <td colspan=2 align="left" style="border-bottom: none;">GSTIN NO :</td>
    <td colspan=2 align="center" style="border-bottom: none;"><?php echo @$from_address['gst_number']; ?></td>
    <td colspan=4 align="left" style="border-bottom: none;"><br></td>
  </tr>
</table>
<table cellspacing="0" border="0" width="100%" style="border-bottom: none;">
  <tr>
    <td colspan=2 align="center" width="33%"><b>Billed From</b></td>
    <td colspan=2 align="center" width="33%"><b>Place of Supply</b></td>
    <td colspan=2 align="center" width="33%"><b>Shipped to</b></td>
    </tr>
  <tr height="90">
    <td colspan=2 align="left" style="padding-left: 5px;">
      WIPRO GE HEALTHCARE PRIVATE LIMITED<BR>C/O  <?php echo @$billed_to['address1'].',<br>'.@$billed_to['address2'].',<br>'.@$billed_to['address3'].',<br>'.@$billed_to['address4'].',<BR>'.@$billed_to['pin_code']; ?>
    </td>
    <td colspan=2 align="left" style="padding-left: 5px;">WIPRO GE HEALTHCARE PRIVATE LIMITED<BR>C/O  <?php echo @$to_address['address1'].',<br>'.@$to_address['address2'].',<br>'.@$to_address['address3'].',<br>'.@$to_address['address4'].',<BR>'.@$to_address['pin_code']; ?>
    </td>
    <td colspan=2 align="left" style="padding-left: 5px;">WIPRO GE HEALTHCARE PRIVATE LIMITED<BR>C/O  <?php echo @$to_address['address1'].',<br>'.@$to_address['address2'].',<br>'.@$to_address['address3'].',<br>'.@$to_address['address4'].',<BR>'.@$to_address['pin_code']; ?>
    </td>
  </tr>
  <tr>
    <td align="center"><b>PAN NO</b></td>
    <td align="center"><?php echo @$billed_to['pan_number']; ?></td>
    <td align="center"><b>PAN NO</b></td>
    <td align="center"><?php echo @$to_address['pan_number']; ?></td>
    <td align="center"><b>PAN NO</b></td>
    <td align="center"><?php echo @$to_address['pan_number']; ?></td>
    </tr>
  <tr>
    <td align="center" style="border-bottom: none;"><b>GSTIN NO</b></td>
    <td align="center" style="border-bottom: none;"><?php echo @$billed_to['gst_number']; ?></td>
    <td align="center" style="border-bottom: none;"><b>GSTIN NO</b></td>
    <td align="center" style="border-bottom: none;"><?php echo @$to_address['gst_number']; ?></td>
    <td align="center" style="border-bottom: none;"><b>GSTIN NO</b></td>
    <td align="center" style="border-bottom: none;"><?php echo @$to_address['gst_number']; ?></td>
  </tr>
  </table>
  <table cellspacing="0" border="0" width="100%" style="border-top: none;">
    <tr>
    <td align="center" rowspan="2" ><b>SNo.</b></td>
    <td align="center" rowspan="2" width="12%"><b>Tool Number</b></td>
    <td align="center" rowspan="2"  width="25%"><b>Tool Description</b></td>
    <td align="center" rowspan="2" width="10%"><b>HSN/SAC Code</b></td>
    <td align="center" rowspan="2"><b>Qty</b></td>
    <td align="center" rowspan="2"><b>UOM</b></td>
    <td align="center" rowspan="2"><b>Rate</b></td>
    <td align="center" rowspan="2"><b>Total</b></td>
    <td align="center" rowspan="2"><b>Discount</b></td>
    <td align="center" rowspan="2" width="10%"><b>Taxable Value</b></td>
    <td align="center" colspan="2"><b>CGST</b></td>
    <td align="center" colspan="2"><b>SGST</b></td>
    <td align="center" colspan="2" width="16%"><b>IGST</b></td>
  </tr>
  <tr>
    <td align="center">Rate</td>
    <td align="center">Amt</td>
    <td align="center">Rate</td>
    <td align="center">Amt</td>
    <td align="center">Rate</td>
    <td align="center">Amt</td>
  </tr>
  <?php $sn = 1;
  foreach ($tools_list as $key => $value) 
  { ?>
    <tr>
    <td align="center"><?php echo $sn++; ?></td>
    <td align="center"><?php echo $value['part_number']; ?></td>
    <td align="center"><?php echo $value['part_description']; ?></td>
    <td align="center"><?php echo $value['hsn_code']; ?></td>
    <td align="center"><?php echo $value['quantity']; ?></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="right"><?php echo indian_format_price(round($value['taxable_value'])); ?></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="right"><?php echo indian_format_price(round($value['gst_percent']));?></td>
    <td align="right"> <?php echo indian_format_price(round($value['tax_amount'])); ?></td>
  </tr>
    
  <?php } ?>
  <tr height="14">
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
    <td style="border-bottom: none;"></td>
  </tr>
  </table>
  <table cellspacing="0" border="0" width="100%" style="border-top: none;">
    <tr>
    <td colspan=4 align="left"><b>Comments / Remarks :</b></td>
    </tr>
  <tr>
    <td align="center" width="25%">Mode of transport</td>
    <td  align="center" width="25%">
    <?php 
    if(@$from_address['courier_type']==1)
    {
      echo "By Courier";
    }
    else if(@$from_address['courier_type']==2)
    {
      echo "By Hand";
    }
    else if(@$from_address['courier_type']==3)
    {
      echo "By Dedicated";
    } ?></td>
    <td align="center" width="25%">Airway Bill #</td>
    <td align="center" width="25%"><?php echo @$from_address['remarks']; ?></td>
    </tr>
  <tr>
    <td align="center"><?php if(@$from_address['courier_name']!=''){ echo "Transporter Name";}else if(@$from_address['vehicle_number']!=''){ echo "Vehicle No#";} ?></td>
    <td align="center"><?php if(@$from_address['courier_name']!=''){ echo @$from_address['courier_name'];}else if(@$from_address['vehicle_number']!=''){ echo @$from_address['vehicle_number'];} ?></td>
    <td align="center">Docket#</td>
    <td align="center">
    <?php echo @$from_address['courier_number']; ?></td>
  </tr>
  <tr>
    <td align="center">SR#</td>
    <td align="center"></td>
    <td align="center">Return Order#</td>
    <td align="center">
    <?php echo @$from_address['return_number']; ?></td>
    </tr>
  <tr>
    <td align="center">FE Name</td>
    <td align="center"><?php echo @$from_address['sso_name'];?></td>
    <td align="center">FE Contact#</td>
    <td align="center"><?php echo @$from_address['mobile_no'];?></td>
    </tr>
  <tr>
    <td align="center">Contact/Driver Name</td>
    <td align="center"><?php echo @$from_address['contact_person']; ?></td>
    <td align="center">Vendor Contact#</td>
    <td align="center"><?php echo @$from_address['phone_number']; ?></td>
  </tr>
  <tr>
    <td align="center">Freight &amp; insurance</td>
    <td align="center"></td>
    <td align="center">whether reverse charge applicable</td>
    <td align="center"></td>
  </tr>
  <tr>
    <td align="center">LC Free text</td>
    <td align="center"></td>
    <td align="center">Hypotheticated to</td>
    <td align="center"></td>
  </tr>
  <tr>
    <td colspan=1 align="center" style="border-bottom: none">Asset Number#</td>
    <td colspan=3 align="left" style="border-bottom: none">
      <?php $inc = 1;
      foreach ($tools_list as $key => $value) 
      {
        echo $inc++.'-('.$value['asset_number_list'].'), ';
      } ?>
    </td>
  </tr>
  </table>
<table cellspacing="0" border="0" width="100%" style="border-top: none;">
  <tr height="21">
    <td colspan=1  align="center" width="11%"><b>Value In <?php echo $currency_name; ?> :</b></td>
    <td colspan=3 align="left"><div style="margin-left:3px; "><?php echo ucwords(convert_number_to_words(round(@$total_amount))); ?></div></td>
    <td colspan=2  align="center" width="22%"><b>Total Value : <?php echo $currency_name.' '.indian_format_price(round(@$total_amount)); ?></b></td>
    </tr>
  <tr>
    <td colspan=2 align="center"><b>Our Bank Details</b></td>
    <td colspan=2 align="center"><b>Payment Details/Cheques DD to be sent to</b></td>
    <td colspan=2 align="center"><b>Enquiry Details</b></td>
    </tr>
  <tr>
    <td align="center">Bank Name</td>
    <td align="center">HSBC Corp Limited</td>
    <td align="center">Attention</td>
    <td align="center">GEHC Collection team</td>
    <td align="center" rowspan=3>HelpLine Numbers</td>
    <td align="center" rowspan=3>91-80-4180 1000<br>91-80-4180 1224<br>91-80-4180 1225</td>
  </tr>
  <tr>
    <td colspan=1 align="center">Account #</td>
    <td colspan=1 align="center">071-062327-002</td>
    <td colspan=1 align="center" rowspan=3>Address</td>
    <td colspan=1 align="center" rowspan=3>
      Regd Office : NO 4, Kadugodi Industrial Area, Sadaramangala, Whitefield 560067<br>Tel : 91-80-4180 1000<br>Fax : 91-80-28452924
    </td>
  </tr>
  <tr>
    <td colspan=1 align="center">Address</td>
    <td colspan=1 align="center">#7 MG Road Bangalore 560011 India</td>
  </tr>
  <tr>
    <td colspan=1 align="center">IFSC/RTGS Code</td>
    <td colspan=1 align="center">HSBC01INDIA / HSBC0560002</td>
    <td colspan=1 align="center">Email</td>
    <td colspan=1 align="center">WGE.Support@ge.com</td>
    </tr>
  <tr>
    <td colspan=6 align="center" style="border-bottom: none;">Overdue Interest @ 24% per annum should be paid if payment is not made on due date. Payment of our bills may be made to our corporate office or to our nearest Branch Office. Branches at ' Ahmedabad, Bangalore, Bhopal, Kolkata, Chandigarh, Chennai, Cochin,  Jaipur, Lucknow, Mumbai, New Delhi, Patna, Pune, Secundrabad. As a Policy we do not want to accept any payment of cash. Further all payments upto RS 50,000/- must be paid by cheque &amp; not by Demand Draft. Cheques are subjected to realisation.<br>We solocit your co-operation in this regard.</td>
  </tr>
</table>
<table>
  <tr>
    <td colspan=6 align="left"><b>STANDARD TERMS AND CONDITIONS OF SALE</b></td>
  </tr>
  <tr>
    <td align="center" width="7%">1</td>
    <td colspan=5 align="left">It is Essential that you notify our nearest branch office of any damage or short receipt of supplies within seven(7) days of its arrival at destination. We will not accept any responsibility for any damage or short receipt of supplies notified after seven(7) days from its arrival at destination</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td colspan=5 align="left">Every care has been taken to pack the materials securely, should you observe any outward damages to the packages, please ensure that you take &quot;Open Delivery&quot; and simultaneously lodge claim with the carriers</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td colspan=5 align="left">We are not liable for any consequestial damages which may arise during the course of usage of the equipment</td>
  </tr>
  <tr>
    <td align="center">4</td>
    <td colspan=5 align="left">Terms and Conditions of Warranty per purchase order/sale contract form an integral part of this invoice</td>
  </tr>
  <tr>
    <td align="center">5</td>
    <td colspan=5 align="left">Other terms and conditions as per quotation/purchase order/sale contract and other Wipro GE Healthcare Private limited documents shall apply</td>
  </tr>
  <tr>
    <td colspan=3 align="center" width="50%"><b>GST Certificate</b></td>
    <td colspan=3 align="center"><b>For Wipro GE Healthcare Private Limited</b></td>
    </tr>
  <tr>
    <td colspan=3 align="center">We hereby certify that our GST numbers as mentioned above are in force on the date of which the sale of the goods covered in this invoice is made by us and that the transcation of sale covered by this invoice has been affected by us in the course of our business.</td>
    <td colspan=3 align="center" valign="bottom"><b>Authorised Signatory</b></td>
  </tr>
  <tr>
    <td colspan=3 align="center"><b>I certify that the satetements contained in this invoice are true and correct.<br>Electronic Reference number :</b></td>
    <td colspan=3 align="center"><b>ORIGINAL COPY</b></td>
    </tr>
</table>
</body>
</html>
<script type="text/javascript">
    function print_srn()
    {
        window.print(); 
    }
    function close_tab()
    {
        close(); 
    }
</script>