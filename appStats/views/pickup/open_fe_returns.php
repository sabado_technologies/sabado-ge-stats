<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
         if($flg ==2)
            { ?>
            <div class="row"> 
                <div class="col-sm-12 col-md-12">
                    <div class="block-flat">
                        <div class="content">
                            <form method="post" action="<?php echo $form_action;?>" id="form_submit" parsley-validate novalidate class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="return_order_id" value="<?php echo storm_encode($rrow['return_order_id']); ?>">  
                            <input type="hidden" name="tool_order_id" value="<?php echo storm_encode($rrow['tool_order_id']); ?>">
                            <input type="hidden" name="rto_id" value="<?php echo storm_encode($rrow['rto_id']); ?>">    
                                <div class="col-sm-12 col-md-12">                           
                                    <div class="header">
                                        <h5 align="center"><strong>Pickup Details</strong></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="no-border">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <td class="data-lable"><strong>Return Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['return_number'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Expected Arrival Date :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['expected_arrival_date'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Address1 :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address1'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>City :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address3'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Pin Code :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['zip_code'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Courier Name :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['courier_name'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Contact Person :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['contact_person'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Vehicle Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['vehicle_number'];?></td>
                                                </tr>
                                                <?php if(getTaskPreference("tool_return_notes",@$rrow['country_id'])) { ?>
                                                <tr>
                                                    <td class="data-lable"><strong>Return Order Notes :</strong></td>
                                                    <td class="data-item"><?php echo (@$rrow['return_order_notes']=="")?"NA":@$rrow['return_order_notes'];?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="no-border">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <td class="data-lable"><strong>Order Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['order_number']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>SSO Detail :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['sso_id'].' - '.$rrow['user_name']?></td>
                                                </tr>
                                               
                                                <tr>
                                                    <td class="data-lable"><strong>Address2 :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address2'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>State :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['address4'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Courier Type :</strong></td>
                                                    <td class="data-item"><?php if(@$rrow['courier_type']==1)
                                                            {
                                                                echo "By Courier";
                                                            }
                                                            else if(@$rrow['courier_type']==2)
                                                            {
                                                                echo "By Hand";
                                                            }
                                                            else if(@$rrow['courier_type']==3)
                                                            {
                                                                echo "By Determined";
                                                            }
                                                            ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Docket/AWP number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['courier_number'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Phone Number :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['phone_number'];?></td>
                                                </tr>
                                                <tr>
                                                    <td class="data-lable"><strong>Remarks :</strong></td>
                                                    <td class="data-item"><?php echo @$rrow['remarks'];?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12"><br>
                                    <div class="row">                           
                                        <div class="header">
                                            <h5 align="center"><strong>Acknowledge Returned Tools</strong></h5>
                                        </div>
                                    </div>
                                    <div class="table-responsive" style="margin-top: 10px;">
                                        <table class="table scan_table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"></th>
                                                    <th class="text-center"><strong>S.No</strong></th>
                                                    <th class="text-center"><strong>Tool Number</strong></th>
                                                    <th class="text-center"><strong>Tool Description</strong></th>
                                                    <th class="text-center"><strong>Asset Number</strong></th>
                                                    <th class="text-center"><strong>Serial Number</strong></th>
                                                    <th class="text-center" width="27%"><strong>Acknowledge Status</strong></th>
                                                    <th class="text-center"><strong>Pallet Location</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if(count(@$asset_list)>0)
                                                {   $sn = 1;
                                                    foreach(@$asset_list as $row)
                                                    { 
                                                    ?>
                                                        <tr id="<?php echo 'main'.$row['asset_id']; ?>">
                                                            <td class="text-center">
                                                                <a class="btn btn-warning md-trigger scan_qr" data-toggle="modal" data-target="#form-primary" href="#" data-asset-number="<?php echo $row['asset_number']; ?>" data-asset-id="<?php echo $row['asset_id']; ?>"><i class="fa fa-qrcode" ></i> Scan</a>
                                                            </td>
                                                            <td class="text-center"><?php echo $sn++; ?></td>
                                                            <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                            <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                            <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                            <td class="text-center"><?php echo $row['serial_number']; ?></td>
                                                            <td class="text-center">
                                                                <div class="col-sm-12 custom_icheck">
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_status" value="1" name="asset_status[<?php echo $row['asset_id'] ?>]" checked style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Received
                                                                </label>
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_status as2" value="3" name="asset_status[<?php echo $row['asset_id'] ?>]" style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Not Received
                                                                </label>
                                                                </div>
                                                                <td class="text-center">
                                                                    <input type="text" autocomplete="off" name="sub_inventory[<?php echo $row['asset_id'];?>]" value="">
                                                                </td>
                                                            </td>
                                                        </tr>
                                                    <?php if(count(@$row['components'])>0)
                                                        {  ?>
                                                            <tr class="details" id="<?php echo 'show'.$row['asset_id']; ?>">
                                                                <td  colspan="8">
                                                            <table class="table tool_list">
                                                                <thead>
                                                                    <th class="text-center"><strong>Serial Number</strong></th>
                                                                    <th class="text-center"><strong>Tool Number</strong></th>
                                                                    <th class="text-center"><strong>Description</strong></th>
                                                                    <th class="text-center"><strong>Tool Level</strong></th>
                                                                    <th class="text-center"><strong>Qty</strong></th>
                                                                    <th class="text-center"><strong>Tool Health</strong></th>
                                                                    <th class="text-center"><strong>Remarks</strong></th>
                                                                </thead>
                                                                <tbody>
                                                           <?php foreach(@$row['components'] as $value)
                                                            { 
                                                            ?>
                                                                <tr class="asset_row">
                                                                    <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_level']; ?></td>
                                                                    <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                                    <td width="32%" class="text-center">
                                                                        <div class="custom_icheck">
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 1) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="1" name="asset_condition_id[<?php echo $row['asset_id']; ?>][<?php echo $value['part_id']; ?>]" <?php if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Good
                                                                        </label>
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="2" name="asset_condition_id[<?php echo $row['asset_id']; ?>][<?php echo $value['part_id']; ?>]" <?php if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Defective
                                                                        </label>
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="3" name="asset_condition_id[<?php echo $row['asset_id']; ?>][<?php echo $value['part_id']; ?>]" <?php if($asset_val[$row['asset_id']][$value['part_id']]['asset_condition_id'] == 3) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Missing
                                                                        </label>
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-center"> 
                                                                        <div class="textbox">
                                                                            <textarea class="form-control textarea" name="remarks[<?php echo $row['asset_id']; ?>][<?php echo $value['part_id']; ?>]"><?php if($asset_val[$row['asset_id']][$value['part_id']]['remarks'] != '') { echo $asset_val[$row['asset_id']][$value['part_id']]['remarks']; } ?></textarea>  
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php     
                                                            }
                                                             ?>
                                                                <tr><td colspan="6" class="text-center"></td>
                                                                    <td class="text-center"><a class="btn btn-info save_point" value="1"><i class="fa fa-check"></i> Save</a></td></tr>


                                                                </tbody>
                                                            </table>
                                                            </td>
                                                        </tr><?php
                                                        }
                                                        else
                                                        { ?>
                                                            <tr><td colspan="5" class="text-center"><span class="label label-primary">No Records Found</span></td></tr>
                                                        <?php 
                                                        }
                                                    }
                                                
                                                } else {?>
                                                    <tr><td colspan="5" class="text-center"><span class="label label-primary">No Records Found</span></td></tr>
                                        <?php   } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                                <div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;"><br><br>
                                        <a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive col-md-offset-2 col-md-8  tab_hide">
                                            <table class="table hover document_table">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="8%"><strong>Sno</strong></th>
                                                        <th class="text-center" width="40%" ><strong>Document Type</strong></th>
                                                        <th class="text-center" width="40%" ><strong>Supported Document</strong></th>
                                                        <th class="text-center" width="8%" ><strong>Delete</strong></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $count = 1;
                                                    if(count(@$docs)>0)
                                                    {   
                                                        foreach($docs as $doc)
                                                        {?> 
                                                            <tr class="attach">
                                                                <input type="hidden" name="doc_id" class="doc_id" value="<?php echo $doc['return_doc_id'];?>">
                                                                <td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
                                                                <td class="text-center">
                                                                    <?php 
                                                                        foreach($documenttypeDetails as $docs)
                                                                        {
                                                                            if($docs['document_type_id']==@$doc['document_type_id'])
                                                                            {
                                                                                echo $docs['name'].' - ('.$doc['created_time'].')'; 
                                                                            }
                                                                        }
                                                                    ?>
                                                                </td>
                                                                <td class="text-center">
                                                                    <a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
                                                                </td>
                                                                <td class="text-center">
                                                                <?php if($doc['status']==1)
                                                                { ?>
                                                                    <a class="btn link btn-default btn-sm deactivate" data-toggle="tooltip" title="Info"> <i class="fa fa-eye"></i></a>
                                                                
                                                                <?php } 
                                                                else
                                                                    {?>
                                                                    <a class="btn link btn-info btn-sm activate" > <i class="fa fa-check"></i></a>
                                                                <?php } ?>
                                                                </td>
                                                            </tr>
                                                         <?php      
                                                        } 
                                                    } ?>            
                                                    <tr class="doc_row">
                                                        <td class="text-center">
                                                            <span class="sno"><?php echo $count; ?></span>
                                                        </td>
                                                        <td class="text-center">
                                                            <select name="document_type[1]" class="form-control doc_type" > 
                                                                <option value="">Select Document Type</option>
                                                                <?php 
                                                                    foreach($documenttypeDetails as $doc)
                                                                    {
                                                                        echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="file" id="document" name="support_document_1" class="document">
                                                        </td>
                                                        
                                                        <td class="text-center"><a <?php if(@$docs){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>      
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-6"><br>
                                        <button class="btn btn-primary ack_submit_btn hidden" onclick="return confirm('Are you sure you want to Submit?')"  type="submit" value="1" name="submit_branch"><i class="fa fa-check"></i> Submit</button>
                                        <a class="btn btn-danger" href="<?php echo SITE_URL;?>open_fe_returns"><i class="fa fa-times"></i> Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>              
                </div>
            </div>
            <input type="hidden" name="hidden_asset_id" id="hidden_asset_id">
            <input type="hidden" name="hidden_asset_number" id="hidden_asset_number">
            <input type="hidden" name="asset_total" class="asset_total" value="<?php echo count($asset_list); ?>">
            <div class="modal fade colored-header" id="form-primary">
                <div class="modal-dialog" style="margin-top: 200px;padding-bottom: 0px;margin-bottom: 0px;width: 600px;height: 200px;">
                    <div class="md-content" style="height: 260px !important;width:500px;">
                        <div class="modal-header">
                            <h3>Scan Asset QR Code</h3>
                        </div>
                        <div class="modal-body" style="overflow: hidden !important;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Asset Number <span class="req-fld">*</span></label> 
                                        <div class="col-sm-8 asset_display">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Scan QR Code <span class="req-fld">*</span></label> 
                                        <div class="col-sm-8">
                                            <input type="text" autocomplete="off" name="asset_number" id="asset_number" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group"> 
                                        <div class="col-sm-offset-4 col-sm-8">
                                            <input type="checkbox" name="vehicle" value="0" class="checkbox_val"> Enable Manual Entry
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 submit_action hidden" style="margin-top: 10px;">
                                    <div class="form-group"> 
                                        <div class="col-sm-offset-4 col-sm-8">
                                            <button class="btn btn-primary submitModal" type="submit" value="1" name="submitModal"><i class="fa fa-check"></i> Submit</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    <?php 
            }
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'open_fe_returns';?>">
                            <div class="col-sm-12 form-group">
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="return_number" placeholder="Return Number" value="<?php echo @$search_data['return_number'];?>" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <?php 
                                    $d['name']        = 'ssoid';
                                    $d['search_data'] = @$search_data['ssoid'];
                                    $this->load->view('sso_dropdown_list',$d); 
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="tool_order_number" placeholder="Order Number" value="<?php echo @$search_data['tool_order_number'];?>" class="form-control">
                                </div>
                                <div class="col-sm-3">    
                                    <button type="submit" data-toggle="tooltip" title="Search" name="searchpickup" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
                                    <a href="<?php echo SITE_URL.'open_fe_returns'; ?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>
                                    <button type="submit" data-toggle="tooltip" title="Download" onclick="return confirm('Are you sure you want to download?')" name="download_open_fe_returns" value="1" formaction="<?php echo SITE_URL.'download_open_fe_returns';?>" class="btn btn-success"><i class="fa fa-cloud-download"></i></button> 
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <div class="col-sm-3">
                                    <input class="form-control date" required size="16" type="text" autocomplete="off"
                                    value="<?php if(@$search_data['date']!=''){ echo date('d-m-Y',strtotime(@$search_data['date']));} ?>" readonly placeholder="Expected Arrival Date" name="date" style="cursor:hand;background-color: #ffffff">  
                                </div>
                                <?php
                                if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray']))
                                {
                                    ?>
                                    <div class="col-sm-3">
                                        <select name="wh_id" class=" main_status supplier_id select2" > 
                                            <option value="">- To Warehouse -</option>
                                            <?php
                                            foreach($warehouse as $row)
                                            {
                                                $selected = ($row['wh_id']==@$search_data['whc_id'])?'selected="selected"':'';
                                                echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
                                            }?>
                                        </select>
                                    </div> <?php

                                }
                                if($task_access == 3 && @$_SESSION['header_country_id']=='')
                                {
                                    ?>
                                    <div class="col-sm-3">
                                        <select name="country_id" class="main_status select2" >    
                                            <option value="">- Country -</option>
                                            <?php
                                            foreach($country_list as $row)
                                            {
                                                $selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
                                                echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                            }?>
                                        </select>
                                    </div>
                                <?php } ?>
                            </div>
					</form>
				</div>
				<div class="table-responsive" style="margin-top: 6px;">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Return Number</strong></th>
                                <th class="text-center"><strong>Owned By</strong></th>
                                <th class="text-center"><strong>Order Number</strong></th>
                                 <th class="text-center"><strong>To Warehouse </strong></th>
                                <th class="text-center"><strong>Pickup Point</strong></th>
                                <th class="text-center"><strong>Expected Arrival Date</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($open_fe_returns_Results)>0)
							{
								foreach($open_fe_returns_Results as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo $row['return_number']; ?></td>
                                        <td class="text-center"><?php echo $row['sso_name']; ?></td>
                                        <td class="text-center"><?php echo $row['order_number']; ?></td>
                                        <td class="text-center"><?php echo $row['wh_name']; ?></td>
                                        <td class="text-center"><?php echo $row['location_name']; ?></td>   
										<td class="text-center"><?php echo indian_format($row['expected_arrival_date']); ?></td>
                                        <td class="text-center"><?php echo $row['country']; ?></td>
										<td class="text-center">
                                        <a class="btn btn-default" data-toggle="tooltip" title="Ack FE Return Tools" style="padding:3px 3px;" href="<?php echo SITE_URL.'acknowledge_fe_return_tools/'.storm_encode($row['return_order_id']);?>"><i class="fa fa-pencil"></i></a>
                                        </td>
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="9" align="center"><span class="label label-primary">No Open FE Returns</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>