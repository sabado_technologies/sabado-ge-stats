<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" target="_blank" action="<?php echo SITE_URL;?>update_pickup_print_details"  parsley-validate novalidate method="post">
							<input type="hidden" name="return_order_id" value="<?php echo storm_encode($ro['return_order_id']); ?>">
								<div class="form-group">
									<label for="inputName" class="col-sm-5 control-label">Return Number :</label>
										<div class="col-sm-5">
											<p class="form-control-static"><h4><strong><?php echo $ro['return_number']; ?></strong></h4></p>
										</div>
								</div>
								<div class="form-group">
									<label for="inputName" class="col-sm-5 control-label">Currently Print is:</label>
										<div class="col-sm-5">
											<p class="form-control-static" style="padding-top: 10px;"><h4><strong>
											<?php 
											if($print_type==1)
											{
												echo 'Invoice :'.$format_number;
											}
											else
											{
												echo 'Delivery Challan :'.$format_number;
											}	?></strong></h4></p>
										</div>
								</div>

								<div class="form-group">
									<label for="inputName" class="col-sm-5 control-label">Generated Date <span class="req-fld">*</span></label>
									<div class="col-sm-2">
										<input type="text" autocomplete="off" readonly class="form-control expected_date" data-min-view="2" name="print_date" value="<?php echo indian_format($print_date); ?>" style="cursor:hand;background-color: #ffffff">
									</div>
								</div>

						 <div class="form-group no-padding"><hr>
                            <div class="col-sm-7">
                                <h4 class="hthin"><u>Billed From</u></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="inputName" class="col-sm-4 control-label">Billed From <span class="req-fld">*</span></label>
                                <div class="col-sm-3">
                                    <select class="form-control billed_to" name="billed_to" required>
                                        <option value="">- Billed From -</option>
                                        <?php
                                        foreach($billed_to_list as $bill)
                                        {
                                        	$selected = ($bill['wh_id']==@$ro['billed_to'])?'selected="selected"':'';
                                            echo '<option value="'.$bill['wh_id'].'" '.$selected.'>'.$bill['wh_code'].' -('.$bill['name'].')</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row"><hr style="margin-left: 18px;">
                            <div class="col-sm-6">
                                <div class="form-group no-padding">
                                    <div class="col-sm-7" style="margin-left:10px;">
                                        <h4 class="hthin"><u>Shipment From</u></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control add1" name="add1" required><?php echo @$rrow['address1']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control add2" name="add2" required><?php echo @$rrow['address2']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control add3" name="add3" required><?php echo @$rrow['address3']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control add4" name="add4" required><?php echo @$rrow['address4']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pin_code" name="pin1" placeholder="Pin Code" required value="<?php echo @$rrow['zip_code']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">GST Number <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control gst_number" placeholder="GST Number" name="gst1" required value="<?php echo @$rrow['gst_number']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">PAN Number <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pan_number" name="pan1" required placeholder="PAN Number" value="<?php echo @$rrow['pan_number']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group no-padding">
                                    <div class="col-sm-7">
                                        <h4 class="hthin"><u>Shipped To</u></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="address1" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?>><?php echo $trrow['address1']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="address2" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?>><?php echo $trrow['address2']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="address3" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?>><?php echo $trrow['address3']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="address4" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?>><?php echo $trrow['address4']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="pin_code" placeholder="Pin Code" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?> value="<?php echo @$trrow['pin_code']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">GST Number <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="GST Number" name="gst_number" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?> value="<?php echo @$trrow['gst_number']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="col-sm-4 control-label">PAN Number <span class="req-fld">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="pan_number" <?php if($trrow['check_val']==0){ echo "readonly";} else { echo "required"; }?> placeholder="PAN Number" value="<?php echo @$trrow['pan_number']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group no-padding"><hr>
							<div class="col-sm-7">
								<h4 class="hthin"><u>Courier Info</u></h4>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 custom-icheck">
									<label for="inputName" class="col-sm-4 control-label">Mode Of Transport <span class="req-fld">*</span></label>
	                            	<div class="col-sm-5 custom_icheck">
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$ro['courier_type']==1) { echo "checked"; } ?>" style=" position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" class="courier_type" value="1" name="courier_type" <?php if(@$ro['courier_type']==1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Courier
		                                </label>
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$ro['courier_type']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" <?php if(@$ro['courier_type']==2) { echo "checked"; } ?> class="courier_type" value="2" name="courier_type" style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Hand
		                                </label>

		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$ro['courier_type']==3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" <?php if(@$ro['courier_type']==3) { echo "checked"; } ?> class="courier_type" value="3" name="courier_type" style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Dedicated
		                                </label>
	                            	</div>
								</div>
							</div>
							<div class="form-group courier_div <?php if(@$ro['courier_type']==2 || @$ro['courier_type']==3) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Courier Name <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" class="form-control courier_name" placeholder="Courier Name" name="courier_name" value="<?php echo @$ro['courier_name']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group docket_div <?php if(@$ro['courier_type']==2 || @$ro['courier_type']==3) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Docket/AWB No. <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" class="form-control docket_num" placeholder="Docket Number" name="courier_number" value="<?php echo @$ro['courier_number']; ?>">
									</div>
								</div>
							</div>

							<div class="form-group vehicle_div <?php if(@$ro['courier_type']==1 || @$ro['courier_type']==2) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Vehicle Number <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" class="form-control vehicle_num" placeholder="Vehicle Number" name="vehicle_number" value="<?php echo @$ro['vehicle_number']; ?>">
									</div>
								</div>
							</div>

							<div class="form-group contact_div <?php if(@$ro['courier_type']==1 ) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Contact Person <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" class="form-control contact_person" placeholder="Contact Person" name="contact_person" value="<?php echo @$ro['contact_person']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group phone_div <?php if(@$ro['courier_type']==1) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Phone Number <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" class="form-control phone_number" placeholder="Phone Number" name="phone_number" value="<?php echo @$ro['phone_number']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group expected_date_row">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Expected Delivery Date <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input class="form-control  expected_date" data-min-view="2" data-date-format="dd-mm-yyyy" size="16" type="text" placeholder="DD-MM-YYYY"  name="expected_delivery_date" readonly value="<?php echo indian_format(@$ro['expected_arrival_date']); ?>" style="cursor:hand;background-color: #ffffff">
									</div>
								</div>
							</div>
							<div class="form-group remarks_row">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">E-Way Bill No.</label>
									<div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="E-Way Bill Number" name="remarks" value="<?php echo @$ro['remarks']; ?>">
									</div>
								</div>
							</div>
                        </div>
								<div class="form-group"><br>
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary" type="submit" name="submit" value="1" onclick="return confirm('Are you sure you want to Update Print Details?')"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL;?>closed_pickup_list"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
							</form>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>