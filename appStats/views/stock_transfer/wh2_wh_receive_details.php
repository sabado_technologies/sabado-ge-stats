<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>		
            <div class="row"> 
                <div class="col-sm-12 col-md-12">
                    <div class="block-flat">
                        <div class="content">
                            <form method="post" action="<?php echo SITE_URL.'insert_wh2_wh_receive';?>" parsley-validate novalidate class="form-horizontal" enctype="multipart/form-data">
                            
                            <input type="hidden" name="order_delivery_id" value="<?php echo $return_info['order_delivery_id']; ?>"> 
                            <input type="hidden" name="tool_order_id" value="<?php echo $tool_order_id; ?>"> 
                            <input type="hidden" value="<?php echo @$return_assets[0]['order_status_id'];?>" name="order_status_id" >
                                <div class="col-sm-12 col-md-12"><br>
                                    <div class="row">                           
                                        <div class="header">
                                            <h5 align="center"><strong>Tools To Be Acknowledge</strong></h5>
                                        </div>
                                    </div>
                                    <div class="table-responsive" style="margin-top: 10px;">
                                        <table class="table scan_table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="text-center"><strong>S.No</strong></th>
                                                    <th class="text-center"><strong>Tool Number</strong></th>
                                                    <th class="text-center"><strong>Tool Description</strong></th>
                                                    <th class="text-center"><strong>Asset Number</strong></th>
                                                    <th class="text-center"><strong>Serial Number</strong></th>
                                                    <th class="text-center" width="27%"><strong>Acknowledge Status</strong></th>
                                                    <th class="text-center"><strong>Pallet Location</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if(count(@$return_parts)>0)
                                                {   $sn = 1;
                                                    foreach(@$return_parts as $oah_id => $row)
                                                    { 
                                                    ?>
                                                        <tr id="<?php echo 'main'.$row['asset_id']; ?>">
                                                          <input type="hidden" name="assetAndOrderedAsset[<?php echo $row['asset_id'];?>]" value="<?php echo $row['ordered_asset_id'];?>" >   
                                                          <input type="hidden" name="assetAndOrderedTool[<?php echo $row['asset_id'];?>]" value="<?php echo $row['ordered_tool_id'];?>" >   
                                                         <input type="hidden" name="oah_id[<?php echo $oah_id;?>]" value="<?php echo $row['ordered_asset_id'];?>" >
                                                            <td class="text-center">
                                                                <a class="btn btn-warning md-trigger scan_qr" data-toggle="modal" data-target="#form-primary" href="#" data-asset-number="<?php echo $row['asset_number']; ?>" data-asset-id="<?php echo $row['asset_id']; ?>"><i class="fa fa-qrcode" ></i> Scan</a>
                                                            </td>
                                                            <td class="text-center"><?php echo $sn++; ?></td>
                                                            <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                            <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                            <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                            <td class="text-center"><?php echo $row['serial_number']; ?></td>
                                                            <td class="text-center ">                                                                
                                                                <div class="col-sm-12 custom_icheck">
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_status" value="1" name="oah_condition_id[<?php echo $row['ordered_asset_id']; ?>]" checked style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Received
                                                                </label>
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_status as2" value="2" name="oah_condition_id[<?php echo $row['ordered_asset_id']; ?>]" style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Not Received
                                                                </label>
                                                                
                                                                </div>
                                                            </td>
                                                            <td class="text-center"><input type="text" autocomplete="off" name="sub_inventory[<?php echo $row['asset_id'];?>];?>" value=""></td>
                                                            
                                                        </tr>
                                                    <?php if(count(@$row['health_data'])>0)
                                                        {  ?>
                                                            <tr class="details" id="<?php echo 'show'.$row['asset_id']; ?>">
                                                                <td  colspan="8">
                                                            <table class="table tool_list">
                                                                <thead>
                                                                    <tr>
                                                                        
                                                                        <th class="text-center"><strong>Serial Number</strong></th>
                                                                        <th class="text-center"><strong>Tool Number</strong></th>
                                                                        <th class="text-center"><strong>Tool Description</strong></th>
                                                                        <th class="text-center"><strong>Tool Level</strong></th>
                                                                        <th class="text-center"><strong>Quantity</strong></th>
                                                                        <th class="text-center"><strong>Tool Health</strong></th>
                                                                        <th class="text-center"><strong>Remarks</strong></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                           <?php foreach(@$row['health_data'] as $value)
                                                            {  ?>
                                                                <tr class="asset_row">
                                                                    <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_level_name']; ?></td>
                                                                    <td class="text-center"><?php echo $value['quantity']; ?></td>                                                                    
                                                                    <input type="hidden" name="oah_oa_health_id_part_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][]" value="<?php echo $value['part_id'];?>"  >                       
                                                                    <td width="32%" class="text-center">
                                                                        <div class="custom_icheck">
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if($value['asset_condition_id'] == 1) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="1" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id'] == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <input type="hidden" class=""  name="old_oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['asset_condition_id'];?>">
                                                                                <input type="hidden" class=""  name="oa_health_id_tool_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['p_tool_id'];?>">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Good
                                                                        </label>
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if(@$value['asset_condition_id'] == 2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="2" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id']== 2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <input type="hidden" class=""  name="old_oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['asset_condition_id'];?>">
                                                                                <input type="hidden" class=""  name="oa_health_id_tool_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['p_tool_id'];?>">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Defective
                                                                        </label>
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if(@$value['asset_condition_id'] == 3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="3" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id'] == 3) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <input type="hidden" class=""  name="old_oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['asset_condition_id'];?>">
                                                                                <input type="hidden" class=""  name="oa_health_id_tool_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['p_tool_id'];?>">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Missing
                                                                        </label>
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <div class="textbox">
                                                                            <textarea class="form-control textarea" name="remarks[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]"><?php if($value['remarks'] != '') { echo $value['asset_condition_id']; } ?></textarea>  
                                                                            <input  type= "hidden"  name="old_remarks[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" value="<?php echo $value['remarks'];?>" > 
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php     
                                                            } ?> 
                                                                <tr><td colspan="6" class="text-center"></td>
                                                                    <td class="text-center"><a class="btn btn-info save_point" value="1"><i class="fa fa-check"></i> Save</a></td></tr>
                                                                </tbody>
                                                            </table>
                                                            </td>
                                                        </tr><?php
                                                        }
                                                        else
                                                        { ?>
                                                            <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                        <?php 
                                                        }
                                                    }
                                                
                                                } else { ?>
                                                    <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                        <?php   } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>     
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-6"><br>
                                        <button class="btn btn-primary ack_submit_btn hidden" onclick="return confirm('Are you sure you want to Submit?')"  type="submit" value="1" name="approve"><i class="fa fa-check"></i> Submit</button>
                                        <a class="btn btn-danger" href="<?php echo SITE_URL;?>wh2_wh_receive"><i class="fa fa-times"></i> Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>              
                </div>
            </div>
	    
        </div>
    </div>
</div>
<input type="hidden" name="hidden_asset_id" id="hidden_asset_id">
<input type="hidden" name="hidden_asset_number" id="hidden_asset_number">
<input type="hidden" name="asset_total" class="asset_total" value="<?php echo count($return_parts); ?>">
<div class="modal fade colored-header" id="form-primary">
    <div class="modal-dialog" style="margin-top: 200px;padding-bottom: 0px;margin-bottom: 0px;width: 600px;height: 200px;">
        <div class="md-content" style="height: 250px !important;width:500px;">
            <div class="modal-header">
                <h3>Scan Asset QR Code</h3>
            </div>
            <div class="modal-body" style="overflow: hidden !important;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Asset Number <span class="req-fld">*</span></label> 
                            <div class="col-sm-8 asset_display">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Scan QR Code <span class="req-fld">*</span></label> 
                            <div class="col-sm-8">
                                <input type="text" autocomplete="off" name="asset_number" id="asset_number" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group"> 
                            <div class="col-sm-offset-4 col-sm-8">
                                <input type="checkbox" name="vehicle" value="0" class="checkbox_val"> Enable Manual Entry
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 submit_action hidden" style="margin-top: 10px;">
                        <div class="form-group"> 
                            <div class="col-sm-offset-4 col-sm-8">
                                <button class="btn btn-primary submitModal" type="submit" value="1" name="submitModal"><i class="fa fa-check"></i> Submit</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>