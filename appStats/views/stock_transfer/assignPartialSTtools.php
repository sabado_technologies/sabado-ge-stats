<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		
		<div class="block-flat">
			
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>insertPartiallyAssignedSTtools">
                    <div class="form-group">
                        <div class="col-md-12">
                          <?php if(isset($fe_check)) { ?>
                            <label for="inputName" class="col-sm-1 control-label">Ship To <span class="req-fld">*</span></label>
                            <div class="col-sm-5 custom_icheck">
                                <?php if(@$fe_check == 1) {  ?>
                                    <label class="radio-inline"> 
                                    <div class="iradio_square-blue <?php if(@$fe_check == 1) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                        <input type="radio" class="fe_check" value="1" name="fe_check" <?php if(@$fe_check ==1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                    </div> 
                                        FE Ordered Address
                                    </label>

                               <?php  }else { ?>

                                    <label class="radio-inline"> 
                                        <div class="iradio_square-blue <?php if(@$fe_check ==0) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                            <input type="radio" class="fe_check" value="0" name="fe_check" <?php if(@$fe_check ==0) { echo "checked"; }?> style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                        </div> 
                                        FE Ordered Warehouse
                                    </label>
                                <?php } ?>

                            </div> 
                          <?php } else { ?>

                            <label for="inputName" class="col-sm-1 control-label">Ship To <span class="req-fld">*</span></label>
                            <div class="col-sm-5 custom_icheck">
                                <?php if(@$order_info['order_delivery_type_id']==1 || $order_info['order_delivery_type_id'] == 3) {  ?>
                                    <label class="radio-inline"> 
                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==1 || $order_info['order_delivery_type_id'] == 3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                        <input type="radio" class="fe_check" value="1" name="fe_check" <?php if(@$order_info['order_delivery_type_id']==1 || $order_info['order_delivery_type_id'] == 3) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                    </div> 
                                        FE Ordered Address
                                    </label>

                               <?php  } ?>

                                <label class="radio-inline"> 
                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==2) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                        <input type="radio" class="fe_check" value="0" name="fe_check" <?php if(@$order_info['order_delivery_type_id']==2 ) { echo "checked"; }?> style="position: absolute; opacity: 0;">
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                    </div> 
                                    FE Ordered Warehouse
                                </label>
                            </div> 

                            <?php } ?> 
                                                                                      
                        </div>
                    </div>
                    <input type="hidden" name="tool_order_id" value="<?php echo $tool_order_id;?>" >
                    <div class="table-responsive" style="margin-top: 10px;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>Tool Number</strong></th>
                                    <th class="text-center"><strong>Tool Description </strong></th>
                                    <th class="text-center"><strong>Required Quantity</strong></th>                             
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count(@$ordered_tools)>0)
                                {   $sn = 1;
                                    foreach(@$ordered_tools as $row)
                                    { 
                                    ?>
                                        <tr>
                                            <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand"></td>
                                            <td class="text-center"><?php echo $sn++; ?></td>
                                            <td class="text-center"><?php echo $row['part_number']; ?></td>
                                            <td class="text-center"><?php echo $row['part_description']; ?></td>
                                            <td class="text-center" ><?php echo ($row['quantity']-$row['available_quantity']-$row['final_qty']); ?></td>
                                            <input type="hidden" name ="od_req_qty[<?php echo $row['ordered_tool_id'];?>]" class="tool_qty<?php echo $row['ordered_tool_id'];?> tot_req" value="<?php echo ($row['quantity']-$row['available_quantity']-$row['final_qty']);?>">

                                            
                                            
                                        </tr>
                                        <?php if(count(@$tools_info[$row['tool_id']])>0)
                                        {  ?>
                                        <tr class="details">
                                            <td></td>
                                                <td  colspan="6"> 
                                                    <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                                        <thead>
                                                            <th></th>
                                                            <th class="text-center">Ware House</th>
                                                            <th class="text-center">Available Quantity</th>
                                                            <th class="text-center">Quantity </th>
                                                        </thead>
                                                        <tbody>
                                                   <?php foreach(@$tools_info[$row['tool_id']] as $value)
                                                    { 
                                                    ?>
                                                        <tr class="toolRow">
                                                            <input type="hidden" name ="OdtAndTool[<?php echo $row['ordered_tool_id'];?>][<?php echo $row['tool_id'];?>]" value="" >
                                                            <td class="text-center"> <input type="checkbox" class="icheck1"  name="post_od_tool[<?php echo $row['ordered_tool_id'];?>]" value="<?php echo $row['tool_id'];?>"></td>
                                                            <td class="text-center"><?php echo $value['wh_name']; ?></td>
                                                            <td class="text-center html_wh_qty_cls html_wh_qty<?php echo $row['ordered_tool_id'];?> " ><?php echo $value['wh_qty']; ?></td>
                                                            <td class="text-center"><input type="number" min="1"  autocomplete="off" disabled class=" text-center toolQuantity t_post_qty<?php echo $row['ordered_tool_id'];?> tot_selected" data-od-qty="<?php echo $row['ordered_tool_id'];?>"  name="post_qty[<?php echo $value['wh_id'];?>][<?php echo $row['ordered_tool_id'];?>]" value=""  ></td>
                                                            
                                                        </tr>
                                                    <?php     
                                                    } ?>
                                                        </tbody>
                                                    </table>
                                            </td>
                                        </tr><?php
                                        }
                                        else
                                        { ?>
                                            <tr class="details"><td colspan="5" align="center"><span class="label label-primary">No Qty is Available In Dedicated Inventory</span></td></tr>
                                        <?php 
                                        }
                                    }
                                
                                } else {?>
                                    <tr><td colspan="5" align="center"><span class="label label-primary">As no Qty Available in Dedicated Warehouse or all tools are are in transfer process, You can not generate Partial Shipment</span></td></tr>
                        <?php   } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-5">
                                <button class="btn btn-primary submit-cls"   type="submit" value="1" name="submit_fe"><i class="fa fa-check"></i> Submit</button>
                                <a class="btn btn-danger cancel_cls" href="<?php echo SITE_URL.'raiseSTforOrder' ?>"><i class="fa fa-times"></i> Cancel</a>
                            </div>
                    </div>                    		
			</div>
		</div>	
	
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>