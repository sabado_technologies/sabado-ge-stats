<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
        <?php
        if(isset($displayResults)&&$displayResults==1)
        {   
        ?>
        <div class="block-flat">
            <table class="table table-bordered"></table>
            <div class="content">
                <div class="row">
                    <form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>closedSTforOrder">
                        
                        <div class="row">
                            <div class="col-sm-12">
                                
                                <!-- <label class="col-sm-2 control-label">Order/ST Number</label> -->
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order/ST Number" value="<?php echo @$searchParams['order_number'];?>" class="form-control">
                                </div>
                                <!-- <label class="col-sm-2 control-label">Shipment To</label>
                                <div class="col-sm-3">
                                    <?php echo form_dropdown('to_wh_id', @$whs, @$searchParams['to_wh_id'],'class="select2"  data-placeholder="Warehosue"  '); ?>
                                </div> -->
                                <div class="col-sm-3">
                                    <select name="to_wh_id" class="main_status select2" >    <option value="">-Shipment To -</option>
                                        <?php
                                        foreach($whs as $row)
                                        {
                                            $selected = ($row['wh_id']==@$searchParams['to_wh_id'])?'selected="selected"':'';
                                            echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' ('.$row['name'].')'.'</option>';
                                        }?>
                                    </select>
                                </div>
                                <?php if($task_access==2 || $task_access==3)
                                    {
                                        ?>
                                        <div class="col-sm-3">
                                            <?php 
                                            $d['name']        = 'cst_sso_id';
                                            $d['search_data'] = @$searchParams['cst_sso_id'];
                                            $this->load->view('sso_dropdown_list',$d); 
                                            ?>
                                        </div> <?php
                                    } ?>
                                <?php
                                    if($task_access==3 && @$_SESSION['header_country_id']=='')
                                    {
                                        ?>
                                        <div class="col-sm-3">
                                            <select name="cst_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
                                                <?php
                                                foreach($country_list as $row)
                                                {
                                                    $selected = ($row['location_id']==@$searchParams['cst_country_id'])?'selected="selected"':'';
                                                    echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                                }?>
                                            </select>
                                        </div>

                                         <?php
                                    }?>
                                    
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-12">
                                <div class="col-sm-8">
                                    <label class="col-sm-4 control-label">Stock Transfer Records :</label>
                                    <div class="col-sm-8 custom_icheck">
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue <?php if(@$searchParams['st_type']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" value="2" name="st_type" <?php if(@$searchParams['st_type']==2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div>
                                            For FE Order
                                        </label>
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue <?php if(@$searchParams['st_type']==1) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" value="1" name="st_type" <?php if(@$searchParams['st_type']==1) { echo "checked"; }?> style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            For WH To WH
                                        </label>
                                    </div>
                                </div>

                                <div class=" col-sm-4" >    
                                    <button type="submit" name="closedSTforOrder" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                    <a href="<?php echo SITE_URL.'closedSTforOrder'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                    <button type="submit" formaction="<?php echo SITE_URL.'download_closed_st'; ?>" onclick="return confirm('Are you sure you want to Download?')" name="download_closed_st" value="1" class="btn btn-success"><i class="fa fa-download"></i> Download</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php if($searchParams['st_type'] == 2) { ?>
                <div class="col-sm-12 col-md-12">
                    <div class="table-responsive" style="margin-top: 10px;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>Order Number</strong></th>
                                    <th class="text-center"><strong>Shipment To </strong></th>
                                    <th class="text-center"><strong>SSO</strong></th>                              
                                    <th class="text-center"><strong>Country</strong></th>   
                                    <th class="text-center"><strong>Actions</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count(@$orderResults)>0)
                                {   //$sn = 1;
                                    foreach(@$orderResults as $row)
                                    { 
                                    ?>
                                        <tr>
                                            <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                            <td class="text-center"><?php echo $sn++; ?></td>
                                            <td class="text-center"><?php echo $row['order_number']; ?></td>
                                            <td class="text-center" style="width:400px">
                                                
                                                 <?php 
                                                $fe_check = 0;
                                                // if fe check is 1 then order address if not then to the warehouse
                                                $fe_check_data = $this->Stock_transfer_m->getOldFeCheck($row['tool_order_id']);
                                                if(count($fe_check_data)>0)
                                                {
                                                    $fe_check = $fe_check_data[0]['fe_check'];
                                                }
                                                if($fe_check == 1)
                                                {
                                                    if($row['order_delivery_type_id']==1 )
                                                    {
                                                        $ib_data = get_install_basedata($row['system_id']);
                                                        echo $ib_data['c_name'].','.$row['system_id'].','.$ib_data['address1'].','.$ib_data['address2'].','.$ib_data['address3'].','.$ib_data['address4'].','.$ib_data['zip_code'];
                                                    }
                                                    else
                                                    {
                                                       if($row['order_delivery_type_id']==3)
                                                        {
                                                            echo $row['address1'].','.$row['address2'].','.$row['address3'].','.$row['address4'].','.$row['pin_code'];
                                                        }
                                                        else
                                                        {
                                                            echo getWhCodeAndName($row['wh_id']);
                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                    echo getWhCodeAndName($row['wh_id']);
                                                }
                                             ?>
                                            </td>
                                            <td class="text-center"><?php echo $row['sso']; ?></td>
                                            <td class="text-center" ><?php echo $row['countryName']; ?></td>
                                            <td class="text-center"> <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View  Details"  href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(9);?>"><i class="fa fa-eye"></i></a>                                                   </td>
                                            
                                        </tr>
                                        <?php if(count(@$st_data[$row['tool_order_id']])>0)
                                        {  ?>
                                        <tr class="details">
                                            <td></td>
                                                <td  colspan="6"> 
                                                    <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                                        <thead>
                                                            <th class="text-center">ST Number</th>
                                                            <th class="text-center">Shipment From</th>
                                                            <th class="text-center">Order Status</th>
                                                            <th class="text-center"> Action </th>
                                                        </thead>
                                                        <tbody>
                                                   <?php foreach(@$st_data[$row['tool_order_id']] as $value)
                                                    { 
                                                    ?>
                                                        <tr class="asset_row">
                                                            <td class="text-center"><?php echo $value['stn_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['wh']; ?></td>
                                                            <td class="text-center">
                                                            <?php
                                                                echo stockTransferStatus($value,1);
                                                              ?></td>  
                                                            <td class="text-center"><a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View  Details"  href="<?php echo SITE_URL.'closed_wh_transfer_info/'.storm_encode($value['tool_order_id']).'/'.storm_encode(6);?>"><i class="fa fa-eye"></i></a>                                                   </td>
                                                        </tr>
                                                    <?php     
                                                    } ?>
                                                        </tbody>
                                                    </table>
                                            </td>
                                        </tr><?php
                                        }
                                        else
                                        { ?>
                                            <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                        <?php 
                                        }
                                    }
                                
                                } else {?>
                                    <tr><td colspan="7" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                        <?php   } ?>
                            </tbody>
                        </table>
                    </div>
                </div> 
                <?php } else { ?>
                <div class="col-sm-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>ST Number</strong></th>
                                    <th class="text-center"><strong>From Warehouse</strong></th>
                                    <th class="text-center"><strong>To Warehouse</strong></th>
                                    <th class="text-center"><strong>Stock Transfer Status</strong></th>
                                    <th class="text-center"><strong>Country</strong></th>
                                    <th class="text-center"><strong>Actions</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count($orderResults)>0)
                                {
                                    foreach($orderResults as $row)
                                    {
                                    ?>
                                        <tr>
                                            <td class="text-center"><?php echo $sn++; ?></td>
                                            <td class="text-center"><?php echo $row['stn_number']; ?></td>
                                            <td class="text-center"><?php echo getWhCodeAndName($row['wh_id']) ?></td>
                                            <td class="text-center"><?php echo getWhCodeAndName($row['to_wh_id']) ?></td>
                                            <td class="text-center"><?php echo $row['cs_name']; ?></td>
                                            <td class="text-center" ><?php echo $row['countryName']; ?></td>
                                            <td class="text-center">                                     
                                            <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View  Details"  href="<?php echo SITE_URL.'closed_wh_transfer_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(6);?>"><i class="fa fa-eye"></i></a>
                                               
                                            </td>
                                        </tr>
                            <?php   }
                                } else {?>
                                    <tr><td colspan="7" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                        <?php   } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pull-left">
                            <div class="dataTables_info" role="status" aria-live="polite">
                                <?php echo @$pagermessage; ?>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="dataTables_paginate paging_bootstrap_full_number">
                                <?php echo @$pagination_links; ?>
                            </div>
                        </div>
                    </div> 
                </div>                  
            </div>
        </div>  
    <?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>