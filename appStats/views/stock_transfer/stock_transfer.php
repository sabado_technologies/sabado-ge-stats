<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>raiseSTforOrder">
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order/ST Number" value="<?php echo @$searchParams['st_order_number'];?>"  class="form-control">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" id="dateFrom" readonly name="request_date" placeholder="Request Date" value="<?php echo @$searchParams['st_request_date'];?>"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" id="dateTo" readonly name="return_date" placeholder="Return Date To" value="<?php echo @$searchParams['st_return_date'];?>"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">							
									<button type="submit" name="raiseSTforOrder" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'raiseSTforOrder'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
                                    <select name="sso_id" class="all_sso_dropdown_list" style="width:100%"> 
										<option value="">- SSO -</option>
										<?php 
										if(isset($searchParams['st_sso_id']))
										{
											$sso = $this->Common_model->get_data_row('user',array('sso_id'=>$searchParams['st_sso_id']));
											echo "<option value=".$sso['sso_id']." selected>".$sso['sso_id']."-(".$sso['name'].")</option>";
										}	?>
									</select>
								</div>
								<?php
									if($task_access==3 && @$_SESSION['header_country_id']=='')
									{
										?>
										<div class="col-sm-3">
											<select name="st_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
												<?php
												foreach($country_list as $row)
												{
													$selected = ($row['location_id']==@$searchParams['st_country_id'])?'selected="selected"':'';
													echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
												}?>
											</select>
										</div>
										 <?php
									}?>	
								</div>
							</div>	
						</div>
					</form>
					<div class="row">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
						</div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center" width="4%"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Order/ST Number</strong></th>
								<th class="text-center"><strong>Requested By</strong></th>
								<th class="text-center"><strong>Ship To</strong></th>
								<th class="text-center" width="9%"><strong>Order Type</strong></th>
								<th class="text-center" width="10%"><strong>Request Date</strong></th>
								<th class="text-center" width="9%"><strong>Need Date</strong></th>
								<th class="text-center" width="10%"><strong>Return Date</strong></th>
								<th class="text-center" width="9%"><strong>Country</strong></th>
								<th class="text-center" width="17%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo ($row['order_type'] == 1)?$row['stn_number']:@$row['order_number'];?></td>										
										<td class="text-center"><?php echo getNameBySSO($row['sso_id']);?></td>
										<td class="text-center"><?php echo $row['address3'];?></td>
										<td class="text-center"><?php echo ($row['order_type'] == 1)?'For WH':'For Order';?></td>
										<td class="text-center"><?php echo indian_format(@$row['deploy_date']);?></td>
										<td class="text-center"><?php echo indian_format(@$row['request_date']);?></td>
										<td class="text-center"><?php echo ($row['order_type'] == 1)?'NA':indian_format(@$row['return_date']);?></td>
										<td class="text-center"><?php echo $row['country_name'];?></td>
										<td class="text-center">
										<?php  // for stock trnafer
											if($row['order_type'] == 1)
											{  ?>
												<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'wh_transfer_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(3);?>"><i class="fa fa-eye"></i></a>                                   
												<?php  // raised by admin then until shipment he can edit and cancel
													if(getRoleByUser($row['sso_id']) == 1 || getRoleByUser($row['sso_id']) == 4) 
													{
														if($row['current_stage_id'] == 1)
														{ ?>
															<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Edit Transfer"  href="<?php echo SITE_URL.'edit_transfer/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>
			                                        		 <a class="btn btn-danger" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Cancel"  onclick="return confirm('Are you sure you want to Cancel?')" href="<?php echo SITE_URL.'cancel_transfer/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-trash-o"></i></a>
												  <?php } ?>

											  <?php } 
											    
											  	// Admin can only approve or reject if request is raise by logistic
											  		else 
												  	{ ?>
												  		<a class="btn btn-primary" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Approve Or reject ST "  href="<?php echo SITE_URL.'approveOrRejectST/'.storm_encode($row['tool_order_id']).'/'.storm_encode(1);?>"><i class="fa fa-pencil"></i></a>
												<?php } ?>											
									<?php }
										// order for FE Order
										 else
										   {
	                                        if($row['current_stage_id'] == 4)
	                                        	{ ?>
	                                        		<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(7);?>"><i class="fa fa-eye"></i></a>     
	                                        		<a class="btn btn-info" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Initiate Partial Shipment From Dedicated Warehouse" href="<?php echo SITE_URL.'initiatePartialShipment/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-shopping-cart"></i></a>   		

	                                        	<?php if(checkSTorderExist($row['tool_order_id']) == 0) { ?>
	                                        		<a class="btn btn-warning" style="padding:3px 3px;" data-container="body" data-placement="top"   data-toggle="tooltip" title="Re check" href="<?php echo SITE_URL.'checkToolsAvailabilityAgain/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-refresh"></i></a>                                   										
	                                        	<?php } ?>
	                                        		<a class="btn btn-primary" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Raise Stock Transfer"  href="<?php echo SITE_URL.'assignSTtoolsForOrder/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>
	                                      <?php } 
	                                       } ?>
                                    </td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="10" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(document).ready(function(){
		select2Ajax('all_sso_dropdown_list', 'all_sso_dropdown_list', 0, 3);
	});
</script>
