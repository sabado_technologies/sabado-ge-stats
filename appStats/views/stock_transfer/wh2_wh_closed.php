<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'wh2_wh_closed';?>">
						<div class="row">
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">From Warehouse</label>
								<div class="col-sm-3">
									<select name="wh_id" class="select2" > 
										<option value="">- From Warehouse -</option>
										<?php 
											foreach($from_wh_list as $row)
											{
												$selected = ($row['wh_id']==@$searchParams['wh_id'])?'selected="selected"':'';
												echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - ('.$row['name'].')</option>';
											}
										?>
									</select>
								</div>
								<label class="col-sm-1 control-label">ST No.</label>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="stn_number" placeholder="ST Number" value="<?php echo @$searchParams['stn_number'];?>"  class="form-control">
								</div>
								<div class="col-sm-3 col-md-3">							
									<button type="submit" name="wh2_wh_closed" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'wh2_wh_closed'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<?php if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray'])) { ?>
								<label class="col-sm-2 control-label">To Warehouse</label>
								<div class="col-sm-3">
									<select name="to_wh_id" class="select2" > 
										<option value="">- To Warehouse -</option>
										<?php 
											foreach($to_wh_list as $row)
											{
												$selected = ($row['wh_id']==@$searchParams['to_wh_id'])?'selected="selected"':'';
												echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - ('.$row['name'].')</option>';
											}
										?>
									</select>
								</div>
								<?php } ?>
								<?php if($task_access == 3 && @$_SESSION['header_country_id']==''){?>
							    <label class="col-sm-1 control-label">Country</label>
								<div class="col-sm-3">
									<select class="select2" name="country_id" >
										<option value="">- Country -</option>
										 <?php
										foreach($countryList as $country)
										{
											$selected = ($country['location_id']==$searchParams['country_id'])?'selected="selected"':'';
											echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
										}
										?>
									</select>
								</div>
								<?php } ?>
							</div>
						</div>
						<div class="row">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
						</div>
					</form>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.No</strong></th>
								<th class="text-center"><strong>ST Number</strong></th>
								<th class="text-center"><strong>Shipment From</strong></th>
								<th class="text-center"><strong>Shipment To</strong></th>
								<th class="text-center"><strong>For Order Number</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							$i = 1;
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$i++;?></td>
										<?php $actual_order_info = $this->Stock_transfer_m->get_actual_order_info($row['tool_order_id']);
										//echo '<pre>';print_r($actual_order_info);exit;
										?>
										<td class="text-center"><?php echo @$row['stn_number'];?></td>
										<td class="text-center"><?php echo @$row['from_wh_name'];?></td>
										<td class="text-center"><?php echo @$row['to_wh_name'];?></td>
										<td class="text-center"><?php if(@$actual_order_info['order_number']!='') { echo @$actual_order_info['order_number']; } else { echo "--"; }?></td>
										<td class="text-center"><?php echo @get_country_location($row['country_id']);?></td>
										<td class="text-center"><a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View  Details"  href="<?php echo SITE_URL.'closed_wh_transfer_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(5);?>"><i class="fa fa-eye"></i></a></td>
					
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="7" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
