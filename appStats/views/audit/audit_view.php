<?php $this->load->view('commons/main_template',$nestedView); ?>
<style type="text/css">
	tr td{
		overflow-wrap: break-word;
	}
</style>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
			<div class="block-flat">
				<table class="table table-bordered"></table>
				<div class="content">
					<div class="row">
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'get_audit_page/'.$sent_data;?>">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="old_value" value="<?php echo @$search_data['old_value'];?>" class="form-control" placeholder="Old Value">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="new_value" value="<?php echo @$search_data['new_value'];?>" class="form-control" placeholder="New Value">
								</div>
								<div class="col-sm-3">
									<select name="sso_user" class="all_sso_dropdown_list" style="width:100%"> 
										<option value="">- SSO -</option>
										<?php 
										if(isset($search_data['sso_user']))
										{
											$sso = $this->Common_model->get_data_row('user',array('sso_id'=>$search_data['sso_user']));
											echo "<option value=".$sso['sso_id']." selected>".$sso['sso_id']."-(".$sso['name'].")</option>";
										}	?>
									</select>
								</div>

								<div class="col-sm-3">
									<button type="submit" name="searchaudit" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
									<a href="<?php echo SITE_URL.'get_audit_page/'.$sent_data;?>" data-toggle="tooltip" class="btn btn-success" title="Refresh"><i class="fa fa-refresh"></i></a>
									<!-- <button type="submit" data-toggle="tooltip" title="Download" name="download_audit" value="1" formaction="<?php echo SITE_URL.'download_audit/'.$sent_data;?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i></button>  -->
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<select class="select2" name="column_name" >
										<option value="">- Column -</option>
										 <?php
										foreach($columnList as $key => $value)
										{
											$selected = ($value==$search_data['column_name'])?'selected="selected"':'';
											echo '<option value="'.$value.'" '.$selected.'>'.$value.'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-sm-3">
									<select class="select2" name="trans_type" >
										<option value="">- Transaction Type -</option>
										 <?php
										foreach($transList as $trans)
										{
											$selected = ($trans['trans_id']==$search_data['trans_type'])?'selected="selected"':'';
											echo '<option value="'.$trans['trans_id'].'" '.$selected.'>'.$trans['name'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
						</form>
					</div><br>
					<div class="table-responsive">
						<div class="col-md-12">
                    		<span><?php echo @$display_name; ?></span>
                    	</div>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center" width="4%"></th>
	                            	<th class="text-center" width="6%"><strong>S.No</strong></th>
	                            	<th class="text-center" width="20%"><strong>Work Flow</strong></th>
	                                <th class="text-center" width="20%"><strong>Type</strong></th>
	                                <th class="text-center" width="30%"><strong>SSO Detail</strong></th>
	                                <th class="text-center" width="20%"><strong>Time</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(count($auditResults)>0)
								{
									foreach($auditResults as $row)
									{
									?>
										<tr>
											<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle_audit_details" title='expand'></td>
											<td class="text-center" ><?php echo $sn++;?></td>
											<td class="text-center"><?php echo $row['work_flow_name']; ?></td>
				 							<td class="text-center" ><?php echo $row['transaction_type']; ?></td>
				 							<td class="text-center" ><?php if(isset($row['user_name'])){ echo $row['user_name'];}else{echo "--"; } ?></td>
											<td class="text-center" ><?php echo date('d-M-Y h:i A',strtotime($row['created_time'])); ?></td>
										</tr>
										<tr class="audit_details">
											<td colspan="6">
											<?php
											if(isset($row['acd']))
											{
											foreach ($row['acd'] as $key => $value) 
											{ 
												if(count($value['final'])>0)
												{   ?>
													<div class="col-md-12">
														<span>Block : <strong><?php echo @$key; ?></strong></span>
													</div>											
													<table>
														<thead>
															<th class="text-center" width="3%"><strong>S.No.</strong></th>
															<th class="text-center" width="20%"><strong>Details Of</strong></th>
															<th class="text-center" width="17%"><strong>Column Name</strong></th>
															<th class="text-center" width="20%"><strong>Old Value</strong></th>
															<th class="text-center" width="20%"><strong>New Value</strong></th>
															<th class="text-center" ><strong width="20%">Remarks</strong></th>
														</thead>
													    <tbody>
													   	<?php 
													   	$sno = 1;
													   	foreach(@$value['final'] as $ress)
													    {
													    	$i = 1;
													    	foreach(@$ress as $rrow)
													    	{

													            $old_value = ($rrow['old_value']!='')?$rrow['old_value']:'--';
																$new_value = ($rrow['new_value']!='')?$rrow['new_value']:'--'; 
													            ?>
													                <tr class="asset_row" style="background-color: white">
													                	<?php if($i == 1){?>
													                    <td align="center" rowspan="<?php echo COUNT($ress); ?>"><?php echo $sno++; ?></td>
													                    <td align="center" rowspan="<?php echo COUNT($ress); ?>"><?php echo $rrow['block_name']; ?></td>
													                   	<?php } ?>
													                    <td align="center"><?php echo $rrow['column_name']; ?></td>
													                    <td align="center"><?php echo validate_string_is_date($old_value); ?></td>
													                    <td align="center"><?php echo validate_string_is_date($new_value); ?></td>
													                    <?php if($i == 1){?>
													                    <td align="center" rowspan="<?php echo COUNT($ress); ?>"><?php echo $rrow['remarks']; ?></td>
													                   	<?php $i++;} ?>
													                </tr>
													            <?php
													        }     
													    } ?>
														</tbody>
													</table>
			                                        <br><?php 
			                                   	}
			                                }
			                            } else{ ?>
			                            	<?php echo $row['remarks']; ?>
			                            <?php } ?>
			                                </td>
										</tr>
								    <?php 
									}
								} 
								else 
								{ ?>
									<tr>
										<td colspan="9" align="center">
											<span class="label label-primary">No Records</span>
										</td>
									</tr>
	                    <?php 	} ?>
							</tbody>
						</table>
	                </div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>          		
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(document).ready(function(){
		select2Ajax('all_sso_dropdown_list', 'all_sso_dropdown_list', 0, 3);
	});
</script>