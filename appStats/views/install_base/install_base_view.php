<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
      <div class="row"> 
      <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
	<?php
	if(isset($flg))
	{
    ?>
    <div class="row"> 
		<div class="col-sm-12 col-md-12">
			<div class="block-flat">
				<div class="content">
					<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
						<?php
                        if($flg==2){
                            ?>
                            <input type="hidden" name="encoded_id" value="<?php echo storm_encode($lrow['install_base_id']);?>">
							<?php
                           }
                        ?>
                        <div class="header">
							<h5 align="left" style="color: #3380FF;"><strong>Country : <?php echo $country_name; ?></strong></h5>
						</div></br>
                        <input type="hidden" name="ib_id" id="install_base_id" value="<?php echo @$lrow['install_base_id'];?>">
                        <input type="hidden" name="cust_site_id" id="customer_site_id" value="<?php echo @$lrow['customer_site_id'];?>">
                        <input type="hidden" name="country_id" value="<?php echo $country_id;?>" id="country_id">

                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Customer ID <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<input type="text" autocomplete="off" required class="form-control" id="customer_id" placeholder="Customer ID" name="customer_number" value="<?php echo @$lrow['customer_number']; ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Site ID <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<input type="text" autocomplete="off" required class="form-control" placeholder="Site ID" name="site_id" value="<?php echo @$lrow['site_id']; ?>">
								</div>
							</div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Customer Name <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<input type="text" autocomplete="off" required class="form-control" id="customer_name" placeholder="Customer Name" name="customer_name" value="<?php echo @$lrow['name']; ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Service Region <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<input type="text" autocomplete="off" required class="form-control" id="service_region" placeholder="Service Region" name="service_region" value="<?php echo @$lrow['service_region']; ?>">
								</div>
							</div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Modality <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<select class="select2" name="modality_id" required>
											<option value="">- Modality -</option>
											 <?php
											foreach($modalityList as $modality)
											{
												$selected = ($modality['modality_id']==$lrow['modality_id'])?'selected="selected"':'';
												echo '<option value="'.$modality['modality_id'].'" '.$selected.'>'.$modality['name'].'</option>';
											}
											?>
									</select>
								</div>
							</div>
							<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Model Type <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<input type="text" autocomplete="off" required class="form-control" placeholder="Model Type" name="model_type" value="<?php echo @$lrow['model_type']; ?>">
								</div>
							</div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">System ID <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<?php
									$region_id = $this->Common_model->get_value('location',array('location_id'=>@$country_id),'region_id');
									$asean_region_id = get_asean_region_id();
									$display_name = 'System ID';
									if($region_id == $asean_region_id)
									{
										$display_name = 'System ID (DI & US) / Serial Number (LCS)';
									} ?>
									<input type="text" autocomplete="off" required class="form-control" placeholder="<?php echo @$display_name; ?>" name="system_id" value="<?php echo @$lrow['system_id']; ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Location <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<select class="select2" name="location_id" required>
											<option value="">- Location -</option>
											 <?php
											foreach($locationList as $location)
											{
												$selected = ($location['location_id']==$lrow['location_id'])?'selected="selected"':'';
												echo '<option value="'.$location['location_id'].'" '.$selected.'>'.$location['city_name'].'</option>';
											}
											?>
									</select>
								</div>
							</div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Asset Number <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<input type="text" autocomplete="off" required class="form-control" placeholder="Asset Number" name="asset_number" value="<?php echo @$lrow['asset_number']; ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Prod. Description <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<textarea class="form-control" required name="product_description"><?php echo @$lrow['product_description']; ?></textarea>
								</div>
							</div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<textarea class="form-control" name="address_1" required><?php echo @$lrow['address1']; ?>
									</textarea>
								</div>
							</div>
							<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<textarea class="form-control" required name="address_2" ><?php echo @$lrow['address2']; ?>
									</textarea>
								</div>
							</div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<textarea class="form-control" required name="address_3" ><?php echo @$lrow['address3']; ?>
									</textarea>
								</div>
							</div>
							<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<textarea class="form-control" required name="address_4"><?php echo @$lrow['address4']; ?>
									</textarea>
								</div>
							</div>
                        </div>
                        <div class="col-md-12 col-sm-12">
							<div class="form-group col-md-6">
							<label for="inputName" class="col-sm-4 control-label">Zip Code <span class="req-fld">*</span></label>
								<div class="col-sm-8">
									<input type="text" autocomplete="off" required class="form-control only-numbers" placeholder="Zip Code" name="zip_code" value="<?php echo @$lrow['zip_code']; ?>">
								</div>
							</div>
                        </div>
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-5"><br>
								<button class="btn btn-primary" value="1" type="submit" name="submit_install_base"><i class="fa fa-check"></i> Submit</button>
								<a class="btn btn-danger" href="<?php echo SITE_URL;?>install_base"><i class="fa fa-times"></i> Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>				
		</div>
	</div>
	<?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>install_base">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="customer_number" value="<?php echo @$search_data['customer_number'];?>" class="form-control" placeholder="Customer ID">
							</div>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="site_id" value="<?php echo @$search_data['site_id'];?>" class="form-control" placeholder="Site ID">
							</div>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="asset_number" value="<?php echo @$search_data['asset_number'];?>" class="form-control" placeholder="Asset Number">
							</div>
							<?php
							if($task_access==3 && $this->session->userdata('header_country_id')=='')
							{
							?>
							<div class="col-sm-3">
								<select name="country_id" class="select2" > 
									<option value="">- Select Country -</option>
									<?php 
										foreach($countryList as $row)
										{
											$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
											echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
										}
									?>
								</select>
							</div>
							<?php }?>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="customer_name" value="<?php echo @$search_data['customer_name'];?>" class="form-control" placeholder="Customer Name">
								</div>
								<div class="col-sm-3">
									<?php 
									if($this->session->userdata('header_country_id')!='')
									{
										$region_id = $this->Common_model->get_value('location',array('location_id'=>@$_SESSION['header_country_id']),'region_id');
										$asean_region_id = get_asean_region_id();
										$display_name = 'System ID';
										if($region_id == $asean_region_id)
										{
											$display_name = 'System ID (DI & US) / Serial No. (LCS)';
										}
									}
									else
									{
										$display_name = 'System ID';
									} ?>
									
									<input type="text" autocomplete="off" name="system_id" value="<?php echo @$search_data['system_id'];?>" class="form-control" placeholder="<?php echo @$display_name; ?>">
								</div>
								<div class="col-sm-3">
									<select class="select2" name="status">
										<option value="">- Select Status -</option>
										<option value="1" <?php if($search_data['status']==1){ echo "selected"; }?>>Active</option>
										<option value="2" <?php if($search_data['status']==2){ echo "selected"; }?>>Inactive</option>
									 </select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="content">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
							<div class="col-sm-12">
								<div class="col-sm-6 col-md-6">							
									<a href="<?php echo SITE_URL.'add_install_base'; ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
									<a href="<?php echo SITE_URL.'bulkupload_install_base'; ?>" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Upload in Bulk</a>
									
								</div>	
								<div class="col-sm-6 col-md-6" align="right">							
									<button type="submit" name="searchinstallbase" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'install_base'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
									<button type="submit" data-toggle="tooltip" title="Download" name="download_install_base" value="1" formaction="<?php echo SITE_URL.'download_install_base';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i>Download Old</button>  
									<button type="submit" data-toggle="tooltip" title="Download" name="download_install_base" value="1" formaction="<?php echo SITE_URL.'download_install_base_xlsx';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i>Download New</button>  
								</div>		
												
									
							</div>
						</div>
					</div>
				</form>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                            	<th class="text-center"><strong>Cust No.</strong></th>
                            	<th class="text-center"><strong>Name</strong></th>
                                <th class="text-center"><strong>Site ID</strong></th>
                               	<th class="text-center"><strong>System ID</strong></th>
                               	<th class="text-center"><strong>Asset No.</strong></th>
                                <th class="text-center"><strong>Product Desc.</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center" width="9%"><strong>Actions</strong></th>

							</tr>
						</thead>
						<tbody id="components_tbl">
							<?php 
							if(count($ibResults)>0)
							{
								foreach($ibResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++; ?></td>
										<td class="text-center"><?php echo $row['customer_number']; ?></td>
										<td class="text-center"><?php echo $row['name']; ?></td>
										<td class="text-center"><?php echo $row['site_id']; ?></td>
										<td class="text-center"><?php echo $row['system_id']; ?></td>
										<td class="text-center"><?php echo $row['asset_number']; ?></td>
										
										<td class="text-center"><?php echo $row['product_description']; ?></td>
										<td class="text-center"><?php echo $row['country']; ?></td>
										<td class="text-center">
                                        <a class="btn btn-default" data-toggle="tooltip" title="Update Installbase Details" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_install_base/'.storm_encode($row['install_base_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php
                                        if($row['ib_status']==1){
                                        ?>
                                        <a class="btn btn-danger" data-toggle="tooltip" title="Deactivate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_install_base/'.storm_encode($row['install_base_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['ib_status']==2){
                                        ?>
                                        <a class="btn btn-info" style="padding:3px 3px;" data-toggle="tooltip" title="Activate" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_install_base/'.storm_encode($row['install_base_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
                                    </td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="9" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
	<?php } ?>
	</div>
	</div>
	</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
