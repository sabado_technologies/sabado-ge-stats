<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form method="post" action="<?php echo $form_action;?>"   parsley-validate novalidate class="form-horizontal">
						    <input type="hidden" name="encoded_id" value="<?php echo storm_encode(@$lrow['supplier_id']);?>">
                            <input type="hidden" name="supplier_id" id="supplier_id" value="<?php echo @$lrow['supplier_id']; ?>">
                            <input type="hidden" name="edit_modality" id="edit_modality" value="<?php echo @count($modality_selected); ?> " >
                            <input type="hidden" name="edit_trans_type" id="edit_trans_type" value="<?php echo @$lrow['cal_sup_type_id']; ?> ">
                        <div class="header">
							<h5 align="left" style="color: #3380FF;"><strong>Country : <?php echo $country_name; ?></strong></h5>
						</div></br>
                        <div class="row">
                            <div class="form-group">
	                           	<div class="col-md-12">
							        <label for="inputName" class="col-sm-2 control-label">Name <span class="req-fld">*</span></label>
							        <div class="col-sm-4">
								        <input type="text" autocomplete="off" required id="supplier_name" class="form-control" placeholder="Supplier name" name="supplier_name" value="<?php echo @$lrow['name']; ?>">
							        </div>
							        <input type="hidden" name="c_id" id="country_id" value="<?php echo @$country_id;?>">
							        <label for="inputName" class="col-sm-2 control-label">Supplier Code <span class="req-fld">*</span></label>
							        <div class="col-sm-4">
								        <input type="text" autocomplete="off" required id="supplier_code" class="form-control" placeholder="Supplier Code" name="supplier_code" value="<?php if($flg == 1){ echo @$supplier_code;  } else{  echo @$lrow['supplier_code']; }?>">
							        </div>
						        </div>
					        </div>
					        <div class="form-group">
						        <div class="col-md-12">
							        <label for="inputName" class="col-sm-2 control-label">Supplier Type <span class="req-fld">*</span></label>
							        <div class="col-sm-4">
								        <select class="form-control supplier_type" name="supplier_type">
								        	<!-- <option value="">Select Supplier Type</option> -->
								        	<?php foreach($supplier_type as $supp)
								        	{    
								        		$selected = '';
								        		if($supp['sup_type_id'] == @$lrow['sup_type_id'])$selected = 'selected';
								        		echo '<option value="'.$supp['sup_type_id'].'"'.$selected.'>'.$supp['sup_type_name'].'</option>';
								        	}  ?>
								        </select>
				        			</div>
				        			<div class="calibration_provide_block">
					        			<label for="inputName" class="col-sm-2 control-label">Calibration <span class="req-fld">*</span></label>
								        <div class="col-sm-4">
									        <select class="form-control cal" required id="calibration" name="calibration">
									        	<option value="">Select Calibration</option>
									        	<?php foreach($calibration as $cal)
									        	{   
									        		$selected='';
									        		if($cal['calibration_id'] == @$lrow['calibration'])$selected='selected';
									        		echo '<option value="'.$cal['calibration_id'].'"'.$selected.'>'.$cal['calibration_name'].'</option>';
									        	}  ?>
									        </select>
					        			</div>
				        			</div>
						        </div>
						    </div>
						    <div class="form-group">
						        <div class="col-md-12">
						        	<div class="modality <?php if(@$flg == 2 && $lrow['sup_type_id']==2){ echo "hidden";} ?>">
						        	<label for="inputName" class="col-sm-2 control-label ">Modality <span class="req-fld">*</span></label>
								    <div class="col-sm-4">
                                    	<?php echo form_dropdown('modality[]', $modality, @$modality_selected,'class="select2 mod"   multiple'); ?>
                                    </div>
                                    </div>
                                	<label for="inputName" class="col-sm-2 control-label">Transport Type <span class="req-fld">*</span></label>
							        <div class="col-sm-4">
								        <select class="form-control" required name="transport_type">
								        	<option value="">Select Transport Type</option>
								        	<?php foreach($trans_type as $type)
								        	{   
								        		$selected = '';
								        		if($type['cal_sup_type_id'] == @$lrow['cal_sup_type_id'])$selected='selected';
								        		echo '<option value="'.$type['cal_sup_type_id'].'"'.$selected.'>'.$type['name'].'</option>';
								        	}  ?>
								        </select>
				        			</div>
						        </div>
						    </div>
						    <div class="form-group">
						        <div class="col-md-12">
						        	<label for="inputName" class="col-sm-2 control-label">Contact Person <span class="req-fld">*</span></label>
								    <div class="col-sm-4">
									    <textarea class="form-control" name="contact_person" required><?php echo @$lrow['contact_person']; ?></textarea>
								    </div>
								    <label for="inputName" class="col-sm-2 control-label">Email <span class="req-fld">*</span></label>
								    <div class="col-sm-4"> 
									    <textarea class="form-control" name="email" required><?php echo @$lrow['email']; ?></textarea>
								    </div>
						        </div>
						    </div>
						    <div class="form-group">
						        <div class="col-md-12">
						        	<label for="inputName" class="col-sm-2 control-label">Phone Number <span class="req-fld">*</span></label>
								    <div class="col-sm-4">
									    <textarea class="form-control" maxlength="30" name="phone_number" required><?php echo @$lrow['contact_number']; ?></textarea>
								    </div>
								    <label class="col-sm-2 control-label">Pincode <span class="req-fld">*</span></label>
	                    			<div class="col-sm-4">
										<input type="text" autocomplete="off"  required class="form-control " placeholder="Pincode" name="pin_code" value="<?php echo @$lrow['pin_code'] ?>">
									</div>
						        </div>
						    </div>
						    <div class="form-group">
						        <div class="col-md-12">
						        	<label class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
	                    			<div class="col-sm-4">
										<textarea required class="form-control" name="address_1"><?php echo @$lrow['address1'];?></textarea>
									</div>
									<label class="col-sm-2 control-label">Address2 <span class="req-fld">*</span></label>
	                    			<div class="col-sm-4">
										<textarea  class="form-control" required name="address_2"><?php echo @$lrow['address2'];?></textarea>
									</div>
						        </div>
						    </div>
						    <div class="form-group">
						        <div class="col-md-12">
						        	<label class="col-sm-2 control-label">City <span class="req-fld">*</span></label>
	                    			<div class="col-sm-4">
										<textarea  class="form-control" required name="address_3"><?php echo @$lrow['address3'];?></textarea>
									</div>
									<label class="col-sm-2 control-label">State <span class="req-fld">*</span></label>
	                    			<div class="col-sm-4">
										<textarea  class="form-control" required name="address_4"><?php echo @$lrow['address4'];?></textarea>
									</div>
						        </div>
						    </div>
						    <div class="form-group">
						        <div class="col-md-12">
						        	<label for="inputName" class="col-sm-2 control-label">GST Number </label>
								    <div class="col-sm-4">
									   	<input type="text" autocomplete="off"   class="form-control " placeholder="GST Number" name="gst_number" value="<?php echo @$lrow['gst_number'] ?>">
								    </div>
								    <label class="col-sm-2 control-label">PAN Number</label>
	                    			<div class="col-sm-4">
										<input type="text" autocomplete="off"   class="form-control " placeholder="PAN Number" name="pan_number" value="<?php echo @$lrow['pan_number'] ?>">
									</div>
									
						        </div>
						    </div>
						    <div class="form-group">
								<div class="col-sm-offset-5 col-sm-5"><br>
									<button class="btn btn-primary" type="submit" value="1" name="submit_supplier"><i class="fa fa-check"></i> Submit</button>
								    <a class="btn btn-danger" href="<?php echo SITE_URL;?>supplier"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
					    </div>                    
						</form>
					</div>
				</div>				
			</div>
		</div>
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>supplier">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="supplier_name" value="<?php echo @$search_data['name'];?>" class="form-control" placeholder="Supplier Name">
								</div>
								<div class="col-sm-3">
									<select class="select2" name="sup_type_id">
										<option value="">- Select Supplier -</option>
										<option value="1" <?php if(@$search_data['sup_type_id']==1){ echo "selected";}?> >Supplier</option>
										<option value="2" <?php if(@$search_data['sup_type_id']==2){ echo "selected";} ?>>Calibration Supplier</option>
									</select>
								</div>
								<div class="col-sm-3">
									<select class="select2" name="calibration">
										<option value="">- Select Calibration -</option>
										<option value="1" <?php if(@$search_data['calibration']==1){ echo "selected";}?> >Provide</option>
										<option value="2" <?php if(@$search_data['calibration']==2){ echo "selected";} ?>>Not Provide</option>
									</select>
								</div>
								<div class=" col-sm-3">
								    <button type="submit" name="searchsupplier" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
									<a href="<?php echo SITE_URL.'supplier';?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>
									<a href="<?php echo SITE_URL.'add_supplier';?>"  class="btn btn-success" data-toggle="tooltip" title="Add New"><i class="fa fa-plus"></i></a>
									<button type="submit" data-toggle="tooltip" title="Download" name="download_supplier" value="1" formaction="<?php echo SITE_URL.'download_supplier';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i></button>  
								</div>
		                    </div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-3">
		                            <?php echo form_dropdown('modality_id', $modality, @$search_data['modality_id'],'class="select2"');?>
		                        </div>
		                        <?php if($task_access == 3 && $this->session->userdata('header_country_id')==''){?>
			                            <div class="col-sm-3">
			                                <select class="select2" name="country_id" >
			                                    <option value="">- Select Country -</option>
			                                     <?php
			                                    foreach($countryList as $country)
			                                    {
			                                        $selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
			                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
			                                    }
			                                    ?>
			                                </select>
			                            </div>
                            		<?php } ?>
								
							</div>
						</div>
					</form>
				</div></br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                            	<th class="text-center" width="11%"><strong>Supplier Code</strong></th>
                                <th class="text-center" width="25%"><strong>Supplier Name</strong></th>
                                <th class="text-center"><strong>Modality</strong></th>
                                <th class="text-center" width="15%"><strong>Contact Person</strong></th>
                                <th class="text-center" width="15%"><strong>Email</strong></th>
                                <th class="text-center" width="15%"><strong>Contact Number</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center" width="15%"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($supplier_results)>0)
							{
								foreach($supplier_results as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo $row['supplier_code']; ?></td>
										<td class="text-center"><?php echo $row['name']; ?></td>
										<td class="text-center" ><?php echo $row['modality_name']; ?></td>
										<td class="text-center"><?php echo $row['contact_person']; ?></td>
										<td class="text-center"><?php echo $row['email']; ?></td>
										<td class="text-center"><?php echo $row['contact_number'] ?></td>
										<td class="text-center"><?php echo $row['country_name']; ?></td>  
                                        <td class="text-center">
										<?php
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-default" data-toggle="tooltip" title="Edit" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_supplier/'.storm_encode($row['supplier_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php
                                        }else{?>
                                        <a class="btn btn-default" data-toggle="tooltip"readonly title="activate supplier to edit"style="padding:3px 3px;" href="#"><i class="fa fa-pencil"></i></a>
                                       <?php }?>
                                        <?php
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" data-toggle="tooltip" title="Deactivate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_supplier/'.storm_encode($row['supplier_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" data-toggle="tooltip" title="Activate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_supplier/'.storm_encode($row['supplier_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?></td>
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="9" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<?php if(isset($flg))
{ ?>
<script type="text/javascript">
    supplier_type();
    calibration_type();
</script>
<?php } ?>					