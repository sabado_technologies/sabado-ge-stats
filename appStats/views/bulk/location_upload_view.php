<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:50%; z-index:2; opacity:0"><img src="<?php echo SITE_URL1;?>assets/images/ajax-loading-img.gif" /></div>
<div class="cl-mcont" id="orderCartContainer">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        	<?php echo $this->session->flashdata('response'); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
	                    <div class="content">
	                    	<form class="form-horizontal" id="bulkUploadFrm" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data" >
	                    		<div class="form-group">
	                                <label for="inputName" class="col-sm-4 control-label">Level ID</label>
	                                <div class="col-sm-3">
	                                 	<input type="text" name="level_id" class="form-control">
									</div>
	                            </div>
	                        	<div class="form-group">
	                                <label for="inputName" class="col-sm-4 control-label">Upload CSV File</label>
	                                <div class="col-sm-6">
	                                 	<input type="file" name="uploadCsv" id="uploadCsv" accept=".csv" >
	                                    <p><small>Note: Upload Data In Specified CSV Format Only</small></p>
									</div>
	                            </div>
	                            <div class="form-group">
	                                <div class="col-sm-offset-4 col-sm-6">
	                                    <button class="btn btn-primary upload_user" type="submit"  value ="1" name="bulkUpload"><i class="fa fa-cloud-upload"></i> Upload Data</button>
	                                    <a class="btn btn-danger" href="<?php echo SITE_URL;?>"><i class="fa fa-times"></i> Cancel</a>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
					</div>
				</div>
			</div>		    
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
