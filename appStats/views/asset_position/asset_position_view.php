<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>asset_position_data">
						
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$searchParams['asset_number'];?>"  class="form-control" maxlength="100">
								</div>
								<div class="col-sm-3">
									<?php
									if($task_access==2 || $task_access==3)
									{
										?>
										<select name="a_warehouse_id" class=" select2" > 
											<option value="">- Warehouse -</option>
											<?php
											foreach($warehouse as $row)
											{
												$selected = ($row['wh_id']==@$searchParams['a_warehouse_id'])?'selected="selected"':'';
												echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
											}?>
										</select>
										<?php

									}?>
								</div>
	                            <div class="col-sm-3 <?php if($task_access==1) echo "hidden";?>">
									<select name="fe_id" class="select2" > 
										<option value="">- Select SSO -</option>
										<?php 
											foreach(get_fe_users($task_access) as $st)
											{
												$selected = ($st['sso_id']==@$searchParams['fe_id'])?'selected="selected"':'';
												echo '<option value="'.$st['sso_id'].'" '.$selected.'>'.$st['name'].'</option>';
											}	
										?>
									</select>
								</div>
								<?php if($task_access == 3 && $_SESSION['header_country_id']==''){ ?>
								<div class="col-sm-3">
									<select name="a_country_id" class="main_status select2" >    
										<option value="">- Country -</option>
										<?php
										foreach($country_list as $row)
										{
											$selected = ($row['location_id']==@$searchParams['a_country_id'])?'selected="selected"':'';
											echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
										}?>
									</select>
								</div>
								<?php } ?>						
							</div>
						</div>	
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="from_date" readonly placeholder="From Date" value="<?php echo @$searchParams['from_date'];?>"  id = "dateFrom" class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="to_date" readonly placeholder="To Date" value="<?php echo @$searchParams['to_date'];?>" id="dateTo"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">
                                    <select name="status_id" class="select2" > 
										<option value="">- Asset Status -</option>
										<?php
										foreach($asset_status as $row)
										{
											$selected = ($row['asset_status_id']==@$searchParams['status_id'])?'selected="selected"':'';
											echo '<option value="'.$row['asset_status_id'].'" '.$selected.'>'.$row['name'].' </option>';
										}?>
									</select>
								</div>
								
								<div class="col-sm-3">													
									<button type="submit" name="search_asset" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Search"><i class="fa fa-search"></i> </button>
									<a href="<?php echo SITE_URL.'asset_position_data'; ?>" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i> </a>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center" width="4%"><strong>S.No</strong></th>
								<th class="text-center" width="7%"><strong>Asset Position Id</strong></th>
								<th class="text-center"><strong>Asset Number</strong></th>
								<th class="text-center" width="8%"><strong>Warehouse</strong></th>
								<th class="text-center" width="8%"><strong>FE</strong></th>
								<th class="text-center" width="18%"><strong>Asset Status</strong></th>
								<th class="text-center" width="8%"><strong>From Date</strong></th>
								<th class="text-center" width="8%"><strong>To Date</strong></th>
								<th class="text-center" width="8%"><strong>Country</strong></th>
								<th class="text-center" width="12%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($asset_results)>0)
							{
								foreach($asset_results as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['asset_position_id'];?></td>
										<td class="text-center"><?php echo @$row['asset_number'];?></td>
										<td class="text-center"><?php echo @$row['wh_name'];?></td>									
										<td class="text-center"><?php echo @$row['user_sso'];?></td>
										
										<td class="text-center"><?php echo @$row['asset_status'];?></td>
										<td class="text-center"><?php echo indian_format(@$row['from_date']);?></td>
										<td class="text-center"><?php if($row['to_date']!=''){ echo indian_format(@$row['to_date']);}  else {echo '';}?></td>
										<td class="text-center"><?php echo @$row['countryName'];?></td>
										<td class="text-center">
											<a class="btn btn-primary" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View"  href="<?php echo SITE_URL.'asset_position_view/'.storm_encode($row['asset_position_id']);?>"><i class="fa fa-eye"></i></a>
                                    	</td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="10" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
