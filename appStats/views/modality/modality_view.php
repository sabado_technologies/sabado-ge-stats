<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form class="form-horizontal" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
							<?php
	                        if($flg==2){
	                            ?>
	                            <input type="hidden" name="encoded_id" value="<?php echo storm_encode($lrow['modality_id']);?>">
	                            <input type="hidden" name="modality_id" id = "modality_id" value="<?php echo $lrow['modality_id'];?>">
	                            
	                            <?php
	                           }
	                        ?>

							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Modality Name <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<input type="text" autocomplete="off" required id="modality_name" class="form-control" placeholder="Modality Name" name="modality_name" value="<?php echo @$lrow['name']; ?>">
									</div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Modality Code <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<input type="text" autocomplete="off" required id="modality_code" minlength="2" maxlength="2" class="form-control" placeholder="Modality Code" name="modality_code" value="<?php echo @$lrow['modality_code']; ?>">
									</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-8">
									<button class="btn btn-primary" type="submit" name="submit_modality"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>modality"><i class="fa fa-times"></i> Cancel</a>

									<?php 
									if($flg ==2)
									{
										$data = 
										array('primary_key' => storm_encode(@$lrow['modality_id']),
									          'table_name'  => 'modality',
									          'work_flow'   => 'modality_master',
									          'details_of'  => @$lrow['name']);
										$sent_data = implode('~',$data); ?>

										<a class="btn btn-default" 
										href="<?php echo SITE_URL.'get_audit_page/'.$sent_data;?>"
										target="_blank"><i class="fa fa-book"></i> Audit Report</a>
									

									
									<?php 
									$data = 
										array('primary_key' => storm_encode(@$lrow['modality_id']),
									          'table_name'  => 'modality',
									          'work_flow'   => 'modality_master',
									          'details_of'  => @$lrow['name']);
										$sent_data = implode('~',$data); ?>
									<span class="block_page">
										<input type="hidden" value="<?php echo $sent_data; ?>" name="sent_data" class="sent_data">
										<a class="btn btn-default md-trigger audit_btn" 
									   data-toggle="modal" data-target="#form-primary" 
									   href="#"><i class="fa fa-book"></i> Audit Report Modal</a>
									</span>
									<?php } ?>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
		<?php 
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{   
	    	?>
			<div class="block-flat">
				<table class="table table-bordered"></table>
				<div class="content">
					<div class="row">
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>modality">
							<div class="col-sm-12">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="modality_name" value="<?php echo @$search_data['modality_name'];?>" class="form-control" placeholder="Modality Name">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="modality_code" minlength="2" maxlength="2" value="<?php echo @$search_data['modality_code'];?>" class="form-control" placeholder="Modality Code">
								</div>
								<div class="col-sm-3">
									<button type="submit" name="searchmodality" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
									<a href="<?php echo SITE_URL.'modality';?>" data-toggle="tooltip" class="btn btn-success" title="Refresh"><i class="fa fa-refresh"></i></a>
									<a href="<?php echo SITE_URL.'add_modality';?>" data-toggle="tooltip" class="btn btn-success" title="Add New"><i class="fa fa-plus"></i></a>
									<button type="submit" data-toggle="tooltip" title="Download" name="download_modality" value="1" formaction="<?php echo SITE_URL.'download_modality';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i></button>  
								</div>
							</div>
						</form>
					</div><br>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
	                            	<th class="text-center"><strong>S.No</strong></th>
	                                <th class="text-center"><strong>Modality Name</strong></th>
	                                <th class="text-center"><strong>Modality Code</strong></th>
	                                <th class="text-center"><strong>Action</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(count($modalityResults)>0)
								{
									foreach($modalityResults as $row)
									{
									?>
										<tr>
											<td class="text-center"><?php echo $sn++;?></td>
											<td class="text-center"><?php echo $row['name']; ?></td>
											<td class="text-center"><?php echo $row['modality_code']; ?></td>
											<td class="text-center">
											<?php if($row['status']==1){?>
	                                        <a class="btn btn-default" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_modality/'.storm_encode($row['modality_id']);?>" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
											<?php } else{?>
	                                        <a class="btn btn-default" style="padding:3px 3px;" href="#" readonly data-toggle="tooltip" title="Activate modality to edit"><i class="fa fa-pencil"></i></a>
											<?php }
	                                        if($row['status']==1){
	                                        ?>
	                                        <a class="btn btn-danger" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_modality/'.storm_encode($row['modality_id']);?>" data-toggle="tooltip" title="Deactivate"><i class="fa fa-trash-o"></i></a>
	                                        <?php
	                                        }
	                                        if($row['status']==2){
	                                        ?>
	                                        <a class="btn btn-info" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_modality/'.storm_encode($row['modality_id']);?>" data-toggle="tooltip" title="Activate"><i class="fa fa-check"></i></a>
	                                        <?php
	                                        }
	                                        ?>

											<!-- <?php 
											$data = 
												array('primary_key' => storm_encode(@$row['modality_id']),
											          'table_name'  => 'modality',
											          'work_flow'   => 'modality_master',
											          'details_of'  => @$row['name']);
												$sent_data = implode('~',$data); ?>
											<span class="block_page">
												<input type="hidden" value="<?php echo $sent_data; ?>" name="sent_data" class="sent_data">
												<a 
												class="btn btn-default md-trigger audit_btn" 	style="padding:3px 3px;" 
											   	data-toggle="modal" 
											   	data-target="#form-primary" 
											   	href="#"
											   	data-toggle="tooltip"
											   	title="Audit Trail Modal"><i class="fa fa-book"></i></a>
											</span> -->
	                                    </td>
										</tr>
							<?php   }
								} else {?>
									<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
	                    <?php 	} ?>
							</tbody>
						</table>
	                </div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>          		
				</div>
			</div>	
		<?php } ?>
		</div>
	</div>
</div>

<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	is_modalitynameExist();
	is_modalitycodeExist();
</script>