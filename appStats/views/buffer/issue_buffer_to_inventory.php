<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:50%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>
	<div class="cl-mcont " id="orderCartContainer">
	    <div class="row"> 
	        <div class="col-sm-12 col-md-12">
			    <?php echo $this->session->flashdata('response'); ?>			
				<div class="block-flat">
					<div class="content">
						<div class="row">
							<form method="post">
								<input type="hidden" name="country_id" value="<?php echo @$country_id; ?>">
								<div class="col-md-4">
									<p style="margin-left: 15px;">Buffer Stock In : <strong><?php echo @$country_name; ?></strong></p>
								</div>
								<div class="col-md-offset-6 col-md-2" style="margin-bottom: 10px;">
									<button type="submit" formaction="<?php echo SITE_URL.'buffer_to_inventory/0'; ?>" name="reset" value="1" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Add More to cart</button>
								</div>
							</form>
						</div>
						<form class="form-horizontal" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
							<input type="hidden" name="country_id" value="<?php echo @$country_id; ?>">
							<div class="row">
								<div class="table-responsive">
									<table class="table table-bordered hover" id="mytable">
										<thead>
											<tr>
												<th class="text-center"><strong>S.No.</strong></th>
												<th class="text-center"><strong>Tool Number</strong></th>
												<th class="text-center"><strong>Tool Description</strong></th>								
												<th class="text-center"><strong>Asset Number</strong></th>
												<th class="text-center"><strong>Serial Number</strong></th>
												<th class="text-center"><strong>Asset Status</strong></th>
												<th class="text-center"><strong>Modality</strong></th>
												<th class="text-center"><strong>Tool Availability</strong></th>
												<th class="text-center"><strong>Action</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php $i= 1;
											if(count(@$asset_list) >0)
											{
												foreach($asset_list as $row)
												{
													
													?>
													<tr class="toolSelectedRow">
														<td class="text-center"><?php echo $i++;?></td>									
														<td class="text-center"><?php echo @$row['part_number'];?></td>
														<td class="text-center"><?php echo @$row['part_description'];?></td>
														<td class="text-center"><?php echo @$row['asset_number'];?></td>
														<td class="text-center"><?php echo @$row['serial_number'];?></td>
														<td class="text-center"><?php echo @$row['asset_status'];?></td>
														<td class="text-center"><?php echo @$row['modality_name'];?></td>
														<td class="text-center"><?php echo get_asset_position($row['asset_id']);?></td>
														<td class="text-center"><a class="btn btn-danger btn-xs removeRow" 
															data-asset_id="<?php echo $row['asset_id']; ?>" href="#"  >
														<i class="fa fa-times"></i></a></td>
													</tr>

											<?php	} 
											}
											else {
											?>	<tr><td colspan="9" align="center"><span class="label label-primary">No Records</span></td></tr>
											<?php } ?>								
										</tbody>
									</table>
								</div>
							</div><br>
							<div class="row">
								<div class=" col-md-12">
									<div class="form-group">
										<label for="inputName" class="col-sm-4 control-label">To Warehouse <span class="req-fld">*</span></label>
										<div class="col-sm-5">
											<select name="wh_id" required class="select2" > 
											<option value="">- To Warehouse -</option>
											<?php 
												foreach($wh_data as $st)
												{
													
													echo '<option value="'.$st['wh_id'].'" >'.$st['wh_code'].' -('.$st['name'].')</option>';
												}
											?>
										</select>
										</div>

									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-success" onclick="return confirm('Are you sure you want to Move Assets to Inventory ?')" type="submit" value="1"  name="submitorder" id="submitOrder"><i class="fa fa-check"></i> Submit</button>
											<a class="btn btn-danger" href="<?php echo SITE_URL;?>buffer_to_inventory"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
							</div>
						</form>
					</div>	             	          		
				</div>
			</div>		
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
	 
	

			
		
