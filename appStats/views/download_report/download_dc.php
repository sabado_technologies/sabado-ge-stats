<!DOCTYPE html>
<!-- Generated by PHPExcel - http://www.phpexcel.net -->
<html>
<?php $in = 1;
$count = count($result);
?>
  <head>
    <title>DC Print</title>
    <link rel="shortcut icon" href="<?php echo assets_url(); ?>images/favicon_1.png" />
    <style type="text/css">

    body,table,td {
    margin: 0;
    padding: 0;
    font-size: 15px;
    font-family: calibri;
    }
     th, td {
        border: 1px solid black;
    }
    
    .tool_table td{
      font-size: 13px;
      overflow-wrap: break-word;
      font-family: calibri;
    }

    .page {
    width: 21cm;
    padding: 0cm;
    min-height: 32cm;
    margin: 1cm auto;
    background: white;
      }
      @page {
        size: A4;
        margin: 0cm auto;
      }
      @media print 
      {
        html, body 
        {
          width: 21cm;
          <?php if($in!=$count)
          {?>
            height: 32cm; <?php
          }?>
        }
        .no-print, .no-print *
          {
              display: none !important;
          }
      }
      table {
          border-collapse: collapse;
      }
    </style>
  </head>
  <body>
    <div style="text-align:center" class="col-md-12">
      <div class="col-md-6">
        <button class="button no-print"  onclick="print_srn()" style="background-color:#3598dc;margin-top: 20px;">Download PDF</button>
        <a class="button no-print"  href="<?php echo SITE_URL.'download_report'; ?>">Close</a>
      </div>
      <div class="col-md-6">
        <h3 class="button no-print">Download from <span style="color: red;" class="button no-print">Google Chrome</span> For Better Quality</h3>
      </div>
    </div>
    <?php 
    foreach ($result as $key => $value)
    { ?>
      <div class="page" style="<?php if($count==$in){ echo 'min-height: 29.7cm !important;'; } ?>">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td colspan=16 align="center" style="border-bottom: none;">
                <img src='<?php echo assets_url()."images/wipro.png"?>' align="left" style="max-width:9%; padding:3px; margin:0px;"/>
                <img src='<?php echo assets_url()."images/ge.png"?>' align="left" style="max-width:9%; padding:3px; margin:0px;"/>
                <div style="margin-right: 135px;">
                <h3 style="margin-bottom: 0px;margin-top: 5px;">WIPRO GE HEALTHCARE PRIVATE LIMITED</h3>
                 <h4 style="margin-bottom: 10px;margin-top: 10px;"><?php echo @$value['bill_address1'].', '.@$value['bill_address2'].',<br>'.@$value['bill_address3'].', '.@$value['bill_address4'].',<br>GST# : '.@$value['bill_gst_number'].', '.@$value['bill_pin_code']; ?>
                </h4>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan=16 align="center" style="border-bottom: none;border-top: none;">
                <h3 style="margin-bottom: 0px;margin-top: 0px;">DELIVERY CHALLAN</h3>
              </td>
            </tr>
            <tr>
              <td align="center" style="font-size: 13px;" width="25%" ><b>GST No.</b></td>
              <td colspan="10" align="center" width="25%"><?php echo $value['bill_gst_number']; ?></td>
              <td align="center" style="font-size: 13px;" width="25%"><b>Challan No.</b></td>
              <td colspan="4" align="center" width="25%"><?php echo $value['format_number']; ?></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 13px;"><b>PAN No.</b></td>
              <td colspan="10" align="center"><?php echo $value['bill_pan_number']; ?></td>
              <td align="center" style="font-size: 13px;"><b>Challan Date</b></td>
              <td colspan="4" align="center"><?php echo date('d-m-Y',strtotime(@$value['created_time'])); ?></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 13px;"><b>
                <?php if(@$value['order_number']!="")
                {
                  echo "Order No#";
                }
                else if(@$value['stn_number']!='')
                {
                  echo "STN Number#";
                }
                else if(@$value['crn_number']!='')
                {
                  echo "CR/RR Number#";
                }
                else if(@$value['return_number']!='')
                {
                  echo "Return Number#";
                }
                else
                {
                  echo "Order No#";
                } ?>
              </b></td>
              <td colspan="10" align="center">
              <?php if(@$value['order_number']!="")
              {
                echo @$value['order_number'];
              }
              else if(@$value['stn_number']!='')
              {
                echo @$value['stn_number'];
              }
              else if(@$value['crn_number']!='')
              {
                echo @$value['crn_number'];
              }
              else if(@$value['return_number'])
              {
                echo @$value['return_number'];
              } ?></td>
              <td align="center" style="font-size: 13px;"><b>SR# :</b></td>
              <td colspan="4" align="center"><?php echo @$value['siebel_number']; ?></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 13px;"><b>
                <?php 
                if(@$value['order_number']!='')
                {
                  echo "FE Name & SSO";
                }
                else if(@$value['stn_number']!='')
                {
                  if(@$value['fe_name1']!='' && @$value['fe_check']==1)
                  {
                    echo "FE Name & SSO";
                  }
                  else if(@$value['to_wh_name']!='' && @$value['fe_check']==0)
                  {
                    echo "WH Name";
                  }
                }
                else if(@$value['supplier_name']!='')
                {
                  echo "Supplier Name";
                }
                else 
                {
                  echo "FE Name & SSO";
                }
                ?></b></td>
              <td colspan="10" align="center">
                <?php 
                if(@$value['order_number']!='')
                {
                  echo @$value['fe_name'];
                }
                else if(@$value['stn_number']!='')
                {
                  if(@$value['fe_name1']!='' && @$value['fe_check']==1)
                  {
                    echo @$value['fe_name1'];
                  }
                  else if(@$value['to_wh_name']!='' && @$value['fe_check']==0)
                  {
                    echo @$value['to_wh_name'];
                  }
                }
                else if(@$value['supplier_name']!='')
                {
                  echo @$value['supplier_name'];
                }
                else 
                {
                  echo @$value['fe_name'];
                }
                ?></td>
              <td align="center" style="font-size: 13px;"><b><?php 
                if(@$value['order_number']!='')
                {
                  echo "FE Contact#";
                }
                elseif (@$value['stn_number']!='') 
                {
                  if(@$value['fe_contact_number1']!='' && @$value['fe_check']==1)
                  {
                    echo "FE Contact#";
                  }
                  else if(@$value['to_wh_contact_number']!='' && @$value['fe_check']==0)
                  {
                    echo "WH Contact#";
                  }
                }
                else if(@$value['supplier_contact_number']!='')
                {
                  echo "Supplier Contact#";
                }
                else {
                  echo "FE Contact#";
                }
                ?></b></td>
              <td colspan="4" align="center">
                <?php 
                if(@$value['order_number']!='')
                {
                  echo @$value['fe_contact_number'];
                }
                elseif (@$value['stn_number']!='') 
                {
                  if(@$value['fe_contact_number1']!='' && @$value['fe_check']==1)
                  {
                    echo @$value['fe_contact_number1'];
                  }
                  else if(@$value['to_wh_contact_number']!='' && @$value['fe_check']==0)
                  {
                    echo @$value['to_wh_contact_number'];
                  }
                }
                else if(@$value['supplier_contact_number']!='')
                {
                  echo @$value['supplier_contact_number'];
                }
                else {
                  echo @$value['fe_contact_number'];
                }
                ?>
              </td>
            </tr>
            <tr>
              <td align="center" style="font-size: 13px;"><b>
                <?php 
                if(@$value['courier_number']!='')
                { 
                  echo "Transporter Name & Docket#";
                }
                else if(@$value['vehicle_number']!='')
                { 
                  echo "vehicle Number";
                }
                else 
                { 
                  echo "Transporter Name"; 
                } ?></b></td>
              <td colspan="10" align="center">
                <?php 
                if(@$value['courier_number']!='')
                { 
                  echo @$value['courier_name'].' & '.@$value['courier_number'];
                }
                else if(@$value['vehicle_number']!='')
                { 
                  echo @$value['vehicle_number'];
                }
                else 
                { 
                  echo "--";
                } ?>
              </td>
              <td align="center" style="font-size: 13px;"><b>Eway bill</b></td>
              <td colspan="4" align="center"><?php echo @$value['airway_bill']; ?></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 13px;"><b>Contact/Driver Name</b></td>
              <td colspan="10" align="center"><?php echo @$value['contact_person']; ?></td>
              <td align="center" style="font-size: 13px;"><b>Vendor Contact#</b></td>
              <td colspan="4" align="center"><?php echo @$value['phone_number']; ?></td>
            </tr>
            <tr height=30>
              <td colspan="11" valign="bottom" style="border-bottom: none"><div style="margin-left: 8px;"><b>Dispatched From :</b></div></td>
              <td colspan="4" valign="bottom" style="border-bottom: none"><div style="margin-left: 8px;"><b>Ship To :</b></div></td>
            </tr>
            <tr height=120>
              <td colspan="11" style="border-bottom: none;border-top: none;" align="left" valign="top"><div style="margin-left: 8px;"><?php echo @$value['from_address1'].',<br>'.@$value['from_address2'].',<br>'.@$value['from_address3'].',<br>'.@$value['from_address4'].',<br>'.@$value['from_pin_code']; ?><BR>For,  WIPRO GE HEALTHCARE PVT LTD</div></td>
              <td colspan="5" style="border-bottom: none;border-top: none;" align="left" valign="top" ><div style="margin-left: 8px;"><?php echo @$value['to_address1'].',<br>'.@$value['to_address2'].',<br>'.@$value['to_address3'].',<br>'.@$value['to_address4'].',<br>'.@$value['to_pin_code']; ?><BR>For,  WIPRO GE HEALTHCARE PVT LTD</div></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 13px;"><b>GST No. </b></td>
              <td colspan="10" align="center"><?php if(@$value['from_gst_number']!=''){echo @$value['from_gst_number'];}else { echo "--";} ?></td>
              <td align="center" style="font-size: 13px;"><b>GST NO. </b></td>
              <td colspan="4" align="center"><?php if(@$value['to_gst_number']!=''){echo @$value['to_gst_number'];}else { echo "--";} ?></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 13px;border-bottom: none;"><b>PAN No.</b></td>
              <td colspan="10" align="center" style="border-bottom: none;"><?php if(@$value['from_pan_number']!=''){echo @$value['from_pan_number'];}else { echo "--";} ?>
              </td>
              <td align="center" style="font-size: 13px;border-bottom: none;"><b>PAN No. </b></td>
              <td colspan="4" align="center" style="border-bottom: none;"><?php if(@$value['to_pan_number']!=''){echo @$value['to_pan_number'];}else { echo "--";} ?></td>
            </tr>
          </tbody>
        </table>
        <table cellspacing="0" border="0" width="100%" style="border-top: none;max-width: 100%" class="tool_table">
          <tr>
            <td align="center" rowspan="2"><b>SNo.</b></td>
            <td align="center" rowspan="2"><b>Tool Number</b></td>
            <td align="center" rowspan="2" width="25%"><b>Tool Description</b></td>
            <td align="center" rowspan="2"><b>HSN Code</b></td>
            <td align="center" rowspan="2"><b>Qty</b></td>
            <td align="center" rowspan="2"><b>Tool Value</b></td>
            <td align="center" colspan="2"><b>CGST</b></td>
            <td align="center" colspan="2"><b>SGST</b></td>
            <td align="center" colspan="2"><b>IGST</b></td>
            <td align="center" rowspan="2"><b>Value <?php echo @$value['currency_name']; ?></b></td>
          </tr>
          <tr>
            <td align="center">Rate</td>
            <td align="center">Amt</td>
            <td align="center">Rate</td>
            <td align="center">Amt</td>
            <td align="center">Rate</td>
            <td align="center">Amt</td>
          </tr>
          <?php $sn = 1;
          foreach ($value['tools_list'] as $key1 => $value1) 
          { ?>
            <tr>
            <td align="center"><?php echo $sn++; ?></td>
            <td align="center"><?php echo $value1['part_number']; ?></td>
            <td align="center"><?php echo $value1['part_description'];?></td>
            <td align="center"><?php echo $value1['hsn_code']; ?></td>
            <td align="center"><?php echo $value1['quantity']; ?></td>
            <td align="center"><?php echo indian_format_price(round($value1['taxable_value'])); ?></td>
            <td align="center"><?php echo indian_format_price(round($value1['sgst_percent'],2));?></td>
            <td align="center"><?php echo indian_format_price(round($value1['sgst_amt']));?></td>
            <td align="center"><?php echo indian_format_price(round($value1['cgst_percent'],2));?></td>
            <td align="center"><?php echo indian_format_price(round($value1['cgst_amt']));?></td>
            <td align="center"><?php echo indian_format_price(round($value1['igst_percent'],2));?></td>
            <td align="center"><?php echo indian_format_price(round($value1['igst_amt']));?></td>
            <td align="center"><?php echo indian_format_price(round($value1['amount'])); ?></td>
          </tr>
          
          <?php } 
          for($inc=0;$inc<3;$inc++){?>
          <tr height="16">
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
            <td style="border-bottom: none;"></td>
          </tr>
          <?php } ?>

          <tr height="21">
            <td align="center" colspan="2" style="font-size: 13px;"><b>Serial : Asset No.</b></td>
            <td colspan="11" align="left" style="font-size: 13px;padding-left: 2px;">
              <?php $inc = 1;
                foreach (@$value['tools_list'] as $key1 => $val1) 
                {
                  echo $inc++.'-('.@$val1['asset_number_list'].'), ';
                } ?>
            </td>
          </tr>
          <tr height="21">
            <td colspan="10" align="left" style="font-size: 13px;border-bottom: none;"><b>Value In <?php echo @$value['currency_name'];?> :</b> <?php echo ucwords(convert_number_to_words(round(@$value['total_amount']))); ?></td>
            <td colspan="3" align="center" style="font-size: 13px;border-bottom: none;"><b>Total Val : <?php echo @$value['currency_name'].' '.indian_format_price(round(@$value['total_amount']));  ?> </b></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0">
          <tbody>     
            <tr height="100">
              <td width="50%" align="center"><!-- 
                *CT - Customer To Trunk Part<br />
                *TC - Trunk To Customer Part -->
              </td>
              <td rowspan="3">Signature <br />
                <br />
                Name of the Signatory<br />
                <br />
                <br />
                Designation/Status
              </td>
            </tr>
            <tr>
              <td><b>Declaration</b></td>
            </tr>
            <tr>
              <td >Certified that the particulars given above are true and correct</td>
            </tr>      
          </tbody>
        </table>
      </div>
    <?php 
    $in++;
    } 
    ?>
</body>
</html>
<script type="text/javascript">
  window.print(); 
  function print_srn()
  {
    window.print(); 
  }
</script>