<?php $this->load->view('commons/main_template',$nestedView); ?>
<style type="text/css">
    .radio-inline{ padding-left: 10px !important;}
    .radio-inline input[type="radio"]{ margin: 0px 4px 0px 0px;}
</style>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
            <div class="block-flat">
                <table class="table table-bordered"></table>
                <div class="content">
                    <div class="row">
                        <form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'download_report_format'; ?>" parsley-validate novalidate>
                            <div class="col-md-12">
                                <div class="form-group">
                                <div class="col-md-12">
                                    <label for="inputName" class="col-sm-2 control-label">Download :</label>
                                    <div class="custom_icheck col-sm-3">
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="download" value="1" name="download" checked style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            Excel 
                                        </label>
                                        <label class="radio-inline" style="margin-left: 32px !important;"> 
                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="download" value="2" name="download"  style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            PDF
                                        </label>
                                    </div>
                                    <label for="inputName" class="col-sm-2 control-label">Type :</label>
                                    <div class="custom_icheck col-sm-4">
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="type" value="3" name="type" checked style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            DC With In State
                                        </label><br>
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="type" value="4" name="type"  style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            DC Out Of State
                                        </label><br>
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="type" value="1" name="type" style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            Invoice
                                        </label><br>
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="type" value="2" name="type"  style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            DC Old Format
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="inputName" class="col-sm-2 control-label">Register :</label>
                                    <div class="custom_icheck col-sm-3">
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="register" value="1" name="register" checked style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            Outward 
                                        </label>
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="register" value="2" name="register"  style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            Inward
                                        </label>
                                    </div>
                                    <label for="inputName" class="col-sm-2 control-label cancel_label">Cancelled Invoice :</label>
                                    <div class="custom_icheck col-sm-4">
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="cancel_type" value="1" name="cancel_type" checked style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            Exclude
                                        </label>
                                        <label class="radio-inline" style="margin-left: 40px !important;"> 
                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="cancel_type" value="2" name="cancel_type"  style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            Include
                                        </label>
                                        <label class="radio-inline"> 
                                            <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                <input type="radio" class="cancel_type" value="3" name="cancel_type"  style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            </div> 
                                            Only
                                        </label>
                                    </div>
                                </div>
                            </div>
                                <div class="form-group">
                                    <?php
                                    if($task_access == 3 && @$_SESSION['header_country_id']=='')
                                    {  ?>
                                        <label for="inputName" class="col-md-2 control-label text_label">Country </label>
                                        <div class="col-sm-3">
                                            <select name="country_id" class="country_id select2" required>    
                                                <option value="">- Country -</option>
                                                <?php
                                                foreach($country_list as $row)
                                                {
                                                    $selected = ($row['location_id']==@$searchParams['country_id'])?'selected="selected"':'';
                                                    echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                                }?>
                                            </select>
                                        </div> <?php
                                    }
                                    else
                                    { ?>
                                        <input type="hidden" name="country_id" value="<?php echo $country_id; ?>" class="c_id"><?php 
                                    } ?>
                                    <label for="inputName" class="col-md-2 control-label text_label">Invoice Number </label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" autocomplete="off" name="print_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-md-2 control-label">State</label>
                                    <div class="col-md-3">
                                        <select class="select3 state_id" name="state_id" style="width:100%">
                                                <option value="">- State -</option>
                                                 <?php
                                                foreach($state_list as $row)
                                                {
                                                    echo '<option value="'.$row['location_id'].'">'.$row['name'].'</option>';
                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <label for="inputName" class="col-md-2 control-label">Warehouse</label>
                                    <div class="col-md-3">
                                        <select class="select4 wh_id" name="wh_id" style="width:100%">
                                                <option value="">- Warehouse -</option>
                                                 <?php
                                                foreach($warehouse_list as $row)
                                                {
                                                    $selected = '';
                                                    if($row['wh_id'] == @$warehouse){ $selected = 'selected';}
                                                    echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' -('.$row['name'].')</option>';
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="inputName" class="col-md-2 control-label">From Date</label>
                                    <div class="col-md-3">
                                        <input class="form-control" size="16" type="text" autocomplete="off" readonly placeholder="From Date" name="from_date" id="dateFrom" style="cursor:hand;background-color: #ffffff">
                                    </div>
                                    <label for="inputName" class="col-md-2 control-label">To Date</label>
                                    <div class="col-md-3">
                                        <input class="form-control" size="16" type="text" autocomplete="off" readonly placeholder="To Date" name="to_date" id="dateTo" style="cursor:hand;background-color: #ffffff">
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-sm-12"><br>
                                <div class="col-sm-offset-4 col-sm-6">
                                    <button class="btn btn-primary" type="submit" value="1" name="submit"><i class="fa fa-check"></i> Submit</button>
                                    <a href="<?php echo SITE_URL.'download_report'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                </div>
                            </div>
                        </form>
                    </div><br>              
                </div>
            </div>  
        </div>
    </div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
    $(document).ready(function(){
    $('.select3, .select4').select2();
});

</script>