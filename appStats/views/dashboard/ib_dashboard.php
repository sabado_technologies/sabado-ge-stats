<?php $this->load->view('commons/main_template',$nestedView); ?>
<style type="text/css">
    .radio-inline{ padding-left: 10px !important;}
    .radio-inline input[type="radio"]{ margin: 0px 4px 0px 0px;}
</style>
 <div class="cl-mcont">
 	<div id="loaderID" style="position:fixed; top:50%; left:58%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
			<div class="block-flat">
				<div class="content">
					<div class="row">
	                    <div class="form-group">
	                        <div class="col-sm-12">
	                            <div class="col-sm-offset-2 col-sm-4 fix_country">
	                                <select class="select2 country_id" name="country_id" >
	                                    <option value="">- Country -</option>
	                                     <?php
	                                    foreach($countryList as $country)
	                                    {
	                                        $selected = ($country['location_id']==$searchParams['country_id'])?'selected="selected"':'';
	                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
	                                    }
	                                    ?>
	                                </select>
	                            </div>
	                            <div class="col-sm-offset-2 col-sm-4  fix_zone">
                                    <div class="btn-group">
                                        <input type="button" data-id="1" name="zone" class="zone btn btn-success" checked  value="All">
                                        <?php foreach ($zones as $zone) { ?>
                                            <input type="button" data-id="<?php echo $zone['location_id']; ?>" name="zone" class="zone btn btn-default" value="<?php echo $zone['name']; ?>">
                                      <?php  }  ?>
                                    </div>
	                            </div> 
	                            <div class="col-sm-4">
	                                <select class="select2 select3 warehouse" style="width:100%" name="warehouse">
	                                    <option value="">Select Warehouse</option>
	                                    <?php
	                                    foreach ($warehouses as $wh_row) {
	                                        echo '<option value="'.$wh_row['wh_id'].'">'.$wh_row['wh_code'].' - '.$wh_row['name'].'</option>';
	                                    }
	                                    ?>
	                                </select>
	                            </div>
	                            
	                        </div>

	                        <div class="col-sm-12" style="margin-top: 10px;">
	                        	<div class="col-sm-offset-2 col-sm-4">
	                                <select class="select2 modality" style="width:100%" name="modality">
	                                    <option value="">Select Modality</option>
	                                    <?php
	                                    foreach ($modality as $mrow) {
	                                        echo '<option value="'.$mrow['modality_id'].'">'.$mrow['name'].'</option>';
	                                    }
	                                    ?>
	                                </select>
	                            </div>
	                            <div class="col-sm-4" >
	                                <select class="tool" style="width:100%">
	                                    <option value="">Select Tool</option>
	                                    
	                                </select>
	                            </div>
	                        </div>
	                    </div>
	                </div>	
	                <div class="row">
	                    <div class="header"></div>
	                    <table class="table table-bordered" ></table>
	                </div>
					<div class="row"><br>
						<div class="col-md-12">
							<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto">
							</div>
						</div>
					</div>
					<div class="row"><br>
						<div class="col-md-12">
							<div id="container2" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">

	Highcharts.setOptions({ colors: [ '#3F51B5','#FF9800','#F44336', '#4CAF50', '#9C27B0', '#795548', '#FFEB3B', '#CDDC39']});
	check_filters();
	select2Ajax('tool', 'get_ib_tools', 0, 3);
	function check_filters()
	{
	    var task_access = <?php echo $task_access; ?>;

	    var session_country_id =<?php if( $searchParams['country_id']!='')
	    { echo $searchParams['country_id'] ; } else { echo 0; }  ?> ;
	    if(session_country_id>0)
	    {
	        $('.fix_zone').show();
	        $('.fix_country').hide();
	    }
	    else if(task_access ==3 & session_country_id==0)
	    {
	        $('.fix_zone').hide();
	        $('.fix_country').show();
	    }
	}
	var zone=1;
   $('.zone').click(function(){
    var zone = $(this).attr('data-id');
    $('.zone').addClass('btn-default').removeClass('btn-success');
    $(this).addClass('btn-success').removeClass('btn-default');
    var data = 'zone='+zone;
    $.ajax({
        url: SITE_URL + 'getWarehousesByZone',
        type:"POST",
        data:data,
        success: function(data){
            //alert(data);
            $('.warehouse').html('');
            $('.warehouse').html(data);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
            filterChart1();

        }
    });
});
   $('.country_id').change(function(){
    var country_id = $('.country_id :selected').val();
    var modality_id = $('.modality :selected').val();
    var data = [{modality:modality_id,country_id:country_id}];
    select2Ajax('tool', 'get_ib_tools', data, 3);
    filterChart1();
    
});
	$('.modality').change(function(){

	    var modality = $('.modality :selected').val();
	    var country_id = $('.country_id :selected').val();
	    var data = [{modality:modality,country_id:country_id}];
	    select2Ajax('tool', 'get_ib_tools', data, 3);
	    filterChart1();
	});

	$('.warehouse,.tool').change(function(){
		filterChart1();
	});

	function filterChart1()
	{
	    var zone =$('.btn-success').attr('data-id');
	    var warehouse = $('.warehouse :selected').val();
	    var modality = $('.modality :selected').val();
	    var tool_id = $('.tool :selected').val();
	    var country_id =$('.country_id :selected').val();
	    var data = 'zone='+zone+'&warehouse='+warehouse+'&modality_id='+modality+'&tool_id='+tool_id+'&country_id='+country_id;
	    //alert(zone+'-->'+warehouse+'-->'+modality);
	    // remove second level chart
	    $("#pcont").css("opacity",0.5);
	    $("#loaderID").css("opacity",1);
	    $.ajax({
	        url: SITE_URL + 'get_tool_ib_data',
	        type:"POST",
	        data:data,
	        dataType:'json', 
	        success: function(data){
	            getCharts(data)
	            $("#pcont").css("opacity",1);
	            $("#loaderID").css("opacity",0);
	      		$('#container2').hide();
	        }
	    });
	}
	
	var chartsData = <?php echo $chartsData ?>;
	getCharts(chartsData);

	function getCharts(chartsData) 
	{
		$('#container2').hide();
		var modality_list = chartsData['modality_list'];
		var CustomerData = chartsData['CustomerData'];
		var ToolData = chartsData['ToolData'];	
		Highcharts.chart('container', {
		    chart: {
		        type: 'bar'
		    },
		    title: {
		        text: 'Modality VS Install Base'
		    },
		    xAxis: {
		        categories: modality_list,
		        title: {
		            text: null
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Tools Count',
		            align: 'high'
		        },
		        labels: {
		            overflow: 'justify'
		        }
		    },
		    tooltip: {
		        valueSuffix: ''
		    },
		    plotOptions: {
		        bar: {
		            dataLabels: {
		                enabled: true
		            }
		        },
		        series: {
		        cursor: 'pointer',
		        point: {
		            events: 
		            {
		                click: function () 
		                {
		                    $('#container2').show();
		                   	filterChart2(this.category);
		                }
		            }
		        }
		    }

		    },
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'top',
		        x: -1,
		        y: 80,
		        floating: true,
		        borderWidth: 1,
		        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
		        shadow: true
		    },
		    credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Customer',
		        data: CustomerData
		    }, {
		        name: 'Modality',
		        data: ToolData
		    }]
		});
	}


	function filterChart2(category)
	{
	    var zone =$('.btn-success').attr('data-id');
	    var warehouse = $('.warehouse :selected').val();
	    var modality = category;
	    var tool_id = $('.tool :selected').val();
	    var country_id = $('.country_id :selected').val();
	    var data = 'zone='+zone+'&warehouse='+warehouse+'&modality='+modality+'&tool_id='+tool_id+'&country_id='+country_id;

	    $("#pcont").css("opacity",0.5);
	    $("#loaderID").css("opacity",1);
	    $.ajax({
	        url: SITE_URL + 'get_tool_modality_ib_data',
	        type:"POST",
	        data:data,
	        dataType:'json', 
	        success: function(data){
	        	var toolData = data['ToolData'];
                if(toolData.length>0)
                {
		            draw_chart2(data)
		            $("#pcont").css("opacity",1);
		            $("#loaderID").css("opacity",0);
		            $('#container2').show();
		            $('#container').show();
		        }
		        else
		        {   
		        	 $('#container2').hide();
		        	alert("Tool List is Not Available For "+category);
		        	$("#pcont").css("opacity",1);
		            $("#loaderID").css("opacity",0);
		        }
	        }
	    });
	}

	function draw_chart2(chartsData1) 
	{
		var tool_name_list = chartsData1['tool_name_list'];
		var CustomerData = chartsData1['CustomerData'];
		var ToolData = chartsData1['ToolData'];	
		Highcharts.chart('container2', {
		    chart: {
		        type: 'bar'
		    },
		    title: {
		        text: 'Tools VS Install Base'
		    },
		    xAxis: {
		        categories: tool_name_list,
		        title: {
		            text: null
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Tools Count',
		            align: 'high'
		        },
		        labels: {
		            overflow: 'justify'
		        }
		    },
		    tooltip: {
		        valueSuffix: ''
		    },
		    plotOptions: {
		        bar: {
		            dataLabels: {
		                enabled: true
		            }
		        },
			},
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'top',
		        x: -1,
		        y: 80,
		        floating: true,
		        borderWidth: 1,
		        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
		        shadow: true
		    },
		    credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Customer',
		        data: CustomerData
		    }, {
		        name: 'Tool',
		        data: ToolData
		    }]
		});
	}


</script>