<?php $this->load->view('commons/main_template', $nestedView);

?>
<div class="cl-mcont">
<div id="loaderID" style="position:fixed; top:50%; left:58%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>

<div class="row"> 
    <div class="col-sm-12 col-md-12">
        <div class="block-flat">
            <div class="content">           
                    
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="col-sm-3 fix_country">
                                <select class="select2 country_id" name="country_id" >
                                    <option value="">- Country -</option>
                                     <?php
                                    foreach($countryList as $country)
                                    {
                                        $selected = ($country['location_id']==$searchParams['country_id'])?'selected="selected"':'';
                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                             <div class="col-sm-5  fix_zone">
                                    <div class="btn-group">
                                        <input type="button" data-id="1"  data-zone ="" name="zone" class="zone btn btn-success" checked  value="All">
                                        <?php foreach ($zones as $zone) { ?>
                                            <input type="button" data-id="<?php echo $zone['location_id']; ?>" name="zone" data-zone ="<?php echo $zone['name']; ?>" class="zone btn btn-default" value="<?php echo $zone['name']; ?>">
                                      <?php  }  ?>
                                    </div>
                            </div> 
                            
                            <div class="col-sm-4">
                                <select class="select2 select3 user_id" style="width:100%" name="user_id">
                                    <option value="">Select FE</option>
                                    <?php
                                    foreach ($fe_users as $fe_row) {
                                        echo '<option value="'.$fe_row['sso_id'].'">'.$fe_row['name'].' ('.$fe_row['sso_id'].')</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-11" align="center">
                            <div id="container1" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-11" align="center">
                            <div id="container2" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-11" align="center">
                            <div id="container3" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                        
                </div>

            </div>
        </div>              
    </div>
</div>
</div>

<style type="text/css">
    .radio-inline{ padding-left: 10px !important;}
    .radio-inline input[type="radio"]{ margin: 0px 4px 0px 0px;}
</style>

<?php 
$this->load->view('commons/main_footer.php', $nestedView); ?>

<script type="text/javascript">

Highcharts.setOptions({ colors: [ '#42A5F5','#A1887F','#FFD54F','#3F51B5','#FF9800','#F44336', '#4CAF50', '#9C27B0', '#795548', '#FFEB3B', '#CDDC39']});

check_filters();
function check_filters()
{
    var task_access = <?php echo $task_access; ?>;

    var session_country_id =<?php if( $searchParams['country_id']!='')
    { echo $searchParams['country_id'] ; } else { echo 0; }  ?> ;
    if(session_country_id>0)
    {
        $('.fix_zone').show();
        $('.fix_country').hide();
    }
    else if(task_access ==3 & session_country_id==0)
    {
        $('.fix_zone').hide();
        $('.fix_country').show();
    }
}

//var icrm = $.noConflict();

var zone=1;
$('.zone').click(function(){
    var zone = $(this).attr('data-id');
    $('.zone').addClass('btn-default').removeClass('btn-success');
    $(this).addClass('btn-success').removeClass('btn-default');
    var data = 'zone='+zone;
    $.ajax({
        url: SITE_URL + 'getFeUsersByZone',
        type:"POST",
        data:data,
        success: function(data){
            $('.user_id').html('');
            $('.user_id').html(data);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
            filterFeTools();
        }
    });
});
$('.country_id').change(function(){
    var country_id = $('.country_id :selected').val();
    var task_access = <?php echo $task_access; ?>;
    var data = 'country_id='+country_id+'&task_access='+task_access;
    $.ajax({
        url: SITE_URL + 'get_fe_users_by_country',
        type:"POST",
        data:data,
        success: function(data){
            $('.user_id').html('');
            $('.user_id').html(data);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
            filterFeTools();
        }
    });
});
$('.user_id').change(function(){
    
    filterFeTools();
});

function filterFeTools()
{
    var zone =$('.btn-success').attr('data-id');
    var country_id = $('.country_id :selected').val();
    var user_id = $('.user_id :selected').val();
    var data = 'zone='+zone+'&user_id='+user_id+'&country_id='+country_id;
    // remove second level chart
    $('#container2').html('').hide();
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    $.ajax({
        url: SITE_URL + 'getFeToolsChart',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){
            //alert(data);
            getFeToolsChartLevel1(data)
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });
}

var firstPieData = <?php echo $firstPieData;?>;
//alert(chart1Data);
getFeToolsChartLevel1(firstPieData);
function getFeToolsChartLevel1(firstPieData) {

    var text ='FE Owned Tools';
    var fe_name = $('.user_id :selected').text();
    var fe = $('.user_id :selected').val();
    if(fe!='')
    {
        text +=' For '+fe_name;
    }
    var zone_name = $('.btn-success').attr('data-zone');
    if(zone_name!='')
    {
        text +=' In '+zone_name;
    }
    var country_name = $('.country_id :selected').text();
    var country = $('.country_id :selected').val();
    if(country!='')
    {
        text +=' In '+country_name;
    }

   
   $('#container1').highcharts({

    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 1,
        plotShadow: false,
        type: 'pie'
    },
    title: {
            text: text
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                point: {
                   events: {
                      click: function(event) {
                          //alert('first click');
                         $('#container2').show();
                         draw_chart2(this.options.name);
                      }
                   }
                },
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f} %)',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                
            }
        },
    series: firstPieData
});


}
//draw_chart2('A2');
// Draw Second Chart
function draw_chart2(modality)
{
    //alert(modality);
    var text = modality+' Tools';
    var fe_name = $('.user_id :selected').text();
    var fe = $('.user_id :selected').val();
    if(fe!='')
    {
        text +=' For '+fe_name;
    }
    var zone_name = $('.btn-success').attr('data-zone');
    if(zone_name!='')
    {
        text +=' In '+zone_name;
    }
    var country_name = $('.country_id :selected').text();
    var country = $('.country_id :selected').val();
    if(country!='')
    {
        text +=' In '+country_name;
    }
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    createChart = function (chartsData) {
        var chart2Series = chartsData["chart1Series"];
        var xAxisCategories2 = chartsData["xAxisCategory"];
        var xAxisLable = chartsData["xAxisLable"];

        $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: text,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories2,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: chart2Series
    });
        
    }

    var zone = $('.btn-success').attr('data-id');
    var user_id = $('.user_id :selected').val();
    var country_id = $('.country_id :selected').val();
    var data = 'zone='+zone+'&user_id='+user_id+'&modality='+modality+'&country_id='+country_id;
    //alert(data);
    $.ajax({
        url: SITE_URL + 'getFeToolsChart2Data',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){
            //alert(data);
            createChart(data);
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });
    
}

</script>
<style type="text/css">
    text[style="cursor:pointer;color:#909090;font-size:9px;fill:#909090;"]{ display: none;}
</style>