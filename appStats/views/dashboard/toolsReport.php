<?php $this->load->view('commons/main_template', $nestedView);

?>
<div class="cl-mcont">
<div id="loaderID" style="position:fixed; top:50%; left:58%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>

<div class="row"> 
	<div class="col-sm-12 col-md-12">
		<div class="block-flat">
			<div class="content">							
				<div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                           <!--  <label class=" col-sm-1 control-label"> Zone : </label>
                            <div class="col-sm-5 custom_icheck" style="padding-left: 0px !important">
                                <label class="radio-inline"> 
                                    <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                        <input type="radio" class="zone" value="1" name="zone" checked style="position: absolute; opacity: 0;">
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                    </div> 
                                    All
                                </label>
                                <?php
                                foreach($zones as $zone)
                                { ?>
                                    <label class="radio-inline"> 
                                    <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                        <input type="radio" class="zone" value="<?php echo $zone['location_id']; ?>" name="zone" style="position: absolute; opacity: 0;">
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                    </div> 
                                    <?php echo $zone['name']; ?>
                                    </label>
                                <?php }
                                ?>
                            </div> -->
                            <input type="hidden" class="series_name" >
                            <div class="col-sm-3 fix_country">
                                <select class="select2 country_id" name="country_id" >
                                    <option value="">- Country -</option>
                                     <?php
                                    foreach($countryList as $country)
                                    {
                                        $selected = ($country['location_id']==$searchParams['country_id'])?'selected="selected"':'';
                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                             <div class="col-sm-5  fix_zone">
                                    <div class="btn-group">
                                        <input type="button" data-id="1" name="zone" class="zone btn btn-success" checked  value="All">
                                        <?php foreach ($zones as $zone) { ?>
                                            <input type="button" data-id="<?php echo $zone['location_id']; ?>" name="zone" class="zone btn btn-default" value="<?php echo $zone['name']; ?>">
                                      <?php  }  ?>
                                    </div>
                            </div>  
                            <div class="col-sm-3">
                                <select class="select2 select3 warehouse" style="width:100%" name="warehouse">
                                    <option value="">Select Warehouse</option>
                                    <?php
                                    foreach ($warehouses as $wh_row) {
                                        echo '<option value="'.$wh_row['wh_id'].'">'.$wh_row['wh_code'].' - '.$wh_row['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="select2 modality" style="width:100%" name="modality">
                                    <option value="">Select Modality</option>
                                    <?php
                                    foreach ($modality as $mrow) {
                                        echo '<option value="'.$mrow['modality_id'].'">'.$mrow['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="select2 tool_type" style="width:100%" name="tool_type">
                                    <option value="">Select Tool Type</option>
                                    <?php
                                    foreach ($tool_type_list as $mrow) {
                                        echo '<option value="'.$mrow['tool_type_id'].'">'.$mrow['name'].' '.'Tool'.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-11" align="center">
                            <div id="container4" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
				
				<div class="row">
					<div class="col-sm-12">
						<div class="col-md-11" align="center">
							<div id="container1" style=" margin: 0 auto"></div>
						</div>
					</div>	
				</div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-11" align="center">
                            <div id="container2" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-11" align="center">
                            <div id="container3" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
			</div>
		</div>				
	</div>
</div>
</div>

<style type="text/css">
    .radio-inline{ padding-left: 10px !important;}
    .radio-inline input[type="radio"]{ margin: 0px 4px 0px 0px;}
</style>

<?php 
$this->load->view('commons/main_footer.php', $nestedView); ?>

<script type="text/javascript">

Highcharts.setOptions({ colors: [ '#42A5F5','#A1887F','#FFD54F','#3F51B5','#FF9800','#F44336', '#4CAF50', '#9C27B0', '#795548', '#FFEB3B', '#CDDC39']});

//var icrm = $.noConflict();
var zone=1;
$('.zone').click(function(){
    var zone = $(this).attr('data-id');
    $('.zone').addClass('btn-default').removeClass('btn-success');
    $(this).addClass('btn-success').removeClass('btn-default');
    var data = 'zone='+zone;
    $.ajax({
        url: SITE_URL + 'getWarehousesByZone',
        type:"POST",
        data:data,
        success: function(data){
            //alert(data);
            $('.warehouse').html('');
            $('.warehouse').html(data);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
            filterTools();

        }
    });
});
$('.country_id').change(function(){
    var country_id = $('.country_id :selected').val();
    var data = 'country_id='+country_id;
    $.ajax({
        url: SITE_URL + 'getWarehousesByCountry',
        type:"POST",
        data:data,
        success: function(data){
            $('.warehouse').html('');
            $('.warehouse').html(data);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
            filterTools();

        }
    });
});
check_filters();
function check_filters()
{
    var task_access = <?php echo $task_access; ?>;

    var session_country_id =<?php if( $searchParams['country_id']!='')
    { echo $searchParams['country_id'] ; } else { echo 0; }  ?> ;
    if(session_country_id>0)
    {
        $('.fix_zone').show();
        $('.fix_country').hide();
    }
    else if(task_access ==3 & session_country_id==0)
    {
        $('.fix_zone').hide();
        $('.fix_country').show();
    }
}
$('.warehouse,.modality,.tool_type').change(function(){
	
    filterTools();
});

function filterTools(series_name='',category='')
{
    var zone =$('.btn-success').attr('data-id');
    var warehouse = $('.warehouse :selected').val();
    var modality = $('.modality :selected').val();
    var tool_type = $('.tool_type :selected').val();
    var country_id = $('.country_id :selected').val();
    var data = 'zone='+zone+'&warehouse='+warehouse+'&modality='+modality+'&tool_type='+tool_type+'&country_id='+country_id+'&series_name='+series_name+'&category='+category;
    $('.series_name').val('');
    $('.series_name').val(category);
    // remove second, third level chart
    $('#container2, #container3').html('').hide();
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    $.ajax({
        url: SITE_URL + 'getToolStatusChart',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){
            if(data['flag'] == 2 && series_name!='')
            {   
               // $('#container1').html('').hide();
                $('#container1').show();
                getToolStatusChartLevel1(data);
            }
            else if(data['flag'] == 1 && series_name == '')
            {    
                $('#container1').html('').hide();
                getcountrycharts(data);
            }
            else
            {
               //  $('#container4').html('').hide();
                $('#container1').show();
               getToolStatusChartLevel1(data);
            }
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });
}
var firstPieData = <?php echo $firstPieData;?>;
if(firstPieData['flag'] ==1)
{
     getcountrycharts(firstPieData);
}
else
{
    getToolStatusChartLevel1(firstPieData);
}

//alert(chart1Data);
function getcountrycharts(firstPieData) {

    var chart1Series = firstPieData["chart1Series"];
    var xAxisCategories = firstPieData["xAxisCategory"];
    var xAxisLable = firstPieData["xAxisLable"];
    var flag = firstPieData['flag'];
    var text = firstPieData['text'];
    if(flag ==1)
    {
        stacking ='normal';
    }
    else
    {
        stacking='null';
    }
    $('#container4').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: text,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
       
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            },
            stackLabels:
            {
                enabled :true
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                stacking : stacking,
                dataLabels: {
                        enabled: true,
                       // color: '#ffffff'
                }
            },
            series: {
            cursor: 'pointer',
            point: {
                events: 
                {
                    click: function () 
                    {  
                        filterTools(this.series.name,this.category);
                    }
                }
            }
        }
        },
        series: chart1Series
    });


}
function getToolStatusChartLevel1(firstPieData) {
//var cat =jQuery.parseJSON(firstPieData);
//console.log(firstPieData);
var first_x_cat = $('.series_name').val();
var text = 'Tools Status';
if(first_x_cat!='')
{
    text += ' In '+first_x_cat;
}
$('#container1').highcharts({
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 1,
        plotShadow: false,
        type: 'pie'
    },
    title: {
            text: text
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                point: {
                   events: {
                      click: function(event) {
                          //alert('first click');
                         $('#container2').show();
                         draw_chart2(this.options.name);
                         $('#container3').html('').hide();
                      }
                   }
                },
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f} %)',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                
            }
        },
    series: firstPieData
});


}
//draw_chart2('A2');
// Draw Second Chart
function draw_chart2(tool_status)
{
    //alert(tool_status);
    var first_x_cat = $('.series_name').val();
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    createChart = function (chartsData) {
        var chart2Series = chartsData["chart1Series"];
        var xAxisCategories2 = chartsData["xAxisCategory"];
        var xAxisLable = chartsData["xAxisLable"];
        var text = chartsData['text'];
        if(first_x_cat!='')
        {
            text+=' In '+first_x_cat;
        }
        $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: text,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories2,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                           //alert('Category: ' + this.category + ', value: ' + this.y);
                            $('#container3').show();
                            draw_chart3(tool_status,this.category);
                        }
                    }
                }
            }
        },
        series: chart2Series
    });
        
    }

    var zone =$('.btn-success').attr('data-id');
    var warehouse = $('.warehouse :selected').val();
    var modality = $('.modality :selected').val();
    var tool_type = $('.tool_type :selected').val();
    var country_id = $('.country_id :selected').val();
    var data = 'zone='+zone+'&warehouse='+warehouse+'&modality='+modality+'&tool_status='+tool_status+'&tool_type='+tool_type+'&first_x_cat='+first_x_cat+'&country_id='+country_id;
    //alert(data);
    $.ajax({
        url: SITE_URL + 'getToolStatusChart2Data',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){
            //alert(data);
            createChart(data);
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });
    
}

// Draw Second Chart
function draw_chart3(tool_status,x_category)
{
    /*alert(tool_status);
    alert(x_category);*/
    var first_x_cat = $('.series_name').val();
    var chart2Title = tool_status+' Tools in '+x_category;
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    createChart3 = function (chartsData) {
        var chart2Series = chartsData["chart1Series"];
        var xAxisCategories2 = chartsData["xAxisCategory"];
        var xAxisLable = chartsData["xAxisLable"];

        $('#container3').highcharts({
        chart: {
            type: 'column',
            inverted:true
        },
        title: {
            text: chart2Title,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories2,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                           //alert('Category: ' + this.category + ', value: ' + this.y);
                            /*$('#container3').show();
                            draw_chart3(this.category);*/
                        }
                    }
                }
            }
        },
        series: chart2Series
    });
        
    }

    var zone =$('.btn-success').attr('data-id');
    var warehouse = $('.warehouse :selected').val();
    var modality = $('.modality :selected').val();
    var tool_type = $('.tool_type :selected').val();
    var country_id = $('.country_id :selected').val();
    var data = 'zone='+zone+'&warehouse='+warehouse+'&modality='+modality+'&tool_status='+tool_status+'&zone_or_wh='+x_category+'&tool_type='+tool_type+'&first_x_cat='+first_x_cat+'&country_id='+country_id;
    //alert(data);
    $.ajax({
        url: SITE_URL + 'getToolStatusChart3Data',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){
            //alert(data);
            createChart3(data);
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });
    
}

</script>
<style type="text/css">
    text[style="cursor:pointer;color:#909090;font-size:9px;fill:#909090;"]{ display: none;}
</style>