<?php $this->load->view('commons/main_template', $nestedView);

?>
<div class="cl-mcont">
<div id="loaderID" style="position:fixed; top:50%; left:58%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>

<div class="row"> 
	<div class="col-sm-12 col-md-12">
		<div class="block-flat">
			<div class="content">			
					
				<div class="row">
					<div class="form-group">
                        <div class="col-sm-12">

                            <?php
                            if($task_access == 3 && @$_SESSION['header_country_id']=='')
                            {
                                ?>
                                <div class="col-sm-3">
                                    <select  class="country select2" >    <option value="">- Select Country -</option>
                                        <?php
                                        foreach($country_list as $row)
                                        {
                                            $selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
                                            echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                        }?>
                                    </select>
                                </div>

                                <?php
                            }
                            ?>
                            <div class="col-sm-offset-1 col-sm-3">
                                <select class="tool col2" style="width:100%" >
                                    <option value="">Select Tool Number/Description</option>
                                    
                                </select>
                            </div>

                            <div class="col-sm-offset-1 col-sm-3">
                                <select class="modality select2" style="width:100%" >
                                    <option value="">Select Modality</option>
                                    <?php
                                    foreach ($modality as $mrow) {
                                        echo '<option value="'.$mrow['modality_id'].'">'.$mrow['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            
                        </div>
					</div>
				</div>
			    <div class="row">
					<div class="col-sm-12">
						<div class="col-md-11" align="center">
							<div id="container1" style=" margin: 0 auto"></div>
						</div>
					</div>	
				</div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-11" align="center">
                            <div id="container2" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-11" align="center">
                            <div id="container3" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
						
				</div>

			</div>
		</div>				
	</div>
</div>
</div>

<!-- <style type="text/css">
    .radio-inline{ padding-left: 10px !important;}
    .radio-inline input[type="radio"]{ margin: 0px 4px 0px 0px;}
</style> -->

<?php 
$this->load->view('commons/main_footer.php', $nestedView); ?>

<script type="text/javascript">

Highcharts.setOptions({ colors: [ '#42A5F5','#A1887F','#FFD54F','#3F51B5','#FF9800','#F44336', '#4CAF50', '#9C27B0', '#795548', '#FFEB3B', '#CDDC39']});
$(document).ready(function(){
        $('.select3').select2();
        select2Ajax('tool', 'get_ajax_tools', 0, 3);
    });

$('.tool,.modality,.country').change(function(){
        var country_id = $('.country').find(':selected').val();
        //select2Ajax('tool', 'get_ajax_tools', country_id, 0);
        /*var data = 'country_id='+country_id;
        $.ajax({
        url: SITE_URL + 'get_calibration_country_based_tools',
        type:"POST",
        data:data,
        success: function(data){
            $('.tool').html('');
            $('.tool').html(data);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
            //get_in_calibration_assets();

            }
        });*/
        get_in_calibration_assets();
    });




function get_in_calibration_assets()
{
    var modality = $('.modality').find(':selected').val();
    var tool = $('.tool').find(':selected').val();
    var country = $('.country').find(':selected').val();
    var data = 'modality='+modality+'&tool='+tool+'&country='+country;
    // remove second level chart
    $('#container2').html('').hide();
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    $.ajax({
        url: SITE_URL + 'get_in_calibration_assets_Chart',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){

            if(data['flag']==2)
            {   
                $('#container1').show();
                get_in_calibration_ChartLevel1(data)
            }
            else if(data['flag']==1)
            {
                get_in_calibration_countries(data);
            }
            else
            {
               get_in_calibration_ChartLevel1(data)
            }
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);

            
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });
}

var firstPieData = <?php echo $firstPieData;?>;
if(firstPieData['flag'] ==1)
{
     get_in_calibration_countries(firstPieData);
}
else
{
    get_in_calibration_ChartLevel1(firstPieData);
}

//get_in_calibration_ChartLevel1(firstPieData);
function get_in_calibration_ChartLevel1(firstPieData) {

    $('#container1').highcharts({
        
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,
            plotShadow: false,
            type: 'pie'
        },
        title: {
                text: 'Calibration Assets',
                style:{
                        fontSize: '16px'
                    }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    point: {
                       events: {
                          click: function(event) {
                             $('#container2').show();
                             draw_chart2(this.options.name,this.options.country);
                          }
                       }
                    },
                    dataLabels: {
                        distance: -40,
                        enabled: true,
                        format: '{point.y}',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend:true
                    
                }
            },
        series: firstPieData
    });


}

function get_in_calibration_countries(firstPieData) {

    var chart1Series = firstPieData["chart1Series"];
    var xAxisCategories = firstPieData["xAxisCategory"];
    var xAxisLable = firstPieData["xAxisLable"];
    var flag = firstPieData['flag'];
    var text = firstPieData['text'];
    if(flag ==1)
    {
        stacking ='normal';
    }
    else
    {
        stacking='null';
    }
    $('#container1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: text,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
       
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                stacking : stacking
                
            },
            series: {
            cursor: 'pointer',
            point: {
                events: 
                {
                    click: function () 
                    {  
                        $('#container2').show();
                        draw_chart2(this.series.name,this.category);
                    }
                }
            }
        }
        },
        series: chart1Series
    });


}

//draw_chart2('A2');
// Draw Second Chart
function draw_chart2(name,country)
{
    var chart2Title =  name+' Current Position in '+country;
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    createChart = function (chartsData) {
        var chart2Series = chartsData["chart1Series"];
        var xAxisCategories2 = chartsData["xAxisCategory"];
        var xAxisLable = chartsData["xAxisLable"];

        $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: chart2Title,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories2,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Assets Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: 
                    {
                        click: function () 
                        {
                            download_field_deployed_excel(this.category,name,country);
                        }
                    }
                }
            }
        },
        series: chart2Series
    });
        
    }

    var modality_id = $('.modality').find(':selected').val();
    var tool_id = $('.tool').find(':selected').val();
    var data = 'modality='+modality_id+'&tool='+tool_id+'&name='+name+'&country_name='+country;
    //alert(data);
    $.ajax({
        url: SITE_URL + 'get_in_calibration_Chart2Data',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){
            //alert(data);
            createChart(data);
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });
    
}
function download_field_deployed_excel(name,stage,country)
{
    var modality_id = $('.modality').find(':selected').val();
    var tool_id = $('.tool').find(':selected').val();

    var url = SITE_URL + 'download_field_deployed_excel';
    var form = $('<form action="' + url + '" method="post" id="theForm">' +
  '<input type="text" autocomplete="off" name="name" value="' + name + '" />' +
  '<input type="text" autocomplete="off" name="modality_id" value="' + modality_id + '" />' +
  '<input type="text" autocomplete="off" name="tool_id" value="' + tool_id + '" />' +
  '<input type="text" autocomplete="off" name="country" value="' + country + '" />' +
  '</form>');
    if(stage=='Out of Calibration' && name == 'Field Deployed')
    {
        $('body').append(form);
        form.submit();
    }
}

</script>
<!-- <style type="text/css">
    text[text-anchor="end"] {
    display: none;
    }
</style> -->