<?php $this->load->view('commons/main_template', $nestedView);?>
<div class="cl-mcont">
    <div id="loaderID" style="position:fixed; top:50%; left:58%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>
    <div class="row"> 
    <div class="col-sm-12 col-md-12">
        <div class="block-flat">
            <div class="content">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="col-sm-3 fix_country">
                                    <select class="select2 country_id" name="country_id" >
                                        <option value="">- Country -</option>
                                         <?php
                                        foreach($countryList as $country)
                                        {
                                            $selected = ($country['location_id']==$searchParams['country_id'])?'selected="selected"':'';
                                            echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                        }
                                        ?>
                                    </select>
                            </div>
                             <div class="col-sm-5  fix_zone">
                                <div class="btn-group">
                                    <input type="button" data-id="1" name="zone" class="zone btn btn-success" checked  value="All">
                                    <?php foreach ($zones as $zone) { ?>
                                        <input type="button" data-id="<?php echo $zone['location_id']; ?>" name="zone" class="zone btn btn-default" value="<?php echo $zone['name']; ?>">
                                  <?php  }  ?>
                                </div>
                            </div>   
                            <div class="col-sm-3">
                                <select class="select3 select2 warehouse" style="width:100%" name="warehouse">
                                    <option value="">- Warehouse -</option>
                                    <?php
                                    foreach ($warehouses as $wh_row) {
                                        echo '<option value="'.$wh_row['wh_id'].'">'.$wh_row['wh_code'].' - '.$wh_row['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="select2 modality" style="width:100%" name="modality">
                                    <option value="">- Modality -</option>
                                    <?php
                                    foreach ($modality as $mrow) {
                                        echo '<option value="'.$mrow['modality_id'].'">'.$mrow['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="select2 tool_type" style="width:100%" name="tool_type">
                                    <option value="">- Tool Type -</option>
                                    <?php
                                    foreach ($tool_type_list as $mrow) {
                                        echo '<option value="'.$mrow['tool_type_id'].'">'.$mrow['name'].' '.'Tool'.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>                        
                    </div>
                </div>  
                <div class="row">
                    <div class="header"></div>
                    <table class="table table-bordered" ></table>
                </div>
                <div class="row"><br>
                    <div class="col-md-12">
                        <div class="col-md-offset-1 col-md-10" align="center">
                            <div id="container4" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                <div class="row"><br>
                    <div class="col-md-12">
                        <div class="col-md-offset-1 col-md-10" align="center">
                            <div id="container1" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                <div class="row"><br>
                    <div class="col-md-12">
                        <div class="col-md-offset-1 col-md-10" align="center">
                            <div id="container2" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                <div class="row"><br>
                    <div class="col-md-12">
                        <div class="col-md-offset-1 col-md-10" align="center">
                            <div id="container3" style=" margin: 0 auto"></div>
                        </div>
                    </div>  
                </div>
                    
            </div>
        </div>
    </div>              
    </div>
</div>

<style type="text/css">
    .radio-inline{ padding-left: 10px !important;}
    .radio-inline input[type="radio"]{ margin: 0px 4px 0px 0px;}
</style>

<?php 
$this->load->view('commons/main_footer.php', $nestedView); ?>

<script type="text/javascript">

Highcharts.setOptions({ colors: [ '#3F51B5','#FF9800','#F44336', '#4CAF50', '#9C27B0', '#795548', '#FFEB3B', '#CDDC39']});

var zone=1;
$('.zone').click(function(){
    var zone = $(this).attr('data-id');
    $('.zone').addClass('btn-default').removeClass('btn-success');
    $(this).addClass('btn-success').removeClass('btn-default');
    var data = 'zone='+zone;
    $.ajax({
        url: SITE_URL + 'getWarehousesByZone',
        type:"POST",
        data:data,
        success: function(data){
            //alert(data);
            $('.warehouse').html('');
            $('.warehouse').html(data);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
            filterChart1();

        }
    });
});

$('.country_id').change(function(){
    var country_id = $('.country_id :selected').val();
    var data = 'country_id='+country_id;
    $.ajax({
        url: SITE_URL + 'getWarehousesByCountry',
        type:"POST",
        data:data,
        success: function(data){
            $('.warehouse').html('');
            $('.warehouse').html(data);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
            filterChart1();

        }
    });
});

//var icrm = $.noConflict();
/*$('.zone').change(function(){
    var zone = $('.zone:checked').val();
    var data = 'zone='+zone;
    $.ajax({
        url: SITE_URL + 'getWarehousesByZone',
        type:"POST",
        data:data,
        success: function(data){
            //alert(data);
            $('.warehouse').html(data);
            filterChart1();

        }
    });
});*/
check_filters();
function check_filters()
{
    var task_access = <?php echo $task_access; ?>;

    var session_country_id =<?php if( $searchParams['country_id']!='')
    { echo $searchParams['country_id'] ; } else { echo 0; }  ?> ;
    if(session_country_id>0)
    {
        $('.fix_zone').show();
        $('.fix_country').hide();
    }
    else if(task_access ==3 & session_country_id==0)
    {
        $('.fix_zone').hide();
        $('.fix_country').show();
    }
}


$('.warehouse,.modality,.tool_type').change(function(){
    
    filterChart1();
});

function filterChart1(series_name='',category='')
{
    var zone =$('.btn-success').attr('data-id');
    var warehouse = $('.warehouse :selected').val();
    var modality = $('.modality :selected').val();
    var tool_type = $('.tool_type :selected').val();
    var country_id = $('.country_id :selected').val();
    var data = 'zone='+zone+'&warehouse='+warehouse+'&modality='+modality+'&tool_type='+tool_type+'&country_id='+country_id+'&series_name='+series_name+'&category='+category;
    // remove second level chart
    $('#container2').html('').hide();
    $('#container3').html('').hide();
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    $.ajax({
        url: SITE_URL + 'getCalDueChart',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){
            if(data['flag']==2 && series_name!='')
            {   
               // $('#container1').html('').hide();
                $('#container1').show();
                getCharts(data);
            }
            else if(data['flag']==1 && series_name == '')
            {
                 $('#container1').html('').hide();
                 getcountrycharts(data);
            }
            else
            {
               //  $('#container4').html('').hide();
               getCharts(data);
            }
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });
}

var chart1Data = <?php echo $chart1Data;?>;
if(chart1Data['flag'] ==1)
{
     getcountrycharts(chart1Data);
}
else
{
    getCharts(chart1Data);
}
function getcountrycharts(chart1Data) {

    var chart1Series = chart1Data["chart1Series"];
    var xAxisCategories = chart1Data["xAxisCategory"];
    var xAxisLable = chart1Data["xAxisLable"];
    var flag = chart1Data['flag'];
    var text = chart1Data['text'];
    if(flag ==1)
    {
        stacking ='normal';
    }
    else
    {
        stacking='null';
    }
    $('#container4').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: text,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
       
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                stacking : stacking
                
            },
            series: {
            cursor: 'pointer',
            point: {
                events: 
                {
                    click: function () 
                    {  
                        filterChart1(this.series.name,this.category);
                    }
                }
            }
        }
        },
        series: chart1Series
    });


}
function getCharts(chart1Data) {
    var chart1Series = chart1Data["chart1Series"];
    var xAxisCategories = chart1Data["xAxisCategory"];
    var xAxisLable = chart1Data["xAxisLable"];
    var flag = chart1Data['flag'];
    var first_x_cat =chart1Data['first_x_cat'];
    var text = chart1Data['text'];
    if(flag ==1)
    {
        stacking ='normal';
    }
    else
    {
        stacking='null';
    }
    $('#container1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: text,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
       
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                stacking : stacking
                
            },
            series: {
            cursor: 'pointer',
            point: {
                events: 
                {
                    click: function () 
                    {  
                        $('#container2').show();
                        $('#container3').show();
                        draw_chart2(this.category,first_x_cat);
                        
                    }
                }
            }
        }
        },
        series: chart1Series
    });


}
//draw_chart2('A2');
// Draw Second Chart
function draw_chart2(category,first_x_cat)
{
    var category1 = category;
    $("#pcont").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    var zone =$('.btn-success').attr('data-id');
    var warehouse = $('.warehouse :selected').val();
    var modality = $('.modality :selected').val();
    var tool_type = $('.tool_type :selected').val();
    var country_id = $('.country_id :selected').val();
    if (typeof country_id === "undefined") {
     country_id ='';
    }
    var data = 'zone='+zone+'&warehouse='+warehouse+'&modality='+modality+'&x_category='+category+'&tool_type='+tool_type+'&first_x_cat='+first_x_cat+'&country_id='+country_id;
    $.ajax({
        url: SITE_URL + 'getCalChart2',
        type:"POST",
        data:data,
        dataType:'json', 
        success: function(data){
            createChart(data);
            createChart2(data);
            $("#pcont").css("opacity",1);
            $("#loaderID").css("opacity",0);
        }
    });

    createChart = function (chartsData) 
    {
        var chart2Series = chartsData["chart1Series"];
        var xAxisCategories2 = chartsData["xAxisCategory"];
        var xAxisLable = chartsData["xAxisLable"];
        var text = chartsData['text'];

        $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: text,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: xAxisCategories2,
            crosshair: true,
            title: {
                text: xAxisLable
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: chart2Series
    });
    }

    createChart2 = function (chartsData2) 
    { 
        var tchart2Series = chartsData2["tchart2Series"];
        var txAxisCategories2 = chartsData2["txAxisCategory"];
        var txAxisLable = chartsData2["txAxisLable"];
        var ttext = chartsData2['ttext'];

        $('#container3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ttext,
            style:{
                    fontSize: '16px'
                }            
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: txAxisCategories2,
            crosshair: true,
            title: {
                text: txAxisLable
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Tools Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: tchart2Series
    });
    }
}

</script>
<style type="text/css">
    text[style="cursor:pointer;color:#909090;font-size:9px;fill:#909090;"]{ display: none;}
</style>