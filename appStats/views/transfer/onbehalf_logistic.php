<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>			
			<div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
								<div class="row">
									<div class="form-group">
										<label for="inputeq_model" class="col-sm-4 control-label">Warehosue <span class="req-fld">*</span></label>
										<div class="col-md-5">
										<select name="onbehalf_logistic" required class="select2"> 
											<option value="">Select Warehouse</option>
											<?php 
												foreach($whs as $wh_data)
												{
													echo '<option value="'.$wh_data['wh_id'].'">'.$wh_data['wh_code'].'-( '.$wh_data['name'].' )'.'</option>';
												}
											?>
										</select>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-5 col-sm-5">
											<button class="btn btn-primary" type="submit" value="1" name="submitsearch"><i class="fa fa-check"></i> Submit</button>
											<a class="btn btn-danger" href="<?php echo SITE_URL;?>"><i class="fa fa-times"></i> Cancel</a>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
