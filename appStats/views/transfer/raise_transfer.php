<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'raise_transfer';?>">
							<input type="hidden" name="warehouse_id" value="<?php echo @$warehouse_id;?>">
							<div class="row">
								<div class="col-sm-12">
									<div class="col-sm-3">
										<input type="text" autocomplete="off" name="part_number" placeholder="Tool Number" value="<?php echo @$searchParams['part_number'];?>"  class="form-control" maxlength="100">
									</div>
									<div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="part_description" placeholder="Tool Description" value="<?php echo @$searchParams['part_description'];?>" id="location" class="form-control" maxlength="100">
									</div>
									<div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$searchParams['asset_number'];?>" id="" class="form-control" maxlength="100">
									</div>
									<div class="col-sm-3">							
										<button type="submit" name="search" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Search"><i class="fa fa-search"></i></button>
										<button type="submit" name="reset" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Reset Search Filters"><i class="fa fa-reply"></i></button>
										<button type="submit" formaction="<?php echo SITE_URL.'raise_transfer'; ?>" name="refresh" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
									</div>
								</div>
								<div class="col-sm-12" style="margin-top: 10px;">
									<div class=" col-sm-3">
										<select name="modality_id" class="select2" > 
											<option value="">Modality</option>
											<?php 
												foreach($modality_details as $mod)
												{
													$selected = ($mod['modality_id']==@$searchParams['modality_id'])?'selected="selected"':'';
													echo '<option value="'.$mod['modality_id'].'" '.$selected.'>'.$mod['name'].'</option>';
												}
											?>
										</select>
									</div>
									<div class="col-sm-3" >
										<select name="asset_status_id[]" class="select2-container select2-container-multi select2 status-multiple" style="width: 100%;" multiple="multiple" >
											<?php 
												$selected='';
												foreach(st_status_array() as $eq)
												{
													$status_arr = array(0);
													if(@$searchParams['asset_status_id']!='')
													{
														$status_arr = @$searchParams['asset_status_id'];
													}
													if(in_array(@$eq['asset_status_id'], @$status_arr))
													{
														$selected = 'selected';
													}
													else
													{
														$selected='';
													}
													echo '<option value="'.$eq['asset_status_id'].'" '.$selected.'>'.$eq['name'].'</option>';
												}
											?>
										</select>
									</div>
									<div class="col-sm-3">
									    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$searchParams['serial_no'];?>" class="form-control">
									</div>		
								</div>
							</div>
						</form>
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'raise_transfer/'.$current_offset;?>">	
							<div class="row">
								<div class="header"></div>
								<div class="content">
									<div class="row" style="margin-bottom:3px;">
										<div class="col-sm-4">
											<p style="margin-left: 15px;"><?php echo 'Available List In : '.@$wh_name; ?></p>
										</div>
										<div class="col-sm-3">
										<?php   
											if(count(@$_SESSION['st_asset_id']) > 0)
											{ ?>
												<label class=" col-sm-12 alert alert-info" align="center" style="padding-top: inherit; padding-bottom: inherit;margin-bottom:2px;">
												<?php 
												echo count(@$_SESSION['st_asset_id']);
												if(count(@$_SESSION['st_asset_id']) == 1)
												{ echo ' Asset <strong>'; } 
												else
												{ echo ' Assets <strong>';}
											?> Added to cart</strong></label>
											<?php
											}?>
										</div>	
									
										<input type="hidden" name="warehouse_id" value="<?php echo @$warehouse_id;?>">
										<div class="col-sm-3" align="right">
											<button type="submit" id="add" name="add" value="1" class="btn btn-success" disabled><i class="fa fa-shopping-cart"></i> Add to Cart</button>
										</div>	
										<div class="col-sm-1">
											<button type="submit" id="issue" formaction="<?php echo SITE_URL.'issue_tool_transfer';?>" name="issue" value="1" class="btn btn-success" <?php 
												if(!isset($_SESSION['st_asset_id']))
												{ ?>
												disabled
											<?php } 
											else if(count(@$_SESSION['st_asset_id']) == 0)
												{ ?>
												disabled
											<?php } ?> ><i class="fa fa-mail-forward"></i> Proceed</button>
										</div>
									</div>
								</div>									
							</div>
						
							<div class="header"></div>
							<div class="table-responsive">
								<table class="table table-bordered hover">
									<thead>
										<tr>
											<th class="text-center" width="5%"><strong></strong></th>
											<th class="text-center"><strong>Asset Number</strong></th>
											<th class="text-center"><strong>Serial Number</strong></th>
											<th class="text-center"><strong>Tool Number</strong></th>
											<th class="text-center"><strong>Tool Description</strong></th>	
											<th class="text-center"><strong>Modality</strong></th>
											<th class="text-center"><strong>Asset Status</strong></th>						
											<th class="text-center"><strong>Tool Availability</strong></th>
										</tr>
									</thead>
									<tbody>
									<?php
										$i = 1;
										if(count($toolResults)>0)
										{
											foreach($toolResults as $row)
											{
												$position = get_asset_position($row['asset_id']);
												$isCarted = false;
												if(isset($_SESSION['st_asset_id'][$row['asset_id']])){$isCarted = true;}
												$chk_st = ($isCarted)?'checked="checked"':'';
												$disable_st = ($isCarted)?'':'disabled="disabled"';
												$comp_qty = ($isCarted)?@$_SESSION['st_asset_id'][$row['asset_id']]:'';
												?>
												<tr class="toolRow">
													<!-- <td class="text-center"><?php echo @$i++;?></td> -->
													<td class="text-center"><input type="checkbox" <?php echo $chk_st; ?> name="asset_id[]" value="<?php echo @$row['asset_id']; ?>" class="icheck"></td>
													<td class="text-center"><?php echo $row['asset_number'];?></td>
													<td class="text-center"><?php echo $row['serial_number'];?></td>
													<td class="text-center"><?php echo $row['part_number'];?></td>
													<td class="text-center"><?php echo $row['part_description'];?></td>
													<td class="text-center"><?php echo $row['modality_name'];?></td>
													<td class="text-center"><?php echo $row['asset_status'];?></td>
													<td class="text-center"><?php echo $position;?></td>
												</tr>
									<?php	}
										} else {
										?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
								<?php 	} ?>
									</tbody>
								</table>
							</div>
						</form>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	add_component_new_row();
	get_asset_level_id();
	check_tool_code_availability();
	check_part_number_availability();
</script>
<script type="text/javascript">
	$(document).ready(function(){
		
		
		$(".status-multiple").select2({
		    placeholder: "- Asset Status -"
		});
	});

</script>
