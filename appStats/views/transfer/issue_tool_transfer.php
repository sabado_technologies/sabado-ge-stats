<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:50%; z-index:2; opacity:0"><img src="<?php echo SITE_URL1;?>assets/images/ajax-loading-img.gif" /></div>
<div class="cl-mcont " id="orderCartContainer">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>			
			<div class="block-flat">
				<div class="content">
						<?php 
							if($flg ==1)
							{?>
								<form method="post">
									<div class="row">
										<div class="col-sm-9" align="left">
										 	<p style="margin-left: 15px;">Available List In : <strong><?php echo @$wh_name; ?></strong></p>
										</div>
										<div class="col-sm-3" align="right">
											<input type="hidden" name="warehouse_id" value="<?php echo $warehouse_id;?>">
											 <button class="btn btn-success" type="submit" value="1" name="raise_transfer" id="raise_transfer" formaction="<?php echo SITE_URL.'raise_transfer/0'?>"><i class="fa fa-shopping-cart"></i> Add More to cart</button>

										 </div>
									</div>
								</form>
							<?php } ?>

					<form role="form" class="form-horizontal" method="post" enctype="multipart/form-data" parsley-validate novalidate action="<?php echo $form_action;?>">
						<input class="form-control"  type="hidden" value="<?php echo @$order_info['tool_order_id'];?>"  name="tool_order_id" id="tool_order_id">
						<input class="form-control"  type="hidden" value="<?php echo @$order_info['stn_number'];?>" name="stn_number">
						<input type="hidden" name="count_c" id="count_c" value="<?php echo @$count_c;?>"> 
						<input type="hidden" name="current_stage_id"  value="<?php echo @$order_info['current_stage_id'];?>"> 		
						<input type="hidden" name="warehouse_id" value="<?php echo $warehouse_id;?>">
						
						
					<div class="header"></div>
					<div class="table-responsive">
						<table class="table table-bordered hover" id="mytable">
							<thead>
								<tr>
									<th class="text-center"><strong>S.No.</strong></th>
									<th class="text-center"><strong>Asset Number</strong></th>
									<th class="text-center"><strong>Serial Number</strong></th>
									<th class="text-center"><strong>Tool Number</strong></th>
									<th class="text-center"><strong>Description</strong></th>								
									<th class="text-center"><strong>Asset Status</strong></th>
									<th class="text-center"><strong>Action</strong></th>
								</tr>
							</thead>
							<tbody>
							<?php $i= 1;
							if(count(@$assets) >0)
							{
								foreach($assets as $row)
								{
									
									?>
									<tr class="toolSelectedRow">
										<td class="text-center"><?php echo $i++;?></td>									
										<input type="hidden" name="asset_id[]" value="<?php echo $row['asset_id'];?>">
										<td class="text-center"><?php echo @$row['asset_number'];?></td>
										<td class="text-center"><?php echo @$row['serial_number'];?></td>
										<td class="text-center"><?php echo @$row['part_number'];?></td>
										<td class="text-center"><?php echo @$row['part_description'];?></td>
										<td class="text-center"><?php echo @$row['status_name'];?></td>
											
										<td class="text-center"><a class="btn btn-danger btn-xs removeRow" 
											data-cid="<?php echo $row['asset_id']; ?>"
											 data-orderid="<?php echo @$row['tool_order_id']; ?>"
											data-orderedasset="<?php echo @$row['ordered_asset_id']; ?>"
											data-orderedtool="<?php echo @$row['ordered_tool_id']; ?>"
											data-assetstatus="<?php echo @$row['asset_status_id']; ?>"
											data-currentstage="<?php echo @$order_info['current_stage_id']; ?>"   href="#"  >
										<i class="fa fa-times"></i></a></td> 
									</tr>

							<?php	} 
							}
							else {
							?>	<tr><td colspan="9" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
								
							</tbody>
						</table>
					</div><br>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<label for="inputName" class="col-sm-2 control-label">Send To Wh <span class="req-fld">*</span></label>
								<div class="col-sm-4">
									<select name="wh_id" required class="select2"> 
											<option value="">- Send To Warehouse -</option>
											<?php 
												foreach($wh_data as $st)
												{
													$selected = ($st['wh_id']==@$order_info['to_wh_id'])?'selected="selected"':'';
													echo '<option value="'.$st['wh_id'].'" '.$selected.'>'.$st['name'].'</option>';
												}
											?>
									</select>
								</div>

								<label for="inputName" class="col-sm-2 control-label">Need Date <span class="req-fld">*</span></label>
								<div class="col-sm-4">
									<input class="form-control" required readonly size="16" type="text" autocomplete="off"  value="<?php if(@$flg == 2) echo indian_format(@$order_info['request_date']); ?>"  placeholder="Date" name="deploy_date" id="deploy_date" style="cursor:hand;background-color: #ffffff">
								</div>
							</div>
						</div>
						<div class="row">
	                                <div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
	                                    <a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a>
	                                </div>
	                                <div class="col-md-12">
	                                    <div class="table-responsive col-md-offset-2 col-md-8  tab_hide">
	                                        <table class="table hover document_table">
	                                            <thead>
	                                                <tr>
	                                                    <th  width="8%"><strong>Sno</strong></th>
	                                                    <th width="40%" ><strong>Document Type</strong></th>
	                                                    <th width="40%" ><strong>Supported Document</strong></th>
	                                                    <th width="8%" ><strong>Delete</strong></th>
	                                                </tr>
	                                            </thead>
	                                            <tbody>
	                                            <?php $count = 1;
	                                                if(count(@$attach_document)>0)
	                                                {   
	                                                    foreach($attach_document as $doc)
	                                                    {?> 
	                                                        <tr class="attach">
	                                                            <input type="hidden" name="doc_id" class="asset_doc_id" value="<?php echo $doc['order_doc_id'];?>">
	                                                            <td align="center"><span class="sno"><?php echo $count++; ?></span></td>
	                                                            <td align="center">
	                                                                <?php 
	                                                                    foreach($documenttypeDetails as $docs)
	                                                                    {
	                                                                        if($docs['document_type_id']==@$doc['document_type_id'])
	                                                                        {
	                                                                            echo $docs['name'].' - ('.$doc['created_time'].')'; 
	                                                                        }
	                                                                    }
	                                                                ?>
	                                                            </td>
	                                                            <td>
	                                                                <a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo SITE_URL1.order_document_path().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
	                                                            </td>
	                                                            <td>
	                                                            <?php if($doc['status']==1)
	                                                            { ?>
	                                                                <a class="btn link btn-danger btn-sm deactivate" > <i class="fa fa-trash-o"></i></a>
	                                                            
	                                                            <?php } 
	                                                            else
	                                                                {?>
	                                                                <a class="btn link btn-info btn-sm activate" > <i class="fa fa-check"></i></a>
	                                                            <?php } ?>
	                                                            </td>
	                                                        </tr>
	                                                     <?php      
	                                                    } 
	                                                } ?>            
	                                                <tr class="doc_row">
	                                                    <td align="center">
	                                                        <span class="sno"><?php echo $count; ?></span>
	                                                    </td>
	                                                    <td>
	                                                        <select name="document_type[1]" class="form-control doc_type" > 
	                                                            <option value="">Select Document Type</option>
	                                                            <?php 
	                                                                foreach($documenttypeDetails as $doc)
	                                                                {
	                                                                    echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
	                                                                }
	                                                            ?>
	                                                        </select>
	                                                    </td>
	                                                    <td>
	                                                        <input type="file" id="document" name="support_document_1" class="document">
	                                                    </td>
	                                                    
	                                                    <td><a <?php if(@$flg == 2){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
	                                                </tr>
	                                            </tbody>
	                                        </table>
	                                    </div>
	                                </div>
	                            </div>
						

						<br>
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-5">
								<button class="btn btn-success" type="submit" value="1" onclick="return confirm('Are you sure you want to Submit ST Request ?')" name="submitorder" id="submitOrder"><i class="fa fa-check"></i> Submit</button>
								<a class="btn btn-danger" href="<?php echo SITE_URL.'raise_transfer';?>"><i class="fa fa-times"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>	             	          		
			</div>
		</div>	
		</form>
	</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">		
	get_warehouse_address();
	get_warehouses_dropdown_by_sso();
</script>