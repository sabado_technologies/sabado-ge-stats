<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>			
			<div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
								<div class="row">
									<?php if($this->session->userdata('header_country_id')=='')
									{ ?>
									<div class="form-group">
										<label class="col-sm-4 control-label">Country<span class="req-fld">*</span></label>
										<div class="col-sm-5">
											<select class="select2 country" required name="country_id" >
												<option value="">- Country -</option>
												 <?php
												foreach($countryList as $country)
												{
													$selected = ($country['location_id']==@$country_id)?'selected="selected"':'';
													echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
												}
												?>
											</select>
										</div>
									</div>
								<?php } ?>
									<div class="form-group">
										<label for="inputeq_model" class="col-sm-4 control-label">Warehouse <span class="req-fld">*</span></label>
										<div class="col-md-5">
										<select name="warehouse_id" required class="select3 wh" style="width: 100%;"> 
											<?php if($flag==1) { ?>
											<option value="">- Select Warehouse -</option>
											<?php 
												foreach($warehouse_list as $row)
												{
													echo '<option value="'.$row['wh_id'].'">'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
												}
											}
											?>
										</select>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-offset-5 col-sm-5">
											<button class="btn btn-primary" type="submit" value="1" name="SubmitWarehouse"><i class="fa fa-check"></i> Submit</button>
											<a class="btn btn-danger" href="<?php echo SITE_URL;?>"><i class="fa fa-times"></i> Cancel</a>
										</div>
									</div>
								</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select3').select2();
	});

$(document).on('change','.country',function(){
var country_id=$(this).val();
if(country_id == '')
{
	$('.wh').html('<option value="">- Select Warehouse -</option>');
	$('.select3').select2('destroy'); 
	$('.select3').select2();
}
else
{
	$.ajax({
	    type:"POST",
	    url:SITE_URL+'ajax_get_warehouse_by_country_id',
	    data:{country_id:country_id},
	    cache:false,
	    success:function(html){
	    	$('.wh').html(html);
	    	$('.select3').select2('destroy'); 
	    	$('.select3').select2();
	    }
	});
}
});
</script>
