<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form method="post" action="<?php echo SITE_URL.'submitApproveOrRejectST';?>" parsley-validate novalidate class="form-horizontal" enctype="multipart/form-data">							
							<div class="header">
								<h5 align="center"><strong>Order Details</strong></h5>
							</div>
							<div class="row">
							<div class="col-md-6">
								<table class="no-border">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="data-lable"><strong>ST Number :</strong></td>
									        <td class="data-item"><?php echo @$order_info['stn_number'];?></td>
									        <input type="hidden" name="stn_number" value="<?php echo $order_info['stn_number'];?>">
									        <input type="hidden" name="tool_order_id" value="<?php echo $order_info['tool_order_id'];?>">
										</tr>
										<tr>
											<td class="data-lable"><strong>From Address :</strong></td>
									        <td class="data-item"></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Warehouse :</strong></td>
									        <td class="data-item"><?php echo getWhCodeAndName($order_info['wh_id']);?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Address1 :</strong></td>
									        <td class="data-item"><?php echo @$wh_data['address1'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Address2:</strong></td>
									        <td class="data-item"><?php echo @$wh_data['address2'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Address3 :</strong></td>
									        <td class="data-item"><?php echo @$wh_data['address3'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Address4 :</strong></td>
									        <td class="data-item"><?php echo @$wh_data['address4'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Pin Code :</strong></td>
									        <td class="data-item"><?php echo @$wh_data['pin_code'];?></td>
										</tr>
										
										
									</tbody>
								</table>
							</div>
							<div class="col-md-6">
								<table class="no-border">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="data-lable"><strong>SSO Detail :</strong></td>
											<td class="data-item"><?php echo @$order_info['sso_id'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>To Address :</strong></td>
									        <td class="data-item"></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Warehouse :</strong></td>
									        <td class="data-item"><?php echo getWhCodeAndName($order_info['to_wh_id']);?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Address1 :</strong></td>
									        <td class="data-item"><?php echo @$order_info['address1'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Address2:</strong></td>
									        <td class="data-item"><?php echo @$order_info['address2'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Address3 :</strong></td>
									        <td class="data-item"><?php echo @$order_info['address3'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Address4 :</strong></td>
									        <td class="data-item"><?php echo @$order_info['address4'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Pin Code :</strong></td>
									        <td class="data-item"><?php echo @$order_info['pin_code'];?></td>
										</tr>
									</tbody>
								</table>
							</div>
							</div><br>
							
							
							<div class="header">
								<h5 align="center"><strong>Ordered Assets</strong></h5>
							</div>
							<div class="table-responsive"> 
								<table class="table tabb">
									<thead>
										<tr>
											<th class="text-center"><strong>S No.</strong></th>
											<th class="text-center"><strong>Asset Number</strong></th>
			                                <th class="text-center"><strong>Tool Number</strong></th>
			                                <th class="text-center" width="30%"><strong>Tool Description</strong></th>
			                                <th class="text-center"><strong>Asset Status</strong></th>
			                              	
										</tr>
									</thead>
									<tbody>
										<?php 
										if(count(@$assets)>0)
										{	$sn = 1;
											foreach(@$assets as $row)
											{
											?>
												<tr class="asset_row">
													<td class="text-center"><?php echo $sn++; ?></td>
													<td class="text-center"><?php echo $row['asset_number']; ?></td>
													<td class="text-center"><?php echo $row['part_number']; ?></td>
													<td class="text-center"><?php echo $row['part_description']; ?></td>									
													<td class="text-center"><?php echo $row['status_name']; ?></td>
													
												</tr>
									<?php   }
										} else {?>
											<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
			                    <?php 	} ?>
									</tbody>
								</table>
			                </div><br>

			                <div class="header">
								<h5 align="center"><strong>Documents Details</strong></h5>
							</div>						
							<div class="table-responsive"> 
								<table class="table tabb">
									<thead>
										<tr>
											<th class="text-center"><strong>S No.</strong></th>
			                                <th class="text-center"><strong>Document Type</strong></th>
			                                <th class="text-center"><strong>Name</strong></th>
			                                <th class="text-center" width="30%"><strong>Download</strong></th>                           	
										</tr>
									</thead>
									<tbody>
										<?php 
										if(count(@$attach_document)>0)
										{	$sn = 1;
											foreach(@$attach_document as $row)
											{
											?>
												<tr class="asset_row">
													<td class="text-center"><?php echo $sn++; ?></td>
													<td class="text-center"><?php echo $this->Common_model->get_value('document_type',array('document_type_id'=>$row['document_type_id']),'name'); ?></td>
													<td class="text-center"><?php echo $row['doc_name']; ?></td>
													<td><a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo SITE_URL1.order_document_path().$row['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $row['name']; ?></td>
													
												</tr>
									<?php   }
										} else {?>
											<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
			                    <?php 	} ?>
									</tbody>
								</table>
			                </div><br>
			                <?php if(@$view == 1) {?>
				                <div class="col-md-12">
				                	<div class="form-group">
	                                        <label for="inputName" class="col-sm-4 control-label">Remarks <span class="req-fld">*</span></label>
	                                        <div class="col-sm-4">
	                                            <textarea class="form-control" <?php if($view == 1){ echo "required" ;}?> name="admin_remarks"></textarea>                                        
	                                        </div>
	                            	</div> <br>
				                </div>
				                
	                            <div class="form-group">
	                                    <div class="col-sm-offset-4 col-sm-6">
	                                        <button class="btn btn-primary" onclick="return confirm('Are you sure you want to Approve ?')"  type="submit" value="1" name="submit_action"><i class="fa fa-check"></i> Approve</button>
	                                        <button class="btn btn-danger" onclick="return confirm('Are you sure you want to Reject ?')"  type="submit" value="2" name="submit_action"><i class="fa fa-check"></i> Reject</button>
	                                        <a class="btn btn" href="<?php echo $form_action;?>"><i class="fa fa-times"></i> Cancel</a>
	                                    </div>
	                            </div>
	                        <?php  } else{ ?>
	                        	<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<a class="btn btn-primary" href="<?php echo $form_action;?>"><i class="fa fa-reply"></i> Back</a>
									</div>
								</div>
	                        <?php } ?>
                            </form>
                        </div>
                            			                
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
