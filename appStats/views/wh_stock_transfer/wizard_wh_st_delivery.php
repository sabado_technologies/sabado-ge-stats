<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
    		<?php echo $this->session->flashdata('response'); ?>
<div class="row wizard-row">
	<div class="col-md-12 fuelux">
		<div class="block-wizard">
			<div id="wizard1" class="wizard wizard-ux">
				<ul class="steps">
					<li data-target="#step1" class="active">Stock Transfer Details<span class="chevron"></span></li>
					<li data-target="#step2">Courier Details<span class="chevron"></span></li>
					<li data-target="#step3">Documentation<span class="chevron"></span></li>
				</ul>
				<div class="actions">
					<button type="button" class="btn btn-xs btn-prev btn-default"> <i class="icon-arrow-left"></i>Prev</button>
					<button type="button" <?php if($check_no == 1) { echo "disabled"; }?> class="btn btn-xs btn-next btn-default forward_btn" data-last="Finish">Next<i class="icon-arrow-right"></i></button>
				</div>
			</div>
			<div class="step-content">
				<form class="form-horizontal" method="post" target="_blank" action="<?php echo $form_action;?>" data-parsley-namespace="data-parsley-" data-parsley-validate novalidate enctype="multipart/form-data">
                <input type="hidden" name="tool_order_id" value="<?php echo storm_encode($tool_order_id)?>">
					<div class="step-pane active" id="step1">
						<div class="col-sm-12 col-md-12">
							<div class="row">							
								<div class="form-group no-padding">
									<div class="col-sm-7">
										<h4 class="hthin"><u>Stock Transfer Info</u></h4>
									</div>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>ST Number:</strong></td>
										        <td class="data-item"><?php echo @$trow['stn_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Request Date:</strong></td>
										        <td class="data-item"><?php echo indian_format(@$trow['request_date']);?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<?php 
									if(@$trow['fe_check'] == 0){ ?>
									
									<tr>
										<td class="data-lable"><strong>To Warehouse:</strong></td>
										<td class="data-item"><?php echo getWhCodeAndName($trow['to_wh_id'])?></td>
									</tr>
									<?php
								}
								else if(@$trow['fe_check'] == 1)
								{?>
									<tr>
										<td class="data-lable"><strong>To SSO:</strong></td>
										<td class="data-item"><?php echo @$sso_name; ?></td>
									</tr>
								<?php } ?>
											<tr>
												<td class="data-lable"><strong>Destination:</strong></td>
												<td class="data-item"><?php echo @$order_address['address3'].', '.$order_address['address4']; ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-12"><br>
							<div class="row">							
								<div class="header">
									<h5 align="center"><strong>Requested Tools</strong></h5>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table hover">
									<thead>
										<tr>
			                            	<th class="text-center"><strong>S.No</strong></th>
			                                <th class="text-center"><strong>Tool Number</strong></th>
			                                 <th class="text-center"><strong>Tool Desc</strong></th>
			                                 <th class="text-center"><strong>Ordered Qty</strong></th>
			                                <th class="text-center"><strong>Avail Qty In Inventory</strong></th>
			                                <th class="text-center"><strong>Assigned Asset(s)</strong></th>
			                                <th class="text-center"><strong>Actions</strong></th>
										</tr>
									</thead>
									<tbody>
										<?php 
										if(count(@$order_tool)>0)
										{	$sn = 1;
											foreach(@$order_tool as $row)
											{
											?>
												<tr>
													<td class="text-center"><?php echo $sn++; ?></td>
													<td class="text-center"><?php echo $row['part_number']; ?></td>
													<td class="text-center"><?php echo $row['part_description']; ?></td>
													<td class="text-center"><?php echo $row['quantity']; ?></td>
													<td class="text-center"><?php echo $row['avail_qty']; ?></td>
													<td class="text-center"><?php echo $row['assets']; ?></td>
													<td class="text-center">
													<?php if($row['quantity'] == $row['asset_count']) {?>
														<b style="color: green;text-align: center;"><i data-container="body" data-placement="top"  data-toggle="tooltip" title="Asset Linked"  class="fa fa-check fa-lg"></i></b>
													<?php }
													else{ ?> 
														<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Assign Asset"  href="<?php echo SITE_URL.'wh_st_asset/'.storm_encode($row['ordered_tool_id']);?>"><i class="fa fa-pencil"></i></a>
													<?php } ?>
													<?php 
														if($row['asset_count']>0)
														{  ?>
															<a class="btn btn-warning" style="padding:3px 3px;"  data-container="body" data-placement="top" data-toggle="tooltip" title="Unlink Scanned Assets"  href="<?php echo SITE_URL.'unlink_st_scanned_assets_individually/'.storm_encode($row['tool_order_id']).'/'.storm_encode($row['ordered_tool_id']);?>" onclick="return confirm('Are you sure you want to Unlink Scanned Assets?')"><i class="fa fa-chain-broken"></i></a>
														<?php 
														}
														?>
			                                        </td>
												</tr>
									<?php   }
										} else {?>
											<tr><td colspan="7" align="center"><span class="label label-primary">No Records Found</span></td></tr>
			                    <?php 	} ?>
									</tbody>
								</table>
			                </div>
						</div>
                       
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-6"><br>
								<?php if($flg == 1) { ?> 
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>wh_stock_transfer_list"><i class="fa fa-times"></i> Cancel</a>
								<?php } else if($flg==2){ ?>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>open_fe_delivery_list"><i class="fa fa-times"></i> Cancel</a>
								<?php } ?>
								<button data-wizard="#wizard1" class="btn btn-primary wizard-next <?php if($check_no == 1) { echo "hidden"; }?> page1_data" value="1">Next Step <i class="fa fa-caret-right"></i></button>
							</div>
						</div>
					</div>
					<div class="step-pane" id="step2">
						<div class="form-group no-padding">
									<div class="col-sm-7">
										<h4 class="hthin"><u>Billed From</u></h4>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label for="inputName" class="col-sm-4 control-label">Billed From <span class="req-fld">*</span></label>
										<div class="col-sm-4">
											<select class="select2 billed_to" name="billed_to" required>
												<option value="">- Billed From -</option>
												<?php
												foreach($billed_to_list as $bill)
												{
													echo '<option value="'.$bill['wh_id'].'">'.$bill['wh_code'].' -('.$bill['name'].')</option>';
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="row"><hr style="margin-left: 18px;">
									<div class="col-sm-6">
										<div class="form-group no-padding">
											<div class="col-sm-7">
												<h4 class="hthin"><u>Shipment From</u></h4>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="add1" readonly><?php echo $billed_from['address1']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="add2" readonly><?php echo $billed_from['address2']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="add3" readonly><?php echo $billed_from['address3']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control" name="add4" readonly><?php echo $billed_from['address4']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control" name="pin" placeholder="Pin Code" readonly value="<?php echo @$billed_from['pin_code']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">GST Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control" placeholder="GST Number" name="gst" readonly value="<?php echo @$billed_from['gst_number']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">PAN Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control" name="pan" readonly placeholder="PAN Number" value="<?php echo @$billed_from['pan_number']; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group no-padding">
											<div class="col-sm-7">
												<h4 class="hthin"><u>Shipped To</u></h4>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control add1" name="address1" required><?php echo $order_address['address1']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control add2" name="address2" required><?php echo $order_address['address2']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control add3" name="address3" required><?php echo $order_address['address3']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea class="form-control add4" name="address4" required><?php echo $order_address['address4']; ?></textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control pin_code" name="pin_code" placeholder="Pin Code" required value="<?php echo @$order_address['pin_code']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">GST Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control gst_number" placeholder="GST Number" name="gst_number" required value="<?php if(@$order_address['gst_number']!=''){ echo @$order_address['gst_number'];} else if(@$country_id!=2){ echo "NA";} ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label for="inputName" class="col-sm-4 control-label">PAN Number <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<input type="text" autocomplete="off" class="form-control pan_number" name="pan_number" required placeholder="PAN Number" value="<?php if(@$order_address['pan_number']!=''){ echo @$order_address['pan_number'];} else if(@$country_id!=2){ echo "NA";} ?>">
												</div>
											</div>
										</div>
									</div>
								</div>
						<div class="form-group no-padding">
									<div class="col-sm-7">
										<h4 class="hthin"><u>Courier Info</u></h4>
									</div>
								</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12 custom-icheck">
									<label for="inputName" class="col-sm-4 control-label">Mode Of Transport <span class="req-fld">*</span></label>
	                            	<div class="col-sm-5 custom_icheck">
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$crow['courier_type']==1 || $flg == 1) { echo "checked"; } ?>" style=" position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" class="courier_type" value="1" name="courier_type" <?php if(@$crow['courier_type']==1 || $flg == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Courier
		                                </label>
		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$crow['courier_type']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" <?php if(@$crow['courier_type']==2) { echo "checked"; } ?> class="courier_type" value="2" name="courier_type" style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Hand
		                                </label>

		                                <label class="radio-inline"> 
		                                    <div class="iradio_square-blue <?php if(@$crow['courier_type']==3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
		                                        <input type="radio" <?php if(@$crow['courier_type']==3) { echo "checked"; } ?> class="courier_type" value="3" name="courier_type" style="position: absolute; opacity: 0;">
		                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		                                    </div> 
		                                    By Dedicated
		                                </label>
	                            	</div>
								</div>
							</div>
							<div class="form-group courier_div <?php if(@$crow['courier_type']==2 || @$crow['courier_type']==3) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Courier Name <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control courier_name" placeholder="Courier Name" name="courier_name" value="<?php echo @$crow['courier_name']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group docket_div <?php if(@$crow['courier_type']==2 || @$crow['courier_type']==3) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Docket/AWB No. <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control docket_num" placeholder="Docket Number" name="courier_number" value="<?php echo @$crow['courier_number']; ?>">
									</div>
								</div>
							</div>

							<div class="form-group vehicle_div <?php if(@$crow['courier_type']==1 || $flg == 1 || @$crow['courier_type']==2) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Vehicle Number <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control vehicle_num" placeholder="Vehicle Number" name="vehicle_number" value="<?php echo @$crow['vehicle_number']; ?>">
									</div>
								</div>
							</div>

							<div class="form-group contact_div <?php if(@$crow['courier_type']==1 || $flg == 1) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Contact Person <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control contact_person" placeholder="Contact Person" name="contact_person" value="<?php echo @$crow['contact_person']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group phone_div <?php if(@$crow['courier_type']==1 || $flg == 1) { echo "hidden"; } ?>">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Phone Number <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control phone_number" placeholder="Phone Number" name="phone_number" value="<?php echo @$crow['phone_number']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group expected_date_row">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">Expected Delivery Date <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input class="form-control  expected_date" data-min-view="2" data-date-format="dd-mm-yyyy" size="16" type="text" autocomplete="off" placeholder="DD-MM-YYYY" name="expected_delivery_date" readonly value="<?php echo @$crow['expected_delivery_date']; ?>" style="cursor:hand;background-color: #ffffff">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label for="inputName" class="col-sm-4 control-label">E-Way Bill No. </label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" class="form-control" placeholder="E-Way Bill Number" name="remarks">
									</div>
								</div>
							</div>
                        </div>
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-6"><br>
								<button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Previous</button>
								<button class="btn btn-primary wizard-next page2_data" value="2">Next Step <i class="fa fa-caret-right"></i></button>
							</div>
						</div>
						<br><br>
					</div>
					<div class="step-pane" id="step3">
						<div class="form-group no-padding">
									<div class="col-sm-7">
										<h4 class="hthin"><u>Attach Docs</u></h4>
									</div>
								</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-5 control-label">Transaction Type <span class="req-fld">*</span></label>
									<div class="col-sm-3">
										<select class="select2 transaction_type" required name="print_type">
											<option value="">- Transaction Type -</option>
											<option value="3">With In State</option>
											<option value="4">Out Of State</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group"><br>
                        		<div class="col-md-12">
									<div class="col-md-offset-8 col-md-2" style="padding-bottom:10px;">
										<a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a><br>
									</div>
										<div class="table-responsive col-md-offset-2 col-md-8 <?php if(count(@$doc)>0) { echo "hidden"; }?> tab_hide">
										<table class="table hover document_table">
											<thead>
												<tr>
													<th class="text-center" width="8%"><strong>Sno</strong></th>
													<th class="text-center" width="40%"><strong>Document Type</strong></th>
													<th class="text-center" width="40%"><strong>Supported Document</strong></th>
													<th class="text-center" width="8%"><strong>Delete</strong></th>
												</tr>
											</thead>
											<tbody>
											<?php $count = 1;
												if(count(@$docs)>0)
												{	
													foreach($docs as $doc)
													{?>	
														<tr class="attach">
															<input type="hidden" name="doc_id" class="doc_id" value="<?php echo $doc['order_delivery_doc_id'];?>">
															<td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
															<td class="text-center">
																<?php 
																	foreach($documenttypeDetails as $docs)
																	{
																		if($docs['document_type_id']==@$doc['document_type_id'])
																		{
																			echo $docs['name'].' - ('.$doc['created_time'].')'; 
																		}
																	}
																?>
															</td>
															<td class="text-center">
						                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
															</td>
															<td class="text-center">
															<?php if($doc['status']==1)
															{ ?>
																<a class="btn link btn-danger btn-sm deactivate" > <i class="fa fa-trash-o"></i></a>
													        
															<?php } 
															else
																{?>
																<a class="btn link btn-info btn-sm activate" > <i class="fa fa-check"></i></a>
															<?php } ?>
													        </td>
														</tr>
													 <?php		
													} 
												} ?>			
												<tr class="doc_row">
													<td class="text-center">
														<span class="sno"><?php echo $count; ?></span>
													</td>
													<td class="text-center">
														<select name="document_type[1]" class="form-control doc_type" > 
															<option value="">Select Document Type</option>
															<?php 
																foreach($documenttypeDetails as $doc)
																{
																	echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
																}
															?>
														</select>
													</td>
													<td class="text-center">
				                                		<input type="file" id="document" name="support_document_1" class="document">
													</td>
													
											        <td class="text-center"><a <?php if(@$flg == 2){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
												</tr>
											</tbody>
										</table>
									</div>
                        			
								</div>
                    		</div>
						</div>
                        <div class="form-group">
							<div class="col-sm-offset-5 col-sm-6">
								<button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Previous</button>
								<button type="submit" onclick="return confirm('Are you sure you want to Submit?')"  class="btn btn-primary submit_btn"><i class="fa fa-check"></i> Submit</button>
								<button type="submit" onclick="return confirm('Are you sure you want to Generate Invoice?')"  class="btn btn-primary invoice_btn hidden"><i class="fa fa-check"></i> Generate Invoice</button>
								<button type="submit" onclick="return confirm('Are you sure you want to Generate Delivery Challan?')"  class="btn btn-primary dc_btn hidden"><i class="fa fa-check"></i> Generate Delivery Challan</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>