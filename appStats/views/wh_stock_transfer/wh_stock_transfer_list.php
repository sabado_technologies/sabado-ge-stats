<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'wh_stock_transfer_list';?>">
					<div class="row">
						
						<div class="col-sm-12 form-group">
							<!-- <label class="col-sm-2 control-label">Shipment To</label> -->
							<div class="col-sm-3">
								<select name="to_wh_id" class="select2"> 
									<option value="">- To Warehouse -</option>
									<?php 
										foreach($to_wh_list as $row)
										{
											$selected = ($row['wh_id']==@$search_data['to_wh_id'])?'selected="selected"':'';
											echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - ('.$row['name'].')</option>';
										}
									?>
								</select>
							</div>
							<!-- <label class="col-sm-2 control-label">ST Number</label> -->
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="stn_number" placeholder="ST Number" value="<?php echo @$search_data['stn_number'];?>" class="form-control">
							</div>
							<!-- <label class="col-sm-2 control-label">Shipment Date</label> -->
							<div class="col-sm-3">
								<input class="form-control date" required size="16" type="text" autocomplete="off"  value="<?php if($search_data['courier_date']!=''){ 
									$date = strtotime("-2 days", strtotime($search_data['courier_date']));
    								$courier_date =  date("d-m-Y", $date); echo $courier_date;
    								} ?>" readonly placeholder="Courier By Date" name="courier_date" style="cursor:hand;background-color: #ffffff">	
							</div>
							
							   <div class="col-sm-3 col-md-3">							
								<button type="submit" name="searchstocktransfer" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
								<a href="<?php echo SITE_URL.'wh_stock_transfer_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
							</div>
						</div>
						<div class="col-sm-12 form-group">
							<?php if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray'])) { ?>
							<!-- <label class="col-sm-2 control-label">Shipment From</label> -->
							<div class="col-sm-3">
								<select name="from_wh_id" class="select2" > 
									<option value="">- From Warehouse -</option>
									<?php 
										foreach($from_wh_list as $row)
										{
											$selected = ($row['wh_id']==@$search_data['from_wh_id'])?'selected="selected"':'';
											echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - ('.$row['name'].')</option>';
										}
									?>
								</select>
							</div>
							<?php } ?>
							<div class="col-sm-3">
								<?php 
								$d['name']        = 'ssoid';
								$d['search_data'] = @$search_data['ssoid'];
								$this->load->view('sso_dropdown_list',$d); 
								?>
							</div>
							<?php if($task_access == 3 && @$_SESSION['header_country_id']==''){?>
						   <!-- <label class="col-sm-2 control-label">Country</label> -->
							<div class="col-sm-3">
								<select class="select2" name="country_id" >
									<option value="">- Country -</option>
									 <?php
									foreach($countryList as $country)
									{
										$selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
										echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
									}
									?>
								</select>
							</div>
							<?php } ?>
							
						</div>
					</div>
				</form>
				<div class="table-responsive" style="margin-top: 10px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>ST Number</strong></th>
                                <th class="text-center"><strong>Shipping From</strong></th>
                                <th class="text-center"><strong>Shipping To</strong></th>
                                <th class="text-center"><strong>Destination</strong></th>
                                <th class="text-center"><strong>Courier By Date</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center" width="12%"><strong>Actions</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(count(@$st_results)>0)
                            {   
                                foreach(@$st_results as $row)
                                {
                                	
                                ?>
                                    <tr>
                                        <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                        <td class="text-center"><?php echo $sn++; ?></td>
                                        <td class="text-center"><?php echo $row['stn_number']; ?></td>
                                        <td class="text-center"><?php echo $row['from_wh_name']; ?></td>
                                        <td class="text-center">
	                                        <?php
	                                        if($row['fe_check'] == 0)
	                                        { 
	                                        	echo $row['to_wh_name'];
	                                        }
	                                        else 
	                                        {
	            								echo $row['user_name']; 
	                                        }
	                                        ?>	
                                        </td>
                                        <td class="text-center"><?php echo @$row['destination']; ?></td>
                                        <td class="text-center"><?php echo indian_format($row['courier_date']); ?></td>
                                        <td class="text-center"><?php echo @$row['country_name']; ?></td>
                                        <td class="text-center">
                                        
                                        <!-- Checking For Order ST or Only ST --> 
                                        <?php if(checkSTidExist($row['tool_order_id']) == 1){?>
                                        	<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'transfer_order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(2);?>"><i class="fa fa-eye"></i></a>                                   
                                        <?php }else { ?>
                                        	<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'wh_transfer_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(1);?>"><i class="fa fa-eye"></i></a>                                   
                                        <?php } ?>
                                        	<?php if($row['current_stage_id'] == 4){ ?>                                        	
		                                        <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Edit ST Details"  href="<?php echo SITE_URL.'edit_transfer/'.storm_encode($row['tool_order_id']).'/'.storm_encode(2);?>"><i class="fa fa-pencil"></i></a>
		                                        
		                                        <a class="btn btn-danger" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Cancel"  onclick="return confirm('Are you sure you want to Cancel?')" href="<?php echo SITE_URL.'cancel_transfer/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        	<?php } else{ ?>
                                        	<a class="btn btn-default" data-toggle="tooltip" title="Give Courier Details" style="padding:3px 3px;" href="<?php echo SITE_URL.'st_courier_details/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>
                                        	<?php  } ?>
                                        	
                                        </td>
                                    </tr>
                                <?php if(count(@$row['tools_list'])>0)
                                    {  $slno = 1; ?>
                                    <tr class="details">
                                        <td  colspan="9">
	                                        <table class="table">
	                                        	<thead>
						                            <tr>
						                                <th class="text-center"><strong>S.No</strong></th>
						                                <th class="text-center"><strong>Tool Number</strong></th>
						                                <th class="text-center"><strong>Tool Description</strong></th>
						                                <th class="text-center"><strong> Qty</strong></th>
						                            </tr>
						                        </thead>
	                                            <tbody>
	                                        <?php foreach(@$row['tools_list'] as $value)
	                                        { 
	                                        ?>
	                                            <tr class="asset_row">
	                                                <td class="text-center"><?php echo $slno++; ?></td>
	                                                <td class="text-center"><?php echo $value['part_number']; ?></td>
	                                                <td class="text-center"><?php echo $value['part_description']; ?></td>
	                                                <td class="text-center"><?php echo $value['quantity']; ?></td>
	                                            </tr>
	                                        <?php     
	                                        } ?>
												</tbody>
	                                        </table>
                                        </td>
                                    </tr><?php
                                    }
                                    else
                                    { ?>
                                        <tr><td colspan="9" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                    <?php 
                                    }
                                }
                            
                            } else {?>
                                <tr><td colspan="9" align="center"><span class="label label-primary">No Stock Transfer Shipment Request</span></td></tr>
                    <?php   } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$('.details').hide();
$(document).on('click',".toggle-details",function () { 
	var row=$(this).closest('tr');
	var next=row.next();
	$('.details').not(next).hide();
	$('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
	next.toggle();
	if (next.is(':hidden')) {
		$(this).attr('src',ASSET_URL+'images/plus.png');
	} else {
		$(this).attr('src',ASSET_URL+'images/minus.png');
	}
});
</script>