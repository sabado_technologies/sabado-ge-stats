<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <title>FE Invoice Print</title>
  <link rel="shortcut icon" href="<?php echo assets_url(); ?>images/favicon_1.png" />
  
<style type="text/css">
body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:15px; }
    
.page 
{
  padding:1cm;
  margin:1cm auto;
background: white;
}
@page {
  size: A4;
  margin: 0cm auto;
}
@media print 
{
  html, body 
  {
    width: 21cm;
    height: 29.70cm;
  }
  .no-print, .no-print *
    {
        display: none !important;
    }
}
table {
    border-collapse: collapse;
}
table, th, td {
    border: 1px solid black;
}
</style>
  
</head>

<body class="page">
<table align="left" cellspacing="0" border="0">
  <colgroup width="111"></colgroup>
  <colgroup width="66"></colgroup>
  <colgroup span="3" width="64"></colgroup>
  <colgroup width="75"></colgroup>
  <colgroup span="5" width="64"></colgroup>
  <colgroup width="86"></colgroup>
  <colgroup span="6" width="64"></colgroup>
  <colgroup width="86"></colgroup>
  <colgroup width="104"></colgroup>
  <tr>
    <td colspan=20 align="center">
            <img src='<?php echo assets_url()."images/wipro.png"?>' align="left" style="max-width:10%; padding:3px; margin:0px;"/>
            <img src='<?php echo assets_url()."images/ge.png"?>' align="left" style="max-width:10%; padding:3px; margin:0px;"/>
            <h3><br>TAX INVOICE (INTERNAL STOCK TRANSFER)<br>WIPRO GE HEALTHCARE PRIVATE LIMITED | WWW.WIPROGE.COM<br>Regd Office : NO 4 Kadugodi Industrial Area, Sadaramangala, Bangalore - 560067<br>Tel : 91-80-4180 1000/1224/1225  Fax : 91-80-2845 2924 </h3>
    </td>
    </tr>
  <tr height=21>
    <td colspan=11 align="center"><b>Goods Shipped from</b></td>
    <td colspan=2  align="center"><b>Invoice Number</b></td>
    <td colspan=2  align="center"><b>Invoice date</b></td>
    <td colspan=5  align="center"><b>Total Value</b></td>
  </tr>
  <tr height="21">
    <td colspan=6  align="center"><b>Billed from Address</b></td>
    <td colspan=5  align="center"><b>Dispatched from Address</b></td>
    <td colspan=2  align="center"><?php echo @$delivery['invoice_number']; ?></td>
    <td colspan=2  align="center"><?php echo date('Y-m-d',strtotime(@$delivery['created_time']));?></td>
    <td colspan=6  align="center">177000</td>
    </tr>
  <tr>
    <td height="63" colspan=6 rowspan=3  align="center" valign=middle>
      <?php echo @$tool_order['wh1_add1'].', '.@$tool_order['wh1_add2'].', '.@$tool_order['wh1_add3'].', '.@$tool_order['wh1_add4'].', '.@$tool_order['wh1_location'].' - '.@$tool_order['wh1_pincode']; ?></td>
    <td height="63" colspan=5 rowspan=3 align="center" valign=middle>
      <?php echo @$tool_order['wh1_add1'].', '.@$tool_order['wh1_add2'].', '.@$tool_order['wh1_add3'].', '.@$tool_order['wh1_add4'].', '.@$tool_order['wh1_location'].' - '.@$tool_order['wh1_pincode']; ?></td>
    <td height="21" align="center" colspan="4">Payment Terms</td>
    <td height="21" colspan=5 align="center"><br></td>
  </tr>
  <tr height="21" >
    <td colspan=4 align="center">Tax Remarks ( If Any)</td>
    <td colspan=5 align="center"><br></td>
  </tr>
  <tr height="21">
    <td colspan=4 align="center">PO Reference</td>
    <td colspan=5 align="center"><br></td>
    </tr>
  <tr height="21">
    <td colspan=2 align="left">PAN NO :</td>
    <td colspan=9 align="center"><br></td>
    <td colspan=2 align="left">Sales Order NO :</td>
    <td colspan=2 align="center"><br></td>
    <td colspan=2 align="center">Customer Code</td>
    <td colspan=3 align="center"><br></td>
  </tr>
  <tr height=21>
    <td colspan=2 align="left">GOLD CARD NO :</td>
    <td colspan=9 align="center"><br></td>
    <td colspan=2 align="left">License Validity NO :</td>
    <td colspan=2 align="center"><br></td>
    <td colspan=2 align="center">License Validity Date</td>
    <td colspan=3 align="center"><br></td>
    </tr>
  <tr height=21>
    <td colspan=2 align="left">CIN NO :</td>
    <td colspan=9 align="center"><br></td>
    <td colspan=9 align="left"><br></td>
  </tr>
  <tr height="21">
    <td colspan=2 align="left">GSTIN NO :</td>
    <td colspan=9 align="center">07AAACW1685J1Z2</td>
    <td colspan=9 align="left"><br></td>
  </tr>
  <tr height="21">
    <td colspan=6  align="center"><b>Billed From</b></td>
    <td colspan=5 align="center"><b>Place of Supply</b></td>
    <td colspan=9 align="center"><b>Shipped to</b></td>
    </tr>
  <tr height="60">
    <td colspan=6 align="center">
      wipro GE Healthcare pvt ltd No 4, kadgudi industrail area sadaramangala bengaluru-67
    </td>
    <td colspan=5 align="center">
      <?php echo @$tool_order['wh2_location']; ?>
    </td>
    <td colspan=9 align="center">
      <?php echo @$tool_order['wh2_add1'].', '.@$tool_order['wh2_add2'].', '.@$tool_order['wh2_add3'].', '.@$tool_order['wh2_add4'].', '.@$tool_order['wh2_pincode']; ?>
    </td>
  </tr>
  <tr height=21>
    <td colspan=2 align="center"><b>PAN NO</b></td>
    <td colspan=4 align="center"><br></td>
    <td colspan=2 align="center"><b>PAN NO</b></td>
    <td colspan=3 align="center"><br></td>
    <td colspan=4 align="center"><b>PAN NO</b></td>
    <td colspan=5 align="center">AAACY0508N</td>
    </tr>
  <tr height="21">
    <td colspan=2 align="center"><b>GSTIN NO</b></td>
    <td colspan=4 align="center">29AAACW1685J1ZW</td>
    <td colspan=2 align="center"><b>GSTIN NO</b></td>
    <td colspan=3 align="center"><br></td>
    <td colspan=4 align="center"><b>GSTIN NO</b></td>
    <td colspan=5 align="center"><br></td>
    </tr>
  <tr height=42>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">SL.NO#</b></td>
    <td align="center" rowspan="2"valign=middle><b><font face="Arial">PRODUCT CODE</b></td>
    <td colspan=3 rowspan="2" align="center" valign=middle><b><font face="Arial">DESCRIPTION</b></td>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">HSN / SAC Code</b></td>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">QTY</b></td>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">UOM</b></td>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">RATE (RS.)</b></td>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">TOTAL</b></td>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">DISCOUNT</b></td>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">TAXABLE VALUE</b></td>
    <td colspan=2 align="center" valign=middle><b><font face="Arial">CGST</b></td>
    <td colspan=2 align="center" valign=middle><b><font face="Arial">SGST</b></td>
    <td colspan=2 align="center" valign=middle><b><font face="Arial">IGST</b></td>
    <td align="center" rowspan="2" valign=middle><b><font face="Arial">TAX TOTAL VALUE</b></td>
    <td align="center" rowspan="2" valign=middle sdnum="1033;0;0.00E+00"><b><font face="Arial">AMOUNT (RS.)</b></td>
  </tr>
  <tr>
    <td align="center"  valign=middle><b><font face="Arial">Rate</b></td>
    <td align="center"  valign=middle><b><font face="Arial">Amount</b></td>
    <td align="center"  valign=middle><b><font face="Arial">Rate</b></td>
    <td align="center"  valign=middle><b><font face="Arial">Amount</b></td>
    <td align="center"  valign=middle><b><font face="Arial">Rate</b></td>
    <td align="center"  valign=middle><b><font face="Arial">Amount</b></td>
  </tr>
  <?php $sn = 1;
  foreach ($tools_list as $key => $value) 
  { ?>
    <tr height="20">
    <td align="center"><?php echo $sn++; ?></td>
    <td align="left" valign=top><?php echo $value['part_number']; ?></td>
    <td colspan=3 align="center"><?php echo $value['part_description']; ?></td>
    <td align="center"><?php echo $value['hsn_code']; ?></td>
    <td align="center"><?php echo $value['quantity']; ?></td>
    <td align="center"></td>
    <td align="left"><br></td>
    <td align="left"><br></td>
    <td align="left"></td>
    <td align="right"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="right"></td>
    <td align="right"></td>
  </tr>
    
  <?php } ?>
  
  <?php 
  if(count($tools_list)<10)
  {
    $tool_count = count($tools_list);
    $i = 10;
    $val = $i-$tool_count;
    for($j = 0; $j<$val; $j++)
    { ?>
    <tr height="20">
      <td></td>
      <td></td>
      <td  colspan=3></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <?php }
  } ?>
  
  
  <tr height="18">
    <td colspan=20 align="left"><b><font face="Arial">Comments / Remarks :</b></td>
    </tr>
  <tr height="15">
    <td colspan=11 align="center"><font face="Arial">Mode of transport</td>
    <td colspan=9  align="center"><font face="Arial"></td>
    </tr>
  <tr height="15">
    <td colspan=11 align="center"><font face="Arial">Hypotheticated to</td>
    <td colspan=9  align="center"><font face="Arial"><br></td>
    </tr>
  <tr height="15">
    <td colspan=11 align="center"><font face="Arial">LC Free text</td>
    <td colspan=9  align="center"><font face="Arial"><br></td>
    </tr>
  <tr height="15">
    <td colspan=11 align="center"><font face="Arial">Airway Bill #</td>
    <td colspan=9  align="center"><font face="Arial"><br></td>
    </tr>
  <tr height="15">
    <td colspan=11 align="center"><font face="Arial">SR#</td>
    <td colspan=9  align="center"><font face="Arial"><br></td>
    </tr>
  <tr height="15">
    <td colspan=11 align="center"><font face="Arial">FE Name &amp; SSO</td>
    <td colspan=9  align="center"><font face="Arial"><br></td>
    </tr>
  <tr height="15">
    <td colspan=11 align="center"><font face="Arial">whether reverse charge applicable</td>
    <td colspan=9  align="center"><font face="Arial"><br></td>
    </tr>
  <tr height="15">
    <td colspan=11 align="center"><font face="Arial">Freight &amp; insurance</td>
    <td colspan=9  align="center"><font face="Arial"><br></td>
    </tr>
  <tr height="21">
    <td colspan=2  align="right"><b><font face="Arial">Value In Words :</b></td>
    <td colspan=13 align="center">one lakh seventy seven thousand only</td>
    <td colspan=2  align="right"><b><font face="Arial">Total Value :</b></td>
    <td colspan=3  align="center"><b>177000</b></td>
    </tr>
  <tr height="21">
    <td colspan=9 align="center"><b>Our Bank Details</b></td>
    <td colspan=6 align="center"><b>Payment Details/Cheques DD to be sent to</b></td>
    <td colspan=5 align="center"><b>Enquiry Details</b></td>
    </tr>
  <tr height="21">
    <td colspan=3 align="center">Bank Name</td>
    <td colspan=6 align="center">HSBC Corp Limited</td>
    <td colspan=2 align="center">Attention</td>
    <td colspan=4 align="center">GEHC Collection team</td>
    <td colspan=2 align="center" rowspan=3>HelpLine Numbers</td>
    <td colspan=3 align="center" rowspan=3>91-80-4180 1000<br>91-80-4180 1224<br>91-80-4180 1225</td>
  </tr>
  <tr height="21">
    <td colspan=3 align="center">Account #</td>
    <td colspan=6 align="center">071-062327-002</td>
    <td colspan=2 align="center" rowspan=3>Address</td>
    <td colspan=4 align="center" rowspan=3>
      Regd Office : NO 4, Kadugodi Industrial Area, Sadaramangala, Whitefield 560067<br>Tel : 91-80-4180 1000<br>Fax : 91-80-28452924
    </td>
  </tr>
  <tr height="21">
    <td colspan=3 align="center">Address</td>
    <td colspan=6 align="center">#7 MG Road Bangalore 560011 India</td>
  </tr>
  <tr height="21">
    <td colspan=3  align="center">IFSC/RTGS Code</td>
    <td colspan=6 align="center">HSBC01INDIA / HSBC0560002</td>
    <td colspan=2 align="center">Email</td>
    <td colspan=3 align="center">WGE.Support@ge.com</td>
    </tr>
  <tr>
    <td colspan=20 align="center">Overdue Interest @ 24% per annum should be paid if payment is not made on due date. Payment of our bills may be made to our corporate office or to our nearest Branch Office. Branches at ' Ahmedabad, Bangalore, Bhopal, Kolkata, Chandigarh, Chennai, Cochin,  Jaipur, Lucknow, Mumbai, New Delhi, Patna, Pune, Secundrabad. As a Policy we do not want to accept any payment of cash. Further all payments upto RS 50,000/- must be paid by cheque &amp; not by Demand Draft. Cheques are subjected to realisation.<br>We solocit your co-operation in this regard.</td>
  </tr>
  <tr height="18">
    <td colspan=20 align="left"><b>STANDARD TERMS AND CONDITIONS OF SALE</b></td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td colspan=19 align="left">It is Essential that you notify our nearest branch office of any damage or short receipt of supplies within seven(7) days of its arrival at destination. We will not accept any responsibility for any damage or short receipt of supplies notified after seven(7) days from its arrival at destination</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td colspan=19 align="left">Every care has been taken to pack the materials securely, should you observe any outward damages to the packages, please ensure that you take &quot;Open Delivery&quot; and simultaneously lodge claim with the carriers</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td colspan=19 align="left">We are not liable for any consequestial damages which may arise during the course of usage of the equipment</td>
  </tr>
  <tr>
    <td align="center">4</td>
    <td colspan=19 align="left">Terms and Conditions of Warranty per purchase order/sale contract form an integral part of this invoice</td>
  </tr>
  <tr>
    <td align="center">5</td>
    <td colspan=19 align="left">Other terms and conditions as per quotation/purchase order/sale contract and other Wipro GE Healthcare Private limited documents shall apply</td>
  </tr>
  <tr height=20>
    <td colspan=12 align="center"><b>GST Certificate</b></td>
    <td colspan=8 align="center"><b>For Wipro GE Healthcare Private Limited</b></td>
    </tr>
  <tr height=60>
    <td colspan=12  align="center">We hereby certify that our GST numbers as mentioned above are in force on the date of which the sale of the goods covered in this invoice is made by us and that the transcation of sale covered by this invoice has been affected by us in the course of our business.</td>
    <td colspan=8 align="center" valign="bottom"><b>Authorised Signatory</b></td>
  </tr>
  <tr  height="21">
    <td colspan=12 align="center"><b>I certify that the satetements contained in this invoice are true and correct.<br>Electronic Reference number :</b></td>
    <td colspan=8 align="center"><b>ORIGINAL COPY</b></td>
    </tr>
</table>

<div style="text-align:center">
  <button class="button no-print"  onclick="print_srn()" style="background-color:#3598dc;margin-top: 20px;margin-bottom: 20px;">Print</button>
  <a class="button no-print" href="<?php echo SITE_URL.'closed_wh_delivery_list';?>">Cancel</a>
</div>
</body>
</html>
<script type="text/javascript">
    function print_srn()
    {
        window.print(); 
    }
</script>