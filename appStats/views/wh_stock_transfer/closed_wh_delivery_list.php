<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
        <?php
		if(isset($flg))
		{
	    ?>
	    <div class="block-flat">
			<div class="content">
				<div class="col-sm-12 col-md-12">
					<div class="row">							
						<div class="row" style="padding: 10px;">							
							<div class="header">
								<h4 align="center"><strong>Stock Transfer Info</strong></h4>
							</div>
						</div>
						<div class="col-md-6">
							<table class="no-border">
								<tbody class="no-border-x no-border-y">
									<tr>
										<td class="data-lable"><strong>ST Number:</strong></td>
								        <td class="data-item"><?php echo @$trow['stn_number'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Request Date:</strong></td>
								        <td class="data-item"><?php echo indian_format(@$trow['request_date']);?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Address1:</strong></td>
								        <td class="data-item"><?php echo @$trow['address1'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>City:</strong></td>
								        <td class="data-item"><?php echo @$trow['address3'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Pin Code:</strong></td>
								        <td class="data-item"><?php echo @$trow['pin_code'];?></td>
									</tr>

									<tr>
										<td class="data-lable"><strong><u>Courier Details :</u></strong></td>
								        <td class="data-item"></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Courier Name:</strong></td>
								        <td class="data-item"><?php echo @$drow['courier_name'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Contact Person:</strong></td>
								        <td class="data-item"><?php echo @$drow['contact_person'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Expected Delivery Date:</strong></td>
								        <td class="data-item"><?php echo indian_format(@$drow['expected_delivery_date']);?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-6">
							<table class="no-border">
								<tbody class="no-border-x no-border-y">
									<?php 
									if(@$trow['fe_check'] == 0){ ?>
									
									<tr>
										<td class="data-lable"><strong>To Warehouse:</strong></td>
										<td class="data-item"><?php echo @$warehouse['wh_code'].' -('.@$warehouse['name'].')'?></td>
									</tr>
									<?php
								}
								else if(@$trow['fe_check'] == 1)
								{?>
									<tr>
										<td class="data-lable"><strong>To SSO:</strong></td>
										<td class="data-item"><?php echo @$sso_name; ?></td>
									</tr>
								<?php } ?>
									<tr>
										<td class="data-lable"><strong>Sent Date:</strong></td>
										<td class="data-item"><?php echo indian_format(@$drow['created_time']);?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Address2:</strong></td>
								        <td class="data-item"><?php echo @$trow['address2'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>State:</strong></td>
								        <td class="data-item"><?php echo @$trow['address4'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Courier Type:</strong></td>
								        <td class="data-item"><?php if(@$drow['courier_type']==1)
										        {
										            echo "By Courier";
										        }
										        else if(@$drow['courier_type']==2)
										        {
										        	echo "By Hand";
										        }
										        else if(@$drow['courier_type']==3)
										        {
										        	echo "By Dedicated";
										        }
										        ?></td>
									</tr>

									<tr>
										<td class="data-lable"></td>
								        <td class="data-item">.</td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Docket/AWB number:</strong></td>
								        <td class="data-item"><?php echo @$drow['courier_number'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Phone Number:</strong></td>
								        <td class="data-item"><?php echo @$drow['phone_number'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Vehicle Number :</strong></td>
								        <td class="data-item"><?php echo @$drow['vehicle_number'];?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div><br>
				</div>

				<div class="col-sm-12 col-md-12"><br>
					<div class="row">							
						<div class="header">
						<h4 align="center"><strong>Requested Tools</strong></h4>
					</div>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table hover">
							<thead>
								<tr>
	                            	<th class="text-center"><strong>S.No</strong></th>
	                                <th class="text-center"><strong>Tool No</strong></th>
	                                 <th class="text-center"><strong>Tool Desc</strong></th>
	                                 <th class="text-center"><strong>Ordered Qty</strong></th>
	                                <th class="text-center"><strong>Assigned Asset(s)</strong></th>
	                                <th class="text-center"><strong>Assigned Serial No(s)</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php 
								if(count(@$order_tool)>0)
								{	$sn = 1;
									foreach(@$order_tool as $row)
									{
									?>
										<tr>
											<td class="text-center"><?php echo $sn++; ?></td>
											<td class="text-center"><?php echo $row['part_number']; ?></td>
											<td class="text-center"><?php echo $row['part_description']; ?></td>
											<td class="text-center"><?php echo $row['quantity']; ?></td>
											<td class="text-center"><?php echo $row['assets']; ?></td>
											<td class="text-center"><?php echo $row['serial_nos']; ?></td>
										</tr>
							<?php   }
								} else {?>
									<tr><td colspan="6" align="center">No Records Found.</td></tr>
	                    <?php 	} ?>
							</tbody>
						</table>
	                </div><br>
				</div>
				<div class="row">							
					<div class="header">
						<h4 align="center"><strong>Attached Documents</strong></h4>
					</div>
					<br>
					<div class="table-responsive col-md-offset-1 col-md-10">
						<table class="table hover">
							<thead>
								<tr>
									<th>S No.</th>
									<th width="18%" class="text-center"><strong>Attached Date</strong></th>
									<th class="text-center"><strong>Document Type</strong></th>
									<th width="30%" class="text-center"><strong>Supported Document</strong></th>
									
								</tr>
							</thead>
							<tbody>
							<?php
								if(count(@$attach_document)>0)
								{	$count = 1;
									foreach($attach_document as $ad)
									{?>	
										<tr>
											<td><?php echo $count++; ?></td>
											<td><?php echo $ad['created_time']; ?></td>
											<td align="center">
													<?php 
														foreach($documenttypeDetails as $doc)
														{
															if($doc['document_type_id']==@$ad['document_type_id'])
															{
																echo $doc['name']; 
															}
														}
													?>
											</td>
											<td>
		                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
											</td>
										</tr>
									 <?php		
									} 
								} 
								else {
									?>	
									<tr><td colspan="4" align="center">No Attached Docs.</td></tr>
								<?php } ?>
							</tbody>
						</table><br>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-5">
							<?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>
							<a class="btn btn-primary" href="<?php echo SITE_URL.'closed_wh_delivery_list'.$current_offset;?>"><i class="fa fa-reply"></i> Back</a>
						</div>
					</div>
				</div><br>

			</div>
		</div>
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'closed_wh_delivery_list';?>">
					<div class="row">
						<div class="col-sm-12 form-group">
								<?php if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray'])) { ?>
								<label class="col-sm-2 control-label">From Warehouse</label>
								<div class="col-sm-3">
									<select name="from_wh_id" class="select2" > 
										<option value="">- From Warehouse -</option>
										<?php 
											foreach($from_wh_list as $row)
											{
												$selected = ($row['wh_id']==@$search_data['from_wh_id'])?'selected="selected"':'';
												echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - ('.$row['name'].')</option>';
											}
										?>
									</select>
								</div>
								<?php } ?>
								<?php if($task_access == 3 && @$_SESSION['header_country_id']==''){?>
							    <label class="col-sm-1 control-label">Country</label>
								<div class="col-sm-3">
									<select class="select2" name="country_id" >
										<option value="">- Country -</option>
										 <?php
										foreach($countryList as $country)
										{
											$selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
											echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
										}
										?>
									</select>
								</div>
								<?php } ?>
						</div>
						<div class="col-sm-12 form-group">
							<label class="col-sm-2 control-label">To WareHouse</label>
							<div class="col-sm-3">
								<select name="to_wh_id" class="select2" > 
									<option value="">- To Warehouse -</option>
									<?php 
										foreach($to_wh_list as $row)
										{
											$selected = ($row['wh_id']==@$search_data['to_wh_id'])?'selected="selected"':'';
											echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - ('.$row['name'].')</option>';
										}
									?>
								</select>
							</div>
							<label class="col-sm-1 control-label">ST No.</label>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="stn_number" placeholder="ST Number" value="<?php echo @$search_data['stn_number'];?>" class="form-control">
							</div>
							<div class="col-sm-3 col-md-3">							
								<button type="submit" name="searchwhdelivery" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
								<a href="<?php echo SITE_URL.'closed_wh_delivery_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
							</div>
						</div>
					</div>
					
				</form>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>ST Number</strong></th>
                                <th class="text-center"><strong>Shipping From</strong></th>
                                <th class="text-center"><strong>Shipping To</strong></th>
                                <th class="text-center"><strong>Destination</strong></th>
                                <th class="text-center"><strong>Delivered Date</strong></th>
						        <th class="text-center"><strong>Stock Transfer Status </strong></th>
						        <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center" width="16%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if(count($closed_wh_deliveryResults)>0)
							{
								foreach($closed_wh_deliveryResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++; ?></td>
										<td class="text-center"><?php echo $row['stn_number']; ?></td>
										<td class="text-center"><?php echo $row['from_wh_name']; ?></td>
										<td class="text-center">
                                        <?php
                                        if($row['fe_check'] == 0)
                                        { 
                                        	echo $row['to_wh_name']; 
                                        }
                                        else 
                                        {
                                        	$sso = $this->Wh_stock_transfer_m->get_fe_sso($row['tool_order_id']);
            								echo $sso['sso_name']; 
                                         }?></td>
										<td class="text-center"><?php echo $row['location_name']; ?></td>
										<td class="text-center"><?php echo ($row['status']==4)?'':indian_format($row['delivered_date']); ?></td>
										<td class="text-center">
											<?php
											$current_stage_id = $this->Common_model->get_value('tool_order',array('tool_order_id'=>$row['tool_order_id']),'current_stage_id');
											if($current_stage_id ==3)
											{
												 $current_stage = 'Closed';
											}
											else
											{
												if($row['fe_check'] == 0)
		                                        	$current_stage = $row['current_stage']; 
		                                        else 
		                                       		$current_stage = 'In Transit WH to FE';
		                                         
		                                    } ?>
										<?php echo ($row['status']==4)?'Cancelled':$current_stage; ?></td>
										<td class="text-center"><?php echo @get_country_location($row['country_id']); ?></td>
										<td class="text-center">
										<?php if($row['status'] == 4) { ?>
										<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'wh_transfer_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(2);?>"><i class="fa fa-eye"></i></a>                                   
										<?php } else { ?>
										<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View Delivery Details"  href="<?php echo SITE_URL.'view_wh_delivery_details/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-eye"></i></a>
										<a class="btn btn-primary" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Print" target="_blank" href="<?php echo SITE_URL.'wh_st_invoice_print/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-print"></i></a>
                                        
                                        <?php if($row['cs']<3){?>
										<a class="btn btn-warning" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Re-Print"  href="<?php echo SITE_URL.'update_wh_print/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-danger" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Cancel Print"  href="<?php echo SITE_URL.'cancel_wh_print/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-trash-o"></i></a>
										<?php } ?>
                                    	<?php }?>    
                                    	</td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="9" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>
		<?php
		}
		?>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>