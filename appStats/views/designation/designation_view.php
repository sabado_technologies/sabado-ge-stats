<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
							<?php
	                        if($flg==2){
	                            ?>
	                             <input type="hidden" name="encoded_id" value="<?php echo storm_encode($lrow['designation_id']);?>">
	                             <input type="hidden" name="desig_id" id="designation_id" value="<?php echo $lrow['designation_id']; ?>">
	                            <?php
	                           }
	                        ?>
	                        <div class="form-group">
								<label for="inputName" class="col-sm-4 control-label">Role<span class="req-fld">*</span></label>
								<div class="col-sm-5">
                                    <?php echo form_dropdown('role_id', $name, @$lrow['role_id'],'class="form-control"') ;?>
                                </div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-sm-4 control-label">Designation<span class="req-fld">*</span></label>
								<div class="col-sm-5">
									<input type="text" autocomplete="off" required=""  class="form-control" id="designation_name" placeholder="Designation" name="designation_name" value="<?php echo @$lrow['designation_name']; ?>">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-5">
									<button class="btn btn-primary" type="submit" name="submit_designation"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>designation"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
		<?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>designation">
						<div class="col-sm-12">
							<label class="col-sm-1 control-label">Role</label>
							<div class="col-sm-2">
                             	<?php echo form_dropdown('roleid', $name, @$search_data['roleid'],'class="form-control" ');?>
							</div>

							
							<label class="col-sm-1 control-label">Designation</label>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="designation_name" value="<?php echo @$search_data['designame'];?>" class="form-control">
							</div>
							<div class="col-sm-3">
								<button type="submit" name="searchdesignation" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
								<button type="submit" name="refreshdesignation" data-toggle="tooltip" title="Refresh" value="1" class="btn btn-success"><i class="fa fa-refresh"></i></button>
								<a href="<?php echo SITE_URL.'add_designation';?>" data-toggle="tooltip" title="Add New" class="btn btn-success"><i class="fa fa-plus"></i></a>
								<button type="submit" data-toggle="tooltip" title="Download" name="download_designation" value="1" formaction="<?php echo SITE_URL.'download_designation';?>" onclick="return confirm('Are you sure you want to Download?')" class="btn btn-success"><i class="fa fa-cloud-download"></i></button>  
							</div>
						</div>
					</form>
				</div><br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Role</strong></th>
                                <th class="text-center"><strong>Designation</strong></th>
                                <th class="text-center"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($designationResults)>0)
							{
								foreach($designationResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo $row['role_name']; ?></td>
										<td class="text-center"><?php echo $row['designation_name']; ?></td>
										<td class="text-center">
                                        <?php
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-default" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_designation/'.storm_encode($row['designation_id']);?>" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

                                        <?php } else{?>
                                        <a class="btn btn-default" style="padding:3px 3px;" href="#" readonly data-toggle="tooltip" title="Activate Designation to edit"><i class="fa fa-pencil"></i></a>
										<?php }
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_designation/'.storm_encode($row['designation_id']);?>" data-toggle="tooltip" title="Deactivate"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_designation/'.storm_encode($row['designation_id']);?>" data-toggle="tooltip" title="Activate"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
                                    </td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
		<?php } ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
check_designation_name_availability();	
</script>