
</div>
</div>
  <!-- Java Scripts -->
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery.select2/select2.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery.nestable/jquery.nestable.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/behaviour/general.js"></script>
  <?php if(@$enableFormWizard!=1) { ?>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
  <?php } ?>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery.magnific-popup/dist/jquery.magnific-popup.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
  <script type="text/javascript" src="<?php echo assets_url(); ?>js/storm/audit.js"></script>

  <?php
  if(count($js_includes)>0)
  {
    foreach($js_includes as $js_file)
    {
      echo $js_file;
    }
  }
  ?>
  
  
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script type="text/javascript">
    $(document).ready(function()
    {
      select2Ajax('sso_dropdown_list', 'sso_dropdown_list', 0, 2);
      var s_role_id = <?php echo $this->session->userdata('role_id'); ?>;
      select2Ajax('popup_sso', 'all_sso_dropdown_list', s_role_id, 2);
      $('.list_item').hide();
      $('.repair_list_item').hide();
      $('.cl-vnavigation').find('.sub-menu').addClass('side_bar_check');
      //$('.alert').fadeTo(5000,1000).slideUp(500);
      App.init();
      $(".expected_date").datetimepicker({autoclose: true,format:"dd-mm-yyyy",startDate: new Date()});
      <?php if(@$enableFormWizard==1)
      { ?>
        App.wizard(); <?php 
      } ?>

      $(".date").datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      // minDate: 0,
      onSelect: function (date) 
      {
       var date2 = $(this).datepicker('getDate');
        //$('.date').datepicker('option', 'minDate', date2);
      }
      });
    });
  //koushik 30-05-2018 start
  $(document).on('click','.show_list',function(){
    $('.list_item').slideDown();
    $('.notification_cal_icon').addClass('hidden');
    $(".show_list").removeClass('show_list').addClass('hide_list');
  });

  $(document).on('click','.hide_list',function(){
    $('.list_item').slideUp();
    $('.notification_cal_icon').removeClass('hidden');
    $(".hide_list").removeClass('hide_list').addClass('show_list');
  });

  $(document).on('click','.show_repair_list',function(){
    $('.repair_list_item').slideDown();
    $('.notification_repair_icon').addClass('hidden');
    $(".show_repair_list").removeClass('show_repair_list').addClass('hide_repair_list');
  });

  $(document).on('click','.hide_repair_list',function(){
    $('.repair_list_item').slideUp();
    $('.notification_repair_icon').removeClass('hidden');
    $(".hide_repair_list").removeClass('hide_repair_list').addClass('show_repair_list');
  });

  $("#dateFrom").datepicker({
  //alert('koko');
      dateFormat: "dd-mm-yy",
      changeMonth: true,
          changeYear: true,
           // minDate: 0,
            onSelect: function (date) {                  
                var date2 = $(this).datepicker('getDate');
                $('#dateTo').datepicker('option', 'minDate', date2);
            }
        });
    $("#dateTo").datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
          changeYear: true,
            onSelect: function (date) {                  
                var date2 = $(this).datepicker('getDate');
                $('#dateFrom').datepicker('option', 'maxDate', date2);                   
            }
        });
    $(document).on('change','#document .document',function(){                
      var ext = $(this).val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['jpg','png','pdf','gif']) == -1) {
          alert('invalid extension! allowed .pdf, .jpg, .png, .gif only');
          $(this).val('');
          //$('#doc').removeClass('hidden');
          return false;
      }
  });

  $(document).on('click','.top_header_check, .side_bar_check',function(){
    var parent_page = $('.parent_page').val();
    var page_list = $('.page_list').val();
    var page_list_arr = page_list.split(',');
    if(jQuery.inArray(parent_page,page_list_arr)!="-1")
    {
      var result = confirm('Are you sure! You want to switch to other Screen? Your filled information will be discarded !');
        if(result == false)
        {
          return false;
        }
    }
    
  });

  $('.notification_details').hide();
  $(document).on('click',".notification_toggle-details",function () 
  { 
    var row=$(this).closest('tr');
    var next=row.next();
    $('.notification_details').not(next).hide();
    next.toggle();
  });

  
  //koushik 30-05-2018 end
  </script>

</body>
</html>