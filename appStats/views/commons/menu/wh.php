<li><a href="#"><i class="fa fa-hand-o-up"></i><span>Raise A New Request</span></a>
  <ul class="sub-menu">
    <li <?php if($parent_page=='raise_transfer') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>raise_transfer"><i class="fa fa-truck"></i><span>Stock Transfer </span></a></li>
    <li <?php if($parent_page=='asset_condition_check') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>asset_condition_check"><i class="fa fa-exclamation-triangle"></i><span>Defective Asset</span></a></li>
    
  </ul>
</li>
<li><a href="#"><i class="fa fa-calendar"></i><span>Shipment Requests</span></a>
  <ul class="sub-menu">
    <li <?php if($parent_page=='fe_delivery') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>fe_delivery_list"><i class="fa fa-wrench"></i><span>Field Engineer</span></a></li>
    <li <?php if($parent_page=='wh_stock_transfer_list') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>wh_stock_transfer_list"><i class="fa  fa-truck"></i><span>Stock Transfer</span></a></li>
    <li <?php if($parent_page=='pickup_list') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>pickup_list"><i class="fa fa-retweet"></i><span>Pickup</span></a></li>
    <li <?php if($parent_page=='wh_calibration') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>wh_calibration"><i class="fa fa-compass"></i><span>Calibration </span></a></li>
    <li <?php if($parent_page=='wh_repair') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>wh_repair"><i class="fa fa-chain-broken"></i><span>Repair</span></a></li>
  </ul>
</li>
<li><a href="#"><i class="fa fa-qrcode"></i><span>Acknowledge Returns</span></a>
  <ul class="sub-menu">
    <li <?php if($parent_page=='open_fe_returns') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>open_fe_returns"><i class="fa fa-share-square-o"></i><span>Field Engineer</span></a></li>
    <li <?php if($parent_page=='wh2_wh_receive') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>wh2_wh_receive"><i class="fa fa-pencil-square-o"></i><span>Stock Transfer</span></a></li>
    <li <?php if($parent_page=='acknowledge_cal_tools') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>vendor_calibration"><i class="fa fa-external-link"></i><span>Calibration </span></a></li>
    <li <?php if($parent_page=='vendor_wh_repair_request') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>vendor_wh_repair_request"><i class="fa fa-link"></i><span>Repair</span></a></li>
  </ul>
</li>
<li><a href="#"><i class="fa fa-folder"></i><span>Closed Requests</span></a>
  <ul class="sub-menu">
    <li <?php if($parent_page=='closed_fe_delivery') echo 'class="active"';?>>
    	<a href="<?php echo SITE_URL; ?>closed_fe_delivery_list"><i class="fa  fa-book"></i><span>Field Engineer</span></a>
    </li>
    <li <?php if($parent_page=='closed_wh_delivery_list') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_wh_delivery_list"><i class="fa  fa-dashboard (alias)"></i><span>Stock Transfer</span></a></li>
    <li <?php if($parent_page=='closed_pickup_list') echo 'class="active"';?>>
        <a href="<?php echo SITE_URL; ?>closed_pickup_list"><i class="fa fa-dropbox"></i><span>Pickup</span></a>
    </li> 

    <li <?php if($parent_page=='closed_cal_delivery_list') echo 'class="active"';?>>
        <a href="<?php echo SITE_URL; ?>closed_cal_delivery_list"><i class="fa fa-gavel"></i><span>Calibration</span></a>
    </li>

    <li <?php if($parent_page=='closed_repair_delivery_list') echo 'class="active"';?>>
        <a href="<?php echo SITE_URL; ?>closed_repair_delivery_list"><i class="fa fa-link"></i><span>Repair</span></a>
    </li>

    <li <?php if($parent_page=='closed_fe_returns') echo 'class="active"';?>>
    	<a href="<?php echo SITE_URL; ?>closed_fe_returns"><i class="fa  fa-book"></i><span>FE Returns</span></a>
    </li>
    <li <?php if($parent_page=='wh2_wh_closed') echo 'class="active"';?>>
    	<a href="<?php echo SITE_URL; ?>wh2_wh_closed"><i class="fa  fa-dashboard (alias)"></i><span>Stock Transfer Returns</span></a>
    </li>
  </ul>
</li>

<li class="side_bar_check <?php if($parent_page=='calibration_certificates'){ echo "active"; }?>"><a href="<?php echo SITE_URL; ?>calibration_certificates"><i class="fa  fa-compass"></i><span>Calibration Certificates</span></a></li>

<li class="side_bar_check <?php if($parent_page=='tools_inventory'){ echo "active"; }?>"><a href="<?php echo SITE_URL; ?>tools_inventory"><i class="fa fa-gavel"></i><span>Tools Inventory Report</span></a></li>

<li class="side_bar_check <?php if($parent_page=='download_report'){ echo "active"; }?>"><a href="<?php echo SITE_URL; ?>download_report"><i class="fa fa-print nav-icon"></i><span>Download Invoices/DC</span></a></li>