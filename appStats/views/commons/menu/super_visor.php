<li><a href="#"><i class="fa fa-bar-chart-o"></i><span>Dashboards</span></a>
    <ul class="sub-menu">    
        <li <?php if($parent_page=='toolsReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>tools_report"><i class="fa fa-gavel"></i><span>Tools Inventory </span></a></li>
        <li <?php if($parent_page=='calibrationDueReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>cal_report"><i class="fa  fa-compass"></i><span>Calibration Queue</span></a></li>
        <?php if($_SESSION['s_region_id']==2){ ?>
          <li <?php if($parent_page=='egm_calibrationDueReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>egm_calibrationDueReport"><i class="fa  fa-compass"></i><span>EGM Calibration Queue</span></a></li>
        <?php } ?>
        <li <?php if($parent_page=='ib_dashboard') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>ib_dashboard"><i class="fa fa-sort-amount-asc"></i><span>Tools Vs Installbase </span></a></li>
        <li <?php if($parent_page=='feOwnedToolsReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>fetools_report"><i class="fa fa-book"></i><span>FE Owned Tools</span></a></li>
        <li <?php if($parent_page=='assets_calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>assets_calibration_report"><i class="fa fa-magic"></i><span>Calibration Report</span></a></li>
    </ul>
</li>
<li><a href="#"><i class="fa fa-dashboard"></i><span>Reports</span></a>
    <ul class="sub-menu">    
        <li <?php if($parent_page=='tools_inventory') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>tools_inventory"><i class="fa fa-gavel"></i><span>Tools Inventory</span></a></li>
        <li <?php if($parent_page=='calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>calibration_report"><i class="fa  fa-compass"></i><span>Calibration</span></a></li>
        <?php if($_SESSION['s_region_id']==2){ ?>
          <li <?php if($parent_page=='egm_calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL.'egm_calibration_report'; ?>"><i class="fa  fa-compass"></i><span>EGM Calibration</span></a></li>
        <?php } ?>
        <li <?php if($parent_page=='toolUtilization') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>toolUtilization"><i class="fa fa-magic"></i><span>Tool Utilization</span></a></li>
        <li <?php if($parent_page=='cancelled_invoice') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>cancelled_invoice"><i class="fa fa-trash-o"></i><span>Cancelled Invoice</span></a></li>
        <li <?php if($parent_page=='calibration_certificates') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>calibration_certificates"><i class="fa fa-envelope nav-icon"></i><span>Calibration Certificates</span></a></li>
        <li <?php if($parent_page=='download_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>download_report"><i class="fa fa-print nav-icon"></i><span>Download Invoices/DC</span></a></li>
        <li <?php if($parent_page=='gps_tracking_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>gps_report_view"><i class="fa fa-arrows nav-icon"></i><span>Tool Tracking Report</span></a></li>
    </ul>
</li>