
<li><a href="#"><i class="fa fa-hand-o-up"></i><span>Raise A New Request</span></a>
  <ul class="sub-menu">
    <?php     
    $page_access = getPageAccess('raise_order',$_SESSION['s_role_id']);
    $page_name = ( ($page_access==1 && count(get_user_countries($_SESSION['sso_id'])) <= 1) || ($page_access==1 && $this->session->userdata('header_country_id')!='') )?"raise_order":"onbehalf_fe";
    ?>
    <li <?php if($parent_page=='raise_order') echo 'class="active"';?>><a href="<?php echo SITE_URL.$page_name; ?>"><i class="fa fa-shopping-cart"></i><span>Tool Order</span></a></li> 
    <li <?php if($parent_page=='raise_pickup') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>raise_pickup"><i class="fa fa-truck"></i><span>Tool Return</span></a></li>
    
  </ul>
</li>
<li><a href="#"><i class="fa fa-folder-open-o"></i><span>Open Requests</span></a>
  <ul class="sub-menu">    
    <li <?php if($parent_page=='open_order') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>open_order"><i class="fa fa-book"></i><span>My Tool Orders</span></a></li>
    <li <?php if($parent_page=='open_fe_return_initiated_list') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>open_fe_return_initiated_list"><i class="fa fa-retweet"></i><span>Pending Pickups</span></a></li>
 </ul>
</li>
<li><a href="#"><i class="fa fa-qrcode"></i><span>Acknowledge Tools</span></a>
  <ul class="sub-menu">
    <li <?php if($parent_page=='receive_order') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>receive_order"><i class="fa fa-bitbucket"></i><span>Ack From WH </span></a></li>
    <li <?php if($parent_page=='fe2_fe_receive') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>fe2_fe_receive"><i class="fa fa-users"></i><span>Ack From FE</span></a></li>
  </ul>
</li>
<li><a href="#"><i class="fa fa-folder-o"></i><span>Closed Requests</span></a>
  <ul class="sub-menu">    
    <li <?php if($parent_page=='closed_order') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_order"><i class="fa fa-shopping-cart"></i><span>Tool Order</span></a></li>
    <li <?php if($parent_page=='closed_receive_order') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_receive_order"><i class="fa fa-bitbucket"></i><span>Acknowledgements </span></a></li>
    <li <?php if($parent_page=='closed_fe_return_initiated_list') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_fe_return_initiated_list"><i class="fa fa-truck"></i><span>Tool Return </span></a></li>
  </ul>
</li>

<li class="side_bar_check <?php if($parent_page=='fe_owned_tools'){ echo "active"; }?>"><a href="<?php echo SITE_URL; ?>fe_owned_tools"><i class="fa fa-trophy"></i><span>My Inventory</span></a></li>

<li><a href="#"><i class="fa fa-bar-chart-o"></i><span>Dashboards</span></a>
  <ul class="sub-menu">    
    <li <?php if($parent_page=='toolsReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>tools_report"><i class="fa fa-gavel"></i><span>Tools Inventory </span></a></li>
    <li <?php if($parent_page=='calibrationDueReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>cal_report"><i class="fa  fa-compass"></i><span>Calibration Queue</span></a></li>
    <?php if($_SESSION['s_region_id']==2){ ?>
      <li <?php if($parent_page=='egm_calibrationDueReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>egm_calibrationDueReport"><i class="fa  fa-compass"></i><span>EGM Calibration Queue</span></a></li>
    <?php } ?>
    <li <?php if($parent_page=='ib_dashboard') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>ib_dashboard"><i class="fa fa-sort-amount-asc"></i><span>Tools Vs Installbase </span></a></li>
    <li <?php if($parent_page=='feOwnedToolsReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>fetools_report"><i class="fa fa-book"></i><span>FE Owned Tools</span></a></li>
    <li <?php if($parent_page=='assets_calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>assets_calibration_report"><i class="fa fa-magic"></i><span>Calibration Report</span></a></li>
   </ul>
</li>
<li><a href="#"><i class="fa fa-dashboard"></i><span>Reports</span></a>
  <ul class="sub-menu">    
    <li <?php if($parent_page=='tools_inventory') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>tools_inventory"><i class="fa fa-gavel"></i><span>Tools Inventory</span></a></li>
    <li <?php if($parent_page=='calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>calibration_report"><i class="fa  fa-compass"></i><span>Calibration</span></a></li>
    <?php if($_SESSION['s_region_id']==2){ ?>
      <li <?php if($parent_page=='egm_calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL.'egm_calibration_report'; ?>"><i class="fa  fa-compass"></i><span>EGM Calibration</span></a></li>
    <?php } ?>
    <li <?php if($parent_page=='toolUtilization') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>toolUtilization"><i class="fa fa-magic"></i><span>Tool Utilization</span></a></li>
    <li <?php if($parent_page=='cancelled_invoice') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>cancelled_invoice"><i class="fa fa-trash-o"></i><span>Cancelled Invoice</span></a></li>
    <li <?php if($parent_page=='calibration_certificates') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>calibration_certificates"><i class="fa fa-envelope nav-icon"></i><span>Calibration Certificates</span></a></li>
    <li <?php if($parent_page=='download_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>download_report"><i class="fa fa-print nav-icon"></i><span>Download Invoices/DC</span></a></li>
     <li <?php if($parent_page=='gps_tracking_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>gps_report_view"><i class="fa fa-arrows nav-icon"></i><span>Tool Tracking Report</span></a></li>
    </ul>
</li>