
<li><a href="#"><i class="fa fa-wrench"></i><span>Manage Masters</span></a>
  <ul class="sub-menu">
    <li <?php if($parent_page=='manage_user') echo 'class="active"';?> ><a href="<?php echo SITE_URL; ?>user"><i class="fa fa-users"></i><span>Users</span></a></li>
    <li <?php if($parent_page=='manage_tool') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>tool"><i class="fa fa-gavel"></i><span>Tools </span></a></li>
    <li <?php if($parent_page=='manage_asset') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>asset"><i class="fa fa-dropbox"></i><span>Assets</span></a></li>
    <li <?php if($parent_page=='install_base') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>install_base"><i class="fa fa-list-alt"></i><span>Install Base </span></a></li>
    <li <?php if($parent_page=='manage_supplier') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>supplier"><i class="fa fa-user"></i><span>Supplier</span></a></li>
    <li <?php if($parent_page=='manage_warehouse') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>branch"><i class="fa fa-truck"></i><span>Warehouse</span></a></li>
    <li <?php if($parent_page=='manage_branch') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>branch_office"><i class="fa  fa-building-o"></i><span>Branch Office</span></a></li>
    <li <?php if($parent_page=='modality') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>modality"><i class="fa  fa-book"></i><span>Modality</span></a></li>
    <li <?php if($parent_page=='manage_equipment_model') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>eq_model"><i class="fa  fa-dashboard (alias)"></i><span>Equipment Model</span></a></li>
    
    <li <?php if($parent_page=='country_master') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>country"><i class="fa fa-plane"></i><span>Country</span></a></li>
    <li <?php if($parent_page=='zone_master') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>zone"><i class="fa fa-truck"></i><span>Zone</span></a></li>
    <li <?php if($parent_page=='state_master') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>state"><i class="fa fa-map-marker"></i><span>State</span></a></li>
    <li <?php if($parent_page=='city_master') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>city"><i class="fa fa-road"></i><span>City</span></a></li>
    <li <?php if($parent_page=='area_master') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>area"><i class="fa fa-thumb-tack"></i><span>Area</span></a></li>
    
    <li <?php if($parent_page=='document_type') echo 'class="active"';?> ><a href="<?php echo SITE_URL; ?>document_type"><i class="fa fa-check nav-icon"></i><span>Document Type</span></a></li>
    
    <li <?php if($parent_page=='designation') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>designation"><i class="fa fa-envelope nav-icon"></i><span>Designation</span></a></li>

    <li <?php if($parent_page=='gps_tracking') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>gps_tracking"><i class="fa fa-arrows nav-icon"></i><span>Tool Trackers</span></a></li>

  </ul>
</li>
<li><a href="#"><i class="fa fa-hand-o-up"></i><span>Raise A New Request</span></a>
  <ul class="sub-menu">
    <?php     
    $page_access = getPageAccess('raise_order',$_SESSION['s_role_id']);
    $page_name = ($page_access==1)?"raise_order":"onbehalf_fe";
    ?>
    <li <?php if($parent_page=='raise_order') echo 'class="active"';?>><a href="<?php echo SITE_URL.$page_name; ?>"><i class="fa fa-shopping-cart"></i><span>Tool Order</span></a></li> 
    <li <?php if($parent_page=='raise_transfer') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>raise_transfer"><i class="fa fa-truck"></i><span>Stock Transfer To WH</span></a></li>
    <li <?php if($parent_page=='calibration') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>calibration"><i class="fa fa-compass"></i><span>Calibration</span></a></li>
    <li <?php if($parent_page=='repair_tool') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>repair_tool"><i class="fa fa-chain-broken"></i><span>Repair</span></a></li>
  </ul>
</li>
<li><a href="#"><i class="fa fa-folder-open-o"></i><span>Open Requests</span></a>
  <ul class="sub-menu">
    
    <li <?php if($parent_page=='open_order') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>open_order"><i class="fa fa-shopping-cart"></i><span>Tool Order</span></a></li>
    <li <?php if($parent_page=='openSTforOrder') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>openSTforOrder"><i class="fa fa-truck"></i><span>Stock Transfer</span></a></li>
    <li <?php if($parent_page=='open_calibration') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>admin_calibration_list"><i class="fa fa-compass"></i><span>Calibration</span></a></li>
     <li <?php if($parent_page=='open_repair_request') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>open_repair_request"><i class="fa fa-chain-broken"></i><span>Repair</span></a></li>
    <li <?php if($parent_page=='qc_calibration') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>qc_calibration"><i class="fa fa-magic"></i><span>QA</span></a></li>
  </ul>
</li>

<li><a href="#"><i class="fa fa-folder-o"></i><span>Closed Requests</span></a>
  <ul class="sub-menu">
    
    <li <?php if($parent_page=='closed_order') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_order"><i class="fa fa-shopping-cart"></i><span>Tool Order</span></a></li>
    <li <?php if($parent_page=='closedSTforOrder') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closedSTforOrder"><i class="fa fa-truck"></i><span>Stock Transfer</span></a></li>
    <li <?php if($parent_page=='closed_calibration') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_calibration"><i class="fa fa-compass"></i><span>Calibration</span></a></li>
    <li <?php if($parent_page=='closed_repair_request') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_repair_request"><i class="fa fa-chain-broken"></i><span>Repair</span></a></li>
   <li <?php if($parent_page=='closed_replacement_tool') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_replacement_tool"><i class="fa fa-refresh"></i><span>Replacement</span></a></li>
   <li <?php if($parent_page=='closed_scrap_list') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_scrap_list"><i class="fa fa-trash-o"></i><span>Scrap</span></a></li>
    <li <?php if($parent_page=='closed_qc_calibration') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>closed_qc_calibration_list"><i class="fa fa-magic"></i><span>QA</span></a></li>
  </ul>
</li>
<li><a href="#"><i class="fa fa-check-square-o"></i><span>Approvals</span></a>
  <ul class="sub-menu">
    <li <?php if($parent_page=='raiseSTforOrder') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>raiseSTforOrder"><i class="fa fa-shopping-cart"></i><span>Stock Transfer</span></a></li>
    <li <?php if($parent_page=='fe2_fe_approval') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>fe2_fe_approval"><i class="fa fa-exchange"></i><span>FE To FE Transfer</span></a></li>
    <li <?php if($parent_page=='exceededOrderDuration') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>exceededOrderDuration"><i class="fa fa-calendar"></i><span>Return Days Extension</span></a></li>
    <li <?php if($parent_page=='address_change') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>address_change"><i class="fa fa-eraser"></i><span>Address Change</span></a></li>
    <li <?php if($parent_page=='missed_asset') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>missed_asset"><i class="fa fa-exclamation"></i><span>Missed Assets</span></a></li>
    <li <?php if($parent_page=='fe_position') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>fe_position_change"><i class="fa fa-user-md"></i><span>FE Position Change</span></a></li>
  </ul>
</li>

<li><a href="#"><i class="fa fa-bar-chart-o"></i><span>Dashboards</span></a>
  <ul class="sub-menu">    
    <li <?php if($parent_page=='toolsReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>tools_report"><i class="fa fa-gavel"></i><span>Tools Inventory </span></a></li>
    <li <?php if($parent_page=='calibrationDueReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>cal_report"><i class="fa  fa-compass"></i><span>Calibration Queue</span></a></li>
    <?php if($_SESSION['s_region_id']==2){ ?>
      <li <?php if($parent_page=='egm_calibrationDueReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>egm_calibrationDueReport"><i class="fa  fa-compass"></i><span>EGM Calibration Queue</span></a></li>
    <?php } ?>
    <li <?php if($parent_page=='ib_dashboard') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>ib_dashboard"><i class="fa fa-sort-amount-asc"></i><span>Tools Vs Installbase </span></a></li>
    <li <?php if($parent_page=='feOwnedToolsReport') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>fetools_report"><i class="fa fa-book"></i><span>FE Owned Tools</span></a></li>
    <li <?php if($parent_page=='assets_calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>assets_calibration_report"><i class="fa fa-magic"></i><span>Calibration Report</span></a></li>

   </ul>
</li>
<li><a href="#"><i class="fa fa-dashboard"></i><span>Reports</span></a>
  <ul class="sub-menu">    
    <li <?php if($parent_page=='tools_inventory') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>tools_inventory"><i class="fa fa-gavel"></i><span>Tools Inventory</span></a></li>
    <li <?php if($parent_page=='calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>calibration_report"><i class="fa  fa-compass"></i><span>Calibration</span></a></li>
    <?php if($_SESSION['s_region_id']==2){ ?>
      <li <?php if($parent_page=='egm_calibration_report') echo 'class="active"';?>><a href="<?php echo SITE_URL.'egm_calibration_report'; ?>"><i class="fa  fa-compass"></i><span>EGM Calibration</span></a></li>
    <?php } ?>
    <li <?php if($parent_page=='toolUtilization') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>toolUtilization"><i class="fa fa-magic"></i><span>Tool Utilization</span></a></li>
    <li <?php if($parent_page=='cancelled_invoice') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>cancelled_invoice"><i class="fa fa-trash-o"></i><span>Cancelled Invoice</span></a></li>
    <li <?php if($parent_page=='calibration_certificates') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>calibration_certificates"><i class="fa fa-envelope nav-icon"></i><span>Calibration Certificates</span></a></li>
    <li <?php if($parent_page=='download_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>download_report"><i class="fa fa-print nav-icon"></i><span>Download Invoices/DC</span></a></li>
    <li <?php if($parent_page=='gps_tracking_report') echo 'class="active"';?>><a href="<?php echo SITE_URL; ?>gps_report_view"><i class="fa fa-arrows nav-icon"></i><span>Tool Tracking Report</span></a></li>
    </ul>
</li>
<li class="side_bar_check <?php if($parent_page=='defective_asset'){ echo "active"; }?>"><a href="<?php echo SITE_URL; ?>defective_asset"><i class="fa fa-cogs"></i><span>Defective Asset</span></a></li>

<li class="side_bar_check <?php if($parent_page=='buffer_to_inventory'){ echo "active"; }?>"><a href="<?php echo SITE_URL; ?>buffer_to_inventory"><i class="fa fa-repeat"></i><span>Buffer to Inventory</span></a></li>

<li class="side_bar_check <?php if($parent_page=='inventory_to_buffer'){ echo "active"; }?>"><a href="<?php echo SITE_URL; ?>inventory_to_buffer"><i class="fa fa-undo"></i><span>Inventory To Buffer </span></a></li>

<li><a href="http://13.126.121.68:8070/geolocation/" target="_blank"><i class="fa fa-map-marker"></i><span>Track Your Tool </span></a></li>