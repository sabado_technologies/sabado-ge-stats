<!-- Modal popup -->
<div class="modal fade colored-header" id="form-primary">
	<div class="modal-dialog" style="width: 90% !important">
		<div class="md-content">
			<div class="modal-header">
	        	<h3 class="header_title">Search Audit Details Here</h3>
	        	<button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">×</button>
	    	</div>
	    	<div class="modal-body form">
	    		<form role="form" class="form-horizontal" id="audit_frm" method="post" action="#">
	    			<input type="hidden" name="current_offset" id="current_offset" value="">
	    			<input type="hidden" name="submit_button" id="submit_button" value="1">
	    			<input type="hidden" name="sent_data" id="sent_data" value="0">

                	<div class="row">
                		<div class="col-sm-12 form-group">
							<div class="col-sm-offset-2 col-sm-3">
								<select class="select3 column_name" name="column_name" style="width:100%">
									<option value="">- Column -</option>
								</select>

							</div>
							<div class="col-sm-3">
								<select class="select4 trans_type" name="trans_type" style="width:100%">
									<option value="">- Transaction Type -</option>
								</select>
							</div>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="sso_user" value="<?php echo @$search_data['sso_user'];?>" class="form-control" placeholder="SSO User">
							</div>
						</div>
                		<div class="col-sm-12 form-group">
							<div class="col-sm-offset-2 col-sm-3">
								<input type="text" autocomplete="off" name="old_value" value="<?php echo @$search_data['old_value'];?>" class="form-control" placeholder="Old Value">
							</div>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="new_value" value="<?php echo @$search_data['new_value'];?>" class="form-control" placeholder="New Value">
							</div>
							
							<div class="col-sm-2">
	                            <button type="button" name="audit_search" id="audit_search" value="1" class="btn btn-success"  data-toggle="tooltip" title="Search"><i class="fa fa-search"></i> </button>
	                            <button type="button" name="reset_search" id="reset_search" value="1" class="btn btn-success"  data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i> </button>
	                        </div>
						</div>
						
                	</div>
                </form>
                <div id="auditResults" align="center"><h4>Search Audit Details Here</h4></div>
                <div id="loaderID" style="position:fixed; top:50%; left:50%; z-index:2; opacity:0"><img src="<?php echo assets_url(); ?>images/ajax-loading-img.gif" /></div>
	    	</div>
	    	<div class="modal-footer">
				<button type="button" class="btn btn-info btn-flat md-close" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="md-overlay"></div>
