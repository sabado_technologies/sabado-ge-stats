<div class="page-head">
	<h3><?php echo $breadCrumbTite;?></h3>
	<ol class="breadcrumb">
    	<?php
		if(isset($breadCrumbOptions))
		{
			$bCount = count($breadCrumbOptions);
			if($bCount>0)
			{
				foreach($breadCrumbOptions as $bCrumb)
				{
					$bClass = $bCrumb['class'];
					$bLable = $bCrumb['label'];
					$bUrl = $bCrumb['url'];
					$side = '';
					if(@$bClass=='')
					{ 
						$side ="side_bar_check"; 
					}
					echo '<li class="'.$bClass.' '.$side.'">';
					if($bUrl!='')
					echo '<a href="'.$bUrl.'">'.$bLable.'</a>';
					else
					echo $bLable;
					echo '</li>';
					
				}
			}
		}
		?>
	</ol>
</div>