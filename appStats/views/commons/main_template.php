<script>
  var SITE_URL = "<?php echo SITE_URL; ?>";
  var ASSET_URL = "<?php echo assets_url();?>";
</script>
<?php 
if(@$_SESSION['sso_id']=='')
{
  echo "<script language='javascript'>";
  echo "window.location.href= SITE_URL";
  echo "</script>";
}

switch_url_based_on_current_page();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon"  href="<?php echo assets_url(); ?>images/favicon_1.png">
  
  <!-- Page Title -->
  <title><?php echo $heading; ?></title>

  <!-- Font Styles -->
  <link rel="stylesheet" type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' >
  <link rel="stylesheet" type="text/css" href='http://fonts.googleapis.com/css?family=Raleway:300,200,100'>
  
  <!-- CSS Files -->
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/jquery.gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>fonts/font-awesome-4/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/jquery.nanoscroller/nanoscroller.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/bootstrap.switch/bootstrap-switch.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/jquery.select2/select2.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/jquery.magnific-popup/dist/magnific-popup.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>js/jquery.niftymodals/css/component.css" />

  <?php
  if(count($css_includes)>0)
  {
    foreach($css_includes as $css_file)
    {
      echo $css_file;
    }
  }
  ?>
  <!-- Style Sheets -->
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/token-inputs/token-input.css" />  
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/token-inputs/token-input-facebook.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/token-inputs/token-input-mac.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/sb-admin.css" /> 
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/quick_buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/jquery-ui.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/style.css" />
</head>
<body>

  <!-- START: Fixed Top navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="fa fa-gear"></span>
        </button>
          <a href="<?php echo SITE_URL;?>" class="top_header_check">
            <img src="<?php echo SITE_URL1;?>assets/images/logo_dashboard.png" style="height:50px; padding-left:8px; padding-right:5px;padding-bottom:5px;padding-top:5px">
          </a>
    </div>
    <div class="container-fluid">
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav" style="padding-left: 25px;">
          <li><a><b><?php echo $this->session->userdata('display_sso_name').' : '.$this->session->userdata('sso_id').' ('.$this->session->userdata('userFullName'); ?>)</b></a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right user-nav">
            <li class="dropdown dropdown-toggle">
              <a href="<?php echo SITE_URL;?>logout" onclick="return confirm('Are you sure you want to LogOut?')"><span>LogOut </span><i class="fa fa-sign-out"></i></a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right user-nav">
    
            <?php
              if(count(@$_SESSION['ChildRolesList'])>1)
              { ?>
                <li class="dropdown top_header_check" style="padding-right: 15px;">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Role : <?php if(@$_SESSION['role_name']!=''){echo $_SESSION['role_name']; } else echo "Select"; ?> <b class="caret"></b></a>
                  <ul class="dropdown-menu" style="right:auto;min-width: 95%">
                  <?php
                    foreach(@$_SESSION['ChildRolesList'] as $role)
                    {?>
                      <li>
                        <a href="<?php echo SITE_URL.'roles/'.storm_encode($role['child_role_id']);?>"><?php echo $role['name']; ?></a>
                      </li><?php 
                    }
                  ?>
                  </ul>
                </li>      
            <?php
              }
              else{ ?>
              <li><a><b>Role : <?php echo $_SESSION['role_name']; ?></b></a></li>
              <?php } ?>
          </ul>
          <ul class="nav navbar-nav navbar-right user-nav">
            <?php 
              if(count(@$_SESSION['countryList'])>1)
              { ?>
                <li class="dropdown top_header_check" style="padding-right: 15px;">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Country : <?php if(@$_SESSION['header_country_name']!='' && @$_SESSION['header_country_id']!=''){echo $_SESSION['header_country_name']; } else echo "All"; ?> <b class="caret"></b></a>
                  <ul class="dropdown-menu" style="right:auto;min-width: 95%">
                    <li>
                        <a href="<?php echo SITE_URL.'countries/'.storm_encode('NA');?>">- All -</a>
                        </li>
                  <?php
                    foreach(@$_SESSION['countryList'] as $cou)
                    { 
                      if($cou['location_id'] != @$_SESSION['header_country_id'])
                      { ?>
                        <li>
                          <a href="<?php echo SITE_URL.'countries/'.storm_encode($cou['location_id']);?>"><?php echo $cou['name']; ?></a>
                        </li><?php 
                      }
                    }
                  ?>
                  </ul>
                </li>      
            <?php
              } 
              else{?>
              <li><a><b>Country : <?php echo $_SESSION['header_country_name']; ?></b></a></li>
              <?php } ?>
          </ul>
          <ul class="nav navbar-nav navbar-right not-nav">
            <li class="button top_header_check" style="margin-top: 3px;" data-container="body" data-placement="bottom"  data-toggle="tooltip" title="Help"><a href="<?php echo SITE_URL.'faq';?>" ><i class="fa fa-question-circle"></i></a></li>       
          </ul>
        </div>
    </div>
  </div>
  <!-- END: Fixed Top navbar -->
  <!-- START: Fixed Sidebar navbar -->
  <div id="cl-wrapper" class="fixed-menu">
    <div class="cl-sidebar">
      <div class="cl-toggle"><i class="fa fa-bars"></i></div>
      <div class="cl-navblock">
        <div class="menu-space">
          <div class="content">
            <input type="hidden" name="dummy" class="parent_page" value="<?php echo @$cur_page; ?>">
            <input type="hidden" name="page_list" class="page_list" value="<?php echo get_all_parent_page_list(); ?>">
            <ul class="cl-vnavigation">
            <li class="side_bar_check <?php if($parent_page=='home'){ echo "active"; }?>"><a href="<?php echo SITE_URL.'home'; ?>"><i class="fa fa-home"></i><span>Home</span></a></li>
              <?php 
                switch(get_logged_user_role())
                { 
                  case 1: 
                  {
                    include('menu/admin.php');
                  }
                  break;

                  case 2:
                  {
                    include('menu/fe.php');
                  }
                  break;

                  case 3: 
                  {
                    include('menu/wh.php');
                  }
                  break;
                  
                  case 4:
                  {
                    include('menu/super_admin.php');
                  }
                  break;

                  case 5:
                  {
                    include('menu/zonal.php');
                  }
                  break;

                  case 6:
                  {
                    include('menu/national.php');
                  }
                  break;
                  case 7:
                  {
                    include('menu/super_visor.php');
                  }
                  break;
                  case 8:
                  {
                    include('menu/qc.php');
                  }
                  break;
                  case 9: 
                  {
                    include('menu/tools_coordinator.php');
                  }
                  break;
                  default:
                  {
                    include('menu/fe.php');
                  }
                }
              ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid" id="pcont">
    <?php $this->load->view('commons/breadCrumb',$nestedView); ?>