<div class="cl-mcont">
	<div class="row">
		<div class="col-md-9">
			<div class="col-md-5">
				<div class="block-flat">
					<div class="header">
						<h4 style="width: 220px;"><i class="fa fa-hand-o-up"></i><b> Raise A New Request</b></h4>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<?php     
								    $page_access = getPageAccess('raise_order',$_SESSION['s_role_id']);
								    $page_name = ( ($page_access==1 && count(get_user_countries($_SESSION['sso_id'])) <= 1) || ($page_access==1 && $this->session->userdata('header_country_id')!='') )?"raise_order":"onbehalf_fe";
								?>
								<a class="quick-button-small metro pink span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Raise Tool Order Request" href="<?php echo SITE_URL.$page_name; ?>">
									<i i class="fa fa-shopping-cart" aria-hidden="true"></i>
									<p>Tool Order</p>
								</a>
							</div>
							<div class="col-sm-6 col-md-6">
								<a class="quick-button-small metro greenLight span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Raise Tool Return Request" href="<?php echo SITE_URL ?>raise_pickup">
									<i class="fa fa-truck"></i>
									<p>Tool Return</p>
								</a>
							</div>
						</div><br>
					</div>
				</div><br>
			</div>
			<div class="col-md-7">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-folder-open-o"></i><b> Open Requests</b></h4>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-4 col-md-4">
								<a class="quick-button-small metro pinkDark span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Open My Tool Order Requests" href="<?php echo SITE_URL ?>open_order">
									<i class="fa fa-book"></i>
									<p>My Orders</p>
								</a>
							</div>
							<div class="col-sm-4 col-md-4">
								<a class="quick-button-small metro green span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Acknowledge Tools From Warehouse" href="<?php echo SITE_URL ?>receive_order">
									<i class="fa fa-bitbucket"></i>
									<p>Ack From WH</p>
								</a>
							</div>
							<div class="col-sm-4 col-md-4">
								<a class="quick-button-small metro orange span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Acknowledge Tools From FE" href="<?php echo SITE_URL ?>fe2_fe_receive">
									<i class="fa fa-users"></i>
									<p>Ack From FE</p>
								</a>
							</div>
						</div><br>
					</div>
				</div><br>
			</div>
			<div class="col-md-5">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-folder-o"></i><b> Closed Requests</b></h4>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<a class="quick-button-small metro grey span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Closed Tool Orders" href="<?php echo SITE_URL ?>closed_order">
									<i i class="fa fa-shopping-cart" aria-hidden="true"></i>
									<p>Tool Order</p>
								</a>
							</div>
							<div class="col-sm-6 col-md-6">
								<a class="quick-button-small metro greenDark  span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Closed FE Return Initiated List" href="<?php echo SITE_URL ?>closed_fe_return_initiated_list">
									<i class="fa fa-truck"></i>
									<p>Tool Return</p>
								</a>
							</div>
						</div><br>
					</div>
				</div><br>
			</div><br>
			<div class="col-md-4">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-trophy"></i><b> My Inventory</b></h4>
					</div>
					<div class="content">
						<div class="row">
								<div class="col-sm-offset-2 col-md-offset-2 col-sm-8 col-md-8">
									<a class="quick-button-small metro red span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="FE Owned Tools" href="<?php echo SITE_URL ?>fe_owned_tools">
										<i class="fa fa-gavel"></i>
										<p>My Inventory</p>
									</a>
								</div>
							</div><br>
					</div>
				</div>
			</div><br>
		</div>
		<div class="col-md-3" style="padding-left: 0px;">
			<div class="block-flat" style="padding:0px 15px;">
				<div class="header row" style="color:#ffffff;padding-left: 20px;background:#ff5f12;padding-top: 0px;padding-bottom: 0px;border-bottom-width: 0px;">
					<h4><i class="fa fa-bell"></i><b> Notifications</b></h4>
				</div>
				<div class="content">
					<div class="list-group">
	                    <div class="table-responsive">
							<table class="table table-bordered hover" id="mytable">
								<?php 
								
									#notification1
									#Need to write Query
									$data1['result'] = array(
									'result'  => ack_from_wh_requests(),
									'name'    => 'Ack from WH',
									'fa_icon' => 'fa fa-bitbucket',
									'url'     => SITE_URL.'receive_order');
									$this->load->view('commons/dashboard/notification',$data1); 

									#notification2
									#Need to write Query
									$data2['result'] = array(
									'result'  => ack_from_fe_requests(),
									'name'    => 'Ack from FE',
									'fa_icon' => 'fa fa-users',
									'url'     => SITE_URL.'fe2_fe_receive');
									$this->load->view('commons/dashboard/notification',$data2); 

									#notification3
									#Need to write Query
									$data3['result'] = array(
									'result'  => tool_to_return_requests(),
									'name'    => 'Tools to be Return',
									'fa_icon' => 'fa fa-truck',
									'url'     => SITE_URL.'raise_pickup');
									$this->load->view('commons/dashboard/notification',$data3); 

									#notification4
									#Need to write Query
									$data4['result'] = array(
									'result'  => return_calibration_tool_requests(),
									'name'    => 'Return Tools for Calibration',
									'fa_icon' => 'fa fa-share-square-o',
									'url'     => SITE_URL.'calibration_required_tools');
									$this->load->view('commons/dashboard/notification',$data4); 

									#notification5
									#Need to write Query
									$data5['result'] = array(
									'result'  => due_in_tool_return_count(),
									'name'    => 'Due in for Tool Return',
									'fa_icon' => 'fa fa-truck',
									'url'     => SITE_URL.'crossed_return_date_orders');
									$this->load->view('commons/dashboard/notification',$data5);

									#notification6
									#Need to write Query
									/*$data6['result'] = array(
									'result'  => fe_transfer_request(),
									'name'    => 'FE Transfer Requests',
									'fa_icon' => 'fa fa-exchange',
									'url'     => SITE_URL.'fe_transfer_request');
									$this->load->view('commons/dashboard/notification',$data6);*/
									
								?>	
							</table>
						</div>
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>