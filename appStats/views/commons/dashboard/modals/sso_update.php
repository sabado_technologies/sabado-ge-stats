<div class="modal  colored-header md-show" id="form-primary" style="display: block; height: 500px; width: 550px;overflow: hidden !important;margin-left: 400px;margin-top: 150px;">
    <div class="md-content">
      <div class="modal-header">
        <h3>Confirm <?php echo $role_name; ?> User</h3>
      </div>
      <form class="form-horizontal" action="<?php echo SITE_URL?>sso_update" method="post" >
  		  <div class="modal-body" style="height: 200px !important;overflow: hidden !important;">
  	      	<div class="row">
  	      		<div class="form-group">
  		        	<label class="col-md-4 control-label"><?php echo $role_name; ?> User <span class="req-fld">*</span></label> 
  		          	<div class="col-md-5">
                    <select name="sso_id" required class="popup_sso" style="width:100%"> 
                    <option value="">- SSO -</option>
                  </select>
  		            </div>
                  <button class="btn btn-primary" type="submit" value="1" name="submitUser"><i class="fa fa-check"></i> Submit</button>
  		        </div>
  	      	</div>
  	    </div>
  	    <div class="modal-footer">
          <a href="<?php echo SITE_URL.'roles/'.storm_encode('special');?>"> <button type="button" class="btn btn-warning btn-flat md-close" data-dismiss="modal"><i class="fa fa-users"></i> Show All <?php echo $role_name."'s"; ?> Data</button></a>
          <a href="<?php echo SITE_URL.'roles/'.storm_encode($this->session->userdata('main_role_id'));?>"> <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button></a>
          	
            
        </div>	      	
      </form>
    </div>
</div>
<div class="md-overlay"></div>