<div class="modal  colored-header md-show" id="form-primary" style="display: block; height: 500px; width: 550px;overflow: hidden !important;margin-left: 400px;margin-top: 150px;">
    <div class="md-content">
      <div class="modal-header">
        <h3>Confirm Location</h3>
      </div>
      <form class="form-horizontal" action="<?php echo SITE_URL?>fe_login_update" method="post" >
  		  <div class="modal-body" style="height: 200px !important;overflow: hidden !important;">
  	      	<div class="row">
  	      		<div class="form-group">
  		        	<label class="col-md-4 control-label"> Current Location <span class="req-fld">*</span></label> 
  		          	<div class="col-md-6">
                    <?php 
                    $fe_position = $this->Common_model->get_value('user',array('sso_id'=>$this->session->userdata('sso_id')),'fe_position');
                    ?>
                    <input type="hidden" name="old_fe_position" value="<?php echo $fe_position; ?>">
  			          	<select name="new_fe_position" required class="select2" > 
        							<option value="">- Location -</option>
        							<?php 
        								foreach($location_list as $row)
        								{
                          $selected = "";
                          if($row['location_id'] == $fe_position){ $selected = 'selected';}
        									echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['city_name'].', '.$row['state_name'].'</option>';
        								}
        							?>
        						</select>
  		            </div>
  		        </div>
  	      	</div>
  	    </div>
  	    <div class="modal-footer">
          	<button class="btn btn-primary" type="submit" value="1" name="submitUser"><i class="fa fa-check"></i> Submit</button>
            <button type="submit" value="close" name="close" class="btn btn-info btn-flat md-close">Cancel</button>
        </div>	      	
      </form>
    </div>
</div>
<div class="md-overlay"></div>