<div class="cl-mcont">
	<div class="row">
		<div class="col-md-9">
			<div class="col-md-6">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-compass"></i><b> Calibration</b></h4>
					</div>
					<div class="content">
						<div class="row">
								
								<div class="col-md-offset-1 col-sm-5 col-md-5">
									<a class="quick-button-small metro grey span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="QA Approvals" href="<?php echo SITE_URL ?>qc_calibration">
										<i class="fa fa-folder-open-o"></i>
										<p>QA Approvals</p>
									</a>
								</div>
								<div class="col-sm-5 col-md-5">
									<a class="quick-button-small metro red span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Closed QA List" href="<?php echo SITE_URL ?>closed_qc_calibration_list">
										<i class="fa fa-folder-o"></i>
										<p>Closed QA List</p>
									</a>
								</div>
							</div><br>
					</div>
				</div>
			</div>			
		</div>	
		<div class="col-md-3" style="padding-left: 0px;">
			<div class="block-flat" style="padding:0px 15px;">
				<div class="header row" style="color:#ffffff;padding-left:20px;background:#ff5f12;">
					<h4><i class="fa fa-bell"></i><b> Notifications</b></h4>
				</div>
				<div class="content">
					<div class="list-group">
	                    <div class="table-responsive">
							<table class="table table-bordered hover" id="mytable">
								<?php 
								
									#Notification1
									$data1['result'] = array(
									'result'  => under_qa_approval(),
									'name'    => 'Under QA Approval',
									'fa_icon' => 'fa fa-magic',
									'url'     => SITE_URL.'qc_calibration');
									$this->load->view('commons/dashboard/notification',$data1); 
								?>	
							</table>
						</div>
	                </div>
				</div>
			</div>
		</div>	
	</div>
</div>