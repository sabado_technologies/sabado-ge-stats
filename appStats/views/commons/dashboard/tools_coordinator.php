<div class="cl-mcont">
	<div class="row">
		<div class="col-md-9">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-calendar"></i><b> Shipment Requests</b></h4>
					</div>
					<div class="content">
						<div class="row">
							<div class=" col-md-offset-1 col-sm-offset-1 col-sm-2 col-md-2">
								<a class="quick-button-small metro pink span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="FE Shipment Requests" href="<?php echo SITE_URL ?>fe_delivery_list">
									<i i class="fa fa-wrench" aria-hidden="true"></i>
									<p>FE</p>
								</a>
							</div>
							<div class="col-sm-2 col-md-2">
								<a class="quick-button-small metro greenLight span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Stock Transfer Shipment Requests" href="<?php echo SITE_URL ?>wh_stock_transfer_list">
									<i class="fa fa-truck"></i>
									<p>Transfer</p>
								</a>
							</div>
							<div class="col-sm-2 col-md-2">
								<a class="quick-button-small metro pinkDark span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Pickup Requests" href="<?php echo SITE_URL ?>pickup_list">
									<i class="fa fa-retweet"></i>
									<p>Pickup</p>
								</a>
							</div>
							<div class="col-sm-2 col-md-2">
								<a class="quick-button-small metro green span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Calibration Shipment Requests" href="<?php echo SITE_URL ?>wh_calibration">
									<i class="fa fa-compass"></i>
									<p>Calibration</p>
								</a>
							</div>
							<div class="col-sm-2 col-md-2">
								<a class="quick-button-small metro orange span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Repair Shipment Requests" href="<?php echo SITE_URL ?>wh_repair">
									<i class="fa fa-chain-broken"></i>
									<p>Repair</p>
								</a>
							</div>
						</div><br>
					</div>
				</div><br>
			</div>
			<div class="col-md-8">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-qrcode"></i><b> Acknowledge Returns</b></h4>
					</div>
					<div class="content">
						<div class="row">
								<div class="col-sm-3 col-md-3">
									<a class="quick-button-small metro greenDark span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="FE Return Tools" href="<?php echo SITE_URL ?>open_fe_returns">
										<i class="fa fa-share-square-o"></i>
										<p>FE</p>
									</a>
								</div>
								<div class="col-sm-3 col-md-3">
									<a class="quick-button-small metro grey span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Stock Transfer Return Tools" href="<?php echo SITE_URL ?>wh2_wh_receive">
										<i class="fa fa-pencil-square-o"></i>
										<p>Transfer</p>
									</a>
								</div>
								<div class="col-sm-3 col-md-3">
									<a class="quick-button-small metro yellow span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Calibration Return Tools" href="<?php echo SITE_URL ?>vendor_calibration">
										<i class="fa fa-external-link"></i>
										<p>Calibration</p>
									</a>
								</div>
								<div class="col-sm-3 col-md-3">
									<a class="quick-button-small metro purple span2"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Repair Return Tools" href="<?php echo SITE_URL ?>vendor_wh_repair_request">
										<i class="fa fa-link"></i>
										<p>Repair</p>
									</a>
								</div>
							</div><br>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-hospital-o"></i><b> Inventory</b></h4>
					</div>
					<div class="content">
						<div class="row">
							<!-- <div class="col-md-6" >
								<a class="quick-button-small metro red span2" href="<?php echo SITE_URL ?>branch" style="padding-right: 20px;">
									<i class="fa fa-truck"></i>
									<p>Availablity</p>
								</a>
							</div> -->
							<div class="col-md-offset-2 col-md-8 col-sm-8">
								<a class="quick-button-small metro blue span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Tools Inventory Report" href="<?php echo SITE_URL ?>tools_inventory">
									<i class="fa fa-gavel"></i>
									<p>Tools Inventory</p>
								</a>
							</div>
						</div><br>
					</div>
				</div><br>
			</div>
		</div>
		<div class="col-md-3" style="padding-left: 0px;">
			<div class="block-flat" style="padding:0px 15px;">
				<div class="header row" style="color:#ffffff;padding-left:20px;background:#ff5f12;">
					<h4><i class="fa fa-bell"></i><b> Notifications</b></h4>
				</div>
				<div class="content">
					<div class="list-group">
						<div class="table-responsive">
							<table class="table table-bordered hover" id="mytable">
								<?php 
								
									#notification1
									$data1['result'] = array(
									'result'  => pending_fe_requests(),
									'name'    => 'Pending FE Requests',
									'fa_icon' => 'fa fa-wrench',
									'url'     => SITE_URL.'fe_delivery_list');
									$this->load->view('commons/dashboard/notification',$data1); 

									#notification2
									$data2['result'] = array(
									'result'  => pending_st_requests(),
									'name'    => 'Pending ST Requests',
									'fa_icon' => 'fa fa-truck',
									'url'     => SITE_URL.'wh_stock_transfer_list');
									$this->load->view('commons/dashboard/notification',$data2); 

									#notification3
									#Need to write Query
									$data3['result'] = array(
									'result'  => pending_pickup_requests(),
									'name'    => 'Pending Pickup Requests',
									'fa_icon' => 'fa fa-retweet',
									'url'     => SITE_URL.'pickup_list');
									$this->load->view('commons/dashboard/notification',$data3); 

									#notification4
									$data4['result'] = array(
									'result'  => pending_calibration_requests(),
									'name'    => 'Pending Calibration Requests',
									'fa_icon' => 'fa fa-compass',
									'url'     => SITE_URL.'wh_calibration');
									$this->load->view('commons/dashboard/notification',$data4); 

									#notification5
									$data5['result'] = array(
									'result'  => pending_repair_requests(),
									'name'    => 'Pending Repair Requests',
									'fa_icon' => 'fa fa-chain-broken',
									'url'     => SITE_URL.'wh_repair');
									$this->load->view('commons/dashboard/notification',$data5);

									#notification6
									$data6['result'] = array(
									'result'  => acknowledge_fe_requests(),
									'name'    => 'Acknowledge FE Tools',
									'fa_icon' => 'fa fa-share-square-o',
									'url'     => SITE_URL.'open_fe_returns');
									$this->load->view('commons/dashboard/notification',$data6); 

									#notification7
									$data7['result'] = array(
									'result'  => acknowledge_wh_requests(),
									'name'    => 'Acknowledge WH Tools',
									'fa_icon' => 'fa fa-pencil-square-o',
									'url'     => SITE_URL.'wh2_wh_receive');
									$this->load->view('commons/dashboard/notification',$data7); 

									#notification8
									$data8['result'] = array(
									'result'  => acknowledge_cal_tools_count(),
									'name'    => 'Acknowledge Cal Tools',
									'fa_icon' => 'fa fa-external-link',
									'url'     => SITE_URL.'vendor_calibration');
									$this->load->view('commons/dashboard/notification',$data8); 

									#notification9
									$data9['result'] = array(
									'result'  => acknowledge_repair_tools_count(),
									'name'    => 'Acknowledge Repair Tools',
									'fa_icon' => 'fa fa-link',
									'url'     => SITE_URL.'vendor_wh_repair_request');
									$this->load->view('commons/dashboard/notification',$data9);

								?>	
							</table>
						</div>
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>