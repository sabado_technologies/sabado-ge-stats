<?php if(@$_SESSION['header_country_id'] == 2) { ?>
	<div class="cl-mcont">
		<div class="row">
			<div class="col-md-9">
				<div class="col-md-8">
					<div class="block-flat">
						<div class="header">
							<h4><i class="fa fa-wrench"></i><b> Manage Masters</b></h4>
						</div>
						<div class="content">
							<div class="row">
								<div class="col-sm-3 col-md-3">
									<?php     
								    $page_access = getPageAccess('raise_order',$_SESSION['s_role_id']);
								    $page_name = ($page_access==1)?"raise_order":"onbehalf_fe";
								    ?>
									<a class="quick-button-small metro greenDark span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Raise Tools Order Request" href="<?php echo SITE_URL.$page_name; ?>">
										<i class="fa fa-shopping-cart"></i>
										<p>Tool Order</p>
									</a>
								</div>
								<div class="col-sm-3 col-md-3">
									<a class="quick-button-small metro grey span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Raise Stock Transfer Request" href="<?php echo SITE_URL ?>raise_transfer">
										<i class="fa fa-truck"></i>
										<p>Transfer</p>
									</a>
								</div>
								<div class="col-sm-3 col-md-3">
									<a class="quick-button-small metro yellow span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage Calibration Requests" href="<?php echo SITE_URL ?>calibration">
										<i class="fa fa-compass"></i>
										<p>Calibration</p>
									</a>
								</div>
								<div class="col-sm-3 col-md-3">
									<a class="quick-button-small metro red span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage Repair Requests" href="<?php echo SITE_URL ?>repair_tool">
										<i class="fa fa-chain-broken"></i>
										<p>Repair</p>
									</a>
								</div>
								
							</div><br>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="block-flat">
						<div class="header">
							<h4><i class="fa fa-cog"></i><b> Manage</b></h4>
						</div>
						<div class="content">
							<div class="row">
								<div class="col-sm-offset-2 col-md-offset-2 col-sm-8 col-md-8">
									<a class="quick-button-small metro grey span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage Defective Assets" href="<?php echo SITE_URL ?>defective_asset">
										<i class="fa  fa-cogs"></i>
										<p>Defective Asset</p>
									</a>
								</div>
							</div><br>
						</div>
					</div><br>
				</div>
				<div class="col-md-6">
					<div class="block-flat">
						<div class="header">
							<h4><i class="fa fa-check-square-o"></i><b> Approvals</b></h4>
						</div>
						<div class="content">
							<div class="row">
								<div class="col-md-4">
									<a class="quick-button-small metro red span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="FE to FE Transfer" href="<?php echo SITE_URL ?>fe2_fe_approval">
										<i class="fa fa-exchange"></i>
										<p>FE to FE</p>
									</a>
								</div>
								<div class="col-md-4">
									<a class="quick-button-small metro blue span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Return Days Extension" href="<?php echo SITE_URL ?>exceededOrderDuration">
										<i class="fa fa-calendar"></i>
										<p>Extension</p>
									</a>
								</div>
								<div class="col-md-4">
									<a class="quick-button-small metro purple span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Change Install Base Address" href="<?php echo SITE_URL ?>address_change">
										<i class="fa fa-eraser"></i>
										<p>Address</p>
									</a>
								</div>
							</div><br>
						</div>
					</div><br>
				</div>
			</div>
			<div class="col-md-3" style="padding-left: 0px;">
				<div class="block-flat" style="padding:0px 15px;">
					<div class="header row" style="color:#ffffff;padding-left: 20px;background:#ff5f12;padding-top: 0px;padding-bottom: 0px;border-bottom-width: 0px;">
						<h4><i class="fa fa-bell"></i><b> Notifications</b></h4>
					</div>
					<div class="content ">
						<div class="list-group scroll_content">
							<table class="table table-bordered" style="margin-bottom: -1 !important;">
								<tr style="background-color: #fcf8e3;">
									<?php
									$result1 = get_upcoming_calibration_requests();
									$res1 = notification_result($result1);
									$result2 = waiting_for_shipment_admin_count();
									$res2 = notification_result($result2);
									$result3 = at_cal_vendor_count();
									$res3 = notification_result($result3);
									$result4 = at_tools_coordinator();
									$res4 = notification_result($result4);
									$result5 = under_qa_approval();
									$res5 = notification_result($result5);
									$result6 = waiting_for_closing();
									$res6 = notification_result($result6);
									$result_cal = $res1[0]+$res2[0]+$res3[0]+$res4[0]+$res5[0]+$res6[0]; ?>
									<td class="show_list fix_top_bottom" style="cursor:pointer;"><i class="fa fa-compass" aria-hidden="true"></i> Calibration Requests <span class="notification_cal_icon"><?php if($result_cal>0){ echo "(".get_notification_count($result_cal).")"; }?></span> <span style="float:right;margin-top: 3px;"><i class="fa fa-caret-down fa-lg"></i></span></td>
								</tr>
							</table>
							<div class="list_item">
								<table class="table table-bordered" style="margin-bottom: -1 !important;">
									<?php 
									
									#notification1
									$data1['result'] = array(
									'result'  => $result1,
									'name'    => 'Upcoming Calibration',
									'fa_icon' => 'fa fa-exclamation-circle',
									'url'     => SITE_URL.'calibration');
									$this->load->view('commons/dashboard/notification',$data1); 


									#Notification2
									$data2['result'] = array(
									'result'  => $result2,
									'name'    => 'Waiting For Shipment',
									'fa_icon' => 'fa fa-compass',
									'url'     => SITE_URL.'calibration_report');
									$this->load->view('commons/dashboard/notification',$data2);


									#Notification3
									/*$data3['result'] = array(
									'result'  => $result3,
									'name'    => 'At Cal Vendor',
									'fa_icon' => 'fa fa-gears (alias)',
									'url'     => SITE_URL.'calibration_report');
									$this->load->view('commons/dashboard/notification',$data3); */

									#Notification4
									$data4['result'] = array(
									'result'  => $result3,
									'name'    => 'Acknowledge Cal Tools',
									'fa_icon' => 'fa fa-qrcode',
									'url'     => SITE_URL.'calibration_report');
									$this->load->view('commons/dashboard/notification',$data4);

									#Notification5
									$data5['result'] = array(
									'result'  => $result4,
									'name'    => 'At Tools Coordinator',
									'fa_icon' => 'fa fa-user',
									'url'     => SITE_URL.'admin_calibration_list');
									$this->load->view('commons/dashboard/notification',$data5); 

									#Notification6
									$data6['result'] = array(
									'result'  => $result5,
									'name'    => 'Under QA Approval',
									'fa_icon' => 'fa fa-magic',
									'url'     => SITE_URL.'qc_calibration');
									$this->load->view('commons/dashboard/notification',$data6); 

									#Notification7
									$data7['result'] = array(
									'result'  => $result6,
									'name'    => 'Waiting For Closing',
									'fa_icon' => 'fa fa-check-square-o',
									'url'     => SITE_URL.'admin_calibration_list');
									$this->load->view('commons/dashboard/notification',$data7); 

									?>	
								</table>
							</div>
							<table class="table table-bordered" style="margin-bottom: -1 !important;">
								<tr style="background-color: #fcf8e3;">
									<?php
									$result01 = get_repair_requests();
									$res01 = notification_result($result01);
									$result02 = waiting_for_repair_shipment_admin_count();
									$res02 = notification_result($result02);
									$result03 = repair_at_cal_vendor_count();
									$res03 = notification_result($result03);
									$result04 = repair_at_tools_coordinator();
									$res04 = notification_result($result04);
									$result_rep = $res01[0]+$res02[0]+$res03[0]+$res04[0]; ?>
									<td class="show_repair_list fix_top_bottom" style="cursor:pointer;"><i class="fa fa-chain-broken" aria-hidden="true"></i> Repair Requests <span class="notification_repair_icon"><?php if($result_rep>0){ echo "(".get_notification_count($result_rep).")"; }?></span> <span style="float:right;margin-top: 3px"><i class="fa fa-caret-down fa-lg"></i></span></td>
								</tr>
							</table>
							<div class="repair_list_item">
								<table class="table table-bordered" style="margin-bottom: -1 !important;">
									<?php 
									
									#notification1
									$data1['result'] = array(
									'result'  => $result01,
									'name'    => 'Repair Requests',
									'fa_icon' => 'fa fa-exclamation-circle',
									'url'     => SITE_URL.'repair_tool');
									$this->load->view('commons/dashboard/notification',$data1); 


									#Notification2
									$data2['result'] = array(
									'result'  => $result02,
									'name'    => 'Waiting For Shipment',
									'fa_icon' => 'fa fa-compass',
									'url'     => SITE_URL.'open_repair_request');
									$this->load->view('commons/dashboard/notification',$data2);


									#Notification3
									/*$data3['result'] = array(
									'result'  => $result03,
									'name'    => 'At Repair Vendor',
									'fa_icon' => 'fa fa-gears (alias)',
									'url'     => SITE_URL.'open_repair_request');
									$this->load->view('commons/dashboard/notification',$data3); */

									#Notification4
									$data4['result'] = array(
									'result'  => $result03,
									'name'    => 'Acknowledge Repair Tools',
									'fa_icon' => 'fa fa-qrcode',
									'url'     => SITE_URL.'open_repair_request');
									$this->load->view('commons/dashboard/notification',$data4);

									#Notification5
									$data5['result'] = array(
									'result'  => $result04,
									'name'    => 'At Tools Coordinator',
									'fa_icon' => 'fa fa-user',
									'url'     => SITE_URL.'open_repair_request');
									$this->load->view('commons/dashboard/notification',$data5); 
									?>	
								</table>
							</div>
							<table class="table table-bordered hover" id="mytable">
								<?php 
								
									#notification1
									# Need to write Query
									$data1['result'] = array(
									'result'  => stock_transfer_count(),
									'name'    => 'Stock Transfer Requests',
									'fa_icon' => 'fa fa-shopping-cart',
									'url'     => SITE_URL.'raiseSTforOrder');
									$this->load->view('commons/dashboard/notification',$data1);
									#notification2
									# Need to write Query
									$data2['result'] = array(
									'result'  => address_change_count(),
									'name'    => 'Address Change Requests',
									'fa_icon' => 'fa fa-check-square-o',
									'url'     => SITE_URL.'address_change');
									$this->load->view('commons/dashboard/notification',$data2); 

									#notification3
									# Need to write Query
									$data3['result'] = array(
									'result'  => due_in_deliveries_count(),
									'name'    => 'Due in For Deliveries',
									'fa_icon' => 'fa fa-exclamation-triangle',
									'url'     => SITE_URL.'crossed_expected_delivery_date');
									$this->load->view('commons/dashboard/notification',$data3); 

									#notification4
									# Need to write Query
									$data4['result'] = array(
									'result'  => fe2_fe_transfer_count(),
									'name'    => 'FE to FE Transfer Requests',
									'fa_icon' => 'fa fa-exchange',
									'url'     => SITE_URL.'fe2_fe_approval');
									$this->load->view('commons/dashboard/notification',$data4); 

									#notification5
									# Need to write Query
									$data5['result'] = array(
									'result'  => due_in_tool_return_count(),
									'name'    => 'Due in For Tool Return',
									'fa_icon' => 'fa fa-truck',
									'url'     => SITE_URL.'crossed_return_date_orders');
									$this->load->view('commons/dashboard/notification',$data5); 

									#notification6
									$data6['result'] = array(
									'result'  => defective_asset_count(),
									'name'    => 'Defective Asset Requests',
									'fa_icon' => 'fa fa-cogs',
									'url'     => SITE_URL.'defective_asset');
									$this->load->view('commons/dashboard/notification',$data6); 

									#notification7
									$data7['result'] = array(
									'result'  => missed_asset_count(),
									'name'    => 'Missed Assets',
									'fa_icon' => 'fa fa-exclamation',
									'url'     => SITE_URL.'missed_asset');
									$this->load->view('commons/dashboard/notification',$data7); 

									#notification8
									# Need to write Query
									$data8['result'] = array(
									'result'  => return_days_count(),
									'name'    => 'Return Days Extension',
									'fa_icon' => 'fa fa-calendar',
									'url'     => SITE_URL.'exceededOrderDuration');
									$this->load->view('commons/dashboard/notification',$data8); 
									

									#notification9
									$data9['result'] = array(
									'result'  => fe_position_change_count(),
									'name'    => 'FE Position Change',
									'fa_icon' => 'fa fa-user-md',
									'url'     => SITE_URL.'fe_position_change');
									$this->load->view('commons/dashboard/notification',$data9);

									#notification 10
									$data10['result'] = array(
									'result'  => pending_fe_requests(),
									'name'    => 'Pending FE Shipments',
									'fa_icon' => 'fa fa-wrench',
									'url'     => SITE_URL.'fe_delivery_admin');
									$this->load->view('commons/dashboard/notification',$data10); 

								?>	
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } else{ ?>
	<div class="cl-mcont">
	<div class="row">
		<div class="col-md-9">
			<div class="col-md-8">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-wrench"></i><b> Manage Masters</b></h4>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-3 col-md-3">
								<a class="quick-button-small metro pink span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage User Master" href="<?php echo SITE_URL ?>user">
									<i class="fa  fa-users"></i>
									<p>User</p>
								</a>
							</div>
							<div class="col-sm-3 col-md-3">
								<a class="quick-button-small metro pinkDark span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage Tools Master" href="<?php echo SITE_URL ?>tool">
									<i class="fa fa-gavel"></i>
									<p>Tools</p>
								</a>
							</div>
							<div class="col-sm-3 col-md-3">
								<a class="quick-button-small metro green span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage Assets Master" href="<?php echo SITE_URL ?>asset">
									<i class="fa fa-dropbox"></i>
									<p>Assets</p>
								</a>
							</div>
							<div class="col-sm-3 col-md-3">
								<a class="quick-button-small metro greenLight span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage Install Base Master" href="<?php echo SITE_URL ?>install_base">
									<i class="fa fa-list-alt"></i>
									<p>Install Base</p>
								</a>
							</div>
							
						</div><br>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-cog"></i><b> Manage</b></h4>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-offset-2 col-md-offset-2 col-sm-8 col-md-8">
								<a class="quick-button-small metro grey span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage Defective Assets" href="<?php echo SITE_URL ?>defective_asset">
									<i class="fa  fa-cogs"></i>
									<p>Defective Asset</p>
								</a>
							</div>
						</div><br>
					</div>
				</div><br>
			</div>
			<div class="col-md-6">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa  fa-hand-o-up"></i><b> Raise A New Request</b></h4>
					</div>
					<div class="content">
						<div class="row">
								<div class="col-sm-4 col-md-4">
									<?php     
								    $page_access = getPageAccess('raise_order',$_SESSION['s_role_id']);
								    $page_name = ($page_access==1)?"raise_order":"onbehalf_fe";
								    ?>
									<a class="quick-button-small metro greenDark span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Raise Tools Order Request" href="<?php echo SITE_URL.$page_name; ?>">
										<i class="fa fa-shopping-cart"></i>
										<p>Tool Order</p>
									</a>
								</div>
								<div class="col-sm-4 col-md-4">
									<a class="quick-button-small metro grey span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Raise Stock Transfer Request" href="<?php echo SITE_URL ?>raise_transfer">
										<i class="fa fa-truck"></i>
										<p>Transfer</p>
									</a>
								</div>
								<div class="col-sm-4 col-md-4">
									<a class="quick-button-small metro yellow span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Manage Calibration Requests" href="<?php echo SITE_URL ?>calibration">
										<i class="fa fa-compass"></i>
										<p>Calibration</p>
									</a>
								</div>
							</div><br>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="block-flat">
					<div class="header">
						<h4><i class="fa fa-check-square-o"></i><b> Approvals</b></h4>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-4">
								<a class="quick-button-small metro red span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="FE to FE Transfer" href="<?php echo SITE_URL ?>fe2_fe_approval">
									<i class="fa fa-exchange"></i>
									<p>FE to FE</p>
								</a>
							</div>
							<div class="col-md-4">
								<a class="quick-button-small metro blue span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Return Days Extension" href="<?php echo SITE_URL ?>exceededOrderDuration">
									<i class="fa fa-calendar"></i>
									<p>Extension</p>
								</a>
							</div>
							<div class="col-md-4">
								<a class="quick-button-small metro purple span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Change Install Base Address" href="<?php echo SITE_URL ?>address_change">
									<i class="fa fa-eraser"></i>
									<p>Address</p>
								</a>
							</div>
						</div><br>
					</div>
				</div><br>
			</div>
		</div>
		<div class="col-md-3" style="padding-left: 0px;">
			<div class="block-flat" style="padding:0px 15px;">
				<div class="header row" style="color:#ffffff;padding-left: 20px;background:#ff5f12;padding-top: 0px;padding-bottom: 0px;border-bottom-width: 0px;">
					<h4><i class="fa fa-bell"></i><b> Notifications</b></h4>
				</div>
				<div class="content ">
					<div class="list-group scroll_content">
						<table class="table table-bordered" style="margin-bottom: -1 !important;">
							<tr style="background-color: #fcf8e3;">
								<?php
								$result1 = get_upcoming_calibration_requests();
								$res1 = notification_result($result1);
								$result2 = waiting_for_shipment_admin_count();
								$res2 = notification_result($result2);
								$result3 = at_cal_vendor_count();
								$res3 = notification_result($result3);
								$result4 = at_tools_coordinator();
								$res4 = notification_result($result4);
								$result5 = under_qa_approval();
								$res5 = notification_result($result5);
								$result6 = waiting_for_closing();
								$res6 = notification_result($result6);
								$result_cal = $res1[0]+$res2[0]+$res3[0]+$res3[0]+$res4[0]+$res5[0]+$res6[0]; ?>
								<td class="show_list fix_top_bottom" style="cursor:pointer;"><i class="fa fa-compass" aria-hidden="true"></i> Calibration Requests <span class="notification_cal_icon"><?php if($result_cal>0){ echo "(".get_notification_count($result_cal).")"; }?></span> <span style="float:right;margin-top: 3px;"><i class="fa fa-caret-down fa-lg"></i></span></td>
							</tr>
						</table>
						<div class="list_item">
							<table class="table table-bordered" style="margin-bottom: -1 !important;">
								<?php 
								
								#notification1
								$data1['result'] = array(
								'result'  => $result1,
								'name'    => 'Upcoming Calibration',
								'fa_icon' => 'fa fa-exclamation-circle',
								'url'     => SITE_URL.'calibration');
								$this->load->view('commons/dashboard/notification',$data1); 


								#Notification2
								$data2['result'] = array(
								'result'  => $result2,
								'name'    => 'Waiting For Shipment',
								'fa_icon' => 'fa fa-compass',
								'url'     => SITE_URL.'calibration_report');
								$this->load->view('commons/dashboard/notification',$data2);


								#Notification3
								$data3['result'] = array(
								'result'  => $result3,
								'name'    => 'At Cal Vendor',
								'fa_icon' => 'fa fa-gears (alias)',
								'url'     => SITE_URL.'calibration_report');
								$this->load->view('commons/dashboard/notification',$data3); 

								#Notification4
								$data4['result'] = array(
								'result'  => $result3,
								'name'    => 'Acknowledge Cal Tools',
								'fa_icon' => 'fa fa-qrcode',
								'url'     => SITE_URL.'calibration_report');
								$this->load->view('commons/dashboard/notification',$data4);

								#Notification5
								$data5['result'] = array(
								'result'  => $result4,
								'name'    => 'At Tools Coordinator',
								'fa_icon' => 'fa fa-user',
								'url'     => SITE_URL.'admin_calibration_list');
								$this->load->view('commons/dashboard/notification',$data5); 

								#Notification6
								$data6['result'] = array(
								'result'  => $result5,
								'name'    => 'Under QA Approval',
								'fa_icon' => 'fa fa-magic',
								'url'     => SITE_URL.'qc_calibration');
								$this->load->view('commons/dashboard/notification',$data6); 

								#Notification7
								$data7['result'] = array(
								'result'  => $result6,
								'name'    => 'Waiting For Closing',
								'fa_icon' => 'fa fa-check-square-o',
								'url'     => SITE_URL.'admin_calibration_list');
								$this->load->view('commons/dashboard/notification',$data7); 

								?>	
							</table>
						</div>
						<table class="table table-bordered" style="margin-bottom: -1 !important;">
							<tr style="background-color: #fcf8e3;">
								<?php
								$result01 = get_repair_requests();
								$res01 = notification_result($result01);
								$result02 = waiting_for_repair_shipment_admin_count();
								$res02 = notification_result($result02);
								$result03 = repair_at_cal_vendor_count();
								$res03 = notification_result($result03);
								$result04 = repair_at_tools_coordinator();
								$res04 = notification_result($result04);
								$result_rep = $res01[0]+$res02[0]+$res03[0]+$res04[0]; ?>
								<td class="show_repair_list fix_top_bottom" style="cursor:pointer;"><i class="fa fa-chain-broken" aria-hidden="true"></i> Repair Requests <span class="notification_repair_icon"><?php if($result_rep>0){ echo "(".get_notification_count($result_rep).")"; }?></span> <span style="float:right;margin-top: 3px"><i class="fa fa-caret-down fa-lg"></i></span></td>
							</tr>
						</table>
						<div class="repair_list_item">
							<table class="table table-bordered" style="margin-bottom: -1 !important;">
								<?php 
								
								#notification1
								$data1['result'] = array(
								'result'  => $result01,
								'name'    => 'Repair Requests',
								'fa_icon' => 'fa fa-exclamation-circle',
								'url'     => SITE_URL.'repair_tool');
								$this->load->view('commons/dashboard/notification',$data1); 


								#Notification2
								$data2['result'] = array(
								'result'  => $result02,
								'name'    => 'Waiting For Shipment',
								'fa_icon' => 'fa fa-compass',
								'url'     => SITE_URL.'open_repair_request');
								$this->load->view('commons/dashboard/notification',$data2);


								#Notification3
								$data3['result'] = array(
								'result'  => $result03,
								'name'    => 'At Repair Vendor',
								'fa_icon' => 'fa fa-gears (alias)',
								'url'     => SITE_URL.'open_repair_request');
								$this->load->view('commons/dashboard/notification',$data3); 

								#Notification4
								$data4['result'] = array(
								'result'  => $result03,
								'name'    => 'Acknowledge Repair Tools',
								'fa_icon' => 'fa fa-qrcode',
								'url'     => SITE_URL.'open_repair_request');
								$this->load->view('commons/dashboard/notification',$data4);

								#Notification5
								$data5['result'] = array(
								'result'  => $result04,
								'name'    => 'At Tools Coordinator',
								'fa_icon' => 'fa fa-user',
								'url'     => SITE_URL.'open_repair_request');
								$this->load->view('commons/dashboard/notification',$data5); 
								?>	
							</table>
						</div>
						<table class="table table-bordered hover" id="mytable">
							<?php 
							
								#notification1
								# Need to write Query
								$data1['result'] = array(
								'result'  => stock_transfer_count(),
								'name'    => 'Stock Transfer Requests',
								'fa_icon' => 'fa fa-shopping-cart',
								'url'     => SITE_URL.'raiseSTforOrder');
								$this->load->view('commons/dashboard/notification',$data1);
								#notification2
								# Need to write Query
								$data2['result'] = array(
								'result'  => address_change_count(),
								'name'    => 'Address Change Requests',
								'fa_icon' => 'fa fa-check-square-o',
								'url'     => SITE_URL.'address_change');
								$this->load->view('commons/dashboard/notification',$data2); 

								#notification3
								# Need to write Query
								$data3['result'] = array(
								'result'  => due_in_deliveries_count(),
								'name'    => 'Due in For Deliveries',
								'fa_icon' => 'fa fa-exclamation-triangle',
								'url'     => SITE_URL.'crossed_expected_delivery_date');
								$this->load->view('commons/dashboard/notification',$data3); 

								#notification4
								# Need to write Query
								$data4['result'] = array(
								'result'  => fe2_fe_transfer_count(),
								'name'    => 'FE to FE Transfer Requests',
								'fa_icon' => 'fa fa-exchange',
								'url'     => SITE_URL.'fe2_fe_approval');
								$this->load->view('commons/dashboard/notification',$data4); 

								#notification5
								# Need to write Query
								$data5['result'] = array(
								'result'  => due_in_tool_return_count(),
								'name'    => 'Due in For Tool Return',
								'fa_icon' => 'fa fa-truck',
								'url'     => SITE_URL.'crossed_return_date_orders');
								$this->load->view('commons/dashboard/notification',$data5); 

								#notification6
								$data6['result'] = array(
								'result'  => defective_asset_count(),
								'name'    => 'Defective Asset Requests',
								'fa_icon' => 'fa fa-cogs',
								'url'     => SITE_URL.'defective_asset');
								$this->load->view('commons/dashboard/notification',$data6); 

								#notification7
								$data7['result'] = array(
								'result'  => missed_asset_count(),
								'name'    => 'Missed Assets',
								'fa_icon' => 'fa fa-exclamation',
								'url'     => SITE_URL.'missed_asset');
								$this->load->view('commons/dashboard/notification',$data7); 

								#notification8
								# Need to write Query
								$data8['result'] = array(
								'result'  => return_days_count(),
								'name'    => 'Return Days Extension',
								'fa_icon' => 'fa fa-calendar',
								'url'     => SITE_URL.'exceededOrderDuration');
								$this->load->view('commons/dashboard/notification',$data8); 
								

								#notification9
								$data9['result'] = array(
								'result'  => fe_position_change_count(),
								'name'    => 'FE Position Change',
								'fa_icon' => 'fa fa-user-md',
								'url'     => SITE_URL.'fe_position_change');
								$this->load->view('commons/dashboard/notification',$data9);

								#notification 10
								$data10['result'] = array(
								'result'  => pending_fe_requests(),
								'name'    => 'Pending FE Shipments',
								'fa_icon' => 'fa fa-wrench',
								'url'     => SITE_URL.'fe_delivery_admin');
								$this->load->view('commons/dashboard/notification',$data10); 

							?>	
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php } ?>
