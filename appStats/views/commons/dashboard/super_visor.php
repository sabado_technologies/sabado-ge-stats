<div class="cl-mcont">
	<div class="row">
		<div class="col-md-8">
			<div class="block-flat">
				<div class="header">
					<h4><i class="fa fa-bar-chart-o"></i><b> Dashboard</b></h4>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-sm-3 col-md-3">
							<a class="quick-button-small metro pink span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Tools Inventory Dashboard" href="<?php echo SITE_URL ?>tools_report">
								<i class="fa fa-gavel"></i>
								<p>Tools Inventory</p>
							</a>
						</div>
						<div class="col-sm-3 col-md-3">
							<a class="quick-button-small metro orange span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Calibration Queue Dashboard" href="<?php echo SITE_URL ?>cal_report">
								<i class="fa  fa-compass"></i>
								<p>Calibration Queue</p>
							</a>
						</div>
						<div class="col-sm-3 col-md-3">
							<a class="quick-button-small metro green span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Tools Vs Installbase" href="<?php echo SITE_URL ?>ib_dashboard">
								<i class="fa fa-sort-amount-asc"></i>
								<p>Tools Vs IB</p>
							</a>
						</div>
						<div class="col-sm-3 col-md-3">
							<a class="quick-button-small metro greenLight span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="FE Owned Tools" href="<?php echo SITE_URL ?>fetools_report">
								<i class="fa fa-book"></i>
								<p>Fe Owned Tools</p>
							</a>
						</div>
						
					</div><br>
				</div>
			</div>
		</div>
		<div class="col-md-8"><br>
			<div class="block-flat">
				<div class="header">
					<h4><i class="fa fa-dashboard"></i><b> Reports</b></h4>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-sm-3 col-md-3">
							<a class="quick-button-small metro pinkDark span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Tools Inventory Report" href="<?php echo SITE_URL ?>tools_inventory">
								<i class="fa fa-gavel"></i>
								<p>Tools Inventory</p>
							</a>
						</div>
						<div class="col-sm-3 col-md-3">
							<a class="quick-button-small metro greenDark span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Calibration Report" href="<?php echo SITE_URL ?>calibration_report">
								<i class="fa  fa-compass"></i>
								<p>Calibration</p>
							</a>
						</div>
						<div class="col-sm-3 col-md-3">
							<a class="quick-button-small metro grey span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Tool Utilization Report" href="<?php echo SITE_URL ?>toolUtilization">
								<i class="fa fa-magic"></i>
								<p>Tool Utilization</p>
							</a>
						</div>
						<div class="col-sm-3 col-md-3">
							<a class="quick-button-small metro blue span2" data-container="body" data-placement="top"  data-toggle="tooltip" title="Cancelled Invoice Report" href="<?php echo SITE_URL ?>cancelled_invoice">
								<i class="fa fa-trash-o"></i>
								<p>Cancelled Invoice</p>
							</a>
						</div>
						
					</div><br>
				</div>
			</div>
		</div>
	</div>
</div>