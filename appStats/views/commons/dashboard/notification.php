<?php 
$notification_arr = notification_result($result['result']);
$url = $result['url'];
$fa_icon = $result['fa_icon'];
$name = $result['name'];
?>
<tr style="background-color: #fcf8e3;">
	<td class="<?php if(count($notification_arr[1])>1){ echo 'notification_toggle-details';}?> fix_top_bottom">
		<a href="<?php if(count($notification_arr[1])<=1){ echo $url;}else echo "#";?>" style="color:#000000 !important;"><i class="<?php echo @$fa_icon; ?>" aria-hidden="true"></i><?php echo @$name; ?> <span class="badge badge-info blink_me" style="float:right;margin-top: 1px;"><?php if($notification_arr[0]>0){ echo $notification_arr[0]; } ?></span></a></td>
</tr>
<?php if(count($notification_arr[1])>1){ ?>
<tr style="background-color: #fcf8e3;" class="notification_details">
	<td>
		<table class="table">
			<?php 
			foreach ($notification_arr[1] as $key => $value) 
			{ ?>
			<tr>
				<td width="2%"><a href="<?php echo $url; ?>" style="color:#000000 !important;"><?php echo @$value['display_name']; ?>
				<span class="badge badge-info" style="float:right;margin-top: 1px;"> <?php echo @$value['count']; ?></span></a>
				</td>
			</tr><?php 
			} ?>
		</table>
	</td>
</tr>
<?php } ?>