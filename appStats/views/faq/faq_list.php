<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-md-6">
										<div class="panel-group accordion accordion-semi well" id="accordion3">
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a data-toggle="collapse" data-parent="#accordion3" href="#ac3-1" class="collapsed">
															<i class="fa fa-angle-right"></i>1. What is STATS?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-1" class="panel-collapse collapse" >
												  <div class="panel-body">
													<strong>Service Tools Asset Tracking System.</strong>
												  </div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-2"> 
															<i class="fa fa-angle-right"></i>2.	How to place tool order?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-2" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>You can search through Tool # or Tool Description and select the Tool # which you needed to place order.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-3"> 
															<i class="fa fa-angle-right"></i>3.	Can I place multiple Tool’s in one order?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-3" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, multiple tools can be placed in one order.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-4"> 
															<i class="fa fa-angle-right"></i>4.	Can I see the status of my ordered tools?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-4" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, you can view the tool ordered status.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-5"> 
															<i class="fa fa-angle-right"></i>5.	Can I still place order my there is no tools available in Stock?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-5" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, you can still go ahead place order for required tools.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-6"> 
															<i class="fa fa-angle-right"></i>6.	Can I transfer my tool order to other FE?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-6" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, you can transfer your tool order to other only if requested FE provides his/her order #.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-7"> 
															<i class="fa fa-angle-right"></i>7.	How do I place order incase my mob/we app is down/if I am at &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;remote location with no connectivity?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-7" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>You can ask other admin team to place order on behalf of you.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-8"> 
															<i class="fa fa-angle-right"></i>8.	How to acknowledge the tools once I receive?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-8" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>You can acknowledge the tool by Scanning the QR code through your mobile app.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-9"> 
															<i class="fa fa-angle-right"></i>9.	How to acknowledge if scan is not working, do I have any &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alternative?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-9" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, you can still I alternative to acknowledge by entering the asset Id directly into tool using web/mobile version app.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-10"> 
															<i class="fa fa-angle-right"></i>10. Do I have a return process to initiate?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-10" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, you can initiate the return process to return your tool back to WH.</strong>
													</div>
												</div>
						          			</div>
						          		</div>
									</div>
									<div class="col-md-6">
										<div class="panel-group accordion accordion-semi well" id="accordion3">
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a data-toggle="collapse" data-parent="#accordion3" href="#ac3-11" class="collapsed">
															<i class="fa fa-angle-right"></i>11.	Will I get an Alert/Notification to return a tool?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-11" class="panel-collapse collapse" >
												  <div class="panel-body">
													<strong>Yes, you will get an Alert/Notification through STATS tool.</strong>
												  </div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-12"> 
															<i class="fa fa-angle-right"></i>12.	Will I get an Alert/Notification on calibration tools?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-12" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, you will get an Alert/Notification through STATS tool for calibrating your tool prior 45, 30 and < 15 days.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-13"> 
															<i class="fa fa-angle-right"></i>13. Can I view my owned tools?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-13" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, you can view your owned tools in My Inventory.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-14"> 
															<i class="fa fa-angle-right"></i>14. Can I view entire tools inventory in India?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-14" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>No, you can view only the tools available at your respective Branch/WH location.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-15"> 
															<i class="fa fa-angle-right"></i>15. How to place order for tools which is available within my &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;respective Zone.
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-15" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>You cannot place order outside your location, but you can ask/take help of your ZTSM to place order for your respective location.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-16"> 
															<i class="fa fa-angle-right"></i>16. Can I initiate return process for Repair/defective tools?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-16" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, you can initiate return process for repair/defective tools.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-17"> 
															<i class="fa fa-angle-right"></i>17. Can I change from current location to other location?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-17" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, but only through admin approval.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-18"> 
															<i class="fa fa-angle-right"></i>18. How do I add any tool which is not added in tools inventory?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-18" class="panel-collapse collapse">
						          					<?php 
						          					$country_id = $this->session->userdata('s_country_id');
						          					$country_name = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
						          					if($country_name == 'Malaysia')
						          					{
						          						$mail = "ASEAN.STATS@ge.com";
						          					}
						          					else
						          					{
						          						$mail = "@HEALTH India Service Tools Team";
						          					}
						          					?>
													<div class="panel-body">
														<strong>Please write an email ( <?php echo @$mail; ?> ) to add the tool.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-19"> 
															<i class="fa fa-angle-right"></i>19. Is the GSTIN number and declaration letter mandate for the &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tools shipping to direct customer site?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-19" class="panel-collapse collapse">
													<div class="panel-body">
														<strong>Yes, GSTIN and Declaration letter is mandate to direct customer shipment.</strong>
													</div>
												</div>
						          			</div>
						          			<div class="panel panel-default">
						          				<div class="panel-heading">
						          					<h4 class="panel-title">
						          						<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-20"> 
															<i class="fa fa-angle-right"></i>20. How do ask for help which is not listed in this FAQ’s?
														</a>
						          					</h4>
						          				</div>
						          				<div id="ac3-20" class="panel-collapse collapse">
						          					
													<div class="panel-body">
														<strong>You can reach out Central Tools Team ( <?php echo @$mail; ?> ) for any support/queries.</strong>
													</div>
												</div>
						          			</div>
						          		</div>
									</div>
								</div>
								<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>raise_faq"  parsley-validate novalidate method="post">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label for="inputName" class="col-sm-4 control-label">New Question ? <span class="req-fld">*</span></label>
													<div class="col-sm-5">
														<textarea class="form-control" name="faq" required></textarea>
													</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-offset-5 col-sm-5">
												<button class="btn btn-primary" onclick="return confirm('Are you sure you want to Raise a Question?')" type="submit" value="1" name="submitUser"><i class="fa fa-check"></i> Submit</button>
												<a class="btn btn-danger" href="<?php echo SITE_URL;?>"><i class="fa fa-times"></i> Back</a>
											</div>
										</div>
									</div>
								</form>
								<div class="row">
									<div class="col-md-6">
										<label class="col-sm-4"></label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<label class="col-sm-4"></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>