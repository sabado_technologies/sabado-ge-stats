<?php
$this->load->view('commons/main_template',$nestedView); 
echo $this->session->flashdata('response');  

switch(get_logged_user_role())
{

    case 1:
        $this->load->view('commons/dashboard/admin',$nestedView); 
    break;

    case 2:
        $this->load->view('commons/dashboard/fe',$nestedView); 
    break;

    case 3:
        $this->load->view('commons/dashboard/wh',$nestedView); 
    break;

    case 4:
        $this->load->view('commons/dashboard/super_admin',$nestedView); 
    break;

    case 5:
        $this->load->view('commons/dashboard/zonal',$nestedView); 
    break;

    case 6:
        $this->load->view('commons/dashboard/national',$nestedView); 
    break;

    case 7:
        $this->load->view('commons/dashboard/super_visor',$nestedView); 
    break;
    case 8:
        $this->load->view('commons/dashboard/qc',$nestedView); 
    break;
    case 9:
        $this->load->view('commons/dashboard/tools_coordinator',$nestedView); 
    break;
      
}
$main_role_id = $this->session->userdata('main_role_id');
$l_modal = $this->session->userdata('l_modal');
#onbehalf of
if($main_role_id ==1 || $main_role_id ==4)
{
    $s_role_id = $this->session->userdata('role_id');

    if($s_role_id!=$main_role_id)
    {
        $sl_modal = $this->session->userdata('sl_modal');
        if($sl_modal==1)
        {
            $data['role_name'] = $this->Common_model->get_value('role',array('role_id'=>$s_role_id),'name');
            $this->load->view('commons/dashboard/modals/sso_update',$data);
        }
    }
}

#fe position update
if($l_modal ==1)
{
    if($main_role_id==2 || $main_role_id==5 || $main_role_id==6)
    {
        $this->load->view('commons/dashboard/modals/location_update');
    }
}

$this->load->view('commons/main_footer',$nestedView);
?>