<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form role="form" method="post" action="<?php echo SITE_URL; ?>location_add" parsley-validate novalidate class="form-horizontal">	

							<input type="hidden" name="location_id" id="location_id" value="<?php echo @$country_edit['location_id'];?>">
	                        <input type="hidden" name="level_id" value="<?php echo @$level_id; ?>">
	                        <input type="hidden" name="parent_id" value="<?php echo @$parent_id; ?>">
	                        <input type="hidden" name="parent_page" value="<?php echo @$parent_page; ?>">
							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Country <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<input type="text" autocomplete="off" required="" class="form-control" placeholder="Country Name" name="name" id="name" value="<?php echo @$country_edit['name'];?>" <?php if($flg!=1 && @$country_edit['name']==get_asean_country_name()){ echo "readonly";} ?>>
									</div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Short Name <span class="req-fld">*</span></label>
									<div class="col-sm-6">
	                                    <input class="form-control" id="short_name" required name="short_name" value="<?php echo @$country_edit['short_name'];?>" placeholder="Country Short Name"  type="text" autocomplete="off">
									</div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Currency <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<input type="text" autocomplete="off" required class="form-control" placeholder="Currency Name" name="currency_name" value="<?php echo @$currency_name;?>">
									</div>
							</div>
							<!-- <div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Cal Trigger Time <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<select class="select2" name="cal_trigger_days" required>
											<option value="">-Cal Trigger Time -</option>
											<option value="45" <?php if($flg == 2 && $country_edit['cal_trigger_days'] == 45){ echo "selected";} ?>>45 Days</option>
											<option value="90" <?php if($flg == 2 && $country_edit['cal_trigger_days'] == 90){ echo "selected";} ?>>90 Days</option>
										</select>
									</div>
							</div> -->
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-5">
									<button class="btn btn-primary" type="submit" value="1" name="submit_location"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>country"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>country">
						<div class="col-sm-12">
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="country_name" value="<?php echo @$search_data['country_name'];?>" class="form-control" placeholder="Country">
							</div>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="currency_name" value="<?php echo @$search_data['currency_name'];?>" class="form-control" placeholder="Currency">
							</div>
							<div class="col-sm-3">
								<button type="submit" name="searchcountry" data-toggle="tooltip" title="Search" value="1" class="btn btn-success submit_country"><i class="fa fa-search"></i></button>
								<a href="<?php echo SITE_URL.'country';?>" class="btn btn-success" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
								<a href="<?php echo SITE_URL.'add_country';?>" class="btn btn-success" data-toggle="tooltip" title="Add New"><i class="fa fa-plus"></i></a>
								<button type="submit" data-toggle="tooltip" title="Download" name="download_country" value="1" formaction="<?php echo SITE_URL.'download_country';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i></button>  
							</div>
						</div>
					</form>
				</div><br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Country Name</strong></th>
                                <th class="text-center"><strong>Short Name</strong></th>
                                <th class="text-center"><strong>Currency</strong></th>
                                <!-- <th class="text-center"><strong>Cal Trigger Time</strong></th> -->
                                <th class="text-center"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($country_results)>0)
							{
								foreach($country_results as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo $row['name']; ?></td>
										<td class="text-center"><?php echo $row['short_name']; ?></td>
										<td class="text-center"><?php echo $row['currency_name']; ?></td>
										<!-- <td class="text-center"><?php echo $row['cal_trigger_days'].' Days'; ?></td> -->
										<td class="text-center">
										<?php if($row['status']==1){?>
										<a class="btn btn-default" data-toggle="tooltip" title="Edit" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_country/'.storm_encode($row['location_id']);?>"><i class="fa fa-pencil"></i></a>
										<?php } else{?>
										<a class="btn btn-default" data-toggle="tooltip" style="padding:3px 3px;" href="#" readonly title="Activate Branch to edit"><i class="fa fa-pencil"></i></a>
										<?php }
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" data-toggle="tooltip" title="Deactivate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_country/'.storm_encode($row['location_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" data-toggle="tooltip" title="Activate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_country/'.storm_encode($row['location_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	check_country();
</script>