<?php $this->load->view('commons/main_template',$nestedView); 
?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form id="location_form" method="post" action="<?php echo SITE_URL; ?>location_add" parsley-validate novalidate class="form-horizontal">
	                        <input type="hidden" name="location_id" id="location_id" value="<?php echo @$area_edit[0]['location_id']; ?>">
	                        <input type="hidden" name="level_id" value="<?php echo @$level_id; ?>">
	                        <input type="hidden" name="c_id"  id="c_id" value="<?php echo @$country_id; ?>">
	                        <input type="hidden" name="parent_page" value="<?php echo @$parent_page; ?>">
	                        <div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Country <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<p style="padding-top: 10px;"><?php
										$country_name = $this->Common_model->get_value('location',array('location_id'=>$country_id),'name');
										echo $country_name; ?></p>
									</div>
							</div>
	                        <div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">City Name <span class="req-fld">*</span></label>
								<div class="col-sm-6">
									<select class="select2" required name="parent_id" id="parent_id">
										<option value="">- Select City -</option>
										<?php
										foreach ($cityList as $row) 
										{
									       $selected = (@$row['location_id']==@$area_edit[0]['parent_id'])?'selected="selected"':'';
                                           echo '<option value="'.@$row['location_id'].'"  '.$selected.'>'.@$row['name'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
	                       	<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Area Name <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<input class="form-control" id="name" required name="name" value="<?php echo @$area_edit[0]['name'];?>" placeholder="Area"  type="text" autocomplete="off">
									</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-5">
									<button class="btn btn-primary submit_area" type="submit" value="1" name="submit_location"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>area"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>area">
					<div class="row">
						<div class="col-sm-12 form-group">
						    <div class="col-sm-3">
								<select class="select2" name="zone_id" >
									<option value="">- Select Zone -</option>
									 <?php
									foreach($zoneList as $zone)
									{
										$selected = ($zone['location_id']==@$search_data['zone_id'])?'selected="selected"':'';
										echo '<option value="'.$zone['location_id'].'" '.$selected.'>'.$zone['name'].'</option>';
									}
									?>
								</select>
							</div>
							<div class="col-sm-3">
								<select class="select2" name="state_id" >
									<option value="">- Select State -</option>
									 <?php
									foreach($stateList as $state)
									{
										$selected = ($state['location_id']==@$search_data['state_id'])?'selected="selected"':'';
										echo '<option value="'.$state['location_id'].'" '.$selected.'>'.$state['name'].'</option>';
									}
									?>
								</select>
							</div>
							<div class="col-sm-3">
								<select class="select2" name="city_id" >
									<option value="">- Select City -</option>
									 <?php
									foreach($cityList as $city)
									{
										$selected = ($city['location_id']==@$search_data['city_id'])?'selected="selected"':'';
										echo '<option value="'.$city['location_id'].'" '.$selected.'>'.$city['name'].'</option>';
									}
									?>
								</select>
							</div>
							<div class="col-sm-3">
								<button type="submit" name="search_area" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
								<a href="<?php echo SITE_URL.'area';?>"  class="btn btn-success" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
								<a href="<?php echo SITE_URL.'add_area';?>"  class="btn btn-success" data-toggle="tooltip" title="Add New"><i class="fa fa-plus"></i></a>
								<button type="submit" data-toggle="tooltip" title="Download" name="download_area" value="1" formaction="<?php echo SITE_URL.'download_area';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i></button>  
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 form-group">
							<?php if($task_access == 3 && $this->session->userdata('header_country_id')==''){?>
							<div class="col-sm-3">
								<select class="select2" name="country_id" >
									<option value="">- Country -</option>
									 <?php
									foreach($countryList as $country)
									{
										$selected = ($country['location_id']==@$search_data['country_id'])?'selected="selected"':'';
										echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
									}
									?>
								</select>
							</div>
							<?php } ?>
							
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="area_name" value="<?php echo @$search_data['area_name'];?>" placeholder="Area Name" class="form-control">
							</div>
							
						</div>
					</div>
					</form>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center" width="3%"><strong>S.No</strong></th>
                            	<th class="text-center"><strong>Country</strong></th>
                            	<th class="text-center"><strong>Zone</strong></th>
                            	<th class="text-center"><strong>State</strong></th>
                            	<th class="text-center"><strong>City </strong></th>
                                <th class="text-center"><strong>Area </strong></th>
                                <th class="text-center" width="10%"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($area_results)>0)
							{
								foreach($area_results as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo @$row['country_name'];?> </td>
										<td class="text-center"><?php echo @$row['zone_name'];?> </td>
										<td class="text-center"><?php echo @$row['state_name'];?> </td>
										<td class="text-center"><?php echo @$row['city_name'];?> </td>
										<td class="text-center"><?php echo $row['name']; ?></td>
										<td class="text-center">
                                        <?php if($row['status']==1){?>
										<a class="btn btn-default" data-toggle="tooltip" title="Edit" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_area/'.storm_encode($row['location_id']);?>"><i class="fa fa-pencil"></i></a>
										<?php }else{?>
										<a class="btn btn-default" data-toggle="tooltip" readonly title="Activate area to edit" style="padding:3px 3px;" href="#"><i class="fa fa-pencil"></i></a>
										<?php }
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" data-toggle="tooltip" title="Deactivate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_area/'.storm_encode($row['location_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" data-toggle="tooltip" title="Activate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_area/'.storm_encode($row['location_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="7" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	check_area();
</script>