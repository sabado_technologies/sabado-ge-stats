<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			<?php
			if(isset($flg))
			{
			    ?>
			    <div class="row"> 
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="col-md-12">
									<div class="header">
										<h5  style="float: left;color: #3380FF;margin-top:5px;"><strong>Country : <?php echo $country_name; ?></strong></h5>
										<h4 align="center" style="padding-right: 113px;margin-bottom: 5px;"><strong>Primary Details</strong></h4>
									</div><br>
								</div>
								<form class="form-horizontal" role="form"  action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
									<input type="hidden" name="tool_id" id="tool_id" value="<?php echo @$lrow['tool_id'];?>">    
									<input type="hidden" name="count_c" id="count_c" value="<?php echo @$count_c;?>">
									<input type="hidden" name="country_id" id="country_id" value="<?php echo @$country_id;?>">                
									<!-- Main Asset-->
										<div class="row">
											<div class="form-group">
												<div class="col-md-12">
													<label for="inputName" class="col-sm-2 control-label">Tool Number <span class="req-fld">*</span></label>
													<div class="col-sm-4">
														<input type="text" autocomplete="off" required class="form-control" placeholder="Tool Number" name="part_number" id="part_number" value="<?php echo @$lrow['part_number']; ?>">
													</div>

													<label for="inputName" class="col-sm-2 control-label">Tool Code <span class="req-fld">*</span></label>
													<div class="col-sm-4">
														<input type="text" autocomplete="off" required class="form-control only-numbers" placeholder="Tool Code" name="tool_code" id="tool_code" parsley-minlength="4" maxlength="4" value="<?php if($flg == 1){ echo @$tool_code;  } else{  echo @$lrow['tool_code']; }?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="inputeq_model" class="col-sm-2 control-label">Asset Level <span class="req-fld">*</span></label>
						                            <div class="col-sm-4">
														<select class="form-control asset_level_id" required name="asset_level_id">
				                                            <option value=""> Select Asset Level </option>
				                                            <?php
				                                              foreach($asset_level_details as $asset_level)
				                                              {
				                                              $selected = ($asset_level['asset_level_id']==@$lrow['asset_level_id'])?'selected="selected"':'';
				                                              echo '<option value="'.@$asset_level['asset_level_id'].'"  '.$selected.'>'.@$asset_level['name'].'</option>';
				                                              }
				                                              ?>
				                                        </select>
													</div>

													
													<label class="col-sm-2 control-label">Tool Description <span class="req-fld">*</span></label>
													<div class="col-sm-4">
														<textarea class="form-control" id="description" required name="part_description"><?php echo @$lrow['part_description'];  ?></textarea>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="inputeq_model" class="col-sm-2 control-label">Tool Type <span class="req-fld">*</span></label>
						                            <div class="col-sm-4">
														<select class="form-control tool_type_id" required name="tool_type_id">
				                                            <option value=""> Select Tool Type </option>
				                                            <?php
				                                              foreach($tool_type_details as $tool_type)
				                                              {
				                                              $selected = ($tool_type['tool_type_id']==@$lrow['tool_type_id'])?'selected="selected"':'';
				                                              echo '<option value="'.@$tool_type['tool_type_id'].'"  '.$selected.'>'.@$tool_type['name'].'</option>';
				                                              }
				                                              ?>
				                                        </select>
													</div>
													<label class="col-sm-2 control-label">Modality <span class="req-fld">*</span></label>
					                                <div class="col-sm-4">
														<select class="select2" required name="modality_id">
				                                            <option value=""> Select Modality </option>
				                                            <?php
				                                              foreach($modality_details as $modality)
				                                              {
				                                              $selected = ($modality['modality_id']==@$lrow['modality_id'])?'selected="selected"':'';
				                                              echo '<option value="'.@$modality['modality_id'].'"  '.$selected.'>'.@$modality['name'].'</option>';
				                                              }
				                                              ?>
				                                        </select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="inputName" class="col-sm-2 control-label">Model <span class="req-fld">*</span></label>
													<div class="col-sm-4">
														<input type="text" autocomplete="off" required class="form-control" placeholder="model" name="model" value="<?php echo @$lrow['model']; ?>">
													</div>
													<label for="inputeq_model" class="col-sm-2 control-label">Supplier <span class="req-fld">*</span></label>
					                                <div class="col-sm-4">
														<select class="form-control" required name="supplier_id">
				                                            <option value=""> Select Supplier </option>
				                                            <?php
				                                              foreach($supplier_detials as $supplier)
				                                              {
				                                              $selected = ($supplier['supplier_id']==@$lrow['supplier_id'])?'selected="selected"':'';
				                                              echo '<option value="'.@$supplier['supplier_id'].'"  '.$selected.'>'.@$supplier['name'].'</option>';
				                                              }
				                                              ?>
				                                        </select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12 custom-icheck">
													<label for="inputName" class="col-sm-2 control-label">Calibration <span class="req-fld">*</span></label>
			                                        <div class="col-sm-4 custom_icheck">
					                                    <label class="radio-inline"> 
					                                        <div class="iradio_square-blue <?php if(@$lrow['cal_type_id']==1 || $flg == 1) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
					                                            <input type="radio" class="cal_type_id" value="1" name="cal_type_id" <?php if(@$lrow['cal_type_id']==1 || $flg == 1) { echo "checked"; }?> style="position: absolute; opacity: 0;">
					                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
					                                        </div> 
					                                        Required
					                                    </label>
					                                    <label class="radio-inline"> 
					                                        <div class="iradio_square-blue <?php if(@$lrow['cal_type_id']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
					                                            <input type="radio" class="cal_type_id" value="2" name="cal_type_id" <?php if(@$lrow['cal_type_id']==2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
					                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
					                                        </div> 
					                                        Not Required
					                                    </label>
					                                </div>
					                                <div class="cal_sup <?php if(@$lrow['cal_type_id']==2) { echo "hidden"; } ?>">
				                                        <label for="inputeq_model" class="col-sm-2 control-label">Calibration Supplier <span class="req-fld">*</span></label>
						                                <div class="col-sm-4">
															<select class="form-control cal_supplier_id"  name="cal_supplier_id">
					                                            <option value=""> Select Calibration Supplier </option>
					                                            <?php
					                                              foreach($cal_supplier_details as $supplier)
					                                              {
					                                              $selected = ($supplier['supplier_id']==@$lrow['cal_supplier_id'])?'selected="selected"':'';
					                                              echo '<option value="'.@$supplier['supplier_id'].'"  '.$selected.'>'.@$supplier['name'].'</option>';
					                                              }
					                                              ?>
					                                        </select>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="inputeq_model" class="col-sm-2 control-label"> Asset Type <span class="req-fld">*</span></label>
					                            	<div class="col-sm-4">
														<label class="radio-inline"> <input type="radio" <?php if(@$lrow['asset_type_id']==1) { echo "checked"; } if($flg == 1) { echo "checked"; }?> value="1" name="asset_type_id" class="icheck"> Assembly</label> 
														<label class="radio-inline"> <input type="radio" value="2" <?php if(@$lrow['asset_type_id']==2) { echo "checked"; }?> class="icheck" name="asset_type_id"> Component</label>
													</div>
													<label for="inputName" class="col-sm-2 control-label">Kit <span class="req-fld">*</span></label>
			                                       	<div class="col-sm-4">
				                                        <label class="radio-inline"> <input type="radio" <?php if(@$lrow['kit']==1) { echo "checked"; } if($flg == 1) { echo "checked"; }?> value="1" name="kit_id" class="icheck"> Yes</label> 
														<label class="radio-inline"> <input type="radio" value="2" class="icheck" name="kit_id" <?php if(@$lrow['kit']==2) { echo "checked"; }?>> No</label>
			                                        </div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="inputName" class="col-sm-2 control-label">Manufacturer <span class="req-fld">*</span></label>
													<div class="col-sm-4">
														<input type="text" autocomplete="off"  required class="form-control" placeholder="Manaufacturer" name="manufacturer" value="<?php echo @$lrow['manufacturer']; ?>">
													</div>
													<label for="inputName" class="col-sm-2 control-label">HSN Code <span class="req-fld">*</span></label>
													<div class="col-sm-4">
														<input type="text" autocomplete="off" required class="form-control" placeholder="HSN Code" name="hsn_code" value="<?php echo @$lrow['hsn_code']; ?>">
													</div>
												</div>
											</div>
											<div class="div2 <?php if(@$lrow['asset_level_id']==3) { echo "hidden";} ?>">
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputName" class="col-sm-2 control-label">Length</label>
														<div class="col-sm-4">
															<input type="text" autocomplete="off"  class="form-control length" placeholder="Length" name="length" value="<?php echo @$lrow['length']; ?>">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputName" class="col-sm-2 control-label">Breadth</label>
														<div class="col-sm-4">
															<input type="text" autocomplete="off"  class="form-control breadth" placeholder="Breadth" name="breadth" value="<?php echo @$lrow['breadth']; ?>">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputName" class="col-sm-2 control-label">Height</label>
														<div class="col-sm-4">
															<input type="text" autocomplete="off"  class="form-control height" placeholder="Height" name="height" value="<?php echo @$lrow['height']; ?>">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
														<label for="inputName" class="col-sm-2 control-label">Weight</label>
														<div class="col-sm-4">
															<input type="text" autocomplete="off" placeholder="Weight" class="form-control only-numbers" name="weight" value="<?php echo @$lrow['weight']; ?>">
														</div>
														<label for="inputeq_model" class="col-sm-2 control-label">Equipment Model </label>
						                            	<div class="col-sm-4">
															<?php echo form_dropdown('eq_model[]', $eq_model, @$eq_model_selected,'class="select2" multiple'); ?>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label for="inputName" class="col-sm-2 control-label">GST % <span class="req-fld">*</span></label>
													<div class="col-sm-4">
														<input type="number" min="0" max="99" maxlength="2" autocomplete="off" required placeholder="GST Percentage" class="form-control" name="gst_percent" value="<?php echo @$lrow['gst_percent']; ?>">
													</div>
													<label for="inputName" class="col-sm-2 control-label">Remarks</label>
													<div class="col-sm-4">
														<textarea class="form-control" name="remarks"><?php echo @$lrow['remarks']; ?></textarea>
													</div>
												</div>
												<!-- gowri 11-april -->
												<label for="inputName" class="col-sm-3 control-label">Allow Order For Oneyear <span class="req-fld">*</span></label>
			                                       	<div class="col-sm-4">
				                                        <label class="radio-inline"> <input type="radio" <?php if(@$lrow['remarks2']==1) { echo "checked"; } ?> value="1" name="remarks2" class="icheck"> Yes</label> 
														<label class="radio-inline"> <input type="radio" <?php if(@$lrow['remarks2']=='') { echo "checked"; } ?> value="2" class="icheck" name="remarks2"> No</label>
			                                        </div>
			                                    <!-- end -->
											</div><br>
										</div>
										<div class="div3 <?php if(@$lrow['asset_level_id']==3) { echo "hidden";} ?>">
											<div class="header" style="margin-bottom: 15px;" align="center">
												<h4><strong>Sub Component Details</strong></h4>		
											</div>
											<div class="col-md-offset-9 col-md-2" style="margin-left: 742px;">
											<a id="add_component" class="btn btn-primary"><i class="fa fa-plus"></i> Add Sub Component</i></a>
											</div>
											<div class="table-responsive col-md-offset-1 col-md-10 hidden tab_hide" style="margin-top: 5px;">
												<table class="table table-bordered hover component_table">
													<thead>
														<tr>
															<th class="text-center" width="8%" ><strong>S.No.</strong></th>
															<th class="text-center" width="40%"><strong>Tool Number</strong></th>
															<th width="20%" class="text-center"><strong>quantity</strong></th>
															<th width="8%" class="text-center"><strong>Delete</strong></th>
														</tr>
													</thead>
													<tbody>
													<?php $count =1;
														if(count(@$component_details)>0)
														{	
															foreach($component_details as $cd)
															{?>	
																<tr class="sub_comp_row">
																	<input type="hidden" name="tool_part_id" class="tool_part_id" value="<?php echo $cd['tool_part_id'];?>">
																	<td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
																	<td>
																		<div class="check_here">
																			<select name="part_arr[]" class="sub_tool_id" style="width:100%"> 
																				<option value="">Select Tool Number - Tool Description</option>
																				<option value="<?php echo $cd['tool_id']; ?>" selected><?php echo $cd['part_desc_name']; ?></option>
																			</select>
																		</div>
																	</td>
																	<td class="text-center">
								                                		<input class="form-control only-numbers sub_quantity" placeholder="Quantity"  name="quantity[]" value="<?php echo @$cd['quantity']; ?>" type="text" autocomplete="off" />
																	</td>
																	
															        <td class="text-center"><a class="btn btn-danger btn-sm deactivate_sub_comp remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
																</tr>
															 <?php		
															} 
														} ?>			
														<tr class="sub_comp_row">
															<td class="text-center"><span class="sno"><?php echo $count; ?></span></td>
															<td class="text-center">
																<div class="check_here">
																	<select name="part_arr[]" class=" sub_tool_id" style="width:100%"> 
																		<option value="">Select Tool Number - Tool Description</option>
																	</select>
																</div>
															</td>
															<td class="text-center">
						                                		<input class="form-control only-numbers sub_quantity" placeholder="Quantity"  name="quantity[]" type="text" autocomplete="off" />
															</td>
															
													        <td class="text-center"><a class="btn btn-danger btn-sm remove_bank_row"> <i class="fa fa-trash-o"></i></a></td>
														</tr>
													</tbody>
												</table><br>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-offset-5 col-sm-5">
												<button class="btn btn-primary" onclick="return confirm('Are you sure you want to Submit?')"  type="submit" value="1" name="submitTool"><i class="fa fa-check"></i> Submit</button>
												<a class="btn btn-danger" href="<?php echo SITE_URL;?>tool"><i class="fa fa-times"></i> Cancel</a>
											</div>
										</div>
								</form>
							</div>

						</div>	
						<br><br><br>			
					</div>
				</div>
				<?php 
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
			<div class="block-flat">
				<div class="content">
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'tool';?>">
							<div class="row">
								<div class="col-sm-12 form-group">
									
									<div class="col-sm-3">
										<input type="text" autocomplete="off" name="part_number" placeholder="Tool Number" value="<?php echo @$searchParams['part_number'];?>"  class="form-control" maxlength="100">
									</div>
									<div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="part_description" placeholder="Tool Description" value="<?php echo @$searchParams['part_description'];?>" id="location" class="form-control" maxlength="100">
									</div>
									<div class="col-sm-3">
										<input type="text" autocomplete="off" name="tool_code" placeholder="Tool Code" value="<?php echo @$searchParams['tool_code'];?>" id="serial_number" class="form-control"  maxlength="80">
									</div>
									<div class="col-sm-3">
										<select class="select2" name="part_level_id">
											<option value=''>- Tool Level -</option>
											<?php 
											foreach ($part_level_list as $key => $value) 
											{
												$selected = ($value['part_level_id']==@$searchParams['part_level_id'])?'selected="selected"':'';
		                                        echo '<option value="'.@$value['part_level_id'].'"  '.$selected.'>'.@$value['name'].'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="col-sm-12 form-group">
									<?php
									if($task_access == 3 && @$_SESSION['header_country_id']=='')
									{
										?>
										<div class="col-sm-3">
											<select name="country_id" class="main_status select2" >    <option value="">- Country -</option>
												<?php
												foreach($country_list as $row)
												{
													$selected = ($row['location_id']==@$searchParams['country_id'])?'selected="selected"':'';
													echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
												}?>
											</select>
										</div> <?php
									}?>
								</div>
							</div>
							<div class="row">
								<div class="header"></div>
								<table class="table table-bordered" ></table>
								<div class="col-sm-12">
									<div class="col-sm-6 col-md-6">							
										<a href="<?php echo SITE_URL.'add_tool'; ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
										<a href="<?php echo SITE_URL.'bulkupload_tool/5'; ?>" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Upload in Bulk</a>
										 
									</div>	
									<div class="col-sm-6 col-md-6" align="right">							
										<button type="submit" name="searchtool" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
										<a href="<?php echo SITE_URL.'tool'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
										<button type="submit" data-toggle="tooltip" title="Download" name="download_tool" value="1" formaction="<?php echo SITE_URL.'download_tool';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i>Download</button> 
									</div>		
													
										
								</div>
							</div>
						</form>
					<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th></th>
								<th class="text-center" width="5%"><strong>S.No.</strong></th>
								<th class="text-center"><strong>Tool Number</strong></th>
								<th class="text-center" width="35%"><strong>Tool Description</strong></th>
								<th class="text-center"><strong>Tool Code</strong></th>
								
								<th class="text-center" width="10%"><strong>Tool Level</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center" width="12%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($toolResults)>0)
							{
								foreach($toolResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php if(count(@$row['tool_parts'])>0) {?><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'><?php } ?></td>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['part_number'];?></td>
										<td class="text-center"><?php echo @$row['part_description'];?></td>
										<td class="text-center"><?php echo @$row['tool_code'];?></td>
										
										<td class="text-center"><?php echo @$row['tool_level_name'];?></td>
										<td align="center"><?php echo @$row['country']; ?></td>								
										<td class="text-center">
                                        <a class="btn btn-default" data-container="body" data-placement="top"  data-toggle="tooltip" title="Update Tool Details" <?php if($row['status']==2){ echo "disabled"; } ?> style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_tool/'.storm_encode($row['tool_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" data-container="body" data-placement="top"  data-toggle="tooltip" title="Deactivate Tool" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_tool/'.storm_encode($row['tool_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" data-container="body" data-placement="top"  data-toggle="tooltip" title="Activate Tool" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_tool/'.storm_encode($row['tool_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
                                        <?php 
											$data = 
											array('primary_key' => storm_encode(@$row['tool_id']),
										          'table_name'  => 'tool',
										          'work_flow'   => 'tool_master',
										          'main_table'  => 'tool',
										          'main_key'    => $row['tool_id']);
											$send_data = implode('~',$data); 
										?>
										<a class="btn btn-primary" 
											style="padding:3px 3px;"
											href="<?php echo SITE_URL.'get_audit_page/'.$send_data;?>"
											target="_blank"
											data-toggle="tooltip" title="Audit Trail"><i class="fa fa-book"></i>
										</a>
                                    </td>
									</tr>
									<?php if(count(@$row['tool_parts'])>0)
                                    {  $i = 1;?>
                                        <tr class="details">
                                            <td  colspan="8">
                                        <table class="table">
                                        	<thead>
                                        		<th class="text-center" width="6%"><strong>S.No.</strong></th>
                                        		<th class="text-center" width="15%"><strong>Tool Number</strong></th>
                                        		<th class="text-center" width="35%"><strong>Tool Description</strong></th>
                                        		<th class="text-center" width="10%"><strong>Tool Code</strong></th>
                                        		<th class="text-center" width="10%"><strong>Tool Level</strong></th>
                                        		<th class="text-center" width="7%"><strong>Quantity</strong></th>
                                        		<th class="text-center" width="10%"><strong>Actions</strong></th>
                                        	</thead>
                                            <tbody>
                                       <?php foreach(@$row['tool_parts'] as $value)
                                        { 
                                        ?>
                                            <tr class="asset_row">
                                                <td align="center" ><?php echo $i++; ?></td>
                                                <td align="center"><?php echo $value['part_number']; ?></td>
                                                <td align="center"><?php echo $value['part_description']; ?></td>
                                                <td align="center"><?php echo $value['tool_code']; ?></td>
                                                <td align="center">L1</td>
                                                <td align="center"><?php echo $value['quantity']; ?></td>
                                                
                                                <td class="text-center">
                                        			<a class="btn btn-default" data-container="body" data-placement="top"  data-toggle="tooltip" title="Update Tool Details" <?php if($row['status']==2 || $value['status']==2){ echo "disabled"; } ?> style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_tool/'.storm_encode($value['tool_id']);?>"><i class="fa fa-pencil"></i></a>
                                        			<?php 
														$data = 
														array('primary_key' => storm_encode(@$value['tool_id']),
													          'table_name'  => 'tool',
													          'work_flow'   => 'tool_master',
													          'main_table'  => 'tool',
													          'main_key'    => $value['tool_id']);
														$send_data = implode('~',$data); 
													?>
													<a class="btn btn-primary" 
														style="padding:3px 3px;"
														href="<?php echo SITE_URL.'get_audit_page/'.$send_data;?>"
														target="_blank"
														data-toggle="tooltip" title="Audit Trail"><i class="fa fa-book"></i>
													</a>
                                    			</td>
                                            </tr>
                                        <?php     
                                        } ?>

                                            </tbody>
                                        </table>
                                        </td>
                                    </tr><?php
                                    }
								}
							} else {
							?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			<?php 
			} ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	add_component_new_row();
	get_asset_level_id();
	check_tool_code_availability();
	check_part_number_availability();
</script>