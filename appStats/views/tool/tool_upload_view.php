<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        	<?php echo $this->session->flashdata('response'); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
	                    <div class="content">
	                    	
	                    	<form class="form-horizontal"  id="bulkUploadFrm" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data" >
	                        	<div class="form-group">
	                                <label for="inputName" class="col-sm-3 control-label">Upload CSV File</label>
	                                <div class="col-sm-6">
	                                 	<input type="file" name="uploadCsv" id="uploadCsv" accept=".csv" >
	                                    <p><small>Note: Upload Data In Specified CSV Format Only</small></p>
									</div>
	                            </div>
	                            <div class="form-group">
	                                <div class="col-sm-offset-3 col-sm-10">
	                                    <button class="btn btn-primary" type="submit"  value ="1" name="bulkUpload"><i class="fa fa-cloud-upload"></i> Upload Data</button>
	                                </div>
	                            </div>
	                        </form>
	                        <div class="row">
	                         	<div class="col-sm12  col-md-12">
	                            	<div class="block-flat1">
	                                    <div class="header">							
	                                        <h4><strong>Instructions</strong></h4>
	                                    </div>
	                                    <div class="content">
	                                    	<div class="col-sm-12">
	                                        	<div class="col-sm-4">
	                                            	<h5><strong>Bulk Upload Template</strong></h5>
	                                                <!-- <p>
														<a href="samples/spares_bulk_upload_sample.csv" class="btn btn-primary">
														<i class="fa fa-gears"></i> Spares Template</a> 
	                                                </p>
	                                                <p>
														<a href="samples/consumables_bulk_upload_sample.csv" class="btn btn-primary">
														<i class="fa fa-gavel"></i> Consumables Template</a>
	                                                </p>-->
	                                                <p>
														<a href="<?php echo SITE_URL1;?>samples/wge_sample_tool_template.csv" class="btn btn-primary"><i class="fa fa-wrench"></i> Tools Template</a>
	                                                </p>
	                                                <?php if(@$failed_upload==1) { ?>
	                                                <p>
														<a href="<?php echo SITE_URL;?>download_missed_tool_uploads" class="btn btn-danger">
														<i class="fa fa-gavel"></i> Missed Upload </a>
	                                                </p> <?php }?>
	                                                <br>
													<!-- <a href="equipmentTypeInfo" target="_blank"><h5><strong>Click to View Equipment Type Info</strong></h5></a> -->
	                                            </div>
	                                            <!-- <div class="col-sm-4">
	                                            	<h5><strong>Modality Info</strong></h5>
	                                                <div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th><strong>Modality ID</strong></th>
																	<th><strong>Modality</strong></th>
																</tr>
															</thead>
															<tbody>
																<?php
																foreach($modality_results as $mrow)
																{
																	?>
																	<tr>
																		<td align="center"><?php echo $mrow['modality_id'];?></td>
																		<td align="center"><?php echo $mrow['name'];?></td>
																	</tr>
																	<?php
																}
																?>
															</tbody>
														</table>
	                                                </div>
	                                                
	                                            </div> -->
	                                            <div class="col-sm-4">
	                                            	<h5><strong>Supplier Info</strong></h5>
	                                                <div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th align="center"><strong>Supplier Code</strong></th>
																	<th align="center"><strong>Name </strong></th>																	
																</tr>
															</thead>
															<tbody>
																<?php
																foreach($supplier as $suprow)
																{
																	?>
																	<tr>
																		<td align="center"><?php echo $suprow['supplier_code'];?></td>
																		<td align="center"><?php echo $suprow['name'];?></td>
																		
																	</tr>
																	<?php
																}
																?>
															</tbody>
														</table>
	                                                </div>
	                                                
	                                        	</div>
	                                            <div class="col-sm-4">
	                                            	<h5><strong>Calibration Supplier </strong></h5>
	                                                <div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th align="center"><strong>Calibration Supplier Code</strong></th>
																	<th align="center"><strong>Name</strong></th>																	
																</tr>
															</thead>
															<tbody>
																<?php
																foreach($cal_supplier as $csup)
																{
																	?>
																	<tr>
																		<td align="center"><?php echo $csup['supplier_code'];?></td>
																		<td align="center"><?php echo $csup['name'];?></td>
																		
																	</tr>
																	<?php
																}
																?>
															</tbody>
														</table>
	                                                </div>
	                                            </div>
	                                            	
	                                            
	                                    </div>
	                                </div>
	                            </div>
	                         </div>
	                    </div>
					</div>
				</div>
			</div>		    
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
