<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>tools_inventory">
						<input type="hidden" name="c_id" id="country_id" value="<?php echo @$country_id;?>">
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_no" placeholder="Tool No" value="<?php echo @$search_data['tool_no'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
	                                <?php 
                                    $d['name']        = 'user_id';
                                    $d['search_data'] = @$search_data['user_id'];
                                    $this->load->view('sso_dropdown_list',$d); 
                                    ?>
	                            </div>
								<div class="col-sm-3">
									<select class="select2 asset_type" name="asset_type" >
										<option value="">- Asset Main Status -</option>
										<option value="1" <?php if(@$search_data['asset_type'] == 1){ echo "selected"; }?>>Active</option>
										<option value="2" <?php if(@$search_data['asset_type'] == 2){ echo "selected"; }?>>InActive</option>
									</select>
								</div>
								<div class="col-sm-3">
									<select class="select2 zone_id" name="zone_id">
										<option value="">- Zone -</option>
										<?php 
										    if(@$zoneList)
											{
												foreach ($zoneList as $zone)
												{
													$selected = ($zone['location_id']==$search_data['zone_id'])?'selected="selected"':'';
													echo '<option value="'.$zone['location_id'].'" '.$selected.'>'.$zone['name'].'</option>';	
												}

											}?>
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Desc" value="<?php echo @$search_data['tool_desc'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<select class="select2" name="modality_id" >
										<option value="">- Modality -</option>
										 <?php
										foreach($modalityList as $mod)
										{
											$selected = ($mod['modality_id']==$search_data['modality_id'])?'selected="selected"':'';
											echo '<option value="'.$mod['modality_id'].'" '.$selected.'>'.$mod['name'].'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-sm-3">
									<select class="select3 asset_status" name="asset_status" style="width:100%">
										<option value="">- Asset Status -</option>
										<?php 
											if(@$search_data['asset_type'] !='')
											{
												foreach ($astatusList as $astatus)
												{
													$selected = ($astatus['asset_status_id']==$search_data['asset_status'])?'selected="selected"':'';
													echo '<option value="'.$astatus['asset_status_id'].'" '.$selected.'>'.$astatus['name'].'</option>';	
												}
											}
										    else
									    	{
									    		foreach ($asset_status as $astatus)
												{
													$selected = ($astatus['asset_status_id']==$search_data['asset_status'])?'selected="selected"':'';
													echo '<option value="'.$astatus['asset_status_id'].'" '.$selected.'>'.$astatus['name'].'</option>';	
												}
									    	}?>
									</select>
								</div>
								<div class="col-sm-3">
									<select class="select4 wh_id" name="warehouse" style="width: 100%">
										<option value="">- Inventory -</option>
										 <?php
										 if($search_data['zone_id']!='')
										 {
										 	foreach($zwhList as $wh)
										 	{
										 		$selected = ($wh['wh_id']==$search_data['warehouse'])?'selected="selected"':'';
												echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_name'].'</option>';
										 	}
										 }
										else
										{
											foreach($whList as $wh)
											{
												$selected = ($wh['wh_id']==$search_data['warehouse'])?'selected="selected"':'';
												echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_name'].'</option>';
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
								    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<input class="form-control date" required size="16" type="text" autocomplete="off" value="<?php if(@$search_data['created_date']!='') { echo indian_format(@$search_data['created_date']); }?>" readonly placeholder="Created Date" name="created_date" style="cursor:hand;background-color: #ffffff">								
								</div>
	                            <div class="col-sm-3">
	                            	<button type="submit" data-toggle="tooltip" title="Search" name="searchtools" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
									<button type="submit" data-toggle="tooltip" title="Download" onclick="return confirm('Are you sure you want to download?')" name="download_tools_inventory" value="1" formaction="<?php echo SITE_URL.'download_tools_inventory';?>" class="btn btn-success"><i class="fa fa-cloud-download"></i></button>  
									<a href="<?php echo SITE_URL.'tools_inventory'; ?>" data-toggle="tooltip" title="Reset" class="btn btn-success"><i class="fa fa-refresh"></i></a>
								</div>
                            </div>
                            
                        	<?php if($task_access == 3 && $_SESSION['header_country_id']==''){?> 
                        		<div class="col-sm-12 form-group">
		                            <div class="col-sm-3">
		                                <select class="select2" name="country_id" >
		                                    <option value="">- Country -</option>
		                                     <?php
		                                    foreach($countryList as $country)
		                                    {
		                                        $selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
		                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
		                                    }
		                                    ?>
		                                </select>
		                            </div>
	                            </div>
                        	<?php } ?>
                            
						</div>
						<div class="row">
							<div class="content">
								<div class="header"></div>
								<table class="table table-bordered" ></table>
							</div>
						</div>
					</form>
<div class="table-responsive" style="margin-top: 10px;">
    <table class="table table-bordered hover">
        <thead>
            <tr>
                <th></th>
                <th class="text-center"><strong>S.No</strong></th>
                <th class="text-center"><strong>Tool No</strong></th>
                <th class="text-center"><strong>Tool Description</strong></th>
                <th class="text-center"><strong>Asset No</strong></th>
                <th class="text-center"><strong>Modality</strong></th>
                <th class="text-center"><strong>Asset Main Status</strong></th>
                <th class="text-center"><strong>Asset Status</strong></th>
                <th class="text-center"><strong>Inventory</strong></th>
                <th class="text-center"><strong>Pallet Location</strong></th>
                <th class="text-center"><strong>Tool Availability</strong></th>
                <th class="text-center"><strong>Country</strong></th>
                <th class="text-center"><strong>Actions</strong></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if(count(@$t_inventory_results)>0)
            {   
                foreach(@$t_inventory_results as $row)
                { 
                ?>
                    <tr>
                        <td class="center "><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                        <td class="text-center"><?php echo $sn++; ?></td>
						<td class="text-center"><?php echo $row['part_number']; ?></td>
						<td class="text-center"><?php echo $row['part_description']; ?></td>
						<td class="text-center"><?php echo $row['asset_number']; ?></td>
						<td class="text-center"><?php echo $row['modality_name']; ?></td>
						<td class="text-center">
							<?php 
							if(@$row['availability_status']==1)
							{
								echo "Active";
							}
							else
							{
								echo "InActive";
							} ?>
						</td>

						<td class="text-center">
							<?php 
							if($row['approval_status']==1)
							{
								echo get_da_status($row['asset_id'],1);
							}
							else
							{
								echo $row['asset_status'];
							} ?>	
						</td>
						<td class="text-center"><?php echo$row['wh_code'].' -('.$row['wh_name'].')'; ?></td>
						<td class="text-center"><?php echo $row['sub_inventory']; ?></td>
						<td class="text-center"><?php echo get_asset_position($row['asset_id']); ?></td>
						<td class="text-center"><?php echo $row['country_name']; ?></td>
						<td class="text-center">
							<a class="btn btn-success" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Print QR"  href="<?php echo SITE_URL.'asset_qr/'.storm_encode($row['asset_id']).'/2';?>"><i class="fa fa-print"></i></a>
                        	<?php $data = array(
                        		      'primary_key' => storm_encode(@$row['asset_id']),
							          'table_name'  => 'asset',
							          'work_flow'   => 'asset_master',
							          'main_table'  => 'asset',
							          'main_key'    => $row['asset_id']);
								$send_data = implode('~',$data); 
							?>
							<a class="btn btn-default" 
								style="padding:3px 3px;margin-top: 5px;"
								href="<?php echo SITE_URL.'get_audit_page/'.$send_data;?>"
								target="_blank"
								data-toggle="tooltip" title="Audit Trail"><i class="fa fa-book"></i>
							</a>
                    	</td>
                    </tr>
                <?php if(count(@$row['asset_list'])>0)
                    {  $slno = 1; ?>
                    <tr class="details">
                        <td  colspan="13">
                            <table class="table">
                            	<thead>
		                            <tr>
		                                <th class="text-center"><strong>Serial Number</strong></th>
		                                <th class="text-center"><strong>Tool Number</strong></th>
		                                <th class="text-center"><strong>Tool Description</strong></th>
		                                <th class="text-center"><strong>Quantity</strong></th>
		                                <th class="text-center"><strong>Tool Level</strong></th>
		                                <th class="text-center"><strong>Tool Health</strong></th>
		                                <th class="text-center"><strong>Remarks</strong></th>
		                            </tr>
		                        </thead>
                                <tbody>
                            <?php foreach(@$row['asset_list'] as $value)
                            { 
                            ?>
                                <tr class="asset_row">
                                    <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                    <td class="text-center"><?php echo $value['part_number']; ?></td>
                                    <td class="text-center"><?php echo $value['part_description']; ?></td>
                                    <td class="text-center"><?php echo $value['quantity']; ?></td>
                                    <td class="text-center"><?php echo $value['part_level_name']; ?></td>
                                    <td class="text-center"><?php 
                                    if($value['p_status']==1)
                                    {
                                    	echo "Good";
                                    }
                                    else if($value['p_status']==2)
                                    {
                                    	echo "Defective";
                                    } 
                                    else if($value['p_status']==3)
                                    {
                                    	echo "Missing";
                                    }?></td>
                                    <td class="text-center"><?php 
                                    if($value['remarks']!='')
                                    {
                                    	echo $value['remarks'];
                                    }
                                    else
                                    {
                                    	echo "--";
                                    } ?></td>
                                </tr>
                            <?php     
                            } ?>
								</tbody>
                            </table>
                        </td>
                    </tr><?php
                    }
                    else
                    { ?>
                        <tr><td colspan="12" class="text-center"><span class="label label-primary">- No Tools Found -</span></td></tr>
                    <?php 
                    }
                }
            
            } else {?>
                <tr><td colspan="13" align="center"><span class="label label-primary">No Records Found</span></td></tr>
    <?php   } ?>
        </tbody>
    </table>
</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>
	          		
				</div>
			</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(document).ready(function(){
    $('.select3, .select4').select2();
});
</script>
