<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>toolUtilization">
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_number" placeholder="Tool No" value="<?php echo @$search_data['tool_number'];?>" class="form-control">
								</div>

								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_description" placeholder="Tool Desc" value="<?php echo @$search_data['tool_description'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="asset_number" placeholder="Asset No" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
								    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<select class="select2" name="modality_id" >
										<option value="">- Modality -</option>
										 <?php
										foreach($modalityList as $mod)
										{
											$selected = ($mod['modality_id']==$search_data['modality_id'])?'selected="selected"':'';
											echo '<option value="'.$mod['modality_id'].'" '.$selected.'>'.$mod['name'].'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-sm-3">
									<select class="select2" name="tool_type_id" >
										<option value="">- Tool Type -</option>
										 <?php
										foreach($toolType as $tt)
										{
											$selected = ($tt['tool_type_id']==$search_data['tool_type_id'])?'selected="selected"':'';
											echo '<option value="'.$tt['tool_type_id'].'" '.$selected.'>'.$tt['name'].'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-sm-3">
									<input id="dateFrom" type="text" autocomplete="off" readonly name="from_date" placeholder="From Date" value="<?php echo @$search_data['from_date'];?>" class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">
									<input id="dateTo" type="text" autocomplete="off" readonly name="to_date" placeholder="To Date" value="<?php echo @$search_data['to_date'];?>" class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<?php 
                                    $d['name']        = 'fe_sso_id';
                                    $d['search_data'] = @$search_data['fe_sso_id'];
                                    $this->load->view('sso_dropdown_list',$d); 
                                    ?>
								</div> 
								<?php
								if($task_access == 3 && @$_SESSION['header_country_id']=='')
								{
									?>
									<div class="col-sm-3">
										<select name="country_id" class="select2" >    
											<option value="">- Country -</option>
											<?php
											foreach($countryList as $row)
											{
												$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
												echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
											}?>
										</select>
									</div> <?php
								}?>
								<div class="col-sm-6" align="right"> 							
									<button type="submit" name="searchtools" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<button type="submit" onclick="return confirm('Are you sure you want to download?')" formaction="<?php echo SITE_URL;?>download_tools_utilization" name="download_tools_utilization" value="1" class="btn btn-success"><i class="fa fa-download"></i> Download</button> 
									<a href="<?php echo SITE_URL.'toolUtilization'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
								</div>	
							</div>
						</div>
						<div class="row">
							<div class="content">
								<div class="header"></div>
							</div>
						</div>
					</form>
					<div class="table-responsive" style="margin-top: 10px;">
					    <table class="table table-bordered hover">
					        <thead>
					            <tr>
					                
					                <th class="text-center"><strong>S.No</strong></th>
					                <th class="text-center" width="13%"><strong>SSO</strong></th>
					                <th class="text-center"><strong>Tool Number</strong></th>
					                <th class="text-center"><strong>Tool Description</strong></th>
					                <th class="text-center"><strong>Asset Number</strong></th>
					                <th class="text-center"><strong>Serial Number</strong></th>
					                <th class="text-center"><strong>Modality</strong></th>
					                <th class="text-center"><strong>Tool Type</strong></th>
					                <th class="text-center" width="8%"><strong>Received Date</strong></th>
					                <th class="text-center" width="8%"><strong>Returned Date</strong></th>
					                <th class="text-center" width="8%"><strong>Country</strong></th>
					                <th class="text-center"><strong>No. Days Used</strong></th>
					           </tr>
					        </thead>
					        <tbody>
					            <?php 
					            if(count(@$t_inventory_results)>0)
					            {   
					                foreach(@$t_inventory_results as $row)
					                { 
					                ?>
					                    <tr>
					                        <td class="text-center"><?php echo $sn++; ?></td>
											<td class="text-center"><?php echo $row['sso']; ?></td>
											<td class="text-center"><?php echo $row['part_number']; ?></td>
											<td class="text-center"><?php echo $row['part_description']; ?></td>
											<td class="text-center"><?php echo $row['asset_number']; ?></td>
											<td class="text-center"><?php echo $row['serial_number']; ?></td>
											<td class="text-center"><?php echo $row['modality']; ?></td>
											<td class="text-center"><?php echo $row['tool_type']; ?></td>
											<td class="text-center"><?php echo indian_format($row['from_date']); ?></td>
											<td class="text-center"><?php if($row['to_date']!=''){ echo indian_format($row['to_date']);}else{ echo "--";} ?></td>
											<td class="text-center"><?php echo $row['country_name']; ?></td>
											<td class="text-center"><?php echo ((@$row['to_date']!='')?dateDiffInDays($row['from_date'],$row['to_date']):dateDiffInDays($row['from_date'],date('Y-m-d'))).' Days'; ?></td>
											<?php
					            
					            } 
					        }else {?>
					                <tr><td colspan="12" align="center"><span class="label label-primary">No Records Found</span></td></tr>
					    <?php   } ?>
					        </tbody>
					    </table>
					</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>
	          		
				</div>
			</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>

