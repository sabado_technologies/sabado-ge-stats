<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        	<?php echo $this->session->flashdata('response'); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>user">
					<div class="row">
						<div class="col-sm-9 col-md-9"></div>
							<div class="col-sm-3 col-md-3">				
								<a href="<?php echo SITE_URL.'tool'; ?>" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
								<a href="<?php echo SITE_URL.'download_missed_tool_uploads' ?>" class="btn btn-success"><i class="fa fa-download"></i> Download List</a>
							</div>	
					</div>
				</form><br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>

                                <th class="text-center"><strong>Part Number</strong></th>
                                <th class="text-center"><strong>Part Description</strong></th>
                                <th class="text-center"><strong>Tool Code</strong></th>
                                
                                <th class="text-center"><strong>Remarks</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php //$i=1;
							if(count($missedResults)>0)
							{
								foreach($missedResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++; ?></td>
										<td class="text-center"><?php echo $row['part_number']; ?></td>
										<td class="text-center"><?php echo $row['part_description']; ?></td>
										<td class="text-center"><?php echo $row['tool_code']; ?></td>
									<!--	<?php $eq_arr = explode('|',$row['eq_model']) ;
												$k= 0;  
												$eq_string = '';
											   foreach ($eq_arr as $key => $value) {
											   		if($k==0)
								                    {
								                    	$eq_string .= $value;
								                        $k++;
								                        continue;
								                    }
								                    $eq_string.= ','.$value;
											   }
											?>  -->
										
										<td class="text-center"><?php echo $row['tool_part_remarks']; ?></td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="13" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>
				</div>
			</div>		    
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
