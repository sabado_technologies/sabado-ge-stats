<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<?php
			if($flg==2)
			{
				?>
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="row">
											<div class="header">
												<h4 align="center"><strong>Asset Details</strong></h4>
											</div>							
											<div class="col-md-6">
												<table class="no-border">
													<tbody class="no-border-x no-border-y">
														<tr>
															<td class="data-lable"><strong>Tool Number :</strong></td>
													        <td class="data-item"><?php echo @$asset_row['part_number'];?></td>
														</tr>
														<tr>
															<td class="data-lable"><strong>Asset Number :</strong></td>
													        <td class="data-item"><?php echo @$asset_row['asset_number'];?></td>
														</tr>
														<tr>
															<td class="data-lable"><strong>Warehouse :</strong></td>
															<td class="data-item"><?php echo @$asset_row['wh_code'].' - '.'('.$asset_row['warehouse'].')'?></td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="col-md-6">
												<table class="no-border">
													<tbody class="no-border-x no-border-y">
														<tr>
															<td class="data-lable"><strong>Tool Description :</strong></td>
													        <td class="data-item"><?php echo @$asset_row['part_description'] ?></td>
														</tr>
														<tr>
															<td class="data-lable"><strong>Serial Number :</strong></td>
															<td class="data-item"><?php echo @$asset_row['serial_number'];?></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="form-group">
				                        		<div class="col-md-12">
													<div class="table-responsive col-md-offset-1 col-md-10">
														<table class="table table-bordered hover">
															<thead>
																<tr>
																	<th class="text-center"><strong>SNo.</strong></th>
																	<th width="18%" class="text-center"><strong>Attached Date</strong></th>
																	<th class="text-center"><strong>Document Type</strong></th>
																	<th width="30%" class="text-center"><strong>Supported Document</strong></th>
																	
																</tr>
															</thead>
															<tbody>
															<?php
																if(count(@$document)>0)
																{	$count = 1;
																	foreach($document as $ad)
																	{?>	
																		<tr>
																			<td align="center"><?php echo $count++; ?></td>
																			<td><?php echo date('d-m-Y H:i:s',strtotime($ad['created_time'])) ?></td>
																			<td align="center">
																				<?php echo $ad['document_type'];?>
																			</td>
																			<td>
										                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
																			</td>
																		</tr>
																	 <?php		
																	} 
																} 
																else {
																	?>	
																	<tr><td colspan="4" align="center">No Attached Docs.</td></tr>
																<?php } ?>
															</tbody>
														</table><br>
													</div>
												</div>
				                    		</div>
				                    		<div class="form-group">
												<div class="col-sm-offset-5 col-sm-5">

													<?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>
													<?php 
													if($page_enable == 2) 
													{ ?>
														<a class="btn btn-primary" href="<?php echo SITE_URL.'fe_owned_tools'.$current_offset;?>"><i class="fa fa-reply"></i> Back</a><?php 
													} 
													else if($page_enable == 1) 
													{ ?>
														<a class="btn btn-primary" href="<?php echo SITE_URL.'calibration_certificates'.$current_offset;?>" ><i class="fa fa-reply"></i> Back</a><?php 
													} ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div><br>
						</div>
					</div>
				</div><?php
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
				<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>calibration_certificates"  parsley-validate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12 form-group">
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="tool_no" placeholder="Tool Number" value="<?php echo @$search_data['tool_no'];?>" id="location" class="form-control" maxlength="100">
										</div>
										<input type="hidden" name="c_id" id="country_id" value="<?php echo @$country_id;?>">
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Description" value="<?php echo @$search_data['tool_desc'];?>" id="serial_number" class="form-control"  maxlength="80">
										</div>
										<div class="col-sm-3">
		                                    <input type="text" autocomplete="off" name="asset_number"  placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
										</div>
										<div class="col-sm-3">							
											<button type="submit" name="searchlist" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
											<a href="<?php echo SITE_URL.'calibration_certificates'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
										</div>
									</div>
									<div class="col-sm-12 form-group">
										<div class="col-sm-3">
										    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
										</div>
										<?php if($task_access == 2 || $task_access== 3){?>
                                            <div class="col-sm-3 ">
                                                <select class="select2" name="wh_id" >
                                                    <option value="">- Warehouse -</option>
                                                     <?php
                                                    foreach($whList as $wh)
                                                    {
                                                        $selected = ($wh['wh_id']==$search_data['whid'])?'selected="selected"':'';
                                                        echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_code'].' -('.$wh['name'].')</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <?php } 
                                            if($task_access == 3 && $_SESSION['header_country_id']==''){?>
                                            <div class="col-sm-3 ">
                                                <select class="select2" name="country_id" >
                                                    <option value="">- Country -</option>
                                                     <?php
                                                    foreach($countryList as $country)
                                                    {
                                                        $selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
                                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        <?php } ?>
									</div>
								</div>
								<div class="table-responsive">
									<table class="table table-bordered hover" id="mytable">
										<thead>
											<tr>
												<th class="text-center"><strong>S.No</strong></th>
												<th class="text-center"><strong>Tool Number</strong></th>
												<th class="text-center"><strong>Tool Description</strong></th>
												<th class="text-center"><strong>Asset Number</strong></th>
												<th class="text-center"><strong>Serial Number</strong></th>
												<th class="text-center"><strong>Warehouse</strong></th>
												<th class="text-center"><strong>Country</strong></th>
												<th class="text-center"><strong>Action</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php
											if(count($cal_list_Results)>0)
											{
												foreach($cal_list_Results as $row)
												{
													?>
													<tr class="toolRow">
														<td class="text-center"><?php echo $sn++;?></td>
														<td class="text-center"><?php echo $row['part_number'];?></td>
														<td class="text-center"><?php echo $row['part_description'];?></td>
														<td class="text-center"><?php echo $row['asset_number'];?></td>
														<td class="text-center"><?php echo $row['serial_number'];?></td>
														<td class="text-center"><?php echo $row['wh_code'].' - '.'('.$row['warehouse'].')';?>
														<td class="text-center"><?php echo $row['country_name'];?></td>	
														</td>
														<td class="text-center">
															<a  class="btn btn-default" 
																style="padding:3px 3px;"  
																data-container="body" 
																data-placement="top"  
																data-toggle="tooltip" 
																title="View"  
																href="<?php echo SITE_URL.'view_calibration_certificates_list/'.storm_encode($row['asset_id']).'/'.storm_encode(1); ?>">
																<i class="fa fa-eye"></i>
															</a>
														</td>
													</tr> <?php
												}
											}
											else 
											{
											?>	<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
											<?php 	} ?>
										</tbody>
									</table>
								</div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form> <?php
		    }?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>