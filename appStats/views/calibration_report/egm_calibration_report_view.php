<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'egm_calibration_report';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="block-flat">
					<div class="content">
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_no" placeholder="Tool No" value="<?php echo @$search_data['tool_no'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Desc" value="<?php echo @$search_data['tool_desc'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="asset_number" placeholder="Asset No" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
								    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="cr_number" placeholder="CR Number" value="<?php echo @$search_data['cr_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<select class="select2 wh_id" name="warehouse" style="min-height: 100%;min-width: 100%;">
										<option value="">- Warehouse -</option>
										<?php
										foreach($whList as $wh)
										{
											$selected = ($wh['wh_id']==$search_data['warehouse'])?'selected="selected"':'';
											echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_name'].'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-sm-3">
									<select class="select2" name="cr_status" style="width: 100%">
										<option value="">- CR Status -</option>
										<?php
											foreach ($current_stage as $stage)
											{
												$selected = ($stage['cal_status_id']==$search_data['cr_status'])?'selected="selected"':'';
												echo '<option value="'.$stage['cal_status_id'].'" '.$selected.'>'.$stage['name'].'</option>';	
											}
										?>
									</select>
								</div>
								<div class="col-sm-3">
									<select class="select2" name="modality_id" >
										<option value="">- Modality -</option>
										 <?php
										foreach($modalityList as $mod)
										{
											$selected = ($mod['modality_id']==$search_data['modality_id'])?'selected="selected"':'';
											echo '<option value="'.$mod['modality_id'].'" '.$selected.'>'.$mod['name'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input class="form-control date" required size="16" type="text" autocomplete="off" value="<?php if(@$search_data['cr_raised_date']!='') { echo indian_format(@$search_data['cr_raised_date']); }?>" readonly placeholder="CR Raised Date" name="cr_raised_date" style="cursor:hand;background-color: #ffffff">								
								</div>
									<?php if($task_access == 3 && $_SESSION['header_country_id']==''){?>
		                            <div class="col-sm-3">
		                                <select class="select2" name="country_id" >
		                                    <option value="">- Country -</option>
		                                     <?php
		                                    foreach($countryList as $country)
		                                    {
		                                        $selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
		                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
		                                    }
		                                    ?>
		                                </select>
		                            </div>
                            	<?php } ?>
                            	<div class="<?php if($_SESSION['header_country_id']!=''){ echo "col-sm-offset-5";} ?> col-sm-4">
	                                <button type="submit" name="searchcalibration" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<button type="submit" onclick="return confirm('Are you sure you want to download?')" formaction="<?php echo SITE_URL.'egm_calibration_download';?>" name="download_calibration" value="1" class="btn btn-success"><i class="fa fa-download"></i> Download</button>
									<a href="<?php echo SITE_URL.'egm_calibration_report'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
	                            </div>
                            </div>
						</div>
						<div class="header"></div>
						<div class="table-responsive">
							<table class="table table-bordered hover">
								<thead>
									<tr>
										<!-- <th class="text-center" width="3%"><strong>SNo</strong></th> -->
										<th class="text-center"><strong>CR Number</strong></th>
										<th class="text-center"><strong>Tool Number</strong></th>
										<th class="text-center"><strong>Tool Description</strong></th>
										<th class="text-center"><strong>Asset No</strong></th>
										<th class="text-center"><strong>Serial No</strong></th>
										<th class="text-center"><strong>Modality</strong></th>
										<th class="text-center"><strong>Asset Status</strong></th>
										<th class="text-center" width="8%"><strong>Cal Due Date</strong></th>
										<th class="text-center"><strong>Remaining day(s)</strong></th>
										<th class="text-center"><strong>CR Status</strong></th>
										<th class="text-center"><strong>Tool Availability</strong></th>
									</tr>
								</thead>
								<tbody>
									<?php
									if(count($calibration_results)>0)
									{
										foreach($calibration_results as $row)
										{
											?>
											<tr>
												<td class="text-center"><?php echo $row['cr_number'];?></td>
												<td class="text-center"><?php echo $row['part_number'];?></td>
												<td class="text-center"><?php echo $row['part_description'];?></td>
												<td class="text-center"><?php echo $row['asset_number'];?></td>
												<td class="text-center"><?php echo $row['serial_number'];?></td>
												<td class="text-center"><?php echo $row['modality_name'];?></td>
												<td class="text-center"><?php echo $row['asset_main_status']; ?></td>
												<td class="text-center"><?php if($row['cal_due_date']!='') { echo indian_format($row['cal_due_date']); }?></td>
												<td class="text-center"><?php 
												if($row['cal_status_id']<3)
												{
													if($row['days']>=0)
													{
														echo $row['days'];
													}
													else
													{
														$day = explode("-", $row['days']);
														echo $day[1].' '.'day(s) exceeded';
													}
												}
												else
												{
													echo "Closed";
												}
												?></td>
												<td class="text-center"><?php 
												if($row['approval_status']==1)
												{
													echo get_da_status($row['asset_id'],1);
												}
												else
												{
													echo $row['cal_status_name'];
												}
												?></td>
												<td class="text-center"><?php echo get_asset_position($row['asset_id']);?></td>
											</tr> 
											<?php
										}
									}
									else 
									{
									?>	<tr><td colspan="12" align="center"><span class="label label-primary">No Records</span></td></tr>
									<?php 	} ?>
								</tbody>
							</table>
						</div>
						<div class="row">
		                	<div class="col-sm-12">
			                    <div class="pull-left">
			                        <div class="dataTables_info" role="status" aria-live="polite">
			                            <?php echo @$pagermessage; ?>
			                        </div>
			                    </div>
			                    <div class="pull-right">
			                        <div class="dataTables_paginate paging_bootstrap_full_number">
			                            <?php echo @$pagination_links; ?>
			                        </div>
			                    </div>
		                	</div> 
		                </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>