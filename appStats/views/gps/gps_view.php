<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
			<div class="block-flat">
				<div class="content">
					<form method="post" action="<?php echo $form_action;?>" parsley-validate novalidate class="form-horizontal">
						<?php
                        if($flg==2){
                            ?>
                            <input type="hidden" name="encoded_id" value="<?php echo storm_encode($row['gps_tool_id']);?>">
                            <input type="hidden" name="desig_id" id="gps_tool_id" value="<?php echo $row['gps_tool_id']; ?>">
                            <?php
                        }
                        ?>
                        <input type="hidden" name="country_id" id="country_id" value="<?php echo @$country_id;?>">
                        <div class="header">
							<h5 align="left" style="color: #3380FF;"><strong>Country : <?php echo $country_name; ?></strong></h5>
						</div></br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
								<label for="inputName" class=" col-sm-4 control-label">Name <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" required class="form-control" placeholder="Tool Tracker Name" name="gps_name" value="<?php echo @$row['name'];?>">
									</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
								<label for="inputName" class=" col-sm-4 control-label">UID Number <span class="req-fld">*</span></label>
									<div class="col-sm-5">
										<input type="text" autocomplete="off" required class="form-control" placeholder="UID Number" name="gps_uid" id="gps_uid" value="<?php echo @$row['uid_number'];?>">
									</div>
                                </div>
                            </div>
                        </div>								
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-6">
								<button class="btn btn-primary" type="submit" value="1" name="submit_gps"><i class="fa fa-check"></i> Submit</button>
								<a class="btn btn-danger" href="<?php echo SITE_URL;?>gps_tracking"><i class="fa fa-times"></i> Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>	
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>gps_tracking">
						<div class="col-sm-12">
							
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="gps_name" placeholder="Tool Tracker Name" value="<?php echo @$search_data['gps_name'];?>" class="form-control">
							</div>
							
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="gps_uid" placeholder="UID Number" value="<?php echo @$search_data['gps_uid'];?>" class="form-control" id="gps_uid">
							</div>
							
							<?php
							if($task_access == 3 && @$_SESSION['header_country_id']=='')
							{
								?>
								<div class="col-sm-3">
									<select name="country_id" class="select2" >    
										<option value="">- Country -</option>
										<?php
										foreach($countryList as $row)
										{
											$selected = ($row['location_id']==@$search_data['gps_country'])?'selected="selected"':'';
											echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
										}?>
									</select>
								</div> <?php
							}?>
							<div class="col-sm-3">
								<button type="submit" name="searchtracking" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
								<a href="<?php echo SITE_URL.'gps_tracking';?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>                                
                                <a data-toggle="tooltip" title="Add New" href="<?php echo SITE_URL.'add_gps_tracking';?>" class="btn btn-success"><i class="fa fa-plus"></i></a>
								<button type="submit" data-toggle="tooltip" title="Download" name="download_gps" value="1" formaction="<?php echo SITE_URL.'download_gps';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i></button>  
							</div>
						</div>
					</form>
				</div><br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                            	<th class="text-center"><strong>Tool Tracker Name</strong></th>
                                <th class="text-center"><strong>UID Number</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($gpsResults)>0)
							{
								foreach($gpsResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo $row['name'];?></td>
										<td class="text-center"><?php echo $row['uid_number']; ?></td>
										<td class="text-center"><?php echo $row['country_name']; ?></td>
										<td class="text-center">
                                        <?php
	                                        if($row['status']==1){
	                                        ?>
	                                        <a class="btn btn-default" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_gps_tracking/'.storm_encode($row['gps_tool_id']);?>" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

	                                        <?php } else{?>
	                                        <a class="btn btn-default" style="padding:3px 3px;" href="#" readonly data-toggle="tooltip" title="Activate GPS Tracker to edit"><i class="fa fa-pencil"></i></a>
											<?php }
	                                        if($row['status']==1){
	                                        ?>
	                                        <a class="btn btn-danger" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_gps_tracking/'.storm_encode($row['gps_tool_id']);?>" data-toggle="tooltip" title="Deactivate"><i class="fa fa-trash-o"></i></a>
	                                        <?php
	                                        }
	                                        if($row['status']==2){
	                                        ?>
	                                        <a class="btn btn-info" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_gps_tracking/'.storm_encode($row['gps_tool_id']);?>" data-toggle="tooltip" title="Activate"><i class="fa fa-check"></i></a>
	                                        <?php
	                                        }
	                                        ?>
	                                    </td>
										
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="5" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	is_gps_uid_exist();
	
</script>