<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
	        <?php echo $this->session->flashdata('response'); ?>
			<div class="block-flat">
				<div class="content">
					<div class="row">
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>gps_report_view">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="gps_name" placeholder="Tool Tracker Name" value="<?php echo @$search_data['gps_name'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="uid_number" placeholder="UID Number" value="<?php echo @$search_data['uid_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<button type="submit" name="search" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>

									<a href="<?php echo SITE_URL.'gps_report_view';?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>                                
	                                <button data-toggle="tooltip" onclick="return confirm('Are you sure you want to download?')" title="Download" type="submit" name="download_gps_tracker_report" value="1" formaction="<?php echo SITE_URL.'download_gps_report';?>" class="btn btn-success"><i class="fa fa-cloud-download"></i></button>  
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
								    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_number" placeholder="Tool Number" value="<?php echo @$search_data['tool_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_description" placeholder="Tool Description" value="<?php echo @$search_data['tool_description'];?>" class="form-control">
								</div>
								<?php
								if($task_access == 3 && @$_SESSION['header_country_id']=='')
								{
									?>
									<div class="col-sm-3">
										<select name="country_id" class="select2" >    
											<option value="">- Country -</option>
											<?php
											foreach($countryList as $row)
											{
												$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
												echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
											}?>
										</select>
									</div> <?php
								}?>
							</div>
						</form>
					</div>
					<div class="row">
						<div class="header"></div>
						<table class="table table-bordered" ></table>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
	                            	<th class="text-center"><strong>S.No</strong></th>
	                            	<th class="text-center"><strong>Tool Tracker Name</strong></th>
	                                <th class="text-center"><strong>UID Number</strong></th>
	                                <th class="text-center"><strong>Tool Description</strong></th>
	                                <th class="text-center"><strong>Asset Number</strong></th>
	                                <th class="text-center"><strong>Serial Number</strong></th>
	                                <th class="text-center"><strong>Tool Availability</strong></th>
	                                <th class="text-center"><strong>Country</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(count($gps_results)>0)
								{
									foreach($gps_results as $row)
									{
									?>
										<tr>
											<td class="text-center"><?php echo $sn++;?></td>
											<td class="text-center"><?php echo $row['name'];?></td>
											<td class="text-center"><?php echo $row['uid_number']; ?></td>
											<td class="text-center"><?php if($row['tool_desc']!=''){ echo $row['tool_desc']; } else { echo "--";}?></td>
											<td class="text-center"><?php if($row['asset_number']!=''){ echo $row['asset_number']; } else { echo "--";}?></td>
											<td class="text-center"><?php if($row['serial_number']!=''){ echo $row['serial_number']; } else { echo "--";}?></td>
											<td class="text-center">
	                                        	<?php if($row['asset_id']!='')
	                                        	{
	                                        		echo get_asset_position($row['asset_id']);
	                                        	}
	                                        	else
	                                        	{
	                                        		echo "--";
	                                        	} ?>
		                                    </td>
		                                    <td class="text-center"><?php echo $row['country_name']; ?></td>
										</tr>
							        <?php   
						            }
								} 
								else {?>
									<tr><td colspan="7" align="center"><span class="label label-primary">No Records</span></td></tr>
	                    <?php 	} ?>
							</tbody>
						</table>
	                </div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>          		
				</div>
			</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>