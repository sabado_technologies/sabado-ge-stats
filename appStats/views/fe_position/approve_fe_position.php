<div class="md-modal colored-header custom-width md-effect-9" id="form-primary">
    <div class="md-content">
      <div class="modal-header">
        <h3>Approve FE Position</h3>
      </div>
      <form class="form-horizontal" role="form" action="<?php echo SITE_URL.'approve_fe_position_change';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data" >
  		  <div class="modal-body" style="height: 200px !important;overflow: hidden !important;">
  	      	<div class="row">
  	      		<div class="form-group">
  		        	<label class="col-md-4 control-label"> Old Warehouse :</label>
  		          	<div class="col-md-2">
                    <p class="old_warehouse control-label"></p>
                    <input type="hidden" name="old_sso_id" class="old_sso_id" value="">
                    <input type="hidden" name="position_change_id" class="position_change_id" value="">
  		            </div>
  		        </div>
              <div class="form-group">
                <label for="inputName" class="col-md-4 control-label"> Warehouse :</label>
                <div class="col-md-5">
                	<select name="fe_warehouse" required class="select2" > 
          					<option value="">- Warehouse -</option>
          					<?php
                    foreach($warehouse as $row)
          					{
      	              echo '<option value="'.$row['wh_id'].'" >'.$row['name'].'</option>';
      	    				}
          					?>
          				</select>
		            </div>
              </div>
  	      	</div>
  	    </div> 
        <div class="modal-footer">
            <button class="btn btn-primary" type="submit" value="1" name="submitUser"><i class="fa fa-check"></i> Submit</button>
        </div>    	
      </form>
      
    </div>
</div>
<div class="md-overlay"></div>