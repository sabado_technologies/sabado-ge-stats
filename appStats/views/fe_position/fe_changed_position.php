<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>fe_position_change">
					<div class="row">
						<div class="col-sm-12">
							<label class="col-sm-1 control-label">SSO ID</label>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="sso_id" placeholder="SSO ID" value="<?php echo @$search_data['ssoid'];?>" class="form-control">
							</div>
								<?php if($task_access == 3){?>
			                        <label class="col-sm-1 control-label">Country</label>
		                            <div class="col-sm-3">
		                                <select class="select2" name="country_id" >
		                                    <option value="">- Country -</option>
		                                     <?php
		                                    foreach($countryList as $country)
		                                    {
		                                        $selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
		                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
		                                    }
		                                    ?>
		                                </select>
		                            </div>
                            	<?php } ?>
							<div class="col-sm-4">							
								<button type="submit" name="searchfeposition" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
								<a href="<?php echo SITE_URL.'fe_position_change'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
							</div>
						</div>
					</div>
				</form>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                            	<th class="text-center"><strong>SSO ID</strong></th>
                                <th class="text-center"><strong>Old FE Position</strong></th>
                                <th class="text-center"><strong>New FE Position</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if(count($fe_positionResults)>0)
							{
								foreach($fe_positionResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++; ?></td>
										<td class="text-center"><?php echo $row['sso_id'].' - '.$row['user_name']; ?></td>
										<td class="text-center"><?php echo $row['old_location']; ?></td>
										<td class="text-center"><?php echo $row['new_location']; ?></td>
										<td class="text-center"><?php echo $row['country_name']; ?></td>
										<td class="text-center">
	                                        <a  class="btn btn-primary accept md-trigger" 
	                                        	data-placement="top"
	                                            data-toggle="tooltip" 
	                                            title="Approve" 
		                                        style="padding:3px 3px;"  
		                                        data-modal="form-primary" 
		                                        data-wh_id="<?php echo $row['wh_id']?>"
		                                        data-country_id="<?php echo $row['country_id'] ?>"   data-warehouse="<?php echo $row['warehouse']; ?>"data-position_change_id="<?php echo $row['fpc_id']?>" 
		                                        href="#">
		                                        <i class="fa fa-thumbs-o-up"></i>Approve &nbsp;
		                                    </a>

	                                        <a class="btn btn-danger" 
	                                        data-placement="top"
	                                        data-toggle="tooltip" 
	                                        title="Reject" 
	                                        style="padding:3px 3px;" 
	                                        onclick="return confirm('Are you sure you want to Reject?')"
	                                        href="<?php echo SITE_URL.'reject_fe_position/'.storm_encode($row['fpc_id']);?>"><i class="fa fa-thumbs-o-down"></i>Reject &nbsp;</a>
                                    	</td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
			</div>
		</div>	
		</div>
	</div>
</div>
<div class="md-overlay"></div>
<?php 
if(count($fe_positionResults)>0)
{
	?>
	<!-- <div class="md-modal colored-header custom-width md-effect-9" id="form-primary"> -->
	<div class="modal colored-header" id="form-primary" style="height: 500px; width: 550px;overflow: hidden !important;margin-left: 400px;margin-top: 150px;">
    <div class="md-content">
      <div class="modal-header">
        <h3>Approve FE Position</h3>
      </div>
      <form class="form-horizontal" role="form" action="<?php echo SITE_URL.'approve_fe_position_change';?>" method="post">
            <input type="hidden" name="fpc_id" class="fpc_id" value="">
  		  	<div class="modal-body" style="height: 200px !important;overflow: hidden !important;">
	  	      	<div class="row">
	  	      		<div class="form-group">
	  		        	<label class="col-md-4 control-label"> Old Warehouse :</label>
	  		          	<div class="col-md-3">
	                    	<p class="old_warehouse control-label" style="padding-top: 5px;" align="left"></p>
	  		            </div>
	  		        </div><br>
	              	<div class="form-group">
	                	<label for="inputName" class="col-md-4 control-label">New Warehouse <span class="req-fld">*</span></label>
	                	<div class="col-md-5">
	                		<select name="fe_warehouse" required class="select3 fe_warehouse" style="width: 300px;"> 
	                			<option value="">- New Warehouse -</option>
	          				</select>
			            </div>
	              </div>
	  	      	</div>
  	    </div> 
        <div class="modal-footer">
            <button class="btn btn-primary" type="submit" value="1" name="submitFePosition"><i class="fa fa-check"></i> Submit</button>
            <button type="button" class="btn btn-danger btn-flat md-close please_close" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
        </div>    	
      </form>
      
    </div>
</div>
<div class="md-overlay"></div> <?php
} ?>
<div class="md-overlay"></div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select3').select2();
	});

	$('.please_close').on('click',function(){
		$('#form-primary').modal('hide');
	});

	$('.accept').on('click',function()
	{
		$('#form-primary').modal('show');
		var warehouse = $(this).data("warehouse");
		var wh_id = $(this).data("wh_id");
		var position_change_id = $(this).data("position_change_id");
		var country_id = $(this).data("country_id");
		
		$('.old_warehouse').text(warehouse);
		$('.fpc_id').val(position_change_id);
		$('.country_id').val(country_id);

  		var data = 'wh_id='+wh_id+'&country_id='+country_id;
		$.ajax({
			type:"POST",
			url:SITE_URL+'get_fe_warehouse',
			data:data,
			cache:false,
			success:function(html)
			{
				$(".fe_warehouse").html(html);
				$('.select3').select2('destroy'); 
	    		$('.select3').select2();
			}
		});
	});
</script>