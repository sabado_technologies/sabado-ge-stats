<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form method="post" action="<?php echo $form_action;?>" parsley-validate novalidate class="form-horizontal">
						<?php
                        if($flg==2){
                            ?>
                            <input type="hidden" name="encoded_id" value="<?php echo storm_encode($branch_row['wh_id']);?>">
                            <input type="hidden" name="branch_id" id="branch_id" value="<?php echo @$branch_row['wh_id']?>">

                            <?php
                        }
                        ?>  
                        <div class="header">
                            <h5 align="left" style="color: #3380FF;"><strong>Country : <?php echo $country_name; ?></strong></h5>
                        </div></br>
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Warehouse <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<input type="text" autocomplete="off" required id="branch_name" class="form-control" placeholder="Warehouse Name" name="name" value="<?php echo @$branch_row['name'];?>">
										</div>
                                    </div>
                                </div>
                                <input type="hidden" name="c_id" id="country_id" value="<?php echo @$country_id;?>">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">Warehouse Code <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" autocomplete="off" size="10" maxlength="10" required="" id="wh_code" class="form-control " placeholder="Warehouse Code" name="wh_code" value="<?php echo @$branch_row['wh_code'];?>">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-4 control-label">Area <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                            <select name="area" required class="select2 area"> 
                                                <option value="">-Select Area-</option>
                                                <?php 
                                               
                                                    foreach($area as $are)
                                                    {
                                                        $selected = "";
                                                        if($are['location_id']== $branch_row['location_id'])
                                                            { 
                                                                $selected='selected';
                                                            }
                                                        echo '<option value="'.$are['location_id'].'" '.$selected.' >'.$are['name'].'</option>';
                                                    }
                                              
                                                ?>
                                            </select> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">Email ID <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="email" autocomplete="off" required="" class="form-control" placeholder="Email id" name="contact_name" value="<?php echo @$branch_row['email'];?>">
                                        </div>
                                    </div>
                                </div>
                            </div>														
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">Managed By <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" autocomplete="off" required="" class="form-control" placeholder="Managed By" name="managed_by" value="<?php echo @$branch_row['managed_by'];?>">
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">Contact Number <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" autocomplete="off" maxlength="20" required="" class="form-control" placeholder="Contact Number" name="contact_no" value="<?php echo @$branch_row['phone'];?>">
                                        </div>
                                    </div>
                                </div> 
                            </div>			
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">GST Number </label>
                                        <div class="col-sm-6">
                                            <input type="text" autocomplete="off"  maxlength="20"  class="form-control" placeholder="GST Number" name="gst_number" value="<?php echo @$branch_row['gst_number'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">PAN Number </label>
                                        <div class="col-sm-6">
                                            <input type="text" autocomplete="off" maxlength="20" class="form-control" placeholder="PAN Number" name="pan_no" value="<?php echo @$branch_row['pan_number'];?>">
                                        </div>
                                    </div>
                                </div>                                
                            </div>	
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">Tin Number</label>
                                        <div class="col-sm-6">
                                            <input type="text" autocomplete="off"  maxlength="20" class="form-control" placeholder="Tin Number" name="tin_number" value="<?php echo @$branch_row['tin_number'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">CST Number</label>
                                        <div class="col-sm-6">
                                            <input type="text" autocomplete="off"  maxlength="20" class="form-control" placeholder="CST Number" name="cst_number" value="<?php echo @$branch_row['cst_number'];?>">
                                        </div>
                                    </div>
                                </div>
                            </div>	
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">Address1 <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                           <textarea required class="form-control" name="address_1"><?php echo @$branch_row['address1'];?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">Address2 <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                            <textarea  class="form-control" required name="address_2"><?php echo @$branch_row['address2'];?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">City <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                           <textarea class="form-control" required name="address_3"><?php echo @$branch_row['address3'];?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">State <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                            <textarea  class="form-control" required name="address_4"><?php echo @$branch_row['address4'];?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="inputName" class="col-sm-4 control-label">Pin Code <span class="req-fld">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" autocomplete="off" size="15" maxlength="15" required="" class="form-control" placeholder="Pin Code" name="pin_no" value="<?php echo @$branch_row['pin_code'];?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive"><br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><strong>Contact</strong></th>
                                            <th class="text-center"><strong>Name</strong></th>
                                            <th class="text-center"><strong>Contact Number</strong></th>
                                            <th class="text-center"><strong>Email</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">Contact Person1 <span class="req-fld">*</span></td>
                                            <td class="text-center"><input type="text" autocomplete="off" required="" id="branch_name" class="form-control" placeholder="Contact Person Name" name="info_name[]" required value="<?php echo @$contact_info1[0];?>"></td>
                                            <td class="text-center"><input type="text" autocomplete="off" size="10" maxlength="15" required="" class="form-control only-numbers" placeholder="Contact Number" required name="info_number[]" value="<?php echo @$contact_info1[1]?>"></td>
                                            <td class="text-center"><input type="email" autocomplete="off" required class="form-control" placeholder="Email" name="info_mail[]" required value="<?php echo @$contact_info1[2];?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">Contact Person2</td>
                                            <td><input type="text" autocomplete="off"  id="branch_name" class="form-control" placeholder="Contact Person Name" name="info_name[]" value="<?php if(@$contact_info2[0]!='') {echo @$contact_info2[0]; } else {echo "";} ?>"></td>
                                            <td><input type="text" autocomplete="off" size="10" maxlength="15"  class="form-control only-numbers" placeholder="Contact Number" name="info_number[]" value="<?php if(@$contact_info2[1]!='') {echo @$contact_info2[1]; } else {echo "";} ?>"></td>
                                            <td><input type="email" autocomplete="off"  class="form-control" placeholder="Email" name="info_mail[]" value="<?php if(@$contact_info2[2]!='') {echo @$contact_info2[2]; } else {echo "";} ?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">Contact Person3</td>
                                            <td><input type="text" autocomplete="off"  id="branch_name" class="form-control" placeholder="Contact Person Name" name="info_name[]" value="<?php if(@$contact_info3[0]!='') {echo @$contact_info3[0]; } else {echo "";} ?>"></td>
                                            <td><input type="text" autocomplete="off" size="10" maxlength="15"  class="form-control only-numbers" placeholder="Contact Number" name="info_number[]" value="<?php if(@$contact_info3[1]!='') {echo @$contact_info3[1]; } else {echo "";} ?>"></td>
                                            <td><input type="email" autocomplete="off"  class="form-control" placeholder="Email" name="info_mail[]" value="<?php if(@$contact_info3[2]!='') {echo @$contact_info3[2]; } else {echo "";} ?>"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>								
							<div class="form-group"><br>
								<div class="col-sm-offset-5 col-sm-6">
									<button class="btn btn-primary" onclick="return confirm('Are you sure you want to Submit?')"  type="submit" value="1" name="submit_branch"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>branch"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>branch">
						<div class="col-sm-12">
                            <div class="col-sm-3">
                                <input type="text" autocomplete="off" name="wh_code" value="<?php echo @$search_data['wh_code'];?>" class="form-control" placeholder="Warehouse Code">
                            </div>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="branch_name" value="<?php echo @$search_data['name'];?>" class="form-control" placeholder="Warehouse Name">
							</div>
                            <?php if($task_access == 3 && $this->session->userdata('header_country_id')==''){?>
                            <div class="col-sm-3">
                                <select class="select2" name="country_id">
                                    <option value="">- Country -</option>
                                     <?php
                                    foreach($countryList as $country)
                                    {
                                        $selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
                                        echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php } ?>
							<div class="col-sm-3">
                                <button type="submit" name="searchbranch" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
                                <a href="<?php echo SITE_URL.'branch';?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>                                
                                <a href="<?php echo SITE_URL.'add_branch';?>" class="btn btn-success" data-toggle="tooltip" title="Add New"><i class="fa fa-plus"></i></a>
                                <button type="submit" data-toggle="tooltip" title="Download" name="download_branch" value="1" formaction="<?php echo SITE_URL.'download_branch';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i></button>  
                            </div>
                        </div>
					</form>
				</div><br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center" width="10"><strong>Wh Code</strong></th>
                                <th class="text-center"><strong>Warehouse</strong></th>
                                <th class="text-center"><strong>Managed By</strong></th>
                                <th class="text-center"><strong>Contact No</strong></th>
                                <th class="text-center"><strong>GST Number</strong></th>
                                <th class="text-center"><strong>PAN Number</strong></th>
                                <th class="text-center"><strong>Location</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($branch_results)>0)
							{
								foreach($branch_results as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
                                        <td class="text-center"><?php echo $row['wh_code']; ?></td>
										<td class="text-center"><?php echo $row['name']; ?></td>
                                        <td class="text-center"><?php echo $row['managed_by']; ?></td>
										<td class="text-center"><?php echo $row['phone']; ?></td>
										<td class="text-center"><?php echo $row['gst_number']; ?></td>
										<td class="text-center"><?php echo $row['pan_number']; ?></td>
                                        <td class="text-center"><?php echo $row['location_name']; ?></td>  
                                        <td class="text-center"><?php echo $row['country_name']; ?></td>  
										<td class="text-center">
										<?php
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-default" data-toggle="tooltip" title="Edit" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_branch/'.storm_encode($row['wh_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php
                                        }else{?>NA
                                       <?php }?>
                                        <?php
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" data-toggle="tooltip" title="Deactivate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_branch/'.storm_encode($row['wh_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" data-toggle="tooltip" title="activate" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_branch/'.storm_encode($row['wh_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
									</tr>
						        <?php   
					            }
							} 
							else {?>
								<tr><td colspan="10" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>