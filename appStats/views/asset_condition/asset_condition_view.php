<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			<?php
			if($flg==2)
			{
			    ?>
			    <form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
			    <div class="row"> 
			    	<div class="block-flat">
						<div class="content">
			    			<input type="hidden" name="asset_id" value="<?php echo storm_encode(@$asset_id);?>">
							<div class="col-sm-12 col-md-12">
								<div class="header">
									<p align="center">Asset Number : <strong><?php echo $asset_number; ?></strong></p>
									<p align="center">Country : <strong><?php echo $country_name; ?></strong></p>
								</div>
							</div>
												
							<div class="col-sm-12 col-md-12">
								<div class="table-responsive">
									<table class="table health_table">
										<thead>
											<tr>
												<th class="text-center"><strong>Serial No</strong></th>
				                                <th class="text-center"><strong>Tool Number</strong></th>
				                                <th class="text-center"><strong>Tool Description</strong></th>
				                                <th class="text-center"><strong>Tool Level</strong></th>
				                                <th class="text-center"><strong>Qty</strong></th>
				                              	<th class="text-center" width="33%"><strong>Tool Health</strong></th>
				                              	<th class="text-center" ><strong>Remarks</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(count(@$asset_details)>0)
											{	
												foreach(@$asset_details as $row)
												{
												?>
<tr class="asset_row">
	<td class="text-center"><?php echo $row['serial_number']; ?></td>
	<td class="text-center"><?php echo $row['part_number']; ?></td>
	<td class="text-center"><?php echo $row['part_description']; ?></td>
	<td class="text-center"><?php echo $row['part_level_name']; ?></td>
	<td class="text-center"><?php echo $row['quantity']; ?></td>
	
	<td class="text-center">
		 <div class="col-sm-12 custom_icheck">
		    <label class="radio-inline"> 
		        <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
		            <input type="radio" class="asset_condition_id" value="1" name="asset_condition_id[<?php echo $row['part_id']; ?>]" checked style="position: absolute; opacity: 0;">
		            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		        </div> 
		       	Good
		    </label>
		    <label class="radio-inline"> 
		        <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
		            <input type="radio" class="asset_condition_id" value="2" name="asset_condition_id[<?php echo $row['part_id']; ?>]"  style="position: absolute; opacity: 0;">
		            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		        </div> 
		        Defective
		    </label>
		    <label class="radio-inline"> 
		        <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
		            <input type="radio" class="asset_condition_id" value="3" name="asset_condition_id[<?php echo $row['part_id']; ?>]" style="position: absolute; opacity: 0;">
		            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
		        </div> 
		        Missing
		    </label>
		</div>
	</td>
	<td>
		<div class="textbox">
			<textarea class="form-control textarea" name="remarks[<?php echo $row['part_id']; ?>]"></textarea> 	
		</div>
	</td>
</tr>
										<?php   }
											} else {?>
												<tr><td colspan="7" align="center"><span class="label label-primary">No Records Found</span></td></tr>
				                    <?php 	} ?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-12"><br>
									<label class="col-sm-4 control-label">Remarks<span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<textarea class="form-control" name="type_remarks" required></textarea>
									</div>
								</div>
							</div>
		            		<div class="form-group"><br>
								<div class="col-sm-offset-5 col-sm-5">
									<button class="btn btn-primary submit_btn" type="submit" onclick="return confirm('Are you sure you want to Submit?')" name="submit_tool"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>asset_condition_check"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<?php   }
		if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'asset_condition_check';?>">
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="part_number" placeholder="Tool Number" value="<?php echo @$searchParams['part_number'];?>"  class="form-control">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$searchParams['asset_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="part_description" placeholder="Tool Description" value="<?php echo @$searchParams['part_description'];?>"  class="form-control">
								</div>
	                            <div class="col-md-3">							
									<button type="submit" name="searchasset" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'asset_condition_check'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
								</div>
							</div>
						</div>
						<div class="row ">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
								    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$searchParams['serial_no'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
									<select name="modality_id" class="select2" > 
										<option value="">- Modality -</option>
										<?php 
											foreach($modality_details as $mod)
											{
												$selected = ($mod['modality_id']==@$searchParams['modality_id'])?'selected="selected"':'';
												echo '<option value="'.$mod['modality_id'].'" '.$selected.'>'.$mod['name'].'</option>';
											}
										?>
									</select>
								</div>
								<?php if($task_access == 2 || $task_access == 3 || isset($_SESSION['whsIndededArray'])){?>
								<div class="col-sm-3 ">
									<select name="wh_id"  class="select2"> 
										<option value="">- Warehouse -</option>
										<?php 
											foreach($whList as $wh)
											{
												$selected = ($wh['wh_id']==$searchParams['whid'])?'selected="selected"':'';
												echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_code'].' -('.$wh['name'].')</option>';
											}
										?>
									</select>
								</div>
								<?php } 
								if($task_access == 3 && $_SESSION['header_country_id']==''){?>
								<div class="col-sm-3">
									<select class="select2" name="country_id" >
										<option value="">- Country -</option>
										 <?php
										foreach($countryList as $country)
										{
											$selected = ($country['location_id']==$searchParams['country_id'])?'selected="selected"':'';
											echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
										}
										?>
									</select>
								</div>
								<?php } ?>
								
							</div>
						</div>
					</form>
					<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Tool Number</strong></th>
								<th class="text-center"><strong>Tool Description</strong></th>
								<th class="text-center"><strong>Asset Number</strong></th>
								<th class="text-center"><strong>Serial Number</strong></th>
								<th class="text-center"><strong>Modality</strong></th>
								<th class="text-center"><strong>Tool Type</strong></th>
								<th class="text-center"><strong>Asset Status</strong></th>
								<th class="text-center"><strong>Tool Availability</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($assetResults)>0)
							{
								foreach($assetResults as $row)
								{
									$var = get_asset_position_with_flag($row['asset_id']);
									$flag = $var[1]; ?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['part_number'];?></td>
										<td class="text-center"><?php echo @$row['part_description'];?></td>
										<td class="text-center"><?php echo @$row['asset_number'];?></td>
										<td class="text-center"><?php echo @$row['serial_number'];?></td>
										<td class="text-center"><?php echo @$row['modality_name'];?></td>										
										<td class="text-center"><?php echo @$row['tool_type_name'].' '.'Tool';?></td>
										<td class="text-center"><?php echo @$row['asset_status'];?></td>
										<td class="text-center"><?php echo get_asset_position($row['asset_id']);?></td>
										<td class="text-center"><?php echo @$row['country_name'];?></td>
										<td class="text-center">			
											<?php if($flag == 1){ ?>						
											<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Give Defective Details"  href="<?php echo SITE_URL.'edit_asset_condition/'.storm_encode($row['asset_id']);?>"><i class="fa fa-pencil"></i></a> 
											<?php } ?>
                                    	</td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="11" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			<?php 
			} ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>