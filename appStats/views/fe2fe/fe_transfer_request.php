<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>fe_transfer_request">
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <?php if($task_access==2 || $task_access==3)
                                    {
                                        ?>
                                        <div class="col-sm-3">
                                            <select name="from_sso" class="select2 main_status supplier_id" > 
                                                <option value="">- From SSO -</option>
                                                <?php
                                                foreach($users as $row)
                                                {
                                                    $selected = ($row['sso_id']==@$searchParams['from_sso'])?'selected="selected"':'';
                                                    echo '<option value="'.$row['sso_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                                }?>
                                            </select>
                                        </div> <?php
                                    } ?>
                                
                                <?php
                                    if($task_access==3 && @$_SESSION['header_country_id']=='')
                                    {
                                        ?>
                                        <div class="col-sm-3">
                                            <select name="country_id" class="main_status select2" >    <option value="">- Select Country -</option>
                                                <?php
                                                foreach($country_list as $row)
                                                {
                                                    $selected = ($row['location_id']==@$searchParams['country_id'])?'selected="selected"':'';
                                                    echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                                }?>
                                            </select>
                                        </div>

                                         <?php
                                    }?> 
                            </div>
                            <div class="col-sm-12 form-group">
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="from_return_date" readonly placeholder="Return Date (From SSO)" value="<?php echo @$searchParams['from_return_date'];?>" class="form-control date" style="cursor:hand;background-color: #ffffff">
                                </div>
                                 <div class="col-sm-3">
                                    <select name="to_sso" class="select2" > 
                                        <option value="">- To SSO -</option>
                                        <?php
                                        foreach($users as $row)
                                        {
                                            $selected = ($row['sso_id']==@$searchParams['to_sso'])?'selected="selected"':'';
                                            echo '<option value="'.$row['sso_id'].'" '.$selected.'>'.$row['name'].'</option>';
                                        }?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="to_request_date" readonly placeholder="Request Date (To SSO)" value="<?php echo @$searchParams['to_request_date'];?>" class="form-control date" style="cursor:hand;background-color: #ffffff">
                                </div>
                                <div class="col-sm-3">    
                                        <button type="submit" name="fe_transfer_request" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                    <a href="<?php echo SITE_URL.'fe_transfer_request'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                </div>
                            </div>
                            
                        </div>
					</form>
				<div class="col-sm-12 col-md-12">
                    <div class="table-responsive" >
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%"></th>
                                    <th class="text-center" width="4%"><strong>S.No</strong></th>
                                    <th class="text-center"><strong>From SSO</strong></th>
                                    <th class="text-center"><strong>Return Date</strong></th>
                                    <th class="text-center"><strong>To SSO</strong></th>
                                    <th class="text-center"><strong>Requested Date</strong></th>
                                    <th class="text-center"><strong>Country</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count(@$fe_transfer_result)>0)
                                {
                                    foreach(@$fe_transfer_result as $row)
                                    { 
                                    ?>
                                        <tr>
                                            <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details"></td>
                                            <td class="text-center"><?php echo $sn++; ?></td>
                                            <td class="text-center"><?php echo $row['from_sso']; ?></td>
                                            <td class="text-center"><?php echo indian_format($row['return_date']); ?></td>
                                            <td class="text-center"><?php echo $row['to_sso']; ?></td>
                                            <td class="text-center"><?php echo indian_format($row['request_date']); ?></td>
                                            <td class="text-center"><?php echo $row['country_name']; ?></td>                                  
                                            
                                        </tr>
                                        <?php if(count(@$asset_data[$row['tool_order_id']])>0)
                                        {  ?>
                                        <tr class="details">
                                                <td  colspan="11"> 
                                                    <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                                        <thead>
                                                            <th class="text-center">Tool Number</th>
                                                            <th class="text-center">Tool Description</th>
                                                            <th class="text-center">Quantity</th>
                                                        </thead>
                                                        <tbody>
                                                   <?php foreach(@$asset_data[$row['tool_order_id']] as $value)
                                                    { 
                                                    ?>
                                                        <tr class="asset_row">
                                                            <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                            <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                            <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                            
                                                        </tr>
                                                    <?php     
                                                    } ?>
                                                        </tbody>
                                                    </table>
                                            </td>
                                        </tr><?php
                                        }
                                        else
                                        { ?>
                                            <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                        <?php 
                                        }
                                    }
                                
                                } else {?>
                                    <tr><td colspan="11" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                        <?php   } ?>
                            </tbody>
                        </table>
                    </div>
                </div> 
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>          		
			</div>
		</div>	
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>