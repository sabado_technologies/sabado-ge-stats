<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>fe2_fe_approval">
						<div class="row">
							<div class="col-sm-12">
								<!-- <label class="col-sm-2 control-label">Order Number</label> -->
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="FE2 Order Number" value="<?php echo @$searchParams['order_number'];?>"  class="form-control">
								</div>
								<!-- <label class="col-sm-2 control-label">Request Date</label> -->
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="deploy_date" readonly placeholder="Request Date" value="<?php echo @$searchParams['deploy_date'];?>" id="dateFrom"  class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								
								<!-- <label class="col-sm-2 control-label">Return Date</label> -->
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="return_date" readonly placeholder="Return Date To" value="<?php echo @$searchParams['return_date'];?>" id="dateTo" class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">							
										<button type="submit" name="fe2_fe_approval" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
										<a href="<?php echo SITE_URL.'fe2_fe_approval'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
									</div>	
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-sm-12">
								
								<!-- <label class="col-sm-2 control-label">Order Type</label> -->
								<div class="col-sm-3">
									<select name="order_delivery_type_id" class="select2"> 
										<option value="">- Order Type -</option>
										<?php 
											foreach($order_type as $ot)
											{
												$selected = ($ot['order_delivery_type_id']==@$searchParams['order_delivery_type_id'])?'selected="selected"':'';
												echo '<option value="'.$ot['order_delivery_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
											}
										?>
									</select>
								</div>
								<?php if($task_access==2 || $task_access==3)
									{
										?>
										<div class="col-sm-3">
											<?php 
                                            $d['name']        = 'fe2_fe_sso_id';
                                            $d['search_data'] = @$searchParams['fe2_fe_sso_id'];
                                            $this->load->view('sso_dropdown_list',$d); 
                                            ?>
										</div> <?php
									} ?>
								<?php
									if($task_access==3 && @$_SESSION['header_country_id']=='')
									{
										?>
										<div class="col-sm-3">
											<select name="fe2_fe_country_id" class="main_status select2" >    <option value="">- Country -</option>
												<?php
												foreach($country_list as $row)
												{
													$selected = ($row['location_id']==@$searchParams['fe2_fe_country_id'])?'selected="selected"':'';
													echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
												}?>
											</select>
										</div>

										 <?php
									}?>	
									
								
								</div>
							</div>	
						</div>
					<!-- 	<div class="row">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
							<div class="col-sm-12">
								<div class="col-sm-4 col-md-4">				
									
								</div>	
								<div class="col-sm-5 col-md-5"></div>
								<div class="col-sm-3 col-md-3">							
									<button type="submit" name="fe2_fe_approval" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'fe2_fe_approval'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
								</div>		
							</div>
						</div> -->
					</form>
					<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>From SSO</strong></th>
								<th class="text-center"><strong>To SSO</strong></th>
								<th class="text-center"><strong>Return No.</strong></th>
								<th class="text-center"><strong>Return Type</strong></th>
								<th class="text-center"><strong>Order Type</strong></th>
								<th class="text-center"><strong>Request Date</strong></th>
								<th class="text-center"><strong>Return Date</strong></th>					
								<th class="text-center"><strong>Country</strong></th>													
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>		
										<td class="text-center"><?php echo @$row['from_sso'];?></td>
										<td class="text-center"><?php echo @$row['to_sso'];?></td>
										<td class="text-center"><?php echo @$row['return_number'];?></td>
										<td class="text-center"><?php echo @$row['return_type'];?></td>
										<td class="text-center"><?php echo @$row['order_type'];?></td>
										<td class="text-center"><?php echo indian_format(@$row['deploy_date']);?></td>
										<td class="text-center"><?php echo indian_format(@$row['return_date']);?></td>
										<td class="text-center"><?php echo @$row['country_name'];?></td>
										<td class="text-center">										
                                        <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Approve FE to FE Transfer"  href="<?php echo SITE_URL.'fe2_fe_approval_details/'.storm_encode($row['rto_id']);?>"><i class="fa fa-pencil"></i></a>
                                       
                                    </td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="11" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
