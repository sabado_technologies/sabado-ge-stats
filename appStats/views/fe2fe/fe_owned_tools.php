<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
        if(@$flg ==1 ){ ?>
            <div class="block-flat">
                <table class="table table-bordered"></table>
                <div class="content">
                    <form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>toolsAvailabilityWithFE">
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['order_number'];?>" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <?php 
                                    $d['name']        = 'ser_sso_id';
                                    $d['search_data'] = @$searchParams['ser_sso_id'];
                                    $this->load->view('sso_dropdown_list',$d); 
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$searchParams['asset_number'];?>" class="form-control">
                                </div>
                                <div class="col-sm-3 col-md-3">    
                                    <button type="submit" name="fe_owned_search" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                    <a href="<?php echo SITE_URL.'fe_owned_tools'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <div class="col-sm-2">
                                    <input type="text" autocomplete="off" name="part_number" placeholder="Tool Number" value="<?php echo @$searchParams['part_number'];?>" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" autocomplete="off" name="part_description" placeholder="Tool Description" value="<?php echo @$searchParams['part_description'];?>" class="form-control">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-sm-12 col-md-12">
                        <div class="table-responsive" style="margin-top: 10px;">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center"><strong>S.No</strong></th>
                                        <th class="text-center"><strong>Order Number</strong></th>
                                        <th class="text-center"><strong>SSO</strong></th>
                                        <th class="text-center"><strong>Asset Number </strong></th>
                                        <th class="text-center"><strong>Tool Number</strong></th>
                                        <th class="text-center"><strong>Tool Description</strong></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count(@$orderResults)>0)
                                    {   $sn = 1;
                                        foreach(@$orderResults as $row)
                                        { 
                                        ?>
                                            <tr>
                                                <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand" ></td>
                                                <td class="text-center"><?php echo $sn++; ?></td>
                                                <td class="text-center"><?php echo $row['order_number']; ?></td>
                                                <td class="text-center"><?php echo $row['sso']; ?></td>
                                                <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                
                                            </tr>
                                            <?php if(count(@$asset_data[$row['asset_id']])>0)
                                            {  ?>
                                            <tr class="details">
                                                <td></td>
                                                    <td  colspan="6"> 
                                                        <table  class="table">
                                                            <thead>
                                                                <th class="text-center">Tool Number</th>
                                                                <th class="text-center">Tool Description</th>
                                                                <th class="text-center">Serial Number</th>
                                                                <th class="text-center">Tool Level </th>
                                                                <th class="text-center">Quantity</th>


                                                            </thead>
                                                            <tbody>
                                                       <?php foreach(@$asset_data[$row['asset_id']] as $value)
                                                        { 
                                                        ?>
                                                            <tr class="asset_row">
                                                                <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                                <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_level_name']; ?></td>
                                                                <td class="text-center"><?php echo $value['parts_quantity']; ?></td>
                                                                
                                                            </tr>
                                                        <?php     
                                                        } ?>
                                                            </tbody>
                                                        </table>
                                                </td>
                                            </tr><?php
                                            }
                                            else
                                            { ?>
                                                <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                            <?php 
                                            }
                                        }
                                    
                                    } else {?>
                                        <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                            <?php   } ?>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-left">
                                <div class="dataTables_info" role="status" aria-live="polite">
                                    <?php echo @$pagermessage; ?>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="dataTables_paginate paging_bootstrap_full_number">
                                    <?php echo @$pagination_links; ?>
                                </div>
                            </div>
                        </div> 
                    </div>                  
                </div>
            </div>

        <?php  }
       
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
    		<div class="block-flat">
    			<table class="table table-bordered"></table>
    			<div class="content">
    					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>fe_owned_tools">
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['order_number'];?>" class="form-control">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="part_number"  placeholder="Tool Number" value="<?php echo @$searchParams['part_number'];?>" class="form-control">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="part_description" placeholder="Tool Description" value="<?php echo @$searchParams['part_description'];?>" class="form-control">
                                    </div>
                                    <div class="col-sm-3 col-md-3">    
                                        <button type="submit" name="fe_owned_search" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                        <a href="<?php echo SITE_URL.'fe_owned_tools'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$searchParams['asset_number'];?>" class="form-control">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$searchParams['serial_no'];?>" class="form-control">
                                    </div>
                                    <?php if($task_access==2 || $task_access==3) { ?>
                                    <div class="col-sm-3">
                                        <?php 
                                        $d['name']        = 'users_id';
                                        $d['search_data'] = @$searchParams['users_id'];
                                        $this->load->view('sso_dropdown_list',$d); 
                                        ?>
                                    </div>
                                    <?php } ?>
                                    <?php if( ($task_access == 3 && @$_SESSION['header_country_id']=='' ) || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']=='') ){?>
                                <div class="col-sm-3">
                                    <select class="select2" name="myi_country_id" >
                                        <option value="">- Country -</option>
                                         <?php
                                        foreach($countryList as $country)
                                        {
                                            $selected = ($country['location_id']==$searchParams['myi_country_id'])?'selected="selected"':'';
                                            echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php } ?>
                                </div>
                            </div>
    					</form>
    				<div class="col-sm-12 col-md-12">
                        <div class="table-responsive" style="margin-top: 10px;">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center"><strong>S.No</strong></th>
                                        <th class="text-center"><strong>Order Number</strong></th>
                                        <th class="text-center"><strong>Asset Number </strong></th>
                                        <th class="text-center"><strong>Tool Number</strong></th>
                                        <th class="text-center"><strong>Tool Description</strong></th>
                                        <th class="text-center"><strong>Cal Certificate</strong></th>
                                        <th class="text-center"><strong>Cal Due Date</strong></th>
                                        <th class="text-center"><strong>Country</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count(@$orderResults)>0)
                                    {
                                        foreach(@$orderResults as $row)
                                        { 
                                            ?>
                                            <tr>
                                                <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand" ></td>
                                                <td class="text-center"><?php echo $sn++; ?></td>
                                                <td class="text-center"><?php echo $row['order_number']; ?></td>
                                                <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                <?php if($row['cal_type_id'] == 1){ ?>
                                                <td class="text-center"> <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View Calibration Certificate"  href="<?php echo SITE_URL.'view_calibration_certificates_list/'.storm_encode($row['asset_id']).'/'.storm_encode(2);?>"><i class="fa fa-eye"></i></a></td>
                                                <?php } else { ?>
                                                <td class="text-center">--</td>
                                                <?php } ?>
                                                <td class="text-center"><?php echo ($row['cal_type_id'] == 1)?indian_format($row['cal_due_date']):'--'; ?></td>
                                                <td class="text-center"><?php echo get_country_location($row['country_id']);?></td>
                                                
                                            </tr>
                                            <?php if(count(@$asset_data[$row['asset_id']])>0)
                                            {  ?>
                                            <tr class="details">
                                                <td></td>
                                                    <td  colspan="8"> 
                                                        <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                                            <thead>
                                                                <th class="text-center">Tool Number</th>
                                                                <th class="text-center">Tool Description</th>
                                                                <th class="text-center">Serial Number</th>
                                                                <th class="text-center">Tool Level </th>
                                                                <th class="text-center">Quantity</th>
                                                            </thead>
                                                            <tbody>
                                                       <?php foreach(@$asset_data[$row['asset_id']] as $value)
                                                        { 
                                                        ?>
                                                            <tr class="asset_row">
                                                                <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                                <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                <td class="text-center"><?php echo $value['part_level_name']; ?></td>
                                                                <td class="text-center"><?php echo $value['parts_quantity']; ?></td>
                                                                
                                                            </tr>
                                                        <?php     
                                                        } ?>
                                                            </tbody>
                                                        </table>
                                                </td>
                                            </tr><?php
                                            }
                                            else
                                            { ?>
                                                <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                            <?php 
                                            }
                                        }
                                    
                                    } else {?>
                                        <tr><td colspan="9" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                            <?php   } ?>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    <div class="row">
                    	<div class="col-sm-12">
    	                    <div class="pull-left">
    	                        <div class="dataTables_info" role="status" aria-live="polite">
    	                            <?php echo @$pagermessage; ?>
    	                        </div>
    	                    </div>
    	                    <div class="pull-right">
    	                        <div class="dataTables_paginate paging_bootstrap_full_number">
    	                            <?php echo @$pagination_links; ?>
    	                        </div>
    	                    </div>
                    	</div> 
                    </div>          		
    			</div>
    		</div>	
	<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>