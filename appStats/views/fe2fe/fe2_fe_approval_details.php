<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>		
            <div class="row"> 
                <div class="col-sm-12 col-md-12">
                    <div class="block-flat">
                        <div class="content">
                            <form method="post" action="<?php echo SITE_URL.'insert_fe2_fe_approval';?>" parsley-validate novalidate class="form-horizontal" enctype="multipart/form-data">
                                <input type="hidden" name="fe1_tool_order_id" value="<?php echo $return_assets[0]['fe1_tool_order_id']; ?>"> 
                                <input type="hidden" name="fe2_tool_order_id" value="<?php echo $return_info['tool_order_id']; ?>"> 
                                <input type="hidden" name="return_type_id" value="<?php echo $return_info['return_type_id']; ?>"> 
                                <input type="hidden" name="return_order_id" value="<?php echo $return_info['return_order_id']; ?>"> 
                                <input type="hidden" value="<?php echo $return_assets[0]['order_status_id'];?>" name="order_status_id" > 
                                <input type="hidden" name="rto_id" value="<?php echo $rto_id; ?>">
                              
                                <div class="col-sm-12 col-md-12">
                        <div class="row">                           
                            <div class="header">
                                        <h5 align="center"><strong>Pickup Details</strong></h5>
                                    </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>Return Number :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['return_number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong><u>From Address :</u></strong></td>
                                            <td class="data-item"></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>SSO Detail :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['sso_id'].' - '.$rrow['user_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address1 :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address1']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address2 :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address2']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address3']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>State :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['address4']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['location_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Pin Code :</strong></td>
                                            <td class="data-item"><?php echo @$rrow['zip_code']?></td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="no-border">
                                    <tbody class="no-border-x no-border-y">
                                        <tr>
                                            <td class="data-lable"><strong>Expected Arrival Date :</strong></td>
                                            <td class="data-item"><?php echo indian_format(@$rrow['expected_arrival_date']);?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong><u>To Address :</u></strong></td>
                                            <td class="data-item"></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>C/o :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['to_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address1 :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address1']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Address2 :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address2']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address3']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>State :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['address4']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>City :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['location_name']?></td>
                                        </tr>
                                        <tr>
                                            <td class="data-lable"><strong>Pin Code :</strong></td>
                                            <td class="data-item"><?php echo @$trrow['pin_code']?></td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                                <div class="col-sm-12 col-md-12"><br>
                                    <div class="row">                           
                                        <div class="header">
                                            <h5 align="center"><strong>Approval Needed Tools</strong></h5>
                                        </div>
                                    </div>
                                    <div class="table-responsive" style="margin-top: 10px;">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"></th>
                                                    <th class="text-center"><strong>S.No</strong></th>
                                                    <th class="text-center"><strong>Tool Number</strong></th>
                                                    <th class="text-center"><strong>Tool Description</strong></th>
                                                    <th class="text-center"><strong>Asset Number</strong></th><th class="text-center"><strong>Serial Number</strong></th>
                                                    <th class="text-center"><strong>Status</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if(count(@$return_parts)>0)
                                                {   $sn = 1;
                                                    foreach(@$return_parts as $oah_id => $row)
                                                    { 
                                                    ?>
                                                        <tr>
                                                          <input type="hidden" name="assetAndOrderedAsset[<?php echo $row['asset_id'];?>]" value="<?php echo $row['ordered_asset_id'];?>" >   
                                                         <input type="hidden" name="oah_id[<?php echo $oah_id;?>]" value="<?php echo $row['ordered_asset_id'];?>" >
                                                            <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand"></td>
                                                            <td class="text-center"><?php echo $sn++; ?></td>
                                                            <td class="text-center"><?php echo $row['part_number']; ?></td>
                                                            <td class="text-center"><?php echo $row['part_description']; ?></td>
                                                            <td class="text-center"><?php echo $row['asset_number']; ?></td>
                                                            <td class="text-center"><?php echo $row['serial_number']; ?></td>
                                                            <td class="text-center hidden">     <div class="col-sm-12 custom_icheck">
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_status" value="1" name="oah_condition_id[<?php echo $row['ordered_asset_id']; ?>]" checked style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Received
                                                                </label>
                                                                <label class="radio-inline"> 
                                                                    <div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                        <input type="radio" class="asset_status" value="2" name="oah_condition_id[<?php echo $row['ordered_asset_id']; ?>]" style="position: absolute; opacity: 0;">
                                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                    </div> 
                                                                    Not Received
                                                                </label>
                                                                
                                                                </div>
                                                            </td>
                                                            <td class="text-center"> <?php echo $row['asset_status']; ?></td>
                                                        </tr>
                                                    <?php if(count(@$row['health_data'])>0)
                                                        {  ?>
                                                            <tr class="details">

                                                                <td  colspan="7">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center"><strong>Serial Number</strong></th>
                                                                        <th class="text-center"><strong>Tool Number</strong></th>
                                                                        <th class="text-center"><strong>Tool Description</strong></th>
                                                                        <th class="text-center"><strong>Tool Level</strong></th>
                                                                        <th class="text-center"><strong>Quantity</strong></th>
                                                                        <th class="text-center"><strong>Tool Health</strong></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                           <?php foreach(@$row['health_data'] as $value)
                                                            {  ?>
                                                                <tr class="asset_row">
                                                                    <td class="text-center"><?php echo $value['serial_number']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_number']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_description']; ?></td>
                                                                    <td class="text-center"><?php echo $value['part_level_name']; ?></td>
                                                                    <td class="text-center"><?php echo $value['quantity']; ?></td>
                                                                    <td class="text-center"><?php echo $value['health_status']; ?></td>
                                                                    <input type="hidden" name="oah_oa_health_id_part_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][]" value="<?php echo $value['part_id'];?>"  >                       
                                                                    <td width="32%" class="hidden">
                                                                        <div class="custom_icheck">
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if($value['asset_condition_id'] == 1) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="1" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id'] == 1) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Good
                                                                        </label>
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if(@$value['asset_condition_id'] == 2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="2" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id']== 2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Defective
                                                                        </label>
                                                                        <label class="radio-inline"> 
                                                                            <div class="iradio_square-blue <?php if(@$value['asset_condition_id'] == 3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
                                                                                <input type="radio" class="asset_condition_id" value="3" name="oa_health_id[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]" <?php if(@$value['asset_condition_id'] == 3) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
                                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                                                            </div> 
                                                                            Missing
                                                                        </label>
                                                                        </div>
                                                                    </td>
                                                                    <td class="hidden">
                                                                        <div class="textbox">
                                                                            <textarea class="form-control textarea" name="remarks[<?php echo $oah_id;?>][<?php echo $value['oa_health_id'];?>][<?php echo $value['part_id']; ?>]"><?php if($value['remarks'] != '') { echo $value['remarks']; } ?></textarea>  
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php     
                                                            } ?> 

                                                                </tbody>
                                                            </table>
                                                            </td>
                                                        </tr><?php
                                                        }
                                                        else
                                                        { ?>
                                                            <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                                        <?php 
                                                        }
                                                    }
                                                
                                                } else { ?>
                                                    <tr><td colspan="5" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                        <?php   } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <br>
                                     <div class="form-group">
                                        <label for="inputName" class="col-sm-3 control-label">Remarks <span class="req-fld">*</span></label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control" required name="admin_remarks"></textarea>                                        
                                        </div>
                                    </div> 
                                </div>
                                
                                
                                  
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6"><br>
                                        <button class="btn btn-primary" onclick="return confirm('Are you sure you want to Approve?')"  type="submit" value="1" name="approve"><i class="fa fa-check"></i> Approve</button>
                                        <button class="btn btn-danger" onclick="return confirm('Are you sure you want to Reject?')"  type="submit" value="2" name="reject"><i class="fa fa-check"></i> Reject</button>
                                        <a class="btn btn" href="<?php echo SITE_URL;?>fe2_fe_approval"><i class="fa fa-times"></i> Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>              
                </div>
            </div>
	    
        </div>
    </div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>