<div class="col-sm-12 col-md-12">
    <div class="table-responsive" style="margin-top: 10px;">
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th class="text-center"><strong>S.No</strong></th>
                    <th class="text-center"><strong>Order Number</strong></th>
                    <th class="text-center"><strong>SSO</strong></th>                    
                    <th class="text-center"><strong>Availability</strong></th>   
                    <th class="text-center"><strong>Tool Number</strong></th>
                    <th class="text-center"><strong>Tool Description</strong></th>
                    
                </tr>
            </thead>
            <tbody>
                <?php 
                if(count(@$orderResults)>0)
                {   
                    foreach(@$orderResults as $row)
                    { 
                    ?>
                        <tr>
                            <td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand" ></td>
                            <td class="text-center"><input type="radio" class="fe1_order" name="fe1_order_chk" value="<?php echo $row['order_number']; ?>"></td>
                            <td class="text-center"><?php echo $sn++; ?></td>
                            <td class="text-center"><?php echo $row['order_number']; ?></td>
                            <td class="text-center"><?php echo $row['sso']; ?></td>                            
                            <td class="text-center"><?php echo $row['countryName']; ?></td>  
                            <td class="text-center"><?php echo $row['part_number']; ?></td>
                            <td class="text-center"><?php echo $row['part_description']; ?></td>
                            
                        </tr>
                        <?php $i=0; if(count(@$asset_data[$row['tool_id']])>0)
                        {  ?>
                        <tr class="details" style="display:none">
                            <td></td>
                                <td  colspan="8"> 
                                    <table cellspacing="0" cellpadding="5" border="0" style="padding-left:50px;" class="table">
                                        <thead>
                                            <th class="text-center">Tool Number</th>
                                            <th class="text-center">Tool Description</th>                                            
                                            <th class="text-center">Tool Level </th>
                                            <th class="text-center">Quantity</th>


                                        </thead>
                                        <tbody>
                                   <?php foreach(@$asset_data[$row['tool_id']] as $value)
                                    { 
                                    ?>
                                        <tr class="asset_row">
                                            <td class="text-center"><?php echo $value['part_number']; ?></td>
                                            <td class="text-center"><?php echo $value['part_description']; ?></td>
                                            <td class="text-center"><?php echo $value['part_level_name']; ?></td>
                                            <td class="text-center"><?php echo ($i==0)?1:$value['parts_quantity']; ?></td>
                                            
                                        </tr>
                                    <?php $i++;     
                                    } ?>
                                        </tbody>
                                    </table>
                            </td>
                        </tr><?php
                        }
                        else
                        { ?>
                            <tr><td colspan="9" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                        <?php 
                        }
                    }
                
                } else {?>
                    <tr><td colspan="8" align="center"><span class="label label-primary">No Records Found</span></td></tr>
        <?php   } ?>
            </tbody>
        </table>
    </div>
</div> 
<div class="row">
    <div class="col-sm-12">
        <div class="pull-left">
            <div class="dataTables_info" role="status" aria-live="polite">
                <?php echo @$pagermessage; ?>
            </div>
        </div>
        <div class="pull-right">
            <div class="dataTables_paginate paging_bootstrap_full_number" id="feToolSearchPagination">
                <?php echo @$pagination_links; ?>
            </div>
        </div>
    </div> 
</div>  