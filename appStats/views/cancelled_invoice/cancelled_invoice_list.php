<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>cancelled_invoice"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12">
										<label class="col-sm-1 control-label">Invoice&nbsp;No.</label>
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="invoice_no" placeholder="Invoice Number" value="<?php echo @$search_data['invoice_no'];?>" id="location" class="form-control" maxlength="100">
										</div>
										<label class="col-sm-2 control-label">Cancelled Date</label>
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="cancelled_date" readonly placeholder="Cancelled Date" value="<?php if(@$search_data['cancelled_date']!=''){
	                                        	echo indian_format($search_data['cancelled_date']);
	                                        	}?>" class="form-control date" style="cursor:hand;background-color: #ffffff">
										</div>
										<div class="col-sm-3">							
											<button type="submit" name="searchinvoice" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
											<a href="<?php echo SITE_URL.'cancelled_invoice'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<?php
										if($task_access==2 || $task_access==3)
										{
											?>
											<label class="col-sm-1 control-label">Warehouse</label>
											<div class="col-sm-3">
												<select name="wh_id" class="select2"> 
													<option value="">- Warehouse -</option>
													<?php
													foreach($warehouse as $row)
													{
														$selected = ($row['wh_id']==@$search_data['whp_id'])?'selected="selected"':'';
														echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
													}?>
												</select>
											</div> <?php

										}
										if($task_access == 3 && @$_SESSION['header_country_id']=='')
										{
											?>
											<label class="col-sm-2 control-label">Country</label>
											<div class="col-sm-3">
												<select name="country_id" class="select2" >
													<option value="">- Country -</option>
													<?php
													foreach($country_list as $row)
													{
														$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
														echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
													}?>
												</select>
											</div> <?php
										}?>
										
									</div>
									<div class="col-md-12">
										<div class="header"></div>
									</div>
								</div>
								
								<div class="table-responsive" style="margin-top: 10px;">
									<table class="table table-bordered hover" id="mytable">
										<thead>
											<tr>
												<th class="text-center"><strong>S.NO</strong></th>
												<th class="text-center"><strong>Invoice Number</strong></th>
												<th class="text-center"><strong>Cancelled Date</strong></th>
												<th class="text-center"><strong>Workflow</strong></th>
												<th class="text-center"><strong>Warehouse</strong></th>
												<th class="text-center"><strong>Reason</strong></th>
												<th class="text-center"><strong>Country</strong></th>
												<th class="text-center"><strong>Print</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php
											if(count($cal_list_Results)>0)
											{
												foreach($cal_list_Results as $row)
												{
													?>
													<tr class="toolRow">
														<td class="text-center"><?php echo $sn++;?></td>
														<td class="text-center"><?php echo $row['format_number'];?></td>
														<td class="text-center"><?php echo date('d-m-Y',strtotime($row['modified_time']));?></td>
														<td class="text-center">
														<?php 
														if($row['order_type']==2)
															echo "Order";
														else if($row['order_type']==1)
															echo "Stock Transfer";
														else if($row['rc_type']==1)
															echo "Repair";
														else if($row['rc_type']==2)
															echo 'Calibration';
														else if($row['return_order_id']!='')
															echo 'Pickup Initiate';
														?>	
														</td>
														<td class="text-center">
															<?php echo $row['wh_code'].' -('.$row['name'].')';?>
														</td>
														<td class="text-center"><?php echo $row['remarks'];?>
														</td>
														<td class="text-center"><?php echo $row['country_name'];?>
														</td>
														<td class="text-center">
															<a class="btn btn-primary" style="padding:3px 3px;" target="_blank" data-container="body" data-placement="top" href="<?php echo SITE_URL.'view_cancelled_invoice/'.storm_encode($row['ph_id']);?>"  data-toggle="tooltip" title="Print" ><i class="fa fa-print"></i></a>
														</td>
													</tr> <?php
												}
											}
											else 
											{
											?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records Found</span></td></tr>
											<?php 	} ?>
										</tbody>
									</table>
								</div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>