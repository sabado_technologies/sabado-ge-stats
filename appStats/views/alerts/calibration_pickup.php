<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>insert_fe_owned_order">
						<input type="hidden" value="<?php echo $owned_assets[0]['order_status_id'];?>" name="order_status_id">
						<input type="hidden" value="<?php echo $tool_order_id;?>" name="tool_order_id">
						<input type="hidden" value="<?php echo count(@$total_owned_assets);?>" name="owned_assets_count">

						<div class="table-responsive">
							<table class="table table-bordered hover">
								<thead>
									<tr>
										<th><input type="checkbox" id="checkAll" name="checkAll"></th>
										<th class="text-center"><strong>S.NO</strong></th>
										<th class="text-center"><strong>Asset Number</strong></th>	
										<th class="text-center"><strong>Part Number</strong></th>	
										<th class="text-center"><strong>Part Description</strong></th>										
									</tr>
								</thead>
								<tbody>
								<?php
									$i = 1;
									if(count(@$owned_assets)>0)
									{
										foreach($owned_assets as $row)
										{?>
											<tr>
												<td><input type="checkbox" class="chkAll openOrder " name="oah_id[<?php echo $row['oah_id'];?>]" value="<?php echo $row['ordered_asset_id'];?>"></td>
												<input type="hidden" name="history_status_id[<?php echo $row['oah_id'];?>]" value="<?php echo $row['history_status_id'];?>">
												<td class="text-center"><?php echo @$i++;?></td>
												<td class="text-center"><?php echo @$row['asset_number'];?></td>
												<td class="text-center"><?php echo @$row['part_number'];?></td>
												<td class="text-center"><?php echo @$row['part_description'];?></td>
											</tr>
								<?php	}
									} else {
									?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
							<?php 	} ?>
								</tbody>
							</table>
						</div><br>
						
						<div class="row">
							<h4> &nbsp;&nbsp;&nbsp;&nbsp;<b> Return From</b></h4>
							
						</div>
						<div class="form-group">
							<div class="col-md-12 custom-icheck">
								<label for="inputName" class="col-sm-2 control-label">Delivery Point <span class="req-fld">*</span></label>
	                           	<div class="col-sm-5 custom_icheck">
	                                <label class="radio-inline"> 
	                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==1 || $flg == 1) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
	                                        <input type="radio"  class="delivery_type" value="1" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==1 || $flg == 1) { echo "checked"; }?> style="position: absolute; opacity: 0;">
	                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
	                                    </div> 
	                                    Customer
	                                </label>
	                                <label class="radio-inline"> 
	                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
	                                        <input type="radio" class="delivery_type" value="2" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
	                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
	                                    </div> 
	                                    Warehouse
	                                </label>
	                                <label class="radio-inline"> 
	                                    <div class="iradio_square-blue <?php if(@$order_info['order_delivery_type_id']==3 ) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
	                                        <input type="radio" class="delivery_type" value="3" name="delivery_type_id" <?php if(@$order_info['order_delivery_type_id']==3 ) { echo "checked"; }?> style="position: absolute; opacity: 0;">
	                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
	                                    </div> 
	                                    Others
	                                </label>
	                            </div>
	                            <div class="site_remarks <?php if(@$order_info['site_readiness']==1 || $flg ==1) { echo "hidden";}?> ">
	                            	<label for="inputName" class="col-sm-1 control-label">  Remarks </label>
									<div class="col-sm-4">
										<textarea name="site_remarks" class="form-control site_remarks_value"><?php echo @$order_info['site_remarks']; ?></textarea>
									</div>
	                            </div>
		                                                    
							</div>
						</div>
						<div class="customerRow <?php if(@$order_info['order_delivery_type_id']==2 || @$order_info['order_delivery_type_id'] == 3){ echo "hidden";}?>">
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Site ID <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<input type="text" autocomplete="off"  class="form-control" id="site_id"  placeholder="Site ID" name="site_id" value="<?php if(@$order_info['order_delivery_type_id'] == 1){ echo @$order_info['site_id']; } ?>">
									</div>

									<label for="inputName" class="col-sm-2 control-label">System ID <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<input type="text" autocomplete="off"  class="form-control" id="system_id" placeholder="System ID" name="system_id" value="<?php if(@$order_info['order_delivery_type_id'] == 1){ echo @$order_info['system_id'];} ?>">
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<textarea class="form-control"  id="ct_address_1" name="ct_address_1" disabled><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address1'];} ?>
										</textarea>
									</div>

									<label for="inputName" class="col-sm-2 control-label">Address2 <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<textarea class="form-control"  id="ct_address_2" name="ct_address_2" disabled><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address2'];} ?>
										</textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">City </label>
									<div class="col-sm-4">
										<textarea class="form-control"  id="ct_address_3" name="ct_address_3" disabled><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address3'];} ?>
										</textarea>
									</div>

									<label for="inputName" class="col-sm-2 control-label">State </label>
									<div class="col-sm-4">
										<textarea class="form-control"  id="ct_address_4"  name="ct_address_4" disabled><?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['address4'];} ?>
										</textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Pin Code <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<input type="text" class="form-control"  disabled value="<?php if (@$order_info['order_delivery_type_id'] ==1){echo @$order_info['pin_code'];}?>" id="ct_pin_code" name="ct_pin_code" >
									</div>
								</div>
							</div>
							
						</div> <!-- end of customer div-->
						<div class="warehouseRow <?php if(@$order_info['order_delivery_type_id']== 1 || @$order_info['order_delivery_type_id']==3 || $flg==1){ echo "hidden";};?>" > <!-- Start of warehouse Div-->
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Warehouse <span class="req-fld">*</span></label>
									<div class="col-sm-4">
									   
											<select name="wh_id" class="form-control wh_id_cls  " > 
											<option value="">Select Warehouse</option>
											<?php 
												foreach($wh_data as $st)
												{
													$selected = ($st['wh_id']==@$wh_id)?'selected="selected"':'';
													echo '<option value="'.$st['wh_id'].'" '.$selected.'>'.$st['name'].'</option>';
												}
											?>
											</select>
											
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<textarea class="form-control"  id="wh_address_1" name="wh_address_1" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address1']; } else{ echo @$order_info['address1']; }  ?></textarea>
									</div>

									<label for="inputName" class="col-sm-2 control-label">Address2 <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<textarea class="form-control"  id="wh_address_2" name="wh_address_2" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address2']; }  else{ echo @$order_info['address2']; }  ?></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">City </label>
									<div class="col-sm-4">
										<textarea class="form-control"  id="wh_address_3" name="wh_address_3" readonly ><?php if($_SESSION['role_id'] == 2){  echo @$wh_data['address3']; } else{ echo @$order_info['address3']; }  ?></textarea>
									</div>

									<label for="inputName" class="col-sm-2 control-label">State </label>
									<div class="col-sm-4">
										<textarea class="form-control"  id="wh_address_4"  name="wh_address_4" readonly ><?php if($_SESSION['role_id'] == 2){ echo @$wh_data['address4']; }  else{ echo @$order_info['address4']; }  ?></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Pin Code <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<input type="text" class="form-control"  id="wh_pin_code"  name="wh_pin_code" readonly value="<?php if($_SESSION['role_id']==2){echo @$wh_data['pin_code']; }  else{ echo @$order_info['pin_code']; } ?>">
									</div>
								</div>
							</div>
						</div> <!-- end of warehouse -->
						<div class="othersRow <?php if(@$order_info['order_delivery_type_id'] == 1 || @$order_info['order_delivery_type_id'] == 2 || $flg ==1){ echo "hidden";} ?>" >
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Address1 <span class="req-fld">*</span></label>
									<div class="col-sm-4">
										<textarea class="form-control"   name="address_1" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address1'];}  ?></textarea>
									</div>

									<label for="inputName" class="col-sm-2 control-label">Address2 </label>
									<div class="col-sm-4">
										<textarea class="form-control"   name="address_2" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address2'];}  ?></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">City </label>
									<div class="col-sm-4">
										<textarea class="form-control"  name="address_3" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address3'];}  ?></textarea>
									</div>

									<label for="inputName" class="col-sm-2 control-label">State </label>
									<div class="col-sm-4">
										<textarea class="form-control"   name="address_4" ><?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['address4'];} ?></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Pin Code </label>
									<div class="col-sm-4">
										<input type="text" class="form-control" autocomplete="off"  id="pin_code" name="pin_code" value="<?php if (@$order_info['order_delivery_type_id'] ==3){echo @$order_info['pin_code'];}  ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<h4> &nbsp;&nbsp;&nbsp;&nbsp;<b> Return To</b></h4>
							
						</div>
						<div class="form-group">
								<div class="col-md-12">
									<label for="inputName" class="col-sm-2 control-label">Return Type <span class="req-fld">*</span></label>
										<div class="col-sm-4">									   
											<select name="return_type_id" required class="form-control return_cls " > 
											
											<?php 
												foreach($return_type as $rt)
												{
													$selected = ($rt['return_type_id']==@$whjnkj_id)?'selected="selected"':'';
													echo '<option value="'.$rt['return_type_id'].'" '.$selected.'>'.$rt['name'].'</option>';
												}
											?>
											</select>										
										</div>
									<div class="wh_div_cls">
										<label for="inputName" class="col-sm-2 control-label">Return To WH <span class="req-fld">*</span></label>
										<div class="col-sm-4  ">									   
												<select name="to_wh_id" class="form-control   " > 
												<option value="">Select Warehouse</option>
												<?php 
													foreach($wh_data as $st2)
													{
														$selected = ($st2['wh_id']==@$wh_id)?'selected="selected"':'';
														echo '<option value="'.$st2['wh_id'].'" '.$selected.'>'.$st2['name'].'</option>';
													}
												?>
												</select>											
										</div>	
									</div>
													
								</div>

						</div>
						<!-- <div class="form-group">
							<div class="col-md-12">
								
							</div>
						</div> -->
						<br>
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-5">
								<button class="btn btn-success" onclick="return confirm('Are you sure you want to Submit?')"  type="submit" value="1" name="submit_pickup" disabled id="submitOrder"><i class="fa fa-check"></i> Submit</button>
								<a class="btn btn-danger" href="<?php echo @$cancel_form_action;?>"><i class="fa fa-times"></i> Cancel</a>
							</div>
						</div>	  
		            </form>        		
				</div>
			</div>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	get_warehouse_address();
</script>
