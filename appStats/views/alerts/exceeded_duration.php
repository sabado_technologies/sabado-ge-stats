<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <?php if(isset($flg))
		    { ?>

		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">	
							<form class="form-horizontal" action="<?php echo SITE_URL;?>submitexceededOrderDuration"  parsley-validate novalidate method="post">						
								<input type="hidden" name="tool_order_id" value="<?php echo $order_info['tool_order_id'];?>">
								<input type="hidden" name="order_number" value="<?php echo $order_info['order_number'];?>">
								<div class="header">
									<h5 align="center"><strong>Order Details</strong></h5>
								</div>
								<div class="row">
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Order Number :</strong></td>
										        <td class="data-item"><?php echo ($order_info['order_type'] == 1)?$order_info['stn_number']:@$order_info['order_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Shipped To :</strong></td>
												<td class="data-lable"><strong></strong></td>
										        
											</tr>
											<tr>
												<td class="data-lable"><strong>Address1 :</strong></td>
										        <td class="data-item"><?php echo @$order_info['address1'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address3 :</strong></td>
										        <td class="data-item"><?php echo @$order_info['address3'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Pin Code :</strong></td>
										        <td class="data-item"><?php echo @$order_info['pin_code'];?></td>
											</tr>
											
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Requested By :</strong></td>
												<td class="data-item"><?php echo @$order_info['sso'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>&nbsp;</strong></td>
												<td class="data-item"><strong></strong></td>
										        
											</tr>
											<tr>
												<td class="data-lable"><strong>Address2 :</strong></td>
										        <td class="data-item"><?php echo @$order_info['address2'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address4 :</strong></td>
										        <td class="data-item"><?php echo @$order_info['address4'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
								</div><br>
								<div class="header">
									<h5 align="center"><strong>Owned Assets</strong></h5>
								</div>
								<div class="table-responsive"> 
									<table class="table tabb">
										<thead>
											<tr>
												<th class="text-center"><strong>S No.</strong></th>
												<th class="text-center"><strong>Asset Number</strong></th>
												<th class="text-center"><strong>Serial Number</strong></th>
				                                <th class="text-center"><strong>Tool Number</strong></th>
				                                <th class="text-center"><strong>Tool Code</strong></th>
				                                <th class="text-center" width="30%"><strong>Tool Description</strong></th>
				                                <th class="text-center" width="10%"><strong>Cal Due Date</strong></th>
				                                <th class="text-center"><strong>Tool Type</strong></th>				                              	
											</tr>
										</thead>
										<tbody>
											<?php 
											if(count(@$tools)>0)
											{	$sn = 1;
												foreach(@$tools as $row)
												{
												?>
													<tr class="asset_row">
														<td class="text-center"><?php echo $sn++; ?></td>
														<td class="text-center"><?php echo $row['asset_number']; ?></td>
														<td class="text-center"><?php echo $row['serial_number']; ?></td>
														<td class="text-center"><?php echo $row['part_number']; ?></td>
														<td class="text-center"><?php echo $row['tool_code']; ?></td>
														<td class="text-center"><?php echo $row['part_description']; ?></td>
														<td class="text-center"><?php if(@$row['cal_due_date']!=''){ echo indian_format(@$row['cal_due_date']);} else{ echo "--"; } ?></td>
														<td class="text-center"><?php echo $row['tool_type']; ?></td>
													</tr>
										<?php   }
											} else {?>
												<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
				                    <?php 	} ?>
										</tbody>
									</table>
				                </div><br>

				                <div class="row">
				                	<div class="form-group">
										<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">Requested  Date :</label>
											<div class="col-sm-3" style="margin-top: 7px;">
												<strong><span><?php echo indian_format($order_info['deploy_date']);?></span></strong>
											</div>
											<label for="inputName" class="col-sm-3 control-label">Old Return Date :</label>
											
											<div class="col-sm-3" style="margin-top: 7px;">
												<strong><span><?php echo indian_format($order_info['return_date']);?></span></strong>
												<input class="form-control" required size="16" type="hidden"   id="oldDate" value="<?php  echo indian_format(@$order_info['return_date']); ?>" readonly placeholder="Date" name="request_date">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											
											<label for="inputName" class="col-sm-2 control-label">Days Duration :</label>
											<div class="col-sm-3" style="margin-top: 7px;">
												<strong><span><?php echo dateDiffInDays(@$order_info['return_date'],@$exd_date_details['return_date']);?> Days</span></strong>
											</div>
											<label for="inputName" class="col-sm-3 control-label">Reason :</label>
											<div class="col-sm-3" style="margin-top: 7px;">
												<strong><span><?php echo @$exd_date_details['approved_remarks'];?></span></strong>
											</div>
										</div>
										<div class="col-md-12">
											<label for="inputName" class="col-sm-2 control-label">New Return Date <span class="req-fld">*</span></label>
											<div class="col-sm-3" style="margin-top: 7px;">
												<input class="form-control" required size="16" type="text" autocomplete="off"   value="<?php echo indian_format(@$exd_date_details['return_date']);?>" id="newDate" readonly placeholder="Date" name="new_return_date" style="cursor:hand;background-color: #ffffff">
											</div>
											<label for="inputName" class="col-sm-3 control-label">Remarks <span class="req-fld">*</span></label>
											<div class="col-sm-3" style="margin-top: 7px;">
												<textarea class="form-control"  required name="approved_remarks"></textarea>
											</div>
										</div>
									</div>
								</div>
								<br>
						    	<div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button class="btn btn-primary" onclick="return confirm('Are you sure you want to Approve?')"  type="submit" value="1" name="ac_submit"><i class="fa fa-check"></i> Approve</button>
                                        <button class="btn btn-danger" onclick="return confirm('Are you sure you want to Reject?')"  type="submit" value="2" name="ac_submit"><i class="fa fa-check"></i> Reject</button>
                                        <a class="btn btn" href="<?php echo SITE_URL;?>exceededOrderDuration"><i class="fa fa-times"></i> Cancel</a>
                                    </div>
	                            </div>
			                </form>
						</div>
					</div>				
				</div>
			</div> 	
		    <?php } ?>
			<?php if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>exceededOrderDuration">
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="order_number" placeholder="Order Number" value="<?php echo @$searchParams['order_number'];?>"  class="form-control">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="deploy_date" readonly placeholder="Request Date" value="<?php echo @$searchParams['deploy_date'];?>" id="dateFrom" class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="return_date" readonly placeholder="Return Date To" value="<?php echo @$searchParams['return_date'];?>"  id="dateTo" class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">							
									<button type="submit" name="exceededOrderDuration" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'exceededOrderDuration'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
									<select name="order_delivery_type_id" class="select2" > 
										<option value="">-Order Type-</option>
										<?php 
											foreach($order_type as $ot)
											{
												$selected = ($ot['order_delivery_type_id']==@$searchParams['order_delivery_type_id'])?'selected="selected"':'';
												echo '<option value="'.$ot['order_delivery_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
											}
										?>
									</select>
								</div>
								<?php if($task_access==2 || $task_access==3) { ?>
								<div class="col-sm-3">
									<?php 
                                    $d['name']        = 'users_id';
                                    $d['search_data'] = @$searchParams['users_id'];
                                    $this->load->view('sso_dropdown_list',$d); 
                                    ?>
								</div>
								<?php } ?>
								
								<?php if($task_access == 3 && @$_SESSION['header_country_id']==''){?>
								<div class="col-sm-3">
									<select class="select2" name="country_id" >
										<option value="">- Country -</option>
										 <?php
										foreach($countryList as $country)
										{
											$selected = ($country['location_id']==$searchParams['country_id'])?'selected="selected"':'';
											echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
										}
										?>
									</select>
								</div>
								<?php } ?>
								
								</div>
								
							</div>	
						</div>
					</form>
					<div class="row">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
						</div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Order Number</strong></th>
								<th class="text-center"><strong>Request From</strong></th>
								
								<th class="text-center"><strong>Order Type</strong></th>
								<th class="text-center"><strong>Request Date</strong></th>
								<th class="text-center"><strong>Old Return Date</strong></th>
								<th class="text-center"><strong>New Return Date</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							$i = 1;
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$i++;?></td>
										<td class="text-center"><?php echo @$row['order_number'];?></td>	
										<td class="text-center"><?php echo @$row['sso'];?></td>
										<td class="text-center"><?php echo @$row['order_type'];?></td>
										<td class="text-center"><?php echo indian_format(@$row['deploy_date']);?></td>
										<td class="text-center"><?php echo indian_format(@$row['return_date']);?></td>
										<td class="text-center"><?php echo indian_format(@$row['exd_return_date']);?></td>
										<td class="text-center"><?php echo @$row['countryName'];?></td>
										<td class="text-center">
											<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(6);?>"><i class="fa fa-eye"></i></a>
																				
                                        <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Update Order"  href="<?php echo SITE_URL.'exceededOrderDurationAction/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>

                                    </td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="9" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>
			<?php } ?>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
