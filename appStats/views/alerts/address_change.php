<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <?php if(isset($flg))
		    { ?>

		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">	
							<form class="form-horizontal" action="<?php echo SITE_URL;?>submit_order_address_change"  parsley-validate novalidate method="post">						
								<div class="header">
									<h5 align="center"><strong>Order Details</strong></h5>
								</div>
								<div class="row">
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Order Number :</strong></td>
										        <td class="data-item"><?php echo @$order_info['order_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Site ID :</strong></td>
										        <td class="data-item"><?php echo @$order_info['site_id'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong><u>Old Address</u> :</strong></td>									        
											</tr>
											<tr>
												<td class="data-lable"><strong>Address1 :</strong></td>
										        <td class="data-item"><?php echo @$customer_info['address1'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address2 :</strong></td>
										        <td class="data-item"><?php echo @$customer_info['address2'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>City :</strong></td>
										        <td class="data-item"><?php echo @$customer_info['address3'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>State :</strong></td>
										        <td class="data-item"><?php echo @$customer_info['address4'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Pin Code :</strong></td>
										        <td class="data-item"><?php echo @$customer_info['zip_code'];?></td>
											</tr>
											
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>SSO Detail :</strong></td>
												<td class="data-item"><?php echo @$order_info['sso_id'];?></td>
												<input type="hidden" name="location_id" value="<?php echo @$order_info['location_id'];?>">
												<input type="hidden" name="tool_order_id" value="<?php echo @$order_info['tool_order_id'];?>">
												<input type="hidden" name="order_number" value="<?php echo @$order_info['order_number'];?>">
												<input type="hidden" name="site_id" value="<?php echo @$order_info['site_id'];?>">
												<input type="hidden" name="address1" value="<?php echo @$order_info['address1'];?>">
												<input type="hidden" name="address2" value="<?php echo @$order_info['address2'];?>">
												<input type="hidden" name="address3" value="<?php echo @$order_info['address3'];?>">
												<input type="hidden" name="address4" value="<?php echo @$order_info['address4'];?>">
												<input type="hidden" name="pin_code" value="<?php echo @$order_info['pin_code'];?>">
											</tr>
											<tr>
												<td class="data-lable"><strong>System ID :</strong></td>
										        <td class="data-item"><?php echo @$order_info['system_id'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong><u>New Address </u> :</strong></td>
										        
											</tr>
											<tr>
												<td class="data-lable"><strong>Address1 :</strong></td>
										        <td class="data-item"><?php echo @$order_info['address1'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address2 :</strong></td>
										        <td class="data-item"><?php echo @$order_info['address2'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>City :</strong></td>
										        <td class="data-item"><?php echo @$order_info['address3'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>State :</strong></td>
										        <td class="data-item"><?php echo @$order_info['address4'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Pin Code :</strong></td>
										        <td class="data-item"><?php echo @$order_info['pin_code'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
								</div><br>
								<div class="header">
									<h5 align="center"><strong>Ordered Tools</strong></h5>
								</div>
								<div class="table-responsive"> 
									<table class="table tabb">
										<thead>
											<tr>
												<th class="text-center"><strong>S No.</strong></th>
				                                <th class="text-center"><strong>Tool Number</strong></th>
				                                <th class="text-center"><strong>Tool Code</strong></th>
				                                <th class="text-center" width="30%"><strong>Tool Description</strong></th>
				                                <th class="text-center"><strong>Ordered Quantity</strong></th>
				                                <th class="text-center"><strong>Available Quantity</strong></th>
				                              	
											</tr>
										</thead>
										<tbody>
											<?php 
											if(count(@$tools)>0)
											{	$sn = 1;
												foreach(@$tools as $row)
												{
												?>
													<tr class="asset_row">
														<td class="text-center"><?php echo $sn++; ?></td>
														<td class="text-center"><?php echo $row['part_number']; ?></td>
														<td class="text-center"><?php echo $row['tool_code']; ?></td>
														<td class="text-center"><?php echo $row['part_description']; ?></td>
														<td class="text-center"><?php echo $row['quantity']; ?></td>
														<td class="text-center"><?php echo $row['available_quantity']; ?></td>
														
													</tr>
										<?php   }
											} else {?>
												<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
				                    <?php 	} ?>
										</tbody>
									</table>
				                </div><br>

				                <div class="header">
									<h5 align="center"><strong>Documents Details</strong></h5>
								</div>
								<div class="table-responsive"> 
									<table class="table tabb">
										<thead>
											<tr>
												<th class="text-center"><strong>S No.</strong></th>
				                                <th class="text-center"><strong>Document Type</strong></th>
				                                <th class="text-center"><strong>Name</strong></th>
				                                <th class="text-center" width="30%"><strong>Download</strong></th>                           	
											</tr>
										</thead>
										<tbody>
											<?php 
											if(count(@$attach_document)>0)
											{	$sn = 1;
												foreach(@$attach_document as $row)
												{
												?>
													<tr class="asset_row">
														<td class="text-center"><?php echo $sn++; ?></td>
														<td class="text-center"><?php echo $this->Common_model->get_value('document_type',array('document_type_id'=>$row['document_type_id']),'name'); ?></td>
														<td class="text-center"><?php echo $row['doc_name']; ?></td>
														<td><a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo SITE_URL1.order_document_path().$row['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $row['name']; ?></td>
														
													</tr>
										<?php   }
											} else {?>
												<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
				                    <?php 	} ?>
										</tbody>
									</table>

				                </div><br>
				                
					     	    <div class="form-group">
							        <label for="inputName" class="col-sm-3 control-label">Remarks <span class="req-fld">*</span></label>
								    <div class="col-sm-5">
									    <textarea class="form-control" required name="remarks"></textarea>									    
								    </div>
							    </div><br>
						    
				                
			                	<div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button class="btn btn-primary" onclick="return confirm('Are you sure you want to Submit?')"  type="submit" value="1" name="ac_submit"><i class="fa fa-check"></i> Approve</button>
                                        <button class="btn btn-danger" onclick="return confirm('Are you sure you want to Submit?')"  type="submit" value="2" name="ac_submit"><i class="fa fa-check"></i> Reject</button>
                                        <a class="btn btn" href="<?php echo SITE_URL;?>address_change"><i class="fa fa-times"></i> Cancel</a>
                                    </div>
	                            </div>

			                </form>
				                
						</div>
					</div>				
				</div>
			</div> 	
		    <?php } ?>
			<?php if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>address_change">
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
                                    <input type="text" name="order_number" autocomplete="off" placeholder="Order Number" value="<?php echo @$searchParams['order_number'];?>"  class="form-control">
								</div>
								<div class="col-sm-3">
                                    <input type="text" name="deploy_date" autocomplete="off" placeholder="Request Date" value="<?php echo @$searchParams['deploy_date'];?>" id="dateFrom" class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								
								<div class="col-sm-3">
                                    <input type="text" name="return_date" autocomplete="off" placeholder="Return Date To" value="<?php echo @$searchParams['return_date'];?>"  id="dateTo" class="form-control" style="cursor:hand;background-color: #ffffff">
								</div>
								<div class="col-sm-3">
									<select name="request_type" class="select2"> 
										
										<?php 
											foreach(addressChangeType() as $ot)
											{
												$selected = ($ot['id']==@$searchParams['request_type'])?'selected="selected"':'';
												echo '<option value="'.$ot['id'].'" '.$selected.'>'.$ot['name'].'</option>';
											}
										?>
									</select>
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
                                    <input type="text" name="return_number" autocomplete="off" placeholder="Return Number" value="<?php echo @$searchParams['return_number'];?>"  class="form-control">
								</div>
								<?php
								if($task_access == 2 || $task_access ==3)
								{
									?>
									<div class="col-sm-3">
										<?php 
										$d['name']        = 'sso_id';
										$d['search_data'] = @$searchParams['a_sso_id'];
										$this->load->view('sso_dropdown_list',$d); 
										?>
									</div> <?php
								}?>
								
								<?php
								if($task_access == 3 && @$_SESSION['header_country_id']=='')
								{
									?>
									<div class="col-sm-3">
										<select name="country_id" class="main_status select2" >    <option value="">- Select Country -</option>
											<?php
											foreach($country_list as $row)
											{
												$selected = ($row['location_id']==@$searchParams['country_id'])?'selected="selected"':'';
												echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
											}?>
										</select>
									</div> <?php
								}?>

								<div class="col-sm-3 col-md-3">							
									<button type="submit" name="address_change" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'address_change'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
								</div>	

							</div>	
						</div>
						<div class="row">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
						</div>
					</form>
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Order Number</strong></th>
								<th class="text-center"><strong>Return Number</strong></th>
								<th class="text-center"><strong>Request From</strong></th>
								<th class="text-center"><strong>Request Date</strong></th>
								<th class="text-center"><strong>Return Date</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							$i = 1;
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$i++;?></td>

										<td class="text-center"><?php echo (@$row['final_order_number']!='')?@$row['final_order_number']:'NA';;?></td>	
										<td class="text-center"><?php echo (@$row['return_number']!='')?@$row['return_number']:'NA';?></td>
										<td class="text-center"><?php echo (@$row['return_number']!='')?$row['ro_sso']:@$row['sso'] ;?></td>
										<td class="text-center"><?php echo indian_format(@$row['deploy_date']);?></td>
										<td class="text-center"><?php echo indian_format(@$row['return_date']);?></td>
										<td class="text-center"><?php echo @$row['country']; ?></td>
										<td class="text-center">
											<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(2);?>"><i class="fa fa-eye"></i></a>
											<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title=" Address Change"  href="<?php 
											if(@$row['return_number']!='')
												echo SITE_URL.'return_address_change/'.storm_encode($row['return_order_id']);
											else
												echo SITE_URL.'order_address_change/'.storm_encode($row['tool_order_id']);
											?>"><i class="fa fa-pencil"></i></a>

                                   	    </td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="9" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>
			<?php } ?>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
