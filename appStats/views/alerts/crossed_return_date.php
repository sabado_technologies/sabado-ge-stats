<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>crossed_return_date_orders">
						<div class="row">
							<div class="col-sm-12">
								<div class="col-sm-3">
                                    <input type="text" name="order_number" autocomplete="off" placeholder="Order Number" value="<?php echo @$searchParams['cr_order_number'];?>"  class="form-control">
								</div>
								<div class="col-sm-3">
									<select name="order_delivery_type_id" class="select2" > 
										<option value="">-Order Type-</option>
										<?php 
											foreach($order_type as $ot)
											{
												$selected = ($ot['order_delivery_type_id']==@$searchParams['cr_order_delivery_type_id'])?'selected="selected"':'';
												echo '<option value="'.$ot['order_delivery_type_id'].'" '.$selected.'>'.$ot['name'].'</option>';
											}
										?>
									</select>
								</div>
								<?php if($task_access==2 || $task_access==3)
									{
										?>
										<div class="col-sm-3">
											<?php 
											$d['name']        = 'crd_sso_id';
											$d['search_data'] = @$searchParams['crd_sso_id'];
											$this->load->view('sso_dropdown_list',$d); 
											?>
										</div> <?php
									} ?>
									<?php
									if( ($task_access==3 && @$_SESSION['header_country_id']=='' )  || ( count($_SESSION['countriesIndexedArray'])>1 && $_SESSION['header_country_id']=='') )
									{
										?>
										<div class="col-sm-3">
											<select name="crd_country_id" class="main_status select2" >    <option value="">- Select Country -</option>
												<?php
												foreach($country_list as $row)
												{
													$selected = ($row['location_id']==@$searchParams['crd_country_id'])?'selected="selected"':'';
													echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
												}?>
											</select>
										</div>

										 <?php
									}?>	

							</div>	
						</div>
						<div class="row">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
							<div class="col-sm-12">
								<div class="col-sm-4 col-md-4">				
									
								</div>	
								<div class="col-sm-5 col-md-5"></div>
								<div class="col-sm-12 " align="right">							
									<button type="submit" name="crossed_return_date_orders" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'crossed_return_date_orders'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
									<button type="submit" name="downloadtoolreturn" value="1" formaction="<?php echo SITE_URL.'downloadtoolreturn';?>" class="btn btn-success"><i class="fa fa-cloud-download"></i> Download</button>  
								</div>		
							</div>
						</div>
					</form>
					<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Order Number</strong></th>
								<th class="text-center"><strong>SSO</strong></th>
								<th class="text-center"><strong>Order Type</strong></th>
								<th class="text-center"><strong>Request Date</strong></th>
								<th class="text-center"><strong>Return Date</strong></th>
								<th class="text-center"><strong>Country</strong></th>					
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($orderResults)>0)
							{
								foreach($orderResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['order_number'];?></td>
										<td class="text-center"><?php echo @$row['user_name'];?></td>
										<td class="text-center"><?php echo @$row['order_type'];?></td>
										<td class="text-center"><?php echo date('d-m-Y',strtotime($row['request_date']));?></td>
										<td class="text-center"><?php echo date('d-m-Y',strtotime($row['return_date']));?></td>	
										<td class="text-center"><?php echo @$row['country_name'];?></td>	
										<td class="text-center">
										<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="View" href="<?php echo SITE_URL.'order_info/'.storm_encode($row['tool_order_id']).'/'.storm_encode(3);?>"><i class="fa fa-eye"></i></a>          
                                    </td>
									</tr>
						<?php	}
							} else {
							?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
