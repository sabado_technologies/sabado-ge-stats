<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'role_task/'.$encoded_id;?>">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-4">
									<select name="page_id" class="select2" > 
										<option value="">- Select Task -</option>
										<?php 
											foreach($pages as $row1)
											{
												$selected='';
												if($row1['page_id'] == @$search_dat['page_id'])
												{
													$selected='selected';
												}

												echo '<option value="'.$row1['page_id'].'" '.$selected.'>'.$row1['name'].'</option>';
											}
										?>
									</select>
								</div>
								<div class="col-sm-4">
									<select name="task_id" class="select2"> 
										<option value="">- Select Sub Task -</option>
										<?php 
											foreach($taskList as $row1)
											{
												$selected='';
												if($row1['task_id'] == @$search_dat['task_id'])
												{
													$selected='selected';
												}

												echo '<option value="'.$row1['task_id'].'" '.$selected.'>'.$row1['name'].'</option>';
											}
										?>
									</select>
								</div>
								<div class="col-sm-4">
									<select name="role_id" class="select2"> 
										<option value="">- Select Role -</option>
										<?php 
											foreach($roles_list as $row1)
											{
												$selected='';
												if($row1['role_id'] == @$search_dat['p_role_id'])
												{
													$selected='selected';
												}

												echo '<option value="'.$row1['role_id'].'" '.$selected.'>'.$row1['name'].'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<div class="col-sm-4">
									<label class="col-sm-12 control-label"><strong><p style="margin-top: 4px;">&nbsp;&nbsp;<?php echo "Role :".$role_name;?></p></strong></label>
						
								</div>
								<div class=" col-sm-offset-4 col-sm-2">
									<button type="submit" name="search_role_task" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
									<a href="<?php echo SITE_URL.'role_task/'.$encoded_id; ?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>
								</div>
							</div>
							
						</div>
					</form>
				</div>
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>insert_assign_task">
					<input type="hidden" name="role_id" value="<?php echo $role_id;?>">
					<input type="hidden" name="c_offset" value="<?php echo $current_offset;?>">
					<input type="hidden" name="encoded_id" value="<?php echo $encoded_id;?>">
					<div class="table-responsive col-sm-offset-1 col-sm-10">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center" width="5%"><strong></strong></th>
	                            	<th class="text-center" width="8%"><strong>S.No</strong></th>
	                            	<!-- <th class="text-center"><strong>Role</strong></th> -->
	                                <th class="text-center"><strong>Task</strong></th>
	                                <th class="text-center"><strong>Title</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(count($page_task)>0)
								{
									foreach($page_task as $row)
									{
									?>
										<tr>
											<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'></td>
											<td class="text-center"><?php echo $sn++;?></td>
											<!-- <td class="text-center"><strong><?php echo $row['role_name']; ?></strong></td> -->
											<td class="text-center"><strong><?php echo $row['name']; ?></strong></td>
											<td class="text-center"><strong><?php echo $row['task_title']; ?></strong></td>
										</tr>
										<tr class="details">
											<?php
											if(count($row['page_tasks'])>0)
											{
												?>
												<td colspan="5"> 
													<table class="table" >
														<thead>
															<th class="text-center" style="background-color: #ffa65a !important;">
															</th>
															<th class="text-center" width="25%" style="background-color: #ffa65a !important;">
																<strong>Sub Task </strong>
															</th>
															<th class="text-center" width="30%" style="background-color: #ffa65a !important;">
																<strong>Title </strong>
															</th>
															<th class="text-center" width="60%" style="background-color: #ffa65a !important;">
																<strong>Access Level</strong>
															</th>
														</thead>
														<tbody>
															<?php
															foreach($row['page_tasks'] as $val)
															{
																$role_task = $this->Common_model->get_data_row('role_task',array('role_id'=>$role_id,'task_id'=>$val['task_id']));
																?>
															<tr>
																<td class="text-center">
																	<input type="checkbox"  name="task[<?php echo $val['task_id']?>]" <?php if(count($role_task)>0) { echo 'checked';} ?> value="<?php echo $val['task_id']?>"  class="icheck">
																	<input type="hidden" name="hidden_value[]" value="<?php echo $val['task_id']?>">
																	
																</td>
																<td class="text-center"><?php echo $val['name'];?></td>
																<td class="text-center"><?php echo $val['title'];?></td>
																<td>
																	<div class="col-sm-12 custom_icheck" >
														                <label class="radio-inline"> 
														                    <div class="iradio_square-blue <?php if(count($role_task)>0 && $role_task['task_access']==1){ echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
														                        <input type="radio" class="asset_condition_id" value="1" name="task_radio[<?php echo $val['task_id']?>]" <?php if(count($role_task)>0 && $role_task['task_access']==1){ echo "checked"; } ?> style="position: absolute; opacity: 0;">
														                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
														                    </div> 
														                   	Individual
														                </label>
														                <label class="radio-inline"> 
														                    <div class="iradio_square-blue <?php if(count($role_task)>0 && $role_task['task_access']==2){ echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
														                        <input type="radio" class="asset_condition_id" value="2" name="task_radio[<?php echo $val['task_id']?>]" <?php if(count($role_task)>0 && $role_task['task_access']==2){ echo "checked"; }?> style="position: absolute; opacity: 0;">
														                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
														                    </div> 
														                    Country
														                </label>
														                <label class="radio-inline"> 
														                    <div class="iradio_square-blue <?php if(count($role_task)>0 && $role_task['task_access']==3){ echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
														                        <input type="radio" class="asset_condition_id" value="3" name="task_radio[<?php echo $val['task_id']?>]" <?php if(count($role_task)>0 && $role_task['task_access']==3){ echo "checked"; }?> style="position: absolute; opacity: 0;">
														                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
														                    </div> 
														                    Global
														                </label>
																	</div>
																</td>
															</tr> <?php
															}?>
														</tbody>
													</table>
												</td> <?php
											} 
											else
											{?>
												<td colspan="3" align="center"><span class="label label-primary">No Records Found</span></td> <?php
											} ?>
										</tr> 
							<?php   }
								} else {?>
									<tr><td colspan="5" align="center"><span class="label label-primary">No Records</span></td></tr>
							<?php 	} ?>
							</tbody>
						</table>
	                </div>
	                
                <div class="row">
                	<div class="col-sm-offset-1 col-sm-10">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
                <div class="form-group">
						<div class="col-sm-offset-5 col-sm-5">
							<button class="btn btn-primary" type="submit" name="submit_role_task" value="1"><i class="fa fa-check"></i> Submit</button>
							<a class="btn btn-danger" href="<?php echo SITE_URL;?>role_page"><i class="fa fa-times"></i> Cancel</a>
						</div>
					</div>
                </form>
			</div>
		</div>	
		<?php } ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>