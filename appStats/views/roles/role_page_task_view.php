<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'role_task_assign/'.$encoded_id;?>">
						<div class="col-sm-12">
							
							<div class="col-sm-offset-1 col-sm-4">
								<select class="select2" name="page_id">
									<option value="">- Select Task -</option>
									 <?php
									foreach($pageList as $page)
									{
										$selected = ($page['page_id']==@$search_dat['page_id'])?'selected="selected"':'';
										echo '<option value="'.$page['page_id'].'" '.$selected.'>'.$page['name'].'</option>';
									}
									?>
								</select>
							</div>
							<div class="col-sm-2">
								<button type="submit" name="search_role_task" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
								<a href="<?php echo SITE_URL.'role_task_assign/'.$encoded_id; ?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>
								
							</div>
							<div class="col-sm-5">
								<label class="control-label"><strong><p style="margin-top: 4px;"><?php echo "Role : ".$role_name;?></p></strong></label>
							</div>
						</div>
					</form>
				</div><br>
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>insert_role_task">
					<input type="hidden" name="role_id" value="<?php echo $role_id;?>">
					<input type="hidden" name="c_offset" value="<?php echo $current_offset;?>">
					<input type="hidden" name="encoded_id" value="<?php echo $encoded_id;?>">
					<div class="table-responsive col-sm-offset-1 col-sm-10">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center" width="5%"><strong></strong></th>
	                            	<th class="text-center" width="8%"><strong>S.No</strong></th>
	                                <th class="text-center"><strong>Task</strong></th>
	                                <th class="text-center"><strong>Title</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(count($page_task)>0)
								{
									foreach($page_task as $row)
									{
										$role_task = $this->Common_model->get_data_row('role_page',array('role_id'=>$role_id,'page_id'=>$row['page_id']));
									?>
										<tr>
											<td class="text-center">
												<input type="checkbox"  name="task[<?php echo $row['page_id']?>]" <?php if(count($role_task)>0) { echo 'checked';} ?> value="<?php echo $row['page_id']?>"  class="icheck">
												<input type="hidden" name="hidden_value[]" value="<?php echo $row['page_id']?>">
											</td>
											
											<td class="text-center"><?php echo $sn++;?></td>
											<td class="text-center"><strong><?php echo $row['name']; ?></strong></td>
											<td class="text-center"><strong><?php echo $row['title']; ?></strong></td>
										</tr>
							<?php   }
								} else {?>
									<tr><td colspan="4" align="center"><span class="label label-primary">No Records</span></td></tr>
							<?php 	} ?>
							</tbody>
						</table>
	                </div>
	                
                <div class="row">
                	<div class="col-sm-offset-1 col-sm-10">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
                <div class="form-group">
						<div class="col-sm-offset-5 col-sm-5">
							<button class="btn btn-primary" type="submit" name="submit_role_task" value="1"><i class="fa fa-check"></i> Submit</button>
							<a class="btn btn-danger" href="<?php echo SITE_URL;?>role_page"><i class="fa fa-times"></i> Cancel</a>
						</div>
					</div>
                </form>
			</div>
		</div>	
		<?php } ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	
</script>