<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form class="form-horizontal" role="form" action="<?php echo @$form_action;?>"  parsley-validate novalidate method="post">
							<?php
	                        if($flg==2){
	                            ?>
	                             <input type="hidden" name="encoded_id" value="<?php echo storm_encode(@$lrow['role_id']);?>">
	                             <input type="hidden" name="role_id" id="role_id" value="<?php echo @$lrow['role_id']; ?>">
	                            <?php
	                           }
	                        ?>
							<div class="form-group">
								<label for="inputName" class="col-sm-4 control-label">Role <span class="req-fld">*</span></label>
								<div class="col-sm-5">
									<input type="text" autocomplete="off" required=""  class="form-control" id="role_name" placeholder="Role Name" name="role_name" value="<?php echo @$lrow['name']; ?>">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-5">
									<button class="btn btn-primary" type="submit" value="1" name="submit_designation"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>role_page"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
		<?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<table class="table table-bordered"></table>
			<div class="content">
				<div class="row">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>role_page">
						<div class="col-sm-12">
							<label class="col-sm-1 control-label">Role</label>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="role_name" value="<?php echo @$search_data['rolename'];?>" class="form-control" placeholder="Role Name">
							</div>
							<div class="col-sm-2">
								<button type="submit" name="search_role" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
								<a href="<?php echo SITE_URL.'role_page'; ?>" data-toggle="tooltip" title="Refresh" class="btn btn-success"><i class="fa fa-refresh"></i></a>
								<a href="<?php echo SITE_URL.'add_role';?>" data-toggle="tooltip" title="Add New" class="btn btn-success"><i class="fa fa-plus"></i></a>
							</div>
						</div>
					</form>
				</div><br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Role</strong></th>
                                <th class="text-center"><strong>Action</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($roleResults)>0)
							{
								foreach($roleResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++;?></td>
										<td class="text-center"><?php echo $row['name']; ?></td>
										<td class="text-center">
                                        <?php
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-default" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_role/'.storm_encode($row['role_id']);?>" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-primary" style="padding:3px 3px;" href="<?php echo SITE_URL.'role_task_assign/'.storm_encode($row['role_id']);?>" data-toggle="tooltip" title="Assign tasks"><i class="fa fa-tasks"></i></a>
                                        <a class="btn btn-warning" style="padding:3px 3px;" href="<?php echo SITE_URL.'role_login/'.storm_encode($row['role_id']);?>" data-toggle="tooltip" title="Assign Roles"><i class="fa fa-sitemap"></i></a>
                                        <a class="btn btn-info" style="padding:3px 3px;" href="<?php echo SITE_URL.'role_task/'.storm_encode($row['role_id']);?>" data-toggle="tooltip" title="Assign Sub tasks"><i class="fa fa-info"></i></a>
										<?php } else{?>
                                        <a class="btn btn-default" style="padding:3px 3px;" href="#" readonly data-toggle="tooltip" title="Activate Role to edit"><i class="fa fa-pencil"></i></a>
										<?php }
                                        if($row['status']==1){
                                        ?>
                                        <a class="btn btn-danger" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_role/'.storm_encode($row['role_id']);?>" data-toggle="tooltip" title="Deactivate"><i class="fa fa-trash-o"></i></a>
                                        <?php
                                        }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_role/'.storm_encode($row['role_id']);?>" data-toggle="tooltip" title="Activate"><i class="fa fa-check"></i></a>
                                        <?php
                                        }
                                        ?>
                                    </td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="3" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
		<?php } ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>