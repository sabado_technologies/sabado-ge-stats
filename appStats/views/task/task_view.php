<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<?php
		if(isset($flg))
		{
	    ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form class="form-horizontal" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
							<?php
	                        if($flg==2){
	                            ?>
	                            <input type="hidden" name="encoded_id" value="<?php echo storm_encode(@$lrow['task_id']);?>">
	                            <input type="hidden" name="task_id" id = "task_id" value="<?php echo @$lrow['task_id'];?>">
	                            
	                            <?php
	                           }
	                        ?>

							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Task <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<select class="select2" id="page_id" name="page_id" required>
											<option value="">- Select Task -</option>
											 <?php
											foreach($pageList as $page)
											{
												$selected = ($page['page_id']==@$lrow['page_id'])?'selected="selected"':'';
												echo '<option value="'.$page['page_id'].'" '.$selected.'>'.$page['name'].'</option>';
											}
											?>
									</select>
									</div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Sub Task Name <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<input type="text" autocomplete="off" required id="task_id" class="form-control" placeholder="Sub Task Name" name="name" value="<?php echo @$lrow['name']; ?>">
									</div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Sub Title <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<textarea required class="form-control" placeholder="Sub Title" name="title" ><?php echo @$lrow['title']; ?></textarea>
									</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-5">
									<button class="btn btn-primary" type="submit" name="submit_task" value="1"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo SITE_URL;?>sub_task"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
		<?php 
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{   
	    	?>
			<div class="block-flat">
				<table class="table table-bordered"></table>
				<div class="content">
					<div class="row">
						<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>sub_task">
							<div class="col-sm-12">
									<div class="col-sm-4">
									<?php echo form_dropdown('page_id',$page_name,@$search_data['page_id'],'class="select2"');?>
									</div>
									<div class="col-sm-4">
										<input type="text" autocomplete="off" name="task_name" value="<?php echo @$search_data['task_name'];?>" placeholder="Sub Task Name" class="form-control">
									</div>

								<div class="col-sm-4">
									<button type="submit" name="searchtask" data-toggle="tooltip" title="Search" value="1" class="btn btn-success"><i class="fa fa-search"></i></button>
									<a href="<?php echo SITE_URL.'sub_task';?>" data-toggle="tooltip" class="btn btn-success" title="Refresh"><i class="fa fa-refresh"></i></a>
									<a href="<?php echo SITE_URL.'add_task';?>" data-toggle="tooltip" class="btn btn-success" title="Add New"><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</form>
					</div><br>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
	                            	<th class="text-center"><strong>S.No</strong></th>
		                        	<th class="text-center"><strong>Task</strong></th>
		                            <th class="text-center"><strong>Sub Task Name</strong></th>
		                            <th class="text-center"><strong>Sub Title</strong></th>
		                            <th class="text-center"><strong>Action</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(count($taskResults)>0)
								{
									foreach($taskResults as $row)
									{
									?>
										<tr>
											<td class="text-center"><?php echo $sn++;?></td>
											<td class="text-center"><?php echo @$row['page_name'];?> </td>
											<td class="text-center"><?php echo $row['name']; ?></td>
											<td class="text-center"><?php echo $row['title']; ?></td>
											<td class="text-center">
											<?php if($row['status']==1){?>
	                                        <a class="btn btn-default" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_task/'.storm_encode($row['task_id']);?>" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
											<?php } else{?>
	                                        <a class="btn btn-default" style="padding:3px 3px;" href="#" readonly data-toggle="tooltip" title="Activate task to edit"><i class="fa fa-pencil"></i></a>
											<?php }
	                                        if($row['status']==1){
	                                        ?>
	                                        <a class="btn btn-danger" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_task/'.storm_encode($row['task_id']);?>" data-toggle="tooltip" title="Deactivate"><i class="fa fa-trash-o"></i></a>
	                                        <?php
	                                        }
	                                        if($row['status']==2){
	                                        ?>
	                                        <a class="btn btn-info" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_task/'.storm_encode($row['task_id']);?>" data-toggle="tooltip" title="Activate"><i class="fa fa-check"></i></a>
	                                        <?php
	                                        }
	                                        ?>
	                                    </td>
										</tr>
							<?php   }
								} else {?>
									<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
	                    <?php 	} ?>
							</tbody>
						</table>
	                </div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>          		
				</div>
			</div>	
		<?php } ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>