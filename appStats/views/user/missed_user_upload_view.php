<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        	<?php echo $this->session->flashdata('response'); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>user">
					<div class="row">
						<div class="col-sm-9 col-md-9"></div>
							<div class="col-sm-3 col-md-3">				
								<a href="<?php echo SITE_URL.'user'; ?>" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
								<a href="<?php echo SITE_URL.'download_missed_user_list/'.storm_encode($upload_id); ?>" class="btn btn-success"><i class="fa fa-cloud-download"></i> Download List</a>
							</div>	
					</div>
				</form><br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>SSO ID</strong></th>
                                <th class="text-center"><strong>Name</strong></th>
                                <th class="text-center"><strong>Role</strong></th>
                                <th class="text-center"><strong>Reporting Manager</strong></th>
                                <th class="text-center"><strong>Designation</strong></th>
                                <th class="text-center"><strong>Wh Code</strong></th>
                                <th class="text-center"><strong>Branch</strong></th>
                                <th class="text-center"><strong>Address1</strong></th>
                                <th class="text-center"><strong>FE Position</strong></th>
                                <th class="text-center"><strong>Assigned Location</strong></th>
                                <th class="text-center"><strong>Modality Code</strong></th>
                                <th class="text-center"><strong>Remarks</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1;
							if(count($missedResults)>0)
							{
								foreach($missedResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $i++; ?></td>
										<td class="text-center"><?php echo $row['sso_id']; ?></td>
										<td class="text-center"><?php echo $row['name']; ?></td>
										<td class="text-center"><?php echo $row['role_name']; ?></td>
										<td class="text-center"><?php if($row['rm_id']!=0){
											echo $row['rm_id']; }?></td>
										<td class="text-center"><?php echo $row['designation_name']; ?></td>
										<td class="text-center"><?php echo $row['wh_code']; ?></td>
										<td class="text-center"><?php echo $row['branch_name']; ?></td>
										<td class="text-center"><?php echo $row['address1']; ?></td>
										<td class="text-center"><?php echo $row['fe_position']; ?></td>
										<td class="text-center"><?php echo $row['assigned_location']; ?></td>
										<td class="text-center"><?php echo $row['modality_list']; ?></td>
										<td class="text-center"><?php echo $row['remarks']; ?></td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="13" align="center"><span class="label label-primary">No Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>
				</div>
			</div>		    
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
