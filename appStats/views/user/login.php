<!DOCTYPE html>
<html lang="en">

<head>
    <script>
        var SITE_URL = "<?php echo SITE_URL; ?>";
        var ASSET_URL = "<?php echo assets_url();?>";
    </script>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>STATS Login</title>
    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    

    <link rel="shortcut icon" href="<?php echo assets_url();?>images/favicon_1.png">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo assets_url(); ?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo assets_url();?>fonts/font-awesome-4/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="<?php echo assets_url();?>css/styles-1.css" rel="stylesheet" />
    <style type="text/css">
        
        html { 
          background: url(<?php echo assets_url();?>images/back.jpg) no-repeat center center fixed; 
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        }
    </style> 

</head>

<body>
    <!--login modal-->
    <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="margin-left:370px;">
            <div class="modal-content" style="width:598px;height:565px;position:center;">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="<?php echo assets_url();?>images/wipro.png" style="padding-left: 12px;"> 
                        </div>
                        <div class="col-md-6" style="padding-left: 0px;">
                            <img src="<?php echo assets_url();?>images/stats.png"> 
                        </div>
                        <div class="col-md-3">
                        
                            <img src="<?php echo assets_url();?>images/ge.png">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        
                        <div class="modal-body" id="divLogin" style="overflow-y:hidden; height: 350px;">
                        <?php echo $this->session->flashdata('response'); ?>
                            <form class="form col-md-12 center-block" method = "POST"   parsley-validate novalidate>
                                <div class="form-group"><br><br>
                                    <input type="text" autocomplete="off" class="form-control input-lg only-numbers" required name = "sso_id" maxlength = "10" placeholder="SSO ID">
                                    <span><h4>&nbsp;  <?php //echo $ssoerr; ?></h4></span>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control input-lg" required name="password" maxlength = "20" placeholder="Password" >
                                    <span><h4>&nbsp;  <?php //echo $passerr; ?> </h4></span>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-2"> </div>
                                    <div class="col-md-8"> 
                                        <div class="form-group">
                                            <button class="btn btn-lg btn-block btn-primary" type="submit" name="submit">Login</button>
                                             <!-- <a href="#" id="forgetAnchor"><p style="text-align:center">Forgot Password</p></a> -->
                                            <span align = "center"><h4>&nbsp;  <?php //echo $autherr; ?></h4></span>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row" id="divForget">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="modal-body">
                            <form class="form col-md-12 center-block" action="<?php echo SITE_URL.'forgotPassword'; ?>" method="POST"  parsley-validate novalidate>
                                <div class="form-group"><br><br>
                                <input type="text" autocomplete="off" class="form-control input-lg only-numbers" required name="sso_id" maxlength = "19" placeholder="Enter SSO ID">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-2"> </div>
                                    <div class="col-md-8"> 
                                        <div class="form-group">
                                            <button class="btn btn-lg btn-block btn-primary" type="submit" value="1" name="Forgetsubmit">Get Reset Link</button>
                                             <a class="btn btn-lg btn-block btn-default" id="LoginForm">Login</a>
                                            <br><br>     
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery.js"></script>
    <script src="<?php echo assets_url(); ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo assets_url(); ?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo assets_url(); ?>js/jquery-ui.min.js" type="text/javascript"></script>   
    <script type="text/javascript" src="<?php echo assets_url(); ?>js/behaviour/general.js"></script>
    <script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery.parsley/parsley.js"></script>

</body>
</html>