<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
      <div class="row"> 
      <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
	<?php
	if(isset($flg))
	{
    ?>
    <div class="row"> 
		<div class="col-sm-12 col-md-12">
			<div class="block-flat">
				<div class="content">
					<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
						<?php
                        if($flg==2){
                            ?>
                            <input type="hidden" name="encoded_id" value="<?php echo storm_encode($lrow['sso_id']);?>">
                            <?php
                           }
                        ?>
                        <input type="hidden" name="s_id" id="sso_id" value="<?php echo @$lrow['sso_id'];?>">
                        <input type="hidden" name="r_id" id="r_id" value="<?php echo @$lrow['role_id'];?>">
                        <input type="hidden" name="c_id" id="country_id" value="<?php echo @$country_id;?>">
                        <div class="header">
							<h5 align="left" style="color: #3380FF;"><strong>Country : <?php echo $country_name; ?></strong></h5>
						</div></br>
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">SSO ID <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<input type="text" autocomplete="off" required maxlength="30" class="form-control only-numbers" id="sso_num" placeholder="SSO ID" name="sso_num" value="<?php echo @$lrow['sso_id']; ?>" <?php if($flg == 2){ echo "readonly"; } ?>>
										</div>
								</div>
                        	</div>
                        	<div class="col-md-6">
                        		<div class="form-group">
									<label for="inputName" class="col-sm-3 control-label">Name <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<input type="text" autocomplete="off" required class="form-control" placeholder="User Name" name="user_name" value="<?php echo @$lrow['name']; ?>">
										</div>
								</div>
                        	</div>
                        </div>
                        <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Mobile Num <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<input type="text" autocomplete="off" required maxlength="12" minlength="10" class="form-control only-numbers" placeholder="Mobile Number" name="mobile_num" value="<?php echo @$lrow['mobile_no']; ?>">
									</div>
								</div>
							</div> 		
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-3 control-label">Email <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<input type="email" autocomplete="off" required class="form-control" placeholder="Email" name="email" value="<?php echo @$lrow['email']; ?>">
										</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label rm">Reporting Manager <?php if(@$flg==2){ if(@$lrow['role_id']==2 ||@$lrow['role_id']==5 ||@$lrow['role_id']==6){ echo '<span class="req-fld">*</span>'; } }?></label>
										<div class="col-sm-6 rm_div">
											<select name="rm_id" class="reporting_manager_id" style="width: 100%;"> 
												<option value="">- Reporting Manager -</option>
												<?php 
												if($flg == 2 && @$lrow['rm_id']!='' && @$lrow['rm_id']!=0)
												{
													echo "<option value=".$lrow['rm_id']." selected>".$lrow['reporting_manager_name']."</option>";
												}
												?>
											</select>
											<span class="req-fld rm_error"></span>
										</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-3 control-label">Position <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<select name="fe_position" required class="fe_position" style="width: 100%;"> 
												<option value="">- Position (City) -</option>
												<?php 
												if($flg == 2)
												{
													echo "<option value=".$lrow['fe_position']." selected>".$lrow['fe_position_name']."</option>";
												}
												?>
											</select>
										</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Warehouse <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<select class="select2 wh_id" name="warehouse_id"  required>
													<option value="">- Warehouse -</option>
													 <?php
													foreach($warehouseList as $row)
													{
														$selected = ($row['wh_id']==$lrow['wh_id'])?'selected="selected"':'';
														echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' -('.$row['name'].')</option>';
													}
													?>
											</select>
										</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-3 control-label">Branch <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<select class="select2" name="branch_id" required>
													<option value="">- Branch -</option>
													 <?php
													foreach($branchList as $branch)
													{
														$selected = ($branch['branch_id']==$lrow['branch_id'])?'selected="selected"':'';
														echo '<option value="'.$branch['branch_id'].'" '.$selected.'>'.$branch['name'].'</option>';
													}
													?>
											</select>
										</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Address 1 <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<textarea class="form-control" name="address1" required><?php echo @$lrow['address1']; ?></textarea>
										</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-3 control-label">Address 2</label>
										<div class="col-sm-6">
											<textarea class="form-control" name="address2" ><?php echo @$lrow['address2']; ?></textarea>
										</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Role <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<select class="select2 role_id" name="role_id" required>
													<option value="">- Role -</option>
													 <?php
													foreach($roleList as $role)
													{
														$ses_role = $this->session->userdata('role_id');
														if($ses_role == 1)
														{
															if($role['role_id'] == 4)
															{
																continue;
															}
														}
														$selected = ($role['role_id']==$lrow['role_id'])?'selected="selected"':'';
														echo '<option value="'.$role['role_id'].'" '.$selected.'>'.$role['name'].'</option>';
													}
													?>
											</select>
										</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputName" class="col-sm-3 control-label">Designation <span class="req-fld">*</span></label>
										<div class="col-sm-6">
											<select class="form-control" name="designation_id" id="designation_id" required>
													<option value="">- Designation -</option>
													<?php if($flg==2)
													{
													 	foreach($designationList as $desig)
														{
															$selected = ($desig['designation_id']==$lrow['designation_id'])?'selected="selected"':'';
															echo '<option value="'.$desig['designation_id'].'" '.$selected.'>'.$desig['name'].'</option>';
														}
													}
													?>
											</select>
										</div>
								</div>
							</div>
						</div>
						<div class="row location_row hidden">
							<div class="col-md-6 ">
								<div class="form-group">
									
									<label for="inputName" class="col-sm-4 control-label llabel"><?php if($flg == 1){ echo "Location"; } else if(@$lrow['role_id']==5){ echo "Zone"; }  else if(@$lrow['role_id']==6){ echo "Country"; }?> <span class="req-fld">*</span></label>

										<div class="col-sm-6 loc_div">
											<select class="form-control location_id" name="location_id" id="location">
													<?php if($flg==2)
													{
														if($lrow['role_id']==5)
														{
															echo "<option value='0'>- Select Zone -</option>";

															foreach($location_zn_List as $loc)
															{
																$selected = ($loc['location_id']==$lrow['location_id'])?'selected="selected"':'';
																echo '<option value="'.$loc['location_id'].'" '.$selected.'>'.$loc['name'].'</option>';
															}
														}
														else if($lrow['role_id']==6)
														{
															echo "<option value='0'>- Select Country -</option>";

															foreach($location_zn_List as $loc)
															{
																$selected = ($loc['location_id']==$lrow['location_id'])?'selected="selected"':'';
																echo '<option value="'.$loc['location_id'].'" '.$selected.'>'.$loc['name'].'</option>';
															}
														}
													}
													?>
											</select>
										</div>
								</div>
							</div>
							<div class="col-md-6 modality">
								<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Modalities <span class="req-fld">*</span></label>
									<div class="col-sm-6">
										<select name="modality[]" multiple="multiple" class="select2-container select2-container-multi select2 modality-multiple" style=" width: 100%;"> 
											<?php foreach($modalityList as $mod)
											{
												$selected = '';
												$modality_arr = array(0);
												if($flg==2)
												{
													if(count($modality_arr)>0)
													{
														$modality_arr = array();
														foreach($modalities as $mrow)
														{
															$modality_arr[] = $mrow['modality_id'];
														}
													}
												}
												if(in_array(@$mod['modality_id'], $modality_arr))
												{
													$selected = 'selected';
												}
												else
												{
													$selected='';
												}
												echo '<option value="'.$mod['modality_id'].'" '.$selected.'>'.$mod['name'].'</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<!--created by gowri on 27/aug/18-->
						<div class="row location_row1 hidden">
							<div class="col-md-6 ">
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label log_zones">Warehouse Zone</label>
									<div class="col-sm-6 loc_div">
										<select class="form-control loc_zone" name="location_id" id="location1">
											<?php if($flg==2)
											{
												if($lrow['role_id']==3)
												{
													echo "<option value='0'>- Select Zone -</option>";
													foreach($location_zn_List as $loc)
													{
														$selected = ($loc['location_id']==$lrow['location_id'])?'selected="selected"':'';
														echo '<option value="'.$loc['location_id'].'" '.$selected.'>'.$loc['name'].'</option>';
													}
												}
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6 warehouse <?php if($flg == 1){ echo "hidden";}else if($flg ==2 && count($zone_wh_list)==0){ echo "hidden";} ?>">
								<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label wh_zone" >Warehouses </label>
									<div class="col-sm-6">
										<div class="radio a" >
											<div class="radio"> 
							                	<label>
							                		<input id="select_all_wh" name="warehouses"
							                		<?php if($flg==2 && (count(@$zone_wh_list)==count(@$sso_wh_arr))){ echo "checked"; } ?> type="checkbox"><strong> Select All</strong>
							                	</label> 
							                </div>
							                <?php 
							                if($flg==2 && count($zone_wh_list)>0)
											{
												foreach($zone_wh_list as $row)
							                	{
													$checked = "";
													foreach($sso_wh_arr as $wrow)
													{
														if($row['wh_id']== @$wrow['wh_id'])
														{ 
															$checked='checked';
															break;
														} 
													} ?>
												
													<div class="radio"> 
								                		<label> 
								                			<?php echo '<input type="checkbox" class="checkbox_wh" name="warehouse_arr[]" value="'.$row['wh_id'].'" '.@$checked.' >' ?> <?php echo $row['wh_name'];?>
								                		</label> 
								                	</div>
								                	<?php
								                }
								            }
								            ?>
						                </div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-5">
								<button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure you want to Submit ?')" value="1" name="submitUser"><i class="fa fa-check"></i> Submit</button>
								<a class="btn btn-danger" href="<?php echo SITE_URL;?>user"><i class="fa fa-times"></i> Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>				
		</div>
	</div>
	<?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>user">
					<div class="row">
						<div class="col-sm-12">
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="sso_num" placeholder="SSO ID" value="<?php echo @$search_data['ssonum'];?>" class="form-control">
							</div>
							<div class="col-sm-3">
								<select class="select2" name="role_id" >
									<option value="">- Role -</option>
									 <?php
									foreach($roleList as $branch)
									{
										$selected = ($branch['role_id']==$search_data['roleid'])?'selected="selected"':'';
										echo '<option value="'.$branch['role_id'].'" '.$selected.'>'.$branch['name'].'</option>';
									}
									?>
								</select>
							</div>
							<div class="col-sm-3">
								<select class="select2" name="branch_num" >
											<option value="">- Branch -</option>
											 <?php
											foreach($branchList as $branch)
											{
												$selected = ($branch['branch_id']==$search_data['branchnum'])?'selected="selected"':'';
												echo '<option value="'.$branch['branch_id'].'" '.$selected.'>'.$branch['name'].'</option>';
											}
											?>
									</select>
							</div>
							<div class="col-sm-3">
								
								<input type="text" autocomplete="off" name="fe_position" placeholder="FE Position" value="<?php echo @$search_data['fe_position'];?>" class="form-control">
							</div>
						</div>
						<div class="col-sm-12" style="margin-top: 5px;">
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="user_name" placeholder="User Name" value="<?php echo @$search_data['username'];?>" class="form-control">
							</div>
							<div class="col-sm-3">
								<select class="select2" name="designation_num" >
									<option value="">- Designation -</option>
									 <?php
									foreach($designationList as $desig)
									{
										$selected = ($desig['designation_id']==$search_data['designationnum'])?'selected="selected"':'';
										echo '<option value="'.$desig['designation_id'].'" '.$selected.'>'.$desig['name'].'</option>';
									}
									?>
								</select>
							</div>
							<div class="col-sm-3">
								<select class="select2 wh_id" name="wh_id" >
									<option value="">- Warehouse -</option>
									 <?php
									foreach($whList as $wh)
									{
										$selected = ($wh['wh_id']==$search_data['whid'])?'selected="selected"':'';
										echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_code'].' -('.$wh['name'].')</option>';
									}
									?>
								</select>
							</div>
							<?php if($task_access == 3 && @$_SESSION['header_country_id']==''){?>
							<div class="col-sm-3">
								<select class="select2" name="country_id" >
									<option value="">- Country -</option>
									 <?php
									foreach($countryList as $country)
									{
										$selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
										echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
									}
									?>
								</select>
							</div>
							<?php } ?>
						</div>
					</div>
					<div class="row">
						<div class="content">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
							<div class="col-sm-12">
								<div class="col-sm-6 col-md-6">							
									<a href="<?php echo SITE_URL.'add_user'; ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
									<a href="<?php echo SITE_URL.'bulkupload_user'; ?>" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Upload in Bulk</a>
									
								</div>	
								<div class="col-sm-6 col-md-6" align="right">							
									<button type="submit" name="searchUser" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
									<a href="<?php echo SITE_URL.'user'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
									<button type="submit" data-toggle="tooltip" title="Download" name="download_user" value="1" formaction="<?php echo SITE_URL.'download_user';?>" class="btn btn-success" onclick="return confirm('Are you sure you want to Download?')"><i class="fa fa-cloud-download"></i>Download</button>  
								</div>		
												
									
							</div>
						</div>
					</div>
				</form>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>SSO ID</strong></th>
                                <th class="text-center"><strong>Name</strong></th>
                                <th class="text-center"><strong>Warehouse</strong></th>
                                <th class="text-center"><strong>Role</strong></th>
                                <th class="text-center"><strong>Designation</strong></th>
                                <th class="text-center"><strong>Branch</strong></th>
                                
                                <th class="text-center"><strong>Mobile No.</strong></th>
                                <th class="text-center"><strong>FE Position</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center" width="12%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if(count($userResults)>0)
							{
								foreach($userResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++; ?></td>
										<td class="text-center"><?php echo $row['sso_id']; ?></td>
										<td class="text-center"><?php echo $row['name']; ?></td>
										<td class="text-center"><?php if($row['wh_arr']!=''){ echo $row['wh_arr']; }else { echo $row['wh'];} ?></td>
										<td class="text-center"><?php echo $row['rle']; ?></td>
										<td class="text-center"><?php echo $row['designation_name']; ?></td>
										<td class="text-center"><?php echo $row['branch_name']; ?></td>
										
										
										<td class="text-center"><?php echo $row['mobile_no']; ?></td>
										<td class="text-center"><?php echo $row['location_name']; ?></td>
										<td class="text-center"><?php echo $row['country']; ?></td>
										<td class="text-center" >
                                        
                                        <?php
                                        $ses_role = $this->session->userdata('role_id');
                                        ?>
                                        <a class="btn btn-default" data-container="body" data-placement="top"  data-toggle="tooltip" title="Update User Details" <?php if($ses_role==1 && $row['role_id']==4){ echo "disabled";}?> <?php if($row['status']==2){ echo "disabled";}?> style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_user/'.storm_encode($row['sso_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php if($row['status'] == 1){ ?>
                                        <a class="btn btn-danger" data-container="body" data-placement="top"  data-toggle="tooltip" title="Deactivate User" <?php if($ses_role==1 && $row['role_id']==4){ echo "disabled";}?> style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Deactivate?')" href="<?php echo SITE_URL.'deactivate_user/'.storm_encode($row['sso_id']);?>"><i class="fa fa-trash-o"></i></a>
                                        <?php }
                                        if($row['status']==2){
                                        ?>
                                        <a class="btn btn-info" data-container="body" data-placement="top"  data-toggle="tooltip" title="Activate User" <?php if($ses_role==1 && $row['role_id']==4){ echo "disabled";}?> style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Activate?')" href="<?php echo SITE_URL.'activate_user/'.storm_encode($row['sso_id']);?>"><i class="fa fa-check"></i></a>
                                        <?php } ?>
                                        
                                    	<?php 
											$data = 
											array('primary_key' => storm_encode(@$row['sso_id']),
										          'table_name'  => 'user',
										          'work_flow'   => 'user_master',
										          'main_table'  => 'user',
										          'main_key'    => $row['sso_id']);
											$send_data = implode('~',$data); 
										?>
										<a class="btn btn-primary" 
											style="padding:3px 3px;"
											href="<?php echo SITE_URL.'get_audit_page/'.$send_data;?>"
											target="_blank"
											data-toggle="tooltip" title="Audit Trail"><i class="fa fa-book"></i>
										</a>

										<?php if( $row['role_id'] ==9  && $row['country_region']==3  ){?>

                                        <a class="btn btn-warning" data-container="body" data-placement="top"  data-toggle="tooltip" title="Assign Countries" style="padding:3px 3px;margin-top: 4px;" href="<?php echo SITE_URL.'user_country/'.storm_encode($row['sso_id']);?>"><i class="fa fa-plane"></i></a>
                                    	<?php } ?> 
                                    </td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="12" align="center"><span class="label label-primary">No User Records</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
	<?php } ?>
	</div>
	</div>
	</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".modality-multiple").select2({
		    placeholder: "- Select Modality -"
		});
	});
</script>