<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="images/favicon.png">

	<title>STATS LogOut</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
	<link href="<?php echo assets_url();?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

	<link rel="stylesheet" href="fonts/font-awesome-4/css/font-awesome.min.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="../../assets/<?php echo assets_url();?>js/html5shiv.js"></script>
	  <script src="../../assets/<?php echo assets_url();?>js/respond.min.js"></script>
	<![endif]-->

	<!-- Custom styles for this template -->
	<link href="<?php echo assets_url();?>css/style2.css" rel="stylesheet" />	

</head>

<body class="texture">

<div id="cl-wrapper" class="error-container">
	<div class="page-error">
		<h1 class="number text-center">STATS</h1>
		<h2 class="description text-center">You are successfully Logged out!</h2>
		<h3 class="text-center"> <a href="<?php echo SITE_URL;?>">Click here to Login<a></h3>
	</div>
	<div class="text-center copy">&copy; 2018 <a href="#">Wipro GE</a></div>

	
</div>

<script src="<?php echo assets_url();?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo assets_url();?>js/behaviour/general.js"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo assets_url();?>js/behaviour/voice-commands.js"></script>
  <script src="<?php echo assets_url();?>js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url();?>js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo assets_url();?>js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo assets_url();?>js/jquery.flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo assets_url();?>js/jquery.flot/jquery.flot.labels.js"></script>
</body>
</html>
