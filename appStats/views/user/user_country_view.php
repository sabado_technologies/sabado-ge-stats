<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
	        <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
								<input type="hidden" name="encoded_id" value="<?php echo storm_encode($user_data['sso_id']);?>">
								<input type="hidden" name="country_region" class="country_region" value="<?php echo $region_id;?>">
								<input type="hidden" name="user_role" class="user_role" value="<?php echo $user_data['role_id'];?>">

		                        <div class="header">
									<h5 align="left" style="color: #3380FF;"><strong>User : <?php echo $user_data['sso_id'].' ('.$user_data['name'].')';; ?></strong></h5>
								</div></br>
		                        <div class="row">
		                        	<div class="col-md-12">
		                        		<div class="form-group">
											<label for="inputName" class="col-sm-5 control-label">Countries <span class="req-fld">*</span></label>
												<div class="col-sm-6">
													<?php
													$checked1 = '';
													if(count($country_list) == count($assigned_country))
													{
														$checked1 = 'checked';
													}
													?>
							                		<input class="select_all" name="select_all" type="checkbox" <?php echo $checked1; ?>> <span>Select All</span><br>
							                	
													<?php 
													foreach($country_list as $row)
													{
														if($user_data['role_id']==9 && ($row['location_id'] == getAseanRegionId() ))
															continue;
														$checked = '';
														if(in_array($row['location_id'], $assigned_country))
														{
															$checked = 'checked';
														}
													if($user_data['country_id'] == $row['location_id'])
													{ ?>
														<i class="fa fa-check"></i>
													<?php
													}
													else
													{
													?>
														<input type="checkbox" class="checkbox_country" name="country[]" <?php echo $checked; ?> value="<?php echo $row['location_id']; ?>"><?php
													}?>
													 <span><?php echo $row['name']; ?></span><br><?php 
													} ?>
												</div>
											
										</div>
		                        	</div>
		                        </div>
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary" type="submit" value="1" name="submitUser"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL;?>user"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
							</form>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(".select_all").change(function(){  //"select all" change 
    	$(".checkbox_country").prop('checked', $(this).prop("checked"));
	});

//".checkbox_modality" change 
$('.checkbox_country').change(function(){ 
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked"))
    { 
        $(".select_all").prop('checked', false); 
    }
    //check "select all" if all checkbox items are checked
    if ($('.checkbox_country:checked').length == $('.checkbox_country').length)
    {
        $(".select_all").prop('checked', true);
    }
});
$("form").submit(function( e ) {
  var user_role = $('.user_role').val();
  var country_region = $('.country_region').val();
  var check_length = $('.checkbox_country:checked').length;
  if(check_length >1 && user_role == 9 && country_region == 3)
  {
  	alert("Sorry!.You can not assign more than 2 countries for Tool Coordinator");
  	return false;
  }
  
});
</script>