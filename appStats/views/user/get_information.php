<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
	    <div class="col-sm-12 col-md-12">
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'get_information_data'; ?>"  parsley-validate novalidate method="post">
		                        <div class="row">
		                        	<div class="col-md-12">
		                        		<div class="form-group">
											<label for="inputName" class="col-sm-2 control-label">Query Type <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<select name="query_type" required class="select2">
														<option value="">- Query Type -</option>
														<option value="1">Get Result Query</option>
														<option value="2">Insert Query</option>
														<option value="3">Update Query</option>
														<option value="4">Delete Query</option>
													</select>
												</div>
										</div>
		                        	</div>
		                        	<div class="col-md-12">
		                        		<div class="form-group">
											<label for="inputName" class="col-sm-2 control-label">Query <span class="req-fld">*</span></label>
												<div class="col-sm-8">
													<textarea name="query" required class="form-control"></textarea>
												</div>
										</div>
		                        	</div>
		                        	
		                        </div>
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary" type="submit" value="1" name="submitUser" onclick="return confirm('Are you sure you want to Submit?')"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL.'get_information';?>"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
							</form>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>