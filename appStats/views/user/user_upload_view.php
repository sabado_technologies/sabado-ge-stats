<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:55%; z-index:2; opacity:0"><img src="<?php echo SITE_URL1;?>assets/images/ajax-loading-img.gif" /></div>
<div class="cl-mcont" id="orderCartContainer">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        	<?php echo $this->session->flashdata('response'); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
	                    <div class="content">
	                    	<form class="form-horizontal" id="bulkUploadFrm" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data" >
	                        	<div class="form-group">
	                                <label for="inputName" class="col-sm-4 control-label">Upload CSV File</label>
	                                <div class="col-sm-6">
	                                 	<input type="file" name="uploadCsv" id="uploadCsv" accept=".csv" >
	                                    <p><small>Note: Upload Data In Specified CSV Format Only</small></p>
									</div>
	                            </div>
	                            <div class="form-group">
	                                <div class="col-sm-offset-4 col-sm-6">
	                                    <button class="btn btn-primary upload_user" type="submit"  value ="1" name="bulkUpload"><i class="fa fa-cloud-upload"></i> Upload Data</button>
	                                    <a class="btn btn-danger" href="<?php echo SITE_URL;?>user"><i class="fa fa-times"></i> Cancel</a>
	                                </div>
	                            </div>
	                        </form>
	                        <div class="row">
	                         	<div class="col-sm12  col-md-12">
	                            	<div class="block-flat1">
	                                    <div class="header">							
	                                        <h4><strong>Instructions</strong></h4>
	                                    </div>
	                                    <div class="content">
	                                    	<div class="col-sm-12">
	                                        	<div class="col-sm-4">
	                                            	<h5><strong>Bulk Upload Template</strong></h5>
	                                                <p>
														<a href="<?php echo SITE_URL1;?>samples/user_sample_template.csv" class="btn btn-primary"><i class="fa fa-users"></i>User Template</a>
	                                                </p>
	                                                <?php if(@$failed_ib_upload != '') { ?>
	                                                <p>
														<a href="<?php echo SITE_URL.'download_missed_ib_uploads/'.storm_encode($failed_ib_upload); ?>" class="btn btn-danger">
														<i class="fa fa-gavel"></i> Missed IB Uploads </a>
	                                                </p> <?php }?>
	                                                <br>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                         </div>
	                    </div>
					</div>
				</div>
			</div>		    
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
