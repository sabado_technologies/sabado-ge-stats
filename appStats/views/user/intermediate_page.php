<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
	    <div class="row"> 
			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="content">
						<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post">
							<div class="row">
	                        	<div class="col-md-12">
	                        		<div class="form-group">
										<label for="inputName" class="col-sm-4 control-label">Country <span class="req-fld">*</span></label>
											<div class="col-sm-4">
												<select class="select2" name="country_id" required>
													<option value="">- Country -</option>
													 <?php
													foreach($countryList as $country)
													{
														$selected = ($country['location_id']==
															@$selected_country)?'selected="selected"':'';
														echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
													}
													?>
												</select>
											</div>
									</div>
	                        	</div>
	                        </div>
	                        <div class="form-group">
								<div class="col-sm-offset-4 col-sm-5">
									<button class="btn btn-primary" type="submit" value="1" name="SubmitCountry"><i class="fa fa-check"></i> Submit</button>
									<a class="btn btn-danger" href="<?php echo @$cancel; ?>"><i class="fa fa-times"></i> Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>