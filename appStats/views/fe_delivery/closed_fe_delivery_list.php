<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
        <?php
		if(isset($flg))
		{
	    ?>
	    <div class="block-flat">
			<div class="content">
				<div class="col-sm-12 col-md-12">
					<div class="row">							
						<div class="row" style="padding: 10px;">							
							<div class="header">
								<h4 align="center"><strong>Order Info</strong></h4>
							</div>
						</div>
						<div class="col-md-6">
							<table class="no-border">
								<tbody class="no-border-x no-border-y">
									<tr>
										<td class="data-lable"><strong>Order Number:</strong></td>
								        <td class="data-item"><?php echo @$trow['order_number'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Request Date:</strong></td>
								        <td class="data-item"><?php echo indian_format(@$trow['request_date']);?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Address1:</strong></td>
								        <td class="data-item"><?php echo @$trow['address1'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>City:</strong></td>
								        <td class="data-item"><?php echo @$trow['address3'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Pin Code:</strong></td>
								        <td class="data-item"><?php echo @$trow['pin_code'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Courier Name:</strong></td>
								        <td class="data-item"><?php echo @$drow['courier_name'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Contact Person:</strong></td>
								        <td class="data-item"><?php echo @$drow['contact_person'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Expected Delivery Date:</strong></td>
								        <td class="data-item"><?php echo indian_format(@$drow['expected_delivery_date']);?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-6">
							<table class="no-border">
								<tbody class="no-border-x no-border-y">
									<tr>
										<td class="data-lable"><strong>SSO Detail:</strong></td>
										<td class="data-item"><?php echo @$trow['sso_id'].' - '.$trow['name']?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Return Date:</strong></td>
										<td class="data-item"><?php echo indian_format(@$trow['return_date'])?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Address2:</strong></td>
								        <td class="data-item"><?php echo @$trow['address2'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>State:</strong></td>
								        <td class="data-item"><?php echo @$trow['address4'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Courier Type:</strong></td>
								        <td class="data-item"><?php if(@$drow['courier_type']==1)
										        {
										            echo "By Courier";
										        }
										        else if(@$drow['courier_type']==2)
										        {
										        	echo "By Hand";
										        }
										        else if(@$drow['courier_type']==3)
										        {
										        	echo "By Dedicated";
										        }
										        ?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Docket/AWB number:</strong></td>
								        <td class="data-item"><?php echo @$drow['courier_number'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Phone Number:</strong></td>
								        <td class="data-item"><?php echo @$drow['phone_number'];?></td>
									</tr>
									<tr>
										<td class="data-lable"><strong>Remarks:</strong></td>
								        <td class="data-item"><?php echo @$drow['remarks'];?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div><br>
				</div>

				<div class="col-sm-12 col-md-12"><br>
					<div class="row">							
						<div class="header">
						<h4 align="center"><strong>Ordered Tools</strong></h4>
					</div>
					</div>
					<br>
					<div class="table-responsive">
						<table class="table hover">
							<thead>
								<tr>
	                            	<th class="text-center"><strong>S.No</strong></th>
	                                <th class="text-center"><strong>Tool Number</strong></th>
	                                <th class="text-center"><strong>Tool Desc</strong></th>
	                                <th class="text-center"><strong>Ordered Qty</strong></th>
	                                <th class="text-center"><strong>Assigned Asset(s)</strong></th>
	                                <th class="text-center"><strong>Assigned Serial Numbers(s)</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php 
								if(count(@$order_tool)>0)
								{	$sn = 1;
									foreach(@$order_tool as $row)
									{
									?>
										<tr>
											<td class="text-center"><?php echo $sn++; ?></td>
											<td class="text-center"><?php echo $row['part_number']; ?></td>
											<td class="text-center"><?php echo $row['part_description']; ?></td>
											<td class="text-center"><?php echo $row['quantity']; ?></td>
											<td class="text-center"><?php echo $row['assets']; ?></td>
											<td class="text-center"><?php echo $row['serial_nos']; ?></td>
										</tr>
							<?php   }
								} else {?>
									<tr><td colspan="6" align="center">No Records Found.</td></tr>
	                    <?php 	} ?>
							</tbody>
						</table>
	                </div><br>
				</div>
				<div class="row">							
					<div class="header">
						<h4 align="center"><strong>Attached Documents</strong></h4>
					</div>
					<br>
					<div class="table-responsive col-md-offset-1 col-md-10">
						<table class="table hover">
							<thead>
								<tr>
									<th class="text-center"><strong>S No.</strong></th>
									<th width="18%" class="text-center"><strong>Attached Date</strong></th>
									<th class="text-center"><strong>Document Type</strong></th>
									<th width="30%" class="text-center"><strong>Supported Document</strong></th>
									
								</tr>
							</thead>
							<tbody>
							<?php
								if(count(@$attach_document)>0)
								{	$count = 1;
									foreach($attach_document as $ad)
									{?>	
										<tr>
											<td class="text-center"><?php echo $count++; ?></td>
											<td><?php echo $ad['created_time']; ?></td>
											<td align="center">
													<?php 
														foreach($documenttypeDetails as $doc)
														{
															if($doc['document_type_id']==@$ad['document_type_id'])
															{
																echo $doc['name']; 
															}
														}
													?>
											</td>
											<td>
		                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
											</td>
										</tr>
									 <?php		
									} 
								} 
								else {
									?>	
									<tr><td colspan="4" align="center">No Attached Docs.</td></tr>
								<?php } ?>
							</tbody>
						</table><br>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-5">
							<?php $current_offset = get_current_offset($_SESSION['current_offset_string']); ?>
							<a class="btn btn-primary" href="<?php echo SITE_URL.'closed_fe_delivery_list'.$current_offset;?>"><i class="fa fa-reply"></i> Back</a>
						</div>
					</div>
				</div><br>

			</div>
		</div>
	    <?php 
		}
		if(isset($displayResults)&&$displayResults==1)
    	{   
    	?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'closed_fe_delivery_list';?>">
					<div class="row">
						<div class="col-sm-12">
							<label class="col-sm-2 control-label">Order Number</label>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="tool_order_number" placeholder="Order Number" value="<?php echo @$search_data['tool_order_number'];?>" class="form-control">
							</div>
							<label class="col-sm-1 control-label">SSO ID</label>
							<div class="col-sm-3">
								<?php 
								$d['name']        = 'ssoid';
								$d['search_data'] = @$search_data['ssoid'];
								$this->load->view('sso_dropdown_list',$d); 
								?>
							</div>
							<div class="col-sm-3 col-md-3">							
								<button type="submit" name="searchfedelivery" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
								<a href="<?php echo SITE_URL.'closed_fe_delivery_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
							</div> 
						</div>
						<div class="col-sm-12">
							<?php
							if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray']))
							{
								?>
								<label class="col-sm-2 control-label">Warehouse</label>
								<div class="col-sm-3">
									<select name="wh_id" class=" main_status supplier_id select2" > 
										<option value="">- Select Warehouse -</option>
										<?php
										foreach($warehouse as $row)
										{
											$selected = ($row['wh_id']==@$search_data['whc_id'])?'selected="selected"':'';
											echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
										}?>
									</select>
								</div> <?php

							}
							if($task_access == 3 && @$_SESSION['header_country_id']=='')
							{
								?>
								<label class="col-sm-1 control-label">Country</label>
								<div class="col-sm-3">
									<select name="country_id" class="main_status select2" >    <option value="">- Select Country -</option>
										<?php
										foreach($country_list as $row)
										{
											$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
											echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
										}?>
									</select>
								</div> <?php
							} ?>
						</div>
					</div>
				</form>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Order Number</strong></th>
                                <th class="text-center"><strong>Shipped To </strong></th>
                                <th class="text-center"><strong>Sent From Wh </strong></th>
                                <th class="text-center"><strong>SSO Details</strong></th>
                                
                                <th class="text-center"><strong>Expected Delivery by</strong></th>
                                <th class="text-center"><strong>Order Status</strong></th>
                                <th class="text-center"><strong>Country</strong></th>
                                <th class="text-center" width="18%"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if(count($closed_fe_deliveryResults)>0)
							{
								foreach($closed_fe_deliveryResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++; ?></td>
										<td class="text-center"><?php echo $row['order_number']; ?></td>
										<td class="text-center"><?php if($row['location_name']!=''){ echo $row['location_name'];}else{ echo "Residential Address"; } ?></td>
										<td class="text-center"><?php echo $row['wh_name']; ?></td>
										<td class="text-center"><?php echo $row['sso_id'].' - '.$row['user_name']; ?></td>
										<td class="text-center"><?php echo indian_format($row['delivery_date']); ?></td>

										<td class="text-center"><?php echo $row['current_stage']; ?></td>
										<td class="text-center"><?php echo $row['country']; ?></td>
										<td class="text-center">
										<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View Delivery Details"  href="<?php echo SITE_URL.'view_fe_delivery_details/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-eye"></i></a>
										<a class="btn btn-primary" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Print"  target="_blank"  href="<?php echo SITE_URL.'fe_invoice_print/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-print"></i></a>
										<?php if($row['tcs']<=6){?>
										<a class="btn btn-warning" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Re-Print"  href="<?php echo SITE_URL.'update_fe_print/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-danger" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Cancel Print"  href="<?php echo SITE_URL.'cancel_fe_print/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-trash-o"></i></a>
										<?php } ?>
                                    	</td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="9" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>
		<?php
		}
		?>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>