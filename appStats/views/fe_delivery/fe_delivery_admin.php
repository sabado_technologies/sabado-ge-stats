<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'fe_delivery_admin';?>">
					<div class="row">
						<div class="col-sm-12 form-group">
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="tool_order_number" placeholder="Order Number" value="<?php echo @$search_data['tool_order_number'];?>" class="form-control">
							</div>
							<div class="col-sm-3">
								<?php 
								$d['name']        = 'ssoid';
								$d['search_data'] = @$search_data['ssoid'];
								$this->load->view('sso_dropdown_list',$d); 
								?>
							</div>
							<div class="col-sm-3">
								<input class="form-control date" required size="16" type="text" autocomplete="off"  value="<?php if($search_data['courier_date']!=''){ 
									$date = strtotime("-2 days", strtotime($search_data['courier_date']));
    								$courier_date =  date("d-m-Y", $date); echo $courier_date;
    								} ?>" readonly placeholder="Ship By Date" name="courier_date" style="cursor:hand;background-color: #ffffff">	
							</div>
							<div class="col-md-3" >							
								<button type="submit" name="searchfedelivery1" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
								<a href="<?php echo SITE_URL.'fe_delivery_admin'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
							</div>
						</div>
						<div class="col-sm-12 form-group">
							<?php
							if($task_access==2 || $task_access==3)
							{
								?>
								<div class="col-sm-3">
									<select name="wh_id" class=" main_status supplier_id select2" > 
										<option value="">- Warehouse -</option>
										<?php
										foreach($warehouse as $row)
										{
											$selected = ($row['wh_id']==@$search_data['whc_id'])?'selected="selected"':'';
											echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
										}?>
									</select>
								</div> <?php

							}
							if($task_access == 3 && @$_SESSION['header_country_id']=='')
							{
								?>
								<div class="col-sm-3">
									<select name="country_id" class="main_status select2" >    <option value="">- Country -</option>
										<?php
										foreach($country_list as $row)
										{
											$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
											echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
										}?>
									</select>
								</div> <?php
							}?>
							<div class="col-sm-3">
								<select name="service_type_id" class="select2"> 
									<option value="">- Service Type -</option>
									<?php
									foreach($service_type as $row)
									{
										$selected = ($row['service_type_id']==@$search_data['service_type_id'])?'selected="selected"':'';
										echo '<option value="'.$row['service_type_id'].'" '.$selected.'>'.$row['name'].'</option>';
									}?>
								</select>
							</div>
						</div>
					</div>
				</form>
				<div class="table-responsive">
					<table class="table table-bordered hover">
        <thead>
            <tr>
                <th></th>
                <th class="text-center"><strong>S.No</strong></th>
                <th class="text-center"><strong>Order Number</strong></th>
                <th class="text-center"><strong>Ordered By</strong></th>
                <th class="text-center"><strong>From Wh</strong></th>
                <th class="text-center"><strong>Need Date</strong></th>
                <th class="text-center"><strong>Destination</strong></th>
               	<th class="text-center"><strong>Ship By Date</strong></th>
               	<th class="text-center"><strong>Service Type</strong></th>
               	<th class="text-center"><strong>Country</strong></th>
            </tr>
        </thead>
        <tbody>
            <?php 
			if(count($fe_deliveryResults)>0)
			{
				foreach($fe_deliveryResults as $row)
				{
				?>
					<tr>
						<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title="Expand" ></td>
						<td class="text-center"><?php echo $sn++; ?></td>
						<td class="text-center"><?php echo $row['order_number']; ?></td>
						<td class="text-center"><?php echo $row['sso_id'].' -('.$row['user_name'].')'; ?></td>
						<td class="text-center"><?php echo $row['wh_name']; ?></td>
						<td class="text-center"><?php echo indian_format($row['request_date']); ?></td>
						<td class="text-center"><?php if($row['location_name']!=''){ echo $row['location_name'];}else{ echo "Residential Address"; } ?></td>
						<td class="text-center"><?php echo indian_format($row['courier_date']); ?></td>
						<td class="text-center"><?php echo $row['service_type_name']; ?></td>
						<td class="text-center"><?php echo $row['country']; ?></td>
					</tr>
                <?php if(count($row['ordered_tool'])>0)
                    {  $slno = 1; ?>
                    <tr class="details">
                        <td  colspan="12">
                            <table class="table">
                            	<thead>
		                            <tr>
		                                <th class="text-center" width="30%"><strong>Tool Number</strong></th>
		                                <th class="text-center" width="30%"><strong>Tool Description</strong></th>
		                                <th class="text-center" width="20%"><strong>Ordered Qty</strong></th>
		                              	<th class="text-center" width="20%"><strong>Avail Qty In Inventory</strong></th>
		                            </tr>
		                        </thead>
                                <tbody>
	                            <?php foreach($row['ordered_tool'] as $value)
	                            { 
	                            ?>
	                                <tr class="asset_row">
	                                    <td class="text-center"><?php echo $value['part_number']; ?></td>
										<td class="text-center"><?php echo $value['part_description']; ?></td>
										<td class="text-center"><?php echo $value['requested_qty']; ?></td>
										<td class="text-center"><?php echo $value['avail_qty']; ?></td>
	                                </tr>
	                            <?php     
	                            } ?>
								</tbody>
                            </table>
                        </td>
                    </tr><?php
                    }
                    else
                    { ?>
                        <tr><td colspan="12" class="text-center"><span class="label label-primary">- No Tools Found -</span></td></tr>
                    <?php 
                    }
                }
            
            } else {?>
                <tr><td colspan="12" align="center"><span class="label label-primary">No Records Found</span></td></tr>
    <?php   } ?>
        </tbody>
    </table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$('.details').hide();
$(document).on('click',".toggle-details",function () { 
    var row=$(this).closest('tr');
    var next=row.next();
    $('.details').not(next).hide();
    $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
    next.toggle();
    if (next.is(':hidden')) {
        $(this).attr('src',ASSET_URL+'images/plus.png');
    } else {
        $(this).attr('src',ASSET_URL+'images/minus.png');
    }
});
</script>