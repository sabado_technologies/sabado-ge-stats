<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL.'fe_delivery_list';?>">
					<div class="row">
						<div class="col-sm-12 form-group">
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="tool_order_number" placeholder="Order Number" value="<?php echo @$search_data['tool_order_number'];?>" class="form-control">
							</div>
							<div class="col-sm-3">
								<?php 
								$d['name']        = 'ssoid';
								$d['search_data'] = @$search_data['ssoid'];
								$this->load->view('sso_dropdown_list',$d); 
								?>
							</div>
							<div class="col-sm-3">
								<input class="form-control date" required size="16" type="text" autocomplete="off"  value="<?php if($search_data['courier_date']!=''){ 
									$date = strtotime("-2 days", strtotime($search_data['courier_date']));
    								$courier_date =  date("d-m-Y", $date); echo $courier_date;
    								} ?>" readonly placeholder="Ship By Date" name="courier_date" style="cursor:hand;background-color: #ffffff">	
							</div>
							<div class="col-md-3" >							
								<button type="submit" name="searchfedelivery" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
								<a href="<?php echo SITE_URL.'fe_delivery_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
							</div>
						</div>
						<div class="col-sm-12 form-group">
							<?php
							if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray']))
							{
								?>
								<div class="col-sm-3">
									<select name="wh_id" class="select2"> 
										<option value="">- Warehouse -</option>
										<?php
										foreach($warehouse as $row)
										{
											$selected = ($row['wh_id']==@$search_data['whc_id'])?'selected="selected"':'';
											echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
										}?>
									</select>
								</div> <?php

							}
							if($task_access == 3 && @$_SESSION['header_country_id']=='')
							{
								?>
								<div class="col-sm-3">
									<select name="country_id" class="select2">    
										<option value="">- Country -</option>
										<?php
										foreach($country_list as $row)
										{
											$selected = ($row['location_id']==@$search_data['country_id'])?'selected="selected"':'';
											echo '<option value="'.$row['location_id'].'" '.$selected.'>'.$row['name'].'</option>';
										}?>
									</select>
								</div> <?php
							}?>
							<div class="col-sm-3">
								<select name="service_type_id" class="select2"> 
									<option value="">- Service Type -</option>
									<?php
									foreach($service_type as $row)
									{
										$selected = ($row['service_type_id']==@$search_data['service_type_id'])?'selected="selected"':'';
										echo '<option value="'.$row['service_type_id'].'" '.$selected.'>'.$row['name'].'</option>';
									}?>
								</select>
							</div>
						</div>
					</div>
				</form>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Order Number</strong></th>
                                <th class="text-center"><strong>Ordered By</strong></th>
                                <th class="text-center"><strong>From Wh</strong></th>
                                <th class="text-center"><strong>Need Date</strong></th>
                                <th class="text-center"><strong>Destination</strong></th>
                               	<th class="text-center"><strong>Ship By Date</strong></th>
                               	<th class="text-center"><strong>Service Type</strong></th>
                               	<th class="text-center"><strong>Country</strong></th>
                                <th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if(count($fe_deliveryResults)>0)
							{
								foreach($fe_deliveryResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++; ?></td>
										<td class="text-center"><?php echo $row['order_number']; ?></td>
										<td class="text-center"><?php echo $row['sso_id'].' -('.$row['user_name'].')'; ?></td>
										<td class="text-center"><?php echo $row['wh_name']; ?></td>
										<td class="text-center"><?php echo indian_format($row['request_date']); ?></td>
										<td class="text-center"><?php if($row['location_name']!=''){ echo $row['location_name'];}else{ echo "Residential Address"; } ?></td>
										<td class="text-center"><?php echo indian_format($row['courier_date']); ?></td>
										<td class="text-center"><?php echo $row['service_type_name']; ?></td>
										<td class="text-center"><?php echo $row['country']; ?></td>
										<td class="text-center">
                                        <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top" <?php if($row['fe_check']==1){ echo "disabled";} ?> data-toggle="tooltip" title="<?php if($row['fe_check']==1){ echo "Waiting For Admin Approval";}else{ echo "Proceed to Shipment";} ?>"  href="<?php echo SITE_URL.'generate_fe_delivery/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php if($row['check_scanned_asset']>0)
                                        { ?>
                                        	<a class="btn btn-warning" style="padding:3px 3px;"  data-container="body" data-placement="top" data-toggle="tooltip" title="Unlink Scanned Assets"  href="<?php echo SITE_URL.'unlink_scanned_assets/'.storm_encode($row['tool_order_id']);?>" onclick="return confirm('Are you sure you want to Unlink Scanned Assets?')"><i class="fa fa-chain-broken"></i></a>
                                        <?php }?>
                                    	</td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="10" align="center"><span class="label label-primary">- No FE Shipment Requests -</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>