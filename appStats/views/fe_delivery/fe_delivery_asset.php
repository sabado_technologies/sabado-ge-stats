<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">							
							<?php
							if(@$asset_details[0]['asset_number']=='')
							{
							?>
							<div class="header">
								<h5 align="center"><strong>Order Details</strong></h5>
							</div>
							<div class="row">
							<div class="col-md-6">
								<table class="no-border">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="data-lable"><strong>Order Number :</strong></td>
									        <td class="data-item"><?php echo @$trow['order_number'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Tool Number :</strong></td>
									        <td class="data-item"><?php echo @$trow['part_number'];?></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-6">
								<table class="no-border">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="data-lable"><strong>SSO Detail :</strong></td>
											<td class="data-item"><?php echo @$trow['sso_id'].' - '.$trow['name']?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Tool Description :</strong></td>
											<td class="data-item"><?php echo @$trow['part_description']?></td>
										</tr>
									</tbody>
								</table>
							</div>
							</div><br>
							<h2 align="center">Scan QR Code to get Asset Details</h2>
							<br>
							<form class="form-horizontal form" role="form" action="<?php echo SITE_URL.'fe_asset_details';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
								<input type="hidden" name="tool_id" value="<?php echo storm_encode($trow['tool_id'])?>">
								<input type="hidden" name="ordered_tool_id" value="<?php echo storm_encode($trow['ordered_tool_id'])?>">
								<div class="form-group">
									<label for="inputName" class="col-sm-5 control-label">Asset Number <span class="req-fld">*</span></label>
		                            <div class="col-sm-3">
										<input type="text" autocomplete="off" name="asset_number" id="asset_number" autofocus="autofocus" class="form-control">
									</div>
									<div class="col-sm-3">
										<input type="checkbox" name="vehicle" value="0" class="checkbox_val"> Enable Manual Entry
									</div>
								</div>
								<div class="form-group submit_action hidden">
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary" type="submit" value="1" name="submitUser"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL.'generate_fe_delivery/'.storm_encode($trow['tool_order_id']); ?>"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
							</form>
							
							<br>
							<div class="row">
								<div class="col-md-5" style="margin-left: 15px;">
									<span><strong>Available Assets in Inventory</strong></span>
								</div>
								<div class="col-md-12 col-sm-12">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th></th>
					                            	<th class="text-center"><strong>S.No</strong></th>
					                                <th class="text-center"><strong>Tool Number</strong></th>
					                                <th class="text-center"><strong>Tool Description</strong></th>
					                                <th class="text-center"><strong>Serial Number</strong></th>
					                                <th class="text-center"><strong>Asset Number</strong></th>
					                                <th class="text-center"><strong>Calibration Due Date</strong></th>
					                                <th class="text-center"><strong>Pallet Location </strong></th>
					                                <th class="text-center"><strong>Asset Status</strong></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												if(count($fe_asset_sub_inventory)>0)
												{	$sn = 1;
													foreach($fe_asset_sub_inventory as $row)
													{
													?>
														<tr>
															<td class="text-center"><input type="radio" name="radio_button" value="<?php echo $row['asset_number']; ?>" class="radioBtn"></td>
															<td class="text-center"><?php echo $sn++; ?></td>
															<td class="text-center"><?php echo $row['part_number']; ?></td>
															<td class="text-center"><?php echo $row['part_description']; ?></td>
															<td class="text-center"><?php echo $row['serial_number']; ?></td>
															<td class="text-center"><strong><?php echo $row['asset_number']; ?></strong></td>
															<td class="text-center"><?php if(@$row['cal_due_date']=='' || @$row['cal_due_date'] == "0000-00-00"){ echo  "--"; } else { echo indian_format($row['cal_due_date']); } ?></td>
															<td class="text-center"><strong><?php if($row['sub_inventory']!=''){ echo $row['sub_inventory'];} else { echo "--";} ?></strong></td>
															<td class="text-center"><?php echo $row['asset_status']; ?></td>
														</tr>
											<?php   }
												} else {?>
													<tr><td colspan="9" align="center"><span class="label label-primary">No Assets In Inventory</span></td></tr>
					                    <?php 	} ?>
											</tbody>
										</table>
					                </div>
								</div>
							</div>
							<?php
							}
							else
							{
							?>
							<div class="header">
								<h5 align="center"><strong>Order Details</strong></h5>
							</div>
							<div class="row">
							<div class="col-md-6">
								<table class="no-border">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="data-lable"><strong>Order Number :</strong></td>
									        <td class="data-item"><?php echo @$trow['order_number'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Tool Number :</strong></td>
									        <td class="data-item"><?php echo @$trow['part_number'];?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Asset Number :</strong></td>
									        <td class="data-item"><?php echo @$asset_details[0]['asset_number'];?></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-6">
								<table class="no-border">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="data-lable"><strong>SSO Detail :</strong></td>
											<td class="data-item"><?php echo @$trow['sso_id'].' - '.$trow['name']?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Tool Description :</strong></td>
											<td class="data-item"><?php echo @$trow['part_description']?></td>
										</tr>
										<tr>
											<td class="data-lable"><strong>Pallet Location :</strong></td>
											<td class="data-item"><?php echo @$asset_details[0]['sub_inventory'];?></td>
										</tr>
									</tbody>
								</table>
							</div>
							</div><br>
							<?php if($display_cr_remarks!=''){ ?>
							<div class="col-md-12">
								<h4 align="center">Note : <strong><span style="color:red;"><?php echo $display_cr_remarks; ?></span></strong></h4>
							</div>
							<?php } ?>
							<form class="form-horizontal"  role="form" action="<?php echo SITE_URL.'insert_fe_delivery_asset';?>" method="post">
								<input type="hidden" name="ordered_tool_id" value="<?php echo storm_encode($ordered_tool_id); ?>">
								<input type="hidden" name="asset_id" value="<?php echo storm_encode($asset_details[0]['asset_id']); ?>">
								<div class="table-responsive"> 
									<table class="table tabb">
										<thead>
											<tr>
												<th class="text-center"><strong>Serial Number</strong></th>
				                                <th class="text-center"><strong>Tool Number</strong></th>
				                                <th class="text-center" width="30%"><strong>Tool Description</strong></th>
				                                <th class="text-center"><strong>Tool Level</strong></th>
				                                <th class="text-center"><strong>Qty</strong></th>
				                              	<th class="text-center" width="32%"><strong>Tool Health</strong></th>
				                              	<th class="text-center" ><strong>Remarks</strong></th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(count(@$asset_details)>0)
											{	
												foreach(@$asset_details as $row)
												{
												?>
													<tr class="asset_row">
														<td class="text-center"><?php echo $row['serial_number']; ?></td>
														<td class="text-center"><?php echo $row['part_number']; ?></td>
														<td class="text-center"><?php echo $row['part_description']; ?></td>
														
														<td class="text-center"><?php echo $row['part_level_name']; ?></td>
														<td class="text-center"><?php echo $row['quantity']; ?></td>
														<td class="text-center">
														 <div class="col-sm-12 custom_icheck">
						                                    <label class="radio-inline"> 
						                                        <div class="iradio_square-blue <?php if(@$row['status']==1) { echo "checked"; }?>" style="position: relative;" aria-checked="true" aria-disabled="false">
						                                            <input type="radio" class="asset_condition_id" value="1" name="asset_condition_id[<?php echo $row['part_id']; ?>]" <?php if(@$row['status']==1) { echo "checked"; }?> style="position: absolute; opacity: 0;">
						                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
						                                        </div> 
						                                       	Good
						                                    </label>
						                                    <label class="radio-inline"> 
						                                        <div class="iradio_square-blue <?php if(@$row['status']==2) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
						                                            <input type="radio" class="asset_condition_id" value="2" name="asset_condition_id[<?php echo $row['part_id']; ?>]" <?php if(@$row['status']==2) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
						                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
						                                        </div> 
						                                        Defective
						                                    </label>
						                                    <label class="radio-inline"> 
						                                        <div class="iradio_square-blue <?php if(@$row['status']==3) { echo "checked"; } ?>" style="position: relative;" aria-checked="true" aria-disabled="false">
						                                            <input type="radio" class="asset_condition_id" value="3" name="asset_condition_id[<?php echo $row['part_id']; ?>]" <?php if(@$row['status']==3) { echo "checked"; } ?> style="position: absolute; opacity: 0;">
						                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
						                                        </div> 
						                                        Missing
						                                    </label>
														</div>
														</td>
														<td>
															<div class="textbox">
						                                   		<textarea class="form-control textarea" name="remarks[<?php echo $row['part_id']; ?>]"></textarea> 	
						                                   	</div>
														</td>
													</tr>
										<?php   }
											} else {?>
												<tr><td colspan="6" align="center"><span class="label label-primary">No Records Found</span></td></tr>
				                    <?php 	} ?>
										</tbody>
									</table>
				                </div><br>
				                <div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure you want to Submit Tool Health?')" value="1" name="submit_fe"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL.'fe_delivery_asset/'.storm_encode($ordered_tool_id); ?>"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
			                </form><br>
							<?php
							}
							?>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<style type="text/css">
    /*form input[type=text]{
        height:0.0001px !important; 
        color: #fff; 
        border-color: none; 
        opacity:0.0001;
    }*/
</style>
<script type="text/javascript">
    $( document ).ready(function() {
    	$('#asset_number').on('input',function(){
            var asset_number = $(this).val();
            var checkbox_val = $('.checkbox_val').val();
            if(checkbox_val == 0)
            {
            	if(asset_number.trim() != '')
	            {
	            	$('.form').submit();
	            }
            }
            
        });
        $('body').on('click',function(e){
        	$('#asset_number').focus();
        });
    });
    $(document).on('click','.checkbox_val',function(){
		var value = $(this).val();
		if(value == 0)
		{
			$('.checkbox_val').val('1');
			$('.submit_action').removeClass('hidden');
		}
		else
		{
			$('.checkbox_val').val('0');
			$('.radioBtn').prop('checked',false);
			$('#asset_number').val('');
			$('.submit_action').addClass('hidden');
		}
	});
	$(document).on('click','.radioBtn',function(){
		var asset_number = $(this).val();
		if(asset_number!='')
		{
			$('#asset_number').val(asset_number);
			$('.checkbox_val').prop('checked', true);
			$('.checkbox_val').val('1');
			$('.submit_action').removeClass('hidden');
		}
	});
</script>