<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
    	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		<div class="block-flat">
			<div class="content">
				<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>open_fe_delivery_list">
					<div class="row">
						<div class="col-sm-12">
							<label class="col-sm-2 control-label">Order Number</label>
							<div class="col-sm-4">
								<input type="text" autocomplete="off" name="tool_order_number" placeholder="Order Number" value="<?php echo @$search_data['tool_order_number'];?>" class="form-control">
							</div>
							<label class="col-sm-2 control-label">SSO ID</label>
							<div class="col-sm-3">
								<input type="text" autocomplete="off" name="ssoid" placeholder="SSO ID" value="<?php echo @$search_data['ssoid'];?>" class="form-control">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-offset-8 col-sm-3 col-md-3" style="padding-top: 10px;">							
								<button type="submit" name="searchfedelivery" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
								<a href="<?php echo SITE_URL.'open_fe_delivery_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
							</div>
						</div>
					</div>
				</form>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
                            	<th class="text-center"><strong>S.No</strong></th>
                                <th class="text-center"><strong>Order Number</strong></th>
                                <th class="text-center"><strong>Couried Location</strong></th>
                                <th class="text-center"><strong>SSO Details</strong></th>
                                <th class="text-center"><strong>return Date</strong></th>
                                <th class="text-center"><strong>Current Stage</strong></th>
                                <th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if(count($open_fe_deliveryResults)>0)
							{
								foreach($open_fe_deliveryResults as $row)
								{
								?>
									<tr>
										<td class="text-center"><?php echo $sn++; ?></td>
										<td class="text-center"><?php echo $row['order_number']; ?></td>
										<td class="text-center"><?php echo $row['location_name']; ?></td>
										<td class="text-center"><?php echo $row['sso_id'].' - '.$row['user_name']; ?></td>
										<td class="text-center"><?php echo $row['return_date']; ?></td>
										<td class="text-center"><?php echo $row['current_stage']; ?></td>
										<td class="text-center">
										<?php if($row['current_stage_id']==6){ ?>
                                        <a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Edit Delivery Details"  href="<?php echo SITE_URL.'edit_generated_fe_details/'.storm_encode($row['tool_order_id']);?>"><i class="fa fa-pencil"></i></a>
                                        <?php } ?>
                                    	</td>
									</tr>
						<?php   }
							} else {?>
								<tr><td colspan="7" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                    <?php 	} ?>
						</tbody>
					</table>
                </div>
                <div class="row">
                	<div class="col-sm-12">
	                    <div class="pull-left">
	                        <div class="dataTables_info" role="status" aria-live="polite">
	                            <?php echo @$pagermessage; ?>
	                        </div>
	                    </div>
	                    <div class="pull-right">
	                        <div class="dataTables_paginate paging_bootstrap_full_number">
	                            <?php echo @$pagination_links; ?>
	                        </div>
	                    </div>
                	</div> 
                </div>
          		
			</div>
		</div>	
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>