<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			<?php
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>scrap">
						<div class="row">
							<div class="col-sm-12">
								<label class="col-sm-2 control-label">Tool Number</label>
	                            <div class="col-sm-3">
									<input type="text" autocomplete="off" name="tool_number" placeholder="Tool Number" value="<?php echo @$searchParams['tool_number'];?>"  class="form-control" maxlength="100">
								</div>
								<label class="col-sm-2 control-label">Asset Number</label>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$searchParams['asset_number'];?>" class="form-control">
								</div>
							</div>
							<div class="col-sm-12">
								<label class="col-sm-2 control-label">Tool Description</label>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="tool_description" placeholder="Tool Description" value="<?php echo @$searchParams['tool_description'];?>" class="form-control">
								</div>
								<label class="col-sm-2 control-label">Warehouse</label>
								<div class="col-sm-3">
									<select name="wh_id" class="form-control" > 
										<option value="">Warehouse</option>
										<?php 
											foreach($wh_details as $wh)
											{
												$selected = ($wh['wh_id']==@$searchParams['wh_id'])?'selected="selected"':'';
												echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['name'].'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="header"></div>
								<table class="table table-bordered" ></table>
								<div class="col-sm-12" align="right">	
									<div class="col-sm-12 col-md-12"></div>
									<div class="col-sm-10 col-md-10">							
										<button type="submit" name="searchasset" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
										<a href="<?php echo SITE_URL.'scrap'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
									</div>	
								</div>
						</div>
					</form>
				<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Tool Number</strong></th>
								<th class="text-center"><strong>Tool Description</strong></th>
								<th class="text-center"><strong>Asset Number</strong></th>
								<th class="text-center"><strong>Warehouse</strong></th>
								<th class="text-center"><strong>Status</strong></th>
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($assetResults)>0)
							{
								foreach($assetResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['tool_number'];?></td>
										<td class="text-center"><?php echo @$row['tool_description'];?></td>
										<td class="text-center"><?php echo @$row['asset_number'];?></td>
										<td class="text-center"><?php echo @$row['warehouse'];?></td>
										<td class="text-center"><?php echo @$row['status_name'].' '.'Tool';?></td>
										<td class="text-center">										
                                			<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top" data-toggle="tooltip" title="Raise Scrap" href="<?php echo SITE_URL.'raise_scrap/'.storm_encode($row['asset_id']);?>"><i class="fa fa-share"></i></a>
                                <?php
                                }
                                ?>
                                    	</td>
									</tr>
						<?php	}
							else {
							?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			<?php 
			} ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>