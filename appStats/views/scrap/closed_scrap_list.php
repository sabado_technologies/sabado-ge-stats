<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <?php echo $this->session->flashdata('response'); ?>
			<?php
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
			<div class="block-flat">
				<div class="content">
					<form role="form" class="form-horizontal" method="post" action="<?php echo SITE_URL;?>closed_scrap_list">
						<div class="row">
							<div class="col-sm-12 form-group">
	                            <div class="col-sm-3">
	                                <input type="text" autocomplete="off" name="tool_number" placeholder="Tool Number" value="<?php echo @$searchParams['tool_number'];?>"  class="form-control" maxlength="100">
	                            </div>
	                            <div class="col-sm-3">
	                                <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$searchParams['asset_number'];?>" class="form-control">
	                            </div>
	                            <div class="col-sm-3">
	                                <input type="text" autocomplete="off" name="tool_description" placeholder="Tool Description" value="<?php echo @$searchParams['tool_description'];?>" class="form-control">
	                            </div>
	                            <div class="col-sm-3 col-md-3">							
										<button type="submit" name="searchasset" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
										<a href="<?php echo SITE_URL.'closed_scrap_list'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
									</div>	
	                        </div>
							<div class="col-sm-12 form-group">
								<div class="col-sm-3">
								    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$searchParams['serial_no'];?>" class="form-control">
								</div>
                                <?php if($task_access == 2 || $task_access==3){?>
                                    <div class="col-sm-3 ">
                                        <select class="select2" name="wh_id" >
                                            <option value="">- Warehouse -</option>
                                             <?php
                                            foreach($whList as $wh)
                                            {
                                                $selected = ($wh['wh_id']==$searchParams['whid'])?'selected="selected"':'';
                                                echo '<option value="'.$wh['wh_id'].'" '.$selected.'>'.$wh['wh_code'].' -('.$wh['name'].')</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <?php } 
                                    if($task_access == 3 && $_SESSION['header_country_id']==''){?>
                                    <div class="col-sm-3 ">
                                        <select class="select2" name="country_id" >
                                            <option value="">- Country -</option>
                                             <?php
                                            foreach($countryList as $country)
                                            {
                                                $selected = ($country['location_id']==$searchParams['country_id'])?'selected="selected"':'';
                                                echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                <?php } ?>
                            </div>
						</div>
						<div class="row">
							<div class="header"></div>
								<table class="table table-bordered" ></table>
						</div>
					</form>
				<div class="header"></div>
				<div class="table-responsive">
					<table class="table table-bordered hover">
						<thead>
							<tr>
								<th class="text-center"><strong>S.NO</strong></th>
								<th class="text-center"><strong>Tool Number</strong></th>
								<th class="text-center"><strong>Tool Description</strong></th>
								<th class="text-center"><strong>Asset Number</strong></th>
								<th class="text-center"><strong>Serial Number</strong></th>
								<th class="text-center"><strong>Ware House</strong></th>
								<th class="text-center"><strong>Scraped Date</strong></th>
								<th class="text-center"><strong>Country</strong></th>
								<th class="text-center"><strong>Actions</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(count($assetResults)>0)
							{
								foreach($assetResults as $row)
								{?>
									<tr>
										<td class="text-center"><?php echo @$sn++;?></td>
										<td class="text-center"><?php echo @$row['tool_number'];?></td>
										<td class="text-center"><?php echo @$row['tool_description'];?></td>
										<td class="text-center"><?php echo @$row['asset_number'];?></td>
										<td class="text-center"><?php echo @$row['serial_number'];?></td>
										<td class="text-center"><?php echo @$row['wh_code'].' -('.$row['warehouse'],')';?></td>
										<td class="text-center"><?php echo date('d-m-Y', strtotime(@$row['scraped_date']));?></td>
										<td class="text-center"><?php echo @$row['country_name'];?></td>
										<td class="text-center">										
                                			<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top" data-toggle="tooltip" title="Closed Scrap Details" href="<?php echo SITE_URL.'view_scraped_list/'.storm_encode($row['scrap_id']);?>"><i class="fa fa-eye"></i></a>
                                <?php
                                }
                                ?>
                                    	</td>
									</tr>
						<?php	}
							else {
							?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Records</span></td></tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	                <div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>	          		
				</div>
			</div>	
			<?php 
			} ?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>