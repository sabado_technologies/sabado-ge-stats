<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Asset Details</strong></h4>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Asset Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Modality :</strong></td>
										        <td class="data-item"><?php echo @$lrow['modality_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Tool Type :</strong></td>
										        <td class="data-item"><?php echo @$lrow['tool_type_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Ware House :</strong></td>
										        <td class="data-item"><?php echo @$lrow['warehouse_code'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Sub Inventory :</strong></td>
										        <td class="data-item"><?php echo @$lrow['sub_inventory'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Asset Type :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_type'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Kit :</strong></td>
										        <td class="data-item"><?php if($lrow['kit']==1)
										        {
										            echo "Yes";
										        }else
										        {
										        	echo "No";
										        }
										        ?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Supplier :</strong></td>
										        <td class="data-item"><?php echo @$lrow['supplier_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Remarks :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_remarks'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Asset Level :</strong></td>
												<td class="data-item"><?php echo @$lrow['asset_level_name']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration :</strong></td>
												<td class="data-item"><?php echo @$lrow['calibration']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration Supplier :</strong></td>
												<td class="data-item"><?php echo @$lrow['cs_name']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration Due Date :</strong></td>
												<td class="data-item"><?php if(@$lrow['due_date']!=''){echo date('d-m-Y',strtotime(@$lrow['due_date']));} else {echo '';}?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibrated Date :</strong></td>
												<td class="data-item"><?php if(@$lrow['calibrated_date']!=''){echo date('d-m-Y',strtotime(@$lrow['calibrated_date']));} else {echo '';}?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Manufacturer :</strong></td>
												<td class="data-item"><?php echo @$lrow['manufacturer']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Po Number :</strong></td>
												<td class="data-item"><?php echo @$lrow['po']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Earpe Number :</strong></td>
												<td class="data-item"><?php echo @$lrow['earpe']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Date of use :</strong></td>
												<td class="data-item"><?php if(@$lrow['date_of_used']!=''){echo date('d-m-Y',strtotime(@$lrow['date_of_used']));} else {echo '';}?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>	
							<br>						
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Main Component</strong></h4>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Serial Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['serial_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Part Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['part_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Part Description :</strong></td>
										        <td class="data-item"><?php echo @$lrow['part_description'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Tool Code :</strong></td>
										        <td class="data-item"><?php echo @$lrow['tool_code'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Cost :</strong></td>
												<td class="data-item"><?php echo @$lrow['cost']?></td>
											</tr>
											
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Quantity :</strong></td>
												<td class="data-item"><?php echo @$lrow['quantity'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Remarks :</strong></td>
										        <td class="data-item"><?php echo @$lrow['part_remarks'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Length :</strong></td>
												<td class="data-item"><?php echo @round($lrow['length'],2);?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Breadth :</strong></td>
												<td class="data-item"><?php echo @round($lrow['breadth'],2);?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Height :</strong></td>
												<td class="data-item"><?php echo @round($lrow['height'],2);?></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
							<br>
							<?php if(count(@$subComponent)>0)
							{ ?>
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Sub Components</strong></h4>
								</div>
								<div class="table-responsive">
								<table class="table">
									<thead>
										<th class="text-center"><strong>Serial No</strong></th>
										<th class="text-center"><strong>Tool No</strong></th>
										<th class="text-center"><strong>Description</strong></th>
										<th class="text-center"><strong>Qty</strong></th>
										<th class="text-center"><strong>Part Status</strong></th>
									</thead>
								<?php foreach ($subComponent as $sub) 
								{ ?>
									<tr>
										<td class="text-center"><?php echo $sub['serial_number']; ?></td>
										<td class="text-center"><?php echo $sub['part_number']; ?></td>
										<td class="text-center"><?php echo $sub['part_description']; ?></td>
										<td class="text-center"><?php echo $sub['quantity']; ?></td>
										<td class="text-center"><?php 
										if($sub['status'] == 1)
										{
											echo "Good";
										}
										else if($sub['status'] == 2)
										{
											echo "Defective";
										}
										else if($sub['status'] == 3)
										{
											echo "Missing";
										}
										?></td>
									</tr>
								<?php } ?>
								</table>
								</div>
							</div><br>
							<?php } ?>
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Attached Documents</strong></h4>
								</div>
								<br>
								<div class="table-responsive col-md-offset-1 col-md-10">
									<table class="table table-bordered hover">
										<thead>
											<tr>
												<th width="5%" class="text-center"><strong>S.No</strong></th>
												<th width="20%" class="text-center"><strong>Attached Date</strong></th>
												<th class="text-center"><strong>Document Type</strong></th>
												<th width="30%" class="text-center"><strong>Supported Document</strong></th>
												
											</tr>
										</thead>
										<tbody>
										<?php
											if(count(@$attach_document)>0)
												{
													$count = 1;
													foreach($attach_document as $ad)
													{?>	
														<tr class="attach">
															<input type="hidden" name="doc_id" class="scrap_doc_id" value="<?php echo $ad['scrap_doc_id'];?>">
															<td><?php echo $count++; ?></td>
															<td align="center"><?php echo date('d-m-Y', strtotime($ad['created_time'])); ?></td>
															<td align="center">
																<?php 
																	foreach($documenttypeDetails as $doc)
																	{
																		if($doc['document_type_id']==@$ad['document_type_id'])
																		{
																			echo $doc['name']; 
																		}
																	}
																?>
															</td>
															<td align="center">
						                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_scrap_document_path().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['name']; ?>
															</td>
														</tr>
													 <?php		
													} 
												} 
											else {
												?>	
												<tr><td colspan="4" align="center">No Attached Docs.</td></tr>
											<?php } ?>
										</tbody>
									</table><br>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<a class="btn btn-primary" href="<?php echo SITE_URL;?>closed_scrap_list"><i class="fa fa-reply"></i> Back</a>
									</div>
								</div>
							</div>
							<br>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>