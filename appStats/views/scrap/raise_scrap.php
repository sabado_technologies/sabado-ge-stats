<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
    <div class="row"> 
        <div class="col-sm-12 col-md-12">
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Asset Details</strong></h4>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Asset Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Modality :</strong></td>
										        <td class="data-item"><?php echo @$lrow['modality_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Tool Type :</strong></td>
										        <td class="data-item"><?php echo @$lrow['tool_type_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Ware House :</strong></td>
										        <td class="data-item"><?php echo @$lrow['warehouse_code'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Sub Inventory :</strong></td>
										        <td class="data-item"><?php echo @$lrow['sub_inventory'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Asset Type :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_type'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Kit :</strong></td>
										        <td class="data-item"><?php if($lrow['kit']==1)
										        {
										            echo "Yes";
										        }else
										        {
										        	echo "No";
										        }
										        ?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Supplier :</strong></td>
										        <td class="data-item"><?php echo @$lrow['supplier_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Remarks :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_remarks'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Asset Level :</strong></td>
												<td class="data-item"><?php echo @$lrow['asset_level_name']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration :</strong></td>
												<td class="data-item"><?php echo @$lrow['calibration']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration Supplier :</strong></td>
												<td class="data-item"><?php echo @$lrow['cs_name']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration Due Date :</strong></td>
												<td class="data-item"><?php if(@$lrow['due_date']!=''){echo date('d-m-Y',strtotime(@$lrow['due_date']));} else {echo '';}?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibrated Date :</strong></td>
												<td class="data-item"><?php if(@$lrow['calibrated_date']!=''){echo date('d-m-Y',strtotime(@$lrow['calibrated_date']));} else {echo '';}?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Manufacturer :</strong></td>
												<td class="data-item"><?php echo @$lrow['manufacturer']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Po Number :</strong></td>
												<td class="data-item"><?php echo @$lrow['po']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Earpe Number :</strong></td>
												<td class="data-item"><?php echo @$lrow['earpe']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Date of use :</strong></td>
												<td class="data-item"><?php if(@$lrow['date_of_used']!=''){echo date('d-m-Y',strtotime(@$lrow['date_of_used']));} else {echo '';}?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>	
							<br>						
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Main Component</strong></h4>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Serial Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['serial_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Part Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['part_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Part Description :</strong></td>
										        <td class="data-item"><?php echo @$lrow['part_description'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Tool Code :</strong></td>
										        <td class="data-item"><?php echo @$lrow['tool_code'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Cost :</strong></td>
												<td class="data-item"><?php echo @$lrow['cost']?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Quantity :</strong></td>
												<td class="data-item"><?php echo @$lrow['quantity'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Status :</strong></td>
												<td class="data-item"><?php echo @$lrow['asset_condition_name']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Length :</strong></td>
												<td class="data-item"><?php echo @round($lrow['length'],2);?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Breadth :</strong></td>
												<td class="data-item"><?php echo @round($lrow['breadth'],2);?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Height :</strong></td>
												<td class="data-item"><?php echo @round($lrow['height'],2);?></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
							<br>
							<?php if(count(@$subComponent)>0)
							{ ?>
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Sub Components</strong></h4>
								</div>
								<div class="table-responsive">
								<table class="table">
									<thead>
										<th class="text-center"><strong>Serial No</strong></th>
										<th class="text-center"><strong>Tool No</strong></th>
										<th class="text-center"><strong>Description</strong></th>
										<th class="text-center"><strong>Qty</strong></th>
										<th class="text-center"><strong>Part Status</strong></th>
									</thead>
								<?php foreach ($subComponent as $sub) 
								{ ?>
									<tr>
										<td class="text-center"><?php echo $sub['serial_number']; ?></td>
										<td class="text-center"><?php echo $sub['part_number']; ?></td>
										<td class="text-center"><?php echo $sub['part_description']; ?></td>
										<td class="text-center"><?php echo $sub['quantity']; ?></td>
										<td class="text-center"><?php 
										if($sub['status'] == 1)
										{
											echo "Good";
										}
										else if($sub['status'] == 2)
										{
											echo "Defective";
										}
										else if($sub['status'] == 3)
										{
											echo "Missing";
										}
										?></td>
									</tr>
								<?php } ?>
								</table>
								</div>
							</div><br>
							<?php } ?>
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Attach Support Documents</strong></h4>
								</div>
								<br>
								<form class="form-horizontal" id="multi" role="form" parsley-validate novalidate method="post" enctype="multipart/form-data" action="<?php echo $form_action?>">
									<input type="hidden" name="asset_id" value="<?php echo storm_encode(@$asset_id)?>">
									<input type="hidden" name="defective_asset_id" value="<?php echo storm_encode(@$defective_asset_id)?>">
									<div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
										<a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a><br>
									</div>
									<div class="form-group">
		                        		<div class="col-md-12">
				                        	<div class="table-responsive col-md-offset-1 col-md-9 tab_hide">
												<table class="table hover document_table">
													<thead>
														<tr>
															<th  width="8%"><strong>Sno</strong></th>
															<th width="40%" ><strong>Document Type</strong></th>
															<th width="40%" ><strong>Supported Document</strong></th>
															<th width="8%" ><strong>Delete</strong></th>
														</tr>
													</thead>
													<tbody>
													<?php $count = 1;
														 ?>			
														<tr class="doc_row">
															<td align="center">
																<span class="sno"><?php echo $count++; ?></span>
															</td>
															<td>
																<select name="document_type[1]" class="form-control doc_type" > 
																	<option value="">Select Document Type</option>
																	<?php 
																		foreach($documenttypeDetails as $doc)
																		{
																			echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
																		}
																	?>
																</select>
															</td>
															<td>
						                                		<input type="file" id="document" name="support_document_1" class="document">
															</td>
															
													        <td><a  class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
														</tr>
													</tbody>
												</table><br>
											</div>
										</div>
		                    		</div>
		                    		<div class="form-group">
		                    			<div class="col-md-12">
		                    				<label class="col-sm-4 control-label">Remarks <span class="req-fld">*</span></label>
		                    				<div class="col-sm-5">
		                    					<textarea class="form-control" name="remarks" required></textarea>
		                    				</div>
		                    			</div>
		                    		</div><br>
									<div class="form-group">
										<div class="col-sm-offset-5 col-sm-5">
											<button class="btn btn-primary"  onclick="return confirm('Are you sure you want to Submit?')" type="submit" value="1" name="submit"><i class="fa fa-check"></i> Submit</button>
											<a class="btn btn-danger" href="<?php echo SITE_URL.$cancel_btn;?>"><i class="fa fa-times"></i> Cancel</a>
										</div>
									</div>
								</form>
							</div>
							<br>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>