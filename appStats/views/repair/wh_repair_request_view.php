<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Asset Details</strong></h4>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Asset Number :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_number'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Modality :</strong></td>
										        <td class="data-item"><?php echo @$lrow['modality_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Tool Type :</strong></td>
										        <td class="data-item"><?php echo @$lrow['tool_type_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Ware House :</strong></td>
										        <td class="data-item"><?php echo @$lrow['warehouse_code'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Sub Inventory :</strong></td>
										        <td class="data-item"><?php echo @$lrow['sub_inventory'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Asset Type :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_type'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Kit :</strong></td>
										        <td class="data-item"><?php if($lrow['kit']==1)
										        {
										            echo "Yes";
										        }else
										        {
										        	echo "No";
										        }
										        ?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Supplier :</strong></td>
										        <td class="data-item"><?php echo @$lrow['supplier_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Remarks :</strong></td>
										        <td class="data-item"><?php echo @$lrow['asset_remarks'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Asset Level :</strong></td>
												<td class="data-item"><?php echo @$lrow['asset_level_name']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration :</strong></td>
												<td class="data-item"><?php echo @$lrow['calibration']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration Supplier :</strong></td>
												<td class="data-item"><?php echo @$lrow['cs_name']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibration Due Date :</strong></td>
												<td class="data-item"><?php if(@$lrow['due_date']!=''){echo date('d-m-Y',strtotime(@$lrow['due_date']));} else {echo '';}?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Calibrated Date :</strong></td>
												<td class="data-item"><?php if(@$lrow['calibrated_date']!=''){echo date('d-m-Y',strtotime(@$lrow['calibrated_date']));} else {echo '';}?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Manufacturer :</strong></td>
												<td class="data-item"><?php echo @$lrow['manufacturer']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Po Number :</strong></td>
												<td class="data-item"><?php echo @$lrow['po']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Earpe Number :</strong></td>
												<td class="data-item"><?php echo @$lrow['earpe']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Date of use :</strong></td>
												<td class="data-item">><?php if(@$lrow['date_of_used']!=''){echo date('d-m-Y',strtotime(@$lrow['date_of_used']));} else {echo '';}?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<br>
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Repair Tool Details</strong></h4>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Asset :</strong></td>
										        <td class="data-item"><?php echo @$lrow['part_description'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Warehouse :</strong></td>
										        <td class="data-item"><?php echo @$repair_row['ware_house'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Current Stage :</strong></td>
										        <td class="data-item"><?php echo @$repair_row['current_stage'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Delivery date :</strong></td>
												<td class="data-item"><?php if($repair_row['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$repair_row['expected_delivery_date']));} else {echo '';} ?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Contact Person :</strong></td>
												<td class="data-item"><?php echo @$repair_row['contact_person1'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Phone Number :</strong></td>
												<td class="data-item"><?php echo @$repair_row['phone_number'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<table class="no-border">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td class="data-lable"><strong>Address1 :</strong></td>
												<td class="data-item"><?php echo @$repair_row['address1']?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address2 :</strong></td>
										        <td class="data-item"><?php echo @$repair_row['address2'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address3 :</strong></td>
												<td class="data-item"><?php echo @$repair_row['address3'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Address4 :</strong></td>
												<td class="data-item"><?php echo @$repair_row['address4'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Pincode :</strong></td>
												<td class="data-item"><?php echo @$repair_row['pin_code'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Courier Name :</strong></td>
												<td class="data-item"><?php echo @$repair_row['courier_name'];?></td>
											</tr>
											<tr>
												<td class="data-lable"><strong>Courier Number :</strong></td>
												<td class="data-item"><?php echo @$repair_row['courier_number'];?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<br>
							<div class="row">							
								<div class="header">
									<h4 align="center"><strong>Attached Documents</strong></h4>
								</div>
								<br>
								<div class="table-responsive col-md-offset-1 col-md-10">
									<table class="table table-bordered hover">
										<thead>
											<tr>
												<th class="text-center">S No.</th>
												<th width="18%" class="text-center"><strong>Attached Date</strong></th>
												<th class="text-center"><strong>Document Type</strong></th>
												<th class="text-center"><strong>Document Attached Stage</strong></th>
												<th width="30%" class="text-center"><strong>Supported Document</strong></th>
												
											</tr>
										</thead>
										<tbody>
										<?php
											if(count(@$attach_document)>0)
											{	$count = 1;
												foreach($attach_document as $ad)
												{?>	
													<tr>
														<td align="center"><?php echo $count++; ?></td>
														<td><?php echo date('d-m-Y H:i:s',strtotime($ad['created_time'])) ?></td>
														<td align="center">
																<?php 
																	foreach($documenttypeDetails as $doc)
																	{
																		if($doc['document_type_id']==@$ad['document_type_id'])
																		{
																			echo $doc['name']; 
																		}
																	}
																?>
														</td>
														<td align="center"><?php echo $ad['current_stage'];?></td>
														<td>
					                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_repair_document_path().$ad['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $ad['doc_name']; ?>
														</td>
													</tr>
												 <?php		
												} 
											} 
											else {
												?>	
												<tr><td colspan="5" align="center">No Attached Docs.</td></tr>
											<?php } ?>
										</tbody>
									</table><br>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-5 col-sm-5">
										<a class="btn btn-primary" href="<?php echo SITE_URL;?>wh_repair_request"><i class="fa fa-reply"></i> Back</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>