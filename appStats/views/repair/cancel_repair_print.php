<?php $this->load->view('commons/main_template',$nestedView); ?>
 <div class="cl-mcont">
    <div class="row"> 
      	<div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('response'); ?>
		    <div class="row"> 
				<div class="col-sm-12 col-md-12">
					<div class="block-flat">
						<div class="content">
							<form class="form-horizontal" target="_blank" action="<?php echo SITE_URL;?>insert_cancel_repair_print"  parsley-validate novalidate method="post">
							<input type="hidden" name="rc_order_id" value="<?php echo storm_encode($rc_order_id); ?>">
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Cancel Print For RRB Number :</label>
										<div class="col-sm-5">
											<p class="form-control-static"  style="padding-top: 5px;"><h5><strong><?php echo $crb_number; ?></strong></h5></p>
										</div>
								</div>
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Currently Print is:</label>
									<div class="col-sm-5">
										<p class="form-control-static" style="padding-top: 10px;"><h4><strong>
										<?php 
										if($print_list['print_type']==1)
										{
											echo 'Invoice :'.$print_list['format_number'];
										}
										else
										{
											echo 'Delivery Challan :'.$print_list['format_number'];
										}	?></strong></h4></p>
									</div>
								</div>
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Cancel Type <span class="req-fld">*</span></label>
										<div class="col-sm-5">
											<select class="form-control cancel_type_id" name="cancel_type_id" required>
												<option value="">- Cancel Type -</option>
												<?php foreach ($cancel_type_list as $type) 
												{
													echo '<option value="'.$type['cancel_type_id'].'">'.$type['name'].'</option>';
												}?>
											</select>
										</div>
								</div>
								<div class="form-group">
									<label for="inputName" class="col-sm-4 control-label">Reason <span class="req-fld">*</span></label>
										<div class="col-sm-5">
											<textarea class="form-control reason" name="reason" required></textarea>
													</div>
								</div>
								
								<div class="form-group">
					                <label class="col-sm-4 control-label">Re-Print <span class="req-fld">*</span></label>
					                <div class="col-sm-6">
					                	<?php if($print_list['print_type']==3){ ?>
					                  	<label class="checkbox-inline"> <input type="checkbox" name="print_type" class="icheck" value="4" required> Out Of State</label>
					                  	<?php } ?>
					                  	<?php if($print_list['print_type']==4){ ?>
					                  	<label class="checkbox-inline"> <input type="checkbox" name="print_type" class="icheck" value="3" required> With In State</label>
					                  	<?php } ?>
					                </div>
					            </div>
								<div class="form-group"><br>
									<div class="col-sm-offset-4 col-sm-5">
										<button class="btn btn-primary submit_btn" type="submit" name="submit" value="1" onclick="return confirm('Are you sure you want to Re-Print?')"><i class="fa fa-check"></i> Submit</button>
										<a class="btn btn-danger" href="<?php echo SITE_URL;?>closed_repair_delivery_list"><i class="fa fa-times"></i> Cancel</a>
									</div>
								</div>
							</form>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
	$(document).on('click','.submit_btn',function(){
	  var cancel_type_id = $('.cancel_type_id :selected').val();
	  var reason = $('.reason').val();
	  var icheck = $('.icheck').val();
	  if($('.icheck').is(':checked')) { var check_btn = 1; }
	  else{ var check_btn = 0;}
	  if(cancel_type_id == undefined || reason == '' || check_btn == 0)
	  {
	  	alert('Please Fill required Fields!');
	  	return false;
	  }
	  $.ajax({
	    url: SITE_URL,
	    context: document.body,
	    success: function(s){
	      window.location.href = SITE_URL+'closed_repair_delivery_list';
	    }
	  });
	});
</script>