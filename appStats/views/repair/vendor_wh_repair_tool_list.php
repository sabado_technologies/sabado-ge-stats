<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<?php
			if($flg==2)
			{
				?>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="block-flat">
								<div class="content">
									<div class="header">
										<h5 align="center"><strong>Order Details</strong></h5>
									</div>
									<div class="row">
										<div class="col-md-6">
											<table class="no-border">
												<tbody class="no-border-x no-border-y">
													<tr>
														<td class="data-lable"><strong>RR Number :</strong></td>
												        <td class="data-item"><?php echo @$rc_number;?></td>
													</tr>
													<tr>
														<td class="data-lable"><strong>Supplier :</strong></td>
												        <td class="data-item"><?php echo $supplier['supplier_code'].' - ('.$supplier['name'].')';?></td>
													</tr>
													<tr>
														<td class="data-lable"><strong>Exp Delivery Date :</strong></td>
												        <td class="data-item"><?php if($order_details['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$order_details['expected_delivery_date']));} else {echo '';} ?></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="col-md-6">
											<table class="no-border">
												<tbody class="no-border-x no-border-y">
													<tr>
														<td class="data-lable"><strong>RMA Number :</strong></td>
												        <td class="data-item"><?php echo @$order_details['rma_number'];?></td>
													</tr>
													<tr>
														<td class="data-lable"><strong>Asset Adding into  Warehouse :</strong></td>
												        <td class="data-item"><!-- <?php echo @$warehouse['wh_code'].' - '.'('.$warehouse['name'].')'?> -->
												        	<?php echo $wh_name; ?>
												        </td>
												    </tr>
													<tr>
														<td class="data-lable"><strong>Exp Return Date :</strong></td>
												        <td class="data-item"><?php if($order_details['expected_return_date']!=''){ echo date('d-m-Y',strtotime(@$order_details['expected_return_date']));} else {echo '';} ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div> <br>
									<h2 align="center">Scan QR Code to get Asset Details</h2>
										<br>
									<form class="form-horizontal form" role="form" action="<?php echo SITE_URL.'vendor_repair_asset_details';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
										<input type="hidden" name="rc_order_id" value="<?php echo storm_encode($rc_order_id)?>">
										<input type="hidden" name="rc_asset_id" value="<?php echo storm_encode($rc_asset_id)?>">
										<input type="hidden" name="add_wh_id" value="<?php echo storm_encode($add_wh_id)?>">
										<div class="form-group">
											<label for="inputName" class="col-sm-5 control-label">Asset Number <span class="req-fld">*</span></label>
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="asset_number" id="asset_number" autofocus class="form-control">
											</div>
											<div class="col-sm-3">
												<input type="checkbox" name="vehicle" value="0" class="checkbox_val"> Enable Manual Entry
											</div>
										</div>
										<div class="form-group submit_action hidden">
											<div class="col-sm-offset-5 col-sm-5">
												<button class="btn btn-primary" type="submit" value="1" name="submitUser"><i class="fa fa-check"></i> Submit</button>
												<a class="btn btn-danger" href="<?php echo SITE_URL.'vendor_wh_repair_request'; ?>"><i class="fa fa-times"></i> Cancel</a>
											</div>
										</div>
									
										<br>
										<div class="row">
											<div class="col-sm-12 col-md-12">
												<div class="table-responsive">
													<table class="table table-bordered hover" id="mytable">
														<thead>
															<tr>
																<th></th>
																<th class="text-center"><strong>RR Number</strong></th>
																<th class="text-center"><strong>Tool Number</strong></th>
																<th class="text-center"><strong>Tool Description</strong></th>
																<th class="text-center"><strong>Asset Number</strong></th>
																<th class="text-center"><strong>Serial Number</strong></th>
															</tr>
														</thead>
														<tbody>
															<?php
															if(count($asset_list)>0)
															{
																
																?>
																<tr class="asset_selected_row">
																	<td class="text-center"><input type="radio" name="radio_button" value="<?php echo $asset_list['asset_number']; ?>" class="radioBtn"></td>
																	<td class="text-center"><?php echo $asset_list['rc_number'];?>
																	</td>
																	<td class="text-center"><?php echo $asset_list['part_number'];?>
																	</td>
																	<td class="text-center"><?php echo $asset_list['part_description'];?></td>
																	<td class="text-center"><?php echo $asset_list['asset_number'];?>
																	</td>
																	<td class="text-center"><?php echo $asset_list['serial_number'];?>
																	</td>
																</tr><?php
															}
															else 
															{
															?>	<tr><td colspan="4" align="center"><span class="label label-primary">No Records</span></td></tr>
															<?php 	} ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</form> 
								</div>
								
							</div>
						</div>
					</div>
				<?php
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
				<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'vendor_wh_repair_request';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="block-flat">
								<div class="content">
									<div class="row">
										<div class="col-sm-12 form-group">
											<!-- <label class="col-sm-1 control-label">RRB No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="rcb_number" placeholder="RRB Number" value="<?php echo @$search_data['rcb_number'];?>"  class="form-control" maxlength="100">
											</div>
											<!-- <label class="col-sm-1 control-label">RR No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="rc_number" placeholder="RR Number" value="<?php echo @$search_data['rc_number'];?>"  class="form-control" maxlength="100">
											</div>
											<!-- <label class="col-sm-1 control-label">Asset No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>"  class="form-control" maxlength="100">
											</div>
											<div class=" col-sm-3">							
												<button type="submit" name="searchtools" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
												<a href="<?php echo SITE_URL.'vendor_wh_repair_request'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
											</div>
										</div>
										<div class="col-sm-12 form-group">
											<div class="col-sm-3">
											    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
											</div>
											<?php if($task_access==2 || $task_access==3 || isset($_SESSION['whsIndededArray'])) { ?>
											<!-- <label class="col-sm-1 control-label">Warehouse</label> -->
											<div class="col-sm-3">
												<select name="wh_id" class="select2 main_status supplier_id" > 
													<option value=""> -Warehouse -</option>
													<?php
													foreach($warehouse as $row)
													{
														$selected = ($row['wh_id']==@$search_data['whr_id'])?'selected="selected"':'';
														echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
													}?>
												</select>
											</div>
											<?php } ?>
											<?php  if($task_access == 3 && @$_SESSION['header_country_id']==''){?>
											   <!-- <label class="col-sm-1 control-label">Country</label> -->
												<div class="col-sm-3">
													<select class="select2" name="country_id" >
														<option value="">- Country -</option>
														 <?php
														foreach($countryList as $country)
														{
															$selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
															echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
														}
														?>
													</select>
												</div>
												<?php } ?>
										</div>
									</div>
									<div class="row">
										<div class="header"></div>
										<table class="table table-bordered" ></table>
										<div class="col-sm-12">
											<div class="col-sm-4 col-md-9"></div>
										</div>
									</div>
									<div class="header"></div>
									<div class="table-responsive">
										<table class="table table-bordered hover">
											<thead>
												<tr >
													<th class="text-center"><strong></strong></th>
													<th class="text-center"><strong>S.NO</strong></th>
													<th class="text-center"><strong>RRB Number</strong></th>
													<th class="text-center"><strong>RMA number</strong></th>
													<th class="text-center"><strong>Sent From WH</strong></th>
													
													<th class="text-center"><strong>Exp Delivery Date</strong></th>
													<th class="text-center"><strong>Exp Return Date</strong></th>
													<th class="text-center"><strong>Country</strong></th>
												</tr>
											</thead>
											<tbody>
												<?php
												if(count($calibration_results)>0)
												{
													foreach($calibration_results as $row)
													{
														?>
														<tr>
															<td class="text-center "><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'></td>
															<td class="text-center"><?php echo $sn++;?></td>
															<td class="text-center"><?php echo $row['rcb_number'];?></td>
															<td class="text-center"><?php echo $row['rma_number'];?></td>
															<td class="text-center"><?php echo $row['wh_code'].'-'.'('.$row['name'].')';?></td>
															
															<td class="text-center"><?php if($row['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$row['expected_delivery_date']));} else {echo '';}?></td>
															<td class="text-center"><?php if($row['expected_return_date']!=''){ echo date('d-m-Y',strtotime(@$row['expected_return_date']));} else {echo '';}?></td>
															<td class="text-center"><?php echo get_country_location($row['country_id']);?>
														</td>
														</tr> 
														<tr class="details">
															<?php
															if(count($row['asset_list'])>0)
															{
																?>
	                                                            <td  colspan="8">
			                                                        <table class="table">
			                                                        	<thead>
			                                                        		<th class="text-center"><strong>RR Number</strong></th>
			                                                        		<th class="text-center"><strong>Tool Number</strong></th>
			                                                        		<th class="text-center"><strong>Tool Description</strong></th>
			                                                        		<th class="text-center"><strong>Asset Number</strong></th>
			                                                        		<th class="text-center"><strong>Serial Number</strong></th>
			                                                        		<th class="text-center"><strong>RR Status</strong></th>
			                                                        		<th class="text-center"><strong>Actions</strong></th>
			                                                        	</thead>
			                                                            <tbody>
			                                                       <?php foreach(@$row['asset_list'] as $value)
			                                                        { 
			                                                        ?>
			                                                            <tr class="asset_row">
			                                                                <td align="center"><?php echo $value['rc_number']; ?></td>
			                                                                <td align="center"><?php echo $value['part_number']; ?></td>
			                                                                <td align="center"><?php echo $value['part_description']; ?></td>
			                                                                <td align="center"><?php echo $value['asset_number']; ?></td>
			                                                                 <td align="center"><?php echo $value['serial_number']; ?></td>
			                                                                <td align="center"><?php echo $value['current_stage']; ?></td>
			                                                                <?php
			                                                                if($value['current_stage_id']==22)
			                                                                {
			                                                                	?>
			                                                                	<td align="center"><a class="btn btn-default" data-modal="form-primary" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_vendor_wh_repair_request/'.storm_encode($value['rc_asset_id']);?>"><i class="fa fa-pencil"></i>
			                                                                	</a></td> <?php
			                                                                }
			                                                                if($value['current_stage_id']==23)
			                                                                {
			                                                                	?>
			                                                                	<td align="center">Scanned</td> <?php
			                                                                }
			                                                                ?>
			                                                            </tr>
			                                                        <?php     
			                                                        } ?>

			                                                            </tbody>
			                                                        </table>
			                                                    </td> <?php
			                                                }
			                                                else
			                                                {
			                                                	?>
			                                                	<td colspan="6" align="center"><span class="label label-primary">No Acknowledge Repair Records</span></td> <?php
			                                                } ?>
	                                                    </tr><?php
													}
												}
												else 
												{
												?>	<tr><td colspan="8" align="center"><span class="label label-primary">No Acknowledge Repair Records</span></td></tr>
										<?php 	} ?>
											</tbody>
										</table>
									</div>
									<div class="row">
					                	<div class="col-sm-12">
						                    <div class="pull-left">
						                        <div class="dataTables_info" role="status" aria-live="polite">
						                            <?php echo @$pagermessage; ?>
						                        </div>
						                    </div>
						                    <div class="pull-right">
						                        <div class="dataTables_paginate paging_bootstrap_full_number">
						                            <?php echo @$pagination_links; ?>
						                        </div>
						                    </div>
					                	</div> 
					                </div>
								</div>
							</div>
						</div>
					</div>
				</form> <?php
		    }?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>
<script type="text/javascript">
    $( document ).ready(function() {
    	$('#asset_number').on('input',function(){
            var asset_number = $(this).val();
            var checkbox_val = $('.checkbox_val').val();
            if(checkbox_val == 0)
            {
            	if(asset_number.trim() != '')
	            {
	            	$('.form').submit();
	            }
            }
            
        });
        $('body').on('click',function(e){
        	$('#asset_number').focus();
        });
    });
    $(document).on('click','.checkbox_val',function(){
		var value = $(this).val();
		if(value == 0)
		{
			$('.checkbox_val').val('1');
			$('.submit_action').removeClass('hidden');
		}
		else
		{
			$('.checkbox_val').val('0');
			$('.radioBtn').prop('checked',false);
			$('#asset_number').val('');
			$('.submit_action').addClass('hidden');
		}
	});
	$(document).on('click','.radioBtn',function(){
		var asset_number = $(this).val();
		if(asset_number!='')
		{
			$('#asset_number').val(asset_number);
			$('.checkbox_val').prop('checked', true);
			$('.checkbox_val').val('1');
			$('.submit_action').removeClass('hidden');
		}
	});
</script>