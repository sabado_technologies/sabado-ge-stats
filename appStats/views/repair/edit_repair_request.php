<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<?php
			if($flg==2)
			{
				?>
				<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
					<input type="hidden" name="rc_order_id"  value="<?php echo storm_encode($rc_order_id);?>">
					<input type="hidden" name="rc_asset_id"  value="<?php echo storm_encode($rc_asset_id);?>">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="block-flat">
								<div class="content">
									<div class="row">
										<div class="col-sm-12 col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered hover" id="mytable">
												<thead>
													<tr>
														<th class="text-center"><strong></strong></th>
														<th class="text-center"><strong>RR Number</strong></th>
														<th class="text-center"><strong>Tool Number</strong></th>
														<th class="text-center"><strong>Tool Description</strong></th>
														<th class="text-center"><strong>Tool Type</strong></th>
														<th class="text-center"><strong>Asset Number</strong></th>
														<th class="text-center"><strong>Serial Number</strong></th>
													</tr>
												</thead>
												<tbody>
													<?php
													if(count($asset_details)>0)
													{
														
														?>
														<tr class="asset_selected_row">
															<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'></td>
															<td class="text-center"><?php echo $asset_details['rc_number'];?>
																
															</td>
															<td class="text-center"><?php echo $asset_details['part_number'];?></td>
															<td class="text-center"><?php echo $asset_details['part_description'];?></td>
															<td class="text-center"><?php echo $asset_details['tool_type'];?></td>
															<td class="text-center"><?php echo $asset_details['asset_number'];?>
															</td>
															<td class="text-center"><?php echo $asset_details['serial_number'];?>
															</td>
														</tr> 
														<tr class="details">
															<?php
															if(count($asset_details['asset_health_list'])>0)
															{
																?>
																<td colspan="7">
																	<table class="table">
																		<thead>
																			<th class="text-center"><strong>Serial Number</strong></th>
																			<th class="text-center"><strong>Tool Number</strong></th>
																			<th class="text-center"><strong>Tool Description</strong></th>
																			<th class="text-center"><strong>Tool Level</strong></th>
																			<th class="text-center"><strong>Qty</strong></th>
																			<th class="text-center"><strong>Tool Health</strong></th>
																		</thead>
																		<tbody>
																			<?php
																			foreach($asset_details['asset_health_list'] as $value)
																			{
																				?>
																				<tr class="asset_row">
																					<td align="center">
																						<?php echo $value['serial_number'];?>
																					</td>
																					<td align="center">
																						<?php echo $value['part_number'];?>
																					</td>
																					<td align="center">
																						<?php echo $value['part_description'];?>
																					</td>
																					<td align="center">
																						<?php echo $value['part_level_name'];?>
																					</td>
																					<td align="center">
																						<?php echo $value['quantity'];?>
																					</td>
																					<td align="center">
																						<?php 
																						if($selected_asset_health[$value['part_id']]['asset_condition_id'] == 1)
																							echo 'Good';
																						if($selected_asset_health[$value['part_id']]['asset_condition_id'] == 2)
																							echo 'Defected';
																						if($selected_asset_health[$value['part_id']]['asset_condition_id'] == 3)
																							echo 'Missing';
																						?>
																					</td>
																				</tr> <?php
																			}?>
																		</tbody>
																	</table>
																</td> <?php
															} 
															else
															{ ?>
																<td colspan="6" align="center"><span class="label label-primary">No Open Calibration Records</span></td> <?php
															}?>
														</tr><?php
													}
													else 
													{
													?>	<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
													<?php 	} ?>
												</tbody>
											</table>
										</div>
										<div class="row">
			                        		<div class="form-group">
			                        			<div class="col-md-12">
			                        				<label class="control-label col-sm-4">Status :</label>
				                        			<div class="custom_icheck col-sm-5">
				                        				<label class="radio-inline">
						                        			<div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
					                                            <input type="radio" class="repair_status" value="1" name="status" checked style="position: absolute; opacity: 0;">
					                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
								                            </div> Repaired
								                        </label>
								                        <label class="radio-inline">
						                        			<div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
					                                            <input type="radio" class="repair_status" value="2" name="status" style="position: absolute; opacity: 0;">
					                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
								                            </div> Not Repaired
								                        </label>
								                    </div>
			                        			</div>
			                        		</div>
		                        		</div>
		                        		<div class="row repair_type hidden">
		                        			<div class="form-group">
		                        				<div class="col-md-12">
			                        				<label class="control-label col-sm-4">Type :</label>
			                        				<div class="custom_icheck col-sm-5">
				                        				<label class="radio-inline">
						                        			<div class="iradio_square-blue checked" style="position: relative;" aria-checked="true" aria-disabled="false">
					                                            <input type="radio" value="1" name="reason" checked style="position: absolute; opacity: 0;">
					                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
								                            </div> Scrap
								                        </label><br>
								                        <label class="radio-inline">
						                        			<div class="iradio_square-blue" style="position: relative;" aria-checked="true" aria-disabled="false">
					                                            <input type="radio" value="2" name="reason" style="position: absolute; opacity: 0;">
					                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
								                            </div> Out of Tolerance
								                        </label>
								                    </div>
			                        			</div>
		                        			</div>
		                        		</div>
		                        		<div class="row">
			                        		<div class="form-group">
				                        		<div class="col-md-12">
			                        				<label class="col-sm-4 control-label">Remarks :</label>
													<div class="col-sm-4">
														<textarea class="form-control" name="remarks"></textarea>
													</div>
			                        			</div>
		                        			</div>
		                        		</div>
											<div class="row">
												<div class="form-group">
					                        		<div class="col-md-12">
							                        	<div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
															<a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a><br>
														</div>
														<div class="table-responsive col-md-offset-1 col-md-9  tab_hide">
<table class="table hover document_table">
	<thead>
		<tr>
			<th  width="8%" class="text-center"><strong>Sno</strong></th>
			<th width="40%" class="text-center"><strong>Document Type</strong></th>
			<th width="40%" class="text-center"><strong>Supported Document</strong></th>
			<th width="8%"  class="text-center"><strong>Delete</strong></th>
		</tr>
	</thead>
		<tbody>
		<?php $count = 1;
			if(count(@$main_doc)>0)
			{	
				foreach($main_doc as $doc)
					{?>	
						<tr class="attach">
							<input type="hidden" name="doc_id" class="doc_id" value="<?php echo $doc['rcd_id'];?>">
							<td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
							<td >
								<?php 
									foreach($documenttypeDetails as $docs)
									{
										if($docs['document_type_id']==@$doc['document_type_id'])
										{
											echo $docs['name'].' - ('.date('d-m-Y H:i:s',strtotime($doc['created_time'])).')'; 
										}
									}
								?>
							</td>
							<td>
		                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
							</td>
							<td class="text-center">
								<a class="btn link btn-danger btn-sm" disabled> <i class="fa fa-trash-o"></i></a>
					        </td>
						</tr>
					 <?php		
					}
				}
				if(count($attach_document)>0)
				{
					foreach($attach_document as $doc)
					{?>	
						<tr class="attach">
							<td class="text-center"><span class="sno"><?php echo $count++; ?></span></td>
							<td>
								<?php 
									foreach($documenttypeDetails as $docs)
									{
										if($docs['document_type_id']==@$doc['document_type_id'])
										{
											echo $docs['name'].' - ('.date('d-m-Y H:i:s',strtotime($doc['created_time'])).')'; 
										}
									}
								?>
							</td>
							<td>
		                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
							</td>
							<td class="text-center">
								<a class="btn link btn-danger btn-sm" disabled> <i class="fa fa-trash-o"></i></a>
			        		</td>
						</tr>
					 <?php		
					}
				}?>			
				<tr class="doc_row">
					<td class="text-center">
						<span class="sno"><?php echo $count; ?></span>
					</td>
					<td>
						<select name="document_type[1]" class="form-control doc_type" > 
							<option value="">Select Document Type</option>
							<?php 
								foreach($documenttypeDetails as $doc)
								{
									echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
								}
							?>
						</select>
					</td>
					<td>
			    		<input type="file" id="document" name="support_document_1" class="document">
					</td>
					
			        <td class="text-center"><a <?php if(@$flg == 2){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
				</tr>
			</tbody>
</table><br>
														</div>
													</div>
					                    		</div>
					                    		<div class="form-group">
													<div class="col-sm-offset-5 col-sm-5">
														<button class="btn btn-primary" onclick="return confirm('Are you sure you want to Submit?')" type="submit" name="submit_tool"><i class="fa fa-check"></i> Submit</button>
														<a class="btn btn-danger" href="<?php echo SITE_URL;?>open_repair_request"><i class="fa fa-times"></i> Cancel</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form> <?php
			}?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>