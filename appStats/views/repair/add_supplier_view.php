<?php $this->load->view('commons/main_template',$nestedView); ?>
<div id="loaderID" style="position:fixed; top:50%; left:50%; z-index:2; opacity:0"><img src="<?php echo SITE_URL1;?>assets/images/ajax-loading-img.gif" /></div>
<div class="cl-mcont" id="orderCartContainer">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12" align="right" style="margin-bottom: 10px;">
										<a href="<?php echo SITE_URL.'repair_tool/0'?>" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Add More to cart</a>
									</div>
									<div class="table-responsive">
										<table class="table table-bordered hover" id="mytable">
											<thead>
												<tr>
													<th class="text-center"><strong>RR Number</strong></th>
													<th class="text-center"><strong>Tool Number</strong></th>
													<th class="text-center"><strong>Tool Description</strong></th>
													<th class="text-center"><strong>Asset Number</strong></th>
													<th class="text-center"><strong>Serial Number</strong></th>
													<th class="text-center"><strong>Warehouse</strong></th>
													<th class="text-center"><strong>Action</strong></th>
												</tr>
											</thead>
											<tbody>
												<?php
												if(count($assets)>0)
												{
													foreach($assets as $row)
													{
														?>
														<tr class="toolSelectedRow">
															<td class="text-center"><?php echo $row['rc_number'];?>
																<input type="hidden" name="rc_asset_id[]" value="<?php echo $row['rc_asset_id']?>">
																<input type="hidden" name="wh_id[]" value="<?php echo $row['wh_id']?>">
															</td>
															<td class="text-center"><?php echo $row['part_number'];?></td>
															<td class="text-center"><?php echo $row['part_description'];?></td>
															<td class="text-center"><?php echo $row['asset_number'];?></td>
															<td class="text-center"><?php echo $row['serial_number'];?></td>
															<td class="text-center"><?php echo $row['wh_code'].' - '.'('.$row['warehouse'].')';?></td>
															<td class="text-center">
																<a class="btn btn-danger btn-xs removeRow" 
															data-cid="<?php echo $row['rc_asset_id']; ?>" href="#"  ><i class="fa fa-times"></i></a>
															</td>
														</tr> <?php
													}
												}
												else 
												{
												?>	<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
												<?php 	} ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="row">
										<div class="form-group">
			                        		<div class="col-md-12">
												<label class="col-sm-2 control-label">Supplier :<span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<select name="supplier_id" required class="select2 supplier_id" > 
														<option value="">- Supplier -</option>
														<?php
														foreach($repair_supplier as $row)
														{
															$selected="";
															if($row['supplier_id']==$lrow['supplier_id']){
																$selected="selected";
															}
															echo '<option value="'.$row['supplier_id'].'" '.$selected.'>'.$row['supplier_code'].' - ('.$row['name'].')</option>';
														}?>
													</select>
												</div>
			                        			<label class="col-sm-2 control-label">Contact Person: </label>
			                        			<div class="col-sm-4 serial_class">
													<input type="text" autocomplete="off" readonly class="form-control contact_person" placeholder="Contact Person" name="contact_person" value="">
												</div>
											</div>
		                        		</div>
		                        		<div class="form-group">
			                        		<div class="col-md-12">
												<label class="col-sm-2 control-label">Exp Delivery Date: <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<input class="form-control" required size="16" type="text" autocomplete="off"  readonly placeholder="Date" name="delivery_date" id="dateFrom" style="cursor:hand;background-color: #ffffff">
												</div>
												<label class="col-sm-2 control-label">Exp Return Date: <span class="req-fld">*</span></label>
												<div class="col-sm-4">
													<input class="form-control" required size="16" type="text" autocomplete="off"  readonly placeholder="Date" name="return_date" id="dateTo" style="cursor:hand;background-color: #ffffff">
												</div>
												
											</div>
		                        		</div>
		                        		<div class="form-group">
			                        		<div class="col-md-12">
												<label class="col-sm-2 control-label">GST Number: </label>
												<div class="col-sm-4">
													<input type="text" autocomplete="off" readonly class="form-control gst_number" placeholder="Gst Number" name="gst_number" >
												</div>
			                        			<label class="col-sm-2 control-label">Pan Number: </label>
												<div class="col-sm-4 serial_class">
													<input type="text" autocomplete="off" readonly  class="form-control pan_number" placeholder="Pan Number" name="pan_number" >
												</div>
											</div>
		                        		</div>
		                        		<div class="form-group">
			                        		<div class="col-md-12">
			                        			<label class="col-sm-2 control-label">Phone Number:</label>
												<div class="col-sm-4 serial_class">
													<input type="text" autocomplete="off"  class="form-control contact_number" placeholder="Phone Number" name="phone_number" readonly >
												</div>
												<label class="col-sm-2 control-label">RMA Number:<span class="req-fld">*</span></label>
			                        			<div class="col-sm-4 serial_class">
													<input type="text" autocomplete="off" required class="form-control" placeholder="RMA number" name="rma_number" >
												</div>
											</div>
		                        		</div>
		                        		<div class="form-group">
			                        		<div class="col-md-12">
			                        			<label class="col-sm-2 control-label">Address1:</label>
			                        			<div class="col-sm-4">
													<textarea class="form-control address1" readonly name="address_1"></textarea>
												</div>
			                        			<label class="col-sm-2 control-label">Address2:</label>
			                        			<div class="col-sm-4 serial_class">
													<textarea class="form-control address2" readonly name="address_2"></textarea>
												</div>
											</div>
		                        		</div>
		                        		<div class="form-group">
			                        		<div class="col-md-12">
			                        			<label class="col-sm-2 control-label">City:</label>
												<div class="col-sm-4">
													<textarea class="form-control address3" readonly name="address_3"></textarea>
												</div>
			                        			<label class="col-sm-2 control-label">State:</label>
			                        			<div class="col-sm-4 serial_class">
													<textarea class="form-control address4" readonly name="address_4"></textarea>
												</div>
											</div>
		                        		</div>
		                        		<div class="form-group">
		                        			<div class="col-md-12">
		                        				<label class="col-sm-2 control-label">Pincode:</label>
			                        			<div class="col-sm-4">
													<input type="text" autocomplete="off" readonly class="form-control pin_code" placeholder="Pincode" name="zip_code" >
												</div>
												<label class="col-sm-2 control-label">Remarks:</label>
			                        			<div class="col-sm-4 serial_class">
													<textarea class="form-control"  name="remarks"></textarea>
												</div>
		                        			</div>
		                        		</div>
		                        		<div class="form-group">
			                        		<div class="col-md-12">
					                        	<div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
													<a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a><br>
												</div>
												<div class="table-responsive col-md-offset-1 col-md-9 tab_hide">
													<table class="table hover document_table">
														<thead>
															<tr>
																<th  width="8%"><strong>Sno</strong></th>
																<th width="40%" ><strong>Document Type</strong></th>
																<th width="40%" ><strong>Supported Document</strong></th>
																<th width="8%" ><strong>Delete</strong></th>
															</tr>
														</thead>
														<tbody>
														<?php $count = 1;
															if(count(@$attach_document)>0)
															{	
																foreach($attach_document as $doc)
																{?>	
																	<tr class="attach">
																		<input type="hidden" name="doc_id" class="doc_id" value="<?php echo $doc['rcd_id'];?>">
																		<td align="center"><span class="sno"><?php echo $count++; ?></span></td>
																		<td align="left">
																			<?php 
																				foreach($documenttypeDetails as $docs)
																				{
																					if($docs['document_type_id']==@$doc['document_type_id'])
																					{
																						echo $docs['name'].' - ('.date('d-m-Y H:i:s',strtotime($doc['created_time'])).')'; 
																					}
																				}
																			?>
																		</td>
																		<td>
									                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
																		</td>
																		<td>
																		<?php if($doc['status']==1)
																		{ ?>
																			<a class="btn link btn-danger btn-sm deactivate" > <i class="fa fa-trash-o"></i></a>
																        
																		<?php } 
																		else
																			{?>
																			<a class="btn link btn-info btn-sm activate" > <i class="fa fa-check"></i></a>
																		<?php } ?>
																        </td>
																	</tr>
																 <?php		
																} 
															} ?>			
															<tr class="doc_row">
																<td align="center">
																	<span class="sno"><?php echo $count; ?></span>
																</td>
																<td>
																	<select name="document_type[1]" class="form-control doc_type" > 
																		<option value="">Select Document Type</option>
																		<?php 
																			foreach($documenttypeDetails as $doc)
																			{
																				echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
																			}
																		?>
																	</select>
																</td>
																<td>
							                                		<input type="file" id="document" name="support_document_1" class="document">
																</td>
																
														        <td><a <?php if(@$flg == 2){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
															</tr>
														</tbody>
													</table><br>
												</div>
											</div>
				                    	</div>
				                    	<div class="form-group">
											<div class="col-sm-offset-5 col-sm-5">
												<button class="btn btn-primary"  onclick="return confirm('Are you sure you want to Submit?')" type="submit" name="submit_tool"><i class="fa fa-check"></i> Submit</button>
												<a class="btn btn-danger" href="<?php echo SITE_URL;?>repair_tool"><i class="fa fa-times"></i> Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>