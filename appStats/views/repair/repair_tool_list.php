<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<div class="block-flat">
				<div class="content">
					<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'repair_tool';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
						<div class="row">
							<div class="col-sm-12 form-group">
	                            <div class="col-sm-3">
									<input type="text" autocomplete="off" name="crn" placeholder="RR Number" value="<?php echo @$search_data['crn'];?>"  class="form-control" maxlength="100">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="tool_no" placeholder="Tool Number" value="<?php echo @$search_data['tool_no'];?>" id="location" class="form-control" maxlength="100">
								</div>
								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Description" value="<?php echo @$search_data['tool_desc'];?>" id="serial_number" class="form-control"  maxlength="80">
								</div>
								<div class="col-md-3">
									<button type="submit" name="searchtools" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Search"><i class="fa fa-search"></i></button>

									<button type="submit" name="reset" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Reset Search Filters"><i class="fa fa-reply"></i></button>

									<button type="submit" formaction="<?php echo SITE_URL.'repair_tool'; ?>" name="refresh" value="1" class="btn btn-success" data-container="body" data-placement="top"  data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
								</div>
							</div>
							<div class="col-sm-12 form-group">

								<div class="col-sm-3">
                                    <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
								</div>
								<div class="col-sm-3">
								    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
								</div>
								<?php if($task_access==2 || $task_access==3) { ?>
									<div class="col-sm-3">
										<select name="wh_id" class="select2 main_status supplier_id" > 
											<option value="">Warehouse</option>
											<?php
											foreach($warehouse as $row)
											{
												$selected = ($row['wh_id']==@$search_data['whr_id'])?'selected="selected"':'';
												echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
											}?>
										</select>
									</div>
								<?php } ?>

								<?php if($task_access == 3 && @$_SESSION['header_country_id']==''){?>
									<div class="col-sm-3">
										<select class="select2" name="country_id" >
											<option value="">- Country -</option>
											 <?php
											foreach($countryList as $country)
											{
												$selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
												echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
											}
											?>
										</select>
									</div>
								<?php } ?>
							</div>
						</div>
						<div class="row">
							<div class="header"></div>
							<table class="table table-bordered" ></table>
						</div>
					</form>
					<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'repair_tool/'.$current_offset;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
						<div class="row" style="margin-bottom:3px;">
							<div class="col-sm-4" align="right"></div>
							<div class="col-sm-3">
							<?php   
							if(count(@$_SESSION['r_wh'])>0)
							{
								if(count($_SESSION['r_asset_id']) > 0)
								{ ?>
									<label class=" col-sm-12 alert alert-info" align="center" style="padding-top: inherit; padding-bottom: inherit;margin-bottom:2px;">
									<?php 
									echo count($_SESSION['r_asset_id']);
									if(count($_SESSION['r_asset_id']) == 1)
									{ echo ' Asset <strong>'; } 
									else
									{ echo ' Assets <strong>';}
								?> Added to cart</strong></label>
								<?php
								}
							}?>
							</div>
							<div class="col-sm-1"></div>
								<div class="col-sm-2" align="right">
								<button type="submit" id="add" name="add" class="btn btn-success" disabled><i class="fa fa-shopping-cart"></i> Add to Cart</button></div>	
								<div class="col-sm-1">
									<a href="<?php echo SITE_URL.'add_repair_supplier';?>" id="issue" name="issue" class="btn btn-success"
									<?php 
										if(!isset($_SESSION['r_asset_id']))
										{ ?>
										disabled
									<?php } 
									else if(count(@$_SESSION['r_asset_id']) == 0)
										{ ?>
										disabled
									<?php } ?>
									><i class="fa fa-mail-forward"></i> Proceed</a>
								</div>
						</div>
						<div class="table-responsive">
							<table class="table table-bordered hover" id="mytable">
								<thead>
									<tr>
										<th class="text-center"><strong></strong></th>
										<th class="text-center"><strong>S.NO</strong></th>
										<th class="text-center"><strong>RR Number</strong></th>
										<th class="text-center"><strong>Tool Number</strong></th>
										<th class="text-center"><strong>Tool Description</strong></th>
										<th class="text-center"><strong>Asset Number</strong></th>
										<th class="text-center"><strong>Serial Number</strong></th>
										<th class="text-center"><strong>Inventory</strong></th>
										<th class="text-center"><strong>Tool Availability</strong></th>
										<th class="text-center"><strong>Asset Status</strong></th>
										<th class="text-center"><strong>Country</strong></th>
										<th class="text-center"><strong>Actions</strong></th>
									</tr>
								</thead>
								<tbody>
									<?php

									if(count($calibration_results)>0)
									{
										
										if(count(@$_SESSION['r_wh'])>0)
										{
											$rc_wh_list = $_SESSION['r_wh'];
											foreach ($rc_wh_list as $key => $value) 
											{ ?>
												<input type="hidden" name="rc_wh_list" value="<?php echo $value; ?>" class="rc_wh_list">
											<?php }
										}
										foreach($calibration_results as $row)
										{
											$isCarted = false;
											if(isset($_SESSION['r_asset_id'][$row['rc_asset_id']])){$isCarted = true;}
											$chk_st = ($isCarted)?'checked="checked"':'';
											$disable_st = ($isCarted)?'':'disabled="disabled"';
											$comp_qty = ($isCarted)?@$_SESSION['r_asset_id'][$row['rc_asset_id']]:'';
											$position=get_asset_position_with_flag($row['asset_id']);
											$text=$position[0];
											$flag=$position[1];

											?>
											<tr class="toolRow">
												<input type="hidden" name="asset_wh[<?php echo $row['rc_asset_id'];?>]" value="<?php echo $row['wh_id']; ?>">
												<td class="text-center">
													<?php if($row['approval_status']==0 && $flag == 1)
													{ ?>
														<input type="checkbox" <?php echo $chk_st; ?> name="asset_id[]" value="<?php echo @$row['rc_asset_id']; ?>" class="icheck" data-wh-id="<?php echo $row['wh_id']; ?>" data-flag="<?php echo $flag; ?>">
													<?php 
													} 
													else 
													{ ?>
														<p data-container="body" data-placement="top" data-toggle="tooltip" title="<?php if($row['approval_status']==1){ echo get_da_status($row['asset_id'],1); }else { echo "Asset Must be in Inventory";} ?>" ><i class="fa fa-exclamation-triangle" style="color:orange"></i>
														</p>
												    <?php } ?>
												</td>
												<td class="text-center"><?php echo $sn++;?></td>
												<td class="text-center"><?php echo $row['rc_number'];?></td>
												<td class="text-center"><?php echo $row['part_number'];?></td>
												<td class="text-center"><?php echo $row['part_description'];?></td>
												<td class="text-center"><?php echo $row['asset_number'];?></td>
												<td class="text-center"><?php echo $row['serial_number'];?></td>
												<td class="text-center"><?php echo $row['wh_code'].' - '.'('.$row['warehouse'].')';?>
												</td>
												<td class="text-center"><?php echo @$text; ?>
												</td>
												<td class="text-center"><?php echo @$row['asset_status_name']; ?>
												</td>
												<td class="text-center"><?php echo get_country_location($row['country_id']);?>
												</td>
												<td class="text-center">
													<?php if($row['approval_status']==0 && $flag == 1){ ?>
													<a class="btn btn-danger" data-container="body" data-placement="top"  data-toggle="tooltip" title="Delete RR Request" style="padding:3px 3px;" onclick="return confirm('Are you sure you want to Delete RR Request?')" href="<?php echo SITE_URL.'delete_repair_cr_request/'.storm_encode($row['rc_asset_id']);?>"><i class="fa fa-trash-o"></i></a>
												<?php } else {
													echo "NA";
												}?>
												</td>
											</tr> <?php
										}
									}
									else 
									{
									?>	<tr><td colspan="12" align="center"><span class="label label-primary">No Records</span></td></tr>
									<?php 	} ?>
								</tbody>
							</table>
						</div>
					</form>
					<div class="row">
	                	<div class="col-sm-12">
		                    <div class="pull-left">
		                        <div class="dataTables_info" role="status" aria-live="polite">
		                            <?php echo @$pagermessage; ?>
		                        </div>
		                    </div>
		                    <div class="pull-right">
		                        <div class="dataTables_paginate paging_bootstrap_full_number">
		                            <?php echo @$pagination_links; ?>
		                        </div>
		                    </div>
	                	</div> 
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>