<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<form class="form-horizontal" role="form" action="<?php echo SITE_URL;?>wh_repair_request"  parsley-validate novalidate method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="block-flat">
							<div class="content">
								<div class="row">
									<div class="col-sm-12">
										<label class="col-sm-2 control-label">Tool Number</label>
										<div class="col-sm-3">
	                                        <input type="text" autocomplete="off" name="tool_no" placeholder="Tool Number" value="<?php echo @$search_data['tool_no'];?>" id="location" class="form-control" maxlength="100">
										</div>
										<label class="col-sm-2 control-label">Tool Description</label>
										<div class="col-sm-3">
											<input type="text" autocomplete="off" name="tool_desc" placeholder="Tool Description" value="<?php echo @$search_data['tool_desc'];?>" id="serial_number" class="form-control"  maxlength="80">
										</div>
									</div>
									<div class="col-sm-12">
										<label class="col-sm-2 control-label">Asset Number</label>
										<div class="col-sm-3">
		                                    <input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>" class="form-control">
										</div>
										<label class="col-sm-2 control-label">Warehouse</label>
										<div class="col-sm-3">
											<select name="wh_id" required class="form-control supplier_id" > 
												<option value="">Warehouse</option>
												<?php
												foreach($warehouse as $row)
												{
													$selected="";
													if($row['wh_id']==$search_data['wh_id']){
														$selected="selected";
													}
													echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' -('.$row['name'].')</option>';
												}?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="header"></div>
									<table class="table table-bordered" ></table>
									<div class="col-sm-12">
										<div class="col-sm-4 col-md-9"></div>
										<div class="col-sm-3 col-md-3">							
											<button type="submit" name="searchtools" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
											<a href="<?php echo SITE_URL.'wh_repair_request'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
										</div>
									</div>
								</div>
								<div class="table-responsive" style="margin-top: 10px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center"><strong>S.No</strong></th>
                                                <th class="text-center"><strong>Asset Number</strong></th>
                                                <th class="text-center"><strong>Tool Number</strong></th>
                                                <th class="text-center"><strong>Tool Description</strong></th>
                                                <th class="text-center"><strong>Warehouse</strong></th>
                                                <th class="text-center"><strong>Current Position</strong></th>
                                                <th class="text-center"><strong>Current Stage</strong></th>
                                                <th class="text-center"><strong>Action</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count(@$repair_results)>0)
                                            {   
                                                foreach(@$repair_results as $row)
                                                { 
                                                ?>
                                                    <tr>
                                                       <td align="center"><?php echo $sn++; ?></td>
                                                        <td ><?php echo $row['asset_number']; ?></td>
                                                        <td align="center"><?php echo $row['part_number']; ?></td>
                                                        <td ><?php echo $row['part_description']; ?></td>
                                                        <td ><?php echo $row['wh_code'].' -('.$row['warehouse'].' )'; ?></td>
                                                        <td ><?php echo get_asset_position($row['asset_id']); ?></td>
                                                        <td><?php echo $row['current_stage'];?></td>
                                                        <td class="text-center">
                                                        	<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View"  href="<?php echo SITE_URL.'view_wh_repair_request/'.storm_encode($row['repair_id']);?>"><i class="fa fa-eye"></i></a>
                                                        	<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="Generate Repair Process"  href="<?php echo SITE_URL.'edit_wh_repair_request/'.storm_encode($row['repair_id']);?>"><i class="fa fa-pencil"></i></a>
                                                        </td>
                                                    </tr><?php
                                                }
                                            } else {?>
                                                <tr><td colspan="8" align="center"><span class="label label-primary">No Records Found</span></td></tr>
                                    <?php   } ?>
                                        </tbody>
                                    </table>
                                </div>
								<div class="row">
				                	<div class="col-sm-12">
					                    <div class="pull-left">
					                        <div class="dataTables_info" role="status" aria-live="polite">
					                            <?php echo @$pagermessage; ?>
					                        </div>
					                    </div>
					                    <div class="pull-right">
					                        <div class="dataTables_paginate paging_bootstrap_full_number">
					                            <?php echo @$pagination_links; ?>
					                        </div>
					                    </div>
				                	</div> 
				                </div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>