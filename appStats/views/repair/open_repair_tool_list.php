<?php $this->load->view('commons/main_template',$nestedView); ?>
<div class="cl-mcont">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<?php echo $this->session->flashdata('response'); ?>
			<?php
			if($flg==2)
			{
				?>
				<form class="form-horizontal" role="form" action="<?php echo $form_action;?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
					<input type="hidden" name="rc_order_id"  value="<?php echo @$rc_order_id;?>">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="block-flat">
								<div class="content">
									<div class="row">
										<div class="col-sm-12 col-md-12">
											<div class="table-responsive">
												<table class="table table-bordered hover" id="mytable">
													<thead>
														<tr>
															<th class="text-center"><strong>RR Number</strong></th>
															<th class="text-center"><strong>Tool Number</strong></th>
															<th class="text-center"><strong>Tool Description</strong></th>
															<th class="text-center"><strong>Asset Number</strong></th>
															<th class="text-center"><strong>Serial Number</strong></th>
															<th class="text-center"><strong>Action</strong></th>
														</tr>
													</thead>
													<tbody>
														<?php
														if(count($asset_details)>0)
														{
															foreach($asset_details as $row)
															{
																?>
																<tr class="asset_selected_row">
																	<td class="text-center"><?php echo $row['rc_number'];?>
																		<input type="hidden" name="rc_asset_id[]" value="<?php echo $row['rc_asset_id']?>">
																		<input type="hidden" name="wh_id[]" value="<?php echo $row['wh_id']?>">
																	</td>
																	<td class="text-center"><?php echo $row['part_number'];?></td>
																	<td class="text-center"><?php echo $row['part_description'];?></td>
																	<td class="text-center"><?php echo $row['asset_number'];?></td>
																	<td class="text-center"><?php echo $row['serial_number'];?></td>
																	<td align="center">
																	<a class="btn btn-danger btn-xs removeasset" data-rc_asset_id="<?php echo $row['rc_asset_id']; ?>" href="#"><i class="fa fa-times"></i></a></td>
																</tr> <?php
															}
														}
														else 
														{
														?>	<tr><td colspan="6" align="center"><span class="label label-primary">No Records</span></td></tr>
														<?php 	} ?>
													</tbody>
												</table>
											</div>
											<div class="row">
												<div class="form-group">
					                        		<div class="col-md-12">
														<label class="col-sm-2 control-label"> Supplier :<span class="req-fld">*</span></label>
														<div class="col-sm-4">
															<select name="supplier_id" required class="form-control main_status supplier_id" > 
																<option value="">- Supplier -</option>
																<?php
																foreach($repair_supplier as $row)
																{
																	$selected = ($row['supplier_id']==@$cal_row['supplier_id'])?'selected="selected"':'';
																	echo '<option value="'.$row['supplier_id'].'" '.$selected.'>'.$row['supplier_code'].' - ('.$row['name'].')</option>';
																}?>
															</select>
														</div>
					                        			<label class="col-sm-2 control-label">Contact Person </label>
					                        			<div class="col-sm-4 serial_class">
															<input type="text" autocomplete="off" readonly value="<?php echo $cal_row['contact_person'];?>" class="form-control serial_number contact_person" placeholder="Contact Person" name="contact_person" value="<?php echo $cal_row['contact_person'];?>" >
														</div>
													</div>
				                        		</div>
				                        		<div class="form-group">
					                        		<div class="col-md-12">
														<label class="col-sm-2 control-label">Exp Delivery Date:<span class="req-fld">*</span></label>
														<div class="col-sm-4">
														<input class="form-control" value="<?php if($cal_row['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$cal_row['expected_delivery_date']));} else {echo '';} ?>" required size="16" type="text" autocomplete="off"  readonly placeholder="Date" name="delivery_date" id="dateFrom" style="cursor:hand;background-color: #ffffff">
														</div>
														<label class="col-sm-2 control-label">Exp Return Date: <span class="req-fld">*</span></label>
														<div class="col-sm-4">
															<input class="form-control" value="<?php if($cal_row['expected_return_date']!=''){ echo date('d-m-Y',strtotime(@$cal_row['expected_return_date']));} else {echo '';} ?>" required size="16" type="text" autocomplete="off"  readonly placeholder="Date" name="return_date" id="dateTo" style="cursor:hand;background-color: #ffffff">
														</div>
													</div>
				                        		</div>
				                        		<div class="form-group">
					                        		<div class="col-md-12">
														<label class="col-sm-2 control-label">GST Number: </label>
														<div class="col-sm-4">
															<input type="text" autocomplete="off" value="<?php echo $cal_row['gst_number'];?>" readonly class="form-control gst_number" placeholder="Gst Number" name="gst_number" >
														</div>
					                        			<label class="col-sm-2 control-label">Pan Number: </label>
														<div class="col-sm-4 serial_class">
															<input type="text" autocomplete="off" readonly class="form-control pan_number" placeholder="Pan Number" name="pan_number" value="<?php echo @$cal_row['pan_number'];?>" >
														</div>
													</div>
				                        		</div>
				                        		<div class="form-group">
					                        		<div class="col-md-12">
														<label class="col-sm-2 control-label">Phone Number:</label>
														<div class="col-sm-4 serial_class">
															<input type="text" autocomplete="off" value="<?php echo $cal_row['phone_number'];?>" readonly class="form-control serial_number contact_number" placeholder="Phone Number" name="phone_number" >
														</div>
					                        			<label class="col-sm-2 control-label">RMA Number: <span class="req-fld">*</span></label>
														<div class="col-sm-4 serial_class">
															<input type="text" autocomplete="off" value="<?php echo $cal_row['rma_number'];?>"  class="form-control" placeholder="RMA Number" name="rma_number" >
														</div>
													</div>
				                        		</div>
				                        		<div class="form-group">
					                        		<div class="col-md-12">
														<label class="col-sm-2 control-label">Address1:</label>
					                        			<div class="col-sm-4">
															<textarea class="form-control address1" readonly name="address_1"><?php echo $cal_row['address1'];?></textarea>
														</div>
					                        			<label class="col-sm-2 control-label">Address2: </label>
					                        			<div class="col-sm-4 serial_class">
															<textarea class="form-control address2" readonly name="address_2"><?php echo $cal_row['address2'];?></textarea>
														</div>
													</div>
				                        		</div>
				                        		<div class="form-group">
					                        		<div class="col-md-12">
														<label class="col-sm-2 control-label">City:</label>
														<div class="col-sm-4">
															<textarea class="form-control address3" readonly name="address_3"><?php echo $cal_row['address3'];?></textarea>
														</div>
					                        			<label class="col-sm-2 control-label">State:</label>
					                        			<div class="col-sm-4 serial_class">
															<textarea class="form-control address4" readonly name="address_4"><?php echo $cal_row['address4'];?></textarea>
														</div>
													</div>
				                        		</div>
				                        		<div class="form-group">
					                        		<div class="col-md-12">
														<label class="col-sm-2 control-label">Pincode: </label>
					                        			<div class="col-sm-4">
															<input type="text" autocomplete="off" value="<?php echo $cal_row['pin_code'];?>" readonly class="form-control  pin_code" placeholder="Pin Code" name="zip_code">
														</div>
														<label class="col-sm-2 control-label">Remarks:</label>
					                        			<div class="col-sm-4 serial_class">
															<textarea class="form-control" name="remarks"></textarea>
														</div>
													</div>
				                        		</div> 
				                        		<div class="form-group">
					                        		<div class="col-md-12">
							                        	<div class="col-md-offset-10 col-md-2" style="padding-bottom:10px;">
															<a class="btn btn-primary tooltips add_document"><i class="fa fa-plus"></i> Add Document</i></a><br>
														</div>
														<div class="table-responsive col-md-offset-1 col-md-9 tab_hide">
															
															<table class="table hover document_table">
																<thead>
																	<tr>
																		<th  width="8%"><strong>Sno</strong></th>
																		<th width="40%" ><strong>Document Type</strong></th>
																		<th width="40%" ><strong>Supported Document</strong></th>
																		<th width="8%" ><strong>Delete</strong></th>
																	</tr>
																</thead>
																<tbody>
																<?php $count = 1;
																	if(count(@$attach_document)>0)
																	{	
																		foreach($attach_document as $doc)
																			{?>	
																				<tr class="attach">
																					<input type="hidden" name="doc_id" class="doc_id" value="<?php echo $doc['rcd_id'];?>">
																					<td align="center"><span class="sno"><?php echo $count++; ?></span></td>
																					<td align="left">
																						<?php 
																							foreach($documenttypeDetails as $docs)
																							{
																								if($docs['document_type_id']==@$doc['document_type_id'])
																								{
																									echo $docs['name'].' - ('.date('d-m-Y H:i:s',strtotime($doc['created_time'])).')'; 
																								}
																							}
																						?>
																					</td>
																					<td>
												                                		<a data-container="body" data-placement="top"  data-toggle="tooltip" title="Download" href="<?php echo get_asset_upload_url().$doc['name'];?>" class="btn blue btn-circle btn-xs" download><i class="fa fa-cloud-download"></i></a> <?php echo $doc['doc_name']; ?>
																					</td>

																					<td>
																					<?php
																					if($doc['status']==1)
																						{ ?>
																							<a class="btn link btn-danger btn-sm deactivate " onclick="return confirm('Are you sure you want to Deactivate?')" > <i class="fa fa-trash-o"></i></a>

																						<?php } 
																						else
																						{?>
																						<a class="btn link btn-info btn-sm activate" onclick="return confirm('Are you sure you want to Activate?')" > <i class="fa fa-check"></i></a>
																					<?php }
																					?>
																					</td>
																				</tr>
																			 <?php		
																			}
																	} ?>			
																	<tr class="doc_row">
																		<td align="center">
																			<span class="sno"><?php echo $count; ?></span>
																		</td>
																		<td>
																			<select name="document_type[1]" class="form-control doc_type" > 
																				<option value="">Select Document Type</option>
																				<?php 
																					foreach($documenttypeDetails as $doc)
																					{
																						echo '<option value="'.$doc['document_type_id'].'">'.$doc['name'].'</option>';
																					}
																				?>
																			</select>
																		</td>
																		<td>
									                                		<input type="file" id="document" name="support_document_1" class="document">
																		</td>
																		
																        <td><a <?php if(@$flg == 2){ ?> style='display:none'; <?php }?> class="btn btn-danger btn-sm remove_bank_row" > <i class="fa fa-trash-o"></i></a></td>
																	</tr>
																</tbody>
															</table><br>
														</div>
													</div>
						                    	</div>
						                    	<div class="form-group">
													<div class="col-sm-offset-5 col-sm-5">
														<button class="btn btn-primary"  onclick="return confirm('Are you sure you want to Submit?')" type="submit" name="submit_tool"><i class="fa fa-check"></i> Submit</button>
														<a class="btn btn-danger" href="<?php echo SITE_URL;?>open_repair_request"><i class="fa fa-times"></i> Cancel</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form> <?php
			}
			if(isset($displayResults)&&$displayResults==1)
	    	{  	    	
	    	?>
				<form class="form-horizontal" role="form" action="<?php echo SITE_URL.'open_repair_request';?>"  parsley-validate novalidate method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="block-flat">
								<div class="content">
									<div class="row">
										<div class="col-sm-12 form-group">
											<!-- <label class="col-sm-1 control-label">RRB No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="rcb_number" placeholder="RRB Number" value="<?php echo @$search_data['rcb_number'];?>"  class="form-control" maxlength="100">
											</div>
											<!-- <label class="col-sm-1 control-label">RR No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="rc_number" placeholder="RR Number" value="<?php echo @$search_data['rc_number'];?>"  class="form-control" maxlength="100">
											</div>
											<!-- <label class="col-sm-1 control-label">Asset No</label> -->
				                            <div class="col-sm-3">
												<input type="text" autocomplete="off" name="asset_number" placeholder="Asset Number" value="<?php echo @$search_data['asset_number'];?>"  class="form-control" maxlength="100">
											</div>
											<div class=" col-sm-3">							
												<button type="submit" name="searchtools" value="1" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
												<a href="<?php echo SITE_URL.'open_repair_request'; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
											</div>
										</div>
										<div class=" col-sm-12 form-group">
										<div class="col-sm-3">
										    <input type="text" autocomplete="off" name="serial_no" placeholder="Serial Number" value="<?php echo @$search_data['serial_no'];?>" class="form-control">
										</div>	
											<?php if($task_access==2 || $task_access==3) { ?>
											<!-- <label class="col-sm-1 control-label">Warehouse</label> -->
											<div class="col-sm-3">
												<select name="wh_id" class="select2 main_status supplier_id" > 
													<option value="">Warehouse</option>
													<?php
													foreach($warehouse as $row)
													{
														$selected = ($row['wh_id']==@$search_data['whr_id'])?'selected="selected"':'';
														echo '<option value="'.$row['wh_id'].'" '.$selected.'>'.$row['wh_code'].' - '.'('.$row['name'].')</option>';
													}?>
												</select>
											</div>
											<?php } ?>
											<?php if($task_access == 3 && @$_SESSION['header_country_id']==''){?>
											   <!-- <label class="col-sm-1 control-label">Country</label> -->
												<div class="col-sm-3">
													<select class="select2" name="country_id" >
														<option value="">- Country -</option>
														 <?php
														foreach($countryList as $country)
														{
															$selected = ($country['location_id']==$search_data['country_id'])?'selected="selected"':'';
															echo '<option value="'.$country['location_id'].'" '.$selected.'>'.$country['name'].'</option>';
														}
														?>
													</select>
												</div>
												<?php } ?>
										</div>
									</div>
									<div class="row">
										<div class="header"></div>
										<table class="table table-bordered" ></table>
									</div>
									<div class="header"></div>
									<div class="table-responsive">
										<table class="table table-bordered hover">
											<thead>
												<tr >
													<th class="text-center"><strong></strong></th>
													<th class="text-center"><strong>S.NO</strong></th>
													<th class="text-center"><strong>RRB Number</strong></th>
													<th class="text-center"><strong>RMA number</strong></th>
													<th class="text-center"><strong>Warehouse</strong></th>
													<th class="text-center"><strong>Exp Delivery Date</strong></th>
													<th class="text-center"><strong>Exp Return Date</strong></th>
													<th class="text-center"><strong>Country</strong></th>
													<th class="text-center"><strong>Actions</strong></th>
												</tr>
											</thead>
											<tbody>
												<?php
												if(count($calibration_results)>0)
												{
													foreach($calibration_results as $row)
													{
														?>
														<tr>
															<td class="text-center"><img src="<?php echo assets_url(); ?>images/plus.png" class="toggle-details" title='expand'></td>
															<td class="text-center"><?php echo $sn++;?></td>
															<td class="text-center"><?php echo $row['rcb_number'];?></td>
															<td class="text-center"><?php echo $row['rma_number'];?></td>
															<td class="text-center"><?php echo $row['wh_code'].'-'.'('.$row['name'].')';?></td>
															<td class="text-center"><?php if($row['expected_delivery_date']!=''){ echo date('d-m-Y',strtotime(@$row['expected_delivery_date']));} else {echo '';}?></td>
															<td class="text-center"><?php if($row['expected_return_date']!=''){ echo date('d-m-Y',strtotime(@$row['expected_return_date']));} else {echo '';}?></td>
															<td class="text-center"><?php echo get_country_location($row['country_id']);?>
														    </td>
															<td class="text-center">
																<a class="btn btn-default" style="padding:3px 3px;"  data-container="body" data-placement="top"  data-toggle="tooltip" title="View"  href="<?php echo SITE_URL.'view_open_repair_request/'.storm_encode($row['rc_order_id']);?>"><i class="fa fa-eye"></i></a>
																<?php
																if(@$row['status']==1)
																{
																	?>
																<a class="btn btn-default" style="padding:3px 3px;" data-container="body" data-placement="top"  data-toggle="tooltip" title="Update Repair Details" href="<?php echo SITE_URL.'edit_open_repair_request/'.storm_encode($row['rc_order_id']);?>"><i class="fa fa-pencil"></i></a>
																<a class="btn btn-danger" style="padding:3px 3px;" data-container="body" data-placement="top" onclick="return confirm('Are you sure you want to Cancel?')"  data-toggle="tooltip" title="Cancel RRB Request" href="<?php echo SITE_URL.'remove_repair_all_assets/'.storm_encode($row['rc_order_id']);?>"><i class="fa fa-trash-o"></i></a> <?php
																} ?>											
															</td>
														</tr> 
														<tr class="details">
	                                                        <?php
															if(count($row['asset_list'])>0)
															{?>    
																<td  colspan="9">
			                                                        <table class="table">
			                                                        	<thead>
			                                                        		<th class="text-center"><strong>RR Number</strong></th>
			                                                        		<th class="text-center"><strong>Tool Number</strong></th>
			                                                        		<th class="text-center"><strong>Tool Description</strong></th>
			                                                        		<th class="text-center"><strong>Asset Number</strong></th>
			                                                        		<th class="text-center"><strong>Serial Number</strong></th>
			                                                        		<th class="text-center"><strong>RR Status</strong></th>
			                                                        		<th class="text-center"><strong>Actions</strong></th>
			                                                        	</thead>
			                                                            <tbody>
			                                                       <?php foreach(@$row['asset_list'] as $value)
			                                                        { 
			                                                        ?>
			                                                            <tr class="asset_row">
			                                                                <td align="center"><?php echo $value['rc_number']; ?></td>
			                                                                <td align="center"><?php echo $value['part_number']; ?></td>
			                                                                <td align="center"><?php echo $value['part_description']; ?></td>
			                                                                <td align="center"><?php echo $value['asset_number']; ?></td>
			                                                                <td align="center"><?php echo $value['serial_number']; ?></td>
			                                                                <td align="center"><?php echo $value['current_stage']; ?></td>
			                                                                <?php
			                                                                if($value['current_stage_id']==23)
			                                                                {
			                                                                	?>
			                                                                	<td align="center"><a class="btn btn-default" data-modal="form-primary" style="padding:3px 3px;" href="<?php echo SITE_URL.'edit_admin_repair_request/'.storm_encode($value['rc_asset_id']);?>"><i class="fa fa-pencil"></i>
			                                                                	</a></td> <?php
			                                                                }
			                                                                else
			                                                                {?> 
			                                                            		<td align="center">NA </td> <?php
			                                                            	}?>
			                                                            </tr>
			                                                        <?php     
			                                                        } ?>

			                                                            </tbody>
			                                                        </table>
			                                                    </td> <?php
			                                                }
			                                                else
			                                                {
			                                                	?><td colspan="8" align="center"><span class="label label-primary">No Open Repair Records</span></td> <?php
			                                                }?>
	                                                    </tr> <?php
													}
												}
												else 
												{
												?>	<tr><td colspan="9" align="center"><span class="label label-primary">No Open Repair Records</span></td></tr>
										<?php 	} ?>
											</tbody>
										</table>
									</div>
									<div class="row">
					                	<div class="col-sm-12">
						                    <div class="pull-left">
						                        <div class="dataTables_info" role="status" aria-live="polite">
						                            <?php echo @$pagermessage; ?>
						                        </div>
						                    </div>
						                    <div class="pull-right">
						                        <div class="dataTables_paginate paging_bootstrap_full_number">
						                            <?php echo @$pagination_links; ?>
						                        </div>
						                    </div>
					                	</div> 
					                </div>
								</div>
							</div>
						</div>
					</div>
				</form> <?php
		    }?>
		</div>
	</div>
</div>
<?php $this->load->view('commons/main_footer',$nestedView); ?>