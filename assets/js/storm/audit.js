$('.audit_details').hide();
$(document).on('click',".toggle_audit_details",function () { 
  var row=$(this).closest('tr');
  var next=row.next();
  $('.audit_details').not(next).hide();
  $('.toggle_audit_details').not(this).attr('src',ASSET_URL+'images/plus.png');

  next.toggle();
  if (next.is(':hidden')) {
    $(this).attr('src',ASSET_URL+'images/plus.png');
    $(this).attr('title','Expand');
  } else {
    $(this).attr('src',ASSET_URL+'images/minus.png');
    $(this).attr('title','Collapse');
  }
});

$(document).on('click','.audit_btn',function(){
  $('.select3, .select4').select2();
  $('#audit_frm').trigger("reset");
  $('#auditResults').html('<h4>Search Audit Details Here</h4>');

  var sent_data = $(this).closest('.block_page').find('.sent_data').val();
  $('#sent_data').val(sent_data);
  $.ajax({
    type:"POST",
    url:SITE_URL+'get_audit_drop_down',
    data:{sent_data:sent_data},
    cache:false,
    success:function(html){
      var obj = jQuery.parseJSON(html);
      $('.column_name').html(obj['result1']);
      $('.select3').select2('destroy'); 
      $('.select3').select2();
      $('.trans_type').html(obj['result2']);
      $('.select4').select2('destroy'); 
      $('.select4').select2();
      $('.header_title').html(obj['result3']);
    }
  });
  var submit_button = 1; // don't depend on segment
  $('#submit_button').val(submit_button);
  searchAuditdetails();
});

$(document).on('click','#audit_search',function(){
  var submit_button = 1; // don't depend on segment
  $('#submit_button').val(submit_button);
  searchAuditdetails();
});

function searchAuditdetails()
{
  $("#form-primary").css("opacity",0.5);
  $("#loaderID").css("opacity",1);
  var data = $('#audit_frm').serialize();
  var current_offset = $('#current_offset').val();
  var url = SITE_URL+'ajax_audit_report';

  var submit_button = $('#submit_button').val();
  if(submit_button == 0 ) // don't depend on submit button
  {
    if(current_offset!='')
    {
      url += '/'+current_offset;
    }
    else
    {
      $('#current_offset').val('');
    }
  }
  else
  {
    $('#current_offset').val('');
  }
  $.ajax({
      type:"POST",
      url:url,
      data:data,
      cache:false,
      success:function(response){
        var obj = jQuery.parseJSON(response);
        $("#form-primary").css("opacity",1);
        $("#loaderID").css("opacity",0);
        $("#auditResults").html(obj['result']);
      }
  });
}

$(document).on('click','#reset_search',function(){
  $('.select3, .select4').select2();
  $('#audit_frm').trigger("reset");
  var sent_data = $('#sent_data').val();
  $.ajax({
    type:"POST",
    url:SITE_URL+'get_audit_drop_down',
    data:{sent_data:sent_data},
    cache:false,
    success:function(html){
      var obj = jQuery.parseJSON(html);
      $('.column_name').html(obj['result1']);
      $('.select3').select2('destroy'); 
      $('.select3').select2();

      $('.trans_type').html(obj['result2']);
      $('.select4').select2('destroy'); 
      $('.select4').select2();
      $('.header_title').html(obj['result3']);
    }
  });
  var submit_button = 1;
  $('#submit_button').val(submit_button);
  searchAuditdetails();
});

$(document).on('click','#auditPagination .pagination a',function(){
  var submit_button  = 0;
  $('#submit_button').val(submit_button);
  var offset = $(this).attr("href");
  if(offset == 0)
  {
    $('#current_offset').val('');
  }
  else
  { 
    var arr = offset.split('#');
    var second = arr[1];
    if(second.length == 0)
    {
      $('#current_offset').val('');
    }
    else
    {
      var second_arr = second.split('/');
      final_offset = second_arr[1];
      $('#current_offset').val(final_offset);
    }
  }
  searchAuditdetails();
});