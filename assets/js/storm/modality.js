function is_modalitynameExist()
{
  $("#modality_name").on('keyup keypress blur change',function () {
      $("#error").remove();
      var name = $(this).val();
      var modality_id = $("#modality_id").val();
      var data = 'name='+name+'&modality_id='+modality_id;
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_modalitynameExist',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#modality_name").after("<p class='color-danger' id='error'>This Modality already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
}
function is_modalitycodeExist()
{
  $("#modality_code").on('keyup keypress blur change',function () {
      $("#error1").remove();
      var name = $(this).val();
      var modality_id = $("#modality_id").val();
      var data = 'name='+name+'&modality_id='+modality_id;
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_modalitycodeExist',
        data:data,
        cache:false,
        success:function(response){
          $("#error1").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#modality_code").after("<p class='color-danger' id='error1'>This Modality Code already exists</p>");
          }
          else
          {
            $("#error1").remove();
          }
        }
      });
    });
}

$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
  if ($('#error1').length) {
    e.preventDefault();
    return false;
  }
  
});
