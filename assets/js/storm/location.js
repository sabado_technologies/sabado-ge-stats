function check_country()
{
  $("#name, .submit_country").on('keyup keypress blur change',function () {
      $(".error").remove();
      var name = $("#name").val();
      var location_id = $("#location_id").val();
      if(location_id=='')
      {
          location_id = 0;
      }
      var data = 'name='+name+'&location_id='+location_id;
      if(name!='')
      {
        $.ajax({
          type:"POST",
          url:SITE_URL+'check_country',
          data:data,
          cache:false,
          success:function(response){
            $(".error").remove();
            if(response>0)
            {
              $("#name").after("<p class='color-danger error'>This Country Name already exists</p>");
            }
            else
            {
              $(".error").remove();
            }
          }
        });
      }
    });
}
function check_zone()
{
  $("#name, #parent_id, .submit_zone").on('keyup keypress blur change',function () {
      $(".error").remove();
      var name = $('#name').val();
      var parent_id = $("#parent_id").val();
      var location_id = $("#location_id").val();
      var country_id = $('#c_id').val();
      if(location_id=='')
        {
            location_id=0;
        }
      var data = 'name='+name+'&parent_id='+parent_id+'&location_id='+location_id+'&country_id='+country_id;
      if(name!='')
      {
        $.ajax({
          type:"POST",
          url:SITE_URL+'check_zone',
          data:data,
          cache:false,
          success:function(response){
            $(".error").remove();
            if(response>0)
            {
              $("#name").after("<p class='color-danger error'>This Zone already exists</p>");
            }
            else
            {
              $(".error").remove();
            }
          }
        });
      }
    });
}

function check_short_name()
{
  $("#short_name, #parent_id,.submit_state").on('keyup keypress blur change',function () {
    
      $(".error1").remove();
      var short_name = $("#short_name").val();
      var parent_id = $("#parent_id").val();
      var location_id = $("#location_id").val();
      var country_id = $('#c_id').val();
      
      var data = 'short_name='+short_name+'&parent_id='+parent_id+'&location_id='+location_id+'&country_id='+country_id;
      if(short_name!='')
      {
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_shortNameExist',
        data:data,
        cache:false,
        success:function(response){
          $(".error1").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#short_name").after("<p class='color-danger error1'>This Short Name already exists</p>");
          }
          else
          {
            $(".error1").remove();
          }
        }
      });
    }
    });
}
function check_state()
{
  $("#name, #parent_id, .submit_state").on('keyup keypress blur change',function () {
      $(".error").remove();
      var name = $('#name').val();
      var parent_id = $("#parent_id").val();
      var location_id = $("#location_id").val();
      var country_id = $('#c_id').val();
      if(location_id=='')
        {
            location_id=0;
        }
      var data = 'name='+name+'&parent_id='+parent_id+'&location_id='+location_id+'&country_id='+country_id;
      if(name!='')
      {
      $.ajax({
        type:"POST",
        url:SITE_URL+'check_state',
        data:data,
        cache:false,
        success:function(response){
          $(".error").remove();
          if(response>0)
          {
            $("#name").after("<p class='color-danger error'>This State already exists</p>");
          }
          else
          {
            $(".error").remove();
          }
        }
      });
    }
    });
}

function check_city()
{
  $("#name, #parent_id, .submit_city").on('keyup keypress blur change',function () {
      //$(".error").remove();
      var name = $("#name").val();
      var parent_id = $("#parent_id").val();
      var location_id = $("#location_id").val();
      var country_id = $('#c_id').val();
      if(location_id=='')
        {
            location_id=0;
        }
      var data = 'name='+name+'&parent_id='+parent_id+'&location_id='+location_id+'&country_id='+country_id;
      if(name!='')
      {
      $.ajax({
        type:"POST",
        url:SITE_URL+'check_city',
        data:data,
        cache:false,
        success:function(response){
          $(".error").remove();
          if(response>0)
          {
            $("#name").after("<p class='color-danger error'>This City already exists</p>");
          }
          else
          {
            $(".error").remove();
          }
        }
      });
    }
    });
}

function check_area()
{
  $("#name, #parent_id,.submit_area").on('keyup keypress blur change',function () {
      $(".error").remove();
      var name = $("#name").val();
      var parent_id = $("#parent_id").val();
      var location_id = $("#location_id").val();
      var country_id = $('#c_id').val();
      if(location_id=='')
        {
            location_id=0;
        }
      var data = 'name='+name+'&parent_id='+parent_id+'&location_id='+location_id+'&country_id='+country_id;
      if(name!='')
      {
      $.ajax({
        type:"POST",
        url:SITE_URL+'check_area',
        data:data,
        cache:false,
        success:function(response){
          $(".error").remove();
          if(response>0)
          {
            $("#name").after("<p class='color-danger error'>This Area already exists</p>");
          }
          else
          {
            $(".error").remove();
          }
        }
      });
    }
    });
}

$("form").submit(function(e) {
  if ($('.error, .error1').length) {
    e.preventDefault();
    return false;
  }
});