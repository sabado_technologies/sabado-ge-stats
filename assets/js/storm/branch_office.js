
$("#branch_name").on('blur',function () {
  $("#error").remove();
  var name = $(this).val();
  var branch_id = $("#branch_id").val();
  var country_id =$('#country_id').val();
  if(branch_id=='')
    {
        branch_id=0;
    }
  var data = 'name='+name+'&branch_id='+branch_id+'&country_id='+country_id;
  $.ajax({
    type:"POST",
    url:SITE_URL+'is_officenameExist',
    data:data,
    cache:false,
    success:function(response){
      $("#error").remove();
      if(response>0)
      {
        $("#branch_name").after("<p class='color-danger' id='error'>This Office Name already exists</p>");
      }
      else
      {
        $("#error").remove();
      }
    }
  });
});

$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});