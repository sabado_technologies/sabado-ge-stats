function is_nameExist()
{
  $("#eq_model_name").on('keyup keypress blur change',function () {
      $("#error").remove();
      var name = $(this).val();
      var eq_model_id = $("#eq_model_id").val();
      var data = 'name='+name+'&eq_model_id='+eq_model_id;
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_eq_model_nameExist',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#eq_model_name").after("<p class='color-danger' id='error'>This Equipement model already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
}

$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});

