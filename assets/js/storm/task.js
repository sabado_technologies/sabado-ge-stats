$("#task_id").on('blur',function () {
      $("#error").remove();
      var name = $(this).val();
      var task_id = $("#task_id").val();
      var page_id = $("#page_id").val();
      var data = 'name='+name+'&task_id='+task_id+'&page_id='+page_id;

      $.ajax({
        type:"POST",
        url:SITE_URL+'is_tasknameExist',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            $("#task_id").after("<p class='color-danger' id='error'>This Task already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });

  $("form").submit(function( e ) {
    if ($('#error').length) {
      e.preventDefault();
      return false;
    }
    
  });