$(document).ready(function(){
	$("#role_name").on('blur',function () {
      $("#error").remove();
      var name = $(this).val();
      var role_id = $("#role_id").val();
      var data = 'name='+name+'&role_id='+role_id;
      
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_role_exist',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            $("#role_name").after("<p class='color-danger' id='error'>This Role name already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
});

$('.details').hide();
$(document).on('click',".toggle-details",function () 
{ 
  var row=$(this).closest('tr');
  var next=row.next();
  $('.details').not(next).hide();
  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
  next.toggle();
  if (next.is(':hidden')) {
    $(this).attr('src',ASSET_URL+'images/plus.png');
    $(this).attr('title','expand');
  } else {
    $(this).attr('src',ASSET_URL+'images/minus.png');
    $(this).attr('title','collapse');
  }
});

$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});