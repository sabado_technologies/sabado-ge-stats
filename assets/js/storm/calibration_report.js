
function isnumber(str)
{
  if (/^\d+$/.test(str.trim())) 
  {
      return parseInt(str);
  } 
  else 
  {
    return false;
  }
}

/*$('.calibration_status').on('change',function(){
  var calibration_status = isnumber($(this).val());
  if(calibration_status!='')
  {
    $.ajax({
      type:"POST",
      url:SITE_URL+'get_cr_status',
      data:{calibration_status:calibration_status},
      cache:false,
      success:function(data){
        var obj = jQuery.parseJSON(data);
        $(".current_stage").html(obj['result1']);
        $('.select4').select2('destroy'); 
        $('.select4').select2();
      }
    });
  }
  });*/

$('.zone_id').change(function(){
    var zone_id = isnumber($(this).val());
    if(zone_id!='')
    {
      var data = 'zone_id='+zone_id;
      $.ajax({
          type:"POST",
          url:SITE_URL+'get_wh_by_zone',
          data:data,
          cache:false,
          success:function(html){
            obj = jQuery.parseJSON(html);
            $('.wh_id').html(obj['result1']);
            $('.select3').select2('destroy'); 
            $('.select3').select2();
          }
      }); 
    }
    else
    {
      $('.wh_id').html('<option value="">- Warehouse -</option>');
      $('.select3').select2('destroy'); 
      $('.select3').select2();
    }
    
});