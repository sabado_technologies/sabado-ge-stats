
  $("#branch_name").on('blur',function () {
      $("#error").remove();
      var name = $(this).val();
      var branch_id = $("#branch_id").val();
      var country_id =$('#country_id').val();
      if(branch_id=='')
        {
            branch_id=0;
        }
      var data = 'name='+name+'&branch_id='+branch_id+'&country_id='+country_id;
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_branchnameExist',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            $("#branch_name").after("<p class='color-danger' id='error'>This Warehouse Name already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });

$("#wh_code").on('blur',function () {
      $("#error1").remove();
      var branch_code = $(this).val();
      var branch_id = $("#branch_id").val();
      var country_id =$('#country_id').val();
      if(branch_id=='')
        {
            branch_id=0;
        }
      var data = 'branch_code='+branch_code+'&branch_id='+branch_id+'&country_id='+country_id;
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_branchcodeExist',
        data:data,
        cache:false,
        success:function(response){
          $("#error1").remove();
          if(response>0)
          {
            $("#wh_code").after("<p class='color-danger' id='error1'>This Warehouse Code already exists</p>");
          }
          else
          {
            $("#error1").remove();
          }
        }
      });
    });




$("form").submit(function( e ) {
  if ($('#error, #error1').length) {
    e.preventDefault();
    return false;
  }
});
