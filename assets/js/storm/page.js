$(document).ready(function(){
	$("#page_name").on('blur',function () {
      $("#error").remove();
      var name = $(this).val();
      var page_id = $("#page_id").val();
      var data = 'name='+name+'&page_id='+page_id;
      
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_page_exist',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            $("#page_name").after("<p class='color-danger' id='error'>This Page name already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
});

$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});