function is_documenttypeExist()
{
  $("#document_type").on('keyup keypress blur change',function () {
      $("#error").remove();
      var name = $(this).val();
      var document_type_id = $("#doc_type_id").val();
      if(document_type_id=='')
        {
            document_type_id=0;
        }
      var data = 'name='+name+'&document_type_id='+document_type_id;
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_documenttypeExist',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            $("#document_type").after("<p class='color-danger' id='error'>This Document Type  Name already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
}
$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});
