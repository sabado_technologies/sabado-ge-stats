$(document).on('change','.icheck',function()
{
  var cur_wh_id = $(this).attr('data-wh-id');
  var cur_flag = $(this).attr('data-flag');
  if(cur_flag!=1)
  {
    alert('Asset Should be in Warehouse to generate Repair Process');
    $(this).prop('checked',false);
    return false;
  }

  var check_length = $('.icheck:checked').length;
  var has_error = false;
  if(check_length>1)
  {
    $('.icheck:checked').each(function()
    {
      var wh_id = $(this).attr('data-wh-id');
      if(wh_id!=cur_wh_id)
      {
        has_error = true;
        alert('Selected Assets must be in same Inventory !'); return false;
      }
    }); 
  }

  /*Modified By Srilekha*/
  /*var check_wh_length = $('.rc_wh_list').length;
  if(check_wh_length>0)
  {
    $('.rc_wh_list').each(function()
    {
      var wh_id = $(this).val();
      if(wh_id!=cur_wh_id)
      {
        has_error = true;
        alert('Selected Assets must be in same Inventory !'); return false;
      }
    });
  }*/

  if(has_error)
  {
    $(this).prop('checked',false);
    return false;

  }
  var checked_val = $(this).val();
  var warehouse=$(this).closest(".toolRow").find(".warehouse").val();
  
  var len=$('.icheck:checked').length;
  if(len > 0)
  {
    $("button[name='add']").prop("disabled",false);
  }
  else
  {
    $("button[name='add']").prop("disabled",true);
  }
});


$('.removeRow').click(function(){
  var rowCount = $('#mytable').find('tbody tr').length; 
  if(confirm('Are you sure you want to remove ?'))
  {
    if(rowCount == 1 )
    {
      alert('Sorry, Minimum one tool is required to generate Repair process !');
      return false;
    }
    var rc_asset_id = parseInt($(this).attr('data-cid')); 
    var data = 'rc_asset_id='+rc_asset_id;
    var toolRow = $(this).closest(".toolSelectedRow");
    //alert(component_id);
    $("#orderCartContainer").css("opacity",0.5);
    $("#loaderID").css("opacity",1);
    $.ajax(
    {
      type:"POST",
      url:SITE_URL+'removerepairToolFromCart',
      data:data,
      cache:false,
      success:function(html)
      {
        //alert(html);
        toolRow.remove();
        $("#orderCartContainer").css("opacity",1);
        $("#loaderID").css("opacity",0);
      }
    });
  }
});

$('.removeasset').click(function(){
  var rowCount = $('#mytable').find('tbody tr').length; 
  if(confirm('Are you sure you want to remove ?'))
  {
    if(rowCount == 1 )
    {
      alert('Sorry, Minimum one tool is required to proceed Repair process !');
      return false;
    }
    var rc_asset_id=parseInt($(this).attr('data-rc_asset_id'));
    var data = 'rc_asset_id='+rc_asset_id;
    var toolRow = $(this).closest(".asset_selected_row");
    $.ajax(
    {
      type:"POST",
      url:SITE_URL+'remove_repair_asset',
      data:data,
      cache:false,
      success:function(html)
      {
       toolRow.remove();
      }
    });
  }
});

counter = 2;
count = 1;
$('.add_document').on('click',function()
{
  if(count == 0)
  {
    $('.tab_hide').removeClass('hidden');
    count++;
  }
  else
  {
    var ele = $('.document_table').find('tbody tr:last'); 
    var sl_no = ele.find('td:first .sno').html();
    var doc_type = ele.find('.doc_type').val();
    var doc = ele.find('.document').val();

    ele.find(".error").remove();
    if(doc_type == '' || doc == '')
    {
      if(doc_type == '')
      {
      ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(doc == '')
      {
        ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
        
      }
      return false;
    }
    
    var ele_clone = ele.clone();
    ele_clone.find('.sno').html(parseInt(sl_no)+1);
    ele_clone.find('input, select').attr("disabled", false).val('');
    ele_clone.find('td:last').show();
    ele_clone.find(".remove_bank_row").show();
    ele.after(ele_clone);
    ele_clone.show();
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/\[1\]/g, '['+counter+']');
    });
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/support\_document\_1/g,'support\_document\_'+counter);
    });
    counter++;
  }
});

$(document).on('click','.remove_bank_row',function(){
  var ele = $(this).closest('.doc_row');
  var rowCount = $('.document_table').find('tbody tr').length;
  if(rowCount==1)
  {
    ele.find('.doc_type').val('');
    ele.find('.document').val('');
    ele.find('.error').remove();
    $('.tab_hide').addClass('hidden');
    count = 0;
  }
  else
  {
     $(this).closest('tr').remove();
  }
  var sl_no=1;
  $('.sno').each(function(){
      $(this).html(sl_no);
      sl_no++;
  }); 
});


$(document).on('blur','.doc_type',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(1) .error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});
$(document).on('blur','.document',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(2) .error');
  ele_error.remove();
});

$(document).on('click','.deactivate',function(){
  var doc_id = $(this).closest('tr').find('.doc_id').val();
  var ele = $(this).closest('tr').find('.deactivate');
  var ele_a = $(this);
  if(doc_id != '')
  {
    var data = 'doc_id='+doc_id;
    $.ajax({
    type:"POST",
    url:SITE_URL+'deactivate_repair_doc',
    data:data,
    cache:false,
    success:function(response){
      if(response>0)
      {
        ele_a.removeClass('btn-danger deactivate').addClass('btn-info activate');
        ele.find('i').removeClass('fa-trash-o').addClass('fa-check');
        alert('Record Has been deactivated');
      }
      else
      {
        alert('Something went Wrong! try Again.');
      }
    }
  });
  }
});

$(document).on('click','.activate',function(){
  var doc_id = $(this).closest('tr').find('.doc_id').val();
  var ele = $(this).closest('tr').find('.activate');
  var ele_a = $(this);
  if(doc_id != '')
  {
    var data = 'doc_id='+doc_id;
    $.ajax({
    type:"POST",
    url:SITE_URL+'activate_repair_doc',
    data:data,
    cache:false,
    success:function(response){
      if(response>0)
      {
        ele_a.removeClass('btn-info activate').addClass('btn-danger deactivate');
        ele.find('i').removeClass('fa-check').addClass('fa-trash-o');
        alert('Record Has been Activated');
      }
      else
      {
        alert('Something went Wrong! try Again.');
      }
    }
  });
  }
});
$('.supplier_id').on('blur change',function()
  {
    var supplier_id = $(this).val();
    $.ajax({
        type:"POST",
        url:SITE_URL+'get_supplier_details_by_id',
        data:{supplier_id:supplier_id},
        cache:false,
        success:function(html)
        {
          if(html!='null')
          {
            var arr = jQuery.parseJSON(html);
            $('.contact_person').val(arr['contact_person']);
            $('.contact_number').val(arr['contact_number']);
            $('.address1').val(arr['address1']);
            $('.address2').val(arr['address2']);
            $('.address3').val(arr['address3']);
            $('.address4').val(arr['address4']);
            $('.pin_code').val(arr['pin_code']);
            $('.gst_number').val(arr['gst_number']);
            $('.pan_number').val(arr['pan_number']);
            //$('#service_region').val(arr['service_region']);
            //$('#site_id').removeAttr("disabled"," ");
          }
          else
          {
            $('.contact_person').val('');
            $('.contact_number').val('');
            $('.address1').val('');
            $('.address2').val('');
            $('.address3').val('');
            $('.address4').val('');
            $('.pin_code').val('');
            $('.gst_number').val('');
            $('.pan_number').val('');
            //$('#service_region').val('');
          }
        }
      });

  });

$(document).on('click','.repair_status',function(){
  var repair_status = $(this).val();
  if(repair_status==1)
  {
    $('.repair_type').addClass('hidden');

  }
  else if(repair_status==2)
  {
    $('.repair_type').removeClass('hidden');
  }
});

$('.details').hide();
$(document).on('click',".toggle-details",function () { 
  var row=$(this).closest('tr');
  var next=row.next();
  $('.details').not(next).hide();
  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
  next.toggle();
  if (next.is(':hidden')) {
    $(this).attr('src',ASSET_URL+'images/plus.png');
    $(this).attr('title','expand');
  } else {
    $(this).attr('src',ASSET_URL+'images/minus.png');
    $(this).attr('title','collapse');
  }
});
$("#dateFrom").datepicker({
  //alert('koko');
  dateFormat: "dd-mm-yy",
  changeMonth: true,
  changeYear: true,
  minDate: 0,
  onSelect: function (date) {                  
    var date2 = $(this).datepicker('getDate');
    $('#dateTo').datepicker('option', 'minDate', date2);
  }
});
  $("#dateTo").datepicker({
    dateFormat: "dd-mm-yy",
    changeMonth: true,
    changeYear: true,
    onSelect: function (date) {
       
      var date2 = $(this).datepicker('getDate');
      $('#dateFrom').datepicker('option', 'maxDate', date2);
       
    }
});
$("form").submit(function( e ) {
var ele = $('.document_table').find('tbody tr:last'); 
var doc_type = ele.find('.doc_type').val();
var doc = ele.find('.document').val();
ele.find(".error").remove();
if(doc_type == '' && doc == ''){ }
  else
  {
    if(doc_type == '')
    {
    ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(doc == '')
    {
      ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
      
    }
    if(doc_type == "" || doc == "")
    {
      alert("please fill All Required Fields!");
      return false;
    }
    
  }
});