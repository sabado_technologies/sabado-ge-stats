/***TABLE COLLAPSE ON 12-Nov-2015***/
$('.details').hide();
$(document).on('click',".toggle-details",function () { 
  var row=$(this).closest('tr');
  var next=row.next();
  $('.details').not(next).hide();
  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
  next.toggle();
  if (next.is(':hidden')) {
    $(this).attr('src',ASSET_URL+'images/plus.png');
    $(this).attr('title','Expand');
  } else {
    $(this).attr('src',ASSET_URL+'images/minus.png');
    $(this).attr('title','Collapse');
  }
});

$(document).on('click','.asset_condition_id',function(){
  var asset_condition_id = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(asset_condition_id==1)
  {
    ele.find(".error").remove();
    ele.find('.textarea').val('');
  }
  else
  {
    var text_data = ele.find('.textarea').val();
    if(text_data == '')
    {
      ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
    }
  }
});

$(document).on('change','.textarea',function(){
  var text = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(text != '')
  {
    ele.find(".error").remove();
  }
  else
  {
    ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
  }

});


