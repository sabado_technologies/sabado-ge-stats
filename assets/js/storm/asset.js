function check_serial_availablity()
{
  $(document).on('blur','.main_serial',function(){
    var cur_serial_number = $(this).val();
    var cur_tool_id = $(this).attr('data-tool_id');
    var cur_tool_part_id = $(this).attr('data-tool_part_id');
    var cur_country_id = $(this).attr('data-country_id');
    var ele = $(this).closest(".serial_class");
    ele.find('.error').remove();
    if(cur_tool_part_id == '')
    {
      cur_tool_part_id = 0;
    }
    if(cur_serial_number!='' && cur_tool_id !='')
    {
      var data = 'serial_number='+cur_serial_number+'&tool_id='+cur_tool_id+'&tool_part_id='+cur_tool_part_id+'&country_id='+cur_country_id;
      $.ajax({
      type:"POST",
      url:SITE_URL+'check_serial_number_availability',
      data:data,
      cache:false,
      success:function(response)
      {
        if(response>0)
        {
          ele.find('.error').remove();
          ele.find('.serial_number').val('');
          ele.find('.serial_number').after("<p class='color-danger error'>Serial Number :"+cur_serial_number+" already Filled !</p>");
        }
        else
        {
          $(".error").remove();
        }
      }
      });
    }
  });

  $(document).on('blur','.serial_number',function () {

    var cur_serial_number = $(this).val();
    var cur_tool_id = $(this).attr('data-tool_id');
    var ele = $(this).closest(".serial_class");
    ele.find('.error').remove();
    if(cur_serial_number !="")
    {
      var error = 0;
      $('.serial_number').each(function()
      {
        var tool_id = $(this).attr('data-tool_id');
        var serial_number = $(this).val();
        if(cur_tool_id == tool_id){}
        else
        {
          if(serial_number == cur_serial_number)
          {
            error++;
          }
        }
      }); 
      if(error>0)
      {
        ele.find('.serial_number').val('');
        ele.find('.serial_number').after("<p class='color-danger error'>Serial Number :"+cur_serial_number+" already Filled !</p>");
      }
      else
      {
        $(".error").remove();
      }
    }
  });
}

function deactivate_attached_record()
{
  $(document).on('click','.deactivate',function(){
    var asset_doc_id = parseInt($(this).closest('tr').find('.asset_doc_id').val());
    var ele = $(this).closest('tr').find('.deactivate');
    var ele_a = $(this);
    if(asset_doc_id != '')
    {
      var data = 'asset_doc_id='+asset_doc_id;
      $.ajax({
      type:"POST",
      url:SITE_URL+'deactivate_attached_record',
      data:data,
      cache:false,
      success:function(response){
        if(response>0)
        {
          ele_a.removeClass('btn-danger deactivate').addClass('btn-info activate');
          ele.find('i').removeClass('fa-trash-o').addClass('fa-check');
          alert('Record Has been deactivated');
        }
        else
        {
          $("#error").remove();
        }
      }
    });
    }
  });
  $(document).on('click','.activate',function(){
    var asset_doc_id = parseInt($(this).closest('tr').find('.asset_doc_id').val());
    var ele = $(this).closest('tr').find('.activate');
    var ele_a = $(this);
    if(asset_doc_id != '')
    {
      var data = 'asset_doc_id='+asset_doc_id;
      $.ajax({
      type:"POST",
      url:SITE_URL+'activate_attached_record',
      data:data,
      cache:false,
      success:function(response){
        if(response>0)
        {
          ele_a.removeClass('btn-info activate').addClass('btn-danger deactivate');
          ele.find('i').removeClass('fa-check').addClass('fa-trash-o');
          alert('Record Has been Activated');
        }
        else
        {
          $("#error").remove();
        }
      }
    });
    }
  });
}

function add_component_new_row()
{
  counter = 2;
  $('#add_document').on('click',function()
  {
      $('.tab_hide').removeClass('hidden');
      var ele = $('.document_table').find('tbody tr:last'); 
      var ele_clone = ele.clone();
      var sl_no = ele.find('td:first .sno').html();
      var doc_type = ele.find('.doc_type').val();
      var doc = ele.find('.document').val();
      ele.find(".error").remove();
      if(doc_type == '' || doc == '')
      {
        if(doc_type == '')
        {
          ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(doc == '')
        {
          ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
        }
        return false;
      }
      ele_clone.find('.sno').html(parseInt(sl_no)+1);
      ele_clone.find('input, select').attr("disabled", false).val('');
      ele_clone.find('td:last').show();
      ele_clone.find(".remove_bank_row").show();
      ele.after(ele_clone);
      ele_clone.show();
      ele_clone.html(function(i, oldHTML) 
      {
        return oldHTML.replace(/\[1\]/g, '['+counter+']');
      });
      ele_clone.html(function(i, oldHTML) 
      {
        return oldHTML.replace(/support\_document\_1/g,'support\_document\_'+counter);
      });
      counter++;
  });

  $(document).on('click','.remove_bank_row',function(){
    var rowCount = $('.document_table').find('tbody tr').length;
    var ele = $('.document_table').find('tbody tr:last'); 
    if(rowCount==1)
    {
      ele.closest('.doc_row').find('.error').remove();
      $('.tab_hide').addClass('hidden');
      count = 0;
    }
    else
    {
       $(this).closest('tr').remove();
    }

    var sl_no=1;
    $('.sno').each(function(){
        $(this).html(sl_no);
        sl_no++;
    }); 
  });

  $(document).on('blur','.doc_type',function(){
    var value = $(this).val();
    var ele = $(this);
    var ele_error = $(this).closest('.doc_row').find('td:eq(1) .error');
    ele_error.remove();
    if(value == '')
    {
      ele.after("<p class='color-danger error'>This Value is Required</p>");
    }
    else
    {
      ele_error.remove();
    }
  });

  $(document).on('blur','.document',function(){
    var value = $(this).val();
    var ele = $(this);
    var ele_error = $(this).closest('.doc_row').find('td:eq(2) .error');
    ele_error.remove();
  });
}

$(document).on('change','#document',function(){                
    var ext = $(this).val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['jpg','png','pdf','gif']) == -1) {
        alert('invalid extension! allowed .pdf, .jpg, .png, .gif only');
        $(this).val('');
        //$('#doc').removeClass('hidden');
        return false;
    }
    /*else{
        $('#doc').addClass('hidden');
    }*/
    
});
$(document).on('change','.parent_asset_status_id',function(){
   var parent_asset_status_id = $(this).val();
   var ele = $(this).closest('.asset_parent').find('div .parent_part_remarks_div');
    if(parent_asset_status_id == 2 || parent_asset_status_id == 3 || parent_asset_status_id==4)
    {
        ele.removeClass('hidden');
    }
    else
    {
        ele.addClass('hidden');
    }
});

$(document).on('change','.main_status',function(){
   var main_status = $(this).val();
    if(main_status != 1)
    {
        $('.main_remark').removeClass('hidden');
    }
    else
    {
        $('.main_remark').addClass('hidden');
    }
});
 
$(document).on('click','.delete_asset',function(){
  var result = confirm('Are you sure you want to Remove?');
  if(result)
  {
    var ele = $(this);
    ele.closest('.sub_div').remove();
    var sl_no=1;
    $('.auto_inc').each(function(){
        $(this).html('<strong>'+sl_no+')</strong>');
        sl_no++;
    });
  }
});
$("form").submit(function( e ) {
  
  set_count = 0
  $('.serial_number').each(function(){
    var serial_number = $(this).val();
    if(serial_number=='')
    {
      set_count++;
    }
  });
  var main_status = $('.main_status').find('option:selected').val();
  if(main_status=='')
  {
    set_count++;
  }
  $('.parent_asset_status_id').each(function(){
    var status = $(this).find('option:selected').val();
    if(status=="")
    {
      set_count++;
    }
  });
  var ele = $('.document_table').find('tbody tr:last'); 
  var doc_type = ele.find('.doc_type').val();
  var doc = ele.find('.document').val();
  ele.find(".error").remove();
  if(doc_type == '' && doc == ''){ }
  else
  {
    if(doc_type == '')
    {
    ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(doc == '')
    {
      ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
      
    }
    if(doc_type == "" || doc == "")
    {
      alert("please fill All Required Fields!");
      return false;
    }
  }
  if(set_count>0)
  {
    alert('Please Fill All Serial Number/Status Fields!');
    return false;
  }
  if ($('#error, .error').length) {
    e.preventDefault();
    return false;
  }
});