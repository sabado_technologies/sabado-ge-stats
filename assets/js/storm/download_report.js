function isnumber(str)
{
  if (/^\d+$/.test(str.trim())) 
  {
      return parseInt(str);
  } 
  else 
  {
    return false;
  }
}

$(document).on('change','.country_id',function(){
  var country_id = isnumber($(this).val());
  if(country_id !='')
  {
    $.ajax({
      type:"POST",
      url:SITE_URL+'get_state_by_country_dropdown',
      data:{country_id:country_id},
      cache:false,
      success:function(data){
        var obj = jQuery.parseJSON(data);
        $(".state_id").html(obj['result1']);
        $('.select3').select2('destroy'); 
        $('.select3').select2();
        $(".wh_id").html(obj['result2']);
        $('.select4').select2('destroy'); 
        $('.select4').select2();
      }
    });
  }
  else
  {
    $('.state_id').html('<option value="">- State -</option>');
    $('.select3').select2('destroy'); 
    $('.select3').select2();
    $('.wh_id').html('<option value="">- Warehouse -</option>');
    $('.select4').select2('destroy'); 
    $('.select4').select2();
  }
  
  

});

$(document).on('change','.state_id',function(){
  var state_id = isnumber($(this).val());
  if(state_id !='')
  {
    $.ajax({
      type:"POST",
      url:SITE_URL+'wh_by_state_dropdown',
      data:{state_id:state_id},
      cache:false,
      success:function(html){
        var obj = jQuery.parseJSON(html);
        $(".wh_id").html(obj['result1']);
        $('.select4').select2('destroy'); 
        $('.select4').select2();
      }
    });
  }
  else
  {
    $('.wh_id').html('<option value="">- Warehouse -</option>');
    $('.select4').select2('destroy'); 
    $('.select4').select2();
  }
});

$(document).on('click','.type',function(){
  var type = $(this).val();
  if(type == 1)
  {
    $('.cancel_label').html('Cancelled Invoice :');
    $('.text_label').html('Invoice Number');
  }
  else if(type == 2)
  {
    $('.cancel_label').html('Cancelled DC :');
    $('.text_label').html('Delivery Challan');
  }
});