$('.icheck').change(function()
{
	var len=$('.icheck:checked').length;
	if(len > 0)
	{
		$("button[name='add']").prop("disabled",false);
	}
	else
	{
		$("button[name='add']").prop("disabled",true);
	}
});

$('.removeRow').click(function(){
  var rowCount = $('#mytable').find('tbody tr').length;	
	if(confirm('Are you sure you want to remove ?'))
	{
    if(rowCount == 1 )
    {
      alert('Sorry!, Minimum one tool is Required to Proceed !');
      return false;
    }
		var asset_id = parseInt($(this).attr('data-asset_id')); 
		$("#orderCartContainer").css("opacity",0.5);
		$("#loaderID").css("opacity",1);
		var data = 'asset_id='+asset_id;
    var toolRow = $(this).closest(".toolSelectedRow");
		$.ajax(
    {
			type:"POST",
			url:SITE_URL+'remove_buffer_asset_from_cart',
			data:data,
			cache:false,
			success:function(html)
      {
				toolRow.remove();
				$("#orderCartContainer").css("opacity",1);
				$("#loaderID").css("opacity",0);
			}
		});
	}
});

$("form").submit(function( e ) {
  if($('#error').length)
  {
    e.preventDefault();
    return false;
  }
});

