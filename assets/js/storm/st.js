$('.details').hide();
$(document).on('click',".toggle-details",function () { 
  var row=$(this).closest('tr');
  var next=row.next();
  $('.details').not(next).hide();
  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
   next.toggle();
    if (next.is(':hidden')) {
      $(this).attr('src',ASSET_URL+'images/plus.png');
      $(this).attr('title','Expand');
    } else {
      $(this).attr('src',ASSET_URL+'images/minus.png');
      $(this).attr('title','Collapse');
    }
});

$('.icheck1').change(function()
{
  if(this.checked)
  {
    //alert("checked");
    $(this).closest(".toolRow").find(".toolQuantity").prop("disabled",false);
    $(this).closest(".toolRow").find(".toolQuantity").val("1");
  }
  else
  {
    //alert("unchecked");
    $(this).closest(".toolRow").find(".toolQuantity").prop("disabled",true);
    $(this).closest(".toolRow").find(".toolQuantity").val("");
  }
});

$(document).on('change','.toolQuantity',function(){
  // checking total lifting quantity to perticular product
    var ordered_tool_id = $(this).data('od-qty');
    var t_qty = 0;
    var req_stock = $('.tool_qty'+ordered_tool_id).val();
    $('.t_post_qty'+ordered_tool_id).each(function(index){
        var post_qty = $(this).val();
        if(post_qty!='')
        {
            t_qty += parseFloat(post_qty);   
            if(t_qty > req_stock)
            {
                alert('Total Quantity should be less than Required Quantity');
                $(this).val(1);        
            }
        }
    });

});


$("form").submit(function( e ) {  
  if(confirm('Are you sure you want to Submit'))
  {
    var whole_qty = 0;
    var error = 0;
    $('.toolQuantity').each(function(index){
      var ordered_tool_id = $(this).data('od-qty');
      var t_qty = 0;
      var req_stock = $('.tool_qty'+ordered_tool_id).val();

      $('.t_post_qty'+ordered_tool_id).each(function(index){
          var post_qty = $(this).val();
          var wh_stock = $('.html_wh_qty'+ordered_tool_id).html();
          //alert(wh_stock+''+post_qty);
          if(post_qty>wh_stock)
          {
            error = 1;
            alert('Selected tool Quantity should be less than Available Qty In warehouse');
          }
          if(post_qty!='')
          {
              whole_qty++;
              t_qty += parseFloat(post_qty);   
              if(t_qty > req_stock)
              {              
                  error = 1;
                  alert('Selected tool Quantity should be less than Required Quantity');
                  $(this).val(1);                          
              }
             
          }
      });
    });
    if(error == 1)
    {
      return false;
    }

    if(whole_qty == 0)
    {
        alert('Please select enter atlease single qty to generate Stock transfer');
        return false;
    }
    $('.submit-cls').hide();
    $('.cancel-cls').hide();
  }
  else
  {
    return false;
  }
});