function is_gps_uid_exist()
{
  $("#gps_uid").on('keyup keypress blur',function () {

      $("#error").remove();
      var gps_uid = $(this).val();
      var gps_tool_id = $("#gps_tool_id").val();
      var country_id = $("#country_id").val();
      var data = 'uid_number='+gps_uid+'&gps_tool_id='+gps_tool_id+'&country_id='+country_id;
      $.ajax({
        type:"POST",
        url:SITE_URL+'is_gps_uid_exist',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#gps_uid").after("<p class='color-danger' id='error'>This UID Number already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
}

$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});