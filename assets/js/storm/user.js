$(document).ready(function(){
  var sso_id = $('#sso_id').val();
  var role_id = $('#r_id').val();
  var country_id = $('#country_id').val();
  select2Ajax('reporting_manager_id', 'get_reporting_manager_list', country_id, 3);
  select2Ajax('fe_position', 'get_fe_position_list', country_id, 3);
  if(sso_id != '' && role_id !='')
  {
    if(role_id == 5 || role_id == 6)
    {
      $('.location_row').removeClass('hidden');
    }
    else
    {
      $('.location_row').addClass('hidden');
    }

    if(role_id == 3)
    {
      $('.location_row1').removeClass('hidden');
    }
    else
    {
      $('.location_row1').addClass('hidden');
    }
  }
});

$("#sso_num").on('blur',function () {
  var sso_num = $(this).val();
  var sso_id = $("#sso_id").val();
  var data = 'sso_num='+sso_num+'&sso_id='+sso_id;
  
  $.ajax({
    type:"POST",
    url:SITE_URL+'is_user_sso_Exist',
    data:data,
    cache:false,
    success:function(response){
       $("#sso_num").next('#error').remove();
      if(response>0)
      {
        $('#sso_num').addClass('parsley-error');
        $("#sso_num").after("<p class='color-danger' id='error'>This SSO ID already exists</p>");
      }
      else
      {
        $('#sso_num').removeClass('parsley-error');
        $("#error").remove();
      }
    }
  });
});

function isnumber(str)
{
  if (/^\d+$/.test(str.trim())) 
  {
      return parseInt(str);
  } 
  else 
  {
    return false;
  }
}

$(document).on('change','.role_id',function(){
  $('.reporting_manager_id').next('#error').remove();
  var role_id = isnumber($(this).val());
  var country_id = isnumber($('#country_id').val());

  if(role_id == '')
  {
    $('#designation_id').html('<option val="">- Designation -</option>');
    $('.rm').html('Reporting Manager');
    $('#location, #location1').html('<option value="0">- Location -</option>');
    $('.location_row, .location_row1').addClass('hidden');
    $('.modality, .warehouse').addClass('hidden');

    //empty selected data
    $('.location_id, .loc_zone').val('');
    $('.checkbox_modality, .checkbox_wh').prop('checked', false);
    $(".a").html('');
  }
  else
  {
    //empty selected data
    $('.location_id, .loc_zone').val('');
    $('.checkbox_modality, .checkbox_wh').prop('checked', false);
    $(".a").html('');
    if(role_id == 5 || role_id == 6)
    {
      $('.location_row').removeClass('hidden');
    }
    else
    {
      $('.location_row').addClass('hidden');
    }

    if(role_id == 3)
    {
      $('.location_row1').removeClass('hidden');
    }
    else
    {
      $('.location_row1').addClass('hidden');
    }

    if(role_id == 2 || role_id==5 || role_id==6)
    {
      $('.rm').html('Reporting Manager <span class="req-fld">*</span>');
    }
    else
    {
      $('.rm').html('Reporting Manager');
    }
    
    $.ajax({
      type:"POST",
      url:SITE_URL+'get_designation_by_role',
      data:{role_id:role_id},
      cache:false,
      success:function(html){
        var obj = jQuery.parseJSON(html);
        $("#designation_id").html(obj['result']);
      }
    });
  
    if(role_id == 5 || role_id ==6)
    {
      if(role_id == 5)
      {
         $('.llabel').html('Zone <span class="req-fld">*</span>');
         var level_id = 3;
      }
      if(role_id == 6)
      {
        $('.llabel').html('Country <span class="req-fld">*</span>');
        var level_id = 2;
      }
      $('.modality').removeClass('hidden');
      if(level_id!='' && country_id!='')
      {
        $.ajax({
          type:"POST",
          url:SITE_URL+'get_location_by_role',
          data:{role_id:level_id,country_id:country_id},
          cache:false,
          success:function(html){
            var obj = jQuery.parseJSON(html);
            $("#location").html(obj['result']);
            $('.location_row').removeClass('hidden');
          }
        });
      }
    }
    else if(role_id == 3)
    {
      $('.log_zones').html('Warehouse Zone');
      $.ajax({
        type:"POST",
        url:SITE_URL+'get_location_by_role',
        data:{role_id:3,country_id:country_id},
        cache:false,
        success:function(html){
          console.log(html);
          var obj = jQuery.parseJSON(html);
          $("#location1").html(obj['result']);
          $('.location_row1').removeClass('hidden');
        }
      });
    }
    else
    {
      $('#location, #location1').html('<option value="0">- Location -</option>');
      $('.location_row, .location_row1').addClass('hidden');
      $('.modality, .warehouse').addClass('hidden');
    }
  }
});

$('.upload_user').on('click',function(){
  $("#orderCartContainer").css("opacity",0.5);
  $("#loaderID").css("opacity",1);
});

$(document).on('change','.reporting_manager_id',function(){
  var role_id = $('#role_id').val();
  var value = $(this).val();
  var ele = $(this);
  ele.next('#error').remove();
  if(role_id==2 || role_id== 5 || role_id== 6)
  {
    if(value == '')
    {
      ele.after("<p class='color-danger' id='error'>This Value is Required</p>");
    }
    else
    {
      ele.next('#error').remove();
    }
  }
});

$(document).on('change','.location_id',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.loc_div').find('.error');
  ele_error.remove();
  if(value == '' || value==0)
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('change','.loc_zone',function()
{
  var location_id = isnumber($(this).val());
  if(location_id == '')
  {
    $('.warehouse').addClass('hidden');
  }
  else
  {
    $('.warehouse').removeClass('hidden');
    $.ajax({
      type:"POST",
      url:SITE_URL+'get_wh_by_zone_user',
      data:{location_id:location_id},
      cache:false,
      success:function(html){
        var obj = jQuery.parseJSON(html);
        //alert(obj);
        console.log(obj);
        $(".a").html(obj['result']);
      }
    });
  }
});

$(document).on("change","#select_all_wh",function(){  //"select all" change 
  $(".checkbox_wh").prop('checked', $(this).prop("checked")); 
});

$(document).on('change','.checkbox_wh',function(){ 
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked")){ 
        $("#select_all_wh").prop('checked', false); 
    }
    //check "select all" if all checkbox items are checked
    if ($('.checkbox_wh:checked').length == $('.checkbox_wh').length ){
        $("#select_all_wh").prop('checked', true);
    }
});

$(document).on('change','.reporting_manager_id',function(){
  var rm_id = $(this).val();
  var role_id = $('.role_id').val();
  $('.rm_error').html('');
  if(role_id==2 || role_id== 5 || role_id== 6)
  {
    if(rm_id == '')
    {
      $('.rm_error').html('This Value is Required.');
      return false;
    }
  }
});

$("form").submit(function( e ) {
  var role_id = $('.role_id').val();
  var rm_id = $('.reporting_manager_id').find('option:selected').val();
  var location_id = $('.location_id').val();
  $('.loc_div').find('.error').remove();
  $('.rm_error').html('');

  if(role_id== 5 || role_id== 6)
  {
    if(location_id == '' || location_id == 0)
    {
      $('.location_id').after("<p class='color-danger error'>This Value is Required</p>");
      return false;
    }
  }
  if(role_id==2 || role_id== 5 || role_id== 6)
  {
    if(rm_id == '')
    {
      $('.rm_error').html('This Value is Required.');
      return false;
    }
  }

  if(role_id == 3)
  {
    var wh_id = $('.wh_id').find('option:selected').val();
    var loc_zone = $('.loc_zone').find('option:selected').val();
    var matched = 0;
    var check_length = $('.checkbox_wh').length;
    if(check_length>0 && wh_id!='' && loc_zone!='')
    {
      $('.checkbox_wh').each(function(){
        var value = $(this).val();
        if(value == wh_id)
        {
          matched++;
        }
      });
      if(matched == 0)
      {
        alert('Please select Warehouse of same Zone!');
        return false;
      }
    }
    
  }
  if ($('#error,.error').length) {
    e.preventDefault();
    return false;
  }
});