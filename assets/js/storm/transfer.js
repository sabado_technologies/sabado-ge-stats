$(document).ready(function(){
  error = 0;
  $('.removeRow').each(function(){
    var flag = parseInt($(this).attr('data-flag'));
    if(flag!=1 && flag!=3)
    {
      error++;
    }
  });
  if(error>0)
  {
    $('.add_more_btn').hide();
  }
  else
  {
    $('.add_more_btn').show();
  }
});

$('.icheck').change(function()
{
	if(this.checked)
	{
		//alert("checked");
		$(this).closest(".toolRow").find(".toolQuantity").prop("disabled",false);
		//$(this).closest(".toolRow").find(".toolQuantity").val("1");
	}
	else
	{
		//alert("unchecked");
		$(this).closest(".toolRow").find(".toolQuantity").prop("disabled",true);
		//$(this).closest(".toolRow").find(".toolQuantity").val("");
	}
	var len=$('.icheck:checked').length;
	if(len > 0)
	{
		$("button[name='add']").prop("disabled",false);
	}
	else
	{
		$("button[name='add']").prop("disabled",true);
	}
});

$("#deploy_date").datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
          changeYear: true,
            minDate: 0,
            onSelect: function (date) {               
                var date = $(this).datepicker('getDate');
                //$('#dateTo').datepicker('option', 'minDate', date2);
            }
        });

$('.removeRow').click(function(){
  var rowCount = $('#mytable').find('tbody tr').length;	
	if(confirm('Are you sure you want to remove'))
	{
    if(rowCount == 1 )
    {
      alert('Sorry!, To Proceed an order should have minimum one tool');
      return false;
    }
		var asset_id = parseInt($(this).attr('data-cid'));
    var tool_order_id = $(this).attr('data-orderid');
    var ordered_tool_id = parseInt($(this).attr('data-orderedtool'));
    var current_stage_id = parseInt($(this).attr('data-currentstage')); 
    var ordered_asset_id = parseInt($(this).attr('data-orderedasset')); 
    var asset_status_id = parseInt($(this).attr('data-assetstatus')); 
		var toolRow = $(this).closest(".toolSelectedRow");
    
		$("#orderCartContainer").css("opacity",0.5);
		$("#loaderID").css("opacity",1);
		var data = 'asset_id='+asset_id+'&tool_order_id='+tool_order_id+'&ordered_tool_id='+ordered_tool_id+'&current_stage_id='+current_stage_id+'&ordered_asset_id='+ordered_asset_id+'&asset_status_id='+asset_status_id;

		$.ajax({
		type:"POST",
		url:SITE_URL+'removeToolFromIssueCartTransfer',
		data:data,
		cache:false,
		success:function(html)
    {
      /*display btn*/
      error = 0;
      $('.removeRow').each(function(){
        var flag = parseInt($(this).attr('data-flag'));
        var each_asset_id = parseInt($(this).attr('data-cid'));
        if(flag!=1 && flag!=3 && asset_id !=each_asset_id)
        {
          error++;
        }
      });
     
			toolRow.remove();
			$("#orderCartContainer").css("opacity",1);
			$("#loaderID").css("opacity",0);
      if(error>0)
      {
        $('.add_more_btn').hide();
      }
      else
      {
        $('.add_more_btn').show();
      }
		}
		});
	}
});


counter = 2;
count = 1;
$('.add_document').on('click',function()
{
  if(count == 0)
  {
    $('.tab_hide').removeClass('hidden');
    count++;
  }
  else
  {
    var ele = $('.document_table').find('tbody tr:last'); 
    var sl_no = ele.find('td:first .sno').html();
    var doc_type = ele.find('.doc_type').val();
    var doc = ele.find('.document').val();

    ele.find(".error").remove();
    if(doc_type == '' || doc == '')
    {
      if(doc_type == '')
      {
      ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(doc == '')
      {
        ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
        
      }
      return false;
    }
    
    var ele_clone = ele.clone();
    ele_clone.find('.sno').html(parseInt(sl_no)+1);
    ele_clone.find('input, select').attr("disabled", false).val('');
    ele_clone.find('td:last').show();
    ele_clone.find(".remove_bank_row").show();
    ele.after(ele_clone);
    ele_clone.show();
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/\[1\]/g, '['+counter+']');
    });
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/support\_document\_1/g,'support\_document\_'+counter);
    });
    counter++;
  }
});

$(document).on('click','.remove_bank_row',function(){
  var ele = $(this).closest('.doc_row');
  var rowCount = $('.document_table').find('tbody tr').length;
  if(rowCount==1)
  {
    ele.find('.doc_type').val('');
    ele.find('.document').val('');
    ele.find('.error').remove();
    $('.tab_hide').addClass('hidden');
    count = 0;
  }
  else
  {
     $(this).closest('tr').remove();
  }
  var sl_no=1;
  $('.sno').each(function(){
      $(this).html(sl_no);
      sl_no++;
  }); 
});

$(document).on('blur','.doc_type',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(1) .error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.document',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(2) .error');
  ele_error.remove();
});

 
  $(document).on('click','.deactivate',function(){
    var asset_doc_id = parseInt($(this).closest('tr').find('.asset_doc_id').val());
    var ele = $(this).closest('tr').find('.deactivate');
    var ele_a = $(this);
    if(asset_doc_id != '')
    {
      var data = 'asset_doc_id='+asset_doc_id;
      $.ajax({
      type:"POST",
      url:SITE_URL+'deactivate_attached_order_rec',
      data:data,
      cache:false,
      success:function(response){
        if(response>0)
        {
          ele_a.removeClass('btn-danger deactivate').addClass('btn-info activate');
          ele.find('i').removeClass('fa-trash-o').addClass('fa-check');
          alert('Record Has been deactivated');
        }
        else
        {
          $("#error").remove();
        }
      }
    });
    }
  });
  $(document).on('click','.activate',function(){
    var asset_doc_id = parseInt($(this).closest('tr').find('.asset_doc_id').val());
    var ele = $(this).closest('tr').find('.activate');
    var ele_a = $(this);
    if(asset_doc_id != '')
    {
      var data = 'asset_doc_id='+asset_doc_id;
      $.ajax({
      type:"POST",
      url:SITE_URL+'activate_attached_order_rec',
      data:data,
      cache:false,
      success:function(response){
        if(response>0)
        {
          ele_a.removeClass('btn-info activate').addClass('btn-danger deactivate');
          ele.find('i').removeClass('fa-check').addClass('fa-trash-o');
          alert('Record Has been Activated');
        }
        else
        {
          $("#error").remove();
        }
      }
    });
    }
  });

  function get_warehouse_address()
  {  //alert('llll');
      var warehouse_id = $('.wh_id_cls').val();
      //alert(warehouse_id);
      var data = 'warehouse_id='+warehouse_id;      
      $.ajax({
          type:"POST",
          url:SITE_URL+'getWarehouseAddress',
          data:data,
          cache:false,          
                success:function(html)
                {                 
                  if(html!='null')
                  {
                    var arr = jQuery.parseJSON(html);                    
                    $('#wh_address_1').val(arr['address1']);
                    $('#wh_address_2').val(arr['address2']);
                    $('#wh_address_3').val(arr['address3']);
                    $('#wh_address_4').val(arr['address4']);                    
                    $('#wh_pin_code').val(arr['zip_code']);
                    $('#gst_number').attr("data-gst",arr['gst_number']); //setter
                    $('#pan_number').attr("data-pan",arr['pan_number']); //setter
                    
                  }                                              
                }        
          
        });

      $(document).on('change','.wh_id_cls',function(){
          var warehouse_id = $('.wh_id_cls').val();
      //alert(warehouse_id);
      var data = 'warehouse_id='+warehouse_id;      
      $.ajax({
          type:"POST",
          url:SITE_URL+'getWarehouseAddress',
          data:data,
          cache:false,          
                success:function(html)
                {                 
                  if(html!='null')
                  {
                    var arr = jQuery.parseJSON(html);                    
                    $('#wh_address_1').val(arr['address1']);
                    $('#wh_address_2').val(arr['address2']);
                    $('#wh_address_3').val(arr['address3']);
                    $('#wh_address_4').val(arr['address4']);                    
                    $('#wh_pin_code').val(arr['zip_code']);
                    $('#gst_number').val(arr['gst_number']);
                    $('#pan_number').val(arr['pan_number']);
                    $('#gst_number').attr("data-gst",arr['gst_number']); //setter
                    $('#pan_number').attr("data-pan",arr['pan_number']); //setter
                  }                                              
                }        
          
        });
      });

  }
function get_warehouses_dropdown_by_sso()
{
  $(document).on('change','.sso_id_cls',function(){

    var sso_id = $('.sso_id_cls').val();
    //var role_id = $(this).find('option:selected').data('role-id');
    //alert(role_id);

    if(sso_id!=''){
        $.ajax({
            type: "POST",
            url: SITE_URL+"htmlWarehouseDropdown",
            data:'sso_id='+sso_id,
            success: function(data){  
             $('#gst_number').val(" ");
            $('#pan_number').val(" ");    
            $(".wh_id_cls").html(data);        

        }
        });
    }


  });


}

$("form").submit(function( e ) {
  if ($('#error').length) 
  {
    e.preventDefault();
    return false;
  }


  
  //return false;
});

