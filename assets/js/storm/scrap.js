counter = 2;
count = 1;
$('.add_document').on('click',function()
{
  if(count == 0)
  {
    $('.tab_hide').removeClass('hidden');
    count++;
  }
  else
  {
    var ele = $('.document_table').find('tbody tr:last'); 
    var sl_no = ele.find('td:first .sno').html();
    var doc_type = ele.find('.doc_type').val();
    var doc = ele.find('.document').val();

    ele.find(".error").remove();
    if(doc_type == '' || doc == '')
    {
      if(doc_type == '')
      {
      ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(doc == '')
      {
        ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
        
      }
      return false;
    }
    
    var ele_clone = ele.clone();
    ele_clone.find('.sno').html(parseInt(sl_no)+1);
    ele_clone.find('input, select').attr("disabled", false).val('');
    ele_clone.find('td:last').show();
    ele_clone.find(".remove_bank_row").show();
    ele.after(ele_clone);
    ele_clone.show();
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/\[1\]/g, '['+counter+']');
    });
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/support\_document\_1/g,'support\_document\_'+counter);
    });
    counter++;
  }
});
$(document).on('click','.remove_bank_row',function(){
  var ele = $(this).closest('.doc_row');
  var rowCount = $('.document_table').find('tbody tr').length;
  if(rowCount==1)
  {
    ele.find('.doc_type').val('');
    ele.find('.document').val('');
    ele.find('.error').remove();
    $('.tab_hide').addClass('hidden');
    count = 0;
  }
  else
  {
     $(this).closest('tr').remove();
  }
  var sl_no=1;
  $('.sno').each(function(){
      $(this).html(sl_no);
      sl_no++;
  }); 
});
$(document).on('blur','.doc_type',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(1) .error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});
$(document).on('blur','.document',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(2) .error');
  ele_error.remove();
});
$("form").submit(function( e ) {
var ele = $('.document_table').find('tbody tr:last'); 
var doc_type = ele.find('.doc_type').val();
var doc = ele.find('.document').val();
ele.find(".error").remove();
if(doc_type == '' && doc == ''){ }
  else
  {
    if(doc_type == '')
    {
    ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(doc == '')
    {
      ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
      
    }
    if(doc_type == "" || doc == "")
    {
      alert("please fill All Required Fields!");
      return false;
    }
    
  }
});
$(document).on('change','#document',function(){                
    var ext = $(this).val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['jpg','png','pdf','gif']) == -1) {
        alert('invalid extension! allowed .pdf, .jpg, .png, .gif only');
        $(this).val('');
        return false;
    }
});