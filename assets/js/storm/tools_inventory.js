function isnumber(str)
{
  if (/^\d+$/.test(str.trim())) 
  {
      return parseInt(str);
  } 
  else 
  {
    return false;
  }
}

$('.asset_type').change(function(){
    var asset_type = isnumber($(this).val());
    if(asset_type!='')
    {
        $.ajax({
            type:"POST",
            url:SITE_URL+'get_asset_status_by_type',
            data:{asset_type:asset_type},
            cache:false,
            success:function(html){
                obj = jQuery.parseJSON(html);
                $('.asset_status').html(obj['result1']);
                $('.select3').select2('destroy'); 
                $('.select3').select2();
            }
        });
    }
    else
    {
        $('.asset_status').html('<option value="">- Asset Status -</option>');
        $('.select3').select2('destroy'); 
        $('.select3').select2();
    }
});
$('.zone_id').change(function(){
    var zone_id = isnumber($(this).val());
    if(zone_id!='')
    {
        var data = 'zone_id='+zone_id;
        $.ajax({
            type:"POST",
            url:SITE_URL+'get_wh_by_zone',
            data:data,
            cache:false,
            success:function(html){
                obj = jQuery.parseJSON(html);
                $('.wh_id').html(obj['result1']);
                $('.select4').select2('destroy'); 
                $('.select4').select2();
            }
        });
    }
    else
    {
        $('.wh_id').html('<option value="">- Inventory -</option>');
        $('.select4').select2('destroy'); 
        $('.select4').select2();
    }
    
});

$('.details').hide();
$(document).on('click',".toggle-details",function () { 
    var row=$(this).closest('tr');
    var next=row.next();
    $('.details').not(next).hide();
    $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
    next.toggle();
    if (next.is(':hidden')) {
        $(this).attr('src',ASSET_URL+'images/plus.png');
    } else {
        $(this).attr('src',ASSET_URL+'images/minus.png');
    }
});