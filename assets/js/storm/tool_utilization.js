$("#from_date").datepicker({
      
          dateFormat: "dd-mm-yy",
          changeMonth: true,
          changeYear: true,
          //minDate: 0,
          onSelect: function (date) {                  
              var date2 = $(this).datepicker('getDate');
              $('#to_date').datepicker('option', 'minDate', date2);

          }
      });
  $("#to_date").datepicker({
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true,
        maxDate:0,
        onSelect: function (date) {
            var date2 = $(this).datepicker('getDate');
            $('#from_date').datepicker('option', 'maxDate', date2);
        }
});