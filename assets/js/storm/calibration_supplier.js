function check_supplier_name_availability()
{
  $("#cal_supplier_name").on('keyup keypress blur change',function () {
      $("#error").remove();
      var name = $(this).val();
      var cal_supplier_id = $("#cal_supplier_id").val();
      var cal_sup_type_id = $(".cal_sup_type_id").val();
      var data = 'name='+name+'&cal_supplier_id='+cal_supplier_id+'&cal_sup_type_id='+cal_sup_type_id;
      
      $.ajax({
        type:"POST",
        url:SITE_URL+'check_supplier_name_availability',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#cal_supplier_name").after("<p class='color-danger' id='error'>This Calibration Supplier name already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
}
$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});