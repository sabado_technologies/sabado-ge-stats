/***TABLE COLLAPSE ON 12-Nov-2015***/
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});

$('.details').hide();
$(document).on('click',".toggle-details",function () { 
  var row=$(this).closest('tr');
  var next=row.next();
  $('.details').not(next).hide();
  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
  next.toggle();
  if (next.is(':hidden')) {
    $(this).attr('src',ASSET_URL+'images/plus.png');
  } else {
    $(this).attr('src',ASSET_URL+'images/minus.png');
  }
});

$(document).on('click','.asset_condition_id',function(){
  var asset_condition_id = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(asset_condition_id==1)
  {
    ele.find(".error").remove();
    ele.find('.textarea').val('');
  }
  else
  {
    var text_data = ele.find('.textarea').val();
    if(text_data == '')
    {
      ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
    }
  }
});

$(document).on('change','.textarea',function(){
  var text = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(text != '')
  {
    ele.find(".error").remove();
  }
  else
  {
    ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
  }

});

$(document).on('click','.save_point',function(){
  var hidden_asset_id = $('#hidden_asset_id').val();
  var hidden_asset_number = $('#hidden_asset_number').val();
  var table = $("#show" + hidden_asset_id).find('.tool_list');
  var count = 0;
  table.find('tbody tr').each(function() 
  {
    var val = $('td',this).find('.asset_condition_id:checked').val();
    $('td',this).find('.error').remove();
    if(val !=1)
    {

    var data = $('td',this).find('.textarea').val();
      if(data == '')
      {
      $('td',this).find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
      count++;
      }
    }
  });
  if(count > 0)
  {
    alert("Please fill All Required Fields!");
    return false;
  }
  var result = confirm('Are you sure, that you checked all items for asset : '+hidden_asset_number+'?');
  if(result)
  {
    $('.scan_table tbody tr').find('td:eq(5) .as2').removeAttr("disabled");
    var main = $('.scan_table').find('#main'+hidden_asset_id);
    main.find('td:eq(0)').html('').html("<a title='Scanned'><i class='fa fa-check-square-o fa-2x scan_success' style='color:#00b061'></i></a>");
    main.find('td:eq(5) .as2').prop('disabled', true);
    main.find('td:eq(5) .as2').removeClass('as2');
    $('.scan_qr').removeAttr("disabled", "disabled");
    var asset_status = 0;
    $('.scan_table').find('tbody tr').each(function() 
    {
      var val = $('td',this).find('.asset_status:checked').val();
      if(val ==2)
      {
        asset_status++;
      }
    });

    var scanned_asset = 0;
    $('.scan_success').each(function(){
        scanned_asset++;
    });

    var total =  parseInt(asset_status) + parseInt(scanned_asset);
    var asset_total = $('.asset_total').val();
    if(total == asset_total)
    {
      $('.ack_submit_btn').removeClass('hidden');
    }
    $('#show'+hidden_asset_id).hide();
  }
});

$('.asset_status').on('click',function(){
  var asset_selected = $(this).val();
  var ele = $(this).closest('tr').find('.scan_qr');
  if(asset_selected == 2)
  {
    ele.attr('disabled','disabled');
    var asset_status = 0;
    $('.scan_table').find('tbody tr').each(function() 
    {
      var val = $('td',this).find('.asset_status:checked').val();
      if(val ==2)
      {
        asset_status++;
      }
    });

    var scanned_asset = 0;
    $('.scan_success').each(function(){
        scanned_asset++;
    });

    var total =  parseInt(asset_status) + parseInt(scanned_asset);
    var asset_total = $('.asset_total').val();
    if(total == asset_total)
    {
      $('.ack_submit_btn').removeClass('hidden');
    }
  }
  else
  {
    ele.removeAttr('disabled','disabled');
    $('.ack_submit_btn').addClass('hidden');
  }
});

$('.scan_qr').on('click',function(){
  $('#hidden_asset_id').val('');
  $('#hidden_asset_number').val('');
  $('.asset_display').html('');
  $('.checkbox_val').val('0');
  $('.checkbox_val').prop('checked', false); // Unchecks it
  $('.submit_action').addClass('hidden');

  var asset_id = $(this).data('asset-id');
  var asset_number = $(this).data('asset-number');
  $('#hidden_asset_id').val(asset_id);
  $('#hidden_asset_number').val(asset_number);
  $('.asset_display').html("<p><b style='margin-left:16px;'>"+asset_number+"</b></p>");

});
$('#form-primary').on("shown.bs.modal", function() {
    $('#asset_number').val('').focus();
});

$(document).on('click','.checkbox_val',function(){
  var value = $(this).val();
  if(value == 0)
  {
    $('.checkbox_val').val('1');
    $('.submit_action').removeClass('hidden');
    $('#asset_number').val('').focus();
  }
  else
  {
    $('.checkbox_val').val('0');
    $('.submit_action').addClass('hidden');
    $('#asset_number').val('').focus();
  }
});


$('#asset_number').on('input',function(e)
{
  var asset_number = $(this).val();
  var hidden_asset_id = $('#hidden_asset_id').val();
  var hidden_asset_number = $('#hidden_asset_number').val();
  var checkbox_val = $('.checkbox_val').val();
  if(checkbox_val == 0)
  {
    $('#asset_number').val('');
    if(asset_number.trim() == hidden_asset_number)
    {
      $('#form-primary').modal('hide');
      $('.scan_table tbody tr').find('td:eq(5) .as2').prop('disabled', true);
      var main = $('.scan_table').find('#main'+hidden_asset_id);
      main.find('td:eq(0)').html("<i title='Acknowledge Asset Parts' class='fa fa-spinner fa-spin fa-2x' style='color:#ff9900'></i>");
      main.find('td:eq(5) .as2').prop('disabled', true);
      $('.scan_qr').attr("disabled", "disabled");
      $("#show" + hidden_asset_id).show();
    }
    else
    {
      alert('Scan Asset Number : '+hidden_asset_number+' !');
      $('#asset_number').val('').focus();
    }
  }
});

$('.submitModal').on('click',function()
{
  var asset_number = $('#asset_number').val();
  var hidden_asset_id = $('#hidden_asset_id').val();
  var hidden_asset_number = $('#hidden_asset_number').val();
  var checkbox_val = $('.checkbox_val').val();
  if(checkbox_val = 1 && asset_number!='')
  {
    $('#asset_number').val('');
    if(asset_number.trim() == hidden_asset_number)
    {
      $('#form-primary').modal('hide');
      $('.scan_table tbody tr').find('td:eq(5) .as2').prop('disabled', true);
      var main = $('.scan_table').find('#main'+hidden_asset_id);
      main.find('td:eq(0)').html("<i title='Acknowledge Asset Parts' class='fa fa-spinner fa-spin fa-2x' style='color:#ff9900'></i>");
      main.find('td:eq(5) .as2').prop('disabled', true);
      $('.scan_qr').attr("disabled", "disabled");
      $("#show" + hidden_asset_id).show();
    }
    else
    {
      alert('Scan Asset Number : '+hidden_asset_number+' !');
      $('#asset_number').val('').focus();
    }
  }
  else
  {
    alert('Please Enter Asset Number !');
    $('#asset_number').val('').focus();
  }

});