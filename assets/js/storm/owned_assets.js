/***TABLE COLLAPSE ON 12-Nov-2015***/
$('.details').hide();
$(document).on('click',".toggle-details",function () {
  var row=$(this).closest('tr');
  var next=row.next();
  $('.details').not(next).hide();
  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');

  next.toggle();
  if (next.is(':hidden')) {
    $(this).attr('src',ASSET_URL+'images/plus.png');
    $(this).attr('title','Expand');
  } else {
    $(this).attr('src',ASSET_URL+'images/minus.png');
    $(this).attr('title','Collapse');
  }
});