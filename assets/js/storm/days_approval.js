
$("#newDate").datepicker({

    dateFormat: "dd-mm-yy",
    changeMonth: true,
    changeYear: true,
    minDate: getCurrentReturnDate()
});

function getCurrentReturnDate()
{
	var tdate = new Date();
	var dd = tdate.getDate(); 
    var MM = tdate.getMonth(); 
    var yyyy = tdate.getFullYear(); 
    var currentDate= dd + "-" +( MM+1) + "-" + yyyy;
	var previous_date = $('#oldDate').val();
	if(previous_date >= currentDate) 
	{
		return previous_date;
	}
	else
	{
		return currentDate;
	}
	
}
