$(document).on('click','.courier_type',function(){
  var courier_type = $(this).val();
  if(courier_type==1)
  {
    $('.courier_div').removeClass('hidden');
    $('.docket_div').removeClass('hidden');
    $('.contact_div').addClass('hidden');
    $('.contact_div').find('.error').remove();
    $('.phone_div').addClass('hidden');
    $('.phone_div').find('.error').remove();
    $('.vehicle_div').addClass('hidden');
    $('.vehicle_div').find('.error').remove();
  }
  else if(courier_type==2)
  {
    $('.contact_div').removeClass('hidden');
    $('.phone_div').removeClass('hidden');
    $('.courier_div').addClass('hidden');
    $('.courier_div').find('.error').remove();
    $('.docket_div').addClass('hidden');
    $('.docket_div').find('.error').remove();
    $('.vehicle_div').addClass('hidden');
    $('.vehicle_div').find('.error').remove();
  }
  else if(courier_type==3)
  {
    $('.vehicle_div').removeClass('hidden');
    $('.contact_div').removeClass('hidden');
    $('.phone_div').removeClass('hidden');
    $('.courier_div').addClass('hidden');
    $('.courier_div').find('.error').remove();
    $('.docket_div').addClass('hidden');
    $('.docket_div').find('.error').remove();
  }
});

$(document).on('blur','.courier_name',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.courier_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.docket_num',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.docket_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.vehicle_num',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.vehicle_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.contact_person',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.contact_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.phone_number',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.phone_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.expected_date',function(){
  var value = $(this).val();
  var ele = $(this);
   
  var ele_error = $(this).closest('.expected_date_row').find('.error');
  ele_error.remove();
});


counter = 2;
count = 1;
$('.add_document').on('click',function()
{
  if(count == 0)
  {
    $('.tab_hide').removeClass('hidden');
    count++;
  }
  else
  {
    var ele = $('.document_table').find('tbody tr:last'); 
    var sl_no = ele.find('td:first .sno').html();
    var doc_type = ele.find('.doc_type').val();
    var doc = ele.find('.document').val();

    ele.find(".error").remove();
    if(doc_type == '' || doc == '')
    {
      if(doc_type == '')
      {
      ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(doc == '')
      {
        ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
        
      }
      return false;
    }
    
    var ele_clone = ele.clone();
    ele_clone.find('.sno').html(parseInt(sl_no)+1);
    ele_clone.find('input, select').attr("disabled", false).val('');
    ele_clone.find('td:last').show();
    ele_clone.find(".remove_bank_row").show();
    ele.after(ele_clone);
    ele_clone.show();
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/\[1\]/g, '['+counter+']');
    });
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/support\_document\_1/g,'support\_document\_'+counter);
    });
    counter++;
  }
});

$(document).on('click','.remove_bank_row',function(){
  var ele = $(this).closest('.doc_row');
  var rowCount = $('.document_table').find('tbody tr').length;
  if(rowCount==1)
  {
    ele.find('.doc_type').val('');
    ele.find('.document').val('');
    ele.find('.error').remove();
    $('.tab_hide').addClass('hidden');
    count = 0;
  }
  else
  {
     $(this).closest('tr').remove();
  }
  var sl_no=1;
  $('.sno').each(function(){
      $(this).html(sl_no);
      sl_no++;
  }); 
});

$(document).on('click','.deactivate',function(){
  var doc_id = $(this).closest('tr').find('.doc_id').val();
  var ele = $(this).closest('tr').find('.deactivate');
  var ele_a = $(this);
  if(doc_id != '')
  {
    var data = 'doc_id='+doc_id;
    $.ajax({
    type:"POST",
    url:SITE_URL+'deactivate_order_delivery_doc',
    data:data,
    cache:false,
    success:function(response){
      if(response>0)
      {
        ele_a.removeClass('btn-danger deactivate').addClass('btn-info activate');
        ele.find('i').removeClass('fa-trash-o').addClass('fa-check');
        alert('Record Has been deactivated');
      }
      else
      {
        alert('Something went Wrong! try Again.');
      }
    }
  });
  }
});

$(document).on('blur','.doc_type',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(1) .error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.document',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(2) .error');
  ele_error.remove();
});

$(document).on('click','.activate',function(){
  var doc_id = $(this).closest('tr').find('.doc_id').val();
  var ele = $(this).closest('tr').find('.activate');
  var ele_a = $(this);
  if(doc_id != '')
  {
    var data = 'doc_id='+doc_id;
    $.ajax({
    type:"POST",
    url:SITE_URL+'activate_order_delivery_doc',
    data:data,
    cache:false,
    success:function(response){
      if(response>0)
      {
        ele_a.removeClass('btn-info activate').addClass('btn-danger deactivate');
        ele.find('i').removeClass('fa-check').addClass('fa-trash-o');
        alert('Record Has been Activated');
      }
      else
      {
        alert('Something went Wrong! try Again.');
      }
    }
  });
  }
});

$(document).on('change','.transaction_type',function(){
  var trans = $(this).val();

  if(trans == '')
  {
    $('.invoice_btn').addClass('hidden');
    $('.dc_btn').addClass('hidden');
    $('.submit_btn').removeClass('hidden');
  }
  else if(trans == 2 || trans == 3 || trans == 4)
  {
    $('.invoice_btn').addClass('hidden');
    $('.dc_btn').removeClass('hidden');
    $('.submit_btn').addClass('hidden');
  }
  else if(trans == 1)
  {
    $('.invoice_btn').removeClass('hidden');
    $('.dc_btn').addClass('hidden');
    $('.submit_btn').addClass('hidden');
  }
});

$(document).on('click','.page2_data, .page1_data',function(){
    var page_number = $(this).val();
    $('.courier_div,.docket_div,.vehicle_div,.contact_div,.phone_div,.expected_date_row').find('.error').remove();
    var courier_type = $('input[name=courier_type]:checked').val();
    var courier_name = $('.courier_name').val();
    var docket_num = $('.docket_num').val();
    var vehicle_num = $('.vehicle_num').val();
    var contact_person = $('.contact_person').val();
    var phone_number = $('.phone_number').val();
    var expected_date = $('.expected_date').val();
    if(expected_date == '' && page_number == 2)
    {
      $('.expected_date').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(courier_type == 1)
    {
      if(courier_name == '' && page_number == 2)
      {
        $('.courier_name').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(docket_num == '' && page_number == 2)
      {
        $('.docket_num').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(courier_name == '' || docket_num == '' || expected_date == '')
      {
        if(page_number == 2)
        {
          alert("Please fill All Required Fields!");
        }
        $('.forward_btn').attr('disabled','disabled');
        $('.page2_data').removeAttr('data-wizard');
        return false;
      }
    }
    else if(courier_type == 2)
    {
      if(contact_person == '' && page_number == 2)
      {
         $('.contact_person').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(phone_number == '' && page_number == 2)
      {
         $('.phone_number').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(contact_person == '' || phone_number == '' || expected_date == '')
      {
        if(page_number == 2)
        {
          alert("Please fill All Required Fields!");
        }
        $('.forward_btn').attr('disabled','disabled');
        $('.page2_data').removeAttr('data-wizard');
        return false;
      }
    }
    else if(courier_type == 3)
    {
      if(contact_person == '' && page_number == 2)
      {
         $('.contact_person').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(phone_number == '' && page_number == 2)
      {
         $('.phone_number').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(vehicle_num == '' && page_number == 2)
      {
         $('.vehicle_num').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(contact_person == '' || phone_number == '' || vehicle_num == '' || expected_date == '')
      {
        if(page_number == 2)
        {
          alert("Please fill All Required Fields!");
        }
        $('.forward_btn').attr('disabled','disabled');
        $('.page2_data').removeAttr('data-wizard');
        return false;
      }
    }

    var billed_to = $('.billed_to :selected').val();
    var add1 = $('.add1').val();
    var add2 = $('.add2').val();
    var add3 = $('.add3').val();
    var add4 = $('.add4').val();
    var pin_code = $('.pin_code').val();
    var gst_number = $('.gst_number').val();
    var pan_number = $('.pan_number').val();
    if(billed_to == '' || add1 == '' || add2 == '' || add3 == '' || add4 == '' || pin_code == '' || gst_number == '' || pan_number == '')
    {
      if(page_number == 2)
      {
        alert("Please fill All Required Fields!");
      }
      $('.forward_btn').attr('disabled','disabled');
      $('.page2_data').removeAttr('data-wizard');
      return false;
    }
    $('.forward_btn').removeAttr('disabled');
    $('.page2_data').attr('data-wizard','#wizard1');
    $('.page2_data').triggerHandler('click');
});

$("form").submit(function( e ) 
{
      $('.courier_div,.docket_div,.vehicle_div,.contact_div,.phone_div,.expected_date_row').find('.error').remove();
      var courier_type = $('input[name=courier_type]:checked').val();
      var courier_name = $('.courier_name').val();
      var docket_num = $('.docket_num').val();
      var vehicle_num = $('.vehicle_num').val();
      var contact_person = $('.contact_person').val();
      var phone_number = $('.phone_number').val();
      var expected_date = $('.expected_date').val();
      if(expected_date == '')
      {
        $('.expected_date').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(courier_type == 1)
      {
        if(courier_name == '')
        {
          $('.courier_name').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(docket_num == '')
        {
          $('.docket_num').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(courier_name == '' || docket_num == '' || expected_date == '')
        {
          alert("Please fill All Required Fields!");
          return false;
        }
      }
      else if(courier_type == 2)
      {
        if(contact_person == '')
        {
           $('.contact_person').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(phone_number == '')
        {
           $('.phone_number').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(contact_person == '' || phone_number == '' || expected_date == '')
        {
          alert("Please fill All Required Fields!");
          return false;
        }
      }
      else if(courier_type == 3)
      {
        if(contact_person == '')
        {
           $('.contact_person').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(phone_number == '')
        {
           $('.phone_number').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(vehicle_num == '')
        {
           $('.vehicle_num').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(contact_person == '' || phone_number == '' || vehicle_num == '' || expected_date == '')
        {
          alert("Please fill All Required Fields!");
          return false;
        }
      }

      var billed_to = $('.billed_to :selected').val();
      var transaction_type = $('.transaction_type :selected').val();
      var add1 = $('.add1').val();
      var add2 = $('.add2').val();
      var add3 = $('.add3').val();
      var add4 = $('.add4').val();
      var pin_code = $('.pin_code').val();
      var gst_number = $('.gst_number').val();
      var pan_number = $('.pan_number').val();
      if(transaction_type == '' || billed_to == '' || add1 == '' || add2 == '' || add3 == '' || add4 == '' || pin_code == '' || gst_number == '' || pan_number == '')
      {
        alert("Please fill All Required Fields!");
        return false;
      }

      var ele = $('.document_table').find('tbody tr:last'); 
      var doc_type = ele.find('.doc_type').val();
      var doc = ele.find('.document').val();
      ele.find(".error").remove();
      if(doc_type == '' && doc == ''){ }
      else
      {
        if(doc_type == '')
        {
        ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(doc == '')
        {
          ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
          
        }
        if(doc_type == "" || doc == "")
        {
          alert("please fill All Required Fields!");
          return false;
        }
      }

  if ($('.error').length) {
    e.preventDefault();
    alert("Please fill All Required Fields!");
    return false;
  }
  $.ajax({
    url: SITE_URL,
    context: document.body,
    success: function(s){
      window.location.href = SITE_URL+'closed_fe_delivery_list';
    }
  });

});

$(".exp_date").datepicker({
  //alert('koko');
  dateFormat: "dd-mm-yy",
  changeMonth: true,
  changeYear: true,
  minDate: 0,
  onSelect: function (date) 
  {                  
      var date2 = $(this).datepicker('getDate');
      $('.exp_date').datepicker('option', 'minDate', date2);
  }
});