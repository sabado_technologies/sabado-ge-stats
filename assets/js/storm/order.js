$('.icheck').change(function(){
	if(this.checked)
	{
		$(this).closest(".toolRow").find(".toolQuantity").prop("disabled",false);
		$(this).closest(".toolRow").find(".toolQuantity").val("1");
	}
	else
	{	
		$(this).closest(".toolRow").find(".toolQuantity").prop("disabled",true);
		$(this).closest(".toolRow").find(".toolQuantity").val("");
	}
	var len=$('.icheck:checked').length;
	if(len > 0)
	{
		$("button[name='add']").prop("disabled",false);
	}
	else
	{
		$("button[name='add']").prop("disabled",true);
	}
});

$('.removeRow').click(function(){
  var rowCount = $('#mytable').find('tbody tr').length;	
	if(confirm('Are you sure you want to remove'))
	{
    if(rowCount == 1 )
    {
      alert('Sorry!, To Proceed an order should have minimum one tool is Required to Proceed');
      return false;
    }
		var tool_id = parseInt($(this).attr('data-cid')); 
    var tool_order_id = $(this).attr('data-orderid');
    var ordered_tool_id = parseInt($(this).attr('data-orderedtool'));
    var available_quantity = parseInt($(this).attr('data-availableqty'));
    var quantity = parseInt($(this).attr('data-quantity')); 
    var current_stage_id = parseInt($(this).attr('data-currentstage')); 
		var toolRow = $(this).closest(".toolSelectedRow");
		$("#orderCartContainer").css("opacity",0.5);
		$("#loaderID").css("opacity",1);
		var data = 'tool_id='+tool_id+'&tool_order_id='+tool_order_id+'&available_quantity='+available_quantity+'&quantity='+quantity+'&ordered_tool_id='+ordered_tool_id+'&current_stage_id='+current_stage_id;  		
		$.ajax({
		 type:"POST",
		 url:SITE_URL+'removeToolFromIssueCart',
		 data:data,
		 cache:false,
		 success:function(html){
			toolRow.remove();
			$("#orderCartContainer").css("opacity",1);
			$("#loaderID").css("opacity",0);
		 }
			});
	}
});

$("#deploy_date").datepicker({
  dateFormat: "dd-mm-yy",
  changeMonth: true,
  changeYear: true,
  minDate: 0,
  maxDate:0,
  onSelect: function (date) {               
    var date = $(this).datepicker('getDate');
    $('#dateTo').datepicker('option', 'minDate', date2);
  }
});

$("#dateFrom").datepicker({
  dateFormat: "dd-mm-yy",
  changeMonth: true,
  changeYear: true,
  minDate: 0,
  onSelect: function (date) {                  
    var date2 = $(this).datepicker('getDate');
    $('#dateTo').datepicker('option', 'minDate', date2);
  }
});

function calculate() {
  var d1 = $('#dateFrom').datepicker('getDate');
  var d2 = $('#dateTo').datepicker('getDate');
  var diff = 1;
  if (d1 && d2) {  
    diff += Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
  }    
  return diff;
}

$(document).on('change','.delivery_type',function(){
  var val = $(this).val();
  if(val==1)
  {
    $('#system_id').val('');
    $('.customerRow').removeClass('hidden');  
    $('.warehouseRow').addClass('hidden');  
    $('.othersRow').addClass('hidden'); 
    $("#wh_error").remove();
  }
  if(val == 2)
  {
	  $('.customerRow').addClass('hidden');  
    $('.warehouseRow').removeClass('hidden');  
    $('.othersRow').addClass('hidden');  
    $("#error1").remove();
    $("#error2").remove();       
    $("#system_error").remove();  
  }
  if(val == 3)
  {
	  $('.customerRow').addClass('hidden');  
    $('.warehouseRow').addClass('hidden');  
    $('.othersRow').removeClass('hidden');        
    $("#system_error").remove();  
    $("#wh_error").remove();
    $("#error1").remove();
    $("#error2").remove();     
  }
});

$(document).on('change','.service_type_id',function(){
  var val = $(this).val();
  if(val==5)
  {
    $('.service_type_remarks_div').removeClass('hidden');  
    $('.service_type_remarks_val').val(" ");
  }
  else
  {
    $('.service_type_remarks_div').addClass('hidden');            
    $("#service_type_error").remove(); 
  }
  if(val==1)
  {
    $('.site_readiness_div').removeClass('hidden');
    $(".site_remarks_value").val('');           
  }
  else
  {
    $('.site_readiness_div').addClass('hidden'); 
    $(".site_remarks_value").val('');    
  }
});

$(document).on('change','.site_readiness',function(){
  var val = $(this).val();
  if(val==1)
  {
    $('.site_remarks').addClass('hidden');
    $(".site_remarks_value").val('');
  }
  else
  {
    $('.site_remarks').removeClass('hidden');
    $(".site_remarks_value").val('');
  }
});

$(document).on('change','.fe_check',function(){
  $('#fe1_order_number_error').remove();
  var val = $(this).val();
  if(val==1)
  {
    $('.fe1_order_number_div').removeClass('hidden');
    $(".fe1_order_number").val('');
  }
  else
  {
    $('.fe1_order_number_div').addClass('hidden');
    $(".fe1_order_number").val('');
  }
});

$(document).on('change','#document',function(){
  var ext = $(this).val().split('.').pop().toLowerCase();
  if($.inArray(ext, ['jpg','png','pdf','gif']) == -1) {
      alert('invalid extension! allowed .pdf, .jpg, .png, .gif only');
      $(this).val('');        
      return false;
  }    
});

$(document).on('change','.change_address',function(){
  var val = $(this).val();
  if(val==1)
  {    	
    $('.address_check').removeClass('hidden');
    $(".address_value").val('');
    $('#ct_address_1').removeAttr("disabled"," ");
    $('#ct_address_2').removeAttr("disabled"," ");
    $('#ct_address_3').removeAttr("disabled"," ");
    $('#ct_address_4').removeAttr("disabled"," ");
    $('#ct_pin_code').removeAttr("disabled"," ");
  }
  else
  {
     $('.address_check').addClass('hidden');
     $(".address_value").val('');
     $('#ct_address_1').attr("disabled","disabled");
     $('#ct_address_2').attr("disabled","disabled");
     $('#ct_address_3').attr("disabled","disabled");
     $('#ct_address_4').attr("disabled","disabled");
     $('#ct_pin_code').attr("disabled","disabled");
  }
});

$(document).on('blur','#system_id',function(){
  var system_id = $('#system_id').val();   
  var transactionUser = $('.transactionUser').val(); 
  var transactionCountry = $('.transactionCountry').val();
  if(system_id != '')
  {
  	$('#error2').remove();   
    $.ajax({
        type:"POST",
        url:SITE_URL+'chackSystemIDAvailability',
        data:{system_id:system_id,transactionUser:transactionUser,transactionCountry:transactionCountry},
        cache:false,
        success:function(response){
        	$("#error2").remove();
        	if(response>0)
        	{
          	$('#system_id').removeClass('parsley-error');
          	$("#error2").remove();
          	$.ajax({
  		        type:"POST",
  		        url:SITE_URL+'getAddressByID',
  		         data:{system_id:system_id,transactionUser:transactionUser},
  		        cache:false,
  		        success:function(html){				          
  		          if(html!='null')
  		          {
  		            var arr = jQuery.parseJSON(html);
  		            $('#site_id').val(arr['site_id']);
                  $('#install_base_id').val(arr['install_base_id']);
  		            $('#ct_address_1').val(arr['address1']);
  		            $('#ct_address_2').val(arr['address2']);
  		            $('#ct_address_3').val(arr['address3']);
  		            $('#ct_address_4').val(arr['address4']);
  		            $('#system_id').val(system_id);
  		            $('#ct_pin_code').val(arr['zip_code']);
                  $('#c_name').val(arr['c_name']);                    
  		          }
  		        }
		        });	          		            	
        	}
          else
          {
          	$("#system_id").after("<p class='color-danger' id='error2'>This System ID Not Available</p>");
            $('#site_id').val('');
            $('#install_base_id').val('');
            $('#ct_address_1').val('');
            $('#ct_address_2').val('');
            $('#ct_address_3').val('');
            $('#ct_address_4').val('');              
            $('#ct_pin_code').val('');
            $('#c_name').val('');
          }
        }
    });
  }
  else
  {
    $("#system_id").val('');
    $('#site_id').val('');
    $('#install_base_id').val('');
    $('#ct_address_1').val('');
    $('#ct_address_2').val('');
    $('#ct_address_3').val('');
    $('#ct_address_4').val('');              
    $('#ct_pin_code').val('');
    $('#c_name').val('');
  }    
});

counter = 2;
count = 1;
$(document).on('click','.add_document',function(){
  if(count == 0)
  {
    $('.tab_hide').removeClass('hidden');
    count++;
  }
  else
  {
    var ele = $('.document_table').find('tbody tr:last'); 
    var sl_no = ele.find('td:first .sno').html();
    var doc_type = ele.find('.doc_type').val();
    var doc = ele.find('.document').val();
    ele.find(".error").remove();
    if(doc_type == '' || doc == '')
    {
      if(doc_type == '')
      {
      ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(doc == '')
      {
        ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");        
      }
      return false;
    }    
    var ele_clone = ele.clone();
    ele_clone.find('.sno').html(parseInt(sl_no)+1);
    ele_clone.find('input, select').attr("disabled", false).val('');
    ele_clone.find('td:last').show();
    ele_clone.find(".remove_bank_row").show();
    ele.after(ele_clone);
    ele_clone.show();
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/\[1\]/g, '['+counter+']');
    });
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/support\_document\_1/g,'support\_document\_'+counter);
    });
    counter++;
  }
});

$(document).on('click','.remove_bank_row',function(){
  var ele = $(this).closest('.doc_row');
  var rowCount = $('.document_table').find('tbody tr').length;
  if(rowCount==1)
  {
    ele.find('.doc_type').val('');
    ele.find('.document').val('');
    ele.find('.error').remove();
    $('.tab_hide').addClass('hidden');
    count = 0;
  }
  else
  {
     $(this).closest('tr').remove();
  }
  var sl_no=1;
  $('.sno').each(function(){
      $(this).html(sl_no);
      sl_no++;
  }); 
});

$(document).on('blur','.doc_type',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(1) .error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.document',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(2) .error');
  ele_error.remove();
});

$(document).on('click','.deactivate',function(){
  var asset_doc_id = parseInt($(this).closest('tr').find('.asset_doc_id').val());
  var ele = $(this).closest('tr').find('.deactivate');
  var ele_a = $(this);
  if(asset_doc_id != '')
  {
    var data = 'asset_doc_id='+asset_doc_id;
    $.ajax({
    type:"POST",
    url:SITE_URL+'deactivate_attached_order_rec',
    data:data,
    cache:false,
    success:function(response){
      if(response>0)
      {
        ele_a.removeClass('btn-danger deactivate').addClass('btn-info activate');
        ele.find('i').removeClass('fa-trash-o').addClass('fa-check');
        alert('Record Has been deactivated');
      }
      else
      {
        $("#error").remove();
      }
    }
  });
  }
});

$(document).on('click','.activate',function(){
  var asset_doc_id = parseInt($(this).closest('tr').find('.asset_doc_id').val());
  var ele = $(this).closest('tr').find('.activate');
  var ele_a = $(this);
  if(asset_doc_id != '')
  {
    var data = 'asset_doc_id='+asset_doc_id;
    $.ajax({
    type:"POST",
    url:SITE_URL+'activate_attached_order_rec',
    data:data,
    cache:false,
    success:function(response){
      if(response>0)
      {
        ele_a.removeClass('btn-info activate').addClass('btn-danger deactivate');
        ele.find('i').removeClass('fa-check').addClass('fa-trash-o');
        alert('Record Has been Activated');
      }
      else
      {
        $("#error").remove();
      }
    }
  });
  }
});

function get_warehouse_address(){
  var warehouse_id = $('.wh_id_cls').val();      
  var data = 'warehouse_id='+warehouse_id;      
  $.ajax({
      type:"POST",
      url:SITE_URL+'getWarehouseAddress',
      data:data,
      cache:false,          
      success:function(html)
      {                 
        if(html!='null')
        {
          var arr = jQuery.parseJSON(html);                    
          $('#wh_address_1').val(arr['address1']);
          $('#wh_address_2').val(arr['address2']);
          $('#wh_address_3').val(arr['address3']);
          $('#wh_address_4').val(arr['address4']);                    
          $('#wh_pin_code').val(arr['pin_code']);
        }  
        else
        {
            $('#wh_address_1').val('');
            $('#wh_address_2').val('');
            $('#wh_address_3').val('');
            $('#wh_address_4').val('');
            $('#wh_pin_code').val('');
        }                                            
      }
  });
}

$(document).on('change','.wh_id_cls',function(){  
  var warehouse_id = $('.wh_id_cls').val();    
  var data = 'warehouse_id='+warehouse_id;      
  $.ajax({
    type:"POST",
    url:SITE_URL+'getWarehouseAddress',
    data:data,
    cache:false,          
    success:function(html)
    {                 
      if(html!='null')
      {
        var arr = jQuery.parseJSON(html);                    
        $('#wh_address_1').val(arr['address1']);
        $('#wh_address_2').val(arr['address2']);
        $('#wh_address_3').val(arr['address3']);
        $('#wh_address_4').val(arr['address4']);                    
        $('#wh_pin_code').val(arr['pin_code']);            
      } 
      else
      {        
        $('#wh_address_1').val('');
        $('#wh_address_2').val('');
        $('#wh_address_3').val('');
        $('#wh_address_4').val('');
        $('#wh_pin_code').val('');        
      }                                             
    }        
  });
});

function get_warehouses_dropdown_by_sso(){
  $(document).on('change','.sso_id_cls',function(){
    var sso_id = $('.sso_id_cls').val();
    var transactionCountry = $('.transactionCountry').val();
    if(sso_id!=''){
      $.ajax({
        type: "POST",
        url: SITE_URL+"htmlWarehouseDropdown",
        data:'sso_id='+sso_id,
        success: function(data){               
        $(".wh_id_cls").html(data);
        }
      });
    }
  });
}

$(document).on('blur',"#fe1_order_number",function (){
  $("#fe1_order_number_error").remove();
  var order_number = $(this).val();
  if(order_number == '')
  {
    $("#fe1_order_number_error").remove();
  }
  else
  {
    var transactionUser = $('.transactionUser').val();
    var transactionCountry = $('.transactionCountry').val();
    var parentPage = $('.transactionPage').val();
    var new_keys = new Array();
    var new_values = new Array();
    $('.toolIdCls').each(function() {
      new_keys.push($(this).val());
    });
    $('.toolQuantity').each(function() {                     
      new_values.push($(this).val());
    });
    $.ajax({
      type:"POST",
      url:SITE_URL+'check_order_number_availability',
      data:{order_number:order_number,sso_id:transactionUser,parent_page:parentPage,transactionCountry:transactionCountry,keys:new_keys,values:new_values},
      cache:false,
      success:function(response){
        var obj = jQuery.parseJSON(response);
        var response = obj['flg'];
        var message = obj['message'];
        $("#fe1_order_number_error").remove();
        if(response>0)
        {              
          $("#fe1_order_number_error").remove();              
        }
        else
        {
          $('#fe1_order_number').val('');
          $("#fe1_order_number").after("<p class='color-danger' id='fe1_order_number_error'> "+message+"  !</p>");
        }
      }
    });
  }
});

$("#dateTo").datepicker({
  dateFormat: "dd-mm-yy",
  changeMonth: true,
  changeYear: true,
  minDate:0,
  onSelect: function (date) {
    var date2 = $(this).datepicker('getDate');
    $('#dateFrom').datepicker('option', 'maxDate', date2);
    var diff = calculate();
    var tool_count=0;
    var tool_id_arr=[];
    $('.fe_tools').each(function(){
      tool_id_arr.push(parseInt($(this).attr('data-cid')));
      tool_count++;
    });
    var check=0;
    $('.fe_tool_arr').each(function(){
      var tool_id = parseInt($(this).attr('data-tid'));
      if(jQuery.inArray(tool_id, tool_id_arr) !== -1)
      {
        check++;
      }
    });
    if(parseInt(check) == parseInt(tool_count))
    {
        if(diff>365)
        {
          alert("The Return Date Should be Less Than A Year To Request Date");                                
          $('#dateTo').val('');
        }
        
    }
    else
    {
      if(diff>10)
      {
         alert('please Select Return date less than 10 days ');            
         $('#dateTo').val('');
      }       
    }      
  }
});

$('.details').hide();
$(document).on('click',".toggle-details",function(){
  var row=$(this).closest('tr');
  var next=row.next();
  $('.details').not(next).hide();
  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');

  next.toggle();
  if (next.is(':hidden')) {
    $(this).attr('src',ASSET_URL+'images/plus.png');
    $(this).attr('title','Expand');
  } else {
    $(this).attr('src',ASSET_URL+'images/minus.png');
    $(this).attr('title','Collapse');
  }
});

$(document).on('click','.check_with_sso',function(){
  $('#fe_owned_tools_frm').trigger("reset");
  $('#feSearchResults').html('<h4>Search FE Orders Here</h4>');
  $.ajax({
    type:"POST",
    url:SITE_URL+'check_with_sso',
    data:{state_id:0},
    cache:false,
    success:function(html){
      var obj = jQuery.parseJSON(html);
      $(".sso_data").html(obj['result1']);
      $('.sso_data').select2('destroy'); 
      $('.sso_data').select2();
    }
  });
});

$(document).on('click','#fe_owned_search',function(){
  var submit_button = 1; // don't depend on segment
  $('#submit_button').val(submit_button);
  searchFeOwnedTools();
});

$(document).on('click','#reset_search',function(){
  $('#fe_owned_tools_frm').trigger("reset");
  $.ajax({
    type:"POST",
    url:SITE_URL+'check_with_sso',
    data:{state_id:0},
    cache:false,
    success:function(html){
      var obj = jQuery.parseJSON(html);
      $(".sso_data").html(obj['result1']);
      $('.sso_data').select2('destroy'); 
      $('.sso_data').select2();
    }
  });
  var submit_button = 1;
  $('#submit_button').val(submit_button);
  searchFeOwnedTools();
});

$(document).on('click','#feToolSearchPagination .pagination a',function(){
  var submit_button  = 0;
  $('#submit_button').val(submit_button);
  var offset = $(this).attr("href");
    if(offset == 0)
    {
      $('#current_offset').val('');
      searchFeOwnedTools();
    }
    else
    { 
      var arr = offset.split('#');
      var second = arr[1];
      if(second.length == 0)
      {
        $('#current_offset').val('');
        searchFeOwnedTools();
      }
      else
      {
        var second_arr = second.split('/');
        final_offset = second_arr[1];
        $('#current_offset').val(final_offset);
        searchFeOwnedTools();
      }
    }
});

function searchFeOwnedTools(){
  $("#form-primary").css("opacity",0.5);
  $("#loaderID").css("opacity",1);  
  var data = $('#fe_owned_tools_frm').serialize();
  var current_offset = $('#current_offset').val();
  var url = SITE_URL+'ajax_toolsAvailabilityWithFE';
  var submit_button = $('#submit_button').val();
  if(submit_button == 0 ) // don't depend on submit button
  {
    if(current_offset!='')
    {
      url += '/'+current_offset;
    }
    else
    {
      $('#current_offset').val('');
    }
  }
  else
  {
    $('#current_offset').val('');
  }
  $.ajax({
    type:"POST",
    url:url,
    data:data,
    cache:false,
    success:function(response){
      var obj = jQuery.parseJSON(response);
      $("#form-primary").css("opacity",1);
      $("#loaderID").css("opacity",0);
      $("#feSearchResults").html(obj['result']);
    }
  });
}

$(document).on('change','.fe1_order',function(){
  var chk_len = $('.fe1_order:checked').length;
  if(chk_len>0)
    $('#confirm_fe1_order').prop('disabled',false);
  else
    $('#confirm_fe1_order').prop('disabled',true);
});

$(document).on('click','#confirm_fe1_order',function(){
  var order_number = $('.fe1_order:checked').val();
  if(order_number!='')
  {
    var transactionUser = $('.transactionUser').val();
    var transactionCountry = $('.transactionCountry').val();
    var parentPage = $('.transactionPage').val();
    var new_keys = new Array();
    var new_values = new Array();
    $('.toolIdCls').each(function() {
      new_keys.push($(this).val());
    });
    $('.toolQuantity').each(function() {                     
      new_values.push($(this).val());
    });
    $.ajax({
      type:"POST",
      url:SITE_URL+'check_order_number_availability',
      data:{order_number:order_number,sso_id:transactionUser,parent_page:parentPage,transactionCountry:transactionCountry,keys:new_keys,values:new_values},
      cache:false,
      success:function(response){
        var obj = jQuery.parseJSON(response);
        var response = obj['flg'];
        var message = obj['message'];
        if(response == 0)
        {              
          alert(message);
          return false;              
        }
        else
        {
          $('#fe1_order_number').val(order_number);
          $('#form-primary').modal('hide');
        }
      }
    });
  }
  else
  {
    $('#fe1_order_number').val('');
  }
});

$("form").submit(function( e ) {
  if($('#error').length)
  {
    e.preventDefault();
    return false;
  }
  if($('#error1').length) 
  {
    e.preventDefault();
    return false;
  }
  if($('#error2').length) 
  {
    e.preventDefault();
    return false;
  }
  var tool_count=0;
  var tool_id_arr=[];
  $('.fe_tools').each(function(){
    tool_id_arr.push(parseInt($(this).attr('data-cid')));
    tool_count++;
  });
  var check=0;
  $('.fe_tool_arr').each(function(){
    var tool_id = parseInt($(this).attr('data-tid'));
    if(jQuery.inArray(tool_id, tool_id_arr) !== -1)
    {
      check++;
    }
  });
  diff=calculate();
  if(parseInt(check)==parseInt(tool_count))
  {
    if(diff>365) 
    {
      alert("The Return Date Should be Less Than a Year"); 
      return false;
    }
  }
  else
  {
    if(diff>10)
    {
      alert('please Select Return date less than 10 days ');
      return false;
    }
  }
  var o_d_type = $('input[name=delivery_type_id]:checked').val();
  var f_service_type_id = $('.service_type_id').val();  
  if(f_service_type_id == 5)
  {
    $("#service_type_error").remove();    
    var f_st_remarks = $('.service_type_remarks_value').val();    
    if(f_st_remarks =='')
    {      
      $('.service_type_remarks_value').after("<p class='color-danger' id='service_type_error'>Please Fill Remarks.</p>");
      return false;
    }
  }
  var fe_check = parseInt($('input[name=fe_check]:checked').val());
  if(fe_check == 1)
  {
    $("#fe1_order_number_error").remove();    
    var fe1_order_number = $('#fe1_order_number').val();    
    if(fe1_order_number =='')
    {
      $('#fe1_order_number').after("<p class='color-danger' id='fe1_order_number_error'>Please Fill FE1 Order Number.</p>");
      return false;
    }
  }
  if(o_d_type == 1)
  {
    $("#system_error").remove();    
    var f_system_id = $('#system_id').val();    
    var address1 = $('#ct_address_1').val();
    var address2 = $('#ct_address_2').val();
    var address3 = $('#ct_address_3').val();
    var address4 = $('#ct_address_4').val();
    var pin_code = $('#ct_pin_code').val();
    if(f_system_id == '')
    {
      $("#system_id").after("<p class='color-danger' id='system_error'>System ID Required</p>");
      return false;
    }
    else
    {
        $("#system_error").remove();
    }
    if(address1=='' || address2 =='' || address3 == '' || address4 =='' || pin_code =='')
    {
      alert('Please Fill All The Required Information');
      return false;
    }
    var address_check = $('input[name=check_address]:checked').val();     
    if(address_check == 1)
    {
      if($('.address_value').val() == '')   
      {
        alert('Please Fill All The Required Information');
        return false;
      }
    }
  }
  if(o_d_type == 2)
  {
    $("#system_error").remove();     
    $("#wh_error").remove();
    var f_wh_id = $('.wh_id_cls').val();
    if(f_wh_id == '')
    {
      $(".wh_id_cls").after("<p class='color-danger' id='wh_error'>Plase Select Warehouse</p>");
      return false;
    }
    else
    {
      $("#wh_error").remove();
    }
    var address1 = $('#wh_address_1').val();
    var address2 = $('#wh_address_2').val();
    var address3 = $('#wh_address_3').val();
    var address4 = $('#wh_address_4').val();
    var pin_code = $('#wh_pin_code').val();
    if(address1=='' || address2 =='' || address3 == '' || address4 =='' || pin_code =='')
    {
      alert('Please Fill All The Required Information');
      return false;
    }
  }
  if(o_d_type == 3)
  {
    $("#system_error").remove();     
    $("#wh_error").remove();
    var address1 = $('#address_1').val();
    var address2 = $('#address_2').val();
    var address3 = $('#address_3').val();
    var address4 = $('#address_4').val();
    var pin_code = $('#pin_code').val();
    if(address1=='' || address2 =='' || address3 == '' || address4 =='' || pin_code =='')
    {
      alert('Please Fill All The Required Information');
      return false;
    }
  }
  var ele = $('.document_table').find('tbody tr:last'); 
  var doc_type = ele.find('.doc_type').val();
  var doc = ele.find('.document').val();
  ele.find(".error").remove();  
  if(doc_type == '' && doc == ''){
  }
  else
  { 
    if(doc_type == '')
    {
    ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(doc == '')
    {
      ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
      
    }
    if(doc_type == "" || doc == "")
    {
      alert("please fill All Required Fields!");
      return false;
    }
  }
  if ($('.error').length) {
    e.preventDefault();
    alert("Please fill All Required Fields!");
    return false;
  }
});