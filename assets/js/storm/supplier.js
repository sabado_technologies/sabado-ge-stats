
  $("#supplier_name").on('blur',function () {
      $("#error").remove();
      var name = $(this).val();
      var supplier_id = $("#supplier_id").val();
      var country_id =$('#country_id').val();
      var data = 'name='+name+'&supplier_id='+supplier_id+'&country_id='+country_id;
      
      $.ajax({
        type:"POST",
        url:SITE_URL+'supplier_name_availability',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#supplier_name").after("<p class='color-danger' id='error'>This Supplier name already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
function supplier_type()
{  
  var modality = $("#edit_modality").val();
  if(modality >0)
  {
     $('.modality').show();
  }
  var supplier_type = $('.supplier_type').val();
  if(supplier_type == 2)
  {
    $('#calibration').html('<option value="1">Provide</option');
  }

  $('.supplier_type').on('change',function(){
    var type = $(this).val();
    if(type == 1)
    {
      $('#calibration').html('<option value="1">Provide</option><option value="2">Not Provide</option');  
      $('.modality').removeClass('hidden');
      $('.calibration_provide_block').show();

    }
    else if(type==2)
    {
      $('#calibration').html('<option value="1">Provide</option');
      $('.trans_type').show();
      $('.modality').addClass('hidden');
      $('.calibration_provide_block').show();
    }
  });
}

function calibration_type()
{ 
   $('.cal').on('change',function(){
     var cal_type = $(this).val();
     if(cal_type == 1)
     {
        $('.trans_type').show();
     }
     else
     {
        $('.trans_type').hide();
     }
  });
}
$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});

