$(document).on('change','.cal_status_id',function(){
	var cal_status_id = $('.cal_status_id :selected').val();
	if(cal_status_id == 3 || cal_status_id == 5)
	{
		$('.calibrated_div').removeClass('hidden');
	}
	else
	{
		$('.calibrated_div').addClass('hidden');
	}
	$('.cal_date, .cal_due_date').val('');
});

$(document).on('blur','.cal_date',function(){
	var value = $(this).val();
	var ele = $(this);
	var ele_error = $(this).closest('.cal_date_div').find('.error');
	ele_error.remove();
});

$(document).on('blur','.cal_due_date',function(){
	var value = $(this).val();
	var ele = $(this);
	var ele_error = $(this).closest('.cal_due_date_div').find('.error');
	ele_error.remove();
});

counter = 2;
$('#add_document').on('click',function()
{
  $('.tab_hide').removeClass('hidden');
  var ele = $('.document_table').find('tbody tr:last'); 
  var ele_clone = ele.clone();
  var sl_no = ele.find('td:first .sno').html();
  var doc_type = ele.find('.doc_type').val();
  var doc = ele.find('.document').val();
  ele.find(".error").remove();
  if(doc_type == '' || doc == '')
  {
    if(doc_type == '')
    {
      ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(doc == '')
    {
      ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
    }
    return false;
  }
  ele_clone.find('.sno').html(parseInt(sl_no)+1);
  ele_clone.find('input, select').attr("disabled", false).val('');
  ele_clone.find('td:last').show();
  ele_clone.find(".remove_bank_row").show();
  ele.after(ele_clone);
  ele_clone.show();
  ele_clone.html(function(i, oldHTML) 
  {
    return oldHTML.replace(/\[1\]/g, '['+counter+']');
  });
  ele_clone.html(function(i, oldHTML) 
  {
    return oldHTML.replace(/support\_document\_1/g,'support\_document\_'+counter);
  });
  counter++;
});

$(document).on('click','.remove_bank_row',function(){
	var rowCount = $('.document_table').find('tbody tr').length;
	var ele = $('.document_table').find('tbody tr:last'); 
	if(rowCount==1)
	{
		ele.closest('.doc_row').find('.error').remove();
		ele.find('input, select').attr("disabled", false).val('');
		count = 0;
	}
	else
	{
		$(this).closest('tr').remove();
	}

	var sl_no=1;
	$('.sno').each(function(){
	    $(this).html(sl_no);
	    sl_no++;
	}); 
});

$(document).on('blur','.doc_type',function(){
var value = $(this).val();
var ele = $(this);
var ele_error = $(this).closest('.doc_row').find('td:eq(1) .error');
ele_error.remove();
if(value == '')
{
  ele.after("<p class='color-danger error'>This Value is Required</p>");
}
else
{
  ele_error.remove();
}
});

$(document).on('blur','.document',function(){
var value = $(this).val();
var ele = $(this);
var ele_error = $(this).closest('.doc_row').find('td:eq(2) .error');
ele_error.remove();
});

$(document).on('change','.document',function(){                
  var ext = $(this).val().split('.').pop().toLowerCase();
  if($.inArray(ext, ['jpg','png','pdf','gif']) == -1) {
    alert('invalid extension! allowed .pdf, .jpg, .png, .gif only');
    $(this).val('');
    return false;
  }
});

$("form").submit(function( e )
{
	var ele = $('.document_table').find('tbody tr:last'); 
	var doc_type = ele.find('.doc_type').val();
	var doc = ele.find('.document').val();
	ele.find(".error").remove();
	if(doc_type == '' && doc == ''){ }
	else
	{
		if(doc_type == '')
		{
			ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
		}
		if(doc == '')
		{
			ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
		  
		}
		if(doc_type == "" || doc == "")
		{
			alert("please fill All Required Fields!");
			return false;
		}
	}

	$('.cal_date_div,.cal_due_date_div,.remarks_div').find('.error').remove();
	var cal_status_id = $('.cal_status_id :selected').val();
	var cal_date = $('.cal_date').val();
	var cal_due_date = $('.cal_due_date').val();
	if(cal_status_id == 3 || cal_status_id == 5)
	{
	    if(cal_date == '')
	    {
			$('.cal_date').after("<p class='color-danger error'>This Value is Required</p>");
	    }
	    if(cal_due_date == '')
	    {
	        $('.cal_due_date').after("<p class='color-danger error'>This Value is Required</p>");
	    }
	    if(cal_due_date == '' || cal_date == '')
	    {
	        alert("Please fill All Required Fields!");
	        return false;
	    }
	}

	if ($('.error').length) {
		e.preventDefault();
		alert("Please fill All Required Fields!");
		return false;
	}
});