/***TABLE COLLAPSE ON 12-Nov-2015***/
$('.details').hide();
$(document).on('click',".toggle-details",function(){  
  var row = $(this).closest('tr');
  var next = row.next();
  $('.details').not(next).hide();
  $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');

  next.toggle();
  if (next.is(':hidden')){
    $(this).attr('src',ASSET_URL+'images/plus.png');
    $(this).attr('title','Expand');
  } 
  else{
    $(this).attr('src',ASSET_URL+'images/minus.png');
    $(this).attr('title','Collapse');
  }
});

function get_warehouse_address()
{
  var warehouse_id = $('.wh_id_cls').find(':selected').val();
  var data = 'warehouse_id='+warehouse_id;      
  $.ajax({
    type:"POST",
    url:SITE_URL+'getWarehouseAddress',
    data:data,
    cache:false,          
    success:function(html)
    {                 
      if(html!='null')
      {
        var arr = jQuery.parseJSON(html);
        $('#wh_address_1').val(arr['address1']);
        $('#wh_address_2').val(arr['address2']);
        $('#wh_address_3').val(arr['address3']);
        $('#wh_address_4').val(arr['address4']);
        $('#wh_pin_code').val(arr['pin_code']);
      }
    }
  });
}

$(document).on('click','.asset_status',function(){
  $('#order_number').val('');
  $("#error1").remove();
});

$(document).on('change','.change_address',function(){
  var val = $(this).val();
  if(val==1)
  {
    $('.address_check').removeClass('hidden');
    $(".address_value").val('');
    $('#ct_address_1').removeAttr("disabled"," ");
    $('#ct_address_2').removeAttr("disabled"," ");
    $('#ct_address_3').removeAttr("disabled"," ");
    $('#ct_address_4').removeAttr("disabled"," ");
    $('#ct_pin_code').removeAttr("disabled"," ");
  }
  else
  {
    $('.address_check').addClass('hidden');
    $(".address_value").val('');
    $('#ct_address_1').attr("disabled","disabled");
    $('#ct_address_2').attr("disabled","disabled");
    $('#ct_address_3').attr("disabled","disabled");
    $('#ct_address_4').attr("disabled","disabled");
    $('#ct_pin_code').attr("disabled","disabled");
  }
});

$(document).on('change','#checkAll',function(){
  if(this.checked)
  {
    $('.openOrder').prop('checked',true);
    $("button[name='submit_pickup']").prop("disabled",false);
  }
  else
  {
    $('.openOrder').prop('checked',false);
    $("button[name='submit_pickup']").prop("disabled",true);
  }
  $('#order_number').val('');
  $("#error1").remove();
});

$(document).on('change','.openOrder',function(){
  var len=$('.chkAll:checked').length;
  if(len > 0)
  {
    $("button[name='submit_pickup']").prop("disabled",false);
  }
  else
  {
    $("button[name='submit_pickup']").prop("disabled",true);
  }
  $('#order_number').val('');
  $("#error1").remove();
});

$(document).on('blur','#system_id',function(){
  var system_id = $('#system_id').val();   
  var transactionUser = $('.transactionUser').val(); 
  if(system_id != '')
  {
    $('#error2').remove();      
    $.ajax({
      type:"POST",
      url:SITE_URL+'chackSystemIDAvailability',
      data:{system_id:system_id,transactionUser:transactionUser},
      cache:false,
      success:function(response)
      {
        $("#error2").remove();
        if(response>0)
        {
          $('#system_id').removeClass('parsley-error');
          $("#error2").remove();
          $.ajax({
            type:"POST",
            url:SITE_URL+'getAddressByID',
            data:{system_id:system_id,transactionUser:transactionUser},
            cache:false,
            success:function(html)
            {                 
              if(html!='null')
              {
                var arr = jQuery.parseJSON(html);
                $('#install_base_id').val(arr['install_base_id']);
                $('#site_id').val(arr['site_id']);
                $('#ct_address_1').val(arr['address1']);
                $('#ct_address_2').val(arr['address2']);
                $('#ct_address_3').val(arr['address3']);
                $('#ct_address_4').val(arr['address4']);
                $('#system_id').val(system_id);
                $('#ct_pin_code').val(arr['zip_code']);
                $('#c_name').val(arr['c_name']); 
              }
            }
          });                             
        }
        else
        {
          $("#system_id").after("<p class='color-danger' id='error2'>This System ID Not Available</p>");
          $('#install_base_id').val('');
          $('#site_id').val('');
          $('#ct_address_1').val('');
          $('#ct_address_2').val('');
          $('#ct_address_3').val('');
          $('#ct_address_4').val('');
          $('#ct_pin_code').val('');
          $('#c_name').val('');
        }
      }
    });
  }
  else
  {
    $('#system_id').val('');
    $('#install_base_id').val('');
    $('#site_id').val('');
    $('#ct_address_1').val('');
    $('#ct_address_2').val('');
    $('#ct_address_3').val('');
    $('#ct_address_4').val('');
    $('#ct_pin_code').val('');
    $('#c_name').val('');
  }
});

$(document).on('change','.return_cls',function(){
  var return_type_id = $(this).val();
  var len=$('.chkAll:checked').length;
  if(len > 0)
  {
    $("button[name='submit_pickup']").prop("disabled",false);
  }
  if(return_type_id == 3 || return_type_id ==4)
  {
    $('.fe2_order').removeClass('hidden');
    $('.wh_div_cls').addClass('hidden');
  }
  else
  {
    $('.fe2_order').addClass('hidden');
    $('.wh_div_cls').removeClass('hidden');
    $("#error1").remove();
    $("#order_number").val('');
  }
});

$(document).on('change','.delivery_type',function(){
  var val = $(this).val();
  var role_id = $('.role_id').val();
  var task_access = $('.task_access').val();
  $('#error1').remove();
  $('#error2').remove();
  if(val==1)
  {
    $('.customerRow').removeClass('hidden');  
    $('.warehouseRow').addClass('hidden');  
    $('.othersRow').addClass('hidden');        
    $("#wh_error").remove(); 
    $("#system_error").remove();

    if(role_id == 5 || role_id == 6|| task_access==2||task_access==3)
    {
      $('.zonal_wh_row').removeClass('hidden');
    }        
  }
  else if(val == 2)
  {
    $('.customerRow').addClass('hidden');  
    $('.warehouseRow').removeClass('hidden');  
    $('.othersRow').addClass('hidden');         
    $("#system_error").remove();     
    if(role_id == 5 || role_id == 6|| task_access==2||task_access==3)
    {
      $('.zonal_wh_row').addClass('hidden');
    }   
  }
  else if(val == 3)
  {
    $('.customerRow').addClass('hidden');  
    $('.warehouseRow').addClass('hidden');  
    $('.othersRow').removeClass('hidden');
    $("#system_error").remove();  
    $("#wh_error").remove();
    
    if(role_id == 5 || role_id == 6|| task_access==2||task_access==3)
    {
      $('.zonal_wh_row').removeClass('hidden');
    } 
  }
});

$(document).on('change','.wh_id_cls',function(){
  var warehouse_id = $('.wh_id_cls').find(':selected').val();
  var data = 'warehouse_id='+warehouse_id;      
  $.ajax({
    type:"POST",
    url:SITE_URL+'getWarehouseAddress',
    data:data,
    cache:false,          
    success:function(html)
    {                 
      if(html!='null')
      {
        var arr = jQuery.parseJSON(html);
        $('#wh_address_1').val(arr['address1']);
        $('#wh_address_2').val(arr['address2']);
        $('#wh_address_3').val(arr['address3']);
        $('#wh_address_4').val(arr['address4']);
        $('#wh_pin_code').val(arr['pin_code']);
      }
    }
  });
});

$(document).on('click','.asset_condition_id',function(){
  var asset_condition_id = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(asset_condition_id==1)
  {
    ele.find(".error").remove();
    ele.find('.textarea').val('');
  }
  else
  {
    var text_data = ele.find('.textarea').val();
    if(text_data == '')
    {
      ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
    }
  }
});

$(document).on('change','.textarea',function(){
  var text = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(text != '')
  {
    ele.find(".error").remove();
  }
  else
  {
    ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
  }
});

//FE tools availability search
$(document).on('click','.check_with_sso',function(){
  var checked_length = $('.chkAll:checked').length;
  if(checked_length==0)
  {
    alert('Please Select Atleast one Asset!');
    return false;
  }
  else
  {
    $('#fe_owned_tools_frm').trigger("reset");
    $('#feSearchResults').html('<h4>Search FE Orders Here</h4>');
    var form_data = $('.raise_pickup_frm').serialize();
    $.ajax({
      type:"POST",
      url:SITE_URL+'pickup_sso_dropdown',
      data:form_data,
      cache:false,
      success:function(html){
        var obj = jQuery.parseJSON(html);
        $(".sso_data").html(obj['result1']);
        $('.sso_data').select2('destroy');
        $('.sso_data').select2();
      }
    });
    $('#form-primary').modal('show');
  }
});

$(document).on('click','#fe_owned_search',function(){
  var submit_button = 1; // don't depend on segment
  $('#submit_button').val(submit_button);
  searchFeOwnedTools();
});

$(document).on('click','#reset_search',function(){
  $('#fe_owned_tools_frm').trigger("reset");
  var submit_button = 1;
  $('.sso_data').select2('destroy'); 
  $('.sso_data').select2();
  $('#submit_button').val(submit_button);
  searchFeOwnedTools();
});

$(document).on('click','#feToolSearchPagination .pagination a',function(){
  var submit_button  =0;
  $('#submit_button').val(submit_button);
  var offset = $(this).attr("href");
  if(offset == 0)
  {
    $('#current_offset').val('');
    searchFeOwnedTools();
  }
  else
  { 
    var arr = offset.split('#');
    var second = arr[1];
    if(second.length == 0)
    {
      $('#current_offset').val('');
      searchFeOwnedTools();
    }
    else
    {
      var second_arr = second.split('/');
      final_offset = second_arr[1];
      $('#current_offset').val(final_offset);
      searchFeOwnedTools();
    }
  }
});

function searchFeOwnedTools()
{
  $("#form-primary").css("opacity",0.5);
  $("#loaderID").css("opacity",1);
  var data = $('#fe_owned_tools_frm').serialize();
  var current_offset = $('#current_offset').val();
  var url = SITE_URL+'ajax_toolsNeededByFE';
  var submit_button = $('#submit_button').val();
  if(submit_button == 0 ) // don't depend on submit button
  {
    if(current_offset!='')
    {
      url += '/'+current_offset;
    }
    else
    {
      $('#current_offset').val('');
    }
  }
  else
  {
    $('#current_offset').val('');
  }
  $.ajax({
    type:"POST",
    url:url,
    data:data,
    cache:false,
    success:function(response){
      var obj = jQuery.parseJSON(response);
      $("#form-primary").css("opacity",1);
      $("#loaderID").css("opacity",0);
      $("#feSearchResults").html(obj['result']);
    }
  });
}

$(document).on('change','.fe1_order',function(){
  var chk_len = $('.fe1_order:checked').length;
  if(chk_len>0)
  {
    $('#confirm_fe1_order').prop('disabled',false);
  }
  else
  {
    $('#confirm_fe1_order').prop('disabled',true);
  }
});

$(document).on('click','#confirm_fe1_order',function(){
  var order_number = $('.fe1_order:checked').val();
  if(order_number!='')
  {
    $('.fe2_order_number').val(order_number);
    var form_data = $('.raise_pickup_frm').serialize();
    $.ajax({
      type:"POST",
      url:SITE_URL+'check_order_matching',
      data:form_data,
      cache:false,
      success:function(html){
        var obj = jQuery.parseJSON(html);
        if(obj['error_message']!='')
        {
          alert(obj['error_message']);
          return false;
        }
        else
        {
          $("#error1").remove();
          $('#order_number').val(order_number);
          $('#form-primary').modal('hide');
        }
      }
    });
  }
  else
  {
    $("#error1").remove();
    $('.fe2_order_number').val('');
    $('#order_number').val('');
  }
});

$(document).on('blur','#order_number',function(){
  $("#error1").remove();
  var order_number = $(this).val();
  if(order_number!='')
  {
    $('.fe2_order_number').val(order_number);
    var form_data = $('.raise_pickup_frm').serialize();
    $.ajax({
      type:"POST",
      url:SITE_URL+'check_order_matching',
      data:form_data,
      cache:false,
      success:function(html){
        var obj = jQuery.parseJSON(html);
        if(obj['error_message']!='')
        {
          alert(obj['error_message']);
          $('#order_number').val('');
          return false;
        }
      }
    });
  }
});

$("form").submit(function(e)
{
  var o_d_type = $('input[name=delivery_type_id]:checked').val();
  if(o_d_type == 1)
  {
    $("#system_error").remove();
    $("#ct_pin_code_error").remove();
    var f_system_id = $('#system_id').val();
    if(f_system_id == '')
    {
      $("#system_id").after("<p class='color-danger' id='system_error'>System ID Required</p>");
      return false;
    }
    else
    {
      $("#system_error").remove();
    }
    var address1 = $('#ct_address_1').val();
    var address2 = $('#ct_address_2').val();
    var address3 = $('#ct_address_3').val();
    var address4 = $('#ct_address_4').val();
    var pin_code = $('#ct_pin_code').val();
    if(address1=='' || address2 =='' || address3 == '' || address4 =='' || pin_code =='')
    {
      alert('Please Fill All The Required Information');
      return false;
    }
    var address_check = $('input[name=check_address]:checked').val();
    if(address_check == 1)
    {
      if($('.address_value').val() == '')
      {
        alert('Please Fill All The Required Information');
        return false;
      }
    }
  }
  if(o_d_type == 2)
  {
    $("#wh_error").remove();
    var f_wh_id = $('.wh_id_cls').find(':selected').val();
    if(f_wh_id == '')
    {
      $(".wh_id_cls").after("<p class='color-danger' id='wh_error'>Plase Select Warehouse</p>");
      return false;
    }
    else
    {
      $("#wh_error").remove();
    }
    var address1 = $('#wh_address_1').val();
    var address2 = $('#wh_address_2').val();
    var address3 = $('#wh_address_3').val();
    var address4 = $('#wh_address_4').val();
    var pin_code = $('#wh_pin_code').val();
    if(address1=='' || address2 =='' || address3 == '' || address4 =='' || pin_code =='')
    {
      alert('Please Fill All The Required Information');
      return false;
    }
  }
  if(o_d_type == 3)
  {
    $("#wh_error").remove();
    $("#system_error").remove();
    var address1 = $('#address_1').val();
    var address2 = $('#address_2').val();
    var address3 = $('#address_3').val();
    var address4 = $('#address_4').val();
    var pin_code = $('#pin_code').val();
    if(address1=='' || address2 =='' || address3 == '' || address4 =='' || pin_code =='')
    {
      alert('Please Fill All The Required Information');
      return false;
    }
  }

  var f_return_type = $('.return_cls').val();
  if(f_return_type == 3 || f_return_type == 4)
  {
    var f_order_number = $('#order_number').val();
    $('#error1').remove();
    if(f_order_number)
    {
      $('#error1').remove();
    }
    else
    {
      $("#order_number").after("<p class='color-danger' id='error1'>This Value Required.</p>");
      return false;
    }

    $('.asset_status:checked').each(function(){
      if($(this).val() == 2)
      {
        alert('Sorry!,you can not give the lost tools to other FE');
        e.preventDefault();
        return false;
      }
   });
  }
  else
  {
    var r_wh_id = parseInt($('.r_wh_cls').find(':selected').val());
    $('#error1').remove();
    if(r_wh_id)
    {
      $('#error1').remove();
    }
    else
    {
      $(".r_wh_cls").after("<p class='color-danger' id='error1'>This Value Required</p>");
      return false;
    }
  }

  $("button[name='submit_pickup']").prop("disabled",true);
});