$(document).on('click','.asset_condition_id',function(){
  var asset_condition_id = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(asset_condition_id==1)
  {
    ele.find(".error").remove();
    ele.find('.textarea').val('');
  }
  else
  {
    ele.removeClass('hidden');
    ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
  }
});

$(document).on('change click','.textarea',function(){
  var text = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(text != '')
  {
    ele.find(".error").remove();
  }
  else
  {
    ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
  }

});

counter = 2;
count = 1;
$('.add_document').on('click',function()
{
  if(count == 0)
  {
    $('.tab_hide').removeClass('hidden');
    count++;
  }
  else
  {
    var ele = $('.document_table').find('tbody tr:last'); 
    var sl_no = ele.find('td:first .sno').html();
    var doc_type = ele.find('.doc_type').val();
    var doc = ele.find('.document').val();

    ele.find(".error").remove();
    if(doc_type == '' || doc == '')
    {
      if(doc_type == '')
      {
      ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
      }
      if(doc == '')
      {
        ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
        
      }
      return false;
    }
    
    var ele_clone = ele.clone();
    ele_clone.find('.sno').html(parseInt(sl_no)+1);
    ele_clone.find('input, select').attr("disabled", false).val('');
    ele_clone.find('td:last').show();
    ele_clone.find(".remove_bank_row").show();
    ele.after(ele_clone);
    ele_clone.show();
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/\[1\]/g, '['+counter+']');
    });
    ele_clone.html(function(i, oldHTML) 
    {
      return oldHTML.replace(/support\_document\_1/g,'support\_document\_'+counter);
    });
    counter++;
  }
});

$(document).on('click','.remove_bank_row',function(){
  var ele = $(this).closest('.doc_row');
  var rowCount = $('.document_table').find('tbody tr').length;
  if(rowCount==1)
  {
    ele.find('.doc_type').val('');
    ele.find('.document').val('');
    ele.find('.error').remove();
    $('.tab_hide').addClass('hidden');
    count = 0;
  }
  else
  {
     $(this).closest('tr').remove();
  }
  var sl_no=1;
  $('.sno').each(function(){
      $(this).html(sl_no);
      sl_no++;
  }); 
});

$(document).on('blur','.doc_type',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(1) .error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.document',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.doc_row').find('td:eq(2) .error');
  ele_error.remove();
});

 $(document).on('click','.activate',function(){
    var doc_id = $(this).closest('tr').find('.doc_id').val();
    var ele = $(this).closest('tr').find('.activate');
    var ele_a = $(this);
    if(doc_id != '')
    {
      var data = 'doc_id='+doc_id;
      $.ajax({
      type:"POST",
      url:SITE_URL+'activate_calibration_delivery_doc',
      data:data,
      cache:false,
      success:function(response){
        if(response>0)
        {
          ele_a.removeClass('btn-info activate').addClass('btn-danger deactivate');
          ele.find('i').removeClass('fa-check').addClass('fa-trash-o');
          alert('Record Has been Activated');
        }
        else
        {
          alert('Something went Wrong! try Again.');
        }
      }
    });
    }
  });

$(document).on('click','.deactivate',function(){
  var doc_id = $(this).closest('tr').find('.doc_id').val();
  var ele = $(this).closest('tr').find('.deactivate');
  var ele_a = $(this);
  if(doc_id != '')
  {
    var data = 'doc_id='+doc_id;
    $.ajax({
    type:"POST",
    url:SITE_URL+'deactivate_calibration_delivery_doc',
    data:data,
    cache:false,
    success:function(response){
      if(response>0)
      {
        ele_a.removeClass('btn-danger deactivate').addClass('btn-info activate');
        ele.find('i').removeClass('fa-trash-o').addClass('fa-check');
        alert('Record Has been deactivated');
      }
      else
      {
        alert('Something went Wrong! try Again.');
      }
    }
  });
  }
});

$(document).on('click','.submit_btn',function()
{
  var ele = $('.document_table').find('tbody tr:last'); 
  var doc_type = ele.find('.doc_type').val();
  var doc = ele.find('.document').val();
  ele.find(".error").remove();
  if(doc_type == '' && doc == ''){ }
  else
  {
    if(doc_type == '')
    {
    ele.find('td:eq(1) .doc_type').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(doc == '')
    {
      ele.find('td:eq(2) .document').after("<p class='color-danger error'>This Value is Required</p>");
      
    }
    if(doc_type == "" || doc == "")
    {
      alert("please fill All Required Fields!");
      return false;
    }
  }

  var count = 0;
  $('.tabb tbody tr').each(function() {
      var val = $('td',this).find('.asset_condition_id:checked').val();
      $('td',this).find('.error').remove();
      if(val !=1)
      {
        var data = $(this).find('.textarea').val();
        if(data == '')
        {
          $('td',this).find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
          count++;
        }

      }
      
  });
  if(count > 0)
  {
    alert("Please fill All Required Fields!");
    return false;
  }
  
  if ($('.error').length) {
    e.preventDefault();
    alert("Please fill All Required Fields!");
    return false;
  }
}); 

$(".date_calibration").datepicker({
    dateFormat: "dd-mm-yy",
    changeMonth: true,
    changeYear: true,
    minDate: 0,
    onSelect: function (date) 
    {
       var date2 = $(this).datepicker('getDate');
       $('.date').datepicker('option', 'minDate', date2);
    }
  });