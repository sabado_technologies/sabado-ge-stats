function check_designation_name_availability()
{
  $("#designation_name").on('keyup keypress blur change',function () {
      $(".error").remove();
      var name = $(this).val();
      var designation_id = $("#designation_id").val();
      var data = 'name='+name+'&designation_id='+designation_id;
      if(name!=''){
      $.ajax({
        type:"POST",
        url:SITE_URL+'check_designation_name_availability',
        data:data,
        cache:false,
        success:function(response){
          $(".error").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#designation_name").after("<p class='color-danger error'>This Designation name already exists</p>");
          }
          else
          {
            $(".error").remove();
          }
        }
      });
    }
    });
}
$("form").submit(function( e ) {
  if ($('.error').length) {
    e.preventDefault();
    return false;
  }
});

