$(document).ready(function(){
  //$('.select3').select2();
  country_id = $('#country_id').val();
  select2Ajax('sub_tool_id', 'get_tool_master_tools', country_id, 3);
});

function add_component_new_row()
{
  $('.details').hide();
  $(document).on('click',".toggle-details",function () { 
    var row=$(this).closest('tr');
    var next=row.next();
    $('.details').not(next).hide();
    $('.toggle-details').not(this).attr('src',ASSET_URL+'images/plus.png');
    next.toggle();
    if (next.is(':hidden')) {
      $(this).attr('src',ASSET_URL+'images/plus.png');
      $(this).attr('title','expand');
    } else {
      $(this).attr('src',ASSET_URL+'images/minus.png');
      $(this).attr('title','collapse');
    }
  });

  var tool_id = $('#tool_id').val();
  var count_c = $('#count_c').val();
  if(tool_id >0 && count_c >0)
  {
    $('.tab_hide').removeClass('hidden');
    var count = 1; 
  }
  else
  {
    var count = 0; 
  }
  
  $('#add_component').on('click',function()
  {
    var ele = $('.component_table').find('tbody').find('tr:last');
    ele.find(".error").remove();
    country_id = $('#country_id').val();
    count++;
    if(count==1)
    {
      $('.tab_hide').removeClass('hidden');
      ele.find('.sub_tool_id').select2('destroy'); 
      ele.find('.sub_tool_id').val(''); 
      select2Ajax('sub_tool_id', 'get_tool_master_tools', country_id, 3);
      ele.find('.sub_quantity').val('');
    }
    else
    {
      var ele = $('.component_table').find('tbody tr:last'); 
      var sl_no = ele.find('td:first .sno').html();
      var sub_tool_id = ele.find('.sub_tool_id :selected').val();
      var sub_quantity = ele.find('.sub_quantity').val();

      ele.find(".error").remove();
      if(sub_tool_id == '' || sub_quantity == '')
      {
        if(sub_tool_id == '')
        {
        ele.find('td:eq(1) .check_here').after("<p class='color-danger error'>This Value is Required</p>");
        }
        if(sub_quantity == '')
        {
          ele.find('td:eq(2) .sub_quantity').after("<p class='color-danger error'>This Value is Required</p>");
        }
        return false;
      }
      ele.find('.sub_tool_id').select2('destroy');
      var ele_clone = ele.clone(true);

      $('.component_table').append(ele_clone);
      ele_clone.find('.sub_tool_id').val('');
      select2Ajax('sub_tool_id', 'get_tool_master_tools', country_id, 3);
      ele_clone.find('.sno').html(parseInt(sl_no)+1);
      ele_clone.find('input, select').attr("disabled", false).val('');
      ele_clone.find('td:last').removeAttr('style').show();
      ele_clone.find(".remove_bank_row").show();
      //ele.after(ele_clone);
      //ele_clone.show();
    }
  });
  
  $(document).on('click','.deactivate_sub_comp',function(){
    var tool_part_id = $(this).closest('tr').find('.tool_part_id').val();
    var ele = $(this).closest('tr');
    if(tool_part_id != '')
    {
      var data = 'tool_part_id='+tool_part_id;
      $.ajax({
      type:"POST",
      url:SITE_URL+'deactivate_tool_sub_component',
      data:data,
      cache:false,
      success:function(response){
        if(response>0)
        {
          //ele.remove();
          alert('Record Has been deactivated !');
          var sl_no=1;
          $('.sno').each(function(){
              $(this).html(sl_no);
              sl_no++;
          });
        }
        else
        {
          alert('No changes Occured !');
        }
      }
    });
    }

  });

  $(document).on('click','.remove_bank_row',function(){
    var rowCount = $('.component_table').find('tbody tr').length;
    var ele = $('.component_table').find('tbody tr:last'); 
    if(rowCount==1)
    {
      ele.closest('.sub_comp_row').find('.error').remove();
      $('.tab_hide').addClass('hidden');
      count = 0;
    }
    else
    {
       $(this).closest('tr').remove();
    }
    var sl_no=1;
    $('.sno').each(function(){
        $(this).html(sl_no);
        sl_no++;
    }); 
  });

  $(document).on('change','.sub_tool_id',function(){
    var value = $(this).val();
    var ele = $(this);
    var country_id = $('#country_id').val();
    var ele_row = ele.closest('.sub_comp_row');
    var ele_error = ele.closest('.sub_comp_row').find('td:eq(1) .error');
    ele_error.remove();

    if(value == '')
    {
      ele.after("<p class='color-danger error'>This Value is Required</p>");
    }
    else
    {
      var check = 0;
      $('.sub_tool_id').each(function(){
          var each_part_id = $(this).val();
          if(value == each_part_id)
          {
            check++;
          }
          if(check>1)
          {
            if(value!='')
            {
              alert('Please Select Other Tool Number!');
              ele_row.find('.sub_tool_id').select2('destroy'); 
              ele_row.find('.sub_tool_id').val(''); 
              select2Ajax('sub_tool_id', 'get_tool_master_tools', country_id, 3);
            }
          }
      });
      ele_error.remove();
    }
  });

$(document).on('blur','.sub_quantity',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.sub_comp_row').find('td:eq(2) .error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

  $(document).on('change','.cal_type_id',function(){

    var val = $(this).val();
    if(val==1)
    {
      $('.cal_sup').removeClass('hidden');
      $(".cal_supplier_id").val('');
    }
    else
    {
      $('.cal_sup').addClass('hidden');
       $(".cal_supplier_id").val('');
    }
});
}

function get_asset_level_id()
{
  $('.asset_level_id').on('change',function(){
    var asset_level_id = $(this).val();
    if(asset_level_id == 3)
    {
      
      $('.div2, .div3').addClass('hidden');
      //$('tool_type_id').removeAttr("required",'');
      //$('length').removeAttr('required','').removeClass('parsley-validated');
      //$('breadth').removeAttr('required','').removeClass('parsley-validated');
      //$('height').removeAttr('required','').removeClass('parsley-validated');

    }
    else
    {
      $('.div2, .div3').removeClass('hidden');
      //$('tool_type_id').Attr('required','required');
      //$('length').attr('required','required');
      //$('breadth').attr('required','required');
      //$('height').attr('required','required');
    }

  });
}

function check_part_number_availability()
{
  $("#part_number").on('blur',function () {
      $("#error").remove();
      var part_number = $(this).val();
      var tool_id = $("#tool_id").val();
      var country_id = $('#country_id').val();
      var data = 'part_number='+part_number+'&tool_id='+tool_id+'&country_id='+country_id;
      
      $.ajax({
        type:"POST",
        url:SITE_URL+'check_part_number_availability',
        data:data,
        cache:false,
        success:function(response){
          $("#error").remove();
          if(response>0)
          {
            //$("#name").val("");
            $("#part_number").after("<p class='color-danger' id='error'>This Part Number already exists</p>");
          }
          else
          {
            $("#error").remove();
          }
        }
      });
    });
}

function check_tool_code_availability()
{
  $("#tool_code").on('keyup keypress blur',function () {
      $("#error1").remove();
      var tool_code = $(this).val();
      var country_id = $('#country_id').val();
      var tool_id = $("#tool_id").val();

      var data = 'tool_code='+tool_code+'&tool_id='+tool_id+'&country_id='+country_id;
      
      $.ajax({
        type:"POST",
        url:SITE_URL+'check_tool_code_availability',
        data:data,
        cache:false,
        success:function(response){
          $("#error1").remove();
          if(response>0)
          {
            $("#tool_code").after("<p class='color-danger' id='error1'>This Tool Code already exists</p>");
          }
          else
          {
            $("#error1").remove();
          }
        }
      });
    });
}

$("form").submit(function( e ) {
  var ele = $('.component_table').find('tbody tr:last'); 
  var sub_tool_id = ele.find('.sub_tool_id :selected').val();
  var sub_quantity = ele.find('.sub_quantity').val();
  ele.find(".error").remove();
  if(sub_tool_id == '' && sub_quantity == ''){ }
  else
  {
    if(sub_tool_id == '')
    {
    ele.find('td:eq(1) .check_here').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(sub_quantity == '')
    {
      ele.find('td:eq(2) .sub_quantity').after("<p class='color-danger error'>This Value is Required</p>");
      
    }
    if(sub_quantity == "" || sub_tool_id == "")
    {
      alert("please fill All Required Fields!");
      return false;
    }
  }
  if ($('#error ,#error1').length) {
    e.preventDefault();
    return false;
  }
});