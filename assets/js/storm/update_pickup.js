$(document).on('click','.courier_type',function(){
  var courier_type = $(this).val();
  if(courier_type==1)
  {
    $('.courier_div').removeClass('hidden');
    $('.docket_div').removeClass('hidden');
    $('.contact_div').addClass('hidden');
    $('.contact_div').find('.error').remove();
    $('.phone_div').addClass('hidden');
    $('.phone_div').find('.error').remove();
    $('.vehicle_div').addClass('hidden');
    $('.vehicle_div').find('.error').remove();
  }
  else if(courier_type==2)
  {
    $('.contact_div').removeClass('hidden');
    $('.phone_div').removeClass('hidden');
    $('.courier_div').addClass('hidden');
    $('.courier_div').find('.error').remove();
    $('.docket_div').addClass('hidden');
    $('.docket_div').find('.error').remove();
    $('.vehicle_div').addClass('hidden');
    $('.vehicle_div').find('.error').remove();
  }
  else if(courier_type==3)
  {
    $('.vehicle_div').removeClass('hidden');
    $('.contact_div').removeClass('hidden');
    $('.phone_div').removeClass('hidden');
    $('.courier_div').addClass('hidden');
    $('.courier_div').find('.error').remove();
    $('.docket_div').addClass('hidden');
    $('.docket_div').find('.error').remove();
  }
});

$(document).on('blur','.courier_name',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.courier_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.docket_num',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.docket_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.vehicle_num',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.vehicle_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.contact_person',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.contact_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.phone_number',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.phone_div').find('.error');
  ele_error.remove();
  if(value == '')
  {
    ele.after("<p class='color-danger error'>This Value is Required</p>");
  }
  else
  {
    ele_error.remove();
  }
});

$(document).on('blur','.expected_date',function(){
  var value = $(this).val();
  var ele = $(this);
  var ele_error = $(this).closest('.expected_date_row').find('.error');
  ele_error.remove();
});

$("form").submit(function( e ) {
  $('.courier_div,.docket_div,.vehicle_div,.contact_div,.phone_div,.expected_date_row').find('.error').remove();
  var courier_type = $('input[name=courier_type]:checked').val();
  var courier_name = $('.courier_name').val();
  var docket_num = $('.docket_num').val();
  var vehicle_num = $('.vehicle_num').val();
  var contact_person = $('.contact_person').val();
  var phone_number = $('.phone_number').val();
  var expected_date = $('.expected_date').val();
  if(expected_date == '')
  {
    $('.expected_date').after("<p class='color-danger error'>This Value is Required</p>");
  }
  if(courier_type == 1)
  {
    if(courier_name == '')
    {
      $('.courier_name').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(docket_num == '')
    {
      $('.docket_num').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(courier_name == '' || docket_num == '' || expected_date == '')
    {
      alert("Please fill All Required Fields!");
      return false;
    }
  }
  else if(courier_type == 2)
  {
    if(contact_person == '')
    {
       $('.contact_person').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(phone_number == '')
    {
       $('.phone_number').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(contact_person == '' || phone_number == '' || expected_date == '')
    {
      alert("Please fill All Required Fields!");
      return false;
    }
  }
  else if(courier_type == 3)
  {
    if(contact_person == '')
    {
       $('.contact_person').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(phone_number == '')
    {
       $('.phone_number').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(vehicle_num == '')
    {
       $('.vehicle_num').after("<p class='color-danger error'>This Value is Required</p>");
    }
    if(contact_person == '' || phone_number == '' || vehicle_num == '' || expected_date == '')
    {
      alert("Please fill All Required Fields!");
      return false;
    }
  }
  var billed_to = $('.billed_to :selected').val();
  var add1 = $('.add1').val();
  var add2 = $('.add2').val();
  var add3 = $('.add3').val();
  var add4 = $('.add4').val();
  var pin_code = $('.pin_code').val();
  var gst_number = $('.gst_number').val();
  var pan_number = $('.pan_number').val();
  if(billed_to == '' || add1 == '' || add2 == '' || add3 == '' || add4 == '' || pin_code == '' || gst_number == '' || pan_number == '')
  {
    alert("Please fill All Required Fields!");
    return false;
  }

  if ($('.error').length) {
    e.preventDefault();
    alert("Please fill All Required Fields!");
    return false;
  }

  $.ajax({
    url: SITE_URL,
    context: document.body,
    success: function(s){
      window.location.href = SITE_URL+'closed_pickup_list';
    }
  });
});