$('.upload_user').on('click',function(){
  $("#orderCartContainer").css("opacity",0.5);
  $("#loaderID").css("opacity",1);
});

$('#customer_id').on('blur change',function()
{
  var customer_id = $(this).val();
  var country_id = $('#country_id').val();
  var data = 'customer_id='+customer_id+'&country_id='+country_id;
  if(customer_id == '')
  {
    $('#site_id').attr("readonly","readonly"); 
    $('#customer_name').val('');
    $('#service_region').val('');
  }
  else
  {
    $.ajax({
      type:"POST",
      url:SITE_URL+'get_customer_details_by_id',
      data:data,
      cache:false,
      success:function(html)
      {
        if(html!='null')
        {
          var arr = jQuery.parseJSON(html);
          $('#customer_name').val(arr['name']);
          $('#service_region').val(arr['service_region']);
          $('#site_id').removeAttr("readonly"," ");
        }
        else
        {
          $('#customer_name').val('');
          $('#service_region').val('');
        }
      }
    });
  }
});

$("form").submit(function( e ) {
  if ($('#error').length) {
    e.preventDefault();
    return false;
  }
});