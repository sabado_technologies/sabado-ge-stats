$(document).on('click','.asset_condition_id',function(){
  var asset_condition_id = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(asset_condition_id==1)
  {
    ele.find(".error").remove();
    ele.find('.textarea').val('');
  }
  else
  {
    ele.removeClass('hidden');
    ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
  }
});

$(document).on('change','.textarea',function(){
  var text = $(this).val();
  var ele = $(this).closest('.asset_row').find('.textbox');
  ele.find(".error").remove();
  if(text != '')
  {
    ele.find(".error").remove();
  }
  else
  {
    ele.find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
  }

});

$(document).on('click','.submit_btn',function(){
  var count = 0; remarks = 0;
  $('.health_table tbody tr').each(function() {
    var val = $('td',this).find('.asset_condition_id:checked').val();
      $('td',this).find('.error').remove();
      if(val !=1)
      {
        count++;
        var data = $('td',this).find('.textarea').val();
        if(data == '')
        {
          $('td',this).find('.textarea').after("<p class='color-danger error'>This Value is Required</p>");
          remarks++;
        }
      }
      
  });
  if(count == 0)
  {
    alert('Please fill Atleast one defective Asset');
    return false;
  }
  if(remarks > 0)
  {
    alert('Please Fill Required Fields!');
    return false;
  }
});